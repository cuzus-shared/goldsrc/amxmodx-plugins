#include <amxmodx>
#include <fakemeta>
#include <hamsandwich>
#include <cstrike>
#include <fun>

#define PLUGIN "R Paintball Gun"
#define VERSION "5.10"
#define AUTHOR "Rul4 - WhooKid"

#define MAX_PAINTBALLS	200
#define TASK_PB_RESET	1000
#define TASK_RELOAD	2000
#define TASK_NADE	5000
#define CROSSHAIR_REMOVED ( 1<<6 )

#define PB_MARKER 1
#define PB_USP 2
#define PB_GLOCK 3
#define PB_SNIPER 4
#define PB_LAUNCHER 5
#define PB_SHOTGUN 6
#define PB_SUPERMARKER 7

new g_msgHideWeapon;
new g_paintballs[MAX_PAINTBALLS], g_pbstatus[MAX_PAINTBALLS], g_pbcount, Float:lastshot[33], Float:nextattack[33], freezetime;
new pbgun, color, shots, blife, sound, bglow, damage, friendlyfire, beamspr;
new blood1, blood2, knife_ohk, jamon, pellets_real, pbslauncher;

new fire_rate_low, fire_rate_medium, fire_rate_high, jam_prob_low, jam_prob_high, gravity, speed_marker, speed_pistol, speed_shotgun, speed_sniper, dispersion, removecrosshair, speed_launcher, speed_smarker ;

static const g_shot_anim[8] = {0, 3, 9, 5, 1, 1, 1, 3};
static const g_pbgun_models[11][] = {"models/v_pbgun.mdl", "models/v_pbgun1.mdl", "models/v_pbgun2.mdl", "models/v_pbgun3.mdl", "models/v_pbgun4.mdl", "models/v_pbgun5.mdl", "models/v_pbgun6.mdl", "models/v_pbgun7.mdl", "models/v_pbgun8.mdl", "models/v_pbgun9.mdl", "models/v_pbgun10.mdl"};

new g_Jammed[33];

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);
	register_cvar("rpaintballgun", VERSION, FCVAR_SERVER|FCVAR_UNLOGGED);
	set_cvar_string("rpaintballgun", VERSION)
	register_clcmd("say /ent", "ent_info", ADMIN_SLAY);
	g_msgHideWeapon  = get_user_msgid( "HideWeapon" );
			
	if (get_pcvar_num(pbgun))
	{
		register_event("CurWeapon", "ev_curweapon", "be");
		register_logevent("ev_roundstart", 2, "0=World triggered", "1=Round_Start");
		if (get_cvar_num("mp_freezetime") > 0)
			register_event("HLTV", "ev_freezetime", "a", "1=0", "2=0");

		register_forward(FM_Touch, "fw_touch");
		register_forward(FM_SetModel, "fw_setmodel");
		register_forward(FM_CmdStart, "fw_cmdstart");
		register_forward(FM_UpdateClientData, "fw_updateclientdata",1);
		if (get_pcvar_num(pbslauncher)) 
			register_forward(FM_Think, "fw_think");

		RegisterHam ( Ham_TakeDamage, "player", "fwd_TakeDamage" );
		
		RegisterHam (Ham_Weapon_Reload, "weapon_m3", "fw_reload", 1);
		RegisterHam (Ham_Weapon_Reload, "weapon_xm1014", "fw_reload", 1);
		RegisterHam (Ham_Weapon_Reload, "weapon_scout", "fw_reload", 1);
		
		set_cvar_num ("sv_maxvelocity", 4000);
		
		new a, max_ents_allow = global_get(glb_maxEntities) - 5;
		for (a = 1; a <= get_pcvar_num(shots); a++)
			if (a < MAX_PAINTBALLS)
				if (engfunc(EngFunc_NumberOfEntities) < max_ents_allow)
				{
					g_paintballs[a] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
					if (pev_valid(g_paintballs[a]))
					{
						set_pev(g_paintballs[a], pev_effects, pev(g_paintballs[a], pev_effects) | EF_NODRAW);
						g_pbcount++;
					}
				}
		if (g_pbcount < 1)
			set_fail_state("[AMXX] Failed to load Realism for Paintball Gun (unable to create ents)");

		server_print("*** %s v%s by %s Enabled ***", PLUGIN, VERSION, AUTHOR);
	}
}

public plugin_precache()
{
	pbgun = register_cvar("amx_pbgun", "1");
	knife_ohk = register_cvar("amx_knifeonehitkill", "1");
	jamon = register_cvar("amx_pbgun_jam","1");

	friendlyfire = get_cvar_pointer("mp_friendlyfire");
	
	if (get_pcvar_num(pbgun)) {
		
		color = register_cvar("pbgun_color", "1");
		shots = register_cvar("pbgun_shots", "100");
		blife = register_cvar("pbgun_life", "15");
		sound = register_cvar("pbgun_sound", "1");
		bglow = register_cvar("pbgun_glow", "c");
		damage = register_cvar("pbgun_damage", "100");
		
		fire_rate_low = register_cvar("pbgun_fireratelow", "1.1");
		fire_rate_high = register_cvar("pbgun_fireratehigh", "0.16");
		fire_rate_medium = register_cvar("pbgun_fireratemedium", "0.7");
		jam_prob_low = register_cvar("pbgun_jamproblow", "30");
		jam_prob_high = register_cvar("pbgun_jamprobhigh", "100");
		gravity = register_cvar("pbgun_gravity", "1");
		speed_marker = register_cvar("pbgun_velocity_marker", "2000");
		speed_smarker = register_cvar("pbgun_velocity_super_marker", "2500");
		speed_pistol = register_cvar("pbgun_velocity_pistol", "1500");
		speed_launcher = register_cvar("pbgun_velocity_launcher", "1500");
		speed_shotgun = register_cvar("pbgun_velocity_shotgun", "1500");
		speed_sniper = register_cvar("pbgun_velocity_sniper", "4000");
		dispersion = register_cvar("pbgun_dispersion", "100");
		removecrosshair = register_cvar("pbgun_nocrosshair", "1");
		pellets_real = register_cvar("pbgun_real_pellets", "1");
		pbslauncher = register_cvar("pbgun_pbslauncher", "1");

		precache_model(g_pbgun_models[3]);
		precache_model(g_pbgun_models[7]);
		precache_model("models/v_pbgun1.mdl");
		precache_model("models/v_pbgun6.mdl");
		precache_model("models/p_pbgun.mdl");
		precache_model("models/p_pbgun1.mdl");
		precache_model("models/w_pbgun.mdl");
		precache_model("models/v_pb_sniper.mdl");
		precache_model("models/p_pb_sniper.mdl");
		precache_model("models/w_pb_sniper.mdl");
		precache_model("models/v_pb_shotgun.mdl");
		precache_model("models/p_pb_shotgun.mdl");
		precache_model("models/w_pb_shotgun.mdl");
		precache_model("models/v_pb_launcher.mdl");
		precache_model("models/w_pb_launcher.mdl");
		precache_model("models/p_pb_launcher.mdl");
		precache_model("models/w_pb_launch_he.mdl");
		precache_model("models/p_pb_knife.mdl");
		precache_model("models/v_pb_knife.mdl");
		precache_model("models/v_pb_superlauncher.mdl");
		precache_model("models/w_pb_supernade.mdl");
		blood1 = precache_model("sprites/blood.spr");
		blood2 = precache_model("sprites/bloodspray.spr");
		precache_model("models/v_pbusp.mdl");
		precache_model("models/p_pbusp.mdl");
		precache_model("models/v_pbglock.mdl");
		precache_model("models/p_pbglock.mdl");

		precache_sound("misc/pb1.wav");
		precache_sound("misc/pb2.wav");
		precache_sound("misc/pb3.wav");
		precache_sound("misc/pb4.wav");
		precache_sound("misc/pbg.wav");
		precache_sound("misc/pb_launcher.wav");
		precache_model("models/w_paintball.mdl");
		precache_model("sprites/paintball.spr");
		precache_sound("weapons/scout_fire-1.wav");
		precache_sound("weapons/dryfire_pistol.wav");
		precache_sound("weapons/scout_bolt.wav");
		precache_sound("weapons/draw.wav");
		precache_sound("weapons/insert.wav");
		precache_sound("weapons/magin.wav");
		precache_sound("weapons/magout.wav");
		precache_sound("weapons/magrel.wav");
		precache_sound("weapons/pullout.wav");
		precache_sound("weapons/roll.wav");
	}
	beamspr = precache_model("sprites/laserbeam.spr");
}

public ent_info(id)
	client_print(id, print_chat, "[AMXX] [Ent Info (Current/Max)] Paintballs: (%d/%d)   Entities: (%d/%d)", g_pbcount, get_pcvar_num(shots), engfunc(EngFunc_NumberOfEntities), global_get(glb_maxEntities));

public ev_curweapon(id)
{
	//replace the weapon model with the PB model
	new model[25];
	pev(id, pev_viewmodel2, model, 24);
	if (equali(model, "models/v_mp5.mdl") && get_pcvar_num(pbgun))
	{
		set_pev(id, pev_viewmodel2, (get_user_team(id) == 1) ? g_pbgun_models[3] : g_pbgun_models[7]);
		set_pev(id, pev_weaponmodel2, "models/p_pbgun1.mdl");
	}
	else if (equali(model, "models/v_usp.mdl") && get_pcvar_num(pbgun))
	{
		set_pev(id, pev_viewmodel2, "models/v_pbusp.mdl");
		set_pev(id, pev_weaponmodel2, "models/p_pbusp.mdl");
	}
	else if (equali(model, "models/v_glock18.mdl") && get_pcvar_num(pbgun))
	{
		set_pev(id, pev_viewmodel2, "models/v_pbglock.mdl");
		set_pev(id, pev_weaponmodel2, "models/p_pbglock.mdl");
	}else if (equali(model, "models/v_scout.mdl") && get_pcvar_num(pbgun))
	{
		set_pev(id, pev_viewmodel2, "models/v_pb_sniper.mdl");
		set_pev(id, pev_weaponmodel2, "models/p_pb_sniper.mdl");
	}else if (equali(model, "models/v_m3.mdl") && get_pcvar_num(pbgun))
	{
		set_pev(id, pev_viewmodel2, "models/v_pb_launcher.mdl");
		set_pev(id, pev_weaponmodel2, "models/p_pb_launcher.mdl");
	}else if (equali(model, "models/v_knife.mdl") && get_pcvar_num(pbgun))
	{
		set_pev(id, pev_viewmodel2, "models/v_pb_knife.mdl");
		set_pev(id, pev_weaponmodel2, "models/p_pb_knife.mdl");
	}else if (equali(model, "models/v_p90.mdl") && get_pcvar_num(pbgun))
	{
		set_pev(id, pev_viewmodel2, (get_user_team(id) == 1) ? "models/v_pbgun1.mdl" : "models/v_pbgun6.mdl" );
		
		set_pev(id, pev_weaponmodel2, "models/p_pbgun1.mdl");
	}else if (equali(model, "models/v_xm1014.mdl") && get_pcvar_num(pbgun))
	{
		set_pev(id, pev_viewmodel2, "models/v_pb_shotgun.mdl");
		set_pev(id, pev_weaponmodel2, "models/p_pb_shotgun.mdl");
	}else if (equali(model, "models/v_flashbang.mdl") && get_pcvar_num(pbgun) && get_pcvar_num(pbslauncher))
	{
		set_pev(id, pev_viewmodel2, "models/v_pb_superlauncher.mdl");
		set_pev(id, pev_weaponmodel2, "models/p_pb_shotgun.mdl");
	}
	
	new my_pbgun = user_has_pbgun(id);
	if (my_pbgun){
		
		if (get_pcvar_num(removecrosshair))
		{
			//Remove the crosshair
			message_begin( MSG_ONE_UNRELIABLE, g_msgHideWeapon, _, id );
			write_byte( CROSSHAIR_REMOVED );
			message_end();
		}
		
	}
}

public fw_setmodel(ent, model[]) 
{
	//replace dropped models with PB models
 	if (equali(model, "models/w_mp5.mdl") || equali(model, "models/w_p90.mdl")) 
		if (get_pcvar_num(pbgun))
		{
			engfunc(EngFunc_SetModel, ent, "models/w_pbgun.mdl");
							
			return FMRES_SUPERCEDE;
		}
	if (equali(model, "models/w_scout.mdl")) 
		if (get_pcvar_num(pbgun))
		{
			engfunc(EngFunc_SetModel, ent, "models/w_pb_sniper.mdl");

			return FMRES_SUPERCEDE;
		}
	if (equali(model, "models/w_m3.mdl")) 
		if (get_pcvar_num(pbgun))
		{
			engfunc(EngFunc_SetModel, ent, "models/w_pb_launcher.mdl");

			return FMRES_SUPERCEDE;
		}
	if (equali(model, "models/w_xm1014.mdl")) 
		if (get_pcvar_num(pbgun))
		{
			engfunc(EngFunc_SetModel, ent, "models/w_pb_shotgun.mdl");
		
			return FMRES_SUPERCEDE;
		}
	if (equali(model, "models/w_flashbang.mdl") && get_pcvar_num(pbslauncher)) 
		if (get_pcvar_num(pbgun))
		{
			engfunc(EngFunc_SetModel, ent, "models/w_pb_supernade.mdl");
		
			return FMRES_SUPERCEDE;
		}

	return FMRES_IGNORED;
}

public fw_updateclientdata(id, sw, cd_handle)
{
	//this function overrides the client side sounds and stuff
	if (user_has_pbgun(id) && cd_handle)
	{

		set_cd(cd_handle, CD_ID, 1);
		get_cd(cd_handle, CD_flNextAttack, nextattack[id]);
		
		return FMRES_HANDLED;

	}
	return FMRES_IGNORED;
}

public fw_reload(weapon)
{
	new id = pev(weapon, pev_owner)	
	new ammo, null = get_user_weapon(id, ammo, null);
	new my_pbgun = user_has_pbgun(id)
	
	if ((my_pbgun == PB_LAUNCHER && ammo < 8) || (my_pbgun == PB_SHOTGUN && ammo < 7) || (my_pbgun == PB_SNIPER && ammo < 10))
	{
		lastshot[id] = get_gametime();
		set_hudmessage(255, 255, 255, -1.0, 0.33, 0, 1.0, 1.0);
		show_hudmessage(id, "Weapon reloading...");

		message_begin(MSG_ONE_UNRELIABLE, SVC_WEAPONANIM, _, id);
		write_byte(3);
		write_byte(0);
		message_end();
	}
}

public try_to_jam(id, my_pbgun){

	//chance of jamming the current weapon
	if(get_pcvar_num(jamon) && !g_Jammed[id] && my_pbgun != PB_GLOCK && my_pbgun != PB_USP && !is_user_bot(id))
	{			
		new average_usage_jam;
		
		switch (my_pbgun)
		{
			case PB_MARKER: average_usage_jam = get_pcvar_num(jam_prob_high);
			case PB_GLOCK: average_usage_jam = get_pcvar_num(jam_prob_high);
			case PB_USP: average_usage_jam = get_pcvar_num(jam_prob_high);
			case PB_SNIPER: average_usage_jam = get_pcvar_num(jam_prob_low);
			case PB_LAUNCHER: average_usage_jam = get_pcvar_num(jam_prob_low);
			case PB_SHOTGUN: average_usage_jam = get_pcvar_num(jam_prob_low);
			case PB_SUPERMARKER: average_usage_jam = get_pcvar_num(jam_prob_high) * 2;
			default: average_usage_jam = get_pcvar_num(jam_prob_high);
		}

		new jam = (random(average_usage_jam)==0);
		if(jam)
		{
			emit_sound(id,CHAN_AUTO, "weapons/dryfire_pistol.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
			g_Jammed[id] = my_pbgun;
		}
	}
	
}

public fw_cmdstart( id, uc_handle, random_seed )
{
    if ( !is_user_alive( id ) )
        return FMRES_IGNORED
    
    static buttons; buttons = get_uc( uc_handle, UC_Buttons )
    new my_pbgun = user_has_pbgun(id);

    //if jammed, clear the chamber
    if ( buttons & IN_USE && my_pbgun == g_Jammed[id] && my_pbgun && get_pcvar_num(jamon))
    	clear_chamber(id);

//if he is shooting with a PBGUN
    if ( buttons & IN_ATTACK && my_pbgun)
    {	
	//check if weapon is jammed
	if(g_Jammed[id] == my_pbgun && !is_user_bot(id) && get_pcvar_num(jamon))
	{
		weapon_jam(id, uc_handle);
		return FMRES_IGNORED;
	}
	
	set_uc (uc_handle, UC_Buttons, buttons & ~IN_ATTACK);

	new ammo, null = get_user_weapon(id, ammo, null);
	if (ammo)
	{
		new Float:gametime = get_gametime(), Float:g_speed;	
					
		switch (my_pbgun)
		{
			case PB_MARKER: g_speed = get_pcvar_float(fire_rate_high);
			case PB_GLOCK: g_speed = get_pcvar_float(fire_rate_high);
			case PB_USP: g_speed = get_pcvar_float(fire_rate_high);
			case PB_SNIPER: g_speed = get_pcvar_float(fire_rate_low);
			case PB_LAUNCHER: g_speed = get_pcvar_float(fire_rate_low);
			case PB_SHOTGUN: g_speed = get_pcvar_float(fire_rate_medium);
			case PB_SUPERMARKER: g_speed = get_pcvar_float(fire_rate_high)/2.0;
			default: g_speed = get_pcvar_float(fire_rate_high);
		}
		
		new cangofull = (my_pbgun == PB_MARKER || my_pbgun == PB_LAUNCHER || my_pbgun == PB_SHOTGUN || my_pbgun == PB_SUPERMARKER);
		
		//check that the button was not pressed before or that the weapon can go full-auto
		new auto = !(pev(id, pev_oldbuttons) & IN_ATTACK) || cangofull;
		
		//check the fire-rate and freezetime
		if (gametime-lastshot[id] > g_speed  && (is_user_bot(id) || nextattack[id] < 0.0) && !freezetime && auto)
		{
		
			new fired;
					
			if (my_pbgun == PB_SHOTGUN)
			{ //shoot 5 pellets
				for (new a = 0; a < 5; a++)
					fired = paint_fire(id, a) || fired;
			}
			else	//shoot 1 pellet
				fired = paint_fire(id, 0);
			
			if (fired)
			{
				
				lastshot[id] = gametime;
				set_user_clip(id, ammo - 1);
				
				//show the animation
				set_pev(id, pev_punchangle, Float:{-0.5, 0.0, 0.0});
				message_begin(MSG_ONE_UNRELIABLE, SVC_WEAPONANIM, _, id);
				write_byte(g_shot_anim[my_pbgun]);
				write_byte(0);
				message_end();
				
				//shooting sound
				if (get_pcvar_num(sound))
					if (my_pbgun != PB_SNIPER)
						if (my_pbgun != PB_LAUNCHER)
							emit_sound(id, CHAN_AUTO, "misc/pbg.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
						else
						emit_sound(id, CHAN_AUTO, "misc/pb_launcher.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
					else
						emit_sound(id, CHAN_AUTO, "weapons/scout_fire-1.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

				try_to_jam(id,my_pbgun)

			}
		}

	}
	return FMRES_IGNORED;
    }

    return FMRES_IGNORED
}

public paint_fire(id, num)
{
	new a, ent;
	while (a++ < g_pbcount - 1 && !ent)
		if (g_pbstatus[a] == 0)
			ent = g_pbstatus[a] = g_paintballs[a];
	if (!ent)
		while (a-- > 1 && !ent)
			if (g_pbstatus[a] == 2)
				ent = g_pbstatus[a] = g_paintballs[a];

	if (pev_valid(ent) && is_user_alive(id))
	{
		new Float:vangles[3], Float:nvelocity[3], Float:voriginf[3], vorigin[3], clr;
		set_pev(ent, pev_owner, id);
		
		new my_pbgun = user_has_pbgun(id);
		
		if (my_pbgun != PB_LAUNCHER) { //create a pellet
			set_pev(ent, pev_classname, "pbBullet");
			engfunc(EngFunc_SetModel, ent, "models/w_paintball.mdl");
			
			
			if (my_pbgun == PB_SHOTGUN) //make them smaller to avoid creation collisions
				engfunc(EngFunc_SetSize, ent, Float:{0.0, 0.0, 0.0}, Float:{1.0, 1.0, 1.0});
			else
				engfunc(EngFunc_SetSize, ent, Float:{-1.0, -1.0, -1.0}, Float:{1.0, 1.0, 1.0});
	
			switch (get_pcvar_num(color))
			{
				case 2: clr = (get_user_team(id) == 1) ? 0 : 1;
				case 3: clr = (get_user_team(id) == 1) ? 4 : 3;
				case 4: clr = (get_user_team(id) == 1) ? 2 : 5;
				default: clr = random_num(0, 6);
			}
			set_pev(ent, pev_skin, clr);
		
			set_pev(ent, pev_gravity, 0.5);	
		}else
		{	//create a nade
			set_pev(ent, pev_classname, "pbExplosive");
			engfunc(EngFunc_SetModel, ent, "models/w_pb_launch_he.mdl");
			engfunc(EngFunc_SetSize, ent, Float:{-3.0, -3.0, -3.0}, Float:{3.0, 3.0, 3.0});

			set_pev(ent, pev_gravity, 1.0);	
		}
		get_user_origin(id, vorigin, 1);
		
		if (my_pbgun == PB_SHOTGUN)
		{ //avoid creation collisions
			vorigin[0] += random(5)-2;
			vorigin[1] += random(5)-2;
			vorigin[2] += num*5
		}
		IVecFVec(vorigin, voriginf);
		engfunc(EngFunc_SetOrigin, ent, voriginf);

		vangles[0] = random_float(-180.0, 180.0);
		vangles[1] = random_float(-180.0, 180.0);
		set_pev(ent, pev_angles, vangles);

		pev(id, pev_v_angle, vangles);
		set_pev(ent, pev_v_angle, vangles);
		pev(id, pev_view_ofs, vangles);
		set_pev(ent, pev_view_ofs, vangles);

		new veloc;
		
		if (get_pcvar_num(gravity))
			set_pev(ent, pev_movetype, MOVETYPE_BOUNCE);
		else
			set_pev(ent, pev_movetype, MOVETYPE_FLY);
		set_pev(ent, pev_solid, 2);
	

		//Different guns shoot at different speeds
		switch (my_pbgun)
			{
				case PB_MARKER: veloc = get_pcvar_num(speed_marker);
				case PB_GLOCK: veloc = get_pcvar_num(speed_pistol);
				case PB_USP: veloc = get_pcvar_num(speed_pistol);
				case PB_SNIPER: veloc = get_pcvar_num(speed_sniper);
				case PB_LAUNCHER: veloc = get_pcvar_num(speed_launcher);
				case PB_SHOTGUN: veloc = get_pcvar_num(speed_shotgun);
				case PB_SUPERMARKER: veloc = get_pcvar_num(speed_smarker);
				default: veloc = get_pcvar_num(speed_pistol);
			}
		
		velocity_by_aim(id, veloc, nvelocity);
		
		//Fix aim for bots
		if (is_user_bot(id))
			switch (my_pbgun)
			{
				case PB_MARKER: nvelocity[2] += (get_pcvar_num(gravity))? 150 : 0;
				case PB_GLOCK: nvelocity[2] += (get_pcvar_num(gravity))? 200 : 0;
				case PB_USP: nvelocity[2] += (get_pcvar_num(gravity))? 200 : 0;
				case PB_SNIPER: nvelocity[2] += (get_pcvar_num(gravity))? 35 : 0;
				case PB_LAUNCHER: nvelocity[2] += 250;
				case PB_SHOTGUN: nvelocity[2] += (get_pcvar_num(gravity))? 50 : 0;
				case PB_SUPERMARKER: nvelocity[2] += (get_pcvar_num(gravity))? 100 : 0;
				default: nvelocity[2] += (get_pcvar_num(gravity))? 150 : 0;
			}
		
		//Recoil!!!
		if (cs_get_user_zoom(id)== CS_SET_NO_ZOOM){
			//the sniper will shoot terribly bad without a scope
			new modifier = (my_pbgun != PB_SNIPER)? 1 : 5;
			
			new buttons = pev(id, pev_button);
			
			if (my_pbgun == PB_SHOTGUN)
				modifier = 2;
			else   //shoot worse while moving
				if (buttons & IN_JUMP || buttons & IN_FORWARD || buttons & IN_BACK || buttons & IN_MOVELEFT || buttons & IN_MOVERIGHT)
					modifier *= 2;
			
			new disp = get_pcvar_num(dispersion);
			nvelocity[0] += (random(disp)-disp/2) * modifier;
			nvelocity[1] += (random(disp)-disp/2) * modifier;
			nvelocity[2] += (random(disp)-disp/2) * modifier;
		}		
		
		set_pev(ent, pev_velocity, nvelocity);
		set_pev(ent, pev_effects, pev(ent, pev_effects) & ~EF_NODRAW);
		
		//glow
		set_task(0.1, "paint_glow", ent);
		
		if (my_pbgun != PB_LAUNCHER) //make it dissapear after a while
			set_task(15.0 , "paint_reset", ent+TASK_PB_RESET);
		else  //make it explode if there is no impact
			set_task(2.0, "time_explode", ent+TASK_PB_RESET);
	}

	return ent;
}

public fw_touch(bullet, ent)
{
	new class[20];
	pev(bullet, pev_classname, class, 19);

	if (!equali(class, "pbBullet")&&!equali(class, "pbHarmlessBullet")&&!equali(class, "pbExplosive"))
		return FMRES_IGNORED;

	new owner = pev(bullet, pev_owner);

	new Float:origin[3], class2[20], is_ent_alive = is_user_alive(ent);
	pev(ent, pev_classname, class2, 19);
	pev(bullet, pev_origin, origin);

	
	if (equali(class, "pbExplosive")){ //it is a nade
		new Float:gametime = get_gametime();
		
		//if it hits something after this time, explode
		if (gametime-lastshot[owner] > 0.5){			
			act_explode(bullet);
			
			set_pev(bullet, pev_velocity, Float:{0.0, 0.0, 0.0});
			set_pev(bullet, pev_classname, "pbPaint");
			set_pev(bullet, pev_solid, 0);
			set_pev(bullet, pev_movetype, 0);
			engfunc(EngFunc_SetModel, bullet, "sprites/paintball.spr");	
			
			new a, findpb = 0;
			while (a++ < g_pbcount && !findpb)
				if (g_paintballs[a] == bullet)
					findpb = g_pbstatus[a] = 2;
			remove_task(bullet);
			remove_task(bullet+TASK_PB_RESET);
			paint_reset(bullet+TASK_PB_RESET);
			return FMRES_HANDLED; 
		}
		else if (!equali(class2, "pbBullet")&&!equali(class2, "pbHarmlessBullet")&&get_pcvar_num(gravity)){
			
			//make it bounce and loose some velocity
			new Float:vel[3];
			pev(bullet,pev_velocity,vel);

			vel[0] /= 5.0;
			vel[1] /= 5.0;
			vel[2] /= 5.0;
			
			set_pev(bullet, pev_velocity, vel);
			
			if (!is_ent_alive) //make damage to world stuff
				ExecuteHam(Ham_TakeDamage, ent, owner, owner, get_pcvar_float(damage), 4098);
	
			
			return FMRES_IGNORED;
		}
	}

	//The bullet hits or bounces
	new hit;
	hit = random(5);

	if (is_ent_alive) //make damage
	{ 
		if (owner == ent || pev(ent, pev_takedamage) == DAMAGE_NO)
			return FMRES_IGNORED;
		if (get_user_team(owner) == get_user_team(ent))
			if (!get_pcvar_num(friendlyfire))
				return FMRES_IGNORED;

		if (hit != 0 && !equali(class, "pbHarmlessBullet"))
			ExecuteHam(Ham_TakeDamage, ent, owner, owner, get_pcvar_float(damage), 4098);
	}

	if (!equali(class2, "pbBullet")&&!equali(class2, "pbHarmlessBullet"))
		if (hit != 0 || !get_pcvar_num(pellets_real))
		{	//stop the pellet and make a splat
			set_pev(bullet, pev_velocity, Float:{0.0, 0.0, 0.0});
			set_pev(bullet, pev_classname, "pbPaint");
			set_pev(bullet, pev_solid, 0);
			set_pev(bullet, pev_movetype, 0);
			engfunc(EngFunc_SetModel, bullet, "sprites/paintball.spr");
	
			new a, findpb = 0;
			while (a++ < g_pbcount && !findpb)
				if (g_paintballs[a] == bullet)
					findpb = g_pbstatus[a] = 2;
	
			remove_task(bullet);
			remove_task(bullet+TASK_PB_RESET);
	
			if (get_pcvar_num(sound))
			{
				static wav[20];
				formatex(wav, 20, is_ent_alive ? "player/pl_pain%d.wav" : "misc/pb%d.wav", is_ent_alive ? random_num(4,7) : random_num(1,4));
				emit_sound(bullet, CHAN_AUTO, wav, 1.0, ATTN_NORM, 0, PITCH_NORM);
			}
	
			new bool:valid_surface = (is_ent_alive || containi(class2, "door") != -1) ? false : true;
			if (pev(ent, pev_health) && !is_ent_alive)
			{
				ExecuteHam(Ham_TakeDamage, ent, owner, owner, float(pev(ent, pev_health)), 0);
				valid_surface = false;
			}
			if (valid_surface)
			{
				paint_splat(bullet);
				set_task(float(get_pcvar_num(blife)), "paint_reset", bullet+TASK_PB_RESET);
			}
			else
				paint_reset(bullet+TASK_PB_RESET);
	
			return FMRES_HANDLED; 
		}else
		{	//the pellet bounces, reduce its speed
			if (get_pcvar_num(gravity)){
				new Float:vel[3];
				pev(bullet,pev_velocity,vel);
				
				vel[0] /= 10.0;
				vel[1] /= 10.0;
				vel[2] /= 10.0;
				set_pev(bullet, pev_velocity, vel);
			}
			//A bullet that bounces becomes harmless to players
			set_pev(bullet, pev_classname, "pbHarmlessBullet");
			return FMRES_IGNORED;
		}
	/*else
	{
		paint_reset(bullet+TASK_PB_RESET);
		return FMRES_HANDLED;
	}
	*/	

	return FMRES_IGNORED;
}

//for the super launcher
public fw_think(ent)
{
	new model[35];
	pev(ent, pev_model, model, 34);

	if (!equali(model, "models/w_pb_supernade.mdl"))
		return FMRES_IGNORED;

	if(!is_user_alive(pev(ent, pev_owner)))
		return FMRES_IGNORED;
		
	set_task(1.6, "act_superboom", ent);
		
	return FMRES_SUPERCEDE;
}

public act_superboom(ent)
{
	for (new a = 0; a < 10; a++)
		set_task(0.1 + a*0.2, "launch_nade", ent + TASK_NADE);	
	
	set_task(2.5, "remove_nade", ent + TASK_NADE);	
		
}

public remove_nade(ent)
{
	remove_task (ent)
	ent -= TASK_NADE
	act_explode(ent);
	engfunc(EngFunc_RemoveEntity, ent);
	
}

public launch_nade(param)
{
	new spawnent = param - TASK_NADE
	
	new a, ent;
	while (a++ < g_pbcount - 1 && !ent)
		if (g_pbstatus[a] == 0)
			ent = g_pbstatus[a] = g_paintballs[a];
	if (!ent)
		while (a-- > 1 && !ent)
			if (g_pbstatus[a] == 2)
				ent = g_pbstatus[a] = g_paintballs[a];

	if (pev_valid(ent)&&pev_valid(spawnent))
	{
		new Float:nvelocity[3], vorigin[3];
		set_pev(ent, pev_owner, pev(spawnent, pev_owner));
		
		//create a nade
		set_pev(ent, pev_classname, "pbExplosive");
		engfunc(EngFunc_SetModel, ent, "models/w_pb_launch_he.mdl");
		engfunc(EngFunc_SetSize, ent, Float:{-3.0, -3.0, -3.0}, Float:{3.0, 3.0, 3.0});

		set_pev(ent, pev_gravity, 1.0);	

		pev(spawnent,pev_origin, vorigin)
		vorigin[2] += 30
		set_pev(ent, pev_origin, vorigin)
		
		//IVecFVec(vorigin, voriginf);
		//engfunc(EngFunc_SetOrigin, ent, vorigin);

		set_pev(ent, pev_movetype, MOVETYPE_BOUNCE);
		
		set_pev(ent, pev_solid, 2);
			
		nvelocity[0] = random_float(-200.0, 200.0);
		nvelocity[1] = random_float(-200.0, 200.0);
		nvelocity[2] = 500.0;	

		set_pev(ent, pev_velocity, nvelocity);
		set_pev(ent, pev_effects, pev(ent, pev_effects) & ~EF_NODRAW);
		
		set_task(2.0, "time_explode", ent+TASK_PB_RESET);
	}

	return ent;
}


public act_explode(ent)
{
	new origin[3], Float:forigin[3], colors[4], owner = pev(ent, pev_owner), user_team = get_user_team(owner);

	colors = (user_team == 1) ? { 255, 0, 247, 70} : { 0, 255, 208, 30};
	pev(ent, pev_origin, forigin);
	FVecIVec(forigin, origin);

	new id, Float:distance = 150.0, Float:porigin[3];
	
	new MaxPlayers = get_maxplayers();

	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_BLOODSPRITE);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2] + 20);
	write_short(blood2);
	write_short(blood1);
	write_byte(colors[2]);
	write_byte(30);
	message_end();
	
	while (id++ < MaxPlayers)
		if (is_user_alive(id))
		{
			pev(id, pev_origin, porigin);
			if (get_distance_f(forigin, porigin) <= distance)
				if (fm_is_visible(ent, id))
					ExecuteHam(Ham_TakeDamage, id, owner, owner, (user_team != get_user_team(id)) ? 100.0 : 300.0, 4098);
		}

	emit_sound(ent, CHAN_AUTO, "weapons/sg_explode.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

	set_pev(ent, pev_classname, "pbHarmlessBullet");

}

stock bool:fm_is_visible(ent, target)
{
	if (pev_valid(ent) && pev_valid(target))
	{
		new Float:start[3], Float:view_ofs[3], Float:point[3];
		pev(ent, pev_origin, start);
		pev(ent, pev_view_ofs, view_ofs);
		pev(target, pev_origin, point);
		start[0] += view_ofs[0];
		start[1] += view_ofs[1];
		start[2] += view_ofs[2];
		engfunc(EngFunc_TraceLine, start, point, 1, ent, 0);
		new Float:fraction;
		get_tr2(0, TR_flFraction, fraction);
		if (fraction == 1.0)
			return true;
	}
	return false;
}

public paint_splat(ent)
{
	new Float:origin[3], Float:norigin[3], Float:viewofs[3], Float:angles[3], Float:normal[3], Float:aiming[3];
	pev(ent, pev_origin, origin);
	pev(ent, pev_view_ofs, viewofs);
	pev(ent, pev_v_angle, angles);

	norigin[0] = origin[0] + viewofs[0];
	norigin[1] = origin[1] + viewofs[1];
	norigin[2] = origin[2] + viewofs[2];
	aiming[0] = norigin[0] + floatcos(angles[1], degrees) * 1000.0;
	aiming[1] = norigin[1] + floatsin(angles[1], degrees) * 1000.0;
	aiming[2] = norigin[2] + floatsin(-angles[0], degrees) * 1000.0;
	
	new tr = 0
	engfunc(EngFunc_TraceLine, norigin, aiming, 0, ent, tr);
	//get_tr2(tr, TR_vecPlaneNormal, normal);
	//EngFunc_TraceHull,(const float *v1, const float *v2, int fNoMonsters, int hullNumber, edict_t *pentToSkip, TraceResult *ptr);
	//engfunc(EngFunc_TraceHull, norigin, aiming, 0, HULL_LARGE, ent, tr)
	get_tr2(tr, TR_vecPlaneNormal, normal)

	
	vector_to_angle(normal, angles);
	angles[1] += 180.0;
	if (angles[1] >= 360.0) angles[1] -= 360.0;	
	
	set_pev(ent, pev_angles, angles);
	set_pev(ent, pev_v_angle, angles);

	origin[0] += (normal[0] * random_float(0.3, 2.7));
	origin[1] += (normal[1] * random_float(0.3, 2.7));
	origin[2] += (normal[2] * random_float(0.3, 2.7));
	engfunc(EngFunc_SetOrigin, ent, origin);
	set_pev(ent, pev_frame, float(random_num( (pev(ent, pev_skin) * 18), (pev(ent, pev_skin) * 18) + 17 ) ));

	if (pev(ent, pev_renderfx) != kRenderFxNone)
		set_rendering(ent);	

}

public paint_glow(ent)
{
	if (pev_valid(ent))
	{
		static pbglow[5], clr[3];
		get_pcvar_string(bglow, pbglow, 4);
		switch (get_pcvar_num(color))
		{
			case 2: clr = (get_user_team(pev(ent, pev_owner))==1) ? {255, 0, 0} : {0, 0, 255};
			default: clr = {255, 255, 255};
		}
		if (read_flags(pbglow) & (1 << 0))
			set_rendering(ent, kRenderFxGlowShell, clr[0], clr[1], clr[2], kRenderNormal, 255);
		if (read_flags(pbglow) & (1 << 1))
		{
			message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
			write_byte(TE_BEAMFOLLOW);
			write_short(ent);
			write_short(beamspr);
			write_byte(4);
			write_byte(2);
			write_byte(clr[0]);
			write_byte(clr[1]);
			write_byte(clr[2]);
			write_byte(255);
			message_end();
		}
	}
}

public paint_reset(ent)
{
	remove_task(ent);
	ent -= TASK_PB_RESET;
	new a, findpb = 1;
	while (a++ <= g_pbcount && findpb)
		if (g_paintballs[a] == ent)
			findpb = g_pbstatus[a] = 0;

	set_pev(ent, pev_effects, pev(ent, pev_effects) | EF_NODRAW);
	engfunc(EngFunc_SetSize, ent, Float:{0.0, 0.0, 0.0}, Float:{0.0, 0.0, 0.0});
	set_pev(ent, pev_velocity, Float:{0.0, 0.0, 0.0});
	engfunc(EngFunc_SetOrigin, ent, Float:{-2000.0, -2000.0, -2000.0});
	if (pev(ent, pev_renderfx) != kRenderFxNone)
		set_rendering(ent);
}

public time_explode(ent)
{
	remove_task(ent);
	ent -= TASK_PB_RESET;
	if (!pev_valid(ent))
		return;
	act_explode(ent);

	set_pev(ent, pev_velocity, Float:{0.0, 0.0, 0.0});
	set_pev(ent, pev_classname, "pbPaint");
	set_pev(ent, pev_solid, 0);
	set_pev(ent, pev_movetype, 0);
	engfunc(EngFunc_SetModel, ent, "sprites/paintball.spr");	
	
	new a, findpb = 0;
	while (a++ < g_pbcount && !findpb)
		if (g_paintballs[a] == ent)
			findpb = g_pbstatus[a] = 2;
	remove_task(ent);
	paint_reset(ent+TASK_PB_RESET);
}


public ev_roundstart()
{
	for (new a = 1; a <= g_pbcount; a++)
		if (g_pbstatus[a] != 0)
			paint_reset(g_paintballs[a]+TASK_PB_RESET);
	if (freezetime)
		freezetime = 0;

}

public ev_freezetime()
	freezetime = 1;

stock user_has_pbgun(id)
{
	new retval;
	
	if (is_user_alive(id))
	{
		switch(get_user_weapon(id))
		{ 
		case CSW_MP5NAVY: 	retval = PB_MARKER;	
		case CSW_USP: 		retval = PB_USP;
		case CSW_GLOCK18: 	retval = PB_GLOCK;
		case CSW_SCOUT: 	retval = PB_SNIPER;
		case CSW_M3: 		retval = PB_LAUNCHER;
		case CSW_XM1014: 	retval = PB_SHOTGUN;
		case CSW_P90: 		retval = PB_SUPERMARKER;
		default: 		retval = 0;
		} 
			
	}
	return retval;
}


stock set_user_clip(id, ammo)
{
	new weaponname[32], weaponid = -1, weapon = get_user_weapon(id, _, _);
	get_weaponname(weapon, weaponname, 31);
	while ((weaponid = engfunc(EngFunc_FindEntityByString, weaponid, "classname", weaponname)) != 0)
		if (pev(weaponid, pev_owner) == id) {
			set_pdata_int(weaponid, 51, ammo, 4);
			return weaponid;
		}
	return 0;
}

// teame06's function
stock set_rendering(index, fx=kRenderFxNone, r=0, g=0, b=0, render=kRenderNormal, amount=16)
{
	set_pev(index, pev_renderfx, fx);
	new Float:RenderColor[3];
	RenderColor[0] = float(r);
	RenderColor[1] = float(g);
	RenderColor[2] = float(b);
	set_pev(index, pev_rendercolor, RenderColor);
	set_pev(index, pev_rendermode, render);
	set_pev(index, pev_renderamt, float(amount));
}

     
public fwd_TakeDamage ( id, i_Inflictor, i_Attacker, Float:f_Damage, i_DamageBits )
    {
	if (!is_user_alive(i_Attacker))
		return PLUGIN_CONTINUE;
    
	if ( get_user_weapon(i_Attacker) == CSW_KNIFE && get_pcvar_num(knife_ohk))
        {
            ExecuteHam(Ham_TakeDamage, id, i_Attacker, i_Attacker, 101.0, 4098);
        }

	return PLUGIN_CONTINUE;
    }

public weapon_jam(id, uc_handle)
{
	if (get_pcvar_num(jamon))
	{
		// show messages	
		set_hudmessage(255, 255, 255, -1.0, 0.33, 0, 6.0, 12.0)
		show_hudmessage(id, "Weapon Jammed - Press Use To Clear")
	
		// block the attack button 
		static buttons; buttons = get_uc( uc_handle, UC_Buttons )
		set_uc (uc_handle, UC_Buttons, buttons & ~IN_ATTACK);
	}
}

public clear_chamber(id)
{
	if (get_pcvar_num(jamon))
	{
		g_Jammed[id] = false;
		emit_sound(id,CHAN_AUTO,"weapons/scout_bolt.wav",1.0,ATTN_NORM,0,PITCH_NORM);
		
		// show messages 
		set_hudmessage(255, 255, 255, -1.0, 0.3, 0, 3.0, 12.0);
		show_hudmessage(id, "Chamber Clear");
	}
}


public make_solid(ent)
{
	remove_task(ent);
	ent -= TASK_PB_RESET;
	if (!pev_valid(ent))
		return;
	set_pev(ent, pev_solid, 2);
	if (get_pcvar_num(gravity))
		set_pev(ent, pev_movetype, MOVETYPE_BOUNCE);
}

stock is_rpaintball_active()
{
	if(!cvar_exists("amx_pbmod"))
	{
		log_amx("Cvar: ^"amx_pbmod^" does not exist.")
		return 0
	}
	return get_cvar_num("amx_pbmod")
}
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang11274\\ f0\\ fs16 \n\\ par }
*/

#include <amxmodx>
#include <fakemeta>
#include <hamsandwich>

#define PLUGIN "R Paintball Nade"
#define VERSION "1.5"
#define AUTHOR "Rul4 - WhooKid"

new pbnade, pbsnade, radius, MaxPlayers, blood1, blood2, smoke[6];

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);
	
	set_cvar_string("rpaintballnade", VERSION)

	if (get_pcvar_num(pbnade)||get_pcvar_num(pbsnade))
	{
		register_event("CurWeapon", "ev_curweapon", "be");
		register_forward(FM_SetModel, "fw_setmodel");
		register_forward(FM_Think, "fw_think");

		radius = register_cvar("pbnade_radius", "150");
		
		MaxPlayers = get_maxplayers();
	}
}

public plugin_precache()
{
	pbnade = register_cvar("amx_pbnade", "1");
	pbsnade = register_cvar("amx_pbsnade", "1");
	register_cvar("rpaintballnade", VERSION, FCVAR_SERVER|FCVAR_UNLOGGED);
	
	if (get_pcvar_num(pbnade))
	{
		precache_model("models/p_pbnade.mdl");
		precache_model("models/v_pbnade.mdl");
		precache_model("models/w_pbnade.mdl");
		blood1 = precache_model("sprites/blood.spr");
		blood2 = precache_model("sprites/bloodspray.spr");
	}
	
	if (get_pcvar_num(pbsnade))
	{
		precache_model("models/v_pbsnade.mdl");
		precache_model("models/w_pbsnade.mdl");
		smoke[0] = precache_model("sprites/gas_puff_01b.spr");
		smoke[1] = precache_model("sprites/gas_puff_01r.spr");
		smoke[2] = precache_model("sprites/gas_puff_01g.spr");
		smoke[3] = precache_model("sprites/gas_puff_01y.spr");
		smoke[4] = precache_model("sprites/gas_puff_01m.spr");
		smoke[5] = precache_model("sprites/gas_puff_01o.spr");
	}
}

public ev_curweapon(id)
{
	new model[35];
	pev(id, pev_viewmodel2, model, 34);
	if (equali(model, "models/v_hegrenade.mdl") && (get_pcvar_num(pbnade)))
	{
		set_pev(id, pev_viewmodel2, "models/v_pbnade.mdl");
		set_pev(id, pev_weaponmodel2, "models/p_pbnade.mdl");
	}
	
	if (equali(model, "models/v_smokegrenade.mdl") && (get_pcvar_num(pbsnade)))
	{
		set_pev(id, pev_viewmodel2, "models/v_pbsnade.mdl");
		//set_pev(id, pev_weaponmodel2, "models/p_pbnade.mdl");
	}
}

public fw_setmodel(ent, model[]) 
{
	if (equali(model, "models/w_hegrenade.mdl") && (get_pcvar_num(pbnade))) 
	{
		engfunc(EngFunc_SetModel, ent, "models/w_pbnade.mdl");
		return FMRES_SUPERCEDE;
	}
	
	if (equali(model, "models/w_smokegrenade.mdl") && (get_pcvar_num(pbsnade))) 
	{
		engfunc(EngFunc_SetModel, ent, "models/w_pbsnade.mdl");
		return FMRES_SUPERCEDE;
	}

	return FMRES_IGNORED;
}

public fw_think(ent)
{
	new model[25];
	pev(ent, pev_model, model, 24);

	if (!equali(model, "models/w_pbnade.mdl") && !equali(model, "models/w_pbsnade.mdl"))
		return FMRES_IGNORED;

	if (equali(model, "models/w_pbnade.mdl"))
		set_task(1.6, "act_explode", ent);
	else
		set_task(2.0, "act_smoke", ent);
	
	return FMRES_SUPERCEDE;
}

public act_explode(ent)
{
	if (!pev_valid(ent))
		return;

	new origin[3], Float:forigin[3], colors[4], owner = pev(ent, pev_owner), user_team = get_user_team(owner);

	colors = (user_team == 1) ? { 255, 0, 247, 70} : { 0, 255, 208, 30};
	pev(ent, pev_origin, forigin);
	FVecIVec(forigin, origin);

	new id, Float:distance = float(get_pcvar_num(radius)), Float:porigin[3];

	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_BLOODSPRITE);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2] + 20);
	write_short(blood2);
	write_short(blood1);
	write_byte(colors[2]);
	write_byte(30);
	message_end();
	
	while (id++ < MaxPlayers)
		if (is_user_alive(id))
		{
			if (user_team != get_user_team(id) || owner == id)
			{
				pev(id, pev_origin, porigin);
				if (get_distance_f(forigin, porigin) <= distance)
					if (fm_is_visible(ent, id))
						ExecuteHam(Ham_TakeDamage, id, ent, owner, (id != owner) ? 100.0 : 300.0, 0);
			}
		}
		
	emit_sound(ent, CHAN_AUTO, "weapons/sg_explode.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

	engfunc(EngFunc_RemoveEntity, ent);
}

public act_smoke(ent)
{

	emit_sound(ent, CHAN_AUTO, "weapons/sg_explode.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
	act_fumes(ent)
	
}
public act_fumes(ent)
{
	if (!pev_valid(ent))
		return;
		
	new origin[3], Float:forigin[3];

	pev(ent, pev_origin, forigin);
	FVecIVec(forigin, origin);
	
//#define TE_FIREFIELD			123		// makes a field of fire.
// coord (origin)
// short (radius) (fire is made in a square around origin. -radius, -radius to radius, radius)
// short (modelindex)
// byte (count)
// byte (flags)
// byte (duration (in seconds) * 10) (will be randomized a bit)

	new select = random(6)

	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_FIREFIELD);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2] + 50);
	write_short(100)
	write_short(smoke[select]);
	write_byte(100)		
	write_byte(TEFIRE_FLAG_ALPHA)//(TEFIRE_FLAG_SOMEFLOAT | TEFIRE_FLAG_ALPHA)		
	write_byte(1000)		
	message_end();

	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_FIREFIELD);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2] + 50);
	write_short(150)
	write_short(smoke[select]);
	write_byte(10)		
	write_byte(TEFIRE_FLAG_ALLFLOAT | TEFIRE_FLAG_ALPHA )		
	write_byte(1000)		
	message_end();

	engfunc(EngFunc_RemoveEntity, ent);
}

stock bool:fm_is_visible(ent, target)
{
	if (pev_valid(ent) && pev_valid(target))
	{
		new Float:start[3], Float:view_ofs[3], Float:point[3];
		pev(ent, pev_origin, start);
		pev(ent, pev_view_ofs, view_ofs);
		pev(target, pev_origin, point);
		start[0] += view_ofs[0];
		start[1] += view_ofs[1];
		start[2] += view_ofs[2];
		engfunc(EngFunc_TraceLine, start, point, 1, ent, 0);
		new Float:fraction;
		get_tr2(0, TR_flFraction, fraction);
		if (fraction == 1.0)
			return true;
	}
	return false;
}

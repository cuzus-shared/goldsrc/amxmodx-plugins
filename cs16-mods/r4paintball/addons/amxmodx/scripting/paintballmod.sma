#include <amxmodx>
#include <amxmisc>
#include <fakemeta>
#include <cstrike>

#define PLUGIN "R Paintball Mod"
#define VERSION "5.8b"
#define AUTHOR "Rul4 - WhooKid"

#define TASK_DELAY 0.1 // The delay between model changing tasks
#define MODELSET_TASK 3000 // offset for the models task
#define MENU_TASK 3500

new onoff, cmodel, money, strip, death, protc, pbgun, pbnade, pbsnade, randomgun, maxplayers, vendetta, vendetta_cant;
new g_team_select[33], g_plyr_skin[33], g_has_kill[33];
new MenuState[33]
new ShowMenu[33]
new g_has_custom_model[33] // whether the player is using a custom model
new g_team[33]
new got_killed_by[33], vendetta_bonus[33], got_killed_times[33], in_vendetta[33]
new pbslauncher, got_slauncher;
new Float:g_models_counter // counter used to set task durations

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);
	register_cvar("rpaintballmod", VERSION, FCVAR_SERVER|FCVAR_UNLOGGED);
	set_cvar_string("rpaintballmod", VERSION)
	if (get_pcvar_num(onoff))
	{
		
		register_logevent("new_round", 2, "0=World triggered", "1=Round_Start");
		register_event("ResetHUD", "ev_resethud", "be");
		register_event("DeathMsg", "ev_death", "a")
		register_event("Money", "ev_money", "be");
		register_clcmd("say /respawn", "say_respawn", _, "<Respawns you if enabled>");

		register_clcmd("say pbguns", "enableMenu")
		register_clcmd("say /pbguns", "enableMenu")

		register_forward(FM_GetGameDescription, "fw_gamedesc");
		register_forward(FM_SetModel, "fw_setmodel", 1);
		if (get_pcvar_num(cmodel))
		{
			register_forward(FM_ClientUserInfoChanged, "fw_clientuserinfochanged");
			register_forward(FM_SetClientKeyValue, "fw_SetClientKeyValue")
			//register_message(get_user_msgid("ClCorpse"), "spawn_clcorpse")
			//set_msg_block(get_user_msgid("ClCorpse"), BLOCK_SET);
		}

		maxplayers = get_maxplayers();
		
		//initialize the menu
		for (new a = 0; a < 33; a++)
		{
			ShowMenu[a] = 1;
			in_vendetta[a] = 0
			got_killed_by[a] = -1
			got_killed_times[a] = 0
		}
		//load the config
		new file[64]
		get_configsdir(file, 63)
		format(file, 63, "%s/rpaintball.cfg", file)
		
		if(file_exists(file)) 
			server_cmd("exec %s", file)
	}

}

public plugin_precache()
{
	onoff = register_cvar("amx_pbmod", "1");
	pbgun = register_cvar("amx_pbgun", "1");
	pbnade = register_cvar("amx_pbnade", "1");
	pbsnade = register_cvar("amx_pbsnade", "1");
	if (get_pcvar_num(onoff)){
		cmodel = register_cvar("amx_pbmodel", "1");
		money = register_cvar("amx_pbmoney", "1");
		strip = register_cvar("amx_pbstrip", "1");
		death = register_cvar("amx_pbdm", "0");
		protc = register_cvar("amx_pbspawnprotect", "5");
		randomgun = register_cvar("pbgun_selectmode", "1");
		vendetta = register_cvar("pbgun_vendetta_enabled", "1");
		vendetta_cant = register_cvar("pbgun_vendetta_cant", "3");
		pbslauncher = register_cvar("pbgun_pbslauncher", "1");
	
		if (get_pcvar_num(cmodel))
			precache_model("models/player/paintballer/paintballer.mdl");
	}
}

public fw_gamedesc()
{
	if (get_pcvar_num(onoff))
	{
		forward_return(FMV_STRING, PLUGIN);
		return FMRES_SUPERCEDE;
	}
	return FMRES_IGNORED;
}

public new_round()
{
	if (get_pcvar_num(onoff) && get_pcvar_num(strip))
	{
		new ent;
		while ((ent = engfunc(EngFunc_FindEntityByString, ent, "classname", "armoury_entity")) != 0)
			engfunc(EngFunc_RemoveEntity, ent);
		
	}

	// Reset the models task counter			
	g_models_counter = 0.0
	
	select_new_slauncher()
	
}

public select_new_slauncher()
{
	
	new original = got_slauncher
	
	got_slauncher++
	
	while((!is_user_connected(got_slauncher) && !is_user_bot(got_slauncher) && got_slauncher != original))
	{
		got_slauncher++

		if (got_slauncher > 32)
			got_slauncher = 0
	}
	
	//client_print(0, print_chat, "%d got it", got_slauncher)
			
}

public ev_resethud(id)
{
	if (get_pcvar_num(onoff))
	{
		if (!task_exists(id))
			set_task(0.3, "player_spawn", id);		
	}
}
public player_spawn(id)
	if (is_user_alive(id))
	{
		if (get_pcvar_num(protc))
		{
			set_pev(id, pev_takedamage, DAMAGE_NO);
			set_task(float(get_pcvar_num(protc)), "player_godmodeoff", id+100);
		}

		if (get_pcvar_num(strip)) //&& !user_has_mp5(id))
		{
			if (pev(id, pev_weapons) & (1 << CSW_C4))
				engclient_cmd(id, "drop", "weapon_c4")
			fm_strip_user_weapons(id);
		}

		if (get_pcvar_num(money))
		{
			message_begin(MSG_ONE_UNRELIABLE, 94, _, id); 
			write_byte(1 << 5);
			message_end();
		}

		if (get_pcvar_num(cmodel))
		{
			set_task(0.1 + g_models_counter, "task_set_model", id+MODELSET_TASK)
			g_models_counter += TASK_DELAY
		}

		remove_task(id);
		set_task(random_float(0.9, 1.3), "player_weapons", id);
		set_task(2.0, "clear_moneyhud", id + 300);		
		
	}

public client_command(id)
	if (get_pcvar_num(cmodel))
	{		
		new command[10], speech[2];
		read_argv(0, command, 9);
		read_argv(1, speech, 1);
		if (containi(command, "join") != -1)
			if (equali(command, "jointeam"))
				g_team_select[id] = str_to_num(speech);
			else if (equali(command, "joinclass"))
				g_plyr_skin[id] = (g_team_select[id] == 1) ? str_to_num(speech) - 1: str_to_num(speech) + 3;
	}

public player_weapons(id)
	if (is_user_alive(id))
	{
	
		menu_cancel(id)
		
		if(in_vendetta[id])
		{
			new kname[32]; get_user_name(got_killed_by[id],kname,31);
			set_hudmessage(255, 255, 255, -1.0, 0.33, 0, 5.0, 5.0);
			show_hudmessage(id, "You took %s's shit for the last time... This is VENDETTA!!!!", kname);
		}

		
		set_pdata_int(id, 386, 120, 5);
		fm_give_item(id, "weapon_knife");
		if (get_user_team(id) == 1)
			fm_give_item(id, "weapon_glock18");
		else 
		{
			set_pdata_int(id, 382, 48, 5);
			fm_give_item(id, "weapon_usp");
		}
		if (get_pcvar_num(pbgun) && !vendetta_bonus[id]){

			if (get_pcvar_num(randomgun)==1){

				if(is_user_bot(id))
				{
					ShowMenu[id] = false;
					MenuState[id] = random(4);
				}
				
				if (ShowMenu[id])
					launchMenu(id);
				else
				{	
					give_menu_weapon(id, MenuState[id]);
					client_print(id, print_chat, "[RPaintballMod] Your equip menu has been disabled. Say 'pbguns' to re-enable it.")	
				}

			}
			else
			{
				new choose;
				if (get_pcvar_num(randomgun)==2){
					choose = random(4);
					
					if (random(20)==0){
						fm_give_item(id, "weapon_p90");
						cs_set_user_bpammo(id, CSW_P90, 300);
						set_hudmessage(255, 255, 255, -1.0, 0.33, 0, 5.0, 5.0);
						show_hudmessage(id, "Lucky you! You got a SUPER MARKER!!!!");
					}
					else
					switch (choose)
					{
						case 0: fm_give_item(id, "weapon_mp5navy");
						case 1: fm_give_item(id, "weapon_scout");
						case 2: fm_give_item(id, "weapon_m3");
						case 3: fm_give_item(id, "weapon_xm1014");
						default: fm_give_item(id, "weapon_mp5navy");
					}
					cs_set_user_bpammo(id, CSW_MP5NAVY, 150);
					cs_set_user_bpammo(id, CSW_SCOUT, 20);
					cs_set_user_bpammo(id, CSW_M3, 16);
					cs_set_user_bpammo(id, CSW_XM1014, 80);
					cs_set_user_bpammo(id, CSW_P90, 300);
				}else
				{
					//always gets a marker
					fm_give_item(id, "weapon_mp5navy");
					cs_set_user_bpammo(id, CSW_MP5NAVY, 150);
	
					//did not kill someone, has a chance of getting a shotgun
					choose = random(3);					
					if (!g_has_kill[id] && !choose){
							fm_give_item(id, "weapon_xm1014");
							cs_set_user_bpammo(id, CSW_XM1014, 80);
						}
						
					//killed 2 or more, gets a sniper or a launcher
					choose = random(2);					
					if (g_has_kill[id]>2)
						if (g_has_kill[id]>4){
							fm_give_item(id, "weapon_p90");
							cs_set_user_bpammo(id, CSW_P90, 300);
							set_hudmessage(255, 255, 255, -1.0, 0.33, 0, 5.0, 5.0);
							show_hudmessage(id, "Holy crap! You got a SUPER MARKER!!!!");
						}	
						else
							if (choose){
								fm_give_item(id, "weapon_scout");
								cs_set_user_bpammo(id, CSW_SCOUT, 20);
							}else
							{
								fm_give_item(id, "weapon_m3");
								cs_set_user_bpammo(id, CSW_M3, 16);
							}
					g_has_kill[id] = 0;
				}
			}	
		}
		
		if(vendetta_bonus[id] == 1)
		{
			fm_give_item(id, "weapon_p90");
			cs_set_user_bpammo(id, CSW_P90, 300);
			set_hudmessage(255, 255, 255, -1.0, 0.33, 0, 5.0, 5.0);
			show_hudmessage(id, "Vendetta! You got a SUPER MARKER!!!!");
		}

		if(vendetta_bonus[id] == -1)
		{
			vendetta_bonus[id] = 0
			set_hudmessage(255, 255, 255, -1.0, 0.33, 0, 5.0, 5.0);
			show_hudmessage(id, "Haha! You got punk'd and didn't get a main gun this round!!!!");
		}

		
		if (get_pcvar_num(pbnade))
			fm_give_item(id, "weapon_hegrenade");
	
		if (get_pcvar_num(pbsnade))
			fm_give_item(id, "weapon_smokegrenade");

		if (get_pcvar_num(pbslauncher) && id == got_slauncher)
		{
			new kname[32]; get_user_name(id,kname,31);
			new teamname[3][] = {"None", "Red Team", "Blue Team"};
			set_hudmessage(255, 255, 255, -1.0, 0.7, 0, 5.0, 5.0);
			show_hudmessage(0, "%s: %s got a super launcher, BEWARE!!!!", teamname[get_user_team(id)], kname);
			//client_print(0, print_chat, "%s: %s got a super launcher, BEWARE!!!!", teamname[get_user_team(id)], kname)
			fm_give_item(id, "weapon_flashbang");
			//fm_give_item(id, "weapon_flashbang");
			
			if (get_pcvar_num(death))
				select_new_slauncher()
	
		}
		
		remove_task(id);
	}

public clear_moneyhud(id)
	if (get_pcvar_num(money))
	{
		message_begin(MSG_ONE_UNRELIABLE, 94, _, id - 300); //HideWeapon
		write_byte(1 << 5);
		message_end();
	}
	
public ev_death()
{

	new killer = read_data(1);
	new victim = read_data(2);
	
	g_has_kill[killer] += 1;
	if (get_pcvar_num(death))
	{
		new id = victim + 200;
		set_task(3.0, "player_spawner", id);
		set_task(3.2, "player_spawner", id);
	}

	if (get_pcvar_num(vendetta) && killer != victim)
	{

		vendetta_bonus[victim] = 0
	
		if (got_killed_by[victim] == killer)
		{
			got_killed_times[victim] += 1;
		}
		else
		{
			got_killed_by[victim] = killer;
			got_killed_times[victim] = 1;
		}
	
		if (got_killed_times[victim] < get_pcvar_num(vendetta_cant))
			in_vendetta[victim] = 0;
		else
			in_vendetta[victim] = 1;

		if (got_killed_by[killer] == victim)
			if (in_vendetta[killer])
			{
				in_vendetta[killer] = 0
				vendetta_bonus[killer] = 1
				vendetta_bonus[victim] = -1
				got_killed_times[killer] = 0
				got_killed_by[killer] = -1
				
				new kname[32]; get_user_name(killer,kname,31);
				
				set_hudmessage(255, 255, 255, -1.0, 0.4, 0, 5.0, 5.0);
				show_hudmessage(0, "%s got VENDETTA!!!!", kname);
			}
			else
				got_killed_times[killer] = 0
			
	}

}

public ev_money(id)
	if (get_pcvar_num(money))
		if (get_pdata_int(id, 115, 5) > 0)
			set_pdata_int(id, 115, 0, 5);

public say_respawn(id)
	if (get_pcvar_num(death))
		if (!is_user_alive(id))
			if (get_user_team(id) == 1 || get_user_team(id) == 2)
			{
				set_task(1.5, "player_spawner", id + 200);
				set_task(1.7, "player_spawner", id + 200);
			}

public player_godmodeoff(id)
	set_pev(id-100, pev_takedamage, DAMAGE_AIM);

public player_spawner(id)
	if (is_user_connected(id - 200))
		if (get_user_team(id - 200) == 1 || get_user_team(id - 200) == 2)
			dllfunc(DLLFunc_Spawn, id - 200);

stock user_has_mp5(id)
{
	new weapons[32], num;
	get_user_weapons(id, weapons, num);
	for (new i = 0; i < num; i++)
		if (weapons[i] == CSW_MP5NAVY)
			return 1;
	return 0;
}

public fw_setmodel(ent, model[])
{
	if (get_pcvar_num(death) && pev_valid(ent))
	{
		new id = pev(ent, pev_owner);
		if ((!is_user_alive(id) || task_exists(id + 200)) && equali(model, "models/w_", 9) && !equali(model, "models/w_weaponbox.mdl"))
		{
			new classname[16];
			pev(ent, pev_classname, classname, 15);
			if (equal(classname, "weaponbox") && !equal(model, "models/w_backpack.mdl"))
				for (new i = maxplayers + 1; i < engfunc(EngFunc_NumberOfEntities) + 5; i++)
					if (pev_valid(i))
						if (ent == pev(i, pev_owner))
						{
							dllfunc(DLLFunc_Think, ent);
							return FMRES_IGNORED;
						}
		}
	}
	return FMRES_IGNORED;
}

public fw_clientuserinfochanged(id, infobuffer)
{
	
//	return (get_pcvar_num(cmodel) && pev(id, pev_deadflag) == DEAD_NO) ? FMRES_SUPERCEDE : FMRES_IGNORED;
		
	if (get_pcvar_num(cmodel) && (g_has_custom_model[id]))
	{

		if (g_team[id] != get_user_team(id))
			g_has_custom_model[id] = 0;
		
		// Get current model
		static currentmodel[32]
		
		engfunc(EngFunc_InfoKeyValue, engfunc(EngFunc_GetInfoKeyBuffer, id), "model", currentmodel, 31)
		
		// Check whether it matches the custom model - if not, set it again
		if (!equal(currentmodel, "paintballer") || g_has_custom_model[id] == 0)
		{
			set_task(0.1 + g_models_counter, "task_set_model", id+MODELSET_TASK)
			g_models_counter += TASK_DELAY
		}
		
		return FMRES_SUPERCEDE;
	}
	return FMRES_IGNORED
}

public fw_SetClientKeyValue(id, const infobuffer[], const key[])
{   
    // Block CS model changes
    if (g_has_custom_model[id] && equal(key, "model"))
        return FMRES_SUPERCEDE;
        
    return FMRES_IGNORED;
}	

////////*****************VEN STOCKS START*****************////////
stock fm_strip_user_weapons(index)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "player_weaponstrip"));
	if (!pev_valid(ent))
		return 0;
	dllfunc(DLLFunc_Spawn, ent);
	dllfunc(DLLFunc_Use, ent, index);
	engfunc(EngFunc_RemoveEntity, ent);
	return 1;
}

stock fm_give_item(index, const item[])
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, item));
	if (!pev_valid(ent))
		return 0;
	new Float:origin[3];
	pev(index, pev_origin, origin);
	engfunc(EngFunc_SetOrigin, ent, origin);
	set_pev(ent, pev_spawnflags, pev(ent, pev_spawnflags) | SF_NORESPAWN);
	dllfunc(DLLFunc_Spawn, ent);
	new save = pev(ent, pev_solid);
	dllfunc(DLLFunc_Touch, ent, index);
	if (pev(ent, pev_solid) != save)
		return ent;
	engfunc(EngFunc_RemoveEntity, ent);
	return -1;
}
////////*****************VEN STOCKS END*****************////////

public enableMenu(id)
{
	ShowMenu[id] = 1
	client_print(id, print_chat, "[PaintballMod] Your equip menu has been re-enabled.")
	return PLUGIN_HANDLED

}

public launchMenu(id)
 {
	new menu = menu_create("\rChoose your main Paintball Gun:", "menu_handler")
	
	menu_additem(menu, "\wMarker", "0", 0)
	menu_additem(menu, "\wShotgun", "1", 0)
	menu_additem(menu, "\wLauncher", "2", 0)
	menu_additem(menu, "\wSniper", "3", 0)
	menu_additem(menu, "Select previous gun", "4", 0, -1)
	menu_additem(menu, "Previous + Don't show menu again", "5", 0, -1)

	menu_setprop(menu, MPROP_EXIT, MEXIT_ALL)
	
	menu_display(id, menu, 0)
	
}

public menu_handler(id, menu, item)
 {

	if (item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}

	new data[6], iName[64]
	new access, callback
	menu_item_getinfo(menu, item, access, data,5, iName, 63, callback)

	new key = str_to_num(data);
	
	if (key==4)
		key = MenuState[id]

	if(key == 5)
	{
		ShowMenu[id] = 0;
		client_print(id, print_chat, "[RPaintballMod] Your equip menu has been disabled. Say 'pbguns' to re-enable it.")	
		key = MenuState[id]
	}
	
	MenuState[id] = key

	give_menu_weapon(id, MenuState[id]);
		
	menu_destroy(menu)
	return PLUGIN_HANDLED
 }

public give_menu_weapon(id,key)
{
	switch (key)
	{
		case 0: fm_give_item(id, "weapon_mp5navy");
		case 1: fm_give_item(id, "weapon_xm1014");
		case 2: fm_give_item(id, "weapon_m3");
		case 3: fm_give_item(id, "weapon_scout");
		default: fm_give_item(id, "weapon_mp5navy");
	}
	
	cs_set_user_bpammo(id, CSW_MP5NAVY, 150);
	cs_set_user_bpammo(id, CSW_SCOUT, 20);
	cs_set_user_bpammo(id, CSW_M3, 16);
	cs_set_user_bpammo(id, CSW_XM1014, 80);

}

public task_set_model(id)
{
    remove_task(id)
    
    // Get player id
    id -= MODELSET_TASK
    
    // Actually set the player's model
    set_user_model(id, "paintballer")
}

public set_user_model(player, const modelname[])
{
	// Set new model
	engfunc(EngFunc_SetClientKeyValue, player, engfunc(EngFunc_GetInfoKeyBuffer, player), "model", modelname)
	
	g_team[player] = get_user_team(player)

	//if (!g_has_custom_model[player])
	if (g_team[player] == 1)
	{
		g_plyr_skin[player] = 0//random(4);
	}
	else
	{
		g_plyr_skin[player] = 4 // + random(4);
	}

		
	set_pev(player, pev_skin, g_plyr_skin[player]);
	
	g_has_custom_model[player] = 1	
}

public spawn_clcorpse()
{
    // Get player id
    static id
    id = get_msg_arg_int(12)
    
    new entCorpse = read_data(2);
    set_pev(entCorpse, pev_skin, g_plyr_skin[id]);
    
    
    // Check if the player is using a custom player model
    if (false && g_has_custom_model[id])
    {
        //client_print(0, print_chat, "spawning corpse for %d", id);

	set_msg_arg_string(1, "")
	
	new entCorpse = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(entCorpse > 0)
	{
		new Float:modelOrigin[3], Float:modelAngle[3]
		pev(id, pev_origin, modelOrigin)
		pev(id, pev_angles, modelAngle)

		client_print(0, print_chat, "corpse coords %f %f %f", float(get_msg_arg_int(2)), float(get_msg_arg_int(3)), float(get_msg_arg_int(4)));		
		client_print(0, print_chat, "corpse coords %i %i %i", get_msg_arg_int(2), get_msg_arg_int(3), get_msg_arg_int(4));		
		//client_print(0, print_chat, "corpse string %f %f %f", float(get_msg_arg_string(2)), float(get_msg_arg_string(3)), float(get_msg_arg_string(4)));		
		client_print(0, print_chat, "player coords %f %f %f", modelOrigin[0], modelOrigin[1], modelOrigin[2]);		
//		client_print(0, print_chat, "player %f %f %f", modelAngle[0], modelAngle[1], modelAngle[2]);		
	
		modelAngle[0] += 90.0
		
		modelOrigin[2] -= 35.0
		
		set_pev(entCorpse, pev_owner, id)
		set_pev(entCorpse, pev_classname, "clcorpse")
		engfunc(EngFunc_SetSize, entCorpse, Float:{-6.0,-12.0,-6.0},Float:{6.0,12.0,6.0})
		engfunc(EngFunc_SetModel, entCorpse, "models/player/paintballer/paintballer.mdl");
		set_pev(entCorpse, pev_skin, g_plyr_skin[id]);
		set_pev(entCorpse, pev_solid, SOLID_NOT)
		set_pev(entCorpse, pev_movetype, MOVETYPE_TOSS)
		set_pev(entCorpse, pev_sequence, 103)
		set_pev(entCorpse, pev_origin, modelOrigin)
		set_pev(entCorpse, pev_angles, modelAngle)
		set_pev(entCorpse, pev_v_angle, pev(id, pev_v_angle))
		set_pev(entCorpse, pev_nextthink, 1.0)
		
		}

	return PLUGIN_HANDLED;
	}
    return PLUGIN_CONTINUE;
}

/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang11274\\ f0\\ fs16 \n\\ par }
*/

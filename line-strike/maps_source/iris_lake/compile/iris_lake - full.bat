// Full

@echo off
set mapname=iris_lake
set modname=cstrike
set hlpath=C:\Games\Half-Life

hlcsg %mapname% -nowadtextures
hlbsp %mapname%
hlvis %mapname% -full
hlrad_x64 %mapname% -extra

del %mapname%.b0
del %mapname%.b1
del %mapname%.b2
del %mapname%.b3
del %mapname%.hsz
del %mapname%.p0
del %mapname%.p1
del %mapname%.p2
del %mapname%.p3
del %mapname%.pln
del %mapname%.prt
del %mapname%.wa_

//del %mapname%.log

copy %mapname%.bsp %hlpath%\%modname%\maps
del %mapname%.bsp

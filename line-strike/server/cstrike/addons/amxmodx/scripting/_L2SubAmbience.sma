#include <amxmodx>

//#define TASK_AMBIENCE_MAKE_SOUND	65536

#define AMB_VOLUME_MIN	0.21
#define AMB_VOLUME_MAX	0.56

#define SOUND_CHANCE	75

// TODO : 
//	Ambience like L2, maps patterns + randomizer

new const S_AMBIENCE[][] =
{
	"skillsound/bind_will_shot.wav", 
	"skillsound/bodytomind_shot.wav", 
	"skillsound/death_whisper_shot.wav", 
	"skillsound/might_shot.wav", 
	"skillsound/wind_shackle_shot.wav",
	"ambsound/ct_bell_01.wav",
	//"ambsound/d_drone_02.wav",
	//"ambsound/ds_wind_01.wav",
	"ambsound/dv_pipe_01.wav",
	"ambsound/ed_chimes_05.wav",
	"ambsound/id_cicada_01.wav",
	"ambsound/id_cicada_02.wav",
	"ambsound/in_cricket_03.wav",
	"ambsound/ms_bell_01.wav",
	"ambsound/nz_pipe_02.wav",
	//"ambsound/st_horror_01.wav",
	"ambsound/an_bird_11.wav",
	"ambsound/an_crow_01.wav",
	"ambsound/an_crow_02.wav",
	"ambsound/id_cicada_04.wav",
	"ambsound/nz_cricket_02.wav",
	"ambsound/nz_cricket_03.wav",
	"ambsound/t_black_smith_loop_01.wav",
	"monsound/queencat_move_01.wav", 
	"monsound/queencat_move_03.wav", 
	"monsound/cutie_cat_wait.wav", 
	"monsound/cutie_cat_death.wav", 
	"monsound/cutie_cat_fall.wav", 
	"monsound/dwarf_ghost_dmg_3.wav", 
	"monsound/fox_Wait.wav", 
	"monsound/hatchling_atkwait.wav", 
	"monsound/pet_wolf_wait.wav", 
	"monsound/strider_Wait.wav", 
	"monsound/vamp_bat_wait.wav", 
	"monsound/vamp_bat_atkwait.wav", 
	"skillsound/soul_shot_cast.wav", 
	"skillsound/spirits_shot_cast.wav", 
	"skillsound/song_of_full.wav",
	"chrsound/NPC_old_cough_01.wav"
};

public plugin_precache()
{
	for(new i; i < sizeof S_AMBIENCE; i++){
		precache_sound(S_AMBIENCE[i]);
	}
}

public plugin_init()
{
	register_plugin("L2PE Ambience", "0.2", "Geekrainian @ cuzus.games");
	
	register_event("DeathMsg", "evDeathMsg", "a");
	
	set_task(60.0, "Ambience_init");
}

public evDeathMsg()
{
	if(random_num(1,25) == 15)
		set_task(random_float(2.0, 10.0), "generateSound");
}

public Ambience_init()
{
	generateSound();
	set_task(random_float(3.0, 45.0), "Ambience_init");
}

public generateSound()
{
	new Float:chance = random_float(0.0, (100.0 / SOUND_CHANCE))
	if(chance <= 1.0){
		emit_sound(0, CHAN_AUTO, S_AMBIENCE[random(sizeof S_AMBIENCE)], random_float(AMB_VOLUME_MIN, AMB_VOLUME_MAX), ATTN_NORM, 0, PITCH_NORM);
	}
}
#include <amxmodx>
#include <amxmisc>
#include <fakemeta_util>
#include <hamsandwich>

#pragma dynamic 32768

#define OFFSET_CSMONEY	115
#define OFFSET_TEAM	114
#define OFFSET_CSDEATHS    444
#define fm_get_user_team(%1)	get_pdata_int( %1, OFFSET_TEAM )
#define fm_set_user_team(%1,%2)	set_pdata_int( %1, OFFSET_TEAM, %2 )
#define fm_get_user_deaths(%1)	get_pdata_int( %1, OFFSET_CSDEATHS )
#define fm_set_user_deaths(%1,%2)	set_pdata_int( %1, OFFSET_CSDEATHS, %2 )
#define fm_get_user_money(%1)	get_pdata_int( %1, OFFSET_CSMONEY )

#define m_flNextAttack	83

#define SPRAY_IMPULSE		201
#define FLASHLIGHT_IMPULSE	100

new const Teams[4][] =
{
	"", // LOL NULL
	"TERRORIST",
	"CT",
	"SPECTATOR"
};

/*new const DeathSounds[][] =
{
	"player/death6.wav",
	"player/die1.wav",
	"player/die2.wav",
	"player/die3.wav"
};*/

new const RadioCommands[][] =
{
	"radio1",
	"radio2",
	"radio3",
	"coverme",
	"takepoint",
	"holdpos",
	"regroup",
	"followme",
	"takingfire",
	"go",
	"fallback",
	"sticktog",
	"getinpos",
	"stormfront",
	"report",
	"roger",
	"enemyspot",
	"needbackup",
	"sectorclear",
	"inposition",
	"reportingin",
	"getout",
	"negative",
	"enemydown"
};

new p_Activity;
new p_MaxPlayers;
new g_iBlocker[ 3 ];
new g_Msg_ScoreInfo;
new g_Msg_DeathMsg;
new g_Msg_Money;
new g_Msg_ScoreAttrib;

new HamHook:fw_Killed;
new HamHook:fw_TakeDamage;
new HamHook:fw_PreThink;
new fw_ClientKill;
new fw_CmdStart;


/*public plugin_precache()
{
	for(new i = 0 ; i < sizeof DeathSounds ; i++)
	{
		if(DeathSounds[i][0])
			engfunc(EngFunc_PrecacheSound, DeathSounds[i]);
	}
}*/

public plugin_init()
{
	register_plugin("Round End Blocker", "1.4", "Dores");
	//register_cvar("round_end_blocker_v", "1.4", FCVAR_SERVER|FCVAR_SPONLY);
	
	fw_ClientKill = register_forward(FM_ClientKill, "Forward_ClientKill");
	fw_CmdStart = register_forward(FM_CmdStart, "Forward_CmdStart");
	
	fw_Killed = RegisterHam(Ham_Killed, "player", "Forward_Death");
	fw_TakeDamage = RegisterHam(Ham_TakeDamage, "player", "Forward_TakeDamage");
	fw_PreThink = RegisterHam(Ham_Player_PreThink, "player", "Forward_PreThink");
	
	p_Activity = get_cvar_pointer("amx_show_activity");
	p_MaxPlayers = register_cvar("reb_max_players", "1");
	
	g_Msg_ScoreInfo = get_user_msgid("ScoreInfo");
	g_Msg_DeathMsg = get_user_msgid("DeathMsg");
	g_Msg_Money = get_user_msgid("Money");
	g_Msg_ScoreAttrib = get_user_msgid("ScoreAttrib");
	
	register_event("HLTV", "ev_newRound", "a", "1=0", "2=0");
	register_event("TeamInfo", "TeamInfo_NotSpectator_Event", "a", "2&TERRORIST", "2&CT");
	register_message(get_user_msgid("SayText"), "Dead_Chat");
	register_clcmd("jointeam", "Cmd_HookJoinTeam");
	register_clcmd("amx_reb", "Cmd_ToggleREB", ADMIN_KICK, "Toggles Round End Blocker on or off");
	
	for (new i = 0 ; i < sizeof (RadioCommands) ; i++)
	{
		register_clcmd(RadioCommands[i], "Block_Radio");
	}
	
	Cmd_ToggleREB(0, 0, 0);
}

/* Thanks ConnorMcLeod for the idea below! */
EnableDisableForwards(bool:bStatus = false)
{
	if(bStatus)
	{
		EnableHamForward(fw_Killed);
		EnableHamForward(fw_TakeDamage);
		EnableHamForward(fw_PreThink);
		
		if(!fw_ClientKill)
		{
			fw_ClientKill = register_forward(FM_ClientKill, "Forward_ClientKill");
		}
		
		if(!fw_CmdStart)
		{
			fw_CmdStart = register_forward(FM_CmdStart, "Forward_CmdStart");
		}
		
		return;
	}
	
	DisableHamForward(fw_Killed);
	DisableHamForward(fw_TakeDamage);
	DisableHamForward(fw_PreThink);
	
	if(fw_ClientKill)
	{
		unregister_forward(FM_ClientKill, fw_ClientKill);
		fw_ClientKill = 0;
	}
	
	if(fw_CmdStart)
	{
		unregister_forward(FM_CmdStart, fw_CmdStart);
		fw_CmdStart = 0;
	}
	
	return;
}
/* Thanks ConnorMcLeod for the idea above! */

getRandomBlocker(team)
{	
	new iPlayers[32], pnum, rand;
	get_players(iPlayers, pnum, "be", Teams[team]);
	
	if(!pnum)
	{
		return 0;
	}
	
	do
	{
		rand = iPlayers[random(pnum)];
	}
	while(g_iBlocker[team] == rand);
	
	return rand;
}

getNotSpectatorPlayersNum(iNum[3])
{
	new iPlayers[2][32], pnum[2];
	get_players(iPlayers[0], pnum[0], "e", Teams[1]);
	get_players(iPlayers[1], pnum[1], "e", Teams[2]);
	
	iNum[0] = pnum[0] + pnum[1];
	iNum[1] = pnum[0];
	iNum[2] = pnum[1];
}

resetBlocker( iFlag = 0 )
{
	new blocker;
	for(new i = 1 ; i < 3 ; i++)
	{
		if((blocker = g_iBlocker[i]))
		{
			if(iFlag)
			{
				user_silentkill(blocker);
				fm_set_user_deaths(blocker, fm_get_user_deaths(blocker) - 1);
			}
			
			g_iBlocker[i] = 0;
		}
	}
	
	return;
}

fm_set_user_money( index, amount )
{
	set_pdata_int( index, OFFSET_CSMONEY, amount );
	
	message_begin( MSG_ONE_UNRELIABLE, g_Msg_Money, _, index );
	write_long( amount );
	write_byte( 1 ); // Always flash.
	message_end();
}

/* The below was taken from Virus Nade by anakin_cstrike due to lazyness(edited by me). */
kill_message( killer, victim, vteam )
{
	new kteam = fm_get_user_team( killer );
	if( killer != victim )
	{
		new kmoney = fm_get_user_money( killer );
		fm_set_user_money( killer, kmoney + 300 );
	}
	new kfrags = get_user_frags( killer );
	( killer == victim ) ? fm_set_user_frags( killer, kfrags - 1 ) : fm_set_user_frags( killer, kfrags + 1 );
	new vfrags = get_user_frags( victim );
	new kdeaths = fm_get_user_deaths( killer );
	new vdeaths = fm_get_user_deaths( victim );
	fm_set_user_deaths( victim, vdeaths + 1 );
	
	message_begin( MSG_ALL, g_Msg_ScoreInfo, _, 0 );
	write_byte( killer );
	( killer == victim ) ? write_short( kfrags - 1 ) : write_short( kfrags + 1 );
	( killer == victim ) ? write_short( kdeaths + 1 ) : write_short( kdeaths );
	write_short( 0 );
	write_short( kteam );
	message_end();
	
	if( killer != victim )
	{
		message_begin (MSG_ALL, g_Msg_ScoreInfo, _, 0 );
		write_byte( victim );
		write_short( vfrags );
		write_short( vdeaths + 1 );
		write_short( 0 );
		write_short( vteam );
		message_end();
	}
	
	message_begin( MSG_ALL, g_Msg_DeathMsg, _, 0 );
	write_byte( killer );
	write_byte( victim );
	write_byte( 0 );
	( killer == victim ) ? write_string( "Suicide" ) : write_string( "A Gun" );
	message_end();
	
	//emit_sound( 0, CHAN_AUTO, DeathSounds[ random( charsmax( DeathSounds ) ) ], VOL_NORM, ATTN_NORM, 0, PITCH_NORM );
	
	log_kill( killer, victim, ( killer == victim ) ? "Suicide" : "A Gun" );
}

log_kill( killer, victim, weapond[] )
{
	new Buffer[ 256 ];
	new kname[ 32 ], vname[ 32 ];
	new kteam[ 16 ], vteam[ 16 ];
	new kauth[ 32 ], vauth[ 32 ];
	new kid, vid;
	
	get_user_name( killer, kname, 31 );
	get_user_team( killer, kteam,15 );
	get_user_authid( killer, kauth,31 );
	kid = get_user_userid( killer );
	
	get_user_name( victim, vname, 31 );
	get_user_team( victim, vteam, 15 );
	get_user_authid( victim, vauth, 31 );
	vid = get_user_userid( victim );
	
	( killer == victim ) ?
		format( Buffer, charsmax( Buffer ), "^"%s<%d><%s><%s>^" committed suicide with ^"%s^"", vname, vid, vteam, vauth, weapond )
	:
		format( Buffer, charsmax( Buffer ), "^"%s<%d><%s><%s>^" killed ^"%s<%d><%s><%s>^" with ^"%s^"", kname, kid, kteam, kauth, vname, vid, vteam, vauth, weapond );
	
	log_message( "%s", Buffer );
}
/* The above was taken from Virus Nade by anakin_cstrike due to lazyness(edited by me). */

public client_disconnect( id )
{
	if( ( 0 < fm_get_user_team( id ) < 3 ) )
	{
		if(!fw_ClientKill)
		{
			new iNum[3];
			
			getNotSpectatorPlayersNum( iNum );
			
			if(iNum[0] - 1 < get_pcvar_num(p_MaxPlayers))
			{
				EnableDisableForwards(true);
			}
			
			return PLUGIN_CONTINUE;
		}
		
		new blocker;
		for(new i = 1 ; i < 3 ; i++)
		{
			if(g_iBlocker[i] == id)
			{			
				blocker = g_iBlocker[i] = getRandomBlocker(i);
				if(blocker)
				{
					setBlocker(blocker);
				}
				break;
			}
		}
	}
	
	return PLUGIN_CONTINUE;
}

public TeamInfo_NotSpectator_Event()
{
	if(fw_ClientKill)
	{
		set_task(0.3, "JoinTeamDelay");
	}
}

public JoinTeamDelay()
{
	new iNum[3];
	
	getNotSpectatorPlayersNum(iNum);
	
	if(iNum[0] >= get_pcvar_num(p_MaxPlayers))
	{
		resetBlocker(1);
		client_print(0, print_chat, "[REB] Too many players to block round ending! Killing blockers(if any)");
		EnableDisableForwards(false);
	}
	
	return PLUGIN_CONTINUE;
}

public Forward_Death( iVic, iAtt, shouldgib )
{
	static iMaxPlayers;
	if(!iMaxPlayers)
	{
		iMaxPlayers = get_maxplayers();
	}
	
	if(!(1 <= iVic <= iMaxPlayers) || !(1 <= iAtt <= iMaxPlayers))
	{
		return HAM_IGNORED;
	}
	
	static team;
	team = fm_get_user_team( iVic );
	
	if( g_iBlocker[ team ] )
		return HAM_IGNORED;
	
	g_iBlocker[ team ] = iVic;
	
	static iNum[ 3 ];
	getNotSpectatorPlayersNum( iNum );
	
	if( iNum[ 0 ] == 3 && iNum[ team ] == 1 )
	{
		set_task( 0.2, "setBlocker", iVic );
		kill_message( iAtt, iVic, team );
		
		//new rand = random( charsmax( DeathSounds ) );
		//emit_sound( 0, CHAN_AUTO, DeathSounds[ rand ], VOL_NORM, ATTN_NORM, 0, PITCH_NORM );
		return HAM_SUPERCEDE;
	}
	
	set_task( 0.2, "setBlocker", iVic );
	
	return HAM_IGNORED;
}

public setBlocker( id )
{	
	ExecuteHamB( Ham_CS_RoundRespawn, id );
	set_pev( id, pev_effects, pev( id, pev_effects ) | EF_NODRAW );
	fm_set_user_rendering( id, kRenderFxNone, 0, 0, 0, kRenderTransAlpha, 50 );
	fm_set_user_noclip( id, 1 );
	fm_strip_user_weapons( id );
	set_pev( id, pev_solid, SOLID_NOT );
	
	message_begin(MSG_ALL, g_Msg_ScoreAttrib, _, 0);
	write_byte(id);
	write_byte(1);
	message_end();
	
	new name[ 32 ];
	get_user_name( id, name, charsmax( name ) );
	new team = fm_get_user_team( id );
	client_print( 0, print_center, "[REB]: %s is the round end blocker for team %s!",name, Teams[ team ] );
	return 1;
}

public ev_newRound()
{
	resetBlocker();
}

public Forward_ClientKill( id )
{
	if (g_iBlocker[1] == id || g_iBlocker[2] == id)
	{
		return FMRES_SUPERCEDE;
	}
	
	else
	{
		new team = fm_get_user_team(id);
		if (!g_iBlocker[team])
		{
			set_task(1.0, "setBlocker", id);
			g_iBlocker[team] = id;
			kill_message(id, id, team);
			//emit_sound(0, CHAN_AUTO, DeathSounds[random(sizeof(DeathSounds))], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
			return FMRES_SUPERCEDE;
		}
	}
	
	return FMRES_IGNORED;
}

public Forward_PreThink(id)
{
	if(g_iBlocker[1] == id || g_iBlocker[2] == id)
	{
		set_pdata_float(id, m_flNextAttack, 1.0);
	}
	
	return HAM_IGNORED;
}

public Forward_CmdStart(id, uc_handle, seed)
{
	if(g_iBlocker[1] == id || g_iBlocker[2] == id)
	{
		new Impulse = get_uc(uc_handle, UC_Impulse);
		
		if(Impulse == FLASHLIGHT_IMPULSE || Impulse == SPRAY_IMPULSE)
		{
			set_uc(uc_handle, UC_Impulse, 0);
		}
	}
	
	return FMRES_IGNORED;
}

public Cmd_ToggleREB(id, lvl, cid)
{
	if(id && !cmd_access(id, lvl, cid, 1))
	{
		return PLUGIN_HANDLED;
	}
	
	static bool:g_bPlugOn;
	
	EnableDisableForwards((g_bPlugOn = !g_bPlugOn));
	if(!g_bPlugOn)
	{
		resetBlocker(1);
	}
	
	new AdName[36];
	get_user_name(id, AdName, 31);
	format(AdName, 35, "[%s]", AdName);
	
	new activity = get_pcvar_num(p_Activity);
	if(activity)
	{
		client_print(0, print_chat, "Admin%s have %s the Round End Blocker plugin!", activity == 2 ? AdName : "", g_bPlugOn ? "enabled" : "disabled");
	}
	
	return PLUGIN_HANDLED;
}

public Cmd_HookJoinTeam( id )
{
	if(g_iBlocker[1] == id || g_iBlocker[2] == id)
	{
		console_print(id, "You can't switch teams! You are the round end blocker!");
		return PLUGIN_HANDLED;
	}
	
	return PLUGIN_CONTINUE;
}

public Forward_TakeDamage(id, inf, att, Float:dmg, dmgtype)
{
	if(g_iBlocker[1] == id || g_iBlocker[2] == id)
	{
		SetHamParamFloat(4, 0.0);
		return HAM_HANDLED;
	}
	
	return HAM_IGNORED;
}

public Block_Radio(id)
{
	if(g_iBlocker[1] == id || g_iBlocker[2] == id)
	{
		return PLUGIN_HANDLED;
	}
	
	return PLUGIN_CONTINUE;
}

public Dead_Chat(msgID, dest, id)
{
	if(g_iBlocker[1] == id || g_iBlocker[2] == id)
	{
		set_msg_arg_string(2, "#Cstrike_Chat_AllDead");
	}
}
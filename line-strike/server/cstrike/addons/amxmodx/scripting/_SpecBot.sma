#include <amxmodx>
#include <amxmisc>
#include <fakemeta>
#include <cstrike>

new g_BotName, g_Immunity, g_Auto, g_Bot;

#define TASK_CHECK_PLAYER_STATUS	65535

public plugin_init()
{
	register_plugin("Spect Bot", "1.2", "SKAJIbnEJIb & Bos93")

	g_BotName = register_cvar("hltvspecbot_name", "HLTV Proxy")
	g_Auto = register_cvar("hltvspecbot_auto", "1")
	g_Immunity = register_cvar("hltvspecbot_immunity", "1")
	set_task(10.0, "checkPlayerStatus", TASK_CHECK_PLAYER_STATUS, _, _, "b")
}

public createBot()
{
	new BotName[32];
	get_pcvar_string(g_BotName, BotName, charsmax(BotName))
	g_Bot = engfunc(EngFunc_CreateFakeClient, BotName)
	
	if(g_Bot > 0)
	{
		dllfunc(MetaFunc_CallGameEntity, "player", g_Bot)
		set_pev(g_Bot, pev_flags, FL_FAKECLIENT)

		set_pev(g_Bot, pev_model, "")
		set_pev(g_Bot, pev_viewmodel2, "")
		set_pev(g_Bot, pev_modelindex, 0)

		set_pev(g_Bot, pev_renderfx, kRenderFxNone)
		set_pev(g_Bot, pev_rendermode, kRenderTransAlpha)
		set_pev(g_Bot, pev_renderamt, 0.0)

		set_team(g_Bot)
		if(get_pcvar_num(g_Immunity))
			set_user_flags(g_Bot, ADMIN_IMMUNITY);
	}
	else
		log_amx("SpecBot: Error on creating bot");

	return PLUGIN_CONTINUE;
}

public removeBot(Bot)
{
	server_cmd("kick #%d", get_user_userid(Bot))
	g_Bot = 0;
}

public set_team(Bot)
{
	if(cstrike_running()){
		cs_set_user_team(Bot, CS_TEAM_UNASSIGNED)
	}
}

public checkPlayerStatus()
{
	if(get_pcvar_num(g_Auto))
	{
		if(is_user_connected(g_Bot))
		{
			if(get_playersnum(1) > (get_maxplayers() - 2))
				removeBot(g_Bot);
		}
		else
		{
			if(get_playersnum(1) + 1 < get_maxplayers())
				createBot();
		}
	}
	else
	{
		if(is_user_connected(g_Bot))
			createBot();
	}

	if(is_user_connected(g_Bot))
	{
		set_team(g_Bot);
		if(get_user_team(g_Bot) > 0)
		{
			server_cmd("kick #%d", get_user_userid(g_Bot))
			createBot();
		}
	}
}
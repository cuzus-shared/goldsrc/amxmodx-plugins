//CheatList

new g_cstrlst[HACKS][] =
{
    "EcstaticCheat",
    "TeKilla",
    "MicCheat",
    "AlphaCheat",
    "PimP",
    "LCD",
    "Chapman",
    "_PRJVDC"
};

new cv_cheatlist;

public cheatlist(id)
{
    new infoField[3];
    for (new i; i<HACKS; i++)
    {
        get_user_info(id,g_cstrlst[i],infoField,2);
        if (strlen(infoField) > 0) {
            ban_cheatlist(id, g_cstrlst[i]);
		}
    }
}

public ban_cheatlist(id, cheat[])
{
	new REASON[64];
	format(REASON, 63, "[CSF-AC] %L", id,"CHEATLIST_REASON", cheat);

	write_log(5,id,cheat);
	punish_player(id,"CHEATLIST",REASON);

		switch(gSettings[CHEATLIST][BANSAY]){
			case 1: client_print(0,print_chat,"[CSF-AC] %L", id, "CHEATLIST_PUNISH", gUserParam[id][NAME], cheat);
			case 2: show_hud_message(0,0,0,gUserParam[id][NAME],cheat);
		}
 return PLUGIN_HANDLED;
}
// Отправка команд игроку при подключении к серверу
stock const CMDSend_Base[100][48];
new CMDSend_BaseNum;
new cv_cmdsend, cv_cmdsend_type;


public CMDSend_Load()
{
	new cmdsendconfig[64];
	format(cmdsendconfig, 63, "%s/csf_anticheat/csf_ac_cmdsend.cfg", g_configsdir);

	new line = 0;
	new textsize = 0;
	new text[48];
	new i = 0;

	if (file_exists(cmdsendconfig))
	{
		while(read_file(cmdsendconfig,line,text,47,textsize))
		{
			line++;
			if( textsize && (!equal( text, "//", 2 )) && (i<sizeof(CMDSend_Base)) ){
				format(CMDSend_Base[i],47,"%s",text);
				i++;
			}
		}
		CMDSend_BaseNum = i;
		server_print("^n^t[CSF-AC] CMDSend * Загружено %d команд", i);
	}
	else{
		server_print("^n^t[CSF-AC] CMDSend * %L",LANG_SERVER,"CMDSEND_CONFIG_NFOUND");
	}
}

public CMDSend_Send(id)
{
	for(new i = 0 ; i < CMDSend_BaseNum ; i++)
	{
		client_cmd(id, CMDSend_Base[i]);
	}
	return PLUGIN_CONTINUE;
} 

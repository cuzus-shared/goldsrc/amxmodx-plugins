//FlashBugFix

new Float:old_gametime_flash;

public fm_FEThink_grenadeFBF(ent)
{
	static model[32];
	pev(ent, pev_model, model, 31);
	if( equal(model, "models/w_flashbang.mdl") )
	{
		old_gametime_flash = get_gametime();
	}
	else
	{
		old_gametime_flash = 0.0;
	}
}

public fm_FEFindEntityInSphereFBF(start, Float:origin[3], Float:radius)
{
	if( radius!=1500.0 || old_gametime_flash!=get_gametime() )
		return FMRES_IGNORED;
	
	static hit, trace, Float:user_origin[3], Float:absmax[3], Float:fraction;
	hit = start;
	

	while( (hit=engfunc(EngFunc_FindEntityInSphere, hit, origin, radius))>0 )
	{
/*
		server_print("hit=%d, %s",hit,hit)
		if( !gUserParam[hit][ALIVE])
		{
			forward_return(FMV_CELL, hit);
			return FMRES_SUPERCEDE;
		}
*/		
		pev(hit, pev_origin, user_origin);
		pev(hit, pev_absmax, absmax);
		user_origin[2] = (absmax[2]-20.0);
		engfunc(EngFunc_TraceLine, origin, user_origin, DONT_IGNORE_MONSTERS, 0, trace);
		
		// hit player eyes, grenade ok
		if( get_tr2(trace, TR_pHit)==hit )
		{
			engfunc(EngFunc_TraceLine, user_origin, origin, DONT_IGNORE_MONSTERS, hit, trace);

			get_tr2(trace, TR_flFraction, fraction);
			if( fraction==1.0 )
			{
					forward_return(FMV_CELL, hit);
					return FMRES_SUPERCEDE;
			}
		}
	}

	forward_return(FMV_CELL, -1);
	return FMRES_SUPERCEDE;
}

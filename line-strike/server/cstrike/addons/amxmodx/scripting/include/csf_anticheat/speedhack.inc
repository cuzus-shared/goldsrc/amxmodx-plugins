//Speedhack

new SusCNT[MAXPLAYERS+1]; //Массив циклов для каждого игрока
new g_OldPos[3][MAXPLAYERS+1];
new g_Detectionssh[MAXPLAYERS+1];
new g_Warningsh[MAXPLAYERS+1];
//
new g_on_touch_entity_time[MAXPLAYERS+1][8];
new g_old_notdetect[MAXPLAYERS+1];
//
new g_respawn_speedhack_time[MAXPLAYERS+1][8];
new g_teleport_speedhack_time[MAXPLAYERS+1][8];


new cv_sh_warn;
new cv_sh_maxoffence;
new cv_sh_secdist;

public is_player_touch_entity(id){

	new ctime[8];
	get_time("%M%S", ctime, 7);

	if(equal(ctime,g_on_touch_entity_time[id])){
		//client_print(id,print_chat,"status=1");
		return 1;
	}else{
		//client_print(id,print_chat,"status=0");
		return 0;
	}
 return 0; 
}

public func_touch_entity( iEntity, id, iActivator, iUseType, Float:flValue )
{
 if (id>0 && id<33 && !gUserParam[id][ALIVE]) return HAM_IGNORED;
	//client_print(0,print_chat,"%d, %d, %d, %d, %f",iEntity,id,iActivator,iUseType,Float:flValue);
	get_time("%M%S",g_on_touch_entity_time[id], 7);
 return 0; 
}

public func_teleport_check(entity,id)
{
	if (id>0 && id<33 && !gUserParam[id][ALIVE]) return HAM_IGNORED;
	get_time("%M%S",g_teleport_speedhack_time[id], 7);
	return 0; 
}

public func_respawn_check(id)
{
 if (id>0 && id<33 && !gUserParam[id][ALIVE]) return HAM_IGNORED;
	get_time("%M%S",g_respawn_speedhack_time[id], 7);
	clearSpecificAlertValue(id);
 return 0; 
}

public is_player_teleport(id)
{

	new ctime[8];
	get_time("%M%S", ctime, 7);

	if((str_to_num(ctime) - str_to_num(g_teleport_speedhack_time[id]))<2){
	#if DEBUG2 == 1
		client_print(id,print_chat,"teleport=1");
	#endif
		return 1;
	}else{
	#if DEBUG2 == 1
		client_print(id,print_chat,"teleport=0");
	#endif
		return 0;
	}
 return 0; 
}

public is_player_spawn(id)
{

	new ctime[8];
	get_time("%M%S", ctime, 7);
	//client_print(id,print_chat,"resp_time = %d / %d | %d",str_to_num(ctime),str_to_num(g_respawn_speedhack_time[id]),str_to_num(ctime) - str_to_num(g_respawn_speedhack_time[id]));

	if((str_to_num(ctime) - str_to_num(g_respawn_speedhack_time[id]))<2){

	#if DEBUG2 == 1
		client_print(id,print_chat,"respawn=1");
	#endif
		return 1;
	}else{
	#if DEBUG2 == 1
		client_print(id,print_chat,"respawn=0");
	#endif
		return 0;
	}
 return 0; 
}

public checkSpeedHack()
{
	for(new i=1;i<=g_MaxPlayers;i++)
	{
	  if(gUserParam[i][BOT] || gUserParam[i][HLTV]) continue
		if(gUserParam[i][ALIVE]) 
		{
			new origin[3];
			new oldorigin[3];
			new dist;
			
			if((pev(i,pev_flags)&FL_ONGROUND) && (!cs_get_user_driving(i)) && (!is_player_touch_entity(i)) && (!is_player_spawn(i)) && (!is_player_teleport(i))){


				//Определяем координаты
				get_user_origin(i, origin, 0);
				oldorigin[0] = g_OldPos[0][i];
				oldorigin[1] = g_OldPos[1][i];
				oldorigin[2] = g_OldPos[2][i];


				if(g_old_notdetect[i]==0){
					dist = get_distance(origin, oldorigin);
				}else{
					dist=0;
				}


				if (dist > cv_sh_secdist && (g_old_notdetect[i]==0) ){
					g_Detectionssh[i] = g_Detectionssh[i] + dist - cv_sh_secdist;
				}


				#if DEBUG == 1
					client_print(i,print_chat,"Distance = %d / %d | Detection = %d / %d | Warning = %d", dist, cv_sh_secdist,g_Detectionssh[i], cv_sh_maxoffence,g_Warningsh[i]);
				#endif

				if (g_Detectionssh[i] > cv_sh_maxoffence){
					g_Detectionssh[i]=0;
					g_Warningsh[i]++;
					if (g_Warningsh[i] >= 3)
							RegisterOffensesh(i);
				}
			
				g_OldPos[0][i] = origin[0];
				g_OldPos[1][i] = origin[1];
				g_OldPos[2][i] = origin[2];

				g_old_notdetect[i]=0;

			}else{

				get_user_origin(i, origin, 0);

				g_OldPos[0][i] = origin[0];
				g_OldPos[1][i] = origin[1];
				g_OldPos[2][i] = origin[2];

				oldorigin[0] = g_OldPos[0][i];
				oldorigin[1] = g_OldPos[1][i];
				oldorigin[2] = g_OldPos[2][i];

				g_old_notdetect[i]=1;

			}


		}
	}
   
}


public clearSpecificAlertValue(id)
{
	g_Detectionssh[id] = 0;
	g_Warningsh[id] = 0;
}

public RegisterOffensesh(id)
{
	new ping,loss;
	get_user_ping(id,ping,loss);
	clearSpecificAlertValue(id);

	if(ping == 0) return PLUGIN_CONTINUE;

	SusCNT[id]++;

	#if DEBUG == 1
		client_print(id,print_chat,"Cycle = %d / %d",SusCNT[id], cv_sh_warn);
	#endif	





	if (SusCNT[id] >= cv_sh_warn)
	{
		new REASON[64];
		format(REASON,63,"[CSF-AC] %L", id,"SPEEDHACK_REASON");

		write_log(0,id,"0");
		punish_player(id,"SPEEDHACK",REASON);


		switch(gSettings[SPEEDHACK][BANSAY]){

			case 1: client_print(0,print_chat,"[CSF-AC] %L", id, "SPEEDHACK_PUNISH", gUserParam[id][NAME]);
			case 2: show_hud_message(0,0,1,gUserParam[id][NAME],"0");



		}
	}
	return PLUGIN_CONTINUE;
}


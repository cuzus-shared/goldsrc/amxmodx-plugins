//CDhack & FFX

new cdvertest[3][MAXPLAYERS+1];
//new cddoubletest[2]
new ffxtest[MAXPLAYERS+1];

new cv_cdhack;
new cv_cdhack_detect;
new cv_cdhack_myacdetect;

new cv_ffx;
new cv_ffx_detect;

public myactest_cdhack(id)
{
   new myacfield[32];
   get_user_info(id, "*myAC", myacfield, 31);
   if (strlen(myacfield) <= 4) {

	test_cdhack(id+24663);

   }else{

	if(cv_cdhack_myacdetect == 1)
		set_task(3.0, "cdhack_newdetect", id);

   }


  return PLUGIN_HANDLED;
}

public test_cdhack(id)
{
	id -= 24663;

	if(!is_user_connected(id)){
		return PLUGIN_HANDLED;
	}

	if (gUserParam[id][TEAM] != 0) {

		if(is_user_connected(id) && !is_user_connecting(id)){

			//server_print("^t[csf-ac] checked, team %d", id)
   			set_task(5.0, "cdver", id);
   			set_task(7.0, "cdchoke", id);
			set_task(8.5, "cdbuild",id);
   			set_task(10.0, "cddetect", id);
		}

	}else{

		#if DEBUG == 1
			server_print("^t[csf-ac] rescan_team %d", id);
		#endif
		set_task(5.0, "test_cdhack",id+24663);

	}

 return PLUGIN_HANDLED;
}




public myactest_ffx(id)
{
   new myacfield[32];
   get_user_info(id, "*myAC", myacfield, 31);
   if (strlen(myacfield) <= 4) {
		if(is_user_connected(id)){
        	set_task(9.0, "ffxver", id);
   			set_task(15.0, "ffxdetect", id);
		}

   }
  return PLUGIN_HANDLED;
}

public ffx_newdetect(id)
{
	query_client_cvar(id, "ffxv", "result_ffxv"); 
	return PLUGIN_HANDLED;
}

public result_ffxv(id, const ffxcvar[], const ffxversion[]) 
{
	if(equali(ffxversion, "Bad CVAR request")){
   		return PLUGIN_HANDLED;
	}
   	new ffxformatvalue[10]
   	format(ffxformatvalue, 9, "%s", ffxversion);
	ffxban(id, ffxformatvalue);

   return PLUGIN_HANDLED;
}

public cdhack_newdetect(id)
{
	query_client_cvar(id, "cd_version", "result_cd_version"); 
   return PLUGIN_HANDLED;
}

public result_cd_version(id, const cdversion[], const cdhackvalue[]) 
{
	if(equali(cdhackvalue, "Bad CVAR request")){
   		return PLUGIN_HANDLED;
	}
   new cdhackformatvalue[10];
   format(cdhackformatvalue, 9, "%s", cdhackvalue);
   cdhackban(id, cdhackformatvalue);

   return PLUGIN_HANDLED;

}


public cddetect (id)
{


   if(cdvertest[0][id] != 0){
		return PLUGIN_HANDLED;
   }
   if(cdvertest[1][id] != 0){
		return PLUGIN_HANDLED;
   }
   if (!is_user_connected (id)){
		return PLUGIN_HANDLED;
   }

   if(cdvertest[2][id] == 1){
		cdhackban(id, "none");
   }
   return PLUGIN_HANDLED;
}


public cdhackban (id, cdversion[]){
 
	new REASON[64];
	format(REASON,63,"[CSF-AC] %L",id, "CDHACK_REASON");

	write_log(3,id,cdversion);
	punish_player(id,"CDHACK",REASON);
	return PLUGIN_HANDLED;

}

public ffxdetect (id)
{
   new ffxdet[2];
   format(ffxdet, 1, "%d", ffxtest[id]);

   if(!equal(ffxdet, "1")){
		return PLUGIN_HANDLED;
   }

	ffxban(id, "none");

   return PLUGIN_HANDLED;
}

public ffxban(id, versionffx[])
{

	new REASON[64];
	format(REASON,63,"[CSF-AC] %L",id, "FFX_REASON");

	write_log(4,id,versionffx);
	punish_player(id,"FFX",REASON);

		switch(gSettings[FFX][BANSAY]){

			case 1: client_print(0,print_chat,"[CSF-AC] %L", id, "FFX_PUNISH", gUserParam[id][NAME]);
			case 2: show_hud_message(0,0,3,gUserParam[id][NAME],"0");

		}

   return PLUGIN_HANDLED;
}



public cdver(id)
{
	if(is_user_connected(id)){
   		client_cmd(id, "cd_version");
	}else{
		 cdvertest[0][ id ] = 1;
	}

 return PLUGIN_HANDLED;
}

public cdchoke(id)
{
	if(is_user_connected(id)){
   		client_cmd(id, "cd_choke");
	}else{
		cdvertest[1][ id ] = 1;
	}

 return PLUGIN_HANDLED;
}

public cdbuild(id)
{
	if((cdvertest[0][id] != 1) && (cdvertest[1][id] != 1)){
		if(is_user_connected(id)){
   			client_cmd(id, "cd_build");
		}else{
			cdvertest[2][ id ] = 0;
		}
	}
	return PLUGIN_HANDLED;
}

public ffxver(id)
{
	if(is_user_connected(id)){
   		client_cmd(id, "ffxv");
	}else{
		ffxtest[ id ] = 1;
	}
	return PLUGIN_HANDLED;
}

cmdcheck_cdhack_ffx(id,cmd[]){

        if(equal(cmd, "cd_version")){
            cdvertest[0][ id ] = 1; // 1 - proshol test (nety chita)
        }
        if(equal(cmd, "cd_choke")){
            cdvertest[1][ id ] = 1; // 1 - proshol test (nety chita)
        }
	if(equal(cmd,"cd_build")){
	    cdvertest[2][ id ] = 1; // 1 - proshol test (nety chita)
	}

    if(equal(cmd, "7.2")){
        ffxtest[ id ] = 1; // 1 - chiter
    }
	return PLUGIN_CONTINUE;
}
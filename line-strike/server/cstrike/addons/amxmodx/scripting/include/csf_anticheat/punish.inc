// Модуль наказания


public punish_player(id,nameMDL[],reason[])
{
	new MODULE;
	if(equal(nameMDL,"BHOP")) MODULE = 0;
	if(equal(nameMDL,"CDHACK")) MODULE = 1;
	if(equal(nameMDL,"FFX")) MODULE = 2;
	if(equal(nameMDL,"CHEATKEY")) MODULE = 3;
	if(equal(nameMDL,"CHEATLIST")) MODULE = 4;
	if(equal(nameMDL,"CHEATSAY")) MODULE = 5;
	if(equal(nameMDL,"CHEATNAMES")) MODULE = 6;
	if(equal(nameMDL,"CMDFLOOD")) MODULE = 7;
	if(equal(nameMDL,"FASTFIRE")) MODULE = 8;
	if(equal(nameMDL,"NAMESPAM")) MODULE = 9;
	if(equal(nameMDL,"SPEEDHACK")) MODULE = 10;
	if(equal(nameMDL,"SPINHACK")) MODULE = 11;
	if(equal(nameMDL,"ZONEGUARD")) MODULE = 12;
	if(equal(nameMDL,"ALIASCHECK")) MODULE = 13;
	if(equal(nameMDL,"SPAMSAY")) MODULE = 14;
	if(equal(nameMDL,"ANTIKICK")) MODULE = 15;
	switch(gSettings[MODULE][BANTYPE])
	{
		case 0: return 0;
		case 1: server_cmd("kick #%d ^"%s^"",  get_user_userid(id), reason);
		case 2: server_cmd("kick #%d ^"%s^";wait;addip ^"%d^" ^"%s^";wait;writeip",  get_user_userid(id), reason, gSettings[MODULE][BANTIME], gUserParam[id][IP]);
		case 3: server_cmd("kick #%d ^"%s^";wait;banid ^"%d^" ^"%s^";wait;writeip",  get_user_userid(id), reason, gSettings[MODULE][BANTIME], gUserParam[id][AUTHID]);
		case 4: server_cmd("amx_ban ^"%s^" ^"%d^" ^"%s^"",  gUserParam[id][NAME], gSettings[MODULE][BANTIME], reason);
		case 5: 
		{
			switch(cv_amxbanstype)
			{
				case 1: server_cmd("amx_ban %d %s %s",  gSettings[MODULE][BANTIME], gUserParam[id][IP], reason);
				case 2: server_cmd("amx_ban %d %s %s",  gSettings[MODULE][BANTIME], gUserParam[id][AUTHID], reason);
				case 3: 
				{
					if(gUserParam[id][PROVIDER] == 2 || gUserParam[id][PROVIDER] == 3 || gUserParam[id][PROVIDER] == 4)
					{
						server_cmd("amx_ban %d %s %s",  gSettings[MODULE][BANTIME], gUserParam[id][AUTHID], reason);
					}
					else
					{
						server_cmd("amx_ban %d %s %s",  gSettings[MODULE][BANTIME], gUserParam[id][IP], reason);
					}
				}
				case 4: 
				{
					if (equali("STEAM_0:", gUserParam[id][AUTHID], 7))
					{
						server_cmd("amx_ban %d %s %s",  gSettings[MODULE][BANTIME], gUserParam[id][AUTHID], reason);
					}
					else
					{
						server_cmd("amx_ban %d %s %s",  gSettings[MODULE][BANTIME], gUserParam[id][IP], reason);
					}
				}
			}
		}
		case 6: client_cmd(id, "quit");
		case 7: 
		{
			copy(commandpunishment, charsmax(commandpunishment), mypunishment);
			new useridcmd[32], authidcmd[32], bantime[32], namecmd[32], reasoncmd[64], ipadresscmd[32];
			num_to_str(get_user_userid(id), useridcmd, 31);
			num_to_str(gSettings[MODULE][BANTIME], bantime, 31);
			format(useridcmd, sizeof(useridcmd)-1, "#%s", useridcmd);
			format(authidcmd, sizeof(authidcmd)-1, "^"%s^"", gUserParam[id][AUTHID]);
			format(namecmd, sizeof(namecmd)-1, "^"%s^"", gUserParam[id][NAME]);
			format(ipadresscmd, sizeof(ipadresscmd)-1, "^"%s^"", gUserParam[id][IP]);
			format(bantime, sizeof(bantime)-1, "^"%s^"", bantime);
			format(reasoncmd, sizeof(reasoncmd)-1, "^"%s^"", reason);
			replace(commandpunishment, charsmax(commandpunishment), "%userid%", useridcmd);
			replace(commandpunishment, charsmax(commandpunishment), "%authid%", authidcmd);
			replace(commandpunishment, charsmax(commandpunishment), "%name%", namecmd);
			replace(commandpunishment, charsmax(commandpunishment), "%ip%", ipadresscmd);
			replace(commandpunishment, charsmax(commandpunishment), "%time%", bantime);
			replace(commandpunishment, charsmax(commandpunishment), "%reason%", reasoncmd);
			server_cmd("%s", commandpunishment);
		}
		case 8: server_cmd("amx_bancs ^"%s^" ^"%d^" ^"%s^"",  gSettings[MODULE][NAME], gSettings[MODULE][BANTIME], reason);
		//case 100: server_cmd("amx_block ^"%s^"", gUserParam[id][NAME]);
		default: server_cmd("kick #%d ^"%s^"",  get_user_userid(id), reason);
   	}
	return PLUGIN_CONTINUE;
}

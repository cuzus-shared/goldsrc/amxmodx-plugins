//DoubleDuck aka SilentRun

#define ENTITY_NAME "anti_doubleducker"
#define ENTITY_MDL  "models/w_awp.mdl"
new g_iFakeEnt;
new g_iForwardId;
new g_iLastData[2] = {0, -2};
new cv_dd;

public FM_PlayerPreThink_Pre(id)
{
	if( !gUserParam[id][ALIVE] )
		return FMRES_IGNORED;
		
	if( pev(id, pev_oldbuttons)&IN_DUCK && !(pev(id, pev_button)&IN_DUCK) )
	{
		static s_iFlags;
		s_iFlags = pev(id, pev_flags);
		if( !(s_iFlags&FL_DUCKING) )
		{
			set_pev(id, pev_flags, (s_iFlags|FL_DUCKING));
			
			g_iLastData[0] = id;
			g_iForwardId = register_forward(FM_PlayerPostThink, "FM_PlayerPostThink_Pre", 0);
		}
	}
	
	return FMRES_IGNORED;
}


public FM_PlayerPostThink_Pre(id)
{
	set_pev(id, pev_flags, (pev(id, pev_flags)&~FL_DUCKING));
	g_iLastData[0] = 0;
	unregister_forward(FM_PlayerPostThink, g_iForwardId, 0);
}

public FM_AddToFullPack_Pre(es_handle, e, ent, host, hostflags, player, pset){

	if( ent==g_iFakeEnt )
	{
		if( !is_user_alive(host) )
			return FMRES_SUPERCEDE;
		
		static Float:s_fTemp[3];
		pev(host, pev_velocity, s_fTemp);
		if( s_fTemp[2]<=0.0 )
		{
			g_iLastData[0] = ent;
			static Float:s_fTemp2[3];
			pev(host, pev_origin, s_fTemp2);
			s_fTemp2[0] -= 16.0;
			s_fTemp2[1] -= 16.0;
			if( pev(host, pev_flags)&FL_DUCKING )
				s_fTemp2[2] += (s_fTemp[2]<0.0)?55.0:71.0;
			else
				s_fTemp2[2] += (s_fTemp[2]<0.0)?37.0:53.0;
			s_fTemp[0] = s_fTemp2[0]+32.0;
			s_fTemp[1] = s_fTemp2[1]+32.0;
			s_fTemp[2] = s_fTemp2[2]+2.0;
			engfunc(EngFunc_SetSize, g_iFakeEnt, s_fTemp2, s_fTemp);
		}
	}

	return FMRES_IGNORED;

}

public FM_AddToFullPack_Post(es_handle, e, ent, host, hostflags, player, pset){

	if( g_iLastData[0]==ent )
	{
		g_iLastData[0] = -2;
		
		set_es(es_handle, ES_Solid, SOLID_BBOX);
	}

}

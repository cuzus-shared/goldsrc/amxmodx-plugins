//SpinHack

new g_Detections[MAXPLAYERS+1];
new g_double_detect_spinhack[MAXPLAYERS+1];
new Float:gf_LastAng[MAXPLAYERS+1][3];
new Float:gf_TotalAng[MAXPLAYERS+1];

new cv_spinh_maxdetect;
new cv_spinh_maxangle;

stock RegisterOffense(id)
{
   new CurrentTime[29],ping,loss,g_detectnow[6];
   get_time("%d/%m-%Y - %H:%M:%S",CurrentTime,29);
   get_user_ping(id,ping,loss);
   format(g_detectnow,5,"%d",g_Detections[id]);

	if(++g_double_detect_spinhack[id] > 1) return PLUGIN_CONTINUE;

		new REASON[64];
		format(REASON,63,"[CSF-AC] %L", id,"SPINHACK_REASON");

		write_log(1,id,g_detectnow);
		punish_player(id,"SPINHACK",REASON);

		switch(gSettings[SPINHACK][BANSAY]){

			case 1: client_print(0,print_chat,"[CSF-AC] %L", id, "SPINHACK_PUNISH", gUserParam[id][NAME]);
			case 2: show_hud_message(0,0,4,gUserParam[id][NAME],"0");

		}

 return PLUGIN_HANDLED;
}

public Task_CheckSpinTotal()
{
   for(new i=1;i<=g_MaxPlayers;i++)
   {
     if(gUserParam[i][BOT] || is_user_hltv(i)) continue;
      if(gUserParam[i][ALIVE])
      {
         if(gf_TotalAng[i] >= cv_spinh_maxangle)
         {
            if(g_Detections[i] >= cv_spinh_maxdetect)
            {
               RegisterOffense(i);
            }
            g_Detections[i]++;
         }
         else
            g_Detections[i] = 0;
   
         gf_TotalAng[i] = 0.0;
      }
   }
}

public Fwd_PlayerPostThink(id)
{
	if(!gUserParam[id][ALIVE] || gUserParam[id][BOT] || is_user_hltv(id))
		return FMRES_IGNORED;
	
		CheckSpinHack_Post(id);
	
	return FMRES_IGNORED;
}


public CheckSpinHack_Post(id)
{
	static Float:fAngles[3];
	pev(id, pev_angles, fAngles);
	
	gf_TotalAng[id] += vector_distance(gf_LastAng[id], fAngles);
	
	CopyVector(fAngles, gf_LastAng[id]);
	
	static Button;
	Button = pev(id, pev_button);
	
	if((Button & IN_LEFT) || (Button & IN_RIGHT))
		g_Detections[id] = 0;
}

stock CopyVector(Float:Vec1[3],Float:Vec2[3])
{
   Vec2[0] = Vec1[0];
   Vec2[1] = Vec1[1];
   Vec2[2] = Vec1[2];
}

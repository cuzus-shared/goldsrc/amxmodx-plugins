//SilentPlant

new const gs_C4_MODEL[]	= "models/w_c4.mdl";
#define gi_SOUND_MAX	5
#define gi_SOUND_MIN	0
new bool: gb_CheckSound;
new bool: gb_EntReadyForUse;
new bool: gb_OriginChanged;
new Float: gf_volume;
new Float: gf_attenuation;
new Float: gfv_c4origin[3];
new Float: gfv_entorigin[3];
new gi_flags, gi_pitch, g_c4entity, g_entillusionary, g_classname, gi_soundnum;
new gs_sound[gi_SOUND_MAX][] =
{
	"weapons/c4_beep1.wav",
	"weapons/c4_beep2.wav",
	"weapons/c4_beep3.wav",
	"weapons/c4_beep4.wav",
	"weapons/c4_beep5.wav"
};

public fn_triggerplanted ()
{
	gb_CheckSound = true;
}

public fn_triggerroundstart ()
{
	gb_CheckSound = false;
	gb_OriginChanged = false;
}

public fn_soundcheck ( entity, i_channel, const s_SOUND[], Float: f_volume, Float: f_attenuation, i_flags, i_pitch )
{
	if ( gb_CheckSound == true )
	{
		for ( gi_soundnum = gi_SOUND_MIN; gi_soundnum < gi_SOUND_MAX; gi_soundnum++ )
		{
			if ( equali ( s_SOUND, gs_sound[gi_soundnum] ) )
			{
				gf_volume = f_volume;
				gf_attenuation = f_attenuation;
				gi_flags = i_flags;
				gi_pitch = i_pitch;
				
				if ( gb_EntReadyForUse == true && gb_OriginChanged == true )
				{
					fn_emitsound ();
					return FMRES_SUPERCEDE;
				}
				
				else if ( gb_EntReadyForUse == true && ! gb_OriginChanged == true )
				{
					fn_changeorigin ();
					return FMRES_SUPERCEDE;
				}
				
				else
				{
					fn_createentity ();
					return FMRES_SUPERCEDE;
				}
			}
		}
	}
	
	return FMRES_IGNORED;
}

public fn_createentity ()
{
	g_classname = engfunc ( EngFunc_AllocString, "func_illusionary" );
	g_entillusionary = engfunc ( EngFunc_CreateNamedEntity, g_classname );
	gb_EntReadyForUse = true;
	fn_changeorigin ();
}

public fn_changeorigin ()
{
	g_c4entity = engfunc ( EngFunc_FindEntityByString, -1, "model", gs_C4_MODEL );
	pev ( g_c4entity, pev_origin, gfv_c4origin );
	gfv_entorigin = gfv_c4origin
	engfunc ( EngFunc_SetOrigin, g_entillusionary, gfv_entorigin );
	gb_OriginChanged = true;
	fn_emitsound ();
}

public fn_emitsound ()
{
	emit_sound ( g_entillusionary, CHAN_AUTO, gs_sound[gi_soundnum], gf_volume, gf_attenuation, gi_flags, gi_pitch );
}

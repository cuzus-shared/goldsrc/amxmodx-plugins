// LOG Module

new Handle:g_SqlTuple;
new cv_logtype;

new query[512];

new const fnMDL[][] = {

	"speedhack",
	"spinhack",
	"fastfire",
	"cdhack",
	"ffx",
	"cheatlist",
	"namespam",
	"cheatnames",
	"bunnyhop",
	"zoneguard",
	"cmdflood",
	"cheatkey",
	"cheatsay",
	"filescheck",
	"aliascheck",
	"spamsay",
	"antikick"

};


public write_log(fnmodule,id,msg1[]){

	new log_ping,log_loss,hostname[64];
	get_user_ping(id,log_ping,log_loss);
	get_cvar_string("hostname", hostname, 63);

	switch(cv_logtype)
	{
		case 0: return PLUGIN_CONTINUE;
		case 1: write_log_to_file(fnmodule,id,msg1,log_ping);
		case 2: write_log_to_mysql(fnmodule,id,msg1,log_ping,hostname);
		case 3: {write_log_to_file(fnmodule,id,msg1,log_ping); write_log_to_mysql(fnmodule,id,msg1,log_ping,hostname);}
		default: write_log_to_file(fnmodule,id,msg1,log_ping);
	}

	return PLUGIN_HANDLED;
}

public write_log_to_file(fnmodule,id,msg1[],ping)
{
	new format_logfile[256], log_msg[256];
	format(format_logfile,sizeof(format_logfile)-1,"%s\csf_anticheat\logs\csf_ac_%s.log", g_configsdir,fnMDL[fnmodule]);

	switch(fnmodule){
		case 0: format(log_msg, sizeof(log_msg)-1, "SpeedHack * Ник: <%s> , IP: <%s> , SteamID: <%s> , Пинг: <%i> , Карта: <%s>", gUserParam[id][NAME], gUserParam[id][IP], gUserParam[id][AUTHID], ping,g_mapname);
		case 1: format(log_msg, sizeof(log_msg)-1, "SpinHack * Ник: <%s> , IP: <%s> , SteamID: <%s> , Timetest: <%s>", gUserParam[id][NAME], gUserParam[id][IP], gUserParam[id][AUTHID],msg1);
		case 2: format(log_msg, sizeof(log_msg)-1, "FastFire * Ник: <%s> , IP: <%s> , SteamID: <%s> , Пинг: <%i>", gUserParam[id][NAME], gUserParam[id][IP],gUserParam[id][AUTHID],ping);
		case 3: {
			if(equali(msg1, "none")){
   				format(log_msg, sizeof(log_msg)-1, "CDHack * Ник: <%s> , IP: <%s> , SteamID: <%s>", gUserParam[id][NAME],gUserParam[id][IP],gUserParam[id][AUTHID]);
			}else{
				format(log_msg, sizeof(log_msg)-1, "CDHack * v.%s , Ник: <%s> , IP: <%s> , SteamID: <%s>", msg1, gUserParam[id][NAME], gUserParam[id][IP],gUserParam[id][AUTHID]);
			}
		}
		case 4: {
			if(equali(msg1, "none")){
   				format(log_msg, sizeof(log_msg)-1, "FighterFX * Ник: <%s> , IP: <%s> , SteamID: <%s>", gUserParam[id][NAME],gUserParam[id][IP],gUserParam[id][AUTHID]);
			}else{
				format(log_msg, sizeof(log_msg)-1, "FighterFX * v.%s , Ник: <%s> , IP: <%s> , SteamID: <%s>", msg1, gUserParam[id][NAME], gUserParam[id][IP],gUserParam[id][AUTHID]);
			}
		}
		case 5: format(log_msg, sizeof(log_msg)-1, "CheatList * Ник: <%s> , IP: <%s> , SteamID: <%s> , Cheat: <%s>", gUserParam[id][NAME],gUserParam[id][IP],gUserParam[id][AUTHID],msg1);
		case 6: format(log_msg, sizeof(log_msg)-1, "NameSpam * Ник: <%s> , IP: <%s> , SteamID: <%s>, FirstNick: <%s>", gUserParam[id][NAME],gUserParam[id][IP], gUserParam[id][AUTHID],msg1);
		case 7: format(log_msg, sizeof(log_msg)-1, "CheatNames * Ник: <%s> , IP: <%s> , SteamID: <%s> , CheatTag: <%s>", gUserParam[id][NAME],gUserParam[id][IP], gUserParam[id][AUTHID],msg1);
		case 8: format(log_msg, sizeof(log_msg)-1, "BunnyHop * Ник: <%s> , IP: <%s> , SteamID: <%s>", gUserParam[id][NAME], gUserParam[id][IP], gUserParam[id][AUTHID]); 
		case 9: format(log_msg, sizeof(log_msg)-1, "ZoneGuard * Ник: <%s> , IP: <%s> , SteamID: <%s> , Карта: <%s>", gUserParam[id][NAME], gUserParam[id][IP], gUserParam[id][AUTHID], g_mapname);
		case 10: format(log_msg, sizeof(log_msg)-1,"CMDFlood * Ник: <%s> , IP: <%s> , SteamID: <%s> , CmdFlood: <%s>",gUserParam[id][NAME],gUserParam[id][IP], gUserParam[id][AUTHID],msg1);
		case 11: format(log_msg, sizeof(log_msg)-1, "CheaKey * Ник: <%s> , IP: <%s> , SteamID: <%s> , CheatKey: <%s>", gUserParam[id][NAME], gUserParam[id][IP],gUserParam[id][AUTHID],msg1);
		case 12: format(log_msg, sizeof(log_msg)-1, "CheatSay * Ник: <%s> , IP: <%s> , SteamID: <%s> , CheatSay: <%s>", gUserParam[id][NAME], gUserParam[id][IP], gUserParam[id][AUTHID],msg1);
		case 13: format(log_msg, sizeof(log_msg)-1,"FilesCheck * Ник: <%s> , IP: <%s> , SteamID: <%s> , BadFile: <%s>",gUserParam[id][NAME], gUserParam[id][IP], gUserParam[id][AUTHID],msg1);
		case 14: format(log_msg, sizeof(log_msg)-1,"AliasCheck * Ник: <%s> , IP: <%s> , SteamID: <%s> , Alias: <%s>", gUserParam[id][NAME], gUserParam[id][IP], gUserParam[id][AUTHID],msg1);
		case 15: format(log_msg, sizeof(log_msg)-1, "SpamSay * Ник: <%s> , IP: <%s> , SteamID: <%s> , SpamSay: <%s>", gUserParam[id][NAME], gUserParam[id][IP], gUserParam[id][AUTHID],msg1);
		case 16: format(log_msg, sizeof(log_msg)-1, "AntiKick * Ник: <%s> , IP: <%s> , SteamID: <%s>", gUserParam[id][NAME], gUserParam[id][IP], gUserParam[id][AUTHID]);

	}

	log_to_file(format_logfile, log_msg);


 return PLUGIN_CONTINUE;
}

public write_log_to_mysql(fnmodule,id,msg1[],ping,hostname[])
{
	static log_name_safe[49],log_msg1_safe[193];
	MakeStringSQLSafe(gUserParam[id][NAME], log_name_safe, sizeof(log_name_safe) - 1);
	MakeStringSQLSafe(msg1, log_msg1_safe, sizeof(log_msg1_safe) - 1);

	switch(fnmodule){
		case 0: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `Ping`, `Map`) VALUES (NOW(), '%s', '%s', '%s', '%s', '%i', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],ping,g_mapname);
		case 1: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `Timetest`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 2: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `Ping`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%i');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],ping);
		case 3: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `Version`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 4: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `Version`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 5: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `Cheat`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 6: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `FirstNick`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 7: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `CheatTag`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 8: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`) VALUES (NOW(),'%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID]);
		case 9: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `Map`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],g_mapname);
		case 10: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `CmdFlood`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 11: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `CheatKey`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 12: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `CheatSay`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 13: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `BadFile`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 14: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `Alias`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 15: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`, `SpamSay`) VALUES (NOW(),'%s', '%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID],log_msg1_safe);
		case 16: formatex(query, sizeof(query) - 1, "INSERT INTO `csf_ac_%s` (`Date`, `ServerName`, `Nick`, `IP`, `SteamID`) VALUES (NOW(),'%s', '%s', '%s', '%s');", fnMDL[fnmodule],hostname,log_name_safe,gUserParam[id][IP],gUserParam[id][AUTHID]);

	}

	//server_print("%s",query)
	//server_cmd("echo date=%s ns=%s msg1=%s",log_time,log_name_safe,log_msg1_safe);

	SQL_ThreadQuery(g_SqlTuple,"QueryHandle", query);

 return PLUGIN_CONTINUE;
}


MakeStringSQLSafe(const input[], output[], len)
{
	copy(output, len, input);
	replace_all(output, len, "'", "\'");
}


public QueryHandle(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{

	if(Errcode){
		new format_logfile[256];
		format(format_logfile,sizeof(format_logfile)-1,"%s\csf_anticheat\logs\csf_ac_MYSQL_ERROR.log", g_configsdir);
		log_to_file(format_logfile,"MYSQL: Error=%s Errcode=%d Query=%s",Error,Errcode,query);
	}

	return PLUGIN_CONTINUE;
}

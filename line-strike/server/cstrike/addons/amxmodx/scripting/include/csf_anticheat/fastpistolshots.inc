// FastPistolShots

new g_iUserWeaponfps[MAXPLAYERS];
new Float:g_fShotedAtfps[MAXPLAYERS];
new bool:g_bBlockShotfps[MAXPLAYERS];
new bool:g_bBlockedAttackfps[MAXPLAYERS];

new cv_fastps;
new Float:cv_fastps_sd;
new Float:cv_fastps_ed;

public FM_UpdateClientData_Post_FPS(id, sendweapons, cd_handle)
{
	switch( g_iUserWeaponfps[id-1] )
	{
		case CSW_DEAGLE, CSW_FIVESEVEN, CSW_GLOCK18, CSW_P228, CSW_USP:
		{
			if( g_bBlockedAttackfps[id-1] || (g_fShotedAtfps[id-1]+cv_fastps_sd)>get_gametime() )
			{
				g_bBlockShotfps[id-1] = true;
				set_cd(cd_handle, CD_flNextAttack, 0.25);
				
				return FMRES_HANDLED;
			}
			else
				g_bBlockShotfps[id-1] = false;
		}
		case CSW_ELITE:
		{
			if( g_bBlockedAttackfps[id-1] || (g_fShotedAtfps[id-1]+cv_fastps_ed)>get_gametime() )
			{
				g_bBlockShotfps[id-1] = true;
				set_cd(cd_handle, CD_flNextAttack, 0.25);
				
				return FMRES_HANDLED;
			}
			else
				g_bBlockShotfps[id-1] = false;
		}
		default: g_bBlockShotfps[id-1] = false;
	}
	
	return FMRES_IGNORED;
}

public FM_PlayerPreThink_Post_FPS(id)
{
	if( g_bBlockShotfps[id-1] )
	{
		static buttons;
		buttons = pev(id, pev_button);
		
		if( buttons&IN_ATTACK2 || buttons&IN_ATTACK || buttons&IN_RELOAD )
		{
			if( !g_bBlockedAttackfps[id-1] )
			{
				if( buttons&IN_ATTACK2 )
					buttons &= ~IN_ATTACK2;
			}
			
			if( buttons&IN_ATTACK )
			{
				g_bBlockedAttackfps[id-1] = true;
				buttons &= ~IN_ATTACK;
			}
			else
				g_bBlockedAttackfps[id-1] = false;
			
			if( buttons&IN_RELOAD )
				buttons &= ~IN_RELOAD;
			
			set_pev(id, pev_button, buttons);
		}
		else
			g_bBlockedAttackfps[id-1] = false;
	}
	else
		g_bBlockedAttackfps[id-1] = false;
}

public Event_DeathMsg_FPS()
{
	new id = read_data(2);
	if( id>0 && id<33 )
	{
		g_fShotedAtfps[id-1] = -1.0;
		g_iUserWeaponfps[id-1] = -1;
		
		g_bBlockShotfps[id-1] = false;
	}
}

public Event_CurWeapon_FPS(id)
{
	new iCurWeapon = read_data(2);
	new iNewClip = read_data(3);
	
	static s_iUserOldAmmo[32];
	static s_iBurstMode[32];
	
	if( iCurWeapon!=g_iUserWeaponfps[id-1] )
	{
		g_fShotedAtfps[id-1] = -1.0;
		g_iUserWeaponfps[id-1] = iCurWeapon;
		s_iUserOldAmmo[id-1] = iNewClip;
		s_iBurstMode[id-1] = 0;
	}
	else if( s_iUserOldAmmo[id-1]>iNewClip )
	{
		if( iCurWeapon==CSW_GLOCK18 )
		{
			if( !is_user_glock_in_burst(id) || s_iBurstMode[id]>=3 )
			{
				g_fShotedAtfps[id-1] = get_gametime();
				s_iUserOldAmmo[id-1] = iNewClip;
				s_iBurstMode[id-1] = 0;
			}
			else
				s_iBurstMode[id-1]++;
		}
		else
		{
			g_fShotedAtfps[id-1] = get_gametime();
			s_iUserOldAmmo[id-1] = iNewClip;
			s_iBurstMode[id-1] = 0;
		}
	}
	else
	{
		s_iUserOldAmmo[id-1] = iNewClip;
		s_iBurstMode[id-1] = 0;
	}
}

is_user_glock_in_burst(id)
{
	if(!is_user_connected(id)) return 0;

	new iWeaponId, Float:fUserOrigin[3], Float:fWeaponOrigin[3];
	pev(id, pev_origin, fUserOrigin);
	
	while( (iWeaponId=engfunc(EngFunc_FindEntityByString, iWeaponId, "classname", "weapon_glock18")) )
	{
		if( id==pev(iWeaponId, pev_owner) )
		{
			pev(iWeaponId, pev_origin, fWeaponOrigin);
			if(fUserOrigin[0]==fWeaponOrigin[0] || fUserOrigin[1]==fWeaponOrigin[1] || fUserOrigin[2]==fWeaponOrigin[2])
			{
				return cs_get_weapon_burst(iWeaponId);
			}
		}
	}
	
	return 0;
}
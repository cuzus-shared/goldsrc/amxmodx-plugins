// FilesCheck

stock const FilesCheck[200][64];
new filescheckconfig[85];
new filescheksnum;
new filescheckload;

public LoadFilesCheck(cfgfilescheck[])
{
	new line = 0;
	new textsize = 0;
	new text[64];

	if (file_exists(cfgfilescheck))
	{
		while(read_file(cfgfilescheck,line,text,63,textsize))
		{
			if( textsize && (!equal( text, "//", 2 )) && (filescheksnum<sizeof(FilesCheck)) ){
				format(FilesCheck[filescheksnum],63,"%s",text);
				if(equali(text, ".mdl"))
				{
					force_unmodified(force_model_samebounds, {0,0,0}, {0,0,0}, text);
				}
				else{
					force_unmodified(force_exactfile, {0,0,0}, {0,0,0}, text);
				}
				filescheksnum++;
			}
			line++;
		}
		
		filescheckload = filescheksnum;
	}
	else{
		filescheckload = -1;
	}

 return PLUGIN_HANDLED;
}
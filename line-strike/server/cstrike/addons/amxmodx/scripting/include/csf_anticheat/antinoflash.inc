new Float:g_flashed_until[33];
new g_msg_screen_fade;

public event_flashed_noflash(id){

	if ((!id) || !is_user_connected(id) ) {
		return PLUGIN_CONTINUE;
	}
	g_flashed_until[id] = read_data(2)/4096.0 + get_gametime();

	return PLUGIN_CONTINUE; 
}

public fw_addtofullpack_noflash(es, e, ent, host, flags, player, set){
	
	//Bots and dead players get all the info
	if (!gUserParam[host][ALIVE] || gUserParam[host][BOT]) {
		return FMRES_IGNORED;
	}

	if(player)
	{

		if(!gUserParam[ent][ALIVE] || ent == host) {
			return FMRES_IGNORED;
		}
	} else {
		if(pev_valid(ent))
		{

			new Classname[33];
			pev(ent, pev_classname, Classname,32);
			new is_grenade = equal(Classname,"grenade");
	
			if(!is_grenade || pev(ent, pev_owner)==host)
				return FMRES_IGNORED;
	
		} else {
			return FMRES_IGNORED;
		}
	}

	if(get_gametime() < g_flashed_until[host])
	{
		forward_return(FMV_CELL, 0);
		return FMRES_SUPERCEDE;
	}
	
	return FMRES_IGNORED;

}

public remove_flash(id){
		
	message_begin(MSG_ONE, g_msg_screen_fade, {0, 0, 0}, id);
	write_short(0);
	write_short(0);
	write_short(0);
	write_byte(0);
	write_byte(0);
	write_byte(0);
	write_byte(0);
	message_end();	

}

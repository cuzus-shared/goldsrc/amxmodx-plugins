// CheatSay
new g_bansaycheat[MAXPLAYERS+1];
stock const CheatReports[256][128];
stock const CheatReports_Number[256][4];

new cv_cheatsay_blocksay;


public Load_CheatSay_Base()
{
	new antisayconfig[64];
	format(antisayconfig, 63, "%s/csf_anticheat/csf_ac_cheatsay.cfg", g_configsdir);
	
	new line = 0;
	new textsize = 0;
	new text[128];
	new tempstr[128];
	new i = 0;
	
	if (file_exists(antisayconfig))
	{
		while(read_file(antisayconfig,line,text,255,textsize))
		{
			line++;
			if( textsize && (!equal( text, "//", 2 )) && (i<sizeof(CheatReports)) ){
				i++;
				format(tempstr,128,"%s",text);
				CheatReports[i]=tempstr;
				format(CheatReports_Number[i],3,"%d",line);
			}
		}
		server_print("^n^t[CSF-AC] CheatSay * %L",LANG_SERVER,"CHEATSAY_LOAD", i);
	}
	else{
		server_print("^n^t[CSF-AC] CheatSay * %L",LANG_SERVER,"CHEATSAY_CONFIG_NFOUND");
	}
}


public Ban_CheatSay(id, said[],numlist[])
{
	++g_bansaycheat[id];
	if(g_bansaycheat[id] > 1) return PLUGIN_CONTINUE;

	new REASON[64];
	format(REASON,63,"[CSF-AC] %L [#%s]", id,"CHEATSAY_REASON",numlist);
	write_log(12,id,said);
	punish_player(id,"CHEATSAY",REASON);

	switch(gSettings[CHEATSAY][BANSAY]){
		case 1: client_print(0, print_chat, "[CSF-AC] %L", id, "CHEATSAY_PUNISH", gUserParam[id][NAME]);
		case 2: show_hud_message(0,0,10,gUserParam[id][NAME],"0");
	}
 return PLUGIN_CONTINUE;
}

public CheckCheatReport(id)
{
	new said[200];
	new i, j;
	read_args(said, 199);
	for(i = 0 ; i < sizeof (CheatReports) ; i++)
	{
 		if(containi(said, CheatReports[i][j]) != -1)
		{
			Ban_CheatSay(id, said,CheatReports_Number[i]);
			if(cv_cheatsay_blocksay == 1){
				return PLUGIN_HANDLED;
			}
		}
	}
	return PLUGIN_CONTINUE;
} 

#define SOFTWARE 	"gl_clear"
#define DIRECT3D 	"r_detailtextures"

// Если есть gl_clear +  есть r_detailtextures = opengl32
// Если есть gl_clear +  нетy r_detailtextures = d3d
// Если нету gl_clear +  нету r_detailtextures = software


// 
new cv_video_renderer;
new cv_video_renderer_detect;
new cv_video_renderer_opengl32;
new cv_video_renderer_d3d;
new cv_video_renderer_software;

enum {
	software = 0,
	direct3d,
	opengl32
}


public check_video_renderer_detect(id)
{
		switch(cv_video_renderer_detect){
			case 1: set_task(3.0, "video_renderer_newdetect", id);
			case 2: set_task(5.0, "myactest_video_renderer", id);
			case 3: {
				if(gUserParam[id][PROTOCOL]){
					switch(gUserParam[id][PROTOCOL]){
						case 47: set_task(5.0, "myactest_video_renderer", id);
						case 48: set_task(3.0, "video_renderer_newdetect", id);
					}
				}else{
					server_print("^t[CSF-AC] %L",id,"CSF_DPROTO");
					set_task(5.0, "myactest_video_renderer", id);
				}
			}
		}

 return PLUGIN_HANDLED;
}

public video_renderer_newdetect(id)
{

	query_client_cvar(id, "gl_clear", "result_video_renderer_gl"); 
	query_client_cvar(id, "r_detailtextures", "result_video_renderer_r"); 
   return PLUGIN_HANDLED;

}

public result_video_renderer_gl(id, const cvargl[], const valuegl[]) 
{
	if(equali(valuegl, "Bad CVAR request")){
   		return PLUGIN_HANDLED;
	}
	gUserParam[id][VIDEO]+=1;
	return PLUGIN_HANDLED;

}

public result_video_renderer_r(id, const cvarr[], const valuer[]) 
{

	if(equali(valuer, "Bad CVAR request")){
		check_video_renderer(id);
   		return PLUGIN_HANDLED;
	}

	gUserParam[id][VIDEO]+=1;
	check_video_renderer(id);

   return PLUGIN_HANDLED;

}


public check_video_renderer(id)
{

	new REASON[91],REASON2[91],continues=0;
	if(cv_video_renderer_opengl32 && cv_video_renderer_d3d && !cv_video_renderer_software && (gUserParam[id][VIDEO] != opengl32 && gUserParam[id][VIDEO] != direct3d))
	{
		format(REASON,90,"[CSF-AC] %L",id, "VIDEO_REASON_2","OpenGL32","Direct3D");
	}
	else if(cv_video_renderer_opengl32 && !cv_video_renderer_d3d && cv_video_renderer_software && (gUserParam[id][VIDEO] != software && gUserParam[id][VIDEO] != opengl32))
	{
		format(REASON,90,"[CSF-AC] %L",id, "VIDEO_REASON_2","OpenGL32","Software");
	}
	else if(!cv_video_renderer_opengl32 && cv_video_renderer_d3d && cv_video_renderer_software && (gUserParam[id][VIDEO] != direct3d && gUserParam[id][VIDEO] != software))
	{
		format(REASON,90,"[CSF-AC] %L",id, "VIDEO_REASON_2","Direct3D","Software");
	}
	else if(cv_video_renderer_opengl32 && !cv_video_renderer_d3d && !cv_video_renderer_software && (gUserParam[id][VIDEO] != opengl32) && (gUserParam[id][VIDEO] == direct3d || gUserParam[id][VIDEO] == software))
	{
		format(REASON,90,"[CSF-AC] %L",id, "VIDEO_REASON_1","OpenGL32");
	}
	else if(!cv_video_renderer_opengl32 && cv_video_renderer_d3d && !cv_video_renderer_software && (gUserParam[id][VIDEO] != direct3d) && (gUserParam[id][VIDEO] == software || gUserParam[id][VIDEO] == opengl32))
	{
		format(REASON,90,"[CSF-AC] %L",id, "VIDEO_REASON_1","Direct3D");
	}
	else if(!cv_video_renderer_opengl32 && !cv_video_renderer_d3d && cv_video_renderer_software && (gUserParam[id][VIDEO] == software) && (gUserParam[id][VIDEO] == opengl32 || gUserParam[id][VIDEO] == direct3d))
	{
		format(REASON,90,"[CSF-AC] %L",id, "VIDEO_REASON_1","Software");
	}else{
		continues=1;
	}


	if(!continues){

		format(REASON2,90,"[CSF-AC] %L",id,"VIDEO_REASON");
		client_print(id,print_console,"%s",REASON2);
		client_print(id,print_console,"%s",REASON);
		server_cmd("kick #%d ^"%s^"",  get_user_userid(id), REASON2) ;

	}


 return PLUGIN_HANDLED;
}

// BhopDetect

new detectcmd[32];
new bhop_warning[MAXPLAYERS+1];
new cv_bhop;
new cv_bhop_warn;
new cv_bhop_warnsay;

public bhop_fm_playerprethink(id) {
    if(gUserParam[id][ALIVE] && (gUserParam[id][PROVIDER] == 1)) {
        if(!(pev(id,pev_flags)&FL_ONGROUND) && (!(pev(id,pev_button)&IN_JUMP) || pev(id,pev_oldbuttons)&IN_JUMP)) {
            client_cmd(id, ";alias _special %s",detectcmd);
		}
    }
    return FMRES_IGNORED;
}

public bhop_detected(id) {
    if(!(pev(id,pev_flags)&FL_ONGROUND) && (!(pev(id,pev_button)&IN_JUMP) || pev(id,pev_oldbuttons)&IN_JUMP)) {

		if(gSettings[BHOP][BANTYPE] == -1){
			return PLUGIN_HANDLED;
		}

		bhop_warning[id]++;

		if(bhop_warning[id] > cv_bhop_warn){

			new REASON[64];
			format(REASON,63,"[CSF-AC] %L", id,"BUNNYHOP_REASON");
			write_log(8,id,"0");
			punish_player(id,"BHOP",REASON);
		
			switch(gSettings[BHOP][BANSAY]){
				case 1: client_print(0,print_chat,"[CSF-AC] %L", id, "BUNNYHOP_PUNISH", gUserParam[id][NAME]);
				case 2: show_hud_message(0,0,7,gUserParam[id][NAME],"0");
			}

		}else{

			client_cmd(id, "spk fvox/buzz.wav");

			switch(cv_bhop_warnsay){

				case 1: client_print(id,print_chat,"[CSF-AC] %L", id, "BUNNYHOP_WARN", bhop_warning[id], cv_bhop_warn);
				case 2: show_hud_message(id,1,8,bhop_warning[id],"0");
			}
		}
    }
    return PLUGIN_HANDLED;
}
//CheatNames

stock const CheatNames[150][32];

new cv_cheatnames;

public CheckCheatNames(id)
{
	new i, j, g_ban,CheatNameClear[32];
	g_ban = 0;
		for(i = 0 ; i < sizeof (CheatNames) ; i++)
		{
			copy(CheatNameClear,31,CheatNames[i][j]);
			replace(CheatNameClear,31,"%","");

 			if( (equal(CheatNames[i][j],"%",1) && (containi(gUserParam[id][NAME],CheatNameClear) != -1) ) || (equal(gUserParam[id][NAME], CheatNameClear))  )
			{
				++g_ban;

				if(g_ban>1) return PLUGIN_CONTINUE;

				new REASON[64];
				format(REASON,63,"[CSF-AC] %L", id,"CHEATNAMES_REASON");
				write_log(7,id,CheatNameClear);
				punish_player(id,"CHEATNAMES",REASON);

				switch(gSettings[CHEATNAMES][BANSAY]){
					case 1: client_print(0,print_chat,"[CSF-AC] %L",id, "CHEATNAMES_PUNISH", gUserParam[id][NAME]);
					case 2: show_hud_message(0,0,9,gUserParam[id][NAME],"0");
				}
			}
		}
	return PLUGIN_CONTINUE;
} 

public EnableCheatNames()
{
	new cheatnamesconfig[64];
	format(cheatnamesconfig, 63, "%s/csf_anticheat/csf_ac_cheatnames.cfg", g_configsdir);
	
	new line = 0;
	new textsize = 0;
	new text[32];
	new tempstr[32];
	new i = 0;

	if (file_exists(cheatnamesconfig))
	{
		while(read_file(cheatnamesconfig,line,text,31,textsize))
		{
			if( textsize && (!equal( text, "//", 2 )) && (i<sizeof(CheatNames)) ){
				format(tempstr,31,"%s",text);
				CheatNames[i++]=tempstr;
			}
			line++;
		}
		server_print("^n^t[CSF-AC] CheatNames * %L",LANG_SERVER,"CHEATNAMES_LOAD", i);
	}else{
		server_print("^n^t[CSF-AC] CheatNames * %L",LANG_SERVER,"CHEATNAMES_CONFIG_NFOUND");
	}
}

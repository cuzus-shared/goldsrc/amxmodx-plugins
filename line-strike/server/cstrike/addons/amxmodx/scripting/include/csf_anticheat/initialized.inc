public initialized(){

	if(get_cvar_num("csf_ac_update_check"))
	{

		upd_update_bases();

	}


	if(get_cvar_num("csf_ac_spinh")){								//SpinHack


		gSettings[SPINHACK][BANTYPE] =  get_cvar_num("csf_ac_spinh_bantype");
		gSettings[SPINHACK][BANTIME] = get_cvar_num("csf_ac_spinh_bantime");
		gSettings[SPINHACK][BANSAY] = get_cvar_num("csf_ac_spinh_bansay");
		cv_spinh_maxdetect = get_cvar_num("csf_ac_spinh_maxdetect");
		cv_spinh_maxangle = get_cvar_num("csf_ac_spinh_maxangle");
		register_forward(FM_PlayerPostThink, "Fwd_PlayerPostThink");
    	set_task(1.0,"Task_CheckSpinTotal",0,_,_,"b");
	}

	if(get_cvar_num("csf_ac_ff")){								//Fastfire

		cv_ff_imprecision = get_cvar_num("csf_ac_ff_imprecision");
		gSettings[FASTFIRE][BANTYPE] =  get_cvar_num("csf_ac_ff_bantype");
		gSettings[FASTFIRE][BANTIME] = get_cvar_num("csf_ac_ff_bantime");
		gSettings[FASTFIRE][BANSAY] = get_cvar_num("csf_ac_ff_bansay");

		register_event( "CurWeapon", "FASTFIRE_Event_ShotFired",  "b" ) ;
		set_task(1.0, "FASTFIRE_checkBulletCount",0,_,_,"b");
		register_event("HLTV", "FASTFIRE_clearDetect", "a", "1=0", "2=0");
	}

	if(get_cvar_num("csf_ac_sh")){								//SpeedHack

		new wl_configsDir[128];
		format(wl_configsDir, sizeof(wl_configsDir)-1, "%s/csf_anticheat/csf_whitelist.cfg", g_configsdir);
		new echowl = whitelistload(wl_configsDir);
		gSettings[SPEEDHACK][BANTYPE] =  get_cvar_num("csf_ac_sh_bantype");
		gSettings[SPEEDHACK][BANTIME] = get_cvar_num("csf_ac_sh_bantime");
		gSettings[SPEEDHACK][BANSAY] = get_cvar_num("csf_ac_sh_bansay");
		cv_sh_warn = get_cvar_num("csf_ac_sh_warn");
		cv_sh_maxoffence = get_cvar_num("csf_ac_sh_maxoffence");
		cv_sh_secdist = get_cvar_num("csf_ac_sh_secdist");
		if(echowl == 1){

			set_task(1.0, "checkSpeedHack",0,_,_,"b");
			RegisterHam( Ham_Use, "func_pushable", "func_touch_entity",0);		//Использование ящиков/кресел и т.п. для быстрого передвижения
			RegisterHam( Ham_Touch, "func_vehicle", "func_touch_entity" );		//Машина
			RegisterHam( Ham_Touch, "func_tracktrain", "func_touch_entity" );	//Управляемый поезд
			RegisterHam( Ham_Touch, "func_train", "func_touch_entity" );		//Неуправляемый поезд
			RegisterHam( Ham_Touch, "func_conveyor", "func_touch_entity" );	//Конвеер (Бегущая дорожка)
			RegisterHam( Ham_Touch, "func_rotating", "func_touch_entity" );		//Кружащаяся поверхность
			RegisterHam( Ham_Touch, "trigger_push", "func_touch_entity");		//Толкающая энтити
			RegisterHam( Ham_Touch, "trigger_teleport", "func_teleport_check");	//Телепорт
			RegisterHam( Ham_Spawn, "player", "func_respawn_check", 1);		//Респаун на сервер CSDM

		}else{
			server_print("^n^t[CSF-AC] SpeedHack * %L","SPEEDHACK_MAP_WLIST");
			server_print("^n^t[CSF-AC] SpeedHack * %L","SPEEDHACK_MAP_OFF");
		}
	

	}

	if(get_cvar_num("csf_ac_bhop")){								//BunnyHop

		cv_bhop = 1;
		gSettings[BHOP][BANTYPE] =  get_cvar_num("csf_ac_bhop_bantype");
		gSettings[BHOP][BANTIME] = get_cvar_num("csf_ac_bhop_bantime");
		gSettings[BHOP][BANSAY] = get_cvar_num("csf_ac_bhop_bansay");
		cv_bhop_warn = get_cvar_num("csf_ac_bhop_warn");
		cv_bhop_warnsay =get_cvar_num("csf_ac_bhop_warnsay");
		formatex(detectcmd,sizeof(detectcmd)-1, "csf_bhop%d%d%d",random_num(0,9),random_num(0,9),random_num(0,9));
		register_clcmd(detectcmd,"bhop_detected");
		register_forward(FM_PlayerPreThink,"bhop_fm_playerprethink",0);
	}

	if(get_cvar_num("csf_ac_dd")){								//DoubleDuck

		cv_dd = 1;

		if(get_cvar_num("csf_ac_dd_type") == 1){
    			register_forward(FM_PlayerPreThink, "FM_PlayerPreThink_Pre", 0);
		}
	
		if(get_cvar_num("csf_ac_dd_type") == 2){

			if( (g_iFakeEnt=engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "func_wall")))>0 )
			{
				set_pev(g_iFakeEnt, pev_classname,  ENTITY_NAME);
				set_pev(g_iFakeEnt, pev_solid,      SOLID_NOT);
				set_pev(g_iFakeEnt, pev_movetype,   MOVETYPE_NONE);
				set_pev(g_iFakeEnt, pev_rendermode, kRenderTransAlpha);
				set_pev(g_iFakeEnt, pev_renderamt,  0.0);
				engfunc(EngFunc_SetModel, g_iFakeEnt, ENTITY_MDL);
				register_forward(FM_AddToFullPack, "FM_AddToFullPack_Pre",  0);
				register_forward(FM_AddToFullPack, "FM_AddToFullPack_Post", 1);
			}
		}
	}

	if(get_cvar_num("csf_ac_silentplant")){							//SilentPlant

		if ( engfunc ( EngFunc_FindEntityByString, -1, "classname", "func_bomb_target" ) )
		{
			register_logevent ( "fn_triggerplanted" , 3, "1=triggered", "2=Planted_The_Bomb" );
			register_logevent ( "fn_triggerroundstart" , 2, "1=Round_Start" );
			register_forward ( FM_EmitSound, "fn_soundcheck" );
		}
	
	gb_EntReadyForUse = false;

	}

	if(get_cvar_num("csf_ac_cheatsay")){							//CheatSay

		gSettings[CHEATSAY][BANTYPE] =  get_cvar_num("csf_ac_cheatsay_bantype");
		gSettings[CHEATSAY][BANTIME] = get_cvar_num("csf_ac_cheatsay_bantime");
		gSettings[CHEATSAY][BANSAY] = get_cvar_num("csf_ac_cheatsay_bansay");
		cv_cheatsay_blocksay = get_cvar_num("csf_ac_cheatsay_blocksay");

  		register_clcmd("say", "CheckCheatReport");
  		register_clcmd("say_team", "CheckCheatReport");
  		Load_CheatSay_Base();
	}

	if(get_cvar_num("csf_ac_spamsay")){							//SpamSay

		gSettings[SPAMSAY][BANTYPE] =  get_cvar_num("csf_ac_spamsay_bantype");
		gSettings[SPAMSAY][BANTIME] = get_cvar_num("csf_ac_spamsay_bantime");
		gSettings[SPAMSAY][BANSAY] = get_cvar_num("csf_ac_spamsay_bansay");
		cv_spamsay_blocksay = get_cvar_num("csf_ac_spamsay_blocksay");

  		register_clcmd("say", "CheckSpamReport");
  		register_clcmd("say_team", "CheckSpamReport");
  		Load_SpamSay_Base();
	}

	if(get_cvar_num("csf_ac_ns")){

		cv_ns = 1;
		gSettings[NAMESPAM][BANTYPE] =  get_cvar_num("csf_ac_ns_bantype");
		gSettings[NAMESPAM][BANTIME] = get_cvar_num("csf_ac_ns_bantime");
		gSettings[NAMESPAM][BANSAY] = get_cvar_num("csf_ac_ns_bansay");
		cv_ns_maxchanges = get_cvar_num("csf_ac_ns_maxchanges");
		cv_ns_timerdown = get_cvar_float("csf_ac_ns_timerdown");					//TimerDown for Namespam

	}

	get_cvar_string("csf_ac_mypunishment", mypunishment, 127);			//MyPunishment

	if(get_cvar_num("csf_ac_cvarguard")){							//CVARGuard

		cv_cvarguard = get_cvar_num("csf_ac_cvarguard");
		cv_cvarguard_block = get_cvar_num("csf_ac_cvarguard_lock");
		cv_cvarguard_multitest = get_cvar_num("csf_ac_cvarguard_multitest");
		register_clcmd( g_szBlockedCmd, "clcmd_blockedcmd" );

		format(cfgcvarguard, sizeof(cfgcvarguard)-1, "%s/csf_anticheat/csf_ac_cvarguard.cfg", g_configsdir);

		if (file_exists(cfgcvarguard)){
			read_config_cvarguard( cfgcvarguard );
		}else{
			server_print("^n^t[CSF-AC] CVAR Guard * %L","CVARGUARD_CONFIG_NFOUND");
		}

	}

	if(get_cvar_num("csf_ac_flashbugfix")){							//FlashBugFix

		RegisterHam(Ham_Think, "grenade", "fm_FEThink_grenadeFBF", 0);
		register_forward(FM_FindEntityInSphere, "fm_FEFindEntityInSphereFBF", 0);

	}

	if(get_cvar_num("csf_ac_hebugfix")){							//HeBugFix


		RegisterHam(Ham_Think, "grenade", "FM_Think_HEFix_Pre", 0);
		register_forward(FM_FindEntityInSphere, "FM_FindEntityInSphere_Pre_HEFix", 0);


	}

	if(get_cvar_num("csf_ac_cheatnames")){						//CheatNames

		cv_cheatnames = 1;
		gSettings[CHEATNAMES][BANTYPE] =  get_cvar_num("csf_ac_cheatnames_bantype");
		gSettings[CHEATNAMES][BANTIME] = get_cvar_num("csf_ac_cheatnames_bantime");
		gSettings[CHEATNAMES][BANSAY] = get_cvar_num("csf_ac_cheatnames_bansay");

		EnableCheatNames();

	}

	if(get_cvar_num("csf_ac_cheatkey")){						//CheatKey

		cv_cheatkey = 1;
		gSettings[CHEATKEY][BANTYPE] =  get_cvar_num("csf_ac_cheatkey_bantype");
		gSettings[CHEATKEY][BANTIME] = get_cvar_num("csf_ac_cheatkey_bantime");
		gSettings[CHEATKEY][BANSAY] = get_cvar_num("csf_ac_cheatkey_bansay");
		cv_cheatkey_immun = get_cvar_num("csf_ac_cheatkey_immun");
		cv_cheatkey_warn = get_cvar_num("csf_ac_cheatkey_warn");
		cv_cheatkey_joinsay = get_cvar_num("csf_ac_cheatkey_joinsay");
		cv_cheatkey_rebind = get_cvar_float("csf_ac_cheatkey_rebind");
		get_cvar_string("csf_ac_cheatkey_flagimmun",cv_cheatkey_flagimmun,1);
		CHEATKEY_Load();

	}


	if(get_cvar_num("csf_ac_zoneguard")){						//ZoneGuard

		cv_zoneguard = 1;
		gSettings[ZONEGUARD][BANTYPE] =  get_cvar_num("csf_ac_zoneguard_bantype");
		gSettings[ZONEGUARD][BANTIME] = get_cvar_num("csf_ac_zoneguard_bantime");
		gSettings[ZONEGUARD][BANSAY] = get_cvar_num("csf_ac_zoneguard_bansay");
	
		register_menu("MainMenu", -1, "MainMenuAction", 0);
		register_menu("EditMenu", -1, "EditMenuAction", 0);
		register_menu("KillMenu", -1, "KillMenuAction", 0);

		register_clcmd("csf_ac_zoneguard_menu", "InitZoneGuard", ADMIN_RCON, " - Открыть меню редактирования запрещенных зон");

		register_event("HLTV", "Event_FreezeTime", "a", "1=0", "2=0");
		register_logevent("Event_RoundStart", 2, "1=Round_Start");
		register_logevent("Event_RoundEnd", 2, "1=Round_End");
		register_forward(FM_Touch, "fw_touch");

		new zonezgfile[200];

		format(zonezgfile, sizeof(zonezgfile)-1, "%s/csf_anticheat/csf_zone_guard/%s.zg", g_configsdir,g_mapname);
	
		if (!file_exists(zonezgfile))
		{
			server_print("^n^t[CSF-AC] ZoneGuard * %L",LANG_SERVER,"ZONEGUARD_NFOUND_ON_MAP", g_mapname);
			server_print("^n^t[CSF-AC] ZoneGuard * %L",LANG_SERVER,"ZONEGUARD_OFF_ON_MAP");

		}else{
			LoadZG(zonezgfile);
		}
	}

	if(get_cvar_num("csf_ac_fastps")){						//FastPistolShots

		cv_fastps = 1;
		cv_fastps_sd = get_cvar_float("csf_ac_fastps_sd");
		cv_fastps_ed = get_cvar_float("csf_ac_fastps_ed");
		register_forward(FM_UpdateClientData, "FM_UpdateClientData_Post_FPS", 1);
		register_forward(FM_PlayerPreThink,   "FM_PlayerPreThink_Post_FPS",   1);
		register_event("CurWeapon", "Event_CurWeapon_FPS", "be", "1=1");
		register_event("DeathMsg",  "Event_DeathMsg_FPS",  "a");

	}


	if(get_cvar_num("csf_ac_cdhack")){

		cv_cdhack = 1;
		gSettings[CDHACK][BANTYPE] =  get_cvar_num("csf_ac_cdhack_bantype");
		gSettings[CDHACK][BANTIME] = get_cvar_num("csf_ac_cdhack_bantime");
		gSettings[CDHACK][BANSAY] = get_cvar_num("csf_ac_cdhack_bansay");
		cv_cdhack_detect = get_cvar_num("csf_ac_cdhack_detect");
		cv_cdhack_myacdetect = get_cvar_num("csf_ac_cdhack_myacdetect");
		register_clcmd("cd_version","clcmd_none");
		register_clcmd("cd_choke","clcmd_none");
		register_clcmd("cd_build","clcmd_none");
	}

	if(get_cvar_num("csf_ac_ffx")){

		cv_ffx = 1;
		gSettings[FFX][BANTYPE] =  get_cvar_num("csf_ac_ffx_bantype");
		gSettings[FFX][BANTIME] = get_cvar_num("csf_ac_ffx_bantime");
		gSettings[FFX][BANSAY] = get_cvar_num("csf_ac_ffx_bansay");
		cv_ffx_detect = get_cvar_num("csf_ac_ffx_detect");
		register_clcmd("ffxv","clcmd_none");


	}

	if(get_cvar_num("csf_ac_cheatlist")){

		cv_cheatlist = 1;
		gSettings[CHEATLIST][BANTYPE] =  get_cvar_num("csf_ac_cheatlist_bantype");
		gSettings[CHEATLIST][BANTIME] = get_cvar_num("csf_ac_cheatlist_bantime");
		gSettings[CHEATLIST][BANSAY] = get_cvar_num("csf_ac_cheatlist_bansay");

	}

	if(get_cvar_num("csf_ac_filescheck")){
		if(filescheckload != -1){
			server_print("^n^t[CSF-AC] FilesCheck * %L",LANG_SERVER,"FILECHECK_LOAD", filescheckload);
		}else{
			server_print("^n^t[CSF-AC] FilesCheck * %L",LANG_SERVER,"FILECHECK_CONFIG_NFOUND");
		}
	}

	if(get_cvar_num("csf_ac_cmdflood")){

		cv_cmdflood = 1;
		cv_cmdflood_checktime = get_cvar_float("csf_ac_cmdflood_checktime");
		cv_cmdflood_checktype = get_cvar_num("csf_ac_cmdflood_checktype");
		gSettings[CMDFLOOD][BANTYPE] =  get_cvar_num("csf_ac_cmdflood_bantype");
		gSettings[CMDFLOOD][BANTIME] = get_cvar_num("csf_ac_cmdflood_bantime");
		gSettings[CMDFLOOD][BANSAY] = get_cvar_num("csf_ac_cmdflood_bansay");

		if(cv_cmdflood_checktype==2){
			Load_CMDFLOOD_Base();
		}
	}


	if(get_cvar_num("csf_ac_aliascheck")){

		cv_aliascheck = 1;
		gSettings[ALIASCHECK][BANTYPE] =  get_cvar_num("csf_ac_aliascheck_bantype");
		gSettings[ALIASCHECK][BANTIME] = get_cvar_num("csf_ac_aliascheck_bantime");
		gSettings[ALIASCHECK][BANSAY] = get_cvar_num("csf_ac_aliascheck_bansay");
		register_clcmd("csf_aliascheck","clcmd_none");
		LOAD_ALIAS_BASE();
	}

	if(get_cvar_num("csf_ac_cmdsend")){

		cv_cmdsend = 1;
		cv_cmdsend_type = get_cvar_num("csf_ac_cmdsend_type");
		CMDSend_Load();

	}


	get_cvar_string("csf_ac_site", cv_site, 63);
	cv_amxbanstype = get_cvar_num("csf_ac_amxbanstype");

	cv_logtype = get_cvar_num("csf_ac_logtype");
	if(cv_logtype == 2 || cv_logtype == 3){

		new Host[64],User[64],Pass[64],Db[64];

		get_cvar_string("csf_ac_sql_host",Host,63);
		get_cvar_string("csf_ac_sql_user",User,63);
		get_cvar_string("csf_ac_sql_pass",Pass,63);
		get_cvar_string("csf_ac_sql_db",Db,63);

		g_SqlTuple = SQL_MakeDbTuple(Host,User,Pass,Db);
	}

	if(get_cvar_num("csf_ac_wallhack")){

		cv_wallhack = get_cvar_num("csf_ac_wallhack");

		switch (get_cvar_num("csf_ac_wallhack_team")){
			case 0: cv_wallhack_team=1;
			case 1: cv_wallhack_team=0;
		}

		cv_wallhack_entity = get_cvar_num("csf_ac_wallhack_entity");
		cv_wallhack_fov = 1;
		cv_wallhack_target = 1;
		cv_wallhack_smooth = 1;
		cv_wallhack_engine = 1;
		cv_wallhack_texture = 1;

		register_forward(FM_Spawn, "fw_spawn_wallhack", 1);
		register_forward(FM_AddToFullPack, 	"fw_addtofullpack_wallhack" 	, 0);
		register_forward(FM_PlayerPreThink,	"fw_prethink_wallhack"	   	, 0);
		register_forward(FM_TraceLine,		"pfw_traceline_wallhack"		, 1);
		register_forward(FM_SetView, 		"fw_setview_wallhack");
	
		RegisterHam(Ham_Spawn, "player", "fw_alive_handle_wallhack", 1);
		RegisterHam(Ham_Killed, "player", "fw_alive_handle_wallhack", 1);
		//RegisterHam(Ham_Blocked, "func_wall", "fw_stuck_wallhack")
		//RegisterHam(Ham_Blocked, "func_ladder", "fw_stuck_wallhack")

		register_event("CurWeapon", "event_active_weapon_wallhack", "be");
	
		register_message(get_user_msgid("ClCorpse"), "message_clcorpse_wallhack");

		thdl = create_tr2();
		
		for (new i=1;i<=g_MaxPlayers;i++)
		{
			add_solid_ent(i);
			g_cl_viewent[i] = i;
		}
	}


	if(get_cvar_num("csf_ac_noflash")){

		register_event("ScreenFade", "event_flashed_noflash", "b", "7=255");
		register_forward(FM_AddToFullPack, "fw_addtofullpack_noflash", 0);
		g_msg_screen_fade = get_user_msgid("ScreenFade");
	}


	if(get_cvar_num("csf_ac_video")){

		cv_video_renderer = 1;
		cv_video_renderer_detect = get_cvar_num("csf_ac_video_detect");
		cv_video_renderer_opengl32 = get_cvar_num("csf_ac_video_opengl32");
		cv_video_renderer_d3d = get_cvar_num("csf_ac_video_direct3d");
		cv_video_renderer_software = get_cvar_num("csf_ac_video_software");
		register_clcmd("gl_clear", "clcmd_none");
		register_clcmd("r_detailtextures", "clcmd_none");

	}

	get_cvar_string("csf_ac_hudpunish", hud_pwc[0],63);
	parse(hud_pwc[0], hud_pwc_arg[0][0], 5, hud_pwc_arg[0][1], 5, hud_pwc_arg[0][2], 5, hud_pwc_arg[0][3], 5,hud_pwc_arg[0][4],5,hud_pwc_arg[0][5],5,hud_pwc_arg[0][6],5,hud_pwc_arg[0][7],5,hud_pwc_arg[0][8],5,hud_pwc_arg[0][9],5,hud_pwc_arg[0][10],5);

	get_cvar_string("csf_ac_hudwarning", hud_pwc[1],63);
	parse(hud_pwc[1], hud_pwc_arg[1][0], 5, hud_pwc_arg[1][1], 5, hud_pwc_arg[1][2], 5, hud_pwc_arg[1][3], 5,hud_pwc_arg[1][4],5,hud_pwc_arg[1][5],5,hud_pwc_arg[1][6],5,hud_pwc_arg[1][7],5,hud_pwc_arg[1][8],5,hud_pwc_arg[1][9],5,hud_pwc_arg[1][10],5);

	get_cvar_string("csf_ac_cheatkey_hud", hud_pwc[2],63);
	parse(hud_pwc[2], hud_pwc_arg[2][0], 5, hud_pwc_arg[2][1], 5, hud_pwc_arg[2][2], 5, hud_pwc_arg[2][3], 5,hud_pwc_arg[2][4],5,hud_pwc_arg[2][5],5,hud_pwc_arg[2][6],5,hud_pwc_arg[2][7],5,hud_pwc_arg[2][8],5,hud_pwc_arg[2][9],5,hud_pwc_arg[2][10],5);


    server_print ("^n^t[CSF-AC] Initialized version %s^n", VERSION );

	return PLUGIN_HANDLED;
}

new Alias_base[1024][64];
new Alias_base_all;
new Alias_player_check[MAXPLAYERS+1];
new bool:Alias_player_checked[MAXPLAYERS+1];
new alias_arg[MAXPLAYERS+1][2][64];
new g_alias_player_recheck[MAXPLAYERS+1];
new cv_aliascheck;
new g_banaliascheck[MAXPLAYERS+1];
new g_checkantifreeze[MAXPLAYERS+1];


public LOAD_ALIAS_BASE()
{
	new alias_base_config[64], g_configsdir[64];
	get_configsdir(g_configsdir, sizeof(g_configsdir));
	format(alias_base_config, 63, "%s/csf_anticheat/csf_ac_alias.cfg", g_configsdir);
	new line = 0;
	new textsize = 0;
	new text[64];
	new tempstr[64];
	new i = 0;
	if (file_exists(alias_base_config))
	{
		while(read_file(alias_base_config,line,text,63,textsize))
		{
			line++;
			if( textsize && (!equal( text, "//", 2 )) && (!equal(text,";",1)) && (!equal( text, "[", 1 )) && (i<sizeof(Alias_base)))
			{
				i++;
				format(tempstr,63,"%s",text);
				replace(tempstr,63,":"," ");
				parse(tempstr, tempstr, 63);
				Alias_base[i]=tempstr;
				register_clcmd(tempstr,"clcmd_none");
			}

		}
		server_print("^n^t[CSF-AC] AliasCheck * %L", LANG_SERVER,"ALIAS_LOAD",i);
		Alias_base_all = i;
	}
	else
	{
		server_print("^n^t[CSF-AC] AliasCheck * %L",LANG_SERVER,"AC_CONF_NFOUND");
	}
	return PLUGIN_CONTINUE;
}


public cmd_check_ALIAS(id,arg1[])
{

	if(Alias_player_checked[id])
	{
		if(equal(Alias_base[Alias_player_check[id]][0],arg1))
		{
			Alias_player_checked[id] = false;
			g_alias_player_recheck[id] = 0;
			g_checkantifreeze[id]=0;
			set_task(random_float(0.3,1.5),"ALIAS_CHECK_PLAYER",id+11035);
		}else{

			if(++g_alias_player_recheck[id]<=2){

				Alias_player_checked[id] = true;
				client_cmd(id,"; %s",Alias_base[Alias_player_check[id]][0]);

			}else{

				if(++g_checkantifreeze[id]==1)
				{ 
					client_cmd(id,"; csf_aliascheck");

				}else if(equal("csf_aliascheck",arg1)){

					if(++g_banaliascheck[id] > 1) {
						return PLUGIN_CONTINUE;
					}
					
					new REASON[64];
					format(REASON,63,"[CSF-AC] %L [#%d]", id,"ALIASCHECK_REASON",Alias_player_check[id]);
					write_log(14,id,Alias_base[Alias_player_check[id]][0]);
					punish_player(id,"ALIASCHECK",REASON);

					switch(gSettings[ALIASCHECK][BANSAY]){

						case 1: client_print(0,print_chat,"[CSF-AC] %L", id, "ALIASCHECK_PUNISH", gUserParam[id][NAME]);
						case 2: show_hud_message(0,0,14,gUserParam[id][NAME],"0");
					}
				}
			}
		}
		format(alias_arg[id][0],63,"%s",arg1);
	}

 return PLUGIN_CONTINUE;
}


public PRETEST_ALIASDETECT(id)
{
	id -= 33621;

	if(!is_user_connected(id)) return PLUGIN_HANDLED;

	if (gUserParam[id][TEAM] != 0) {

			//server_print("^t[csf-ac] checked, team %d", id)
   			set_task(5.0, "ALIAS_CHECK_PLAYER", id+11035);
	}else{
		//server_print("^t[csf-ac] rescan_team %d", id)

		set_task(5.0, "PRETEST_ALIASDETECT",id+33621);

	}

 return PLUGIN_HANDLED;
}


public ALIAS_CHECK_PLAYER(id)
{
	id-=11035;
	//server_print("%d - alias_check_player = 0",id);
	//server_print("%d / %d",Alias_player_check[id]+1, Alias_base_all)
	if(Alias_player_check[id] < Alias_base_all)
	{

		Alias_player_check[id]++;
		Alias_player_checked[id] = true;
		client_cmd(id,"; %s",Alias_base[Alias_player_check[id]][0]);
		//server_print("alias_base = %s",Alias_base[Alias_player_check[id]][0])

	}
	else
	{
		Alias_player_checked[id] = false;
		remove_task(id+11035);
	}

}

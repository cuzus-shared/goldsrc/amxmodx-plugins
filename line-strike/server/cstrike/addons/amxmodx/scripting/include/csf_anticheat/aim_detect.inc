/*
Created Nice Aim Detector by sector for www.chatbox.do.am
Web Help - www.chatbox.do.am

All ingenious is simple - Nice. Made in Russia.
*/

#include <amxmodx>
#include <fakemeta>
#include <hamsandwich>

#define nPlugin "Nice Aim Detector"

#define nHead "Head aim hack"
#define nBody "Body aim hack"

new nAD_Head[33]
new nAD_Chest[33]
new nAD_Stomach[33]
new nAD_Larm[33]
new nAD_Rarm[33]
new nAD_Lleg[33]
new nAD_Rleg[33]

new nAD_Head_Warns[33]
new nAD_Hits_Warns[33]

new nTimes[33]
new nReason[64]

new nHitsGroup = 75

new nAD_Flag_Cvar
new nAD_Head_Cvar
new nAD_Hits_Cvar
new nAD_Type_Cvar
new nAD_Time_Cvar
new nAD_Warns_Cvar
new nAD_Bantime_Cvar
new nAD_Punishment_Cvar

public plugin_init() 
{
	register_plugin(nPlugin, "1.8", "sector")
	
	register_dictionary("nad.txt")
	
	server_cmd("exec addons/amxmodx/configs/nad/nad.cfg")
	
	RegisterHam(Ham_Spawn, "player", "nice_aim_detector_spawn", 1)
	RegisterHam(Ham_Killed, "player", "nice_aim_detector_head", 1)
	RegisterHam(Ham_TraceAttack, "player", "nice_aim_detector_hits", 1)
	
	nAD_Flag_Cvar = register_cvar("nad_flag", "")
	nAD_Head_Cvar = register_cvar("nad_head", "")
	nAD_Hits_Cvar = register_cvar("nad_hits", "")
	nAD_Type_Cvar = register_cvar("nad_type", "")
	nAD_Time_Cvar = register_cvar("nad_time", "")
	nAD_Warns_Cvar = register_cvar("nad_warns", "")
	nAD_Bantime_Cvar = register_cvar("nad_bantime", "")
	nAD_Punishment_Cvar = register_cvar("nad_punishment", "")
}

public client_putinserver(i)
{
	nAD_Head_Warns[i] = 0
	nAD_Hits_Warns[i] = 0
	
	nTimes[i] = 0
	
	set_task(60.0, "nice_aim_detector_time", i, _, _, "b")
}

public client_disconnect(i)
{
	if(task_exists(i))
	{
		remove_task(i)
	}
}

public nice_aim_detector_spawn(i)
{
	new nType = get_pcvar_num(nAD_Type_Cvar)
	
	switch(nType)
	{
		case 0:
		{
			nChatPrint(i, "^1[^4%s^1] %L", nPlugin, LANG_PLAYER, "NAD_TEST", nAD_Head[i], nAD_Chest[i], nAD_Stomach[i], nAD_Larm[i], nAD_Rarm[i], nAD_Lleg[i], nAD_Rleg[i])
		}
		case 1:
		{
			new nHeads = get_pcvar_num(nAD_Head_Cvar)
			new nHits = get_pcvar_num(nAD_Hits_Cvar)
			new nWarns = get_pcvar_num(nAD_Warns_Cvar)
			
			new nLen[33]
			
			get_pcvar_string(nAD_Flag_Cvar, nLen, 32)
			
			if(strlen(nLen) && get_user_flags(i) & read_flags(nLen))
			{
				return HAM_HANDLED
			}
			else
			if(nAD_Head[i] >= nHeads)
			{
				nAD_Head_Warns[i]++
				
				if(nAD_Head_Warns[i] == (nWarns - 1))
				{
					nChatPrint(i, "^1[^4%s^1] %L", nPlugin, LANG_PLAYER, "NAD_WARNS", nAD_Head_Warns[i], nWarns, nHead)
				}
				else
				if(nAD_Head_Warns[i] == nWarns)
				{
					nReason = nHead
					
					nice_aim_detector_punishment(i, nReason)
				}
			}
			else
			if((nAD_Chest[i] >= nHits) || (nAD_Stomach[i]  >= nHits) || (nAD_Larm[i]  >= nHits) || (nAD_Rarm[i]  >= nHits) || (nAD_Lleg[i]  >= nHits) || (nAD_Rleg[i]  >= nHits))
			{
				nAD_Hits_Warns[i]++
				
				if(nAD_Hits_Warns[i] == (nWarns - 1))
				{
					nChatPrint(i, "^1[^4%s^1] %L", nPlugin, LANG_PLAYER, "NAD_WARNS", nAD_Hits_Warns[i], nWarns, nBody)
				}
				else
				if(nAD_Hits_Warns[i] == nWarns)
				{
					nReason = nBody
					
					nice_aim_detector_punishment(i, nReason)
				}
			}
		}
	}
	
	nAD_Head[i] = 0
	nAD_Chest[i] = 0
	nAD_Stomach[i] = 0
	nAD_Larm[i] = 0
	nAD_Rarm[i] = 0
	nAD_Lleg[i] = 0
	nAD_Rleg[i] = 0
	
	return HAM_SUPERCEDE
}

public nice_aim_detector_head(id, i, shouldgib)
{
	if(is_user_alive(i))
	{
		if(1 <= i <= get_maxplayers() && get_pdata_int(id, nHitsGroup, 5) == HIT_HEAD)
		{
			nAD_Head[i]++
		}
	}
}

public nice_aim_detector_hits(id, i, shouldgib)
{
	if(is_user_alive(i))
	{
		if(1 <= i <= get_maxplayers() && get_pdata_int(id, nHitsGroup, 5) == HIT_CHEST)
		{
			nAD_Chest[i]++
		}
		else
		if(1 <= i <= get_maxplayers() && get_pdata_int(id, nHitsGroup, 5) == HIT_STOMACH)
		{
			nAD_Stomach[i]++
		}
		else
		if(1 <= i <= get_maxplayers() && get_pdata_int(id, nHitsGroup, 5) == HIT_LEFTARM)
		{
			nAD_Larm[i]++
		}
		else
		if(1 <= i <= get_maxplayers() && get_pdata_int(id, nHitsGroup, 5) == HIT_RIGHTARM)
		{
			nAD_Rarm[i]++
		}
		else
		if(1 <= i <= get_maxplayers() && get_pdata_int(id, nHitsGroup, 5) == HIT_LEFTLEG)
		{
			nAD_Lleg[i]++
		}
		else
		if(1 <= i <= get_maxplayers() && get_pdata_int(id, nHitsGroup, 5) == HIT_RIGHTLEG)
		{
			nAD_Rleg[i]++
		}
	}
}

public nice_aim_detector_time(i)
{
	new nTime = get_pcvar_num(nAD_Time_Cvar)
	
	nTimes[i]++
	
	if(nTimes[i] == nTime)
	{
		nAD_Head_Warns[i] = 0
		nAD_Hits_Warns[i] = 0
		
		nTimes[i] = 0
	}
}

public nice_aim_detector_punishment(i, nReason[])
{
	new nPunishment = get_pcvar_num(nAD_Punishment_Cvar)
	new nBantime = get_pcvar_num(nAD_Bantime_Cvar)
	
	new nAid[35], nName[33], nIP[33]
	
	get_user_authid(i, nAid, 34)
	get_user_name(i, nName, 32)
	get_user_ip(i, nIP, 32, 1)
	
	new nUid = get_user_userid(i)
	
	nChatPrint(0, "^1[^4%s^1] %L", nPlugin, LANG_PLAYER, "NAD_PUNISH", nName, nReason)
	
	client_cmd(i, "snapshot")
	
	switch(nPunishment)
	{
		case 0:server_cmd("kick #%d ^"[%s] %s.^"", nUid, nPlugin, nReason)
		case 1:server_cmd("addip %d %s;writeip", nBantime, nIP)
		case 2:
		{
			if(containi(nAid, "STEAM_0:")!=-1)
			{
				server_cmd("amx_ban #%d %d ^"[%s] %s.^"", nUid, nBantime, nPlugin, nReason)
			}
			else
			{
				server_cmd("amx_banip #%d %d ^"[%s] %s.^"", nUid, nBantime, nPlugin, nReason)
			}
		}
		case 3:server_cmd("amx_ban %d #%d ^"[%s] %s.^"", nBantime, nUid, nPlugin, nReason)
		case 4:server_cmd("amx_ban #%d %d ^"[%s] %s.^"", nUid, nBantime, nPlugin, nReason)
		case 5:server_cmd("amx_superban #%d %d ^"[%s] %s.^"", nUid, nBantime, nPlugin, nReason)
	}
	
	log_to_file("addons/amxmodx/configs/nad/nad.txt", "   [%s]   '%s'  -  <%s>", nPlugin, nName, nReason)
}

stock nChatPrint(const id, const input[], any:...)
{
	 new nCount = 1, nNum[32]
	 
	 static nMsgs[191]
	 
	 vformat(nMsgs, 190, input, 3)
	 
	 replace_all(nMsgs, 190, "!g", "^4")
	 replace_all(nMsgs, 190, "!n", "^1")
	 replace_all(nMsgs, 190, "!t", "^3")

	 
	 if(id) nNum[0] = id; else get_players(nNum, nCount, "ch")
	 {
	 	for(new i = 0; i < nCount; i++)
		{
			if(is_user_connected(nNum[i]))
			{
				message_begin(MSG_ONE_UNRELIABLE, get_user_msgid("SayText"), _, nNum[i])
				
				write_byte(nNum[i])
				
				write_string(nMsgs)
				
				message_end()
			}
		}
	}
}

#pragma tabsize 0
enum { cvar, value };

#define MAXPLAYERS 32
//
#define PLAYER_LEFT 128
#define PLAYER_RIGHT 256
//
#define HACKS 8


// Huds
new hud_pwc[3][64];			//punish, warning,cheatkey
new hud_pwc_arg[3][11][6];		//punish, warning,cheatkey

//
new p_dproto_data[2];

//
new cv_site[64];
new cv_amxbanstype;
//
new g_MaxPlayers;
new g_mapname[32];
new g_configsdir[64];
new g_mainconfig[128];
//
new mypunishment[128];
new commandpunishment[256];

//
//Teams
#define	CS_TEAM_UNASSIGNED 0
#define	CS_TEAM_T 1
#define	CS_TEAM_CT 2
#define	CS_TEAM_SPECTATOR 3


//

enum _:Module_list {

	BHOP = 0,
	CDHACK,
	FFX,
	CHEATKEY,
	CHEATLIST,
	CHEATSAY,
	CHEATNAMES,
	CMDFLOOD,
	FASTFIRE,
	NAMESPAM,
	SPEEDHACK,
	SPINHACK,
	ZONEGUARD,
	ALIASCHECK,
	SPAMSAY,
	ANTIKICK

}

enum _:Parameter_list {

	BANTYPE = 0,
	BANTIME,
	BANSAY	

}

enum _:Player_parameters {

	NAME[32],
	IP[17],
	AUTHID[25],
	PROVIDER,
	PROTOCOL,
	Bool:BOT,
	Bool:HLTV,
	Bool:ALIVE,
	TEAM,
	VIDEO,
	WEAPON,
	AMMO

}
new gSettings[Module_list][Parameter_list];
new gUserParam[MAXPLAYERS+1][Player_parameters];

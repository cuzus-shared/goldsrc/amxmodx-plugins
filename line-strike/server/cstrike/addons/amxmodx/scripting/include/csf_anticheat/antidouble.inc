public check_double_plugins(){

	if(get_cvar_num("csf_ac_cheatsay") == 1){
		pause("cd","anti_sayEN.amxx");
		pause("cd","anti_sayRUS.amxx");

	}

	if(get_cvar_num("csf_ac_fastfire") == 1){

		pause("cd","anti_fastfire.amxx");

	}

	if(get_cvar_num("csf_ac_spinhack") == 1){

		pause("cd","ac_spinhackdetector.amxx");

	}

	if(get_cvar_num("csf_ac_dd") == 1){

		pause("cd","antidoubleduck.amxx");
		pause("cd","antisilentrun.amxx");

	}

	if(get_cvar_num("csf_ac_flashbugfix") == 1){

		pause("cd","antiflashbug.amxx");

	}
 
	if(get_cvar_num("csf_ac_hebugfix") == 1){

		pause("cd","antihebug.amxx");

	}

	if(get_cvar_num("csf_ac_sh") == 1){

		pause("cd","anti_speedhack.amxx");

	}

	if(get_cvar_num("csf_ac_cdhack") == 1){

		pause("cd","red-anti4.amxx");
		pause("cd","anti4.amxx");
		pause("cd","Red-anti.amxx");

	}

	if(get_cvar_num("csf_ac_silentplant") == 1){

		pause("cd","antisilentplant.amxx");

	}

	if(get_cvar_num("csf_ac_cheatlist") == 1){

		pause("cd","ec.amxx");

	}

	if(get_cvar_num("csf_ac_bhop") == 1){

		pause("cd","bhop_detector.amxx");

	}

	if(get_cvar_num("csf_ac_wallhack") == 1){

		pause("cd","block_wallhack.amxx");

	}

	if(get_cvar_num("csf_ac_cvarguard") == 1){

		pause("cd","amx_cvarguard.amxx");
		pause("cd", "cvar_checker.amxx");

	}

	if(get_cvar_num("csf_ac_zoneguard") == 1){

		pause("cd","walkguard.amxx");

	}

	if(get_cvar_num("csf_ac_fastps") == 1){

		pause("cd","antifast_pistol_shots.amxx");

	}

	if(get_cvar_num("csf_ac_cmdflood") == 1){

		pause("cd","fullupdate.amxx");
		pause("cd","fullupdate_blocker.amxx");

	}

	if(get_cvar_num("csf_ac_filescheck") == 1){

		pause("cd","Smoke_FIX.amxx");
	}


 return PLUGIN_HANDLED;
}
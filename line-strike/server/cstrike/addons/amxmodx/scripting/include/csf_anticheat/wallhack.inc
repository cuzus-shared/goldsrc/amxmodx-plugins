//WallHack Guard 4.5

new cv_wallhack;
new cv_wallhack_team;
new cv_wallhack_entity;
new cv_wallhack_fov;
new cv_wallhack_target;
new cv_wallhack_smooth;
new cv_wallhack_engine;
new cv_wallhack_texture;
new thdl;


//#define point_test
#define GENERAL_X_Y_SORROUNDING 			18.5 		// 16.0
#define CONSTANT_Z_CROUCH_UP 				31.25 		// 32.0
#define CONSTANT_Z_CROUCH_DOWN 				17.5 		// 16.0
#define CONSTANT_Z_STANDUP_UP 				34.0 		// 36.0
#define CONSTANT_Z_STANDUP_DOWN 			35.25 		// 36.0

#define GENERAL_X_Y_SORROUNDING_HALF		9.25 		// 8.0
#define GENERAL_X_Y_SORROUNDING_HALF2		12.0 		// 8.0
#define CONSTANT_Z_CROUCH_UP_HALF 			15.5 		// 16.0
#define CONSTANT_Z_CROUCH_DOWN_HALF			8.75 		// 8.0
#define CONSTANT_Z_STANDUP_UP_HALF			17.0 		// 18.0
#define CONSTANT_Z_STANDUP_DOWN_HALF		17.5 		// 18.0

#define ANGLE_COS_HEIGHT_CHECK				0.7071		// cos(45  degrees)

#define FRAME_OFFSET_CONSTANT				0.0208

new const Float:weapon_edge_point[CSW_P90+1] =
{
    0.00, // nothing
    32.8, // p228 
    0.00, // shield
    38.9, // scout
    0.00, // hegrenade
    31.2, // xm1014
    0.00, // c4
    26.0, // mac10
    32.9, // aug
    0.00, // smokegrenade
    23.5, // elite
    32.7, // fiveseven  
    27.0, // ump45
    40.0, // sg550   
    26.5, // galil
    32.6, // famas    
    38.9, // usp ( without silencer 23.5 )
    32.6, // glock     
    39.5, // awp     
    30.4, // mp5        
    30.5, // m249
    30.1, // m3    
    41.4, // m4a1 ( without silencer 32.6 )
    39.2, // tmp         
    42.2, // g3sg1      
    0.00, // flashbang
    34.1, // deagle  
    34.0, // sg552   
    24.8, // ak47       
    0.00, // knife
    25.4  // p90
};

new const Float:vec_multi_lateral[] = 
{
	GENERAL_X_Y_SORROUNDING,
	-GENERAL_X_Y_SORROUNDING,
	GENERAL_X_Y_SORROUNDING_HALF2,
	-GENERAL_X_Y_SORROUNDING_HALF
};

new const Float:vec_add_height_crouch[] =
{
	CONSTANT_Z_CROUCH_UP,
	-CONSTANT_Z_CROUCH_DOWN,
	CONSTANT_Z_CROUCH_UP_HALF,
	-CONSTANT_Z_CROUCH_DOWN_HALF
};

new const Float:vec_add_height_standup[] =
{
	CONSTANT_Z_STANDUP_UP,
	-CONSTANT_Z_STANDUP_DOWN,
	CONSTANT_Z_STANDUP_UP_HALF,
	-CONSTANT_Z_STANDUP_DOWN_HALF
};

new g_cl_weapon[MAXPLAYERS + 1];
new g_cl_viewent[MAXPLAYERS + 1];
new bs_cl_alive, bs_cl_ducking, bs_cl_connect, bs_cl_bot, bs_cl_announce;

#define add_bot_property(%1)						bs_cl_bot |= (1<<(%1 - 1))
#define del_bot_property(%1)						bs_cl_bot &= ~(1<<(%1 - 1))
#define has_bot_property(%1)						(bs_cl_bot & (1<<(%1 - 1)))
#define add_connect_property(%1)					bs_cl_connect |= (1<<(%1 - 1))
#define del_connect_property(%1)					bs_cl_connect &= ~(1<<(%1 - 1))
#define has_connect_property(%1)					(bs_cl_connect & (1<<(%1 - 1)))
#define add_duck_property(%1)						bs_cl_ducking |= (1<<(%1 - 1))
#define del_duck_property(%1)						bs_cl_ducking &= ~(1<<(%1 - 1))
#define has_duck_property(%1)						(bs_cl_ducking & (1<<(%1 - 1)))
#define add_alive_property(%1)						bs_cl_alive |= (1<<(%1 - 1))
#define del_alive_property(%1)						bs_cl_alive &= ~(1<<(%1 - 1))
#define has_alive_property(%1)						(bs_cl_alive & (1<<(%1 - 1)))
#define add_alive_property(%1)						bs_cl_alive |= (1<<(%1 - 1))
#define del_alive_property(%1)						bs_cl_alive &= ~(1<<(%1 - 1))
#define has_alive_property(%1)						(bs_cl_alive & (1<<(%1 - 1)))
#define add_announced(%1)							bs_cl_announce |= (1<<(%1 - 1))
#define del_announced(%1)							bs_cl_announce &= ~(1<<(%1 - 1))
#define has_been_announced(%1)						(bs_cl_announce & (1<<(%1 - 1)))

new bs_cl_targets[MAXPLAYERS + 1];
new bs_cl_seen_by[MAXPLAYERS + 1];
new bs_cl_smooth[MAXPLAYERS + 1] = {~0, ...}; // ~0 = -4294967295

// %0 is the player that targets, is seen by , can smooth
#define add_targeted_player(%0,%1)					bs_cl_targets[%0] |= (1<<(%1 - 1))
#define del_targeted_player(%0,%1)					bs_cl_targets[%0] &= ~(1<<(%1 - 1))
#define player_targets_user(%0,%1)					(bs_cl_targets[%0] & (1<<(%1 - 1)))
#define add_seen_by_player(%0,%1)					bs_cl_seen_by[%0] |= (1<<(%1 - 1))
#define del_seen_by_player(%0,%1)					bs_cl_seen_by[%0] &= ~(1<<(%1 - 1))
#define is_user_seen_by(%0,%1)						(bs_cl_seen_by[%0] & (1<<(%1 - 1)))
#define enable_smooth_between(%0,%1)				bs_cl_smooth[%0] |= (1<<(%1 - 1))
#define disable_smooth_between(%0,%1)				bs_cl_smooth[%0] &= ~(1<<(%1 - 1))
#define can_use_smooth_between(%0,%1)				(bs_cl_smooth[%0] & (1<<(%1 - 1)))

stock spr_bomb;
stock beampoint;

// These bitsums allow 2048 entities storage. I think that it is enough :P.
new bs_array_transp[64];				// BitSum, This is equal to 64*32 bools (good for quick search)
new bs_array_solid[64];				// BitSum, This is equal to 64*32 bools (good for quick search)

#define add_transparent_ent(%1) 	bs_array_transp[((%1 - 1) / 32)] |= (1<<((%1 - 1) % 32))
#define del_transparent_ent(%1) 	bs_array_transp[((%1 - 1) / 32)] &= ~(1<<((%1 - 1) % 32))
#define  is_transparent_ent(%1)		(bs_array_transp[((%1 - 1) / 32)] & (1<<((%1 - 1) % 32)))
#define add_solid_ent(%1) 			bs_array_solid[((%1 - 1) / 32)] |= (1<<((%1 - 1) % 32))
#define del_solid_ent(%1) 			bs_array_solid[((%1 - 1) / 32)] &= ~(1<<((%1 - 1) % 32))
#define  is_solid_ent(%1)			(bs_array_solid[((%1 - 1) / 32)] & (1<<((%1 - 1) % 32)))

new bool:g_donttexture = false;

public fw_spawn_wallhack(ent)
{
	if (!pev_valid(ent))
		return FMRES_IGNORED;
	
	static rendermode, Float:renderamt;
	
	rendermode = pev(ent, pev_rendermode);
	pev(ent, pev_renderamt, renderamt);
	
	if (((rendermode == kRenderTransColor || rendermode == kRenderGlow || rendermode == kRenderTransTexture) && renderamt < 255.0) || (rendermode == kRenderTransAdd))
	{
		add_transparent_ent(ent);
		
		return FMRES_IGNORED;
	}
	
	return FMRES_IGNORED;
}

public message_clcorpse_wallhack(msg_id, msg_dest, entity)
{
	if (!has_connect_property(get_msg_arg_int(12)))
		return PLUGIN_HANDLED;
	
	return PLUGIN_CONTINUE;
}



public fw_stuck_wallhack(stuckent, id)
{
	if (is_transparent_ent(stuckent) && 1 <= id <= g_MaxPlayers)
		return HAM_SUPERCEDE;
	
	return HAM_IGNORED;
}

public fw_setview_wallhack(id, attachent)
{
	g_cl_viewent[id] = attachent;
	
	return FMRES_IGNORED;
}

public fw_alive_handle_wallhack(id)
{
	if (!gUserParam[id][ALIVE])
	{
		del_alive_property(id);
	}
	else
	{
		if (!has_bot_property(id) && !has_been_announced(id))
		{
			add_announced(id);
		}
		
		add_alive_property(id);
		rst_client_stat_wallhack(id);
	}
}

public pfw_traceline_wallhack(const Float:start[3], const Float:end[3], cond, id, tr)
{
	
	if (id <= 0 || id > g_MaxPlayers)
		return FMRES_IGNORED;
	
	if (!has_connect_property(id))
		return FMRES_IGNORED;
	
	new target = get_tr(TR_pHit);
	
	if (!has_alive_property(id))
	{
		bs_cl_targets[id] = 0;
	}
	else if ((1 <= target <= g_MaxPlayers) && gUserParam[target][ALIVE])
	{
		add_targeted_player(id, target);
	}
	
	
	return FMRES_IGNORED;
}

public fw_prethink_wallhack(id)
{

	
	if (!has_connect_property(id))
		return FMRES_IGNORED;
	
	if (has_alive_property(id))
	{
		if (pev(id, pev_flags) & FL_DUCKING)
		{
			add_duck_property(id);
		}
		else
		{
			del_duck_property(id);
		}
	}
	
	return FMRES_IGNORED;
}

public fw_addtofullpack_wallhack(es, e, ent, host, flags, player, set)
{

	
	if (!has_connect_property(host))
		return FMRES_IGNORED;
	
	if (!has_alive_property(host))
		return FMRES_IGNORED;
	
	if (has_bot_property(host) || gUserParam[host][TEAM] == 3 || gUserParam[host][TEAM] == 0)
		return FMRES_IGNORED;
	
	if (!player && !cv_wallhack_entity)
	{
		return FMRES_IGNORED;
	}
	
	if (!player && cv_wallhack_entity)
	{
		if (!pev_valid(ent))
			return FMRES_IGNORED;
		
		if (pev(ent, pev_owner) == host)
			return FMRES_IGNORED;
		
		static class_name[10];
		pev(ent, pev_classname, class_name, charsmax(class_name));
		
		if (!equal(class_name, "grenade"))
		{
			if (!equal(class_name, "g_cl_weaponbox"))
			{
				return FMRES_IGNORED;
			}
		}
		
		if (cv_wallhack_engine && g_cl_viewent[host] == host)
		{
			if (!engfunc(EngFunc_CheckVisibility, ent, set))
			{
				// the ent cannot be seen so we block the send channel
				forward_return(FMV_CELL, 0);
				return FMRES_SUPERCEDE;
			}
		}
		
		static Float:origin[3], Float:end[3], Float:plane_vec[3], Float:normal[3]
		
		if ( g_cl_viewent[host] == host )
			pev(host, pev_origin, origin);
		else
			pev(g_cl_viewent[host], pev_origin, origin);
		
		if (cv_wallhack_fov)
		{
			pev(host, pev_v_angle, normal);
			angle_vector(normal,ANGLEVECTOR_FORWARD, normal);
			
			pev(ent, pev_origin, end);
			xs_vec_sub_wallhack(end, origin, plane_vec);
			xs_vec_mul_scalar_wallhack(plane_vec,  (1.0/xs_vec_len_wallhack(plane_vec)), plane_vec);
			
			if (xs_vec_dot_wallhack(plane_vec, normal) < 0)
			{
				forward_return(FMV_CELL, 0);
				return FMRES_SUPERCEDE;
			}
			
			if (g_cl_viewent[host] == host)
			{
				pev(host, pev_view_ofs, plane_vec);
				xs_vec_add_wallhack(plane_vec, origin, origin);
			}
		}
		else
		{
			if (g_cl_viewent[host] == host)
			{
				pev(host, pev_view_ofs, end);
				
				xs_vec_add_wallhack(end, origin, origin);
			}
			
			pev(ent, pev_origin, end);
		}
		
		if (is_point_almost_visible(origin, end, ent))
			return FMRES_IGNORED;
		
#if defined point_test
		return FMRES_IGNORED;
#else
		// the player cannot be seen so we block the send channel
		forward_return(FMV_CELL, 0);
		return FMRES_SUPERCEDE;
#endif
	}
	
	if (!has_alive_property(ent))
		return FMRES_IGNORED;
	
	if (cv_wallhack_team && gUserParam[ent][TEAM] == gUserParam[host][TEAM])
		return FMRES_IGNORED;
	
	if (host != ent)
	{
		if (cv_wallhack_target)
		{
			if (is_user_seen_by(host, ent))
			{
				del_targeted_player(host, ent);
				del_seen_by_player(host, ent);
				disable_smooth_between(host, ent);
				
				return FMRES_IGNORED;
			}
			
			if (player_targets_user(host, ent))
			{
				del_targeted_player(host, ent);
				del_seen_by_player(host, ent);
				disable_smooth_between(host, ent);
				
				return FMRES_IGNORED;
			}
		}
		
		if (cv_wallhack_engine && g_cl_viewent[host] == host)
		{
			if (!engfunc(EngFunc_CheckVisibility, ent, set))
			{
				// the ent cannot be seen so we block the send channel
				enable_smooth_between(host, ent);
				
				forward_return(FMV_CELL, 0);
				return FMRES_SUPERCEDE;
			}
		}
		
		static Float:origin[3], Float:start[3], Float:end[3], Float:addict[3], Float:plane_vec[3], Float:normal[3], ignore_ent;
		
		ignore_ent = host;
		
		if (g_cl_viewent[host] == host)
			pev(host, pev_origin, origin);
		else
			pev(g_cl_viewent[host], pev_origin, origin);
		
		if (cv_wallhack_fov)
		{
			pev(host, pev_v_angle, normal);
			angle_vector(normal, ANGLEVECTOR_FORWARD, normal);
			
			pev(ent, pev_origin, end);
			xs_vec_sub_wallhack(end, origin, plane_vec);
			xs_vec_mul_scalar_wallhack(plane_vec,  (1.0 / xs_vec_len_wallhack(plane_vec)), plane_vec);
			
			if (xs_vec_dot_wallhack(plane_vec, normal) < 0)
			{
				enable_smooth_between(host, ent);
				
				forward_return(FMV_CELL, 0);
				return FMRES_SUPERCEDE;
			}
			
			if (g_cl_viewent[host] == host)
			{
				pev(host, pev_view_ofs, start);
				xs_vec_add_wallhack(start, origin, start);
			}
			else
			{
				start = origin;
			}
			
			if (cv_wallhack_smooth && can_use_smooth_between(host, ent))
			{
				pev(host, pev_velocity, origin);
				
				if (!xs_vec_equal_wallhack(origin, Float:{0.0, 0.0, 0.0}))
				{
					xs_vec_mul_scalar_wallhack(origin, FRAME_OFFSET_CONSTANT, origin);
					
					xs_vec_add_wallhack(start, origin, start);
				}
				
				pev(ent, pev_velocity, origin);
				
				if (!xs_vec_equal_wallhack(origin, Float:{0.0, 0.0, 0.0}))
				{
					xs_vec_mul_scalar_wallhack(origin, FRAME_OFFSET_CONSTANT, origin);
					
					xs_vec_add_wallhack(origin, end, origin);
				}
				else
				{
					origin = end;
				}
			}
			else
			{
				origin = end;
			}
		}
		else
		{
			if (g_cl_viewent[host] == host)
			{
				pev(host, pev_view_ofs, start);
				xs_vec_add_wallhack(start, origin, start);
			}
			else
			{
				start = origin;
			}
			
			pev(ent, pev_origin, end);
			
			if (cv_wallhack_smooth && can_use_smooth_between(host, ent))
			{
				pev(host, pev_velocity, origin);
				
				if (!xs_vec_equal_wallhack(origin, Float:{0.0, 0.0, 0.0}))
				{
					xs_vec_mul_scalar_wallhack(origin, FRAME_OFFSET_CONSTANT, origin);
					
					xs_vec_add_wallhack(start, origin, start);
				}
				
				pev(ent, pev_velocity, origin);
				
				if (!xs_vec_equal_wallhack(origin, Float:{0.0, 0.0, 0.0}))
				{
					xs_vec_mul_scalar_wallhack(origin, FRAME_OFFSET_CONSTANT, origin);
					
					xs_vec_add_wallhack(origin, end, origin);
				}
				else
				{
					origin = end;
				}
			}
			else
			{
				origin = end;
			}
		}
		
		xs_vec_sub_wallhack(start, origin, normal);
		
		// If origin is visible don't do anything
		if (cv_wallhack_texture)
		{
			if (is_point_visible_txt_wallhack(start, origin, ignore_ent))
			{
				disable_smooth_between(host, ent);
				return FMRES_IGNORED;
			}
		}
		else
		{
			if (is_point_visible(start, origin, ignore_ent))
			{
				disable_smooth_between(host, ent);
				return FMRES_IGNORED;
			}
		}
		
		pev(ent, pev_view_ofs, end);
		xs_vec_add_wallhack(end, origin, end);
		
		// If eye origin is visible don't do anything
		if (is_point_visible(start, end, ignore_ent))
		{
			disable_smooth_between(host, ent);
			return FMRES_IGNORED;
		}
		
		// Check g_cl_weapon point
		if (weapon_edge_point[g_cl_weapon[ent]] != 0.00)
		{
			pev(ent, pev_v_angle, addict);
			angle_vector(addict, ANGLEVECTOR_FORWARD, addict);
			xs_vec_mul_scalar_wallhack(addict, weapon_edge_point[g_cl_weapon[ent]], addict);
			xs_vec_add_wallhack(end, addict, end);
			
			// If g_cl_weapon head is visible don't do anything
			if (is_point_visible(start, end, ignore_ent))
			{
				disable_smooth_between(host, ent);
				return FMRES_IGNORED;
			}
		}
		
		// We use this to obtain the plain.
		xs_vec_mul_scalar_wallhack(normal, 1.0/(xs_vec_len_wallhack(normal)), normal);
		vector_to_angle(normal, plane_vec);
		angle_vector(plane_vec, ANGLEVECTOR_RIGHT, plane_vec);
		
		if (floatabs(normal[2]) <= ANGLE_COS_HEIGHT_CHECK)
		{
			if (has_duck_property(ent))
			{
				for (new i=0;i<4;i++)
				{
					if (i<2)
					{
						for (new j=0;j<2;j++)
						{
							xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
							addict[2] = vec_add_height_crouch[j];
							xs_vec_add_wallhack(origin, addict, end);
							
							if (is_point_visible(start, end, ignore_ent))
							{
								disable_smooth_between(host, ent);
								return FMRES_IGNORED;
							}
						}
					}
					else
					{
						for (new j=2;j<4;j++)
						{
							xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
							addict[2] = vec_add_height_crouch[j];
							xs_vec_add_wallhack(origin, addict, end);
							
							if (is_point_visible(start, end, ignore_ent))
							{
								disable_smooth_between(host, ent);
								return FMRES_IGNORED;
							}
						}
					}
				}
			}
			else
			{
				for (new i=0;i<4;i++)
				{
					if (i<2)
					{
						for (new j=0;j<2;j++)
						{
							xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
							addict[2] = vec_add_height_standup[j];
							xs_vec_add_wallhack(origin, addict, end);
							
							if (is_point_visible(start, end, ignore_ent))
							{
								disable_smooth_between(host, ent);
								return FMRES_IGNORED;
							}
						}
					}
					else
					{
						for (new j=2;j<4;j++)
						{
							xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
							addict[2] = vec_add_height_standup[j];
							xs_vec_add_wallhack(origin, addict, end);
							
							if (is_point_visible(start, end, ignore_ent))
							{
								disable_smooth_between(host, ent);
								return FMRES_IGNORED;
							}
						}
					}
				}
			}
		}
		else
		{
			if (normal[2] > 0.0)
			{
				normal[2] = 0.0;
				xs_vec_mul_scalar_wallhack(normal, 1/(xs_vec_len_wallhack(normal)), normal);
				
				if (has_duck_property(ent))
				{
					for (new i=0;i<4;i++)
					{
						if (i<2)
						{
							for (new j=0;j<2;j++)
							{
								xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
								addict[2] = vec_add_height_crouch[j];
								xs_vec_add_wallhack(origin, addict, end);
								xs_vec_mul_scalar_wallhack(normal, (j == 0) ? (-GENERAL_X_Y_SORROUNDING) : (GENERAL_X_Y_SORROUNDING), addict);
								xs_vec_add_wallhack(end, addict, end);
								
								if (is_point_visible(start, end, ignore_ent))
								{
									disable_smooth_between(host, ent);
									return FMRES_IGNORED;
								}
							}
						}
						else
						{
							for (new j=2;j<4;j++)
							{
								xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
								addict[2] = vec_add_height_crouch[j];
								xs_vec_add_wallhack(origin, addict, end);
								
								if (is_point_visible(start, end, ignore_ent))
								{
									disable_smooth_between(host, ent);
									return FMRES_IGNORED;
								}
							}
						}
					}
				}
				else
				{
					for (new i=0;i<4;i++)
					{
						if (i<2)
						{
							for (new j=0;j<2;j++)
							{
								xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
								addict[2] = vec_add_height_standup[j];
								xs_vec_add_wallhack(origin, addict, end);
								xs_vec_mul_scalar_wallhack(normal, (j == 0) ? (-GENERAL_X_Y_SORROUNDING) : (GENERAL_X_Y_SORROUNDING), addict);
								xs_vec_add_wallhack(end, addict, end);
								
								if (is_point_visible(start, end, ignore_ent))
								{
									disable_smooth_between(host, ent);
									return FMRES_IGNORED;
								}
							}
						}
						else
						{
							for (new j=2;j<4;j++)
							{
								xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
								addict[2] = vec_add_height_standup[j];
								xs_vec_add_wallhack(origin, addict, end);
								
								if (is_point_visible(start, end, ignore_ent))
								{
									disable_smooth_between(host, ent);
									return FMRES_IGNORED;
								}
							}
						}
					}
				}
			}
			else
			{
				normal[2] = 0.0;
				xs_vec_mul_scalar_wallhack(normal, 1/(xs_vec_len_wallhack(normal)), normal);
				
				if (has_duck_property(ent))
				{
					for (new i=0;i<4;i++)
					{
						if (i<2)
						{
							for (new j=0;j<2;j++)
							{
								xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
								addict[2] = vec_add_height_crouch[j];
								xs_vec_add_wallhack(origin, addict, end);
								xs_vec_mul_scalar_wallhack(normal, (j == 0) ? GENERAL_X_Y_SORROUNDING : (-GENERAL_X_Y_SORROUNDING), addict);
								xs_vec_add_wallhack(end, addict, end);
								
								if (is_point_visible(start, end, ignore_ent))
								{
									disable_smooth_between(host, ent);
									return FMRES_IGNORED;
								}
							}
						}
						else
						{
							for (new j=2;j<4;j++)
							{
								xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
								addict[2] = vec_add_height_crouch[j];
								xs_vec_add_wallhack(origin, addict, end);
								
								if (is_point_visible(start, end, ignore_ent))
								{
									disable_smooth_between(host, ent);
									return FMRES_IGNORED;
								}
							}
						}
					}
				}
				else
				{
					for (new i=0;i<4;i++)
					{
						if (i<2)
						{
							for (new j=0;j<2;j++)
							{
								xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
								addict[2] = vec_add_height_standup[j];
								xs_vec_add_wallhack(origin, addict, end);
								xs_vec_mul_scalar_wallhack(normal, (j == 0) ? GENERAL_X_Y_SORROUNDING : (-GENERAL_X_Y_SORROUNDING), addict);
								xs_vec_add_wallhack(end, addict, end);
								
								if (is_point_visible(start, end, ignore_ent))
								{
									disable_smooth_between(host, ent);
									return FMRES_IGNORED;
								}
							}
						}
						else
						{
							for (new j=2;j<4;j++)
							{
								xs_vec_mul_scalar_wallhack(plane_vec, vec_multi_lateral[i], addict);
								addict[2] = vec_add_height_standup[j];
								xs_vec_add_wallhack(origin, addict, end);
								
								if (is_point_visible(start, end, ignore_ent))
								{
									disable_smooth_between(host, ent);
									return FMRES_IGNORED;
								}
							}
						}
					}
				}
			}
		}
		
#if defined point_test
		return FMRES_IGNORED;
#else
		enable_smooth_between(host, ent);
	
		// the player cannot be seen so we block the send channel
		forward_return(FMV_CELL, 0);
		return FMRES_SUPERCEDE;
#endif
	}
	
	return FMRES_IGNORED;
}

public event_active_weapon_wallhack(id)
{
	
	if (read_data(1) == 1)
	{
		g_cl_weapon[id] = read_data(2);
		
		if (g_cl_weapon[id] < CSW_P228 || g_cl_weapon[id] > CSW_P90)
		{
			g_cl_weapon[id] = CSW_KNIFE;
		}
	}
	
	return PLUGIN_CONTINUE;
}


#if !defined point_test
bool:is_point_visible(const Float:start[3], const Float:point[3], ignore_ent)
{
	engfunc(EngFunc_TraceLine, start, point, IGNORE_GLASS | IGNORE_MONSTERS, ignore_ent, thdl);

	static Float:fraction;
	get_tr2(thdl, TR_flFraction, fraction);
	
	return (fraction == 1.0);
}

bool:is_point_visible_txt_wallhack(const Float:start[3], const Float:point[3], ignore_ent)
{
	engfunc(EngFunc_TraceLine, start, point, IGNORE_GLASS | IGNORE_MONSTERS, ignore_ent, thdl);
	
	static ent;
	ent = get_tr2(thdl, TR_pHit);

	static Float:fraction;
	get_tr2(thdl, TR_flFraction, fraction);
	
	if (fraction != 1.0 && ent > g_MaxPlayers && !g_donttexture)
	{
		if (!is_transparent_ent(ent) && !is_solid_ent(ent))
		{
			static texture_name[2];
			static Float:vec[3];
			xs_vec_sub_wallhack(point, start, vec);
			xs_vec_mul_scalar_wallhack(vec, (5000.0 / xs_vec_len_wallhack(vec)), vec);
			xs_vec_add_wallhack(start, vec, vec);
			
			engfunc(EngFunc_TraceTexture, ent, start, vec, texture_name, charsmax(texture_name));
			if (equal(texture_name, "{"))
			{
				add_transparent_ent(ent);
				
				//set_pev(ent, pev_solid, SOLID_BBOX)
				
				static players[32], num, id, Float:origin[3];
				get_players(players, num, "a");
				
				for (new i=0;i<num;i++)
				{
					id = players[i];
					
					if ( pev(id,pev_groundentity) == ent )
					{
						pev(id, pev_origin, origin);
						
						origin[2] += 1.0;
						
						set_pev(id, pev_origin, origin);
					}
				}
				
				ignore_ent = ent;
				
				engfunc(EngFunc_TraceLine, start, point, IGNORE_GLASS | IGNORE_MONSTERS, ignore_ent, thdl);
				
				get_tr2(thdl, TR_flFraction, fraction);
				
				return (fraction == 1.0);
			}
			else
			{
				add_solid_ent(ent);
				return (fraction == 1.0);
			}
		}
		else
		{
			if (is_solid_ent(ent))
			{
				return (fraction == 1.0);
			}
			else
			{
				ignore_ent = ent;
				engfunc(EngFunc_TraceLine, start, point, IGNORE_GLASS | IGNORE_MONSTERS, ignore_ent, thdl);
				get_tr2(thdl, TR_flFraction, fraction);
				return (fraction == 1.0);
			}
		}
	}
	
	return (fraction == 1.0);
}


bool:is_point_almost_visible(const Float:start[3], const Float:point[3], ignore_ent)
{
	engfunc(EngFunc_TraceLine, start, point, IGNORE_GLASS | IGNORE_MONSTERS, ignore_ent, thdl);
	
	static Float:fraction;
	get_tr2(thdl, TR_flFraction, fraction);
	
	return (fraction >= 0.9);
}
#else

bool:is_point_visible_wallhack(const Float:start[3], const Float:point[3], ignore_ent)
{
	bomb_led_wallhack(point);
	return false;
}


bool:is_point_almost_visible_wallhack(const Float:start[3], const Float:point[3], ignore_ent)
{
	bomb_led_wallhack(point);
	return false;
}
#endif


rst_client_stat_wallhack(id)
{
	bs_cl_seen_by[id] = 0;
	bs_cl_targets[id] = 0;
	bs_cl_smooth[id] = ~0;
}



stock xs_vec_add_wallhack(const Float:in1[], const Float:in2[], Float:out[])
{
	out[0] = in1[0] + in2[0];
	out[1] = in1[1] + in2[1];
	out[2] = in1[2] + in2[2];
}

stock xs_vec_sub_wallhack(const Float:in1[], const Float:in2[], Float:out[])
{
	out[0] = in1[0] - in2[0];
	out[1] = in1[1] - in2[1];
	out[2] = in1[2] - in2[2];
}

stock xs_vec_mul_scalar_wallhack(const Float:vec[], Float:scalar, Float:out[])
{
	out[0] = vec[0] * scalar;
	out[1] = vec[1] * scalar;
	out[2] = vec[2] * scalar;
}

stock Float:xs_vec_len_wallhack(const Float:vec[3])
{
	return floatsqroot(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
}

stock Float:xs_vec_dot_wallhack(const Float:vec[], const Float:vec2[])
{
	return (vec[0]*vec2[0] + vec[1]*vec2[1] + vec[2]*vec2[2]);
}

bool:xs_vec_equal_wallhack(const Float:vec1[], const Float:vec2[])
{
	return (vec1[0] == vec2[0]) && (vec1[1] == vec2[1]) && (vec1[2] == vec2[2]);
}

stock bomb_led_wallhack(const Float:point[3])
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_GLOWSPRITE);
	engfunc(EngFunc_WriteCoord, point[0]);
	engfunc(EngFunc_WriteCoord, point[1]);
	engfunc(EngFunc_WriteCoord, point[2]);
	write_short(spr_bomb);
	write_byte(1);
	write_byte(3);
	write_byte(255);
	message_end();
}

stock draw_line_wallhack(const Float:start[3], const Float:end[3])
{
	engfunc(EngFunc_MessageBegin, MSG_ALL, SVC_TEMPENTITY, Float:{0.0,0.0,0.0}, 0);
	write_byte(TE_BEAMPOINTS);
	engfunc(EngFunc_WriteCoord, start[0]);
	engfunc(EngFunc_WriteCoord, start[1]);
	engfunc(EngFunc_WriteCoord, start[2]);
	engfunc(EngFunc_WriteCoord, end[0]);
	engfunc(EngFunc_WriteCoord, end[1]);
	engfunc(EngFunc_WriteCoord, end[2]);
	write_short(beampoint);
	write_byte(0);
	write_byte(0);
	write_byte(25);
	write_byte(10);
	write_byte(0);
	write_byte(255);
	write_byte(255);
	write_byte(255);
	write_byte(127);
	write_byte(1);
	message_end();
}
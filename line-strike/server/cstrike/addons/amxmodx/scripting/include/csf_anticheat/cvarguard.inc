//CvarGuard
#define MAX_CVARS 64
#define MAX_CVAR_LEN 32
#define MAX_FILEPATH_LEN 128
#define MAX_FILE_LINE_LEN 128

new const g_szBlockedCmd[] = "csf_cmd_blocked_restart_game";
new cfgcvarguard[129];

new const g_commands[MAX_CVARS][2][MAX_CVAR_LEN];
new cv_cvarguard;
new cv_cvarguard_block;
new cv_cvarguard_multitest;

public read_config_cvarguard( const szFilePath[] ) {
	new szLine[MAX_FILE_LINE_LEN];
	new line_count, line_len, index,i=0,parsedParams;
	
	new szCvar[MAX_CVAR_LEN];
	new szValue[MAX_CVAR_LEN];
	
	while( read_file( szFilePath, line_count++, szLine, MAX_FILE_LINE_LEN, line_len ) ) {
		i++;
		if( line_len && ( !equal( szLine, "//", 2 ) ) && ( !equal( szLine, ";", 1 ) ) && (index<MAX_CVARS) )
		{
			parsedParams=parse( szLine, szCvar, MAX_CVAR_LEN-1, szValue, MAX_CVAR_LEN-1);
			
			if (parsedParams != 2)
			{
				server_print("^t[CSF-AC] CVAR Guard * %L",LANG_SERVER,"CVARGUARD_ERROR", i);
				continue;
			}


			g_commands[index][cvar] = szCvar;
			g_commands[index][value] = szValue;
			
			index++;
		}
	}

		server_print("^n^t[CSF-AC] CVAR Guard * %L",LANG_SERVER,"CVARGUARD_LOAD", index);

}

public loop_commands_cvarguard( id ) {
	for( new i; i < MAX_CVARS; i++ )
	{
		if( g_commands[i][cvar][0] )
			execute_command_cvarguard( id, i );
	}
}

public execute_command_cvarguard( id, index ) {
	new szClCmd[128];
	format( szClCmd, 127, "%s %s", g_commands[index][cvar], g_commands[index][value] );

	if(cv_cvarguard_block == 1){

		if(cv_cvarguard_multitest == 1){

			if(gUserParam[id][PROVIDER]){

				if(gUserParam[id][PROVIDER]==1){
					format( szClCmd, 127, "%s; alias %s %s", szClCmd, g_commands[index][cvar], g_szBlockedCmd );
				}

			}else{
				server_print("^t[CSF-AC] %L",LANG_SERVER,"CSF_DPROTO");
			}
		}
		
		else{
			format( szClCmd, 127, "%s; alias %s %s", szClCmd, g_commands[index][cvar], g_szBlockedCmd );
		}
	}

	client_cmd( id, szClCmd );
}

public clcmd_blockedcmd( id ) {
	client_print( id, print_console, "[CSF-AC] %L", id, "CVARGUARD_BLOCKED" );
	return PLUGIN_HANDLED;
}

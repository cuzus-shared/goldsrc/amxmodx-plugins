//FastFire

new g_detectfastfire[MAXPLAYERS+1];
new g_nCurWeapon[MAXPLAYERS+1][2];
new cv_ff_imprecision;

public FASTFIRE_Event_ShotFired( id ) 
{ 
   new weaponID = read_data( 2 );
   new wAmmo = read_data( 3 );

   g_MaxPlayers = get_maxplayers();
   
   if( g_nCurWeapon[id][0] != weaponID )
   { 
      g_nCurWeapon[id][0] = weaponID ;
      g_nCurWeapon[id][1] = wAmmo;
      return PLUGIN_CONTINUE;
   } 
   if( g_nCurWeapon[id][1] < wAmmo ) 
   { 
      g_nCurWeapon[id][1] = wAmmo;
      return PLUGIN_CONTINUE;
   } 
   if( g_nCurWeapon[id][1] == wAmmo )
      return PLUGIN_CONTINUE;
   g_nCurWeapon[id][1] = wAmmo;
   g_nCurWeapon[id][0] = weaponID;

	gUserParam[id][WEAPON] = weaponID;
	gUserParam[id][AMMO]++;

	//client_print(id,print_chat,"ammo = %d / weapon =  %d / ostatok = %d",gUserParam[id][AMMO], weaponID,wAmmo)

   return PLUGIN_CONTINUE;
}  


public FASTFIRE_checkBulletCount()
{

      for(new id=1;id<=g_MaxPlayers;id++)
      {
         if(gUserParam[id][ALIVE])
         {
			//client_print(id,print_chat,"ammo = %d",gUserParam[id][AMMO])
			switch(gUserParam[id][WEAPON])
			{
				case 11: {if(gUserParam[id][AMMO] > (7+(cv_ff_imprecision*7/100))) FASTFIRE_Detect(id);}
				case 16: {if(gUserParam[id][AMMO] > (7+(cv_ff_imprecision*7/100))) FASTFIRE_Detect(id);}
				case 17: {if(gUserParam[id][AMMO] > (7+(cv_ff_imprecision*7/100))) FASTFIRE_Detect(id);}
				case 1: {if(gUserParam[id][AMMO] > (7+(cv_ff_imprecision*7/100))) FASTFIRE_Detect(id);}
				case 26: {if(gUserParam[id][AMMO] > (4+(cv_ff_imprecision*4/100))) FASTFIRE_Detect(id);}
				case 10: {if(gUserParam[id][AMMO] > (9+(cv_ff_imprecision*9/100))) FASTFIRE_Detect(id);}

				case 21: {if(gUserParam[id][AMMO] > (2+(cv_ff_imprecision*2/100))) FASTFIRE_Detect(id);}
				case 5: {if(gUserParam[id][AMMO] > (4+(cv_ff_imprecision*4/100))) FASTFIRE_Detect(id);}

				case 7: {if(gUserParam[id][AMMO] > (13+(cv_ff_imprecision*13/100))) FASTFIRE_Detect(id);}
				case 19: {if(gUserParam[id][AMMO] > (13+(cv_ff_imprecision*13/100))) FASTFIRE_Detect(id);}
				case 12: {if(gUserParam[id][AMMO] > (10+(cv_ff_imprecision*10/100))) FASTFIRE_Detect(id);}
				case 30: {if(gUserParam[id][AMMO] > (15+(cv_ff_imprecision*15/100))) FASTFIRE_Detect(id);}
				case 23: {if(gUserParam[id][AMMO] > (13+(cv_ff_imprecision*13/100))) FASTFIRE_Detect(id);}

				case 15: {if(gUserParam[id][AMMO] > (11+(cv_ff_imprecision*11/100))) FASTFIRE_Detect(id);}
				case 3: {if(gUserParam[id][AMMO] > (1+(cv_ff_imprecision*1/100))) FASTFIRE_Detect(id);}
				case 22: {if(gUserParam[id][AMMO] > (12+(cv_ff_imprecision*12/100))) FASTFIRE_Detect(id);}
				case 8: {if(gUserParam[id][AMMO] > (11+(cv_ff_imprecision*11/100))) FASTFIRE_Detect(id);}
				case 13: {if(gUserParam[id][AMMO] > (4+(cv_ff_imprecision*4/100))) FASTFIRE_Detect(id);}
				case 18: {if(gUserParam[id][AMMO] > (1+(cv_ff_imprecision*1/100))) FASTFIRE_Detect(id);}
				case 14: {if(gUserParam[id][AMMO] > (11+(cv_ff_imprecision*11/100))) FASTFIRE_Detect(id);}
				case 28: {if(gUserParam[id][AMMO] > (11+(cv_ff_imprecision*11/100))) FASTFIRE_Detect(id);}
				case 27: {if(gUserParam[id][AMMO] > (12+(cv_ff_imprecision*12/100))) FASTFIRE_Detect(id);}
				case 24: {if(gUserParam[id][AMMO] > (4+(cv_ff_imprecision*4/100))) FASTFIRE_Detect(id);}
				case 20: {if(gUserParam[id][AMMO] > (10+(cv_ff_imprecision*10/100))) FASTFIRE_Detect(id);}
			}
         }
         gUserParam[id][AMMO] = 0;
      }
 return PLUGIN_CONTINUE;
}

public FASTFIRE_clearDetect()
{
	for(new id=1;id<=g_MaxPlayers;id++)
	{
		g_detectfastfire[id] = 0;
		//client_print(id,print_chat,"ClearDetectFF");
	}
}


public FASTFIRE_Detect(id)
{
		new ping,loss;
		get_user_ping(id,ping,loss);
		if(ping == 0) return PLUGIN_HANDLED;
		gUserParam[id][AMMO]=0;
		//client_print(id,print_chat,"%d / 4",g_detectfastfire[id]+1);
		if(++g_detectfastfire[id] < 4) return PLUGIN_CONTINUE;

		new REASON[64];
		format(REASON,63,"[CSF-AC] %L", id,"FASTFIRE_REASON");

		write_log(2,id,"0");
		punish_player(id,"FASTFIRE",REASON);

		switch(gSettings[FASTFIRE][BANSAY]){

			case 1: client_print(0,print_chat,"[CSF-AC] %L", id, "FASTFIRE_PUNISH", gUserParam[id][NAME]);
			case 2: show_hud_message(0,0,5,gUserParam[id][NAME],"0");
		}


	return PLUGIN_HANDLED;
}
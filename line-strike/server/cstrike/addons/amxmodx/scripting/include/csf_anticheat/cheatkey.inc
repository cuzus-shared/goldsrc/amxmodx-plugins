// CheatKey

#define MAX_WARNINGS 2
#define MAX_BINDS 32
#define MAX_BIND_LEN 32
#define MAX_FILE_LINE_LEN_CK 128
new g_iCKeyUses[MAXPLAYERS+1], cheathelp[5][256], bcheatkey[MAXPLAYERS+1], bindcheatkeys[128];
new const g_ck_cmd[MAX_BINDS][2][MAX_BIND_LEN];
new gcmdcheatkey[MAXPLAYERS+1];
new cheatkeyfcmd[20];


new cv_cheatkey;
new cv_cheatkey_immun;
new cv_cheatkey_warn;
new cv_cheatkey_joinsay;
new cv_cheatkey_flagimmun[2];
new Float:cv_cheatkey_rebind;

public CHEATKEY_Load()
{

	new szFilePath[128];
	format( szFilePath, sizeof(szFilePath)-1, "%s/csf_anticheat/csf_ac_cheatkey.cfg", g_configsdir);

	if (file_exists(szFilePath)){
		formatex(gcmdcheatkey,sizeof(gcmdcheatkey)-1, "csf_cheatkey%i%i%i",random_num(0,9),random_num(0,9),random_num(0,9));
		register_clcmd(gcmdcheatkey,"fnCHEATKEY");
		read_config_cheatkey(szFilePath);
		if(cv_cheatkey_rebind != 0.0){
			set_task(cv_cheatkey_rebind, "loop_commands_cheatkey",0,_,_,"b");
			//set_task(cv_cheatkey_rebind,"loop_commands_cheatkey",0,"",0,"b");
		}

	}else{
		server_print("^n^t[CSF-AC] CheatKey * %L",LANG_SERVER,"CHEATKEY_CONFIG_NFOUND");
	}
}

public read_config_cheatkey( const szFilePath[] ) 
{
	new szLine[MAX_FILE_LINE_LEN_CK];
	new line_count, line_len, index;
	new szBIND[MAX_BIND_LEN];
	
	while( read_file( szFilePath, line_count++, szLine, MAX_FILE_LINE_LEN_CK, line_len ) ) {
		if( line_len && ( !equal( szLine, "//", 2 ) ) && (index<MAX_BINDS)  )
		{
			parse( szLine, szBIND, MAX_BIND_LEN-1);
			g_ck_cmd[index][cvar] = szBIND;
			format(bcheatkey, 31, "%s", szBIND);
			
			if(index==0){
				format(bindcheatkeys, 127, "%s", bcheatkey);
			}else{
				format(bindcheatkeys, 127, "%s, %s", bindcheatkeys, bcheatkey);
			}

			index++;
		}
	}
	server_print ("^n^t[CSF-AC] CheatKey * %L",LANG_SERVER,"CHEATKEY_LOAD", bindcheatkeys );
}

public fnCHEATKEY(id) 
{ 
	if(!((cv_cheatkey_immun == 1) && (get_user_flags(id) & read_flags(cv_cheatkey_flagimmun)))){

		read_argv( 1, cheatkeyfcmd, sizeof( cheatkeyfcmd ) - 1 );
		fnWarningsCheatKey(id,cheatkeyfcmd);

	}
    return PLUGIN_HANDLED;
} 

public loop_commands_cheatkey( id ) 
{
 if(is_user_bot(id) || is_user_connecting(id) || is_user_hltv(id)) return PLUGIN_HANDLED;
	if(!((cv_cheatkey_immun == 1) && (get_user_flags(id) & read_flags(cv_cheatkey_flagimmun)))){
		for( new i; i < MAX_BINDS; i++ )
		{
			if( g_ck_cmd[i][cvar][0] )
				execute_cmd_cheatkey( id, i );
		}
	}
	return PLUGIN_HANDLED;
}

public execute_cmd_cheatkey( id, index ) 
{
	new szClCmd[128];
	format( szClCmd, 127, "bind %s ^"%s %s^"", g_ck_cmd[index][cvar], gcmdcheatkey, g_ck_cmd[index][cvar]);
	
	client_cmd( id, szClCmd );
}

public showWarnCheatKey(param[]) 
{

	loop_commands_cheatkey( param[0] );
	format(cheathelp[0], 255, "%L",  param[0], "CHEATKEY_HELP0");
	format(cheathelp[1], 255, "%L",  param[0], "CHEATKEY_HELP1");
	format(cheathelp[2], 255, "%L",  param[0], "CHEATKEY_HELP2");
	format(cheathelp[3], 255, "%L",  param[0], "CHEATKEY_HELP3", bindcheatkeys);
	format(cheathelp[4], 255, "%L",  param[0], "CHEATKEY_HELP4");



    switch (cv_cheatkey_joinsay){

	case 1:
		{

    			client_print(param[0],print_chat,"[CSF-AC] %s",  cheathelp[0]);
    			client_print(param[0],print_chat,"[CSF-AC] %s",  cheathelp[1]);
    			client_print(param[0],print_chat,"[CSF-AC] %s",  cheathelp[2]);
    			client_print(param[0],print_chat,"[CSF-AC] %s",  cheathelp[3]);
    			client_print(param[0],print_chat,"[CSF-AC] %s",  cheathelp[4]);

		}

	case 2:
		{
			set_hudmessage(str_to_num(hud_pwc_arg[2][0]),
						   str_to_num(hud_pwc_arg[2][1]),
						   str_to_num(hud_pwc_arg[2][2]),
						   str_to_float(hud_pwc_arg[2][3]),
						   str_to_float(hud_pwc_arg[2][4]),
						   str_to_num(hud_pwc_arg[2][5]),
						   str_to_float(hud_pwc_arg[2][6]),
						   str_to_float(hud_pwc_arg[2][7]),
						   str_to_float(hud_pwc_arg[2][8]),
						   str_to_float(hud_pwc_arg[2][9]),
						   str_to_num(hud_pwc_arg[2][10])); //Warning
   			show_hudmessage(param[0], "%s^n^n%s^n^n%s^n^n%s^n^n%s", cheathelp[0],cheathelp[1],cheathelp[2],cheathelp[3],cheathelp[4]);
    			client_print(param[0],print_console,"[CSF-AC] %s",  cheathelp[0]);
    			client_print(param[0],print_console,"[CSF-AC] %s",  cheathelp[1]);
    			client_print(param[0],print_console,"[CSF-AC] %s",  cheathelp[2]);
    			client_print(param[0],print_console,"[CSF-AC] %s",  cheathelp[3]);
    			client_print(param[0],print_console,"[CSF-AC] %s",  cheathelp[4]);

		}
    }

    
    return PLUGIN_HANDLED ;
}

public showBanCheatKey(id,cheatkeycmd[])
{
	new REASON[64];
	format(REASON,63,"[CSF-AC] %L", id,"CHEATKEY_REASON");

	write_log(11,id,cheatkeycmd);
	punish_player(id,"CHEATKEY",REASON);

	switch(gSettings[CHEATKEY][BANSAY]){

		case 1: client_print(0,print_chat,"[CSF-AC] %L", id, "CHEATKEY_PUNISH", gUserParam[id][NAME]);
		case 2: show_hud_message(0,0,11,gUserParam[id][NAME],"0");

	}


}

public ScreenShotCheatKey(id) 
{
        client_cmd(id,"snapshot;snapshot");
        client_cmd(id, "kill");
}

public fnWarningsCheatKey(id,cheatkeycmd[]) 
{ 
    new message[256], hostname[64], ctime[64], players[32], inum;
    get_cvar_string("hostname", hostname, 63);
    get_time("%d/%m/%Y - %H:%M:%S", ctime, 63);
    get_players(players,inum);
    format(message, 255, "[CSF-AC] %L", id, "CHEATKEY_USEKEY", gUserParam[id][NAME],cheatkeycmd);

     for(new i = 0 ; i < inum ; ++i){
         if (access(players[i], ADMIN_CHAT)){
             client_print(players[i],print_chat,"%s", message);
         }
     }

	g_iCKeyUses[id]++;

	if(cv_cheatkey_warn == 0){

		if(g_iCKeyUses[id] > 1) return PLUGIN_HANDLED;
		showBanCheatKey(id,cheatkeycmd);

	 return PLUGIN_HANDLED;
	}



    if(g_iCKeyUses[id] > cv_cheatkey_warn)
    {

		if(g_iCKeyUses[id] > cv_cheatkey_warn+1){
			return PLUGIN_HANDLED;
		}

		client_print(id,print_chat,"[CSF-AC] %L",  id, "CHEATKEY_SERVER", hostname);
		client_print(id,print_chat,"[CSF-AC]  %L",  id, "CHEATKEY_DATE_TIME", ctime);
		ScreenShotCheatKey(id);
		client_print(id,print_console,"^n=====================================================^n");
		client_print(id,print_console,"[CSF-AC] %L", id, "CHEATKEY_CONSOLEPUNISH0");
		client_print(id,print_console,"[CSF-AC] %L", id, "CHEATKEY_CONSOLEPUNISH1");
		client_print(id,print_console,"[CSF-AC] %L", id, "CHEATKEY_CONSOLEPUNISH2", cv_site);
		client_print(id,print_console,"^n=====================================================^n");
		showBanCheatKey(id,cheatkeycmd);
    }else{
        client_print(id,print_chat,"[CSF-AC] %L",  id, "CHEATKEY_WARN0");
		if(cv_cheatkey_warn > 1){
			client_print(id,print_chat,"[CSF-AC] %L",  id, "CHEATKEY_WARN1-1", g_iCKeyUses[id], cv_cheatkey_warn);
		}
		else
		{
			client_print(id,print_chat,"[CSF-AC] %L",  id, "CHEATKEY_WARN1");
			client_print(id,print_chat,"[CSF-AC] %L",  id, "CHEATKEY_WARN2", bindcheatkeys);
		}
			client_print(id,print_chat,"[CSF-AC] %L",  id, "CHEATKEY_SERVER", hostname);
			client_print(id,print_chat,"[CSF-AC]  %L",  id, "CHEATKEY_DATE_TIME", ctime);
			ScreenShotCheatKey(id);
    }
	return PLUGIN_HANDLED;
}

//ZoneGuard

new cv_zoneguard;

enum ZONEMODE {
	ZM_BLOCK_ALL,
	ZM_KILL
}
new zonezgmode[ZONEMODE][] = { "ZONEGUARD_BLOCKING",  "ZONEGUARD_CHEATER" };
new zonezgname[ZONEMODE][] = { "csf_ac_blockzone", "csf_ac_killzone" };
new solidtyp[ZONEMODE] = { SOLID_BBOX, SOLID_TRIGGER };
new zonezgcolor[ZONEMODE][3] = {
	{ 255, 255, 255 },	// alle Blockieren
	{ 255, 0, 0 }	// Kill
};
#define ZONEID pev_iuser1
#define CAMPERTIME pev_iuser2
new zonezg_color_aktiv[3] = { 0, 0, 255 };
new zonezg_color_red[3] = { 255, 0, 0 };
new zonezg_color_green[3] = { 255, 255, 0 };
#define MAXZONESZG 100
new zonezg[MAXZONESZG];
new maxzonezgs;
new indexzg;
new setupunitszg = 10;
new directionzg = 0;
new koordinatenzg[3][] = { "ZONEGUARD_X_KOORD", "ZONEGUARD_Y_KOORD", "ZONEGUARD_Z_KOORD" };
new spr_dotzg;
new editorzg = 0;
new zonekillid[33];
#define TASK_BASIS_SHOWZONES 1000
enum ROUNDSTATUS {
	RS_UNDEFINED,
	RS_RUNNING,
	RS_FREEZETIME,
	RS_END,
}
new ROUNDSTATUS:roundstatus = RS_UNDEFINED;

public RandomDirection(player) {
	new Float:velocity[3];
	velocity[0] = random_float(-256.0, 256.0);
	velocity[1] = random_float(-256.0, 256.0);
	velocity[2] = random_float(-256.0, 256.0);
	set_pev(player, pev_velocity, velocity);
}


public CreateZone(Float:position[3], Float:mins[3], Float:maxs[3], zm, campertime) {
	new entity = fm_create_entity("info_target");
	set_pev(entity, pev_classname, "skyzonezgguardzonezg");
	fm_entity_set_model(entity, "models/gib_skull.mdl");
	fm_entity_set_origin(entity, position);

	set_pev(entity, pev_movetype, MOVETYPE_FLY);
	new id = pev(entity, ZONEID);
	if (editorzg)
	{
		set_pev(entity, pev_solid, SOLID_NOT);
	} else
	{
		set_pev(entity, pev_solid, solidtyp[ZONEMODE:id]);
	}
	
	fm_entity_set_size(entity, mins, maxs);
	
	fm_set_entity_visibility(entity, 0);
	
	set_pev(entity, ZONEID, zm);
	set_pev(entity, CAMPERTIME, campertime);

	
	return entity;
}

public CreateNewZone(Float:position[3]) {
	new Float:mins[3] = { -32.0, -32.0, -32.0 };
	new Float:maxs[3] = { 32.0, 32.0, 32.0 };
	return CreateZone(position, mins, maxs, 0, 10);	// ZM_NONE
}

public CreateZoneOnPlayer(player) {
	// Position und erzeugen
	new Float:position[3];
	pev(player, pev_origin, position);
	
	new entity = CreateNewZone(position);
	FindAllZones();
	
	for(new i = 0; i < maxzonezgs; i++) {
		if(zonezg[i] == entity){
			indexzg = i;
		}
	}
}

public SaveZG(player) {
	new zonezgfile[200];
	new mapstringonfile[64];
	get_configsdir(zonezgfile, 199);
	format(zonezgfile, 199, "%s/csf_anticheat/csf_zone_guard", zonezgfile);
	if (!dir_exists(zonezgfile)) {
		mkdir(zonezgfile);
	}
	
	format(zonezgfile, 199, "%s/%s.zg", zonezgfile, g_mapname);
	delete_file(zonezgfile);
	
	FindAllZones();
	
	format(mapstringonfile,63,"// The list of prohibited areas for %s", g_mapname);
	
	// Header
	write_file(zonezgfile, "//************************************************//");
	write_file(zonezgfile, "//");
	write_file(zonezgfile, "// CSFile.Info Anti-Cheat");
	write_file(zonezgfile, mapstringonfile);
	write_file(zonezgfile, "//                                                                        ");
	write_file(zonezgfile, "//************************************************//");
	write_file(zonezgfile, "//");
	write_file(zonezgfile, "//^t<Zone type> <Position (x/y/z)> <MinS (x/y/z)> <MaxS (x/y/z)>");
	write_file(zonezgfile, "//");
	write_file(zonezgfile, "//^tBlock Zone - csf_ac_blockzone ^t<Position (x/y/z)> <MinS (x/y/z)> <MaxS (x/y/z)>");
	write_file(zonezgfile, "//^tRestrict Zone - csf_ac_killzone ^t<Position (x/y/z)> <MinS (x/y/z)> <MaxS (x/y/z)>");
	write_file(zonezgfile, "//");
	write_file(zonezgfile, "");
	
	for(new i = 0; i < maxzonezgs; i++)
	{
		new z = zonezg[i];	// das Entity
		
		// diverse Daten der Zone
		new zm = pev(z, ZONEID);
		
		// Koordinaten holen
		new Float:pos[3];
		pev(z, pev_origin, pos);
		
		// Dimensionen holen
		new Float:mins[3], Float:maxs[3];
		pev(z, pev_mins, mins);
		pev(z, pev_maxs, maxs);
		
		// Ausgabe formatieren
		//  -> Type und CamperTime
		new output[1000];
		format(output, 999, "%s", zonezgname[ZONEMODE:zm]);
		//  -> Position
		format(output, 999, "%s %.1f %.1f %.1f", output, pos[0], pos[1], pos[2]);
		//  -> Dimensionen
		format(output, 999, "%s %.0f %.0f %.0f", output, mins[0], mins[1], mins[2]);
		format(output, 999, "%s %.0f %.0f %.0f", output, maxs[0], maxs[1], maxs[2]);
		
		
		// und schreiben
		write_file(zonezgfile, output);
	}
	
	client_print(player, print_chat, "%L", player, "ZONEGUARD_FILE_SAVED", zonezgfile);
}

public LoadZG(zonezgfile[]) {

	new input[1000], line = 0, len,loadzones;
	
	while( (line = read_file(zonezgfile , line , input , 127 , len) ) != 0 ) 
	{
		if (!strlen(input)  || (input[0] == ';')  || equal(input, "//", 2)) {
			continue;
		}

		new data[20], zm = 0, ct;
		new Float:mins[3], Float:maxs[3], Float:pos[3];

		strbreak(input, data, 20, input, 999);
		zm = -1;
		for(new i = 0; ZONEMODE:i < ZONEMODE; ZONEMODE:i++)
		{

			if (equal(data, zonezgname[ZONEMODE:i])) zm = i;
		}
		
		if (zm == -1)
		{
			server_print("^t[CSF-AC] ZoneGuard * Error / Warning -> '%s' ... dropped", data);
			continue;
		}

		strbreak(input, data, 20, input, 999);	pos[0] = str_to_float(data);
		strbreak(input, data, 20, input, 999);	pos[1] = str_to_float(data);
		strbreak(input, data, 20, input, 999);	pos[2] = str_to_float(data);
		
		strbreak(input, data, 20, input, 999);	mins[0] = str_to_float(data);
		strbreak(input, data, 20, input, 999);	mins[1] = str_to_float(data);
		strbreak(input, data, 20, input, 999);	mins[2] = str_to_float(data);

		strbreak(input, data, 20, input, 999);	maxs[0] = str_to_float(data);
		strbreak(input, data, 20, input, 999);	maxs[1] = str_to_float(data);
		strbreak(input, data, 20, input, 999);	maxs[2] = str_to_float(data);

		loadzones++;


		// und nun noch erstellen
		CreateZone(pos, mins, maxs, zm, ct);
	}
	
	server_print("^n^t[CSF-AC] ZoneGuard * %L",LANG_SERVER,"ZONEGUARD_LOAD", loadzones);

	FindAllZones();
	HideAllZones();
}

public FX_Box(Float:sizemin[3], Float:sizemax[3], color[3], life) {
	// FX
	message_begin(MSG_ALL, SVC_TEMPENTITY);

	write_byte(31);
	
	write_coord( floatround( sizemin[0] ) ); // x
	write_coord( floatround( sizemin[1] ) ); // y
	write_coord( floatround( sizemin[2] ) ); // z
	
	write_coord( floatround( sizemax[0] ) ); // x
	write_coord( floatround( sizemax[1] ) ); // y
	write_coord( floatround( sizemax[2] ) ); // z

	write_short(life);	// Life
	
	write_byte(color[0]);	// Color R / G / B
	write_byte(color[1]);
	write_byte(color[2]);
	
	message_end(); 
}

public FX_Line(start[3], stop[3], color[3], brightness) {
	message_begin(MSG_ONE_UNRELIABLE, SVC_TEMPENTITY, _, editorzg);
	
	write_byte( TE_BEAMPOINTS );
	
	write_coord(start[0]);
	write_coord(start[1]);
	write_coord(start[2]);
	
	write_coord(stop[0]);
	write_coord(stop[1]);
	write_coord(stop[2]);
	
	write_short( spr_dotzg );
	
	write_byte( 1 );	// framestart 
	write_byte( 1 );	// framerate 
	write_byte( 4 );	// life in 0.1's 
	write_byte( 5 );	// width
	write_byte( 0 ); 	// noise 
	
	write_byte( color[0] );   // r, g, b 
	write_byte( color[1] );   // r, g, b 
	write_byte( color[2] );   // r, g, b 
	
	write_byte( brightness );  	// brightness 
	write_byte( 0 );   	// speed 
	
	message_end();
}

public DrawLine(Float:x1, Float:y1, Float:z1, Float:x2, Float:y2, Float:z2, color[3]) {
	new start[3];
	new stop[3];
	
	start[0] = floatround( x1 );
	start[1] = floatround( y1 );
	start[2] = floatround( z1 );
	
	stop[0] = floatround( x2 );
	stop[1] = floatround( y2 );
	stop[2] = floatround( z2 );

	FX_Line(start, stop, color, 200);
}

public ShowAllZones() {
	FindAllZones();	// zur Sicherheit alle suchen
	
	for(new i = 0; i < maxzonezgs; i++)
	{
		new z = zonezg[i];
		remove_task(TASK_BASIS_SHOWZONES + z);
		set_pev(z, pev_solid, SOLID_NOT);
		set_task(0.2, "ShowZoneBox", TASK_BASIS_SHOWZONES + z, _, _, "b");
	}
}

public ShowZoneBox(entity) {
	entity -= TASK_BASIS_SHOWZONES;
	if ((!fm_is_valid_ent(entity)) || !editorzg) { return; }


	new Float:pos[3];
	pev(entity, pev_origin, pos);
	if (!fm_is_in_viewcone(editorzg, pos) && (entity != zonezg[indexzg])) {return;}

	new Float:editorzgpos[3];
	pev(editorzg, pev_origin, editorzgpos);
	new Float:hitpoint[3];
	fm_trace_line(-1, editorzgpos, pos, hitpoint);

	if (entity == zonezg[indexzg]) { DrawLine(editorzgpos[0], editorzgpos[1], editorzgpos[2] - 16.0, pos[0], pos[1], pos[2], { 255, 0, 0} ); }

	new Float:dh = vector_distance(editorzgpos, pos) - vector_distance(editorzgpos, hitpoint);
	if ( (floatabs(dh) > 128.0) && (entity != zonezg[indexzg])) {return;}


	new Float:mins[3], Float:maxs[3];
	pev(entity, pev_mins, mins);
	pev(entity, pev_maxs, maxs);


	mins[0] += pos[0];
	mins[1] += pos[1];
	mins[2] += pos[2];
	maxs[0] += pos[0];
	maxs[1] += pos[1];
	maxs[2] += pos[2];
	
	new id = pev(entity, ZONEID);
	
	new color[3];
	color[0] = (zonezg[indexzg] == entity) ? zonezg_color_aktiv[0] : zonezgcolor[ZONEMODE:id][0];
	color[1] = (zonezg[indexzg] == entity) ? zonezg_color_aktiv[1] : zonezgcolor[ZONEMODE:id][1];
	color[2] = (zonezg[indexzg] == entity) ? zonezg_color_aktiv[2] : zonezgcolor[ZONEMODE:id][2];
	
	// einzelnen Linien der Box zeichnen
	//  -> alle Linien beginnen bei maxs
	DrawLine(maxs[0], maxs[1], maxs[2], mins[0], maxs[1], maxs[2], color);
	DrawLine(maxs[0], maxs[1], maxs[2], maxs[0], mins[1], maxs[2], color);
	DrawLine(maxs[0], maxs[1], maxs[2], maxs[0], maxs[1], mins[2], color);
	//  -> alle Linien beginnen bei mins
	DrawLine(mins[0], mins[1], mins[2], maxs[0], mins[1], mins[2], color);
	DrawLine(mins[0], mins[1], mins[2], mins[0], maxs[1], mins[2], color);
	DrawLine(mins[0], mins[1], mins[2], mins[0], mins[1], maxs[2], color);
	//  -> die restlichen 6 Lininen
	DrawLine(mins[0], maxs[1], maxs[2], mins[0], maxs[1], mins[2], color);
	DrawLine(mins[0], maxs[1], mins[2], maxs[0], maxs[1], mins[2], color);
	DrawLine(maxs[0], maxs[1], mins[2], maxs[0], mins[1], mins[2], color);
	DrawLine(maxs[0], mins[1], mins[2], maxs[0], mins[1], maxs[2], color);
	DrawLine(maxs[0], mins[1], maxs[2], mins[0], mins[1], maxs[2], color);
	DrawLine(mins[0], mins[1], maxs[2], mins[0], maxs[1], maxs[2], color);

	// der Rest wird nur gezeichnet wenn es sich um ide aktuelle Box handelt
	if (entity != zonezg[indexzg]) return;
	
	// jetzt noch die Koordinaten-Linien
	if (directionzg == 0)	// X-Koordinaten
	{
		DrawLine(maxs[0], maxs[1], maxs[2], maxs[0], mins[1], mins[2], zonezg_color_green);
		DrawLine(maxs[0], maxs[1], mins[2], maxs[0], mins[1], maxs[2], zonezg_color_green);
		
		DrawLine(mins[0], maxs[1], maxs[2], mins[0], mins[1], mins[2], zonezg_color_red);
		DrawLine(mins[0], maxs[1], mins[2], mins[0], mins[1], maxs[2], zonezg_color_red);
	}
	if (directionzg == 1)	// Y-Koordinaten
	{
		DrawLine(mins[0], mins[1], mins[2], maxs[0], mins[1], maxs[2], zonezg_color_red);
		DrawLine(maxs[0], mins[1], mins[2], mins[0], mins[1], maxs[2], zonezg_color_red);

		DrawLine(mins[0], maxs[1], mins[2], maxs[0], maxs[1], maxs[2], zonezg_color_green);
		DrawLine(maxs[0], maxs[1], mins[2], mins[0], maxs[1], maxs[2], zonezg_color_green);
	}	
	if (directionzg == 2)	// Z-Koordinaten
	{
		DrawLine(maxs[0], maxs[1], maxs[2], mins[0], mins[1], maxs[2], zonezg_color_green);
		DrawLine(maxs[0], mins[1], maxs[2], mins[0], maxs[1], maxs[2], zonezg_color_green);

		DrawLine(maxs[0], maxs[1], mins[2], mins[0], mins[1], mins[2], zonezg_color_red);
		DrawLine(maxs[0], mins[1], mins[2], mins[0], maxs[1], mins[2], zonezg_color_red);
	}
}

public HideAllZones() {
	editorzg = 0;
	for(new i = 0; i < maxzonezgs; i++)
	{
		new id = pev(zonezg[i], ZONEID);
		set_pev(zonezg[i], pev_solid, solidtyp[ZONEMODE:id]);
		remove_task(TASK_BASIS_SHOWZONES + zonezg[i]);
	}
}

public FindAllZones() {
	new entity = -1;
	maxzonezgs = 0;
	while( (entity = fm_find_ent_by_class(entity, "skyzonezgguardzonezg")) )
	{
		zonezg[maxzonezgs] = entity;
		maxzonezgs++;
	}
}

public InitZoneGuard(player) {
	new name[33], steam[33], ipadr[33];
	get_user_name(player, name, 32);
	get_user_authid(player, steam, 32);
	get_user_ip(player, ipadr, 32, 1);
	
	if (!(get_user_flags(player) & ADMIN_RCON))
	{
		log_amx("[CSF-AC] Zone Guard * No access rights for Menu - '%s' <%s> <%s>", name, ipadr,steam);
		return PLUGIN_HANDLED;
	}
	
	editorzg = player;
	FindAllZones();
	ShowAllZones();
	
	set_task(0.1, "OpenZoneGuardMenu", player);

	return PLUGIN_HANDLED;
}

public OpenZoneGuardMenu(player) {
	new trans[70];
	new menu[1024];
	new zm = -1;

	new menukeys = MENU_KEY_0 + MENU_KEY_4 + MENU_KEY_9;
	
	if (fm_is_valid_ent(zonezg[indexzg])) 
	{
		zm = pev(zonezg[indexzg], ZONEID);
	}
	
	format(menu, 1023, "\d[CSF-AC] - Zone Guard\w");
	format(menu, 1023, "%s^n", menu);
	format(menu, 1023, "%s^n", menu);
	format(menu, 1023, "%L", player, "ZONEGUARD_FOUND", menu, maxzonezgs);
	
	if (zm != -1)
	{
		format(trans, 69, "%L", player, zonezgmode[ZONEMODE:zm] );

		format(menu, 1023, "%L", player, "ZONEGUARD_CURRENT_ZONE", menu, trans);


		menukeys += MENU_KEY_2 + MENU_KEY_3 + MENU_KEY_1;
		format(menu, 1023, "%s^n", menu);		// Leerzeile
		format(menu, 1023, "%s^n", menu);		// Leerzeile
		format(menu, 1023, "%L", player, "ZONEGUARD_EDIT", menu);
		format(menu, 1023, "%L", player, "ZONEGUARD_CHANGE", menu);
	}
	
	format(menu, 1023, "%s^n", menu);		// Leerzeile
	format(menu, 1023, "%L" ,player, "ZONEGUARD_CREATE", menu);
	
	if (zm != -1)
	{
		menukeys += MENU_KEY_6;
		format(menu, 1023, "%L", player, "ZONEGUARD_DELETE", menu);
	}
	format(menu, 1023, "%L", player, "ZONEGUARD_SAVE", menu);
		
	format(menu, 1023, "%s^n", menu);		// Leerzeile
	format(menu, 1023, "%L" ,player, "ZONEGUARD_EXIT", menu);
	
	show_menu(player, menukeys, menu, -1, "MainMenu");
	client_cmd(player, "spk sound/buttons/blip1.wav");
}

public MainMenuAction(player, key) {
	key = (key == 10) ? 0 : key + 1;
	switch(key) 
	{
		case 1: {
				// Zone editieren
				if (fm_is_valid_ent(zonezg[indexzg])) OpenEditMenu(player); else OpenZoneGuardMenu(player);
			}
		case 2: {
				// vorherige Zone
				indexzg = (indexzg > 0) ? indexzg - 1 : indexzg;
				OpenZoneGuardMenu(player);
			}
		case 3: {
				// nРґchste Zone
				indexzg = (indexzg < maxzonezgs - 1) ? indexzg + 1 : indexzg;
				OpenZoneGuardMenu(player);
			}
		case 4:	{
				// neue Zone СЊber dem Spieler
				if (maxzonezgs < MAXZONESZG - 1)
				{
					CreateZoneOnPlayer(player);
					ShowAllZones();
					MainMenuAction(player, 0);	// selber aufrufen
				} else
				{
					client_print(player, print_chat, "%L", player, "ZONEGUARD_FULL");
					client_cmd(player, "spk sound/buttons/button10.wav");
					set_task(0.5, "OpenZoneGuardMenu", player);
				}
			}
		case 6: {
				// aktuelle Zone lС†schen
				OpenKillMenu(player);
			}
		case 9: {
				// Zonen speichern
				SaveZG(player);
				OpenZoneGuardMenu(player);
			}
		case 10:{
				editorzg = 0;
				HideAllZones();
			}
	}
}

public OpenEditMenu(player) {
	new trans[70];
	
	new menu[1024];
	new menukeys = MENU_KEY_0 + MENU_KEY_1 + MENU_KEY_4 + MENU_KEY_5 + MENU_KEY_6 + MENU_KEY_7 + MENU_KEY_8 + MENU_KEY_9;
	
	format(menu, 1023, "\d[CSF-AC] - Zone Guard * Edit zone\w");
	format(menu, 1023, "%s^n", menu);		// Leerzeile
	format(menu, 1023, "%s^n", menu);		// Leerzeile

	new zm = -1;
	if (fm_is_valid_ent(zonezg[indexzg]))
	{
		zm = pev(zonezg[indexzg], ZONEID);
	}
	
	if (zm != -1)
	{
		format(trans, 69, "%L", player, zonezgmode[ZONEMODE:zm]);

			format(menu, 1023, "%L", player, "ZONEGUARD_CURRENT_NONE", menu, trans);
			format(menu, 1023, "%s^n", menu);

	}
	
	format(menu, 1023, "%s^n", menu);
	
	format(trans, 49, "%L", player, koordinatenzg[directionzg]);
	format(menu, 1023, "%L", player, "ZONEGUARD_SIZE_INIT", menu, trans);
	format(menu, 1023, "%L", player, "ZONEGUARD_SIZE_MINS", menu);
	format(menu, 1023, "%L", player, "ZONEGUARD_SIZE_MAXS", menu);
	format(menu, 1023, "%L", player, "ZONEGUARD_SIZE_STEP", menu, setupunitszg);
	format(menu, 1023, "%s^n", menu);	
	format(menu, 1023, "%s^n", menu);	
	format(menu, 1023, "%L", player, "ZONEGUARD_SIZE_QUIT", menu);
	
	show_menu(player, menukeys, menu, -1, "EditMenu");
	client_cmd(player, "spk sound/buttons/blip1.wav");
}

public EditMenuAction(player, key) {
	key = (key == 10) ? 0 : key + 1;
	switch(key)
	{
		case 1: {
				// nРґchster ZoneMode
				new zm = -1;
				zm = pev(zonezg[indexzg], ZONEID);
				if (ZONEMODE:zm == ZM_KILL) zm = 0; else zm++;
				set_pev(zonezg[indexzg], ZONEID, zm);
				OpenEditMenu(player);
			}
		case 2: {
				// Campertime runter
				new ct = pev(zonezg[indexzg], CAMPERTIME);
				ct = (ct > 5) ? ct - 1 : 5;
				set_pev(zonezg[indexzg], CAMPERTIME, ct);
				OpenEditMenu(player);
			}
		case 3: {
				// Campertime hoch
				new ct = pev(zonezg[indexzg], CAMPERTIME);
				ct = (ct < 30) ? ct + 1 : 30;
				set_pev(zonezg[indexzg], CAMPERTIME, ct);
				OpenEditMenu(player);
			}
		case 4: {
				// Editier-Richtung Рґndern
				directionzg = (directionzg < 2) ? directionzg + 1 : 0;
				OpenEditMenu(player);
			}
		case 5: {
				// von "mins" / rot etwas abziehen -> schmaler
				ZuRotAddieren();
				OpenEditMenu(player);
			}
		case 6: {
				// zu "mins" / rot etwas addieren -> breiter
				VonRotAbziehen();
				OpenEditMenu(player);
			}
		case 7: {
				// von "maxs" / gelb etwas abziehen -> schmaler
				VonGelbAbziehen();
				OpenEditMenu(player);
			}
		case 8: {
				// zu "maxs" / gelb etwas addierne -> breiter
				ZuGelbAddieren();
				OpenEditMenu(player);
			}
		case 9: {
				// Schreitweite Рґndern
				setupunitszg = (setupunitszg < 100) ? setupunitszg * 10 : 1;
				OpenEditMenu(player);
			}
		case 10:{
				OpenZoneGuardMenu(player);
			}
	}
}

public KillMenuAction(player, key) {
	key = (key == 10) ? 0 : key + 1;
	switch(key)
	{
		case 1: {
				client_print(player, print_chat, "[CSF-AC] %L", player, "ZONEGUARD_KILL_NO");
			}
		case 10:{
				fm_remove_entity(zonezg[indexzg]);
				indexzg--;
				if (indexzg < 0) indexzg = 0;
				client_print(player, print_chat, "[CSF-AC] %L", player, "ZONEGUARD_KILL_YES");
				FindAllZones();
			}
	}
	OpenZoneGuardMenu(player);
}

public VonRotAbziehen() {
	new entity = zonezg[indexzg];
	
	// Koordinaten holen
	new Float:pos[3];
	pev(entity, pev_origin, pos);

	// Dimensionen holen
	new Float:mins[3], Float:maxs[3];
	pev(entity, pev_mins, mins);
	pev(entity, pev_maxs, maxs);

	// kС†nnte Probleme geben -> zu klein
	//if ((floatabs(mins[directionzg]) + maxs[directionzg]) < setupunitszg + 1) return
	
	mins[directionzg] -= float(setupunitszg) / 2.0;
	maxs[directionzg] += float(setupunitszg) / 2.0;
	pos[directionzg] -= float(setupunitszg) / 2.0;
	
	set_pev(entity, pev_origin, pos);
	fm_entity_set_size(entity, mins, maxs);
}

public ZuRotAddieren() {
	new entity = zonezg[indexzg];
	
	// Koordinaten holen
	new Float:pos[3];
	pev(entity, pev_origin, pos);

	// Dimensionen holen
	new Float:mins[3], Float:maxs[3];
	pev(entity, pev_mins, mins);
	pev(entity, pev_maxs, maxs);

	// kС†nnte Probleme geben -> zu klein
	if ((floatabs(mins[directionzg]) + maxs[directionzg]) < setupunitszg + 1) return;

	mins[directionzg] += float(setupunitszg) / 2.0;
	maxs[directionzg] -= float(setupunitszg) / 2.0;
	pos[directionzg] += float(setupunitszg) / 2.0;
	
	set_pev(entity, pev_origin, pos);
	fm_entity_set_size(entity, mins, maxs);
}

public VonGelbAbziehen() {
	new entity = zonezg[indexzg];
	
	// Koordinaten holen
	new Float:pos[3];
	pev(entity, pev_origin, pos);

	// Dimensionen holen
	new Float:mins[3], Float:maxs[3];
	pev(entity, pev_mins, mins);
	pev(entity, pev_maxs, maxs);

	// kС†nnte Probleme geben -> zu klein
	if ((floatabs(mins[directionzg]) + maxs[directionzg]) < setupunitszg + 1) return;

	mins[directionzg] += float(setupunitszg) / 2.0;
	maxs[directionzg] -= float(setupunitszg) / 2.0;
	pos[directionzg] -= float(setupunitszg) / 2.0;
	
	set_pev(entity, pev_origin, pos);
	fm_entity_set_size(entity, mins, maxs);
}

public ZuGelbAddieren() {
	new entity = zonezg[indexzg];
	
	// Koordinaten holen
	new Float:pos[3];
	pev(entity, pev_origin, pos);

	// Dimensionen holen
	new Float:mins[3], Float:maxs[3];
	pev(entity, pev_mins, mins);
	pev(entity, pev_maxs, maxs);

	// kС†nnte Probleme geben -> zu klein
	//if ((floatabs(mins[directionzg]) + maxs[directionzg]) < setupunitszg + 1) return

	mins[directionzg] -= float(setupunitszg) / 2.0;
	maxs[directionzg] += float(setupunitszg) / 2.0;
	pos[directionzg] += float(setupunitszg) / 2.0;
	
	set_pev(entity, pev_origin, pos);
	fm_entity_set_size(entity, mins, maxs);
}

public OpenKillMenu(player) {
	new menu[1024];
	
	format(menu, 1023, "%L", player, "ZONEGUARD_KILL_INIT");
	format(menu, 1023, "%L", player, "ZONEGUARD_KILL_ASK", menu); // ja - nein - vieleicht
	
	show_menu(player, MENU_KEY_1 + MENU_KEY_0, menu, -1, "KillMenu");
	
	client_cmd(player, "spk sound/buttons/button10.wav");
}

stock fm_set_kvd(entity, const key[], const value[], const classname[] = "") {
	if (classname[0]) {
		set_kvd(0, KV_ClassName, classname);
	}
	else {
		new class[32];
		pev(entity, pev_classname, class, sizeof class - 1);
		set_kvd(0, KV_ClassName, class);
	}

	set_kvd(0, KV_KeyName, key);
	set_kvd(0, KV_Value, value);
	set_kvd(0, KV_fHandled, 0);

	return dllfunc(DLLFunc_KeyValue, entity, 0);
}

stock fm_fake_touch(toucher, touched)
	return dllfunc(DLLFunc_Touch, toucher, touched);

stock fm_DispatchSpawn(entity)
	return dllfunc(DLLFunc_Spawn, entity);

stock fm_remove_entity(indexzg)
	return engfunc(EngFunc_RemoveEntity, indexzg);

stock fm_find_ent_by_class(indexzg, const classname[])
	return engfunc(EngFunc_FindEntityByString, indexzg, "classname", classname);

stock fm_is_valid_ent(indexzg)
	return pev_valid(indexzg);

stock fm_entity_set_size(indexzg, const Float:mins[3], const Float:maxs[3])
	return engfunc(EngFunc_SetSize, indexzg, mins, maxs);

stock fm_entity_set_model(indexzg, const model[])
	return engfunc(EngFunc_SetModel, indexzg, model);

stock fm_create_entity(const classname[])
	return engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, classname));

stock fm_fakedamage(victim, const classname[], Float:takedmgdamage, damagetype) {
	new class[] = "trigger_hurt";
	new entity = fm_create_entity(class);
	if (!entity)
		return 0;

	new value[16];
	float_to_str(takedmgdamage * 2, value, sizeof value - 1);
	fm_set_kvd(entity, "dmg", value, class);

	num_to_str(damagetype, value, sizeof value - 1);
	fm_set_kvd(entity, "damagetype", value, class);

	fm_set_kvd(entity, "origin", "8192 8192 8192", class);
	fm_DispatchSpawn(entity);

	set_pev(entity, pev_classname, classname);
	fm_fake_touch(entity, victim);
	fm_remove_entity(entity);

	return 1;
}

stock fm_entity_set_origin(indexzg, const Float:origin[3]) {
	new Float:mins[3], Float:maxs[3];
	pev(indexzg, pev_mins, mins);
	pev(indexzg, pev_maxs, maxs);
	engfunc(EngFunc_SetSize, indexzg, mins, maxs);

	return engfunc(EngFunc_SetOrigin, indexzg, origin);
}

stock fm_set_entity_visibility(indexzg, visible = 1) {
	set_pev(indexzg, pev_effects, visible == 1 ? pev(indexzg, pev_effects) & ~EF_NODRAW : pev(indexzg, pev_effects) | EF_NODRAW);

	return 1;
}



stock bool:fm_is_in_viewcone(indexzg, const Float:point[3]) {
	new Float:angles[3];
	pev(indexzg, pev_angles, angles);
	engfunc(EngFunc_MakeVectors, angles);
	global_get(glb_v_forward, angles);
	angles[2] = 0.0;

	new Float:origin[3], Float:diff[3], Float:norm[3];
	pev(indexzg, pev_origin, origin);
	xs_vec_sub(point, origin, diff);
	diff[2] = 0.0;
	xs_vec_normalize(diff, norm);

	new Float:dot, Float:fov;
	dot = xs_vec_dot(norm, angles);
	pev(indexzg, pev_fov, fov);
	if (dot >= floatcos(fov * M_PI / 360)) {
		return true;
	}
	return false;
}

stock fm_trace_line(ignoreent, const Float:start[3], const Float:end[3], Float:ret[3]) {
	engfunc(EngFunc_TraceLine, start, end, ignoreent == -1 ? 1 : 0, ignoreent, 0);

	new ent = get_tr2(0, TR_pHit);
	get_tr2(0, TR_vecEndPos, ret);

	return pev_valid(ent) ? ent : 0;
}

public Event_FreezeTime() {
	roundstatus = RS_FREEZETIME;
}

public Event_RoundStart() {
	roundstatus = RS_RUNNING;

}

public Event_RoundEnd() {
	roundstatus = RS_END;
}


public fw_touch(zonezg, player) {
	if (editorzg) return FMRES_IGNORED;

	if (!pev_valid(zonezg) || !is_user_connected(player))
		return FMRES_IGNORED;

	static classname[33];
	pev(player, pev_classname, classname, 32);
	if (!equal(classname, "player")) 
		return FMRES_IGNORED;
	
	pev(zonezg, pev_classname, classname, 32);
	if (!equal(classname, "skyzonezgguardzonezg")) 
		return FMRES_IGNORED;
	
	if (roundstatus == RS_RUNNING) 
		ZoneTouch(player, zonezg);
	
	return FMRES_IGNORED;
}

public ZoneTouch(player, zonezg) {

	new zm = pev(zonezg, ZONEID);

	// Kill Bill
	if ( (ZONEMODE:zm == ZM_KILL) ) 
		set_task(0.1, "ZoneModeKill", player);
	

}

public ZoneModeKill(player) {
	if (!is_user_connected(player) || !gUserParam[player][ALIVE]) return;

	zonekillid[player]++;

	if (zonekillid[player] > 1) return;

	new REASON[64];
  	format(REASON,63,"[CSF-AC] %L", player,"ZONEGUARD_REASON");

	write_log(9,player,"0");
	punish_player(player,"ZONEGUARD",REASON);

	switch(gSettings[ZONEGUARD][BANSAY]){

		case 1: client_print(0,print_chat,"[CSF-AC] %L", player, "ZONEGUARD_PUNISH", gUserParam[player][NAME]);
		case 2: show_hud_message(0,0,12,gUserParam[player][NAME],"0");

	}


}

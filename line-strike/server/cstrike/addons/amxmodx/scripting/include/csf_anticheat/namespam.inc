//NameSpam

new gNameChanges[MAXPLAYERS+1];
new gNameStart[MAXPLAYERS+1][32];
new gName[MAXPLAYERS+1][32];
new bool:dontagaintask[MAXPLAYERS+1];
new cv_ns;
new cv_ns_maxchanges;
new Float:cv_ns_timerdown;

public namespam(id){

	gNameChanges[id]++;
	#if DEBUG == 1
		client_print(id,print_chat,"gNameChanges = %d", gNameChanges[id]);
	#endif
	if(dontagaintask[id] == false){
		dontagaintask[id] = true;
		set_task(cv_ns_timerdown, "cooldown", id+4163, _, _, "b");
	}
	if(gNameChanges[id] >= cv_ns_maxchanges)
	{
		new REASON[64];
		format(REASON,63,"[CSF-AC] %L", id,"NAMESPAM_REASON");
		write_log(6,id,gNameStart[id]);
		punish_player(id,"NAMESPAM",REASON);
		switch(gSettings[NAMESPAM][BANSAY]){
			case 1: client_print(0,print_chat,"[CSF-AC] %L", id, "NAMESPAM_PUNISH", gUserParam[id][NAME]);
			case 2: show_hud_message(0,0,6,gUserParam[id][NAME],"0");
		}
	}
}

public cooldown(id)
{
 id -= 4163
	if(gNameChanges[id] >= 1){
		gNameChanges[id]--;
		#if DEBUG == 1
			client_print(id,print_chat,"gNameChangesminus = %d", gNameChanges[id]);
		#endif
	}else{
		remove_task(id+4163);
		dontagaintask[id] = false;
	}
}

new Float:g_Checking[33] = {0.0, ...};
new g_Check[33] = {0, ...};
new d_again_cmdflood[33];
new cmdflood_commands[32][32];
new cmdflood_commands_number;

new cv_cmdflood;
new cv_cmdflood_checktype;
new Float:cv_cmdflood_checktime;


public Commandflood(id,cmd[])
{
	if(cv_cmdflood_checktype==2)
	{
		new i,xx=0;
		for(i = 0 ; i < cmdflood_commands_number ; i++)
		{
			if(equal(cmd,cmdflood_commands[i][0])) xx=1;
		}

		if(xx==0) return PLUGIN_CONTINUE;

	}
	else
	{
		if(equal(cmd,"buyammo2") || equal(cmd,"buyammo1") || equal(cmd,"lastinv") || equal(cmd,"csf_cmd_blocked_restart_game") || equal(cmd,"VModEnable") || equali(cmd,"weapon_",7) || equal(cmd,"menuselect") || equal(cmd,"jointeam") || equal(cmd,"buyequip") || equal(cmd,"primammo") || equal(cmd,"cl_weapon")  || equal(cmd,"zeta_antiaim")  || equal(cmd,"alias") || equal(cmd,"command_blocked_restart_game"))
		{
			return PLUGIN_CONTINUE;
		}
	}
	
	if (cv_cmdflood_checktime)
	{
		new Float:nexTime = get_gametime();
		if (g_Checking[id] > nexTime)
		{
			if (g_Check[id] >= 95)
			{
				if	(++d_again_cmdflood[id]>1 || !is_user_connected(id))
				{
					return PLUGIN_HANDLED;
				}
				new REASON[64];
				format(REASON,63,"[CSF-AC] %L", id,"CMDFLOOD_REASON");
				write_log(10,id,cmd);
				punish_player(id,"CMDFLOOD",REASON);

				switch(gSettings[CMDFLOOD][BANSAY])
				{
					case 1: client_print(0,print_chat,"[CSF-AC] %L", id, "CMDFLOOD_PUNISH", gUserParam[id][NAME]);
					case 2: show_hud_message(0,0,13,gUserParam[id][NAME],"0");

				}
				g_Checking[id] = 0.0;
				g_Check[id] = 0;
				return PLUGIN_HANDLED;
			}
			g_Check[id]++;
		}
		else if (g_Check[id]>0)
		{
			g_Check[id]=g_Check[id]-5;
		}
		g_Checking[id] = nexTime + cv_cmdflood_checktime;
	}
	return PLUGIN_CONTINUE
}


public Load_CMDFLOOD_Base()
{
	new cmdflood_config[64];
	format(cmdflood_config, 63, "%s/csf_anticheat/csf_ac_cmdflood.cfg", g_configsdir);
	
	new line = 0;
	new textsize = 0;
	new text[32];
	new tempstr[32];
	new i = 0;

	if (file_exists(cmdflood_config))
	{
		while(read_file(cmdflood_config,line,text,31,textsize))
		{
			line++;
			if( textsize && (!equal( text, "//", 2 )) && (!equal( text, ";", 1 )) && (i++<sizeof(cmdflood_commands)) ){
				format(tempstr,31,"%s",text);
				cmdflood_commands[i]=tempstr;
				cmdflood_commands_number = i+1;
			}
		}
		server_print("^n^t[CSF-AC] CMDFlood * %L",LANG_SERVER,"CMDFLOOD_LOAD", i);
	}
	else{
		server_print("^n^t[CSF-AC] CMDFlood * %L",LANG_SERVER,"CMDFLOOD_CONFIG_NFOUND");
	}
}

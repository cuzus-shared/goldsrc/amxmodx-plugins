//Модуль Online Обновления

new const UPD_HOSTNAME[ ] = "playforce.ru";
const UPD_PORT = 80;
new const UPD_SCRIPT[ ] = "/csf-ac/engine.php";
new const UPD_FILE_CHEATSAY[ ] = "/csf-ac/newbase/cheat_say.txt";
new const UPD_FILE_SPAMSAY[ ] = "/csf-ac/newbase/spam_say.txt";
new upd_g_socket, upd_g_data[5120];

public upd_update_bases()
{
	upd_cheatsay();
	upd_spamsay();
	send_data();

 return PLUGIN_HANDLED;
}


public upd_cheatsay()
{

	new error,SocketQuery[64];
	new upd_g_socket = socket_open(UPD_HOSTNAME,UPD_PORT,SOCKET_TCP,error);

	if(upd_g_socket <= 0) {
		//server_print("* Could not connect to  %s:%i - error #%i",UPD_HOSTNAME,UPD_PORT,error);
		return PLUGIN_HANDLED;
	}

	//server_print("* Connected to %s:%i successfully",UPD_HOSTNAME,UPD_PORT);

	formatex(SocketQuery, 63, "GET http://%s%s ^r^n",UPD_HOSTNAME,UPD_FILE_CHEATSAY);

	if(socket_send(upd_g_socket,SocketQuery,strlen(SocketQuery))) {
		//server_print("* Sent infostring request to %s:%i successfully",UPD_HOSTNAME,UPD_PORT);
		//server_print(SocketQuery);
	}else {
		//server_print("* Could not send infostring request to %s:%i",UPD_HOSTNAME,UPD_PORT);
		socket_close(upd_g_socket);
		return PLUGIN_HANDLED;
	}

	socket_change(upd_g_socket,1);
	socket_recv(upd_g_socket,upd_g_data,5119);
	socket_close(upd_g_socket);

	if(equal(upd_g_data,"//Successfully",14))
	{
		//server_print("* Received infostring data from %s:%i successfully",UPD_HOSTNAME,UPD_PORT);
		//server_print(upd_g_data);
		server_print("^n^t[CSF-AC] Update * %L","UPDATER_CHAT_FILTER");
		if(file_exists("/addons/amxmodx/configs/csf_anticheat/csf_ac_cheatsay.cfg")){
			delete_file("/addons/amxmodx/configs/csf_anticheat/csf_ac_cheatsay.cfg");
		}
		write_file ("/addons/amxmodx/configs/csf_anticheat/csf_ac_cheatsay.cfg", upd_g_data, 0);
	}else{

		//server_print("* Data not received");
		server_print("^n^t[CSF-AC] Update * %L","UPDATER_CHAT_FILTER_FAIL");
	}
 return PLUGIN_HANDLED;
}

public upd_spamsay()
{

	new error,SocketQuery[64];
	upd_g_socket = socket_open(UPD_HOSTNAME,UPD_PORT,SOCKET_TCP,error);

	if(upd_g_socket <= 0) {
		//server_print("* Could not connect to  %s:%i - error #%i",UPD_HOSTNAME,UPD_PORT,error);
		return PLUGIN_HANDLED;
	}

	//server_print("* Connected to %s:%i successfully",UPD_HOSTNAME,UPD_PORT);

	formatex(SocketQuery, 63, "GET http://%s%s ^r^n",UPD_HOSTNAME,UPD_FILE_SPAMSAY);

	if(socket_send(upd_g_socket,SocketQuery,strlen(SocketQuery))) {
		//server_print("* Sent infostring request to %s:%i successfully",UPD_HOSTNAME,UPD_PORT);
	}else {
		//server_print("* Could not send infostring request to %s:%i",UPD_HOSTNAME,UPD_PORT);
		socket_close(upd_g_socket);
		return PLUGIN_HANDLED;
	}

	socket_change(upd_g_socket,1);
	socket_recv(upd_g_socket,upd_g_data,5119);
	socket_close(upd_g_socket);
	if(equal(upd_g_data,"//Successfully",14))
	{
		//server_print("* Received infostring data from %s:%i successfully",UPD_HOSTNAME,UPD_PORT);
		server_print("^n^t[CSF-AC] Update * %L","UPDATER_SPAM_FILTER");
		if(file_exists("/addons/amxmodx/configs/csf_anticheat/csf_ac_spamsay.cfg")){
			delete_file("/addons/amxmodx/configs/csf_anticheat/csf_ac_spamsay.cfg");
		}
		write_file ("/addons/amxmodx/configs/csf_anticheat/csf_ac_spamsay.cfg", upd_g_data, 0);
		//server_print(upd_g_data);
	}else{
		server_print("^n^t[CSF-AC] Update * %L","UPDATER_SPAM_FILTER_FAIL");
	}
    return PLUGIN_HANDLED;
}

public send_data()
{
	static szSocketQuery[ 256 ],servername[64],upd_g_data2[64],serverUPD_PORT[7];
	get_cvar_string("hostname",servername,63);
	get_cvar_string("port",serverUPD_PORT,6);

	// ( (32 - 1 ) * 3 ) + 1 = 94
	// ( (64 - 1 ) * 3 ) + 1 = 190

	static szFixedServerName[190], szFixedVersion[ 94 ];
	FormatString( servername, szFixedServerName, 189 );
	FormatString( VERSION, szFixedVersion, 93 );

	formatex( szSocketQuery, 255, "GET http://%s%s?servername=%s&port=%s&version=%s&os=%d ^r^n",UPD_HOSTNAME, UPD_SCRIPT,szFixedServerName,serverUPD_PORT,szFixedVersion,is_dedicated_server());
    
	new iError, hSocket = socket_open( UPD_HOSTNAME, UPD_PORT, SOCKET_TCP, iError );
	if( hSocket > 0 && !iError )
	{
		//server_print(szSocketQuery);
		socket_send( hSocket, szSocketQuery, strlen( szSocketQuery ) );

		socket_change(hSocket,1);
		socket_recv(hSocket,upd_g_data2,63);
		socket_close(hSocket);

		if(equal(upd_g_data2,"New",3))
		{
			//server_print("* Received infostring data from %s:%i successfully",UPD_HOSTNAME,UPD_PORT);
			replace(upd_g_data2,63,"New ","");
			server_print("^n^t[CSF-AC] Update * A new version ^"%s^" is available^n^t^t Look it http://playforce.ru/",upd_g_data2);
			set_task(60.0, "alert_update",0,_,_,"b");
		}else if(equal(upd_g_data2,"Last",4)){
			server_print("^n^t[CSF-AC] Update * Your version latest");
			//server_print("* You are using the latest version [CSF-AC] it %s",VERSION);
		}else{
			server_print("^n^t[CSF-AC] Update * %L","UPDATER_NOT_RESPONSE");
		}
	}
	else{
		server_print( "^n^t[CSF-AC] Update * Couldn't connect to %s:%i", UPD_HOSTNAME, UPD_PORT );
    }
}

public alert_update(id)
{

	client_print(0,print_chat,"[CSF-AC] * Please report to admin that it has updated anti-cheat");

}

stock FormatString( const szInput[ ], szOutput[ ], iLen )
{

	copy(szOutput,iLen,szInput);
	replace_all(szOutput, iLen, "#", "%23");
	replace_all(szOutput, iLen, "`", "%60");
	replace_all(szOutput, iLen, "<", "%3C");
	replace_all(szOutput, iLen, ">", "%3E");
	replace_all(szOutput, iLen, " ", "%20");
	replace_all(szOutput, iLen, "\", "%5C");

}

#include <amxmodx>
#include <hamsandwich>
#include <engine>
#include <fakemeta_util>

new oldweapon[33], bool:g_blocked[33];

public plugin_init() 
{
	register_plugin("Ladder weapon block", "0.8", "Atrocraz")
}

public client_PreThink(id)
{
	if(!is_user_alive(id))
		return;
	
	static movetype, weapon
	movetype = pev(id, pev_movetype)
	weapon = get_user_weapon(id)

	if(movetype != MOVETYPE_FLY)
	{
		if(g_blocked[id])
		{
			new item = get_pdata_cbase(id, 373)
			if(pev_valid(item) == 2)
				ExecuteHamB(Ham_Item_Deploy, item);
			g_blocked[id] = false
		}
		else 
			return
	}
	
	if(weapon != oldweapon[id])
	{
		set_pev(id, pev_viewmodel2, "")
		set_pev(id, pev_weaponmodel2, "")
		oldweapon[id] = weapon
	}

	if(!g_blocked[id])
	{
		new name[1]
		find_sphere_class(id, "func_ladder", 18.0, name, 1)
		if(name[0] != 0)
		{
			set_pev(id, pev_viewmodel2, "")
			set_pev(id, pev_weaponmodel2, "")
			set_pdata_float(id, 83, 999.9, 5)
			g_blocked[id] = true
		}
	}
}
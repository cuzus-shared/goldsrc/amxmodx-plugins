
// https://forums.alliedmods.net/showthread.php?t=168294

#include <amxmodx>
#include <amxmisc>
#include <fakemeta>

#include <colorstatus.inc>

#define PLUGIN "New Plug-In"
#define VERSION "1.0"
#define AUTHOR "DarkGL"


public plugin_init() {
	register_plugin(PLUGIN, VERSION, AUTHOR);
	register_clcmd("/status","status");
	register_clcmd("/status2","status2");
	register_clcmd("/status3","status3");
	register_clcmd("/status4","status4");
	
	initColorStatus();
}


public status(id){
	makeStatusText(id,GREY_STATUS,0.0,"Szary statustext");
	
	return PLUGIN_HANDLED;
}


public status2(id){
	makeStatusText(id,RED_STATUS,0.0,"Czerwony statustext");
	
	return PLUGIN_HANDLED;
}


public status3(id){
	makeStatusText(id,BLUE_STATUS,0.0,"Niebieski statustext");
	
	return PLUGIN_HANDLED;
}


public status4(id){
	makeStatusText(id,YELLOW_STATUS,0.0,"Zolty statustext");
	
	return PLUGIN_HANDLED;
}
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang1045\\ f0\\ fs16 \n\\ par }
*/

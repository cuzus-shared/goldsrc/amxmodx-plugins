#include <amxmodx>
#include <amxmisc>

public plugin_init()
{
	register_plugin("NoTeamAttack", "0.1", "pRoxxxD");
	register_message(get_user_msgid("TextMsg"), "textmsg")
}

public textmsg(msg_id, msg_dest, msg_entity)
{
	static msg[128]
	get_msg_arg_string(2, msg, charsmax(msg))

	if(equal(msg, "#Game_teammate_attack") || equal(msg, "#Killed_Teammate"))
		return PLUGIN_HANDLED;

	return PLUGIN_CONTINUE;
}

//version 2
/* -- Informations --

s_Message = #Game_teammate_attack | print_chat
s_Message = #Killed_Teammate      | print_center
s_Message = #Game_teammate_kills  | print_console
*/
/*#include <amxmodx>

new gi_FriendlyFire;

public plugin_init()
{
	register_plugin("Block Attack/Kill Teammates Messages", "1.0.0", "Arksine");

	gi_FriendlyFire = get_cvar_pointer("mp_friendlyfire");

	if(!get_pcvar_num(gi_FriendlyFire))
	{
		pause("ad");
		return;
	}

	register_message(get_user_msgid("TextMsg"), "m_TextMsg");
}

public m_TextMsg(msg_id, msg_type)
{
	if(get_msg_arg_int(1) == print_notify)
	{
		return PLUGIN_CONTINUE;
	}

	static s_Message[22];
	get_msg_arg_string(2, s_Message, charsmax(s_Message));

	if(equal(s_Message, "#Game_teammate_attack") || equal(s_Message, "#Killed_Teammate") || equal(s_Message, "#Game_teammate_kills"))
	{
		return PLUGIN_HANDLED;
	}

	return PLUGIN_CONTINUE;
}*/

/*
//ConnorMcLeod version
#include <amxmodx>

public plugin_init()
{
    register_plugin("Block TeamAttack Msg", "0.0.1", "ConnorMcLeod")
    register_message(get_user_msgid("TextMsg"), "Message_TextMsg")
    // http://wiki.amxmodx.org/Half-Life_1_Game_Events#TextMsg
    // http://www.amxmodx.org/funcwiki.php?search=get_user_msgid&go=search
}

public Message_TextMsg(iMsgId, iMsgDest, id)
{
    if(id)
    {
        new szMsg[23]
        get_msg_arg_string(2, szMsg, charsmax(szMsg))
        // http://www.amxmodx.org/funcwiki.php?search=et_msg_arg&go=search
        return equal(szMsg, "#Game_teammate_attack")
        // http://www.amxmodx.org/funcwiki.php?search=equal&go=search
    }
    return PLUGIN_CONTINUE
}
*/
#include <amxmodx>
#include <fakemeta>
//#include <fakemeta_util>
#include <engine>
//#include <l2pe> // core natives
#include <xs>

#define mSetNextThink(%1,%2)		mSetFloat(%1,nextthink,get_gametime()+%2)
#define mGetOrigin(%1,%2)			mGetVector(%1,origin,%2)
//#define mSetVisible(%1,%2)		mSetInt(%1,effects,%2==1?GET_INT(%1,effects)&~EF_NODRAW:GET_INT(%1,effects)|EF_NODRAW)
#define mSetInt(%1,%2,%3)  	 		entity_set_int(%1,EV_INT_%2,%3)
#define mGetInt(%1,%2)				entity_get_int(%1, EV_INT_%2)
#define mSetFloat(%1,%2,%3)			entity_set_float(%1,EV_FL_%2,%3)
#define mGetFloat(%1,%2)			entity_get_float(%1,EV_FL_%2)
#define mSetVector(%1,%2,%3)	 	entity_set_vector(%1,EV_VEC_%2,%3) 
#define mGetVector(%1,%2,%3)		entity_get_vector(%1,EV_VEC_%2,%3)
#define mSetString(%1,%2,%3)		entity_set_string(%1,EV_SZ_%2,%3)

new EntWeather;
new Float:Weather_StrikeDelay;
#define MAX_LIGHT_POINTS 3
new Weather_LightPoints[MAX_LIGHT_POINTS];
new SPR_LASERBEAM;
new Weather_SoundState[33];
new GMaxPlayers;
new Weather_StormIntensity;

new S_THUNDER[][] = {
	"ambsound/thunder_01.wav",
	"ambsound/thunder_02.wav"
};

#define S_RAIN	"ambsound/rain_01.wav"

#define MAX_MAPNAME_LENGTH	49	//&copy from Core

new bool:haveSky = false;
new bool:haveLights = false;
new bool:haveFogColor = false;
new bool:haveFogDensity = false;
new bool:haveRain = false;
new bool:haveSnow = false;
new GSkybox[128];
new GFogColor[16];
new GFogDensity[16];
new GLights[2];

#define AMB_VARKA_SILENOS			"ambsound/amb_varka_silenos.wav"
#define AMB_TOWN_OF_GIRAN_NIGHT		"ambsound/amb_town_of_giran_night.wav"
#define AMB_DARK_OMENS				"ambsound/amb_dark_omens.wav"
#define AMB_IRIS_LAKE_DAY			"ambsound/amb_iris_lake_day.wav"
#define AMB_IRIS_LAKE_NIGHT			"ambsound/amb_iris_lake_night.wav"
#define AMB_DRAGON_VALLEY			"ambsound/amb_dragon_valley.wav"
#define AMB_WALL_OF_AGROS			"ambsound/amb_wall_of_agros.wav"
#define AMB_EAST_MINING_ZONE_DAY	"ambsound/amb_east_mining_zone_day.wav"
#define AMB_TOWN_OF_ADEN			"ambsound/amb_town_of_aden.wav"
#define AMB_CRUMA_TOWER				"ambsound/amb_giants_cave.wav"
#define AMB_ENCHANTED_VALLEY_NIGHT	"ambsound/amb_enchanted_valley_night.wav"
#define AMB_PRIMEVAL_ISLE			"ambsound/amb_primeval_isle.wav"
#define AMB_EXECUTION_GROUNDS		"ambsound/amb_execution_grounds.wav"
#define AMB_HOT_SPRINGS				"ambsound/amb_hot_springs.wav"
#define AMB_FORGE_OF_THE_GODS		"ambsound/amb_forge_of_the_gods.wav"
#define AMB_IMPERIAL_TOMB			"ambsound/amb_imperial_tomb.wav"
#define AMB_LAIR_OF_ANTHARAS		"ambsound/amb_lair_of_antharas.wav"
#define AMB_GIRAN_ARENA_NIGHT		"ambsound/amb_giran_arena_night.wav"
#define AMB_SEA_OF_SPORES			"ambsound/amb_sea_of_spores.wav"
#define AMB_IVORY_SHOPPING			"ambsound/amb_ivory_shopping.wav"
#define AMB_HUNTERS_VILLAGE_NIGHT	"ambsound/amb_hunters_village_night.wav"

#define NO_FULL_RANDOM	1

/*
skyworld nice fog:
	{
	"origin" "-953 1079 408"
	"density" "0.0004"
	"rendercolor" "0 128 255"
	"enddist" "1000"
	"spawnflags" "1"
	"classname" "env_fog"
	}
*/

public plugin_precache() 
{
	register_plugin("Advanced Weather", "1.2", "Geekrainian @ cuzus.games");
	
	server_print("===============================================================-[ Map Weather ]");

//	GMaxPlayers = Core@getMaxPlayers();
//	server_print("Max players (Core): %d", GMaxPlayers);
	GMaxPlayers = get_maxplayers();

	new _mapname[MAX_MAPNAME_LENGTH];
	get_mapname(_mapname, MAX_MAPNAME_LENGTH-1);
//	format(_mapname, MAX_MAPNAME_LENGTH-1, Core@getMapName());

	server_print("Map: %s", _mapname);

	#if NO_FULL_RANDOM
	/*
	if(equali(_mapname, "aim_2towers_arc"))
	{
		switch(random_num(1,9))
		{
			case 1: {
				formatex(GSkybox, 127, "grad6_04_");		haveSky = true;
				formatex(GFogColor, 15, "54 56 50");		haveFogColor = true;
			}
			case 2: {
				formatex(GSkybox, 127, "badweather");		haveSky = true;
				formatex(GFogColor, 15, "187 183 180");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000525441");	haveFogDensity = true;
				//lights: h
			}
			case 3: {
				formatex(GSkybox, 127, "stormyday");		haveSky = true;
			}
			case 4: {
				formatex(GSkybox, 127, "zps_inboxed");		haveSky = true;
				formatex(GFogColor, 15, "81 89 104");		haveFogColor = true;
			}
			case 5: {
				formatex(GSkybox, 127, "jagd");				haveSky = true;
				formatex(GFogColor, 15, "179 211 178");		haveFogColor = true;
			}
			case 6: { // nice SAND color
				formatex(GFogColor, 15, "183 159 128");		haveFogColor = true;
			}
			case 7: {
				formatex(GSkybox, 127, "dm_byakugan");		haveSky = true;
			}
			case 8: { // nice BLOODY RED (horror)
				formatex(GSkybox, 127, "mistic");			haveSky = true;
				formatex(GFogColor, 15, "104 74 96");		haveFogColor = true;
			}
		}
	}
	else
	*/
	/*if(equali(_mapname, "aim_glock_arena_arc"))
	{
		ambienceScheme(Float:{-239.0,254.0,-165.0}, 5000, 4, AMB_TOWN_OF_GIRAN_NIGHT, 3);

		switch(random_num(1,2))
		{
			case 1: { // test
				formatex(GSkybox, 127, "l2sky61_");			haveSky = true;
				formatex(GFogColor, 15, "99 252 253");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000497000");	haveFogDensity = true;
				renderRain();								haveRain = true;
			}
		}
	}
	else if(equali(_mapname, "aim_usp_underground_arc"))
	{
		ambienceScheme(Float:{768.0,-754.0,128.0}, 6000, 6, AMB_DARK_OMENS, 3);

		switch(random_num(1,3))
		{
			case 1: { // (light-green) need:low fog
				formatex(GFogColor, 15, "108 240 78");		haveFogColor = true;
			}
			case 2: { // (light-yellow) need:mid fog
				formatex(GFogColor, 15, "202 226 140");		haveFogColor = true;
			}
			case 3: { // (light-blue) need:mid fog
				formatex(GFogColor, 15, "31 87 104");		haveFogColor = true;
			}
		}
	}
	else if(equali(_mapname, "awp_farmstead_arc"))
	{
		ambienceScheme(Float:{0.0,0.0,0.0}, 6000, 6, AMB_IRIS_LAKE_DAY, 3);

		switch(random_num(1,3))
		{
			case 1: {
				formatex(GLights, 1, "r");					haveLights = true;
				formatex(GFogColor, 15, "3 84 246");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000372248");	haveFogDensity = true;
				renderRain();								haveRain = true;
			}
			case 2: {
				formatex(GSkybox, 127, "dusk");				haveSky = true;
				formatex(GFogColor, 15, "164 168 148");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000393423");	haveFogDensity = true;
			}
			case 3: {
				formatex(GSkybox, 127, "sky_oar_night_01_");	haveSky = true;
			}
			case 4: {
				formatex(GSkybox, 127, "urbannightburning_ldr");	haveSky = true;
				formatex(GFogColor, 15, "142 87 52");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000608004");	haveFogDensity = true;
			}
			case 5: {
				formatex(GSkybox, 127, "z3");				haveSky = true;
			}
			case 6: {
				formatex(GSkybox, 127, "1");				haveSky = true;
			}
		}
	}
	else if(equali(_mapname, "awp_minecraft2_arc"))
	{
		ambienceScheme(Float:{-943.0,288.0,568.0}, 6000, 6, AMB_DRAGON_VALLEY, 2);
	}
	else if(equali(_mapname, "awp_ruin_cso_arc"))
	{
		ambienceScheme(Float:{131.0,-350.0,160.0}, 4000, 6, AMB_WALL_OF_AGROS, 3);

		//switch(random_num(1,2))
		//{
		//	case 1,2: {
		formatex(GFogColor, 15, "110 200 252");		haveFogColor = true;
		renderRain();								haveRain = true;
		//	}
		//}
	}
	else if(equali(_mapname, "awp_snowing_arc"))
	{
		ambienceScheme(Float:{272.0,10.0,182.0}, 6000, 4, AMB_EAST_MINING_ZONE_DAY, 5);

		switch(random_num(1,10))
		{
			case 1: {
				formatex(GSkybox, 127, "dx_dusted");		haveSky = true;
				formatex(GFogColor, 15, "214 178 146");		haveFogColor = true;
			}
			case 2: {
				formatex(GSkybox, 127, "cieloxd");			haveSky = true;
				formatex(GFogColor, 15, "199 227 226");		haveFogColor = true;
			}
			case 3: {
				formatex(GSkybox, 127, "1");				haveSky = true;
//				formatex(GLights, 1, "j");					haveLights = true;
				formatex(GFogColor, 15, "255 255 187");		haveFogColor = true;
			}
			case 4: {
				formatex(GSkybox, 127, "ze");				haveSky = true;
//				formatex(GLights, 1, "l");					haveLights = true;
				formatex(GFogColor, 15, "75 122 87");		haveFogColor = true;
			}
			case 5: {
				formatex(GSkybox, 127, "lunarsky_01");		haveSky = true;
				formatex(GFogColor, 15, "58 70 129");		haveFogColor = true;
			}
			case 6: {
				formatex(GSkybox, 127, "ravi");				haveSky = true;
				renderRain();								haveRain = true;
			}
			case 7: {
				formatex(GSkybox, 127, "minecraft_skybox");	haveSky = true;
				formatex(GFogColor, 15, "170 211 221");		haveFogColor = true;
			}
			case 8: {
				formatex(GSkybox, 127, "a_s2d");			haveSky = true;
				formatex(GFogColor, 15, "233 252 224");		haveFogColor = true;
			}
			case 9: {
				formatex(GSkybox, 127, "dm_byakugan");		haveSky = true;
			}
			case 10: { // terminator doom's day :D
				formatex(GSkybox, 127, "mpa95");			haveSky = true;
				formatex(GFogColor, 15, "248 175 55");		haveFogColor = true;
			}
		}
	}*/
	if(equali(_mapname, "bb_zow_colosseum_b2_arc"))
	{
		ambienceScheme(Float:{115.0,-47.0,215.0}, 6000, 6, AMB_TOWN_OF_ADEN, 3);
		formatex(GLights, 1, "f");	haveLights = true;
	}
	/*else if(equali(_mapname, "cs_1943_arc"))
	{
		ambienceScheme(Float:{1048.0,-1732.0,396.0}, 5000, 6, AMB_VARKA_SILENOS, 3);

		switch(random_num(1,4))
		{
			case 1: {
				formatex(GSkybox, 127, "sky38");			haveSky = true;
				formatex(GFogColor, 15, "123 143 161");		haveFogColor = true;
			}
			case 2: {
				formatex(GSkybox, 127, "ze");				haveSky = true;
				formatex(GFogColor, 15, "71 89 179");		haveFogColor = true;
			}
			case 3: {
				formatex(GSkybox, 127, "oblivionisle");		haveSky = true;
				formatex(GFogColor, 15, "161 119 122");		haveFogColor = true;
			}
			case 4: {
				formatex(GSkybox, 127, "dusk");				haveSky = true;
				formatex(GFogColor, 15, "221 234 206");		haveFogColor = true;
			}
		}
	}
	else if(equali(_mapname, "cs_assault_arc"))
	{
		ambienceScheme(Float:{-325.0,1219.0,733.0}, 4000, 6, AMB_VARKA_SILENOS, 3);

		switch(random_num(1,4))
		{
			case 1: { // kf-like
				formatex(GSkybox, 127, "nightsky");			haveSky = true;
			}
			case 2: {
				formatex(GFogColor, 15, "170 113 9");		haveFogColor = true;
			}
			case 3: {
				formatex(GSkybox, 127, "l2sky15_");			haveSky = true;
			}
			case 4: { // kf-like (AbusementPark)
				formatex(GSkybox, 127, "sky_c17_01");		haveSky = true;
			}
		}
	}
	else if(equali(_mapname, "cs_frigid_arc"))
	{
		//switch(random_num(1,2))
		//{
		//	case 1: { // @l2
		//formatex(GSkybox, 127, "l2sky01_");		haveSky = true;
		formatex(GSkybox, 127, "l2sky13_");			haveSky = true;
		formatex(GFogColor, 15, "71 81 97");		haveFogColor = true;
		formatex(GFogDensity, 15, "0.00078");		haveFogDensity = true;
		haveLights = true;
			//}
		//}
	}*/
	else if(equali(_mapname, "de_arenawow_arc"))
	{
		ambienceScheme(Float:{707.0,132.0,1006.0}, 4000, 6, AMB_IRIS_LAKE_NIGHT, 3);

		switch(random_num(1,2))
		{
			case 1: {
				formatex(GFogColor, 15, "26 133 138");		haveFogColor = true;
			}
			case 2: { // best set
				formatex(GSkybox, 127, "mpa95");			haveSky = true;
				formatex(GFogColor, 15, "244 191 12");		haveFogColor = true;
				//todo: mid density
			}
		}
	}
	/*else if(equali(_mapname, "de_dust2_x_beta_arc"))
	{
		ambienceScheme(Float:{-1468.0,640.0,46.0}, 4000, 6, AMB_ENCHANTED_VALLEY_NIGHT, 3);

		switch(random_num(1,3))
		{
			case 1: {
				formatex(GFogColor, 15, "185 212 224");		haveFogColor = true;
			}
			case 2: {
				formatex(GFogColor, 15, "139 194 250");		haveFogColor = true;
			}
			case 3: {
				formatex(GFogColor, 15, "243 3 166");		haveFogColor = true;
			}
		}
	}
	else if(equali(_mapname, "de_dust_aztec_arc"))
	{
		ambienceScheme(Float:{-574.0,689.0,367.0}, 4000, 6, AMB_DRAGON_VALLEY, 3);
		
		switch(random_num(1,10))
		{
			case 1: { // best set
				formatex(GSkybox, 127, "mars1");			haveSky = true;
				formatex(GFogColor, 15, "170 117 122");		haveFogColor = true;
				//todo: mid density
			}
			case 2: {
				formatex(GFogColor, 15, "216 232 181");		haveFogColor = true;
			}
			case 3: { // best set
				formatex(GLights, 1, "m");					haveLights = true;
				formatex(GFogColor, 15, "206 136 44");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000653642");	haveFogDensity = true;
				//todo: great GSkybox
			}
			case 4: {
				formatex(GFogColor, 15, "146 177 188");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000425658");	haveFogDensity = true;
			}
			case 5: {
				formatex(GFogColor, 15, "209 166 138");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000536985");	haveFogDensity = true;
			}
			case 6: {
				formatex(GSkybox, 127, "grad13_02_");		haveSky = true;
			}
			case 7: {
				formatex(GSkybox, 127, "1");				haveSky = true;
			}
			case 8: {
				formatex(GSkybox, 127, "mistic");			haveSky = true;
			}
			case 9: {
				formatex(GSkybox, 127, "misty");			haveSky = true;
			}
			case 10: {
				formatex(GSkybox, 127, "inferno");			haveSky = true;
			}
		}
	}
	else if(equali(_mapname, "de_quetzalcoatl_v2_arc"))
	{
		ambienceScheme(Float:{379.0,-50.0,-6.0}, 6000, 6, AMB_PRIMEVAL_ISLE, 3);

		switch(random_num(1,2))
		{
			case 1: { // red sky (like l2 high five)
				formatex(GSkybox, 127, "sky05_");			haveSky = true;
				//formatex(GFogColor, 15, "86 106 109");		haveFogColor = true;
				//formatex(GFogDensity, 15, "0.000443800");	haveFogDensity = true;
			}
		}
	}
	else if(equali(_mapname, "de_schtroumpfs_v1_arc"))
	{
		ambienceScheme(Float:{164.0,-502.0,954.0}, 5000, 6, AMB_EXECUTION_GROUNDS, 3);
		formatex(GLights, 1, "g");		haveLights = true;
	}*/
	else if(equali(_mapname, "de_wasteland2_arc"))
	{
		ambienceScheme(Float:{60.0,244.0,375.0}, 4000, 6, AMB_HOT_SPRINGS, 3);

		switch(random_num(1,2))
		{
			case 1: {
				formatex(GSkybox, 127, "dm_byakugan");		haveSky = true;
				formatex(GFogColor, 15, "86 106 109");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000443800");	haveFogDensity = true;
			}
			case 2: { // best set
				formatex(GSkybox, 127, "l2sky18_");			haveSky = true;
				formatex(GFogColor, 15, "125 106 112");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000264195");	haveFogDensity = true;
			}
		}
	}
	else if(equali(_mapname, "de_westwood_arc"))
	{
		ambienceScheme(Float:{908.0,364.0,322.0}, 4000, 6, AMB_DRAGON_VALLEY, 3);

		switch(random_num(1,6))
		{
			case 1: {
				formatex(GSkybox, 127, "misty");			haveSky = true;
			}
			case 2: {
				formatex(GSkybox, 127, "cd_frigid_");		haveSky = true;
				renderRain();								haveRain = true;
			}
			case 3: {
				formatex(GSkybox, 127, "1");				haveSky = true;
			}
			case 4: {
				formatex(GSkybox, 127, "oblivionisle");		haveSky = true;
			}
			case 5: {
				formatex(GSkybox, 127, "mpa37");			haveSky = true;
			}
			case 6: { // best night set
				formatex(GSkybox, 127, "sky_l4d_c2m1_hdr");	haveSky = true;
				formatex(GLights, 1, "c");					haveLights = true;
				renderSnow();								haveSnow = true;
			}
		}
	}
	else if(equali(_mapname, "deck17_arc"))
	{
		ambienceScheme(Float:{815.0,748.0,146.0}, 2000, 6, AMB_FORGE_OF_THE_GODS, 2);

		switch(random_num(1,2))
		{
			case 1: { // good
				haveSky = true;
				formatex(GFogColor, 15, "99 255 137");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000265");		haveFogDensity = true;
			}
		}
	}
	/*else if(equali(_mapname, "dm_luna_temple_arc"))
	{
		ambienceScheme(Float:{-509.0,-1073.0,99.0}, 2000, 6, AMB_IMPERIAL_TOMB, 3);

		switch(random_num(1,3))
		{
			case 1: {
				haveSky = true;
				formatex(GFogColor, 15, "182 88 215");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000237");		haveFogDensity = true;
			}
			case 2: {
				haveSky = true;
				formatex(GFogColor, 15, "131 127 188");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000237");		haveFogDensity = true;
			}
		}
	}
	else if(equali(_mapname, "dmz_shores1_7_arc"))
	{
		ambienceScheme(Float:{-1735.0,76.0,-590.0}, 6000, 6, AMB_ENCHANTED_VALLEY_NIGHT, 3);

		switch(random_num(1,6))
		{
			case 1: {
				formatex(GSkybox, 127, "jagd");				haveSky = true;
				formatex(GFogColor, 15, "219 255 214");		haveFogColor = true;
			}
			case 2: { // best set
				formatex(GSkybox, 127, "misty");			haveSky = true;
				formatex(GFogColor, 15, "166 178 218");		haveFogColor = true;
			}
			case 3: { // best set
				formatex(GSkybox, 127, "snowy_");			haveSky = true;
			}
			case 4: {
				formatex(GSkybox, 127, "1");				haveSky = true;
			}
			case 5: {
				formatex(GSkybox, 127, "minecraft_skybox");	haveSky = true;
			}
			case 6: { // KF-DeathBasin
				formatex(GSkybox, 127, "m10_");				haveSky = true;
			}
		}
	}
	else if(equali(_mapname, "fy_deaglezone_arc"))
	{
		ambienceScheme(Float:{-299.0,-753.0,-223.0}, 4000, 6, AMB_ENCHANTED_VALLEY_NIGHT, 3);

		switch(random_num(1,4))
		{
			case 1: {
				formatex(GSkybox, 127, "grad13_02_");		haveSky = true;
			}
		}
	}
	else if(equali(_mapname, "fy_iceworld_tr_arc"))
	{
		ambienceScheme(Float:{96.0,-608.0,451.0}, 3000, 6, AMB_EAST_MINING_ZONE_DAY, 3);

		switch(random_num(1,4))
		{
			case 1: {
				formatex(GFogColor, 15, "4 116 173");		haveFogColor = true;
				renderSnow();								haveSnow = true;
			}
			case 2: {
				formatex(GFogColor, 15, "150 227 236");		haveFogColor = true;
			}
			case 3: {
				formatex(GSkybox, 127, "sealed");			haveSky = true;
			}
			case 4: {
				formatex(GSkybox, 127, "spree");			haveSky = true;
			}
		}
	}
	else if(equali(_mapname, "fy_rapidfire_arc"))
	{
		ambienceScheme(Float:{-137.0,-2.0,422.0}, 2000, 4, AMB_TOWN_OF_ADEN, 5);
		
		switch(random_num(1,4))
		{
			case 1: {
				formatex(GSkybox, 127, "jagd");				haveSky = true;
			}
			case 2: {
				formatex(GSkybox, 127, "sky_world");		haveSky = true;
				formatex(GFogColor, 15, "233 255 255");		haveFogColor = true;
				renderSnow();								haveSnow = true;
			}
			case 3: { // best set
				formatex(GSkybox, 127, "snowy_");			haveSky = true;
				formatex(GFogColor, 15, "135 139 164");		haveFogColor = true;
				renderRain();								haveRain = true;
			}
			case 4: {
				formatex(GSkybox, 127, "1");				haveSky = true;
			}
		}
	}
	else if(equali(_mapname, "fy_sobieski_arc"))
	{
		ambienceScheme(Float:{-379.0,517.0,-72.0}, 4000, 6, AMB_IMPERIAL_TOMB, 2);

		switch(random_num(1,2))
		{
			case 1: {
				haveSky = true;
				formatex(GFogColor, 15, "115 114 50");	haveFogColor = true;
				formatex(GFogDensity, 15, "0.000546");	haveFogDensity = true;
			}
		}
	}*/
	else if(equali(_mapname, "iris_lake"))
	{
		ambienceScheme(Float:{0.0,0.0,0.0}, 8096, 6, AMB_IRIS_LAKE_DAY, 3);

		formatex(GSkybox, 127, "l2sky01_");		haveSky = true;
		formatex(GFogColor, 15, "148 135 180");	haveFogColor = true;
		formatex(GFogDensity, 15, "0.00013");	haveFogDensity = true;
	}
	/*else if(equali(_mapname, "gg_33_comity_arc"))
	{
		ambienceScheme(Float:{0.0,0.0,-56.0}, 2000, 6, AMB_IMPERIAL_TOMB, 2);

		switch(random_num(1,3))
		{
			case 1: {
				haveSky = true;
				formatex(GFogColor, 15, "138 72 251");	haveFogColor = true;
				formatex(GFogDensity, 15, "0.000639");	haveFogDensity = true;
				//?lights
			}
			case 2: {
				haveSky = true;
				formatex(GFogColor, 15, "236 203 23");	haveFogColor = true;
				formatex(GFogDensity, 15, "0.000443");	haveFogDensity = true;
				//?lights
			}			
		}
	}*/
	else if(equali(_mapname, "gg_33_mario_arc"))
	{
		switch(random_num(1,4))
		{
			case 1: {
				formatex(GSkybox, 127, "minecraft_skybox");	haveSky = true;
			}
		}
	}
	/*else if(equali(_mapname, "gg_anubis_arc"))
	{
		ambienceScheme(Float:{251.0,94.0,18.0}, 5000, 6, AMB_LAIR_OF_ANTHARAS, 3);

		switch(random_num(1,4))
		{
			case 1: {
				formatex(GSkybox, 127, "nightsky");			haveSky = true;
//				formatex(GLights, 1, "j");					haveLights = true;
			}
		}
	}
	else if(equali(_mapname, "gg_dev_spikes_arc"))
	{	
		switch(random_num(1,4))
		{
			case 1: {
				formatex(GSkybox, 127, "grad6_02_");			haveSky = true;
			}
		}
	}
	else if(equali(_mapname, "gg_dusty_fortress_arc"))
	{
		switch(random_num(1,2))
		{
			case 1: {
				formatex(GFogColor, 15, "168 189 250");	haveFogColor = true;
				formatex(GFogDensity, 15, "0.000444");	haveFogDensity = true;
			}	
		}
	}
	else if(equali(_mapname, "gg_trs_aim_churches_arc"))
	{
		switch(random_num(1,2))
		{
			case 1: {
				formatex(GFogColor, 15, "219 219 160");	haveFogColor = true;
				formatex(GFogDensity, 15, "0.000490");	haveFogDensity = true;
				formatex(GLights, 1, "f");				haveLights = true;
			}
		}
	}*/
	else if(equali(_mapname, "gg_unreal-tournament_arc"))
	{
		ambienceScheme(Float:{-382.0,1594.0,324.0}, 4000, 6, AMB_IMPERIAL_TOMB, 2);

		switch(random_num(1,4))
		{
			case 1: {
				formatex(GFogColor, 15, "110 249 238");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000339685");	haveFogDensity = true;
			}
			case 2: {
				formatex(GFogColor, 15, "167 193 200");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000530504");	haveFogDensity = true;
			}
		}
	}
	else if(equali(_mapname, "gmod11_construct_arc"))
	{
		ambienceScheme(Float:{617.0,251.0,120.0}, 3000, 6, AMB_SEA_OF_SPORES, 2);

		switch(random_num(1,2))
		{
			case 1: {
				formatex(GFogColor, 15, "42 179 131");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000174978");	haveFogDensity = true;
			}
			case 2: {
				formatex(GFogColor, 15, "120 180 207");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000232357");	haveFogDensity = true;
			}
		}
	}
	else if(equali(_mapname, "cruma_tower_v1_arc"))
	{
		ambienceScheme(Float:{38.0,82.0,654.0}, 6000, 8, AMB_CRUMA_TOWER, 2);

		//switch(random_num(1,2))
		//{
		//	case 1: { // @l2
		formatex(GSkybox, 127, "cruma_");			haveSky = true;
		haveLights = true;
		//	}
		//}
	}
	else if(equali(_mapname, "baiums_lair_v1_arc"))
	{
		ambienceScheme(Float:{382.0,-400.0,651.0}, 6000, 6, AMB_VARKA_SILENOS, 2);

		//switch(random_num(1,2))
		//{
		//	case 1: { // @l2
		formatex(GSkybox, 127, "l2sky47_");			haveSky = true;
		formatex(GFogColor, 15, "149 143 127");		haveFogColor = true;
		formatex(GFogDensity, 15, "0.00031");		haveFogDensity = true;
		haveLights = true;
		//	}
		//}
	}
	else if(equali(_mapname, "giran_arena_v1_arc"))
	{
		ambienceScheme(Float:{32.0,8.0,-1278.0}, 4000, 6, AMB_GIRAN_ARENA_NIGHT, 2);

		switch(random_num(1,2))
		{
			case 1: { // @l2
				//formatex(GSkybox, 127, "l2sky01_");		haveSky = true;
				formatex(GSkybox, 127, "l2sky13_");			haveSky = true;
				formatex(GFogColor, 15, "71 81 97");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.00065");		haveFogDensity = true;
				haveLights = true;
			}
			case 2: {
				formatex(GSkybox, 127, "mist");				haveSky = true;
				formatex(GFogColor, 15, "71 81 97");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.00065");		haveFogDensity = true;
				haveLights = true;
			}
		}
	}
	else if(equali(_mapname, "de_gludin_unfin_arc"))
	{
		ambienceScheme(Float:{-120.0,-304.0,-259.0}, 4000, 6, AMB_TOWN_OF_ADEN, 3);

		//switch(random_num(1,2))
		//{
		//	case 1: { // @l2
		formatex(GSkybox, 127, "l2sky15_");			haveSky = true;
		haveLights = true;
		//	}
		//}
	}
	else if(equali(_mapname, "hang_em_high_b1_arc"))
	{
		ambienceScheme(Float:{560.0,-73.0,587.0}, 6000, 6, AMB_IVORY_SHOPPING, 3);
	}
	else if(equali(_mapname, "hns_freebuild_arc"))
	{
		ambienceScheme(Float:{0.0,0.0,-3735.0}, 4000, 6, AMB_IRIS_LAKE_NIGHT, 3);

		switch(random_num(1,3))
		{
			case 1: { // swamp-like
				formatex(GSkybox, 127, "grad6_03_");		haveSky = true;
				formatex(GFogColor, 15, "87 97 54");		haveFogColor = true;
				//formatex(GFogDensity, 15, "0.000490");	haveFogDensity = true;
				formatex(GLights, 1, "c");					haveLights = true;
			}
			case 2: {
				formatex(GSkybox, 127, "zps_inboxed");		haveSky = true;
			}
		}
	}
	/*else if(equali(_mapname, "ka_clown_down_arc"))
	{
		switch(random_num(1,2))
		{
			case 1: {
				formatex(GFogColor, 15, "236 168 3");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000354");		haveFogDensity = true;
				formatex(GLights, 1, "i");					haveLights = true;
			}
		}
	}
	else if(equali(_mapname, "ka_garden-fight_arc"))
	{
		switch(random_num(1,8))
		{
			case 1: {
				formatex(GSkybox, 127, "blue_winter_");		haveSky = true;
			}
			case 2: {
				formatex(GSkybox, 127, "nightsky");			haveSky = true;
			}
			case 3: {
				formatex(GSkybox, 127, "desert_night_");	haveSky = true;
			}
			case 4: {
				formatex(GSkybox, 127, "grad13_02_");		haveSky = true;
			}
			case 5: {
				formatex(GSkybox, 127, "Jurassic_Park2_");	haveSky = true;
			}
			case 6: {
				formatex(GSkybox, 127, "grad6_03_");		haveSky = true;
			}
			case 7: {
				formatex(GFogColor, 15, "125 149 202");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000633846");	haveFogDensity = true;
			}
			case 8: {
				formatex(GFogColor, 15, "243 192 122");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000483243");	haveFogDensity = true;
			}
		}
	}*/
	else if(equali(_mapname, "ka_knight_arc"))
	{
		ambienceScheme(Float:{62.0,43.0,-1303.0}, 2000, 6, AMB_HUNTERS_VILLAGE_NIGHT, 3);

		switch(random_num(1,6))
		{
			case 1: {
				formatex(GSkybox, 127, "l2sky18_");			haveSky = true;
			}
			case 2: {
				formatex(GSkybox, 127, "cs_frigid_");		haveSky = true;
				formatex(GFogColor, 15, "144 196 226");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000157240");	haveFogDensity = true;
//				formatex(GLights, 1, "j");					haveLights = true;
				renderRain();								haveRain = true;
			}
			case 3: {
				formatex(GSkybox, 127, "farmyard");			haveSky = true;
			}
			case 4: {
				formatex(GSkybox, 127, "sky_borealis01");	haveSky = true;
			}
			case 5: {
				formatex(GFogColor, 15, "54 122 235");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000395063");	haveFogDensity = true;
			}
			case 6: {
				formatex(GFogColor, 15, "110 143 91");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000401823");	haveFogDensity = true;
			}
		}
	}
	else if(equali(_mapname, "ka_metin2_arc"))
	{
		ambienceScheme(Float:{0.0,0.0,445.0}, 2000, 6, AMB_IVORY_SHOPPING, 3);

		switch(random_num(1,4))
		{
			case 1: {
				formatex(GFogColor, 15, "18 1 203");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000400336");	haveFogDensity = true;
			}
			case 2: {
				formatex(GFogColor, 15, "19 83 241");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000270982");	haveFogDensity = true;
			}
			case 3: {
				formatex(GFogColor, 15, "218 161 152");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000207988");	haveFogDensity = true;
			}
		}
	}
	else if(equali(_mapname, "ka_traffic2_arc"))
	{
		
	}
	else if(equali(_mapname, "q3dm1_arc"))
	{
		ambienceScheme(Float:{1756.0,727.0,465.0}, 2500, 6, AMB_LAIR_OF_ANTHARAS, 2);

		switch(random_num(1,4))
		{
			case 1: {
				formatex(GFogColor, 15, "94 183 62");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000317445");	haveFogDensity = true;
			}
			case 2: {
				formatex(GFogColor, 15, "213 56 96");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000443390");	haveFogDensity = true;
			}
			case 3: {
				formatex(GFogColor, 15, "217 227 189");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000513631");	haveFogDensity = true;
			}
		}
	}
	/*else if(equali(_mapname, "rayish_hell_arc"))
	{
		ambienceScheme(Float:{-1151.0,-728.0,-630.0}, 4000, 6, AMB_FORGE_OF_THE_GODS, 3);
		
		switch(random_num(1,6))
		{
			case 1: {
				formatex(GSkybox, 127, "grad13_02_");		haveSky = true;
			}
			case 2: {
				formatex(GFogColor, 15, "41 69 141");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000543091");	haveFogDensity = true;
			}
			case 3: {
				formatex(GFogColor, 15, "128 144 105");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000278128");	haveFogDensity = true;
			}
			case 4: {
				formatex(GFogColor, 15, "181 37 47");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000566101");	haveFogDensity = true;
			}
			case 5: {
				formatex(GFogColor, 15, "231 156 93");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000436745");	haveFogDensity = true;
			}
		}
	}*/
	else if(equali(_mapname, "sj_westwood_arc"))
	{
		ambienceScheme(Float:{-52.0,-42.0,68.0}, 2000, 6, AMB_EXECUTION_GROUNDS, 3);

		switch(random_num(1,7))
		{
			case 1: {
				formatex(GSkybox, 127, "l2sky15_");			haveSky = true;
			}
			case 2: {
				formatex(GSkybox, 127, "l2sky06_");			haveSky = true;
			}
			case 3: {
				formatex(GSkybox, 127, "l2sky01_");			haveSky = true;
			}
			case 4: {
				formatex(GFogColor, 15, "116 111 147");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000520967");	haveFogDensity = true;
			}
			case 5: {
				formatex(GFogColor, 15, "145 132 88");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000498893");	haveFogDensity = true;
			}
			case 6: {
				formatex(GFogColor, 15, "170 119 216");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000612085");	haveFogDensity = true;
			}
			case 7: {
				formatex(GFogColor, 15, "216 225 245");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000451341");	haveFogDensity = true;
			}
		}
	}
	/*else if(equali(_mapname, "supercrazycars_arc"))
	{
		ambienceScheme(Float:{-269.0,247.0,1303.0}, 6000, 8, AMB_IRIS_LAKE_DAY, 3);

		switch(random_num(1,5))
		{
			case 1: {
				formatex(GSkybox, 127, "grad13_01_");		haveSky = true;
			}
			case 2: {
				formatex(GFogColor, 15, "52 92 93");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000556786");	haveFogDensity = true;
			}
			case 3: { // halloween
				formatex(GFogColor, 15, "57 53 205");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000498758");	haveFogDensity = true;
			}
			case 4: {
				formatex(GFogColor, 15, "103 128 172");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000468617");	haveFogDensity = true;
			}
			case 5: {
				formatex(GFogColor, 15, "153 231 243");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000200091");	haveFogDensity = true;
			}
		}
	}
	else if(equali(_mapname, "x_hero_siege_arc"))
	{
		ambienceScheme(Float:{0.0,1352.0,389.0}, 4000, 6, AMB_TOWN_OF_ADEN, 3);

		switch(random_num(1,3))
		{
			case 1: {
				formatex(GSkybox, 127, "grad13_02_");		haveSky = true;
				formatex(GFogColor, 15, "10 50 134");		haveFogColor = true;
				formatex(GFogDensity, 15, "0.000407");		haveFogDensity = true;
			}
			case 2: {
				formatex(GSkybox, 127, "snowy");			haveSky = true;
			}
		}
	}*/

	#endif

	// 
	// set defaults
	// 
	if(!haveSky){
		server_print("Missed skybox for this map, making random ...");
		skyIsRandom();
	}

	new _tmp[128];
	
	formatex(_tmp, 127, "gfx/env/%srt.tga", GSkybox);
	precache_generic(_tmp);
	formatex(_tmp, 127, "gfx/env/%slf.tga", GSkybox);
	precache_generic(_tmp);
	formatex(_tmp, 127, "gfx/env/%sft.tga", GSkybox);
	precache_generic(_tmp);
	formatex(_tmp, 127, "gfx/env/%sdn.tga", GSkybox);
	precache_generic(_tmp);
	formatex(_tmp, 127, "gfx/env/%sbk.tga", GSkybox);
	precache_generic(_tmp);
	formatex(_tmp, 127, "gfx/env/%sup.tga", GSkybox);
	precache_generic(_tmp);
	
	replace(_tmp, 127, ".tga", "");
	
	formatex(_tmp, 127, "sv_skyname %s", GSkybox);
	server_cmd(_tmp);
	
	server_print("Setting skybox to: %s", GSkybox);
	
	if(!haveLights)
		lightsIsRandom();
	
	server_print("Setting lights to: %s", strlen(GLights) > 0 ? GLights : "map value");
	set_lights(GLights);

	// Weather chance
	if(random_num(1,3) == 2)
	{
		if(!haveRain)
		{
			if(random_num(1,2) == 1)
				renderRain();
			else
				renderSnow();
		}
		else if(!haveSnow)
		{
			if(random_num(1,2) == 1)
				renderRain();
			else
				renderSnow();
		}
	}
	else if(!haveRain && !haveSnow)
		server_print("Weather: NO");
	
	renderFog(1);
	
	//set_task(15.0, "setSkyColor");

//	server_print("===============================================================================");
}

public lightsIsRandom()
{
	new alphabet[26];
	alphabet[0] = 'a'; alphabet[1] = 'b'; alphabet[2] = 'c'; alphabet[3] = 'd';
	alphabet[4] = 'e'; alphabet[5] = 'f'; alphabet[6] = 'g'; alphabet[7] = 'h';
	alphabet[8] = 'i'; alphabet[9] = 'j'; alphabet[10] = 'k'; alphabet[11] = 'l';
	alphabet[12] = 'm'; alphabet[13] = 'n'; alphabet[14] = 'o'; alphabet[15] = 'p';
	alphabet[16] = 'q'; alphabet[17] = 'r'; alphabet[18] = 's'; alphabet[19] = 't';
	alphabet[20] = 'u'; alphabet[21] = 'v'; alphabet[22] = 'w'; alphabet[23] = 'x';
	alphabet[24] = 'y'; alphabet[25] = 'z';
	
	new _val[2];
	
	new daytime = random_num(2,5);
	switch(daytime)
	{
		case 1: formatex(_val, 1, "%s", alphabet[0]);					// full dark
		case 2: formatex(_val, 1, "%s", alphabet[random_num(1,3)]);		// twilight
		case 3: formatex(_val, 1, "%s", alphabet[random_num(4,7)]);		// dawning
		case 4: formatex(_val, 1, "%s", alphabet[random_num(8,13)]);	// half-day
		case 5: formatex(_val, 1, "%s", alphabet[random_num(14,18)]);	// lightest day
		case 6: formatex(_val, 1, "%s", alphabet[random_num(19,25)]);	// EPIC LIGHT
	}

	//set_lights(_val);
	formatex(GLights, 1, "%s", _val);
	server_print("Randomed lights: %s", _val);
}

public renderFog(power)
{
	new fog = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "env_fog"));
	if(fog)
	{
		switch(power)
		{
			case 1: // low
			{
				if(!haveFogColor){
					new r = random_num(0,255);	//zm_italy:64 102 121
					new g = random_num(0,255);
					new b = random_num(0,255);
					formatex(GFogColor, 15, "%d %d %d", r,g,b);
				}
				
				if(!haveFogDensity){
					new Float:rndens = random_float(0.0007, 0.00001);
					formatex(GFogDensity, 15, "%.6f", rndens);
				}
				
				fm_set_kvd(fog, "rendercolor", GFogColor, "env_fog");
				fm_set_kvd(fog, "density", GFogDensity, "env_fog");
				
				server_print("Fog: RGB = %s, density = %s", GFogColor, GFogDensity);
			}
// <!-- 
// UNUSED
			case 2: // normal
			{
				fm_set_kvd(fog, "density", "0.0010", "env_fog");
				fm_set_kvd(fog, "rendercolor", "128 128 128", "env_fog");
			}
			case 3: // high
			{
				fm_set_kvd(fog, "density", "0.0015", "env_fog");
				fm_set_kvd(fog, "rendercolor", "128 128 128", "env_fog");
			}
// -->
		}
	}
}


public renderRain()
{
	server_print("Weather: Rainy");
	
	SPR_LASERBEAM = precache_model("sprites/laserbeam.spr");
	precache_model("models/chick.mdl");
	
	//precache_sound("ambience/rain.wav");
	//precache_sound("ambience/thunder_clap.wav");
	
	for(new i; i < sizeof S_THUNDER; i++){
		precache_sound(S_THUNDER[i]);
	}

	precache_sound(S_RAIN);
	
	EntWeather = create_entity("env_rain");
	register_think("env_rain", "WeatherSystem");
	mSetNextThink(EntWeather, 1.0);
}

public renderSnow()
{
	server_print("Weather: Snowy");
	EntWeather = create_entity("env_snow");
}

public skyIsRandom()
{
	server_print("skyIsRandom()");

	switch(random_num(2,11)) // [2]night ... [11]L2
	{
		// night (����)
		case 2: {
			server_print("Skybox: [2]night-> ...");
			new _night = random_num(1,40);
			server_print("Skybox: ... [%d]", _night);
			switch(_night)
			{
				case 1:		formatex(GSkybox, 127, "bioshock_rapture_");
				case 2:		formatex(GSkybox, 127, "g_night");
				case 3:		formatex(GSkybox, 127, "mnight");
				case 4:		formatex(GSkybox, 127, "grimmnight");
				case 5:		formatex(GSkybox, 127, "52h03");
				case 6:		formatex(GSkybox, 127, "dark");
				case 7:		formatex(GSkybox, 127, "dwell");
				case 8:		formatex(GSkybox, 127, "z3");
				case 9:		formatex(GSkybox, 127, "excity");
				case 10:	formatex(GSkybox, 127, "lol_night");
				case 11:	formatex(GSkybox, 127, "dx_village");
				case 12:	formatex(GSkybox, 127, "farmyard");
				case 13:	formatex(GSkybox, 127, "nightocean");
				case 14:	formatex(GSkybox, 127, "nightsea");
				case 15:	formatex(GSkybox, 127, "ravi");
				case 16:	formatex(GSkybox, 127, "lunarsky_01");
				case 17:	formatex(GSkybox, 127, "sky51");
				case 18:	formatex(GSkybox, 127, "sky_oar_night_01_");
				case 19:	formatex(GSkybox, 127, "fullmoon");
				case 20:	formatex(GSkybox, 127, "devilish_");
				case 21:	formatex(GSkybox, 127, "arm1_");
				case 22:	formatex(GSkybox, 127, "ngblue_");
				case 23:	formatex(GSkybox, 127, "nightsky"); // kf-like moon (2)
				case 24:	formatex(GSkybox, 127, "sealed");
				case 25:	formatex(GSkybox, 127, "streetsky");
				case 26:	formatex(GSkybox, 127, "uncastle");
				case 27:	formatex(GSkybox, 127, "desert_night_"); // kf-like moon
				case 28:	formatex(GSkybox, 127, "waterworld15");
				case 29:	formatex(GSkybox, 127, "grimmnight2");
				case 30:	formatex(GSkybox, 127, "hell");
				case 31:	formatex(GSkybox, 127, "siv2");
				case 32:	formatex(GSkybox, 127, "cv_");
				case 33:	formatex(GSkybox, 127, "dawn");
				case 34:	formatex(GSkybox, 127, "lmmnts_1");
				case 35:	formatex(GSkybox, 127, "nuclear_winter2");
				case 36:	formatex(GSkybox, 127, "onlymoon_");
				case 37:	formatex(GSkybox, 127, "spdog_");
				case 38:	formatex(GSkybox, 127, "xld_");
				case 39:	formatex(GSkybox, 127, "zombiehell");
				case 40:	formatex(GSkybox, 127, "dx_deepwater");
			}
		}
		// evening (�����)
		case 3: {
			server_print("Skybox: [3]evening-> ...");
			new _even = random_num(1,20);
			server_print("Skybox: ... [%d]", _even);
			switch(_even)
			{
				case 1,2:	formatex(GSkybox, 127, "inferno");
				case 3:		formatex(GSkybox, 127, "aristocracy");
				case 4:		formatex(GSkybox, 127, "whit");
				case 5:		formatex(GSkybox, 127, "1");
				case 6:		formatex(GSkybox, 127, "sky12");
				case 7:		formatex(GSkybox, 127, "arcn"); //arctic night
				case 8:		formatex(GSkybox, 127, "mpa119");
				case 9:		formatex(GSkybox, 127, "mpa37");
				case 10:	formatex(GSkybox, 127, "sky38");
				case 11:	formatex(GSkybox, 127, "sunsetmountain");
				case 12:	formatex(GSkybox, 127, "nightball_");
				case 13:	formatex(GSkybox, 127, "sky_l4d_c2m1_hdr");
				case 14:	formatex(GSkybox, 127, "sky_l4d_night02_hdr");
				case 15:	formatex(GSkybox, 127, "ntdm2_");
				case 16:	formatex(GSkybox, 127, "sky_quarry03");
				case 17:	formatex(GSkybox, 127, "xen");
				case 18:	formatex(GSkybox, 127, "badomen_");
				case 19:	formatex(GSkybox, 127, "arcd"); //arctic day
				case 20:	formatex(GSkybox, 127, "snowdrake_"); //arctic (edit)
			}
		}
		// overcast (�������� ����������)
		case 4: {
			server_print("Skybox: [4]overcast-> ...");
			new _gameover = random_num(1,29);
			server_print("Skybox: ... [%d]", _gameover);
			switch(_gameover)
			{
				case 1:		formatex(GSkybox, 127, "badweather");
				//formatex(GSkybox, 127, "CCCP"); // == badweather, but warmified
				case 2:		formatex(GSkybox, 127, "de_sbd_");
				case 3:		formatex(GSkybox, 127, "fof04");
				case 4:		formatex(GSkybox, 127, "mistic");
				case 5:		formatex(GSkybox, 127, "cs_frigid_");
				case 6:		formatex(GSkybox, 127, "snowy_");
				case 7:		formatex(GSkybox, 127, "l1escape1_");
				case 8:		formatex(GSkybox, 127, "italy");
				case 9:		formatex(GSkybox, 127, "spree");
				case 10:	formatex(GSkybox, 127, "jagd");
				case 11:	formatex(GSkybox, 127, "sky_airexchange01");
				case 12:	formatex(GSkybox, 127, "sky_borealis01");
				case 13:	formatex(GSkybox, 127, "sky_c17_01");
				case 14:	formatex(GSkybox, 127, "sky_c17_02");
				case 15:	formatex(GSkybox, 127, "sky_c17_03");
				case 16:	formatex(GSkybox, 127, "sky_depot01");
				case 17:	formatex(GSkybox, 127, "snowy");
				case 18:	formatex(GSkybox, 127, "l2sky06_");
				case 19:	formatex(GSkybox, 127, "l2sky15_");
				case 20:	formatex(GSkybox, 127, "l2sky18_");
				case 21:	formatex(GSkybox, 127, "l2sky45_");
				case 22:	formatex(GSkybox, 127, "hs_");
				case 23:	formatex(GSkybox, 127, "kvartal");
				case 24:	formatex(GSkybox, 127, "morass");
				case 25:	formatex(GSkybox, 127, "mountain2");
				case 26:	formatex(GSkybox, 127, "sarajevo_");
				case 27:	formatex(GSkybox, 127, "tumanno_");
				case 28:	formatex(GSkybox, 127, "winterday02");
				case 29:	formatex(GSkybox, 127, "winterday03");
			}
		}
		// sunset (�����)
		case 5: {
			server_print("Skybox: [5]sunset-> ...");
			new _sunstrike = random_num(1,25);
			server_print("Skybox: ... [%d]", _sunstrike);
			switch(_sunstrike)
			{
				case 1:		formatex(GSkybox, 127, "deadlock");
				case 2:		formatex(GSkybox, 127, "anubis");
				case 3:		formatex(GSkybox, 127, "arcss");
				case 4:		formatex(GSkybox, 127, "l2sky01_");
				case 5:		formatex(GSkybox, 127, "dx_dusted");
				case 6:		formatex(GSkybox, 127, "mpa95");
				case 7:		formatex(GSkybox, 127, "52h02");
				case 8:		formatex(GSkybox, 127, "bloody-heresy");
				case 9:		formatex(GSkybox, 127, "ze");
				case 10:	formatex(GSkybox, 127, "dm_byakugan");
				case 11:	formatex(GSkybox, 127, "dusk");
				case 12:	formatex(GSkybox, 127, "industrywest_");
				case 13:	formatex(GSkybox, 127, "mars1");
				case 14:	formatex(GSkybox, 127, "oblivionisle");
				case 15:	formatex(GSkybox, 127, "dam_");
				case 16:	formatex(GSkybox, 127, "depression-pass_");
				case 17:	formatex(GSkybox, 127, "l2sky61_");
				case 18:	formatex(GSkybox, 127, "fy_camp_");
				case 19:	formatex(GSkybox, 127, "desert01");
				case 20:	formatex(GSkybox, 127, "mpa82_cs");
				case 21:	formatex(GSkybox, 127, "mpa109");
				case 22:	formatex(GSkybox, 127, "sky17");
				case 23:	formatex(GSkybox, 127, "sky32");
				case 24:	formatex(GSkybox, 127, "twildes");
				case 25:	formatex(GSkybox, 127, "subway");
			}
		}
		// fair (����-�������-��������)
		case 6: {
			server_print("Skybox: [6]fair-> ...");
			new _hellfair = random_num(1,21);
			server_print("Skybox: ... [%d]", _hellfair);
			switch(_hellfair)
			{
				case 1:		formatex(GSkybox, 127, "grnplsnt");
				case 2:		formatex(GSkybox, 127, "sky3");
				case 3:		formatex(GSkybox, 127, "zp_trains2");
				case 4:		formatex(GSkybox, 127, "zps_inboxed");
				case 5:		formatex(GSkybox, 127, "day256");
				case 6:		formatex(GSkybox, 127, "minecraft_skybox");
				case 7:		formatex(GSkybox, 127, "sky_world");
				case 8:		formatex(GSkybox, 127, "grass");
				case 9:		formatex(GSkybox, 127, "cieloxd");
				case 10:	formatex(GSkybox, 127, "sky25");
				case 11:	formatex(GSkybox, 127, "landscape_");
				case 12:	formatex(GSkybox, 127, "hnoon");
				case 13:	formatex(GSkybox, 127, "52h06");
				case 14:	formatex(GSkybox, 127, "Jurassic_Park2_");
				case 15:	formatex(GSkybox, 127, "desert_evening_");
				case 16:	formatex(GSkybox, 127, "desert");
				case 17:	formatex(GSkybox, 127, "desnoon");
				case 18:	formatex(GSkybox, 127, "mine_"); // kf-like DeathBasin (2)
				case 19:	formatex(GSkybox, 127, "mountain");
				case 20:	formatex(GSkybox, 127, "pgs");
				case 21:	formatex(GSkybox, 127, "desdusk");
			}
		}
		// cloudy (�������)
		case 7: {
			server_print("Skybox: [7]cloudy-> ...");
			new _claud9 = random_num(1,28);
			server_print("Skybox: ... [%d]", _claud9);
			switch(_claud9)
			{
				case 1:		formatex(GSkybox, 127, "ferreiro_");
				case 2:		formatex(GSkybox, 127, "a_s2d");
				case 3:		formatex(GSkybox, 127, "blood");
				case 4:		formatex(GSkybox, 127, "de_shrine");
				case 5:		formatex(GSkybox, 127, "doom");
				case 6:		formatex(GSkybox, 127, "balsun");
				case 7:		formatex(GSkybox, 127, "misty");
				case 8:		formatex(GSkybox, 127, "stormyday");
				case 9:		formatex(GSkybox, 127, "flowsky09_");
				case 10:	formatex(GSkybox, 127, "sky45");
				case 11:	formatex(GSkybox, 127, "blue_winter_");
				case 12:	formatex(GSkybox, 127, "sky_palace01");
				case 13:	formatex(GSkybox, 127, "sky_quarry01hdr");
				case 14:	formatex(GSkybox, 127, "sky_spire01");
				case 15:	formatex(GSkybox, 127, "sky_station01");
				case 16:	formatex(GSkybox, 127, "urbannightburning_ldr");
				case 17:	formatex(GSkybox, 127, "urbannightstorm_ldr");
				case 18:	formatex(GSkybox, 127, "fatal");
				case 19:	formatex(GSkybox, 127, "cliff");
				case 20:	formatex(GSkybox, 127, "despair06_");
				case 21:	formatex(GSkybox, 127, "despair07_");
				case 22:	formatex(GSkybox, 127, "fellaville_");
				case 23:	formatex(GSkybox, 127, "mist");
				case 24:	formatex(GSkybox, 127, "phobos");
				case 25:	formatex(GSkybox, 127, "pirates");
				case 26:	formatex(GSkybox, 127, "sky_19_cube_");
				case 27:	formatex(GSkybox, 127, "skyted");
				case 28:	formatex(GSkybox, 127, "water_");
			}
		}
		// quake 3 skies
		case 8: {
			server_print("Skybox: [8]q3-> ...");
			new _quake3 = random_num(1,7);
			server_print("Skybox: ... [%d]", _quake3);
			switch(_quake3)
			{
				case 1:		formatex(GSkybox, 127, "sky01_");
				case 2:		formatex(GSkybox, 127, "sky04_");
				case 3:		formatex(GSkybox, 127, "sky05_");
				case 4:		formatex(GSkybox, 127, "sky06_");
				case 5:		formatex(GSkybox, 127, "sky07_");
				case 6:		formatex(GSkybox, 127, "sky12_");
				case 7:		formatex(GSkybox, 127, "sky21_");
			}
		}
		// gradients
		case 9: {
			server_print("Skybox: [9]gradients-> ...");
			new _grad = random_num(1,12);
			server_print("Skybox: ... [%d]", _grad);
			switch(_grad)
			{
				case 1:		formatex(GSkybox, 127, "grad2_01_");
				case 2:		formatex(GSkybox, 127, "grad2_02_");
				case 3:		formatex(GSkybox, 127, "grad6_01_");
				case 4:		formatex(GSkybox, 127, "grad6_02_");
				case 5:		formatex(GSkybox, 127, "grad6_03_");
				case 6:		formatex(GSkybox, 127, "grad6_04_");
				case 7:		formatex(GSkybox, 127, "grad6_05_");
				case 8:		formatex(GSkybox, 127, "grad6_06_");
				case 9:		formatex(GSkybox, 127, "grad6_07_");
				case 10:	formatex(GSkybox, 127, "grad13_01_");
				case 11:	formatex(GSkybox, 127, "grad13_02_");
				case 12:	formatex(GSkybox, 127, "grad13_03_");
			}
		}
		// killing floor
		case 10: {
			server_print("Skybox: [10]kf-> ...");
			new _kf = random_num(1,4);
			server_print("Skybox: ... [%d]", _kf);
			switch(_kf)
			{
				case 1:		formatex(GSkybox, 127, "ForestFiller");
				case 2:		formatex(GSkybox, 127, "Horzine_Factory_Facade_Diff");
				case 3:		formatex(GSkybox, 127, "night02");
				case 4:		formatex(GSkybox, 127, "m10_");
			}
		}
		// Lineage 2
		case 11: {
			server_print("Skybox: [11]L2-> ...");
			new _lin2 = random_num(1,7);
			server_print("Skybox: ... [%d]", _lin2);
			switch(_lin2){
				case 1:		formatex(GSkybox, 127, "l2sky107_");
				case 2:		formatex(GSkybox, 127, "l2sky138_");
				case 3:		formatex(GSkybox, 127, "l2sky177_");
				case 4:		formatex(GSkybox, 127, "l2sky227_");
				case 5:		formatex(GSkybox, 127, "l2sky568_");
				case 6:		formatex(GSkybox, 127, "l2sky673_");
				case 7:		formatex(GSkybox, 127, "l2sky917_");
			}
		}
	}
	// switch END
	//-->
	
	haveSky = true;
	server_print("Randomed Skybox: %s", GSkybox);
}

/*
public client_putinserver(id)
{
	client_cmd(id, "sv_skycolor_r 0");
	client_cmd(id, "sv_skycolor_g 0");
	client_cmd(id, "sv_skycolor_b 0");
//	client_cmd(id, "cl_weather 1");
}

public setSkyColor()
{
	server_cmd("sv_skycolor_r 0")
	server_cmd("sv_skycolor_g 0")
	server_cmd("sv_skycolor_b 0")
}
*/

public WeatherSystem(entid)
{
	if(entid == EntWeather) 
	{
		Weather_StormIntensity = 50;
		
		new victim = GetSomeoneUnworthy(); 
		
		if(Weather_StormIntensity) 
		{
			if(Weather_StrikeDelay < get_gametime()) 
			{
				if(victim){
					CreateLightningPoints(victim);
				}
			}
		}
		mSetNextThink(EntWeather,2.0)
	}

	return PLUGIN_CONTINUE;
}

GetSomeoneUnworthy()
{
	new cnt, id, total[33];
	for(id=1; id<GMaxPlayers; id++)
		if(is_user_alive(id))
			if(is_user_outside(id)) 
			{
				total[cnt++] = id;	
				
				if(!Weather_SoundState[id])
				{
					Weather_SoundState[id] = 1;
					//client_cmd(id, "speak ambience/rain.wav");
					client_cmd(id, "speak %s", S_RAIN);
				}	
			}
			else if(Weather_SoundState[id]) 
			{
				Weather_SoundState[id] = 0;
				client_cmd(id, "speak NULL")
			}
	
	if(cnt)
		return total[random_num(0, (cnt-1))];
	return 0;
}

CreateLightningPoints(victim) 
{
	if(is_valid_ent(Weather_LightPoints[0]))
		return 0;
	
	new ent, x, Float:tVel[3];
	new Float:vOrig[3];
	new Float:mins[3] = { -1.0, -1.0, -1.0 };
	new Float:maxs[3] = { 1.0, 1.0, 1.0 };
	new Float:dist = is_user_outside(victim)-5;
	
	mGetOrigin(victim, vOrig);
	
	if(dist > 700.0)
		dist = 700.0;
	
	vOrig[2] += dist;

	for(x=0; x<MAX_LIGHT_POINTS; x++) 
	{
		ent = create_entity("env_sprite");
		mSetInt(ent,movetype,MOVETYPE_FLY);
		mSetInt(ent,solid,SOLID_TRIGGER);
		mSetFloat(ent,renderamt,0.0);
		mSetInt(ent,rendermode,kRenderTransAlpha);
		entity_set_model(ent, "models/chick.mdl");
		
		mSetVector(ent,mins,mins)
		mSetVector(ent,maxs,maxs)
		tVel[0] = random_float(-500.0,500.0);
		tVel[1] = random_float(-500.0,500.0);
		tVel[2] = random_float((dist<=700.0?0.0:-100.0),(dist<=700.0?0.0:50.0));
		
		mSetVector(ent,origin,vOrig)
		mSetVector(ent,velocity,tVel)
		Weather_LightPoints[x] = ent;
	}
	
	//emit_sound(ent, CHAN_STREAM, "ambience/thunder_clap.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
	emit_sound(ent, CHAN_STREAM, S_THUNDER[random(sizeof S_THUNDER)], random_float(0.6,1.0), ATTN_NORM, 0, PITCH_NORM);
	set_task(random_float(0.6,2.0), "Lightning", victim);
	
	return 1;
}

public Lightning(victim) 
{
	new x, a, b, rand;
	new endpoint = MAX_LIGHT_POINTS-1;
	while(x < endpoint)
	{
		a = Weather_LightPoints[x];
		b = Weather_LightPoints[x+1];
		x++
		if(x == endpoint)
		{
			rand = random_num(1,1000);
			if(rand == 1)
			{
				b = victim;
				FAKE_DAMAGE(victim,"Lightning",100.0,1);
			}
		}
		CreateBeam(a,b);
	}
	
	for(x=0; x<MAX_LIGHT_POINTS; x++)
		if(is_valid_ent(Weather_LightPoints[x]))
			remove_entity(Weather_LightPoints[x])
	
	if(Weather_StormIntensity > 100)
	{
		set_cvar_num("weather_storm", 100);
		Weather_StormIntensity = 100;	
	}
	
	new Float:mins = 50.0 - float(Weather_StormIntensity / 2);
	new Float:maxs = 50.0 - float(Weather_StormIntensity / 3);
	Weather_StrikeDelay = get_gametime() + random_float(mins, maxs);
}

Float:is_user_outside(id)
{
	new Float:origin[3], Float:dist;
	mGetOrigin(id, origin)
	
	dist = origin[2];
	
	while(point_contents(origin) == -1)
		origin[2] += 5.0;
	
	if(point_contents(origin) == -6)
		return (origin[2]-dist);
	
	return 0.0;
}

CreateBeam(entA, entB)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(8);
	write_short(entA);
	write_short(entB);
	write_short(SPR_LASERBEAM);
	write_byte(0);
	write_byte(10);
	write_byte(5); 
	write_byte(8);
	write_byte(100);
	write_byte(255);
	write_byte(255);
	write_byte(255);
	write_byte(255)
	write_byte(10);
	message_end();
}

stock fm_set_kvd(entity, const key[], const value[], const classname[] = "")
{
	set_kvd(0, KV_ClassName, classname), set_kvd(0, KV_KeyName, key)
	set_kvd(0, KV_Value, value), set_kvd(0, KV_fHandled, 0)

	return dllfunc(DLLFunc_KeyValue, entity, 0)
}

stock FAKE_DAMAGE(idvictim,szClassname[],Float:takedmgdamage,damagetype)
{
	new entity = create_entity("trigger_hurt")
	if(entity)
	{
		new szDamage[16]
		format(szDamage, 15, "%f", takedmgdamage * 2)
		DispatchKeyValue(entity, "dmg", szDamage)
		format(szDamage, 15, "%i", damagetype)
		DispatchKeyValue(entity,"damagetype",szDamage)
		DispatchSpawn(entity)
		mSetString(entity,classname,szClassname)
		fake_touch(entity,idvictim)
		remove_entity(entity)
		return 1
	}
	return 0
}

public ambienceScheme(Float:origin[3], size, pieces, sound[], volume)
{
	//new pieces = 6; // count of entities
	new Float:angle_diff = 360.0 / float(pieces);
	new Float:angle[3], Float:fin_point[3], Float:v2[3];
	
	new total = 0;

	while(angle[1] < 360.0)
	{
		angle_vector(angle, ANGLEVECTOR_FORWARD, fin_point);
		angle[1] += angle_diff;
		angle_vector(angle, ANGLEVECTOR_FORWARD, v2);
		xs_vec_mul_scalar(fin_point, float(size), fin_point);
		xs_vec_mul_scalar(v2, float(size), v2);
		xs_vec_add(fin_point, origin, fin_point);
		xs_vec_add(v2, origin, v2);
		
		new ent = create_entity("ambient_generic");
		if(ent){
			entity_set_string(ent, EV_SZ_classname, "ambient_generic");
			
			/*
			#define AMBIENT_SOUND_STATIC		0    // medium radius attenuation
			#define AMBIENT_SOUND_EVERYWHERE	1
			#define AMBIENT_SOUND_SMALLRADIUS	2
			#define AMBIENT_SOUND_MEDIUMRADIUS	4
			#define AMBIENT_SOUND_LARGERADIUS	8
			#define AMBIENT_SOUND_START_SILENT	16
			#define AMBIENT_SOUND_NOT_LOOPING	32
			*/
			entity_set_int(ent, EV_INT_spawnflags, 1);
			
			entity_set_float(ent, EV_FL_health, volume > 10 ? 10.0 : float(volume));
			//entity_set_string(ent, EV_SZ_targetname, targetname)
			//entity_set_string(ent, EV_SZ_message, file)

			//DispatchKeyValue(ent, "spawnflags", "1");
			DispatchKeyValue(ent, "message", sound);
			//DispatchKeyValue(ent, "volstart", volume);
			DispatchKeyValue(ent,"pitchstart", "100");
			DispatchKeyValue(ent, "pitch", "100");
			
			DispatchSpawn(ent);
			entity_set_origin(ent, fin_point);
			
			server_print("Created ambient_generic #%d at [%f,%f,%f]", total, fin_point[0],fin_point[1],fin_point[2]);
		}
		
		total++;
		if(total >= pieces)
			break;
	}
}
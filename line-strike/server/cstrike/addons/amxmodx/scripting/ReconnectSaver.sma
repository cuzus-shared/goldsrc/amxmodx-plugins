#include <amxmodx>
#include <cstrike>
#include <fun>

new RCAN[] = "amx_reconnect_can"
new RTIME[] = "amx_reconnect_time"
new SCORESAVE[] = "amx_reconnect_ss"
new RSTATIC[] = "amx_reconnect_static"
new RSTIME[] = "amx_reconnect_stime"

new t_disconnect[33] = {0, ...}
new t_scoresave[33] = {0, ...}
new ips[33][24]
new sfrags[33] = {0, ...}
new sdeaths[33] = {0, ...}
new useretry[33] = {0, ...}

public plugin_init()
{
	register_plugin("Anti reconnect", "JohnJ", "3.0")
	
	register_cvar(RCAN, "1") // ��������� �������� (�� ������)
	register_cvar(RTIME, "10") // ��� ������� ���������� �� ����� ��������� �������� N ������
	register_cvar(SCORESAVE, "1") // ��������� ���� ��� ���������� (���� �� �������� amx_reconnect_can 1) 
	register_cvar(RSTATIC, "1") // ������ ������� � ������� ����� ������ � ������� (���� ����� amx_reconnect_static 0 - � ������� ����� ������ �� ������)
	register_cvar(RSTIME, "60") // ��������� ���� ��� ������ � ������� N ������ ����� ��� ����� �� ip
	register_event("TeamInfo", "outspec", "a") // ��������������� ���� ������ � ������������ � ����������� ��� ������ ��� ����� �� �������
}

public client_connect(id)
{
	new maxexit = get_cvar_num(RTIME)
	new maxstata = get_cvar_num(RSTIME)
	new canreconnect = get_cvar_num(RCAN)
	new ssave = get_cvar_num(SCORESAVE)
	new ip[24]
	get_user_ip(id, ip, 23, 0)

	if((maxexit > 0) && (canreconnect == 0) && (equali(ip, ips[id])))
	{
		new Float:nexTime = get_gametime()

		if(t_disconnect[id] > nexTime)
		{
			new stat = get_cvar_num(RSTATIC)
			new timewait
			if(stat == 0)
			{
				t_disconnect[id] = floatround(nexTime) + maxexit 
				t_scoresave[id] = floatround(nexTime) + maxstata 
				timewait = maxexit
			}
			else
			{
				timewait = t_disconnect[id] - floatround(nexTime)
			}

			server_cmd("kick #%d ^"Anti Reconnect: Please wait % d seconds.^"", get_user_userid(id), timewait)
			
			return PLUGIN_CONTINUE
		}
	}
	if(ssave == 1)
	{
		new Float:nexTime = get_gametime()

		if(t_scoresave[id] <= nexTime)
		{
			sdeaths[id] = 0
			sfrags[id] = 0
			useretry[id] = 0
		}
		return PLUGIN_CONTINUE
	}

	return PLUGIN_CONTINUE
}

public outspec()
{
	new id = read_data(1)
	if((useretry[id] == 1) && (is_user_connected(id)))
	{
		cs_set_user_deaths(id, sdeaths[id])
		set_user_frags(id, sfrags[id])
		useretry[id] = 0 
		sdeaths[id] = 0 
		sfrags[id] = 0
	}
	return PLUGIN_CONTINUE
}

public client_disconnect(id)
{
	new maxexit = get_cvar_num(RTIME)
	new maxstata = get_cvar_num(RSTIME)
	new ssave = get_cvar_num(SCORESAVE)

	new Float:theTime = get_gametime()
	t_disconnect[id] = floatround(theTime) + maxexit
	t_scoresave[id] = floatround(theTime) + maxstata
	
	get_user_ip(id, ips[id], 23, 0)

	if(ssave == 1)
	{
		sdeaths[id] = get_user_deaths(id)
		sfrags[id] = get_user_frags(id)
		useretry[id] = 1
	}
	return PLUGIN_CONTINUE
}
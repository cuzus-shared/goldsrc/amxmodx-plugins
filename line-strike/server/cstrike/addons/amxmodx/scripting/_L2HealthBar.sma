#include <amxmodx>
#include <cstrike>
#include <fakemeta>
#include <hamsandwich>

#define TEAM_MODE 		1		// 0 - Отображается на всех игроках, 1 - только на союзниках, 2 - только на противниках
#define ALIVE_MODE 		0		// 0 - Отображается в любом состоянии игрока, 1 - только в живом, 2 - только в мертвом
#define MAX_HEALTH 		255.0

#define HEALTHBAR_PLAYER	"cache/textures/ps_hpbar.utx"
//#define HEALTHBAR_ADMIN		"cache/textures/ps_expbar.utx"

new GMaxPlayers;
new HealthBar[33];
new IsAlive[33];
new CsTeams:PlayerTeam[33];

public plugin_precache()
{
	precache_model(HEALTHBAR_PLAYER);
//	precache_model(HEALTHBAR_ADMIN);
}

public plugin_init()
{
	register_plugin("Lifebar", "1.0", "Psycrow");

	register_forward(FM_AddToFullPack, "fwAddToFullPack", 1);
	RegisterHam(Ham_Spawn, "player", "fwHamSpawn", 1);
	register_event("DeathMsg", "evDeathMsg", "a");
	register_event("Health", "evHealth", "be");

	GMaxPlayers = get_maxplayers();

	for(new i = 1; i <= GMaxPlayers; i++)
	{
		HealthBar[i] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "env_sprite"));

		if(!pev_valid(HealthBar[i]))
			continue;

		engfunc(EngFunc_SetModel, HealthBar[i], HEALTHBAR_PLAYER);
		set_pev(HealthBar[i], pev_movetype, MOVETYPE_FOLLOW);
		set_pev(HealthBar[i], pev_aiment, i);
		set_pev(HealthBar[i], pev_scale, 0.1);

//		set_pev(HealthBar[i], pev_renderfx, kRenderFxGlowShell);
		set_pev(HealthBar[i], pev_rendercolor, Float:{0.0, 0.0, 0.0});
		set_pev(HealthBar[i], pev_rendermode, kRenderTransAdd);//kRenderTransAlpha);
		set_pev(HealthBar[i], pev_renderamt, 0.0);
	}
}

public client_putinserver(id)
{
	IsAlive[id] = 0;
	PlayerTeam[id] = CS_TEAM_SPECTATOR;

	/*if(get_user_flags(id) & ADMIN_IMMUNITY){
		engfunc(EngFunc_SetModel, HealthBar[id], HEALTHBAR_ADMIN);
	}*/
}

public client_disconnect(id)
{
	IsAlive[id] = 0;
	PlayerTeam[id] = CS_TEAM_UNASSIGNED;

	/*if(get_user_flags(id) & ADMIN_IMMUNITY){
		engfunc(EngFunc_SetModel, HealthBar[id], HEALTHBAR_PLAYER);
	}*/
}

public fwAddToFullPack(es, e, ent, host)
{
	static i;
	for(i = 1; i <= GMaxPlayers; i++)
	{
		if(HealthBar[i] == ent)
		{
			if(i == host || !IsAlive[i] 
			|| (TEAM_MODE == 1 && PlayerTeam[i] != PlayerTeam[host]) 
			|| (TEAM_MODE == 2 && PlayerTeam[i] == PlayerTeam[host])
			|| (ALIVE_MODE == 1 && !IsAlive[host]) || (ALIVE_MODE == 2 && IsAlive[host]))
				return;

			static Float: fOrigin[3];
			pev(i, pev_origin, fOrigin);						
			fOrigin[2] += 35.0;//30.0;
			set_es(es, ES_AimEnt, 0);
			set_es(es, ES_Origin, fOrigin);
			set_es(es, ES_RenderAmt, 255.0);
		}
	}
}

public fwHamSpawn(id)
{
	if(is_user_alive(id))
	{
		IsAlive[id] = 1;
		PlayerTeam[id] = cs_get_user_team(id);
		set_pev(HealthBar[id], pev_frame, MAX_HEALTH); // MAX HP as frame 255
	}
}

public evDeathMsg()
{
	static id;
	id = read_data(2);
	IsAlive[id] = 0;
	PlayerTeam[id] = cs_get_user_team(id);
}

public evHealth(id)
{
	static hp;
	hp = get_user_health(id);
	set_pev(HealthBar[id], pev_frame, float(hp) - (hp >= 1 ? 1.0 : 0.0)); // bugfix: frame offset -1
	
//	if(hp > MAX_HEALTH)
//		hp = MAX_HEALTH;
//	set_pev(HealthBar[id], pev_frame, hp * 128 / MAX_HEALTH - 1.0);

}
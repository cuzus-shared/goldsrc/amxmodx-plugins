#include <amxmodx>
#include <fakemeta>
#include <hamsandwich>

const WEAPON_SUIT = 31;
const WEAPON_SUIT_BIT = 1<<WEAPON_SUIT;
 
new g_bConnectedPlayers[33 char];
 
public plugin_init()
{
	register_plugin("No Spawn Weapons", "0.0.1", "ConnorMcLeod");
	RegisterHam(Ham_Spawn, "player", "OnCBasePlayer_Spawn", false);
}
 
public client_connect(id)
{
	g_bConnectedPlayers{id} = false;
}
 
public client_putinserver(id)
{
	g_bConnectedPlayers{id} = true;
}
 
public OnCBasePlayer_Spawn(id)
{
	const m_iTeam = 114;
	const m_bHasReceivedDefItems = 480;

	if(1 <= get_pdata_int(id, m_iTeam) <= 2)
	{
		set_pdata_bool(id, m_bHasReceivedDefItems, true);
		new weapons = pev(id, pev_weapons);
		
		if(~weapons & WEAPON_SUIT_BIT){
			set_pev(id, pev_weapons, weapons | WEAPON_SUIT_BIT);
		}
		return HAM_HANDLED;
	}
	return HAM_IGNORED;
}
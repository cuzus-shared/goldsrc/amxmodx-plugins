// --------------------------------------------------
// Private Store
// --------------------------------------------------
new PrivateStoreDummy[33];
new PrivateStoreSign[33];

// +sell
public Cmd_slashSell(id)
{
	PrivateStore_dummy(id);
	PrivateStore_sign(id);
}

public Cmd_slashBuy(id)
{
	
}

new dummies = 0;

public PrivateStore_dummy(id)
{
	if(dummies > 10)
	{
		for(new i=0; i<=11; i++){
			engfunc(EngFunc_RemoveEntity, PrivateStoreDummy[i]);
		}
		dummies=0;
	}
	
	PrivateStoreDummy[dummies] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	
	set_pev(PrivateStoreDummy[dummies], pev_classname, "ps_player");
	new ent_model[64];
	switch(random_num(1,8))
	{
		case 1: { // CT urban
			ent_model = "models/player/urban/urban.mdl";
		}
		case 2: { // T terror
			ent_model = "models/player/terror/terror.mdl";
		}
		case 3: { // T leet
			ent_model = "models/player/leet/leet.mdl";
		}
		case 4: { // T arctic
			ent_model = "models/player/arctic/arctic.mdl";
		}
		case 5: { // CT gsg9
			ent_model = "models/player/gsg9/gsg9.mdl";
		}
		case 6: { // CT gign
			ent_model = "models/player/gign/gign.mdl";
		}
		case 7: { // CT sas
			ent_model = "models/player/sas/sas.mdl";
		}
		case 8: { // T guerilla
			ent_model = "models/player/guerilla/guerilla.mdl";
		}
	}
	engfunc(EngFunc_SetModel, PrivateStoreDummy[dummies], ent_model);
	new Float:origin[3];
	pev(id, pev_origin, origin);
	origin[2] -= 33.0;
	set_pev(PrivateStoreDummy[dummies], pev_origin, origin);
	
	new Float:angles[3];
	pev(id, pev_angles, angles);
	angles[1] = 0.0;
	angles[2] = 0.0;
	set_pev(PrivateStoreDummy[dummies], pev_angles, angles);

	set_pev(PrivateStoreDummy[dummies], pev_solid, SOLID_NOT);

	new Float:maxs[3] = {16.0, 16.0, 36.0}; //76.0
	new Float:mins[3] = {-16.0, -16.0, -36.0};
	engfunc(EngFunc_SetSize, PrivateStoreDummy[dummies], mins, maxs);

	set_pev(PrivateStoreDummy[dummies], pev_sequence, 114); // sit1
	set_pev(PrivateStoreDummy[dummies], pev_animtime, 2.0);
	set_pev(PrivateStoreDummy[dummies], pev_framerate, 1.0);
	set_pev(PrivateStoreDummy[dummies], pev_nextthink, 1.0);
	
	engfunc(EngFunc_DropToFloor, PrivateStoreDummy[dummies]);
	
	dummies++;
	
	
/*	if(PrivateStoreDummy[id]){
		// if valid !
		engfunc(EngFunc_RemoveEntity, PrivateStoreDummy[id]);
		PrivateStoreDummy[id] = 0;
	}

	PrivateStoreDummy[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	
	if(PrivateStoreDummy[id])
	{
		set_pev(PrivateStoreDummy[id], pev_classname, "ps_player");
		// &copy from Dash
		new CsInternalModel:model;
		cs_get_user_team(id, model);
		new ent_model[64];
		switch(model)
		{
			case 1: { // CT urban
				ent_model = "models/player/urban/urban.mdl";
			}
			case 2: { // T terror
				ent_model = "models/player/terror/terror.mdl";
			}
			case 3: { // T leet
				ent_model = "models/player/leet/leet.mdl";
			}
			case 4: { // T arctic
				ent_model = "models/player/arctic/arctic.mdl";
			}
			case 5: { // CT gsg9
				ent_model = "models/player/gsg9/gsg9.mdl";
			}
			case 6: { // CT gign
				ent_model = "models/player/gign/gign.mdl";
			}
			case 7: { // CT sas
				ent_model = "models/player/sas/sas.mdl";
			}
			case 8: { // T guerilla
				ent_model = "models/player/guerilla/guerilla.mdl";
			}
		}
		engfunc(EngFunc_SetModel, PrivateStoreDummy[id], ent_model);
		
		new Float:origin[3];
		pev(id, pev_origin, origin);
		origin[2] -= 33.0;
		set_pev(PrivateStoreDummy[id], pev_origin, origin);
		
		new Float:angles[3];
		pev(id, pev_angles, angles);
		set_pev(PrivateStoreDummy[id], pev_angles, angles);

		set_pev(PrivateStoreDummy[id], pev_solid, SOLID_NOT);

		new Float:maxs[3] = {16.0, 16.0, 36.0}; //76.0
		new Float:mins[3] = {-16.0, -16.0, -36.0};
		engfunc(EngFunc_SetSize, PrivateStoreDummy[id], mins, maxs);

		set_pev(PrivateStoreDummy[id], pev_sequence, 114); // sit1
		set_pev(PrivateStoreDummy[id], pev_animtime, 2.0);
		set_pev(PrivateStoreDummy[id], pev_framerate, 1.0);
		set_pev(PrivateStoreDummy[id], pev_nextthink, 1.0);
		
		engfunc(EngFunc_DropToFloor, PrivateStoreDummy[id]);
	}
*/
}

new signs = 0;

public PrivateStore_sign(id)
{
	if(signs > 10)
	{
		for(new i=0; i<=11; i++){
			engfunc(EngFunc_RemoveEntity, PrivateStoreSign[i]);
		}
		signs = 0;
	}
	
	PrivateStoreSign[signs] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	set_pev(PrivateStoreSign[signs], pev_classname, "ps_sign");

// tmp removed from cache
/*	
	new sprid = random_num(1,2);
	if(sprid==1)
		engfunc(EngFunc_SetModel, PrivateStoreSign[signs], SPR_PS_SELL_128);
	else
		engfunc(EngFunc_SetModel, PrivateStoreSign[signs], SPR_PS_BUY_128);
*/
	engfunc(EngFunc_SetSize, PrivateStoreSign[signs], Float:{0.0,0.0,0.0}, Float:{1.0,1.0,150.0});
	new Float:origin[3];
	pev(id, pev_origin, origin);
	//new Float:z = random_float(40.0, 280.0);
	origin[2] += 20.0; //+= z;
	engfunc(EngFunc_SetOrigin, PrivateStoreSign[signs], origin);
	set_pev(PrivateStoreSign[signs], pev_solid, SOLID_NOT);
	set_pev(PrivateStoreSign[signs], pev_movetype, MOVETYPE_NONE);
	set_pev(PrivateStoreSign[signs], pev_scale, 0.6);
	set_pev(PrivateStoreSign[signs], pev_rendermode, 2);
	set_pev(PrivateStoreSign[signs], pev_renderamt, 225.0);
	set_pev(PrivateStoreSign[signs], pev_renderfx, 14);
	
	signs++;

/*
	if(PrivateStoreSign[id]){
		engfunc(EngFunc_RemoveEntity, PrivateStoreSign[id]);
		PrivateStoreSign[id] = 0;
	}
	
	if(!PrivateStoreDummy[id])
		return;
	
	//PrivateStoreSign[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "env_sprite"));
	PrivateStoreSign[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	
	if(PrivateStoreSign[id])
	{
		set_pev(PrivateStoreSign[id], pev_classname, "ps_sign");

		new sprid = random_num(1,2);
		if(sprid==1)
			engfunc(EngFunc_SetModel, PrivateStoreSign[id], SPR_PS_SELL_128);
		else
			engfunc(EngFunc_SetModel, PrivateStoreSign[id], SPR_PS_BUY_128);
		
		engfunc(EngFunc_SetSize, PrivateStoreSign[id], Float:{0.0,0.0,0.0}, Float:{1.0,1.0,150.0});
		
		new Float:origin[3];
		pev(id, pev_origin, origin);
		//new Float:z = random_float(40.0, 280.0);
		origin[2] += 20.0; //+= z;
		engfunc(EngFunc_SetOrigin, PrivateStoreSign[id], origin);
		set_pev(PrivateStoreSign[id], pev_solid, SOLID_NOT);
		set_pev(PrivateStoreSign[id], pev_movetype, MOVETYPE_NONE); //MOVETYPE_FOLLOW
		//set_pev(PrivateStoreSign[id], pev_aiment, PrivateStoreDummy[id]);
		
		// renderamt: 224.979782 | renderfx: 14 | rendermode: 2 | spr: 1 | scale: 0.564626
		
		set_pev(PrivateStoreSign[id], pev_scale, 0.6);
		set_pev(PrivateStoreSign[id], pev_rendermode, 2);
		set_pev(PrivateStoreSign[id], pev_renderamt, 225.0);
		set_pev(PrivateStoreSign[id], pev_renderfx, 14);

		// new Float:scale = random_float(0.1,0.9);
		// set_pev(PrivateStoreSign[id], pev_scale, scale);
		// new rend = random_num(0,5);
		// set_pev(PrivateStoreSign[id], pev_rendermode, rend); // 5 - Additive
		// new Float:a = random_float(10.0, 255.0);
		// set_pev(PrivateStoreSign[id], pev_renderamt, a);
		// new r = random_num(0,16);
		// set_pev(PrivateStoreSign[id], pev_renderfx, r);

		// client_print(id, print_chat, "// renderamt: %f | renderfx: %d | rendermode: %d | spr: %d | scale: %f", a, r, rend, sprid, scale);
		// client_print(id, print_console, "// renderamt: %f | renderfx: %d | rendermode: %d | spr: %d | scale: %f", a, r, rend, sprid, scale);
	}
*/
}
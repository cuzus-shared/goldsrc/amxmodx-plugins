// --------------------------------------------------
// Kill Assist
// 
// Thanks to Digi (a.k.a. Hunter-Digital) for Kill Assist plugin
// --------------------------------------------------
	
	// todo: Kill Assist
	//new clip, ammo, weapon_id = get_user_weapon(victim,clip,ammo); 
	//killassist_hamPlayerKilledPre(victim, killer, gib, weapon_id);

	
#define is_player(%1) (1 <= %1 <= GMaxPlayers)
#define KILL_ASSIST_MIN_DAMAGE	45
enum{
	TEAM_NONE = 0,
	TEAM_TE = 1,
	TEAM_CT = 2,
	TEAM_SPEC = 3
}
new GPlayerTeam[33];
new GPlayerName[33][32];	// ?33 ?32
new bool:GPlayerIsAlive[33] = {false, ...};
new bool:GPlayerConnected[33] = {false, ...};
new KillAssistDamage[33][33];

// http://amxxmodx.ru/core/message_stocksinc/67-make_deathmsg-funkciya-sozdaet-soobschenie-o-smerteubiystve-igroka.html

public killassist_clientPutInServer(id)
{
	GPlayerConnected[id] = true;
	get_user_name(id, GPlayerName[id], 31)
}

public killassist_clientDisconnect(id)
{
	GPlayerTeam[id] = TEAM_NONE;
	GPlayerIsAlive[id] = false;
	GPlayerConnected[id] = false;
}

public killassist_hamPlayerSpawn(id)
{
	GPlayerIsAlive[id] = true; // for Kill Assist

	static p, szName[32];
	get_user_name(id, szName, 31);

	if(!equali(szName, GPlayerName[id])) // make sure he has his name !
	{
		set_msg_block(gmsgSayText, BLOCK_ONCE);
		set_user_info(id, "name", GPlayerName[id]);
	}

	// reset damage meters
	for(p = 1; p <= g_max_players; p++)
		KillAssistDamage[id][p] = 0;
}

public killassist_hamTakeDamagePlayer(victim, Float:damage)
{
	if(!is_player(victim))
		return PLUGIN_CONTINUE;

	static attacker;
	attacker = get_user_attacker(victim);

	if(!is_player(attacker))
		return PLUGIN_CONTINUE;

	KillAssistDamage[attacker][victim] += floatround(damage,floatround_round); //read_data(2);

	return PLUGIN_CONTINUE;
}

public killassist_hamPlayerKilledPre(victim, killer, headshot, weapon_id)
{
	if(!is_player(victim)) return;
	//	return PLUGIN_CONTINUE;

	GPlayerIsAlive[victim] = false;

	if(!is_player(killer)) return;
	//	return PLUGIN_CONTINUE;

	//static szWeapon[24] = weapon_name;
	//static szWeapon[33] = weapon_name;
	new headshot, killer_team;
	
	static weapon_name[24];
	get_weaponname(weapon_id, weapon_name, 23)
	replace(weapon_name, 23, "weapon_", "")

	killer_team = GPlayerTeam[killer];
	//headshot = read_data(3);
	//read_data(4, szWeapon, 23);

	if(killer != victim && GPlayerTeam[victim] != killer_team)
	{
		static killer2, damage2, p;
		killer2 = 0;
		damage2 = 0;

		for(p = 1; p <= g_max_players; p++)
		{
			if(p != killer && GPlayerConnected[p] && GPlayerIsAlive[p] 
			&& killer_team == GPlayerTeam[p] && KillAssistDamage[p][victim] >= KILL_ASSIST_MIN_DAMAGE && KillAssistDamage[p][victim] > damage2)
			{
				killer2 = p;
				damage2 = KillAssistDamage[p][victim];
			}
			KillAssistDamage[p][victim] = 0;
		}

		if(killer2 > 0 && damage2 > KILL_ASSIST_MIN_DAMAGE)
		{
			// give frags
			static player_frags;
			player_frags = get_user_frags(killer2) + 1;
			set_user_frags(killer2, player_frags);

			Create_MSG_ScoreInfo(killer2, player_frags, get_user_deaths(killer2), 0, killer_team)

			// TODO: give money for assist (SYNC WITH MoneyKill plugin)
			/*static iMoney;
			iMoney = cs_get_user_money(killer2) + ch_pCVar_giveMoney

			if(iMoney > 16000)
				iMoney = 16000;
				
			cs_set_user_money(killer2, iMoney)

			if(GPlayerIsAlive[killer2]) // no reason to send a money message when the player has no hud :}
			{
				message_begin(MSG_ONE_UNRELIABLE, msgID_money, _, killer2)
				write_long(iMoney)
				write_byte(1)
				message_end()
			}*/

			static szName1[32], iName1Len, szName2[32], iName2Len, szNames[32]; //, szWeaponLong[32];

			iName1Len = get_user_name(killer, szName1, 31)
			iName2Len = get_user_name(killer2, szName2, 31)

			GPlayerName[killer] = szName1

			if(iName1Len < 14)
			{
				formatex(szName1, iName1Len, "%s", szName1)
				formatex(szName2, 28-iName1Len, "%s", szName2)
			}
			else if(iName2Len < 14)
			{
				formatex(szName1, 28-iName2Len, "%s", szName1)
				formatex(szName2, iName2Len, "%s", szName2)
			}else{
				formatex(szName1, 13, "%s", szName1)
				formatex(szName2, 13, "%s", szName2)
			}
			
			formatex(szNames, 31, "%s + %s", szName1, szName2)

			set_msg_block(gmsgSayText, BLOCK_ONCE)
			set_user_info(killer, "name", szNames)

			//if(equali(weapon_name, "grenade")) //szWeapon
			//	szWeaponLong = "weapon_hegrenade"
			//else
			//	formatex(szWeaponLong, 31, "weapon_%s", szWeapon)
			
			static args[4];
			args[0] = victim;
			args[1] = killer;
			args[2] = headshot;
			args[3] = weapon_id;

			set_task(0.1, "killassist_playerKilledPost", 0, args, 4)
		}
		//else if(ch_pCVar_enabled == 1)
		//Create_MSG_DeathMsg(killer, victim, headshot, weapon_name);
	}
	//else if(ch_pCVar_enabled == 1)
	//Create_MSG_DeathMsg(killer, victim, headshot, weapon_name);
	//return PLUGIN_CONTINUE;
}

public killassist_playerKilledPost(arg[])
{
	static killer, szWeapon[24];
	killer = arg[1];

	get_weaponname(arg[3], szWeapon, 23)
	replace(szWeapon, 23, "weapon_", "")

	Create_MSG_DeathMsg(killer, arg[0], arg[2], szWeapon)	// Stocks.inl

	set_msg_block(gmsgSayText, BLOCK_ONCE)
	set_user_info(killer, "name", GPlayerName[killer])

	return PLUGIN_CONTINUE;
}
// adrenaline
#define TASK_ADRENALINE_FADE		7795
#define TASK_ADRENALINE_END			7595
#define TASK_ADRENALINE_POV_ADD		7395
#define TASK_ADRENALINE_POV_REMOVE	7295

public cmd_dslashAdrenaline(id)
{
	if(!is_user_alive(id))
		return PLUGIN_HANDLED;
		
	//message_begin(MSG_ONE, gmsgBarTime, _, id);
	//write_short(10);	// duration = 10 sec
	//message_end();
	
	set_task(0.1, "adrenalineFade", id+TASK_ADRENALINE_FADE);
	set_task(0.2, "adrenalinePov", id+TASK_ADRENALINE_POV_ADD);
	//set_task(0.6, "adrenalinePOV", id+TASK_ADRENALINE_POV);
	set_task(10.0, "adrenalineEnd", id+TASK_ADRENALINE_END);
	
	/*AdrInc[id] = 0;
	while(AdrInc[id] < 170){
		set_task(0.3, "adrenalinePovInc", id+TASK_ADRENALINE_POV_INC);
		AdrInc[id]++;
	}*/
	
	return PLUGIN_HANDLED;
}

public adrenalineEnd(taskid)
{
	new id = taskid-TASK_ADRENALINE_END;
	
	//st_ScreenFade(id, 1<<10, 1<<10*10, FFADE_OUT, 0, 100, 0, 45, true);
	st_ScreenFade(id, 1<<10, 1<<10, FFADE_IN, 0, 100, 0, 45, true);
	
	adrenalinePovRemove(id);
	
	/*if(AdrInc[id] != 170)
		AdrInc[id] = 170;
	while(AdrInc[id] > 0){
		set_task(0.1, "adrenalinePovDec", id+TASK_ADRENALINE_POV_DEC);
		AdrInc[id]--;
	}*/
	
	if(task_exists(id+TASK_ADRENALINE_FADE))
		remove_task(id+TASK_ADRENALINE_FADE);
	if(task_exists(id+TASK_ADRENALINE_POV_ADD))
		remove_task(id+TASK_ADRENALINE_POV_ADD);
	//if(task_exists(id+TASK_ADRENALINE_POV_REMOVE))
	//	remove_task(id+TASK_ADRENALINE_POV_REMOVE);
}

public adrenalineFade(taskid)
{
	new id = taskid-TASK_ADRENALINE_FADE;
	st_ScreenFade(id, 1<<0, 1<<0, 1<<2, 0,1000,0,45, true);	// infinity fade
}

public adrenalinePov(taskid)
{
	new id = taskid-TASK_ADRENALINE_POV_ADD;
	
	if(!is_user_connected(id) || !is_user_alive(id))
		return PLUGIN_CONTINUE;
	
	message_begin(MSG_ONE, gmsgSetFOV, {0,0,0}, id);
	write_byte(150);
	message_end();
	
	return PLUGIN_HANDLED;
}

public adrenalinePovRemove(id)
{
	if(!is_user_connected(id) || !is_user_alive(id))
		return PLUGIN_CONTINUE;
	
	message_begin(MSG_ONE, gmsgSetFOV, {0,0,0}, id);
	write_byte(0);
	message_end();
	
	return PLUGIN_HANDLED;
}
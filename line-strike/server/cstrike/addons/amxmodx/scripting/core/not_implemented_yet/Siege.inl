// 
//
// ����� ������ ����� 20:00
// ������������: 2�
// � �������� ����� -- ����� ����� �� Siege + ��������� Siege mod'a (if mapname == ...) -> call siege

//todo : rework spawn points + todo waypos [pb_castle]

//like IO -- objectives on maps -- can siege (owner/defender) or VS NPC at start (goto IO and chk sieges)

/**		@IO Forts
	������ ����� ������ �������� ���:
	1) ������� ��������� ����� ��������� � ���� ���������� ����� � ������ ��� ������ �������.
	2) ��� ������ ������ ������� ���������� - ����� ����� �� ������ ����� ����������������� � ������ �����.
	3) ������ ����� ������� ������ � ������ � ��� ����.
	4) ����� ��������� ���� ����� ������� ��� ����:
	- ��������� ����� �������� �����, ����� ����� ���� ��������� ���������� ������ � ����� � ������� ������
	- ����� ���������� �� ��������� ����� �����, ����� ��������� ������ � ������� ������
	- ������������ �� ������ ����� Seal Of Ruler (���� ����� 3 ������).
	5) ���� ��������! ���� � ���� ����� ����� ����� (�� ����� 1 ������� �� ���� IP-�����!)
	��� ������������ �������? - ��� ������ ���� ������� � ����� ����� - ��� ������ ����� ����� �� ������.
*/


/*

@myIDs
* ������� : ��� ����� ������ ���\��� ��������� [ webstats sync ]
* ���� �� ����� ����� ��� � �� ����� �������� - �������� � ���
* ��� ������ �� ������ ���� �� ��������
* ����� ��������� ����� ����� : 
1) ���������� ������ (sync hud: which team taken castle now)
2) �������� ������������� ������ �� ����� ������� �����
* ���������� ������ ������������ ����� ������ 1 ����� ����� �� ������ (���� ��� ������� - ���������� �����������. ������� ������������) -- ����� ����� ����
* @truel2 : SEAL casttime : 2 min

@headquarters @myIDs
* ������������ ����� ������ ������ 1 hq �� ����� �� ����� �������
* ��������� ����� �����, � ���� ���� ����� �������� (@l2 : 300 gemstone B)
* ����� ������ ����� ������ - ����� ��������� ������ ( �� ��� �� ��������� ����� )
* ����� ��������� - � ���� ������������ ���� Headquarters, ������� ������� � ������ + OFFSET

*/


// Siege Headquarters

// The Build Headquarters skill is obtained by the clan leader when the clan reaches level 4. 
// This skill establishes an encampment near the castle/battlefield. 
// It allows clan members to restart at that site (not at the second closest town) and regenerate HP and MP faster than they would normally.

// This skill may only be used by clan leaders on the siege's attacker list and consumes 300 Gemstone C when cast. 
// This skill may be used only within a designated area of the battlefield, and can only be used once during the siege period.

// A siege encampment disappears when the siege period is over.

// The headquarters flag also has a certain HP and can be attacked. If the flag falls, 
// members of the clan restart from the second nearest town upon their deaths.

public Create_Headquarters(id)
{
	// get ids X Y Z
	// add rand offset to x, y
	// create cast line
	// @getl2 cast time
	// create entity
	// add to core entity killed,takedamage etc
}
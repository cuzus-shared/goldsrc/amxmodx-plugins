// --------------------------------------------------
// Database
// --------------------------------------------------

#define MYSQL_HOST	"localhost"
#define MYSQL_USER	"root"
#define MYSQL_PASS	"root"
#define MYSQL_DB	"linestrike"

public Database_init()
{
	out("==================================================================-[ Database ]");

	SQL_Tuple = SQL_MakeDbTuple(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
	
	new SQL_ErrorCode;
	new SQL_ErrorString[512];
	new Handle:SqlConnection = SQL_Connect(SQL_Tuple, SQL_ErrorCode, SQL_ErrorString, 511);
	
	if(SqlConnection == Empty_Handle){
		Log(FATAL, "Database", "MySQL connection failed!");
		pluginFail(SQL_ErrorString, 0); // plugin quit
	}
	else{
		Log(INFO, "Database", "MySQL connected. Preparing tables ...");
		static query[2048];
		static res; res = 0;

		/* TABLE: `characters` */
		res += formatex(query[res], charsmax(query)-res, "CREATE TABLE IF NOT EXISTS `characters` (`id` INT NOT NULL AUTO_INCREMENT, ");
		res += formatex(query[res], charsmax(query)-res, "`char_name` VARCHAR(45) NOT NULL, ");
		res += formatex(query[res], charsmax(query)-res, "`password` VARCHAR(100) NOT NULL, ");
		res += formatex(query[res], charsmax(query)-res, "`email` VARCHAR(100) NOT NULL, ");
		res += formatex(query[res], charsmax(query)-res, "`reg_ip` VARCHAR(20) NOT NULL DEFAULT '0.0.0.0', ");
		res += formatex(query[res], charsmax(query)-res, "`last_ip` VARCHAR(20) NOT NULL DEFAULT '0.0.0.0', ");

		res += formatex(query[res], charsmax(query)-res, "`title` VARCHAR(17) DEFAULT '', ");
		res += formatex(query[res], charsmax(query)-res, "`clan_tag` VARCHAR(17) DEFAULT '', ");
		
		res += formatex(query[res], charsmax(query)-res, "`race` INT(1) NOT NULL DEFAULT 1, "); //skinid
		res += formatex(query[res], charsmax(query)-res, "`classid` INT(2) NOT NULL DEFAULT 1, "); //last_classid
		res += formatex(query[res], charsmax(query)-res, "`hair` INT(4) NOT NULL DEFAULT 0, ");
		res += formatex(query[res], charsmax(query)-res, "`pvp` INT(11) NOT NULL DEFAULT 0, ");
		res += formatex(query[res], charsmax(query)-res, "`pk` INT(11) NOT NULL DEFAULT 0, ");
		res += formatex(query[res], charsmax(query)-res, "`deaths` INT(11) NOT NULL DEFAULT 0, ");
		res += formatex(query[res], charsmax(query)-res, "`hero` INT(1) NOT NULL DEFAULT 0, ");
		res += formatex(query[res], charsmax(query)-res, "`noble` INT(1) NOT NULL DEFAULT 0, ");
		
		res += formatex(query[res], charsmax(query)-res, "`online_time` INT(11) NOT NULL DEFAULT 0, ");
		res += formatex(query[res], charsmax(query)-res, "`played_maps` INT(11) NOT NULL DEFAULT 0, ");
		res += formatex(query[res], charsmax(query)-res, "`maps_win` INT(11) NOT NULL DEFAULT 0, ");
		res += formatex(query[res], charsmax(query)-res, "`maps_lose` INT(11) NOT NULL DEFAULT 0, ");
		
		res += formatex(query[res], charsmax(query)-res, "`access_level` INT(1) NOT NULL DEFAULT 0, ");
		res += formatex(query[res], charsmax(query)-res, "`birthday` VARCHAR(20) NOT NULL DEFAULT '', ");
		res += formatex(query[res], charsmax(query)-res, "`last_visit` VARCHAR(20) NOT NULL DEFAULT '', ");

		res += formatex(query[res], charsmax(query)-res, "`weapon_enchant` VARCHAR(25) NOT NULL DEFAULT 'a00b00c00d00e00f00g00h00', ");

		res += formatex(query[res], charsmax(query)-res, "`a_exp` INT(11) NOT NULL DEFAULT 0, `a_lvl` INT(11) NOT NULL DEFAULT 1, ");
		res += formatex(query[res], charsmax(query)-res, "`b_exp` INT(11) NOT NULL DEFAULT 0, `b_lvl` INT(11) NOT NULL DEFAULT 1, ");
		res += formatex(query[res], charsmax(query)-res, "`c_exp` INT(11) NOT NULL DEFAULT 0, `c_lvl` INT(11) NOT NULL DEFAULT 1, ");
		res += formatex(query[res], charsmax(query)-res, "`d_exp` INT(11) NOT NULL DEFAULT 0, `d_lvl` INT(11) NOT NULL DEFAULT 1, ");
		res += formatex(query[res], charsmax(query)-res, "`e_exp` INT(11) NOT NULL DEFAULT 0, `e_lvl` INT(11) NOT NULL DEFAULT 1, ");
		res += formatex(query[res], charsmax(query)-res, "`f_exp` INT(11) NOT NULL DEFAULT 0, `f_lvl` INT(11) NOT NULL DEFAULT 1, ");
		res += formatex(query[res], charsmax(query)-res, "`g_exp` INT(11) NOT NULL DEFAULT 0, `g_lvl` INT(11) NOT NULL DEFAULT 1, ");
		res += formatex(query[res], charsmax(query)-res, "`h_exp` INT(11) NOT NULL DEFAULT 0, `h_lvl` INT(11) NOT NULL DEFAULT 1, ");
		
		res += formatex(query[res], charsmax(query)-res, "`ban_chat` INT(11) NOT NULL DEFAULT 0, "); // ban chat time (if > 0 chat is banned)
		res += formatex(query[res], charsmax(query)-res, "`ban_chat_reason` VARCHAR(255) DEFAULT '', ");
		res += formatex(query[res], charsmax(query)-res, "`ban_acc` INT(1) NOT NULL DEFAULT 0, ");
		res += formatex(query[res], charsmax(query)-res, "`ban_acc_reason` VARCHAR(255) DEFAULT '', ");
		
		res += formatex(query[res], charsmax(query)-res, "PRIMARY KEY (id) ); ");
		
		new Handle:CreateCharactersTable = SQL_PrepareQuery(SqlConnection, query);
		if(!SQL_Execute(CreateCharactersTable)){
			Log(ERROR, "Database", "Error in creating table `characters`");
			SQL_QueryError(CreateCharactersTable, SQL_ErrorString, 511);
			pluginFail(SQL_ErrorString, 0);
		}
		SQL_FreeHandle(CreateCharactersTable);
		
		/* TABLE: `online` */
		new Handle:CreateOnlineTable = SQL_PrepareQuery(SqlConnection, "CREATE TABLE IF NOT EXISTS `online` (`char_name` VARCHAR(45) NOT NULL); ");
		if(!SQL_Execute(CreateOnlineTable)){
			SQL_QueryError(CreateOnlineTable, SQL_ErrorString, 511);
			Log(ERROR, "Database", "Error in creating table `online`");
			pluginFail(SQL_ErrorString, 0);
		}
		SQL_FreeHandle(CreateOnlineTable);
		
		/* TABLE: `spawnlist` */
		new Handle:CreateSpawnlistTable = SQL_PrepareQuery(SqlConnection, "CREATE TABLE IF NOT EXISTS `spawnlist` (`id` INT NOT NULL AUTO_INCREMENT, `map` VARCHAR(60) NOT NULL, `descr` VARCHAR(255) DEFAULT '', `npcid` INT(11) NOT NULL DEFAULT 0, `loc` VARCHAR(64) NOT NULL, `heading` VARCHAR(48) NOT NULL, PRIMARY KEY (id)); ");
		if(!SQL_Execute(CreateSpawnlistTable)){
			SQL_QueryError(CreateSpawnlistTable, SQL_ErrorString, 511);
			Log(ERROR, "Database", "Error in creating table `spawnlist`");
			pluginFail(SQL_ErrorString, 0);
		}
		SQL_FreeHandle(CreateSpawnlistTable);
		
		new Handle:SelectFromSpawnlist = SQL_PrepareQuery(SqlConnection, "SELECT * FROM `spawnlist`;");
		if(SQL_Execute(SelectFromSpawnlist))
		{
			if(SQL_NumResults(SelectFromSpawnlist) > 0){
				Log(DEBUG, "Database", "Table `spawnlist` is not empty, skip filling ...");
			}
			else
			{
				static query[4096];
				static len; len = 0;
				len += formatex(query[len], charsmax(query)-len, "INSERT INTO `spawnlist` (`id`,`map`,`descr`,`npcid`,`loc`,`heading`) VALUES ");
				// TODO: read from *SQL files
				// cruma_tower_v1_arc
				//	-- main room (near ct)
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Porta', '20213', '730.48 80.39 -215.96', '-0.46 -154.91'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Porta', '20213', '-322.81 217.02 -218.96', '0.307 -149.88'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Excuro', '20214', '35.05 944.97 -215.96', '-0.285 160.4'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Excuro', '20214', '1097.36 769.53 -219.96', '-0.17 -109.5'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Excuro', '20214', '231.03 -280.94 -218.96', '-0.197 92.55'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Excuro', '20214', '745.26 -357.18 -215.96', '-0.57 -56.6'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Catherok', '21035', '560.6 596.02 -218.96', '0.022 -81.9'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Catherok', '21035', '-339.63 -279.05 -218.96', '-0.527 40.495'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Catherok', '21035', '453.57 -349.84 -218.96', '-0.85 -173.9'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Mordeo', '20215', '-158.67 -737.15 -215.96', '-0.61 -11.6'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Treasure Chest', '21808', '698.6 364.78 -215.97', '-0.59 -136.8'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Treasure Chest', '21808', '93.3 427.53 -215.96', '-0.109 -4.1'), ");
				//	-- 2nd room (near t)
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Porta', '20213', '533.06 -2967.47 -218.96', '0.32 128.04'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Excuro', '20214', '25.79 -3437.42 -219.96', '-0.9 -159.15'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Excuro', '20214', '1106.4 -2803.99 -218.96', '-0.8 118.04'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Excuro', '20214', '1557.99 -2680.02 -219.96', '-0.85 76.58'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Mordeo', '20215', '939.47 -3327.13 -218.96', '-0.5 -113.31'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Mordeo', '20215', '1297.2 -2029.8 -219.96', '-0.47 -72.98'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Catherok', '21035', '1274.67 -3069.21 -218.96', '-0.04 117.83'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Catherok', '21035', '537.93 -2359.19 -218.96', '-0.7 127.16'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Catherok', '21035', '970.98 -2793.34 -211.96', '-0.54 9.9'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'cruma_tower_v1_arc', 'Treasure Chest', '21808', '750.38 -2735.17 -211.96', '-0.13 -62.25'), ");
				// deck17_arc
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Bone Puppeteer', '21581', '248.03 1733.98 -411.96', '-0.13 139.64'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Behemoth Zombie', '21578', '769.28 1609.48 -411.96', '-0.26 -41.83'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Bone Puppeteer', '21581', '892.68 2063.67 -411.96', '-0.9 15.23'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Bone Puppeteer', '21581', '1305.76 1722.64 -411.96', '-0.3 -146.95'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Bone Puppeteer', '21581', '836.68 1032.31 -491.96', '0.3 -81.48'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Bone Puppeteer', '21581', '791.63 505.13 -491.96', '-0.17 6.28'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Behemoth Zombie', '21578', '1160.24 -171.61 -491.96', '0.5 136.49'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Behemoth Zombie', '21578', '580.18 -62.45 -107.96', '0.35 16.76'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Behemoth Zombie', '21578', '998.17 -191.02 -107.96', '0.39 132.54'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Bone Puppeteer', '21581', '226.406 703.44 -123.96', '-0.15 89.6'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Behemoth Zombie', '21578', '888.28 774.99 -381.5', '-0.5 55.0'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Bone Puppeteer', '21581', '912.0 -199.0 -779.96', '-0.5 -147.0'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'deck17_arc', 'Behemoth Zombie', '21578', '734.6 1794.18 -763.96', '0.35 118.79'), ");
				// baiums_lair_v1_arc
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'baiums_lair_v1_arc', 'Guardian Archangel', '21067', '-814.0 1266.54 294.0', '0.5 -138.9'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'baiums_lair_v1_arc', 'Guardian Archangel', '21067', '-1243.0 -1222.57 294.0', '0.29 54.3'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'baiums_lair_v1_arc', 'Guardian Archangel', '21067', '925.35 -2026.6 294.0', '0.06 148.7'), ");
				len += formatex(query[len], charsmax(query)-len, "(NULL, 'baiums_lair_v1_arc', 'Guardian Archangel', '21067', '2242.39 -154.92 294.0', '-0.08 -153.2'); ");

				new Handle:InsertSpawnlistTable = SQL_PrepareQuery(SqlConnection, query);
				if(!SQL_Execute(InsertSpawnlistTable)){
					SQL_QueryError(InsertSpawnlistTable, SQL_ErrorString, 511);
					Log(ERROR, "Database", "Error on inserting table `spawnlist`");
					pluginFail(SQL_ErrorString, 0);
				}
				SQL_FreeHandle(InsertSpawnlistTable);
			}
		}
		else{
			SQL_QueryError(SelectFromSpawnlist, SQL_ErrorString, 511);
			Log(ERROR, "Database", "Error on select from table `spawnlist`");
			pluginFail(SQL_ErrorString, 0);
		}
		SQL_FreeHandle(SelectFromSpawnlist);

		/* TABLE: `maps` */
		new Handle:CreateMapsTable = SQL_PrepareQuery(SqlConnection, "CREATE TABLE IF NOT EXISTS `maps` (`id` INT NOT NULL, `map_name` VARCHAR(60) NOT NULL, PRIMARY KEY (id)); "); //AUTO_INCREMENT
		if(!SQL_Execute(CreateMapsTable)){
			SQL_QueryError(CreateMapsTable, SQL_ErrorString, 511);
			Log(ERROR, "Database", "Error in creating table `maps`");
			pluginFail(SQL_ErrorString, 0);
		}
		SQL_FreeHandle(CreateMapsTable);
		
		new Handle:SelectFromMaps = SQL_PrepareQuery(SqlConnection, "SELECT * FROM `maps`;");
		if(SQL_Execute(SelectFromMaps))
		{
			if(SQL_NumResults(SelectFromMaps) > 0){
				Log(DEBUG, "Database", "Table `maps` is not empty, skip filling ...");
			}
			else{
				new Handle:InsertMapsTable = SQL_PrepareQuery(SqlConnection, "INSERT INTO `maps` VALUES (1,'null'),(2,'null'),(3,'null'),(4,'null'),(5,'null'); ");
				if(!SQL_Execute(InsertMapsTable)){
					SQL_QueryError(InsertMapsTable, SQL_ErrorString, 511);
					Log(ERROR, "Database", "Error in inserting table `maps`");
					pluginFail(SQL_ErrorString, 0);
				}
				SQL_FreeHandle(InsertMapsTable);
			}
		}else{
			SQL_QueryError(SelectFromMaps, SQL_ErrorString, 511);
			Log(ERROR, "Database", "Error in select from table `maps`");
			pluginFail(SQL_ErrorString, 0);
		}
		SQL_FreeHandle(SelectFromMaps);

		// ================================
		// Auto-Save Manager global task
		set_task(MYSQL_AUTO_SAVE_TIME, "dbAutoSaveManager", _, _, _, "b");
	}
	
	SQL_FreeHandle(SqlConnection);
	
	// clear `online` table on server start
	updateOnlineStatus(0);
}

public dbAutoSaveInstant()
{
	Log(DEBUG, "Database", "dbAutoSaveInstant(): Saving all players data ...");
	if(warmupConfirmed())
		for(new id = 1; id <= GMaxPlayers; id++)
			if(is_user_connected(id))
				Player_saveData(id);
}

public dbAutoSaveManager()
{
	Log(DEBUG, "Database", "dbAutoSaveManager(): Saving data ...");
	if(warmupConfirmed())
		for(new id = 1; id <= GMaxPlayers; id++)
			if(is_user_connected(id))
			{
				new Float:tasktime = random_float(0.3,33.0);
				set_task(tasktime, "dbAutoSaveTask", TASK_MYSQL_AUTO_SAVE+id);
				Log(DEBUG, "Database", "Added AutoSave task in %ds for player '%s'", floatround(tasktime), getName(id));
			}
}

public dbAutoSaveTask(id)
{
	id -= TASK_MYSQL_AUTO_SAVE;
	Log(DEBUG, "Database", "AutoSave task running in this moment");
	Player_saveData(id);
}

updateOnlineStatus(id, bool:action=true)
{
	new SQL_ErrorCode;
	new SQL_ErrorString[512];
	new Handle:SqlConnection = SQL_Connect(SQL_Tuple, SQL_ErrorCode, SQL_ErrorString, 511);
	if(SqlConnection == Empty_Handle){
		Log(FATAL, "Database", "MySQL connection failed!");
		pluginFail(SQL_ErrorString, id);
	}

	new player[MAX_CHARNAME_LENGTH];
	new info[4] = "NIL";
	
	static query[256];
	static len; len=0;

	if(id > 0 && id < 33)
	{
		get_user_name(id, player, MAX_CHARNAME_LENGTH-1);

		if(action){ // online
			len += formatex(query[len], charsmax(query)-len, "INSERT INTO `online` (`char_name`) VALUES ('%s');", player);
			format(info, 4, "ON");
		}
		else if(!action){ // offline
			len += formatex(query[len], charsmax(query)-len, "DELETE FROM `online` WHERE char_name='%s';", player);
			format(info, 4, "OFF");
		}
	}
	else if(id == 0) // clear table
	{
		len += formatex(query[len], charsmax(query)-len, "DELETE FROM `online`;");
		format(info, 4, "OFF");
		format(player, 4, "ALL");
	}

	new Handle:InsertIntoOnline = SQL_PrepareQuery(SqlConnection, query);
	if(!SQL_Execute(InsertIntoOnline)){
		SQL_QueryError(InsertIntoOnline, SQL_ErrorString, 511);
		pluginFail(SQL_ErrorString, id);
	}else{
		Log(DEBUG, "Database", "Update online status = %s for '%s'", info, player);
	}

	SQL_FreeHandle(InsertIntoOnline);
	SQL_FreeHandle(SqlConnection);
}

/* Used only for thread queries: SQL_ThreadQuery(SQL_Tuple, "QueryHandler", query); */
public QueryHandler(FailState, Handle:Query, Error[], ErrCode, Data[], DataSize, Float:QueryTime)
{
	switch(FailState){
		case TQUERY_CONNECT_FAILED:	Log(FATAL, "Database", "Failed to connect (%d): %s", ErrCode, Error);
		case TQUERY_QUERY_FAILED:	Log(FATAL, "Database", "(%d): %s", ErrCode, Error);
		//case TQUERY_SUCCESS
	}
	return PLUGIN_HANDLED;
}

public Database_pluginEnd() // close connection
{
	if(SQL_Tuple != Empty_Handle)
		SQL_FreeHandle(SQL_Tuple);
}
// --------------------------------------------------
// Shoot
// --------------------------------------------------

public makeBowShot(id)
{
	if(drawFire(id)) 
	{
		if(pev(id, pev_flags) & FL_DUCKING)
			Player_playAnimP(id, "crouch_shoot_shotgun");
		else
			Player_playAnimP(id, "ref_shoot_shotgun");

		Player_playAnimV(id, 1); //shoot1

		emit_sound(id, CHAN_WEAPON, "common/null.wav", VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
		emit_sound(id, CHAN_WEAPON, S_BOW_SMALL[random_num(0,5)], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	}
}

public drawFire(id)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		set_pev(ent, pev_classname, "arrow");
		engfunc(EngFunc_SetModel, ent, W_ARROW);
		engfunc(EngFunc_SetSize, ent, Float:{-1.0, -1.0, 0.0}, Float:{1.0, 1.0, 1.0});
		
		new vorigin[3], Float:voriginf[3], Float:vangles[3], Float:nvelocity[3];

		get_user_origin(id, vorigin, 1); // 1 - position from eyes (weapon aiming)
		IVecFVec(vorigin, voriginf);
		voriginf[2] -= 16.0;
		engfunc(EngFunc_SetOrigin, ent, voriginf);

		/*������ ���� ����� �������� angle[1] *=-1
		������ ����� �������� ��������� �����, �� ������ �������������� ������� ����*/
		pev(id, pev_angles, vangles);
		//vangles[0] = random_float(-180.0, 180.0);
		//vangles[1] += -90.0;
//		vangles[0] *= -1;
		vangles[2] += 180.0;
		set_pev(ent, pev_angles, vangles);

		set_pev(ent, pev_movetype, MOVETYPE_BOUNCE);
		set_pev(ent, pev_solid, 2);
		set_pev(ent, pev_gravity, 1.0); //0.5
		set_pev(ent, pev_owner, id);

		// Set the fly spd and shot power
		new veloc = 0;
		ShotDamage[id] = 0.0;

		if(is_user_bot(id)){
			veloc = random_num(1800, 4400);
			ShotDamage[id] = random_float(CLASSDMG_A_SHOT_90_MIN, CLASSDMG_A_SHOT_MAX);
		}
		else
		{
			if(ShotPowerAmt[id] < 10){
				veloc = 350;
				ShotDamage[id] = random_float(CLASSDMG_A_SHOT_10_MIN, CLASSDMG_A_SHOT_10_MAX);
			}
			else if(ShotPowerAmt[id] > 10 && ShotPowerAmt[id] < 30){
				veloc = 1500;
				ShotDamage[id] = random_float(CLASSDMG_A_SHOT_30_MIN, CLASSDMG_A_SHOT_30_MAX);
			}
			else if(ShotPowerAmt[id] > 30 && ShotPowerAmt[id] < 60){
				veloc = 2500;
				ShotDamage[id] = random_float(CLASSDMG_A_SHOT_60_MIN, CLASSDMG_A_SHOT_60_MAX);
			}
			else if(ShotPowerAmt[id] > 60 && ShotPowerAmt[id] < 90){
				veloc = 3500;
				ShotDamage[id] = random_float(CLASSDMG_A_SHOT_90_MIN, CLASSDMG_A_SHOT_90_MAX);
			}
			else if(ShotPowerAmt[id] > 90){
				veloc = 3800;
				ShotDamage[id] = CLASSDMG_A_SHOT_MAX;
			}
		}

		// More damage and accuracy with zoom
		if(PlayerHasZoom[id])
		{
			veloc += 250;
			ShotDamage[id] += DAMAGE_ADD_WITH_ZOOM; //25.0
		}
		if(PIN_Snipe[id])
		{
			veloc += 150;
			ShotDamage[id] += random_float(SKILLDAMAGE_SNIPE_MIN, SKILLDAMAGE_SNIPE_MAX);
		}

		velocity_by_aim(id, veloc, nvelocity);

		ShotPowerAmt[id] = 0;
		setShotPower(id, 0);

	/*	
		// shoot worse while moving
		new buttons = pev(id, pev_button);
		if(buttons & IN_JUMP || buttons & IN_FORWARD || buttons & IN_BACK || buttons & IN_MOVELEFT || buttons & IN_MOVERIGHT)
			modifier *= 2;
		
		new disp = 88; // dispersion
		nvelocity[0] += (random(disp)-disp/2) * modifier;
		nvelocity[1] += (random(disp)-disp/2) * modifier;
		nvelocity[2] += (random(disp)-disp/2) * modifier;
	*/
	
		if(getTeam(id) == 1)
			fm_set_rendering(ent, kRenderFxGlowShell, 55,5,5, kRenderNormal, 9); //3
		else
			fm_set_rendering(ent, kRenderFxGlowShell, 5,5,55, kRenderNormal, 9); //3
		
		set_task(0.1, "setArrowTrail", ent+TASK_SET_ARROW_TRAIL);
		
		set_pev(ent, pev_velocity, nvelocity);
		
		//set_pev(ent, pev_effects, pev(ent, pev_effects) & ~EF_NODRAW);

		if(SAY2_DATA[id][S2_ARROW_CAM] && !is_user_bot(id)){
			//attach_view(id, ent);
			engfunc(EngFunc_SetView, id, ent);
		}

		return true; //ent;
	}
	return false;
}

public setArrowTrail(ent)
{
	ent -= TASK_SET_ARROW_TRAIL;

	if(pev_valid(ent))
	{
		new clr[3];
//		clr = (get_user_team(pev(ent, pev_owner)) == 1) ? {180,150,150} : {150,150,180};
		clr = (get_user_team(pev(ent, pev_owner)) == 1) ? {185,50,50} : {50,50,185};

		message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
		write_byte(TE_BEAMFOLLOW);
		write_short(ent);

		// randomize trails sprites
		switch(rand(1,4)){
			case 1: {write_short(SPR_BULLET_TRAIL[0]);}
			case 2: {write_short(SPR_BULLET_TRAIL[1]);}
			case 3: {write_short(SPR_BULLET_TRAIL[2]);}
			case 4: {write_short(SPR_XBEAM4);}
		}
//		write_byte(random_num(2,5)); 			// lifetime
		write_byte(rand(5,7)); 					// lifetime
		write_byte(1);							// size
		write_byte(clr[0]);						// R
		write_byte(clr[1]);						// G
		write_byte(clr[2]);						// B
//		write_byte(random_num(190,240));		// opacity
		write_byte(255);						// opacity
		message_end();
	}

	if(task_exists(ent+TASK_SET_ARROW_TRAIL))
		remove_task(ent+TASK_SET_ARROW_TRAIL);
}

public removeZoom(id)
{
	PlayerHasZoom[id] = false;
	cs_set_user_zoom(id, CS_RESET_ZOOM, 0);
	return PLUGIN_HANDLED;
}

public setZoom(id)
{
	PlayerHasZoom[id] = true;
	cs_set_user_zoom(id, CS_SET_AUGSG552_ZOOM, 0);
	return PLUGIN_HANDLED;
}
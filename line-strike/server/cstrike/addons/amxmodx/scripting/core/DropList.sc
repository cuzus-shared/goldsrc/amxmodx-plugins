// --------------------------------------------------
// Drop List
// --------------------------------------------------

/**
 * This function randomize item drop position near the died npc.
 */
public DropList_setParams(ent, Float:origin[3], Float:angles[3])
{
	origin[0] += random_float(-30.0, 30.0);
	origin[1] += random_float(-30.0, 30.0);
	set_pev(ent, pev_origin, origin);

	angles[1] += random_num(1, 360);
	set_pev(ent, pev_angles, angles);

	set_pev(ent, pev_solid, SOLID_TRIGGER);
	set_pev(ent, pev_movetype, MOVETYPE_TOSS);

	engfunc(EngFunc_DropToFloor, ent);

	return true;
}

/**
 * This function implements any type of possible drop. No more needs to check everytime the NpcId.
 * 
 * Return dropped items amount.
 */
public DropList_drop(npc, bool:adena, adena_chance, adena_amount, bool:etcitem, etcitem_chance, etcitem_amount, bool:potion, potion_chance, potion_amount)
{
	new Float:origin[3], Float:angles[3];
	pev(npc, pev_origin, origin);
	pev(npc, pev_angles, angles);
	npc = 0; // we want to write here return value

	if(adena)
	{
		if(random_num(0,100) <= adena_chance)
		{
			for(new i=1; i <= random_num(1,adena_amount); i++)
			{
				new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
				if(ent)
				{
					set_pev(ent, pev_classname, "Item@Adena");
					engfunc(EngFunc_SetModel, ent, M_L2DROP_ITEM);
					set_pev(ent, pev_body, DROPITEM_ADENA); // <DropItem>
					engfunc(EngFunc_SetSize, ent, Float:{-2.79, -0.0, -6.14}, Float:{2.42, 1.99, 6.35});

					if(DropList_setParams(ent, origin, angles))
						emit_sound(ent, CHAN_VOICE, S_ITEMDROP_ETC_MONEY, random_float(0.7,1.0), ATTN_STATIC, 0, PITCH_NORM);
					
					npc++;
				}
			}
		}
	}
	if(etcitem)
	{
		if(random_num(0,100) <= etcitem_chance)
		{
			for(new i=1; i <= random_num(1,etcitem_amount); i++)
			{
				new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
				if(ent)
				{
					set_pev(ent, pev_classname, "Item@Etc");
					engfunc(EngFunc_SetModel, ent, M_L2DROP_ITEM);
					set_pev(ent, pev_body, random_num(DROPITEM_CRYSTAL, DROPITEM_THREAD)); //1,12	// <DropItem>
					engfunc(EngFunc_SetSize, ent, Float:{-1.0, -1.0, 0.0}, Float:{1.0, 1.0, 3.0});

					if(DropList_setParams(ent, origin, angles))
						emit_sound(ent, CHAN_VOICE, (random_num(1,2)==1 ? S_ITEMDROP_FIGURE : S_ITEMDROP_TOMBSTONE), random_float(0.7,1.0), ATTN_STATIC, 0, PITCH_NORM);
					
					npc++;
				}
			}
		}
	}
	if(potion)
	{
		if(random_num(0,100) <= potion_chance)
		{
			for(new i=1; i <= random_num(1,potion_amount); i++)
			{
				new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
				if(ent)
				{
					set_pev(ent, pev_classname, "Item@Potion");
					engfunc(EngFunc_SetModel, ent, M_L2POTION);
					set_pev(ent, pev_body, random_num(0,6)); // 0 - HP, 1 - MP
					engfunc(EngFunc_SetSize, ent, Float:{-1.0, -1.0, -7.5}, Float:{1.0, 1.0, 7.5}); // {-23.160000, -13.660000, -0.050000}, {11.470000, 12.780000, 6.720000}

					if(DropList_setParams(ent, origin, angles))
						emit_sound(ent, CHAN_VOICE, S_ITEMDROP_POTION, random_float(0.7,1.0), ATTN_STATIC, 0, PITCH_NORM);
					
					npc++;
				}
			}
		}
	}

	return npc;
}
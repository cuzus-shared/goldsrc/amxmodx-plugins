// --------------------------------------------------
// Guard
// --------------------------------------------------

#define NPCID_GUARD			31032
#define NPCID_SENTRY		31034

#define GUARD_HEALTH		500.0
#define GUARD_DAMAGE_MIN	10.0
#define GUARD_DAMAGE_MAX	30.0
#define GUARD_AGGR_RANGE	500.0

#define ANIM_GUARD_IDLE		0	//1, 17, 18
#define ANIM_GUARD_ATK		10
#define ANIM_GUARD_DMG		13
#define ANIM_GUARD_DEATH	15

public thinkGuardBase(ent) // think base for Guards (used 2 times)
{
	//set_pev(ent, pev_frame, 1);
	//set_pev(ent, pev_sequence, ANIM_GUARD_IDLE);
	//set_pev(ent, pev_gaitsequence, ANIM_GUARD_IDLE);
	//set_pev(ent, pev_framerate, 1.5); //1.2
	Npc_playAnim(ent, ANIM_GUARD_IDLE, 1.4);

	set_pev(ent, pev_nextthink, get_gametime() + 2.5); //3.0
	
	new Float:origin[3];
	pev(ent, pev_origin, origin);
	new target = fm_find_ent_in_sphere(target, origin, GUARD_AGGR_RANGE);
	if(target)
	{
		if(pev_valid(target))
		{
			static this[32];
			pev(target, pev_classname, this, 31);
			
			if(isPlayer(this) || isMonster(this))
			{
				if(Util_canSee(ent, target))
					return target;
			}
		}
	}
	
	return -1;
}

public thinkTGuard(ent) // Core's think
{
	if(!pev_valid(ent))
        return;
	if(pev(ent, pev_health) <= 0.0){
		Log(WARN, "Guard", " thinkTGuard(): Entity ID %d is dead but not removed", ent);
		return;
	}
	
	new target = thinkGuardBase(ent);
	
	if(target)
	{
		if(isAlive(target) && cs_get_user_team(target) == CS_TEAM_CT)
			Guard_attack(ent, target);
		/*else
		{
			static this[32];
			pev(target, pev_classname, this, 31);
			
			if(isMonster(this)){
				Guard_attack(ent, target);
				Log(DEBUG, "Guard", " thinkTGuard(): Monster in range (ID=%d)", target);
			}
		}*/
	}
}

public thinkCTGuard(ent) // Core's think
{
	if(!pev_valid(ent))
        return;
	if(pev(ent, pev_health) <= 0.0){
		Log(WARN, "Guard", " thinkTGuard(): Entity ID %d is dead but not removed", ent);
		return;
	}
	
	new target = thinkGuardBase(ent);
	
	if(target)
	{
		if(isAlive(target) && cs_get_user_team(target) == CS_TEAM_T)
			Guard_attack(ent, target);
		/*else
		{
			static this[32];
			pev(target, pev_classname, this, 31);
			
			if(isMonster(this)){
				Guard_attack(ent, target);
				Log(DEBUG, "Guard", " thinkCTGuard(): Monster in range (ID=%d)", target);
			}
		}*/
	}
}

public Guard_posAround(Float:origin[3], size, pieces, team)
{
	//origin[2] =+ 6.0; // GROUND OFFSET
	new Float:angle_diff = 360.0 / float(pieces);
	new Float:a[3];
	new Float:v1[3], Float:v2[3];

	while(a[1] < 360.0)
	{
		angle_vector(a, ANGLEVECTOR_FORWARD, v1);
		a[1] += angle_diff;
		angle_vector(a, ANGLEVECTOR_FORWARD, v2);
		
		xs_vec_mul_scalar(v1, float(size), v1);
		xs_vec_mul_scalar(v2, float(size), v2);
		
		xs_vec_add(v1, origin, v1);
		xs_vec_add(v2, origin, v2);
		
		new iorigin[3];
		FVecIVec(Float:v1, iorigin);
		new Float:angles[3];
		angles[1] = randFloat(-180.0,180.0);
		angles[2] = 0.0;
		Guard_spawn(v1, angles, team);
	}
}

public Guard_setNameSprite(Float:origin[3], team)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		set_pev(ent, pev_classname, "Spr@NpcName");

		if(team == 1)
			engfunc(EngFunc_SetModel, ent, SPR_NPCNAME_EAST);
		else if(team == 2)
			engfunc(EngFunc_SetModel, ent, SPR_NPCNAME_WEST);
		
		set_pev(ent, pev_frame, 1.0);
		
		origin[2] += 45.0;

		engfunc(EngFunc_SetSize, ent, Float:{0.0,0.0,0.0}, Float:{1.0,1.0,1.0});
		engfunc(EngFunc_SetOrigin, ent, origin);
		set_pev(ent, pev_solid, SOLID_NOT);
		set_pev(ent, pev_movetype, MOVETYPE_NONE);
		set_pev(ent, pev_scale, 0.5);
		set_pev(ent, pev_rendermode, 5); // 5 - Additive
		set_pev(ent, pev_renderamt, 255.0);
		set_pev(ent, pev_renderfx, 14);
	}
}

public Guard_spawn(Float:origin[3], Float:angles[3], team)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		switch(team)
		{
			case 1: { // T
				set_pev(ent, pev_classname, "L2Guard@East");
				fm_set_rendering(ent, kRenderFxGlowShell, 25,5,5, kRenderNormal, 3);
				Guard_setNameSprite(origin, team);
			}
			case 2: { // CT
				set_pev(ent, pev_classname, "L2Guard@West");
				fm_set_rendering(ent, kRenderFxGlowShell, 5,5,25, kRenderNormal, 3);
				Guard_setNameSprite(origin, team);
			}
		}
		
		engfunc(EngFunc_SetModel, ent, M_NPC_GUARD_FDARKELF);
		set_pev(ent, pev_body, 1);
		engfunc(EngFunc_SetSize, ent, Float:{-12.0, -12.0, 0.0}, Float:{12.0, 12.0, 76.0});
		engfunc(EngFunc_SetOrigin, ent, origin);
		
		Log(DEBUG, "Guard", "Guard_spawn(): entId=%d (%d,%d,%d)", ent, floatround(origin[0]), floatround(origin[1]), floatround(origin[2]));
		
		set_pev(ent, pev_angles, angles);
		
		set_pev(ent, pev_solid, SOLID_BBOX);
		set_pev(ent, pev_movetype, MOVETYPE_NONE);
		set_pev(ent, pev_health, GUARD_HEALTH);
		set_pev(ent, pev_takedamage, DAMAGE_YES);
		set_pev(ent, pev_deadflag, DEAD_NO);
		set_pev(ent, pev_gravity, 1.0);

//		set_pev(ent, pev_owner, id);
//		set_pev(ent, pev_controller_0, 125);
//		set_pev(ent, pev_controller_1, 125);
//		set_pev(ent, pev_controller_2, 125);
//		set_pev(ent, pev_controller_3, 125);
		set_pev(ent, pev_animtime, 2.0); //2.0
		set_pev(ent, pev_framerate, 1.0);
		set_pev(ent, pev_sequence, 1); //0
		set_pev(ent, pev_gaitsequence, 1);
		set_pev(ent, pev_nextthink, get_gametime() + 3.0);
		set_pev(ent, pev_gamestate, 1);
		
		engfunc(EngFunc_DropToFloor, ent);
		dllfunc(DLLFunc_Think, ent);
		
//		RegisterHamFromEntity(Ham_TakeDamage, ent, "Guard_hamTakeDamage", 1);
//		set_task(3.0, "Guard_spawned", ent + TASK_GUARD_SPAWNED);
	}
}

/*public Guard_spawned(taskid)
{
	new ent = taskid-TASK_GUARD_SPAWNED;

	remove_task(taskid);
}*/

public Guard_takeDamage(victim, inflicator, attacker, Float:damage, damage_type) // Core's hamTakeDamageEntity
{
	//Npc_playAnim(victim, ANIM_GUARD_DMG, 1.2);		// interrupt for other anims -> disabled
	
	if(randInt(1,3) == 3)
		emit_sound(victim, CHAN_VOICE, S_NPC_GUARD_FDARKELF[randInt(4,6)], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
}

public Guard_killed(guard)
{
	new Float:origin[4];
	new Float:tmp[3];
	pev(guard, pev_origin, tmp);
	origin[0] = tmp[0];
	origin[1] = tmp[1];
	origin[2] = tmp[2];
	
	Log(DEBUG, "Guard", "Guard_killed(): entId=%d (%d,%d,%d)", guard, floatround(origin[0]), floatround(origin[1]), floatround(origin[2]));

	new this[32];
	pev(guard, pev_classname, this, 31);
	if(equali(this, "L2Guard@East"))
		origin[3] = 1.0;
	else if(equali(this, "L2Guard@West"))
		origin[3] = 2.0;

	set_task(randFloat(25.0,30.0), "Guard_respawn", guard+TASK_GUARD_RESPAWN, _:origin, 4); // float task
	
	Npc_playAnim(guard, ANIM_GUARD_DEATH, 1.0);

	emit_sound(guard, CHAN_VOICE, S_NPC_GUARD_FDARKELF[3], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	
	fm_set_rendering(guard, kRenderFxNone, 255,255,255, kRenderTransAdd, 200);

	set_pev(guard, pev_solid, SOLID_NOT);
	set_pev(guard, pev_takedamage, 0.0);
	set_pev(guard, pev_movetype, MOVETYPE_FLY);
	set_pev(guard, pev_deadflag, DEAD_DYING);
	
	set_pev(guard, pev_gravity, 0.5);
	set_pev(guard, pev_velocity, {0.0, 0.0, 25.0});

	set_task(randFloat(3.5, 5.0), "Guard_removeCorpse", guard+TASK_GUARD_REMOVE_CORPSE);
}

public Guard_removeCorpse(ent) //taskid
{
	ent -= TASK_GUARD_REMOVE_CORPSE;

	new Float:origin[3];
	getEntOrigin(ent, origin);

	// Remove name sprite
	new namespr = -1;
	static this[32];
	while((namespr = engfunc(EngFunc_FindEntityInSphere, namespr, origin, 75.0)) != 0) // RANGE = 50
	{
		pev(namespr, pev_classname, this, 31);
		
		if(equali(this, "Spr@NpcName")){
			entityDispose(namespr);
			Log(DEBUG, "Guard", "while(...): remove namesprId=%d ==> break", namespr);
			break;
		}
	}
	// Remove entity
	if(pev_valid(ent))
		entityDispose(ent);

	remove_task(ent+TASK_GUARD_REMOVE_CORPSE);
}

public Guard_respawn(Float:origin[4], taskid)
{
	new Float:new_origin[3]; // killed guard position
	new_origin[0] = origin[0];
	new_origin[1] = origin[1];
	new_origin[2] = origin[2];
	
	new Float:angles[3];
	angles[0] = 0.0;
	angles[1] += randInt(1,360);
	angles[2] = 0.0;
	
	Guard_spawn(new_origin, angles, floatround(origin[3]) /* team: 1 or 2 */);
	
	remove_task(taskid);
}

public Guard_attack(guard, target)
{
	Npc_setAim(guard, target);

	Npc_playAnim(guard, ANIM_GUARD_ATK, 1.8); //1.6
	
	new params[2];
	params[0] = guard;
	params[1] = target;
	set_task(0.9, "Guard_attackPost", target+TASK_GUARD_ATTACK_POST, params, 2);
}

public Guard_attackPost(params[2], taskid) // params[0] = guard, params[1] = target
{
	if(Guard_shoot(params[0], params[1])){
		if(randInt(1,3 == 2)) 
			emit_sound(params[0], CHAN_AUTO, S_NPC_GUARD_FDARKELF[randInt(0,2)], randFloat(0.3,0.6), ATTN_NORM, 0, PITCH_NORM);	// chr atk
	}
}

public bool:Guard_shoot(guard, target)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		set_pev(ent, pev_classname, "guard_arrow");
		engfunc(EngFunc_SetModel, ent, W_ARROW);
		engfunc(EngFunc_SetSize, ent, Float:{-1.0, -1.0, 0.0}, Float:{1.0, 1.0, 1.0});

		new Float:origin[3];
		//pev(guard, pev_origin, origin);
		
		Util_getAimOriginDist(guard, origin, 15.0, false); // ignorewalls=false
		
		//origin[2] += 72.0;
		
		engfunc(EngFunc_SetOrigin, ent, origin);

		set_pev(ent, pev_solid, SOLID_BBOX);
		set_pev(ent, pev_scale, 1.0);
		set_pev(ent, pev_gravity, 1.0);
		set_pev(ent, pev_movetype, MOVETYPE_FLY);
		
		//&copy from Shoot.inl
		/*первый угол нужно умножить angle[1] *=-1
		просто когда например стреляешь вверх, то модель противоположно смотрит вниз*/
		pev(guard, pev_angles, origin); // OPT: angles
		origin[2] += 180.0;
		set_pev(ent, pev_angles, origin);

		new Float:targ_origin[3];
		pev(target, pev_origin, targ_origin);
		
		set_velocity_to_origin(ent, targ_origin, 700.0);
//		Util_moveEntity(ent, origin, 700.0);
		
		//set_task(0.1, "setArrowTrail", ent+TASK_ARROW_TRAIL);

		emit_sound(guard, CHAN_AUTO, S_BOW_SMALL[randInt(0,5)], randFloat(0.7,1.0), ATTN_NORM, 0, PITCH_NORM); // bow shot &copy

		return true;
	}
	return false;
}
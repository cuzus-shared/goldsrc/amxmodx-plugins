// --------------------------------------------------
// POD Bots
// --------------------------------------------------

public Bot_init()
{
	if(!Config[BOTS_ENABLED]){
		Log(INFO, "Podbot", "Bots are disabled")
		return;
	}

	if(isTeamGameMode()){
		server_cmd("pb_bot_join_team ANY");
		server_cmd("pb_ffa 0");
		server_cmd("mp_friendlyfire 0");
	}
	else if(isDmGameMode()){
		server_cmd("pb_bot_join_team T");
		server_cmd("pb_ffa 1");
		server_cmd("mp_friendlyfire 1");
	}
	else if(isContestGameMode()){
		server_cmd("pb_bot_join_team ANY");
		// bots no ffa etc / no friendlyfire
	}

	//server_cmd("pb_mapstartbotdelay 3");		// через сколько секунд после загрузки карты добавятся боты

	// Join tasks
	for(new i=0; i <= Config[MAX_BOTS_AMOUT]; i++)
	{
		if(!task_exists(i+TASK_PODBOT_AUTO_JOIN)){
			new Float:time_to_join = random_float(Float:Config[BOTS_TIME_TO_JOIN_MIN], Float:Config[BOTS_TIME_TO_JOIN_MAX]);
			set_task(time_to_join, "Bot_joinTask", i+TASK_PODBOT_AUTO_JOIN);
			
			Log(DEBUG, "Bot", "Bot #%d will be added after %ds", i, floatround(time_to_join));
		}
	}

	// Unjoin tasks
	if(Config[BOT_UNJOINS_ENABLED])
		for(new i=0; i <= Config[MAX_BOT_UNJOIN_TASKS]; i++)
		{
			if(random_num(1,4) == 1) // 25%
			{
				new Float:unjoin_time = 0.0;
				switch(GameMode)
				{
					case MODE_OUTPOST_DEFENSE: 	{unjoin_time = randF(240.0, 2500.0);}	// 4 min - 42 min
//					case MODE_CASTLE_SIEGE: 
					case MODE_LAST_HERO: 		{unjoin_time = randF(190.0, 600.0);}		// 3 min - 10 min
					case MODE_TEAM_VS_TEAM: 	{unjoin_time = randF(240.0, 1080.0);}	// 4 min - 18 min
				}
				set_task(unjoin_time, "Bot_unjoinTask", TASK_PODBOT_UNJOIN + i);
				Log(DEBUG, "Bot", "Added unjoin task#%d in %d min", TASK_PODBOT_UNJOIN + i, floatround(unjoin_time / 60));
			}
		}
}

public Bot_joinTask(taskid)
{
	server_cmd("pb add %d", random_num(50,100));
	remove_task(taskid);
}

public Bot_unjoinTask(taskid)
{
	new _list[32], bots;
	get_players(_list, bots, "dh") // "d" - skip real players. "h" - skip HLTV.

	for(new i = 0; i < bots; i++){
		server_cmd("pb remove #%d", get_user_userid(_list[i]));
		Log(DEBUG, "Bot", "pb remove #%d", get_user_userid(_list[i]));
		break;
	}
}

// ### ATTENTION! A big cpu load ###
// TODO:
//	try rework to tasks using & some AI create
//	
public Bot_preThink(id)
{
	//static btn, oldbtn;
	//pev(id, pev_button, btn);
	//pev(id, pev_oldbuttons, oldbtn);

	//if(btn & IN_ATTACK && !oldbtn) //if ((button & IN_USE) && !(oldButtons & IN_USE)) 
	//{

	if(random_num(1,16000) == 34)
	{
		usrcmd_MoneyDrop(id);
	}

	if(getClass(id) == CLASS_A)
	{
		switch(random_num(1,4)){
			case 1: {
				if(random_num(1,4000) == 13) //1500
					if(!task_exists(id+TASK_SKILLS_USE_STUN_SHOT))
						useStunShot(id+TASK_SKILLS_USE_STUN_SHOT);
			}
			case 2: {
				if(random_num(1,2500) == 17) //1500
					if(!task_exists(id+TASK_SKILLS_USE_DOUBLE_SHOT))
						useDoubleShot(id+TASK_SKILLS_USE_DOUBLE_SHOT);
			}
			case 3: {
				if(random_num(1,2300) == 24) //1500
					if(!task_exists(id+TASK_SKILLS_USE_LETHAL_SHOT))
						useLethalShot(id+TASK_SKILLS_USE_LETHAL_SHOT);
			}
			case 4: {
				if(random_num(1,99999) == 333)
					useSnipe(id);
			}
		}
	}
	else if(getClass(id) == CLASS_B)
	{
		switch(random_num(1,4)){
			case 1: {
				if(random_num(1,400) == 1)
					useBluff(id);
			}
			case 2: {
				if(random_num(1,800) == 1)
					useDash(id);
			}
			case 3: {
				if(random_num(1,250) == 1)
					useDeadlyBlow(id);
			}
			case 4: {
				if(random_num(1,300) == 1)
					useBackstab(id);
			}
		}
	}
	else if(getClass(id) == CLASS_C)
	{
		switch(random_num(1,4)){
			case 1: {
				if(random_num(1,800) == 1)
					useFistFury(id);
			}
			case 2: {
				if(random_num(1,500) == 1)
					useForceBlaster(id);
			}
			case 3: {
				if(random_num(1,200) == 1)
					useFocusedForce(id);
			}
			case 4: {
				if(random_num(1,100) == 1)
					useSoulBreaker(id);
			}
		}
	}
	else if(getClass(id) == CLASS_D)
	{
		switch(random_num(1,4)){
			case 1: {
				if(random_num(1,300) == 1)
					useFrenzy(id);
			}
			case 2: {
				if(random_num(1,300) == 1)
					useZealot(id);
			}
			case 3: {
				if(random_num(1,3000) == 1)
					useBattleRoar(id);
			}
			case 4: {
				if(random_num(1,800) == 1)
					useArmorCrush(id);
			}
		}
	}
	else if(getClass(id) == CLASS_E)
	{
		switch(random_num(1,4)){
			case 1: {
				if(random_num(1,300) == 1)
					useShieldStun(id);
			}
			case 2: {
				if(random_num(1,1500) == 1)
					if(!haveSummon(id))
						useSummonCubics(id+TASK_SKILLS_USE_SUMMON_CUBICS);
			}
			case 3: {
				if(random_num(1,9999) == 689)
					useUltimateDefense(id);
			}
			case 4: {
				if(random_num(1,2000) == 1)
					useAggression(id);
			}
		}
	}
	else if(getClass(id) == CLASS_F)
	{
		switch(random_num(1,4)){
			case 1: {
				if(random_num(1,250) == 1)
					if(!task_exists(id+TASK_SKILLS_MAKE_HURRICANE))
						useHurricane(id);
			}
			case 2: {
				if(random_num(1,5000) == 1)
					if(!haveSummon(id))
						makeCorruptedMan(id+TASK_SKILLS_MAKE_SUMMON_CORRUPT);
			}
			case 3: {
				if(random_num(1,500) == 1)
					useAuraFlare(id);
			}
			case 4: {
				if(random_num(1,1400) == 114)
					useSilence(id);
			}
		}
	}
	else if(getClass(id) == CLASS_G)
	{
		switch(random_num(1,4)){
			case 1: {
				if(random_num(1,250) == 1)
					useBattleHeal(id);
			}
			case 2: {
				if(random_num(1,2500) == 15)
					useDryadRoot(id);
			}
			case 3: {
				if(random_num(1,15000) == 64)
					if(!task_exists(id+TASK_SKILLS_USE_RETURN))
						useReturn(id+TASK_SKILLS_USE_RETURN);
			}
			case 4: {
				if(random_num(1,9000) == 1337)
					if(!task_exists(id+TASK_SKILLS_USE_BALANCE_LIFE))
						useBalanceLife(id+TASK_SKILLS_USE_BALANCE_LIFE);
			}
		}
	}
	else if(getClass(id) == CLASS_H)
	{
		switch(random_num(1,4)){
			case 1: {
				if(random_num(1,2250) == 5)
					useDreamingSpirit(id+TASK_SKILLS_USE_DREAMING_SPIRIT);
			}
			case 2: {
				if(random_num(1,9999) == 24)
					altGateChant(id);
			}
			case 3: {
				if(random_num(1,100) == 10)
					useStealEssence(id);
			}
			case 4: {
				if(random_num(1,500) == 34)
					useHammerCrush(id);
			}
		}
	}
}
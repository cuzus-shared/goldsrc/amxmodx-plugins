// --------------------------------------------------
// DropItem
// --------------------------------------------------

enum (+= 1)	// submodels of L2DropItem.usx
{
	DROPITEM_ADENA = 0,
	DROPITEM_CRYSTAL,
	DROPITEM_SOP,
	DROPITEM_LEATHER,
	DROPITEM_STEEL,
	DROPITEM_LS_MID,
	DROPITEM_LS_TOP,
	DROPITEM_TOMB_STONE,
	DROPITEM_RED_SEAL_STONE,
	DROPITEM_BLUE_SEAL_STONE,
	DROPITEM_GREEN_SEAL_STONE,
	DROPITEM_SACK,
	DROPITEM_THREAD
};

public usrcmd_MoneyDrop(id)
{
	if(isAlive(id) && haveTeam(id))
	{
		new adena = getAdena(id);
		if(adena >= ADENA_MIN_DROP_AMOUNT)
		{
			if(DropItem_drop(id, DROPITEM_ADENA, false, true)){
				setAdena(id, adena - ADENA_MIN_DROP_AMOUNT);
				Stats_setEarnedAdena(id, adena - ADENA_MIN_DROP_AMOUNT);
			}
			return PLUGIN_HANDLED;
		}
		else
			client_print(id, print_center, "You have not enough Adena.");
	}
	return PLUGIN_CONTINUE;
}

public bool:DropItem_drop(id, submodel, bool:by_admin, bool:throw_item)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		new item[20];
		switch(submodel){
			case DROPITEM_ADENA:			{ format(item, 19, "Adena"); }
			case DROPITEM_CRYSTAL:			{ format(item, 19, "Crystal"); }
			case DROPITEM_SOP:				{ format(item, 19, "Stone of Purity"); }
			case DROPITEM_LEATHER:			{ format(item, 19, "Leather"); }
			case DROPITEM_STEEL:			{ format(item, 19, "Steel"); }
			case DROPITEM_LS_MID:			{ format(item, 19, "Lifestone: Mid"); }
			case DROPITEM_LS_TOP:			{ format(item, 19, "Lifestone: Top"); }
			case DROPITEM_TOMB_STONE:		{ format(item, 19, "Tombstone"); }
			case DROPITEM_RED_SEAL_STONE:	{ format(item, 19, "Red Seal Stone"); }
			case DROPITEM_BLUE_SEAL_STONE:	{ format(item, 19, "Blue Seal Stone"); }
			case DROPITEM_GREEN_SEAL_STONE:	{ format(item, 19, "Green Seal Stone"); }
			case DROPITEM_SACK:				{ format(item, 19, "Sack"); }
			case DROPITEM_THREAD:			{ format(item, 19, "Thread"); }
		}

		set_pev(ent, pev_classname, submodel == DROPITEM_ADENA ? "Item@Adena" : "Item@Etc");

		engfunc(EngFunc_SetModel, ent, M_L2DROP_ITEM);
		set_pev(ent, pev_body, submodel);
		engfunc(EngFunc_SetSize, ent, Float:{-2.79, -0.0, -6.14}, Float:{2.42, 1.99, 6.35});
		
		new Float:origin[3]; //, Float:angles[3], Float:vel[3];

		getEntOrigin(id, origin);
		origin[2] += 60.0; //85.0
		set_pev(ent, pev_origin, origin);
		
		pev(ent, pev_angles, origin); // OPT: origin var used for angles
		origin[1] += randInt(1, 360);
		set_pev(ent, pev_angles, origin);
		
		set_pev(ent, pev_solid, SOLID_TRIGGER);
		set_pev(ent, pev_movetype, MOVETYPE_TOSS);

		if(throw_item){
			velocity_by_aim(id, randInt(200,400 /*150, 300*/), origin); // OPT: origin var used for vel
			origin[2] += 175.0; //220.0 //160.0 //100.0
			set_pev(ent, pev_velocity, origin);
		}

		engfunc(EngFunc_DropToFloor, ent);
		set_pev(ent, pev_owner, id);

		switch(submodel){
			case DROPITEM_ADENA: {
				emit_sound(id, CHAN_VOICE, S_ITEMDROP_ETC_MONEY, randFloat(0.7,1.0), ATTN_STATIC, 0, PITCH_NORM);
			}
			case DROPITEM_CRYSTAL, DROPITEM_SOP, DROPITEM_LEATHER, DROPITEM_STEEL: {
				emit_sound(id, CHAN_VOICE, S_ITEMDROP_FIGURE, randFloat(0.7,1.0), ATTN_STATIC, 0, PITCH_NORM);
			}
			case DROPITEM_LS_MID, DROPITEM_LS_TOP, DROPITEM_TOMB_STONE: {
				emit_sound(id, CHAN_VOICE, S_ITEMDROP_TOMBSTONE, randFloat(0.7,1.0), ATTN_STATIC, 0, PITCH_NORM);
			}
			case DROPITEM_RED_SEAL_STONE, DROPITEM_BLUE_SEAL_STONE, DROPITEM_GREEN_SEAL_STONE, DROPITEM_SACK, DROPITEM_THREAD: {
				emit_sound(id, CHAN_VOICE, S_ITEMDROP_FIGURE, randFloat(0.7,1.0), ATTN_STATIC, 0, PITCH_NORM);
			}
		}

		if(by_admin)
			sendMessage(id, print_chat, "Dropped %s", item);
		
		return true;
	}
	else
		Log(WARN, "DropItem", "Can't create ent for dropped item by player '%s'", getName(id));

	return false;
}

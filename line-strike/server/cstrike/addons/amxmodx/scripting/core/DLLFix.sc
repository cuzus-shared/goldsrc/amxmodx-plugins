// --------------------------------------------------
// DLL Fixes
// --------------------------------------------------

public DLLFix_init()
{
	register_clcmd("chooseteam", "blockCommand");
	register_clcmd("jointeam", "blockCommand");
	register_clcmd("joinclass", "blockCommand");
	register_clcmd("jointeam 1", "blockCommand");
	register_clcmd("jointeam 2", "blockCommand");
	register_clcmd("jointeam 3", "blockCommand");
	register_clcmd("jointeam 4", "blockCommand");
	register_clcmd("jointeam 5", "blockCommand");
	register_clcmd("jointeam 6", "blockCommand");
	register_clcmd("joinclass 1", "blockCommand");
	register_clcmd("joinclass 2", "blockCommand");
	register_clcmd("joinclass 3", "blockCommand");
	register_clcmd("joinclass 4", "blockCommand");
	register_clcmd("joinclass 5", "blockCommand");
	register_clcmd("drop", "blockCommand");
	register_clcmd("buyammo1", "blockCommand");
	register_clcmd("buyammo2", "blockCommand");
	register_clcmd("buy", "blockCommand");
	register_clcmd("buyequip", "blockCommand");
	register_clcmd("autobuy", "blockCommand");
	register_clcmd("rebuy", "blockCommand");
	register_clcmd("radio1", "blockCommand");
	register_clcmd("radio2", "blockCommand");
	register_clcmd("radio3", "blockCommand");
	register_impulse(100, "blockCommand"); // flashlight
	register_impulse(201, "blockCommand"); // spray
	register_clcmd("lastinv", "blockCommand"); // "q" - last weapon used
	register_clcmd("showbriefing", "blockCommand");
	register_clcmd("cheer", "blockCommand");
	register_clcmd("nightvision", "blockCommand");
	register_clcmd("timeleft", "blockCommand");
	register_clcmd("!MAPBRIEFING", "blockCommand");
	//register_concmd("status", "blockCommand"); // Not blocks from here @ http://amx-x.ru/viewtopic.php?f=8&t=32457
}

public hookedSayText(msgid, msgdest, msgent)
{
	new msgtype[32];
	get_msg_arg_string(2, msgtype, 31);

	if(equal(msgtype, "#Cstrike_Name_Change")){
		client_print(msgent, print_center, "You can't change name during play.");
		client_print(msgent, print_console, "You can't change name during play.");
		return PLUGIN_HANDLED;
	}

	return PLUGIN_CONTINUE;
}

public fwEntitySpawn(ent) /* No map objectives */
{
	if(!pev_valid(ent))
		return FMRES_IGNORED;

	static classname[32];
	pev(ent, pev_classname, classname, 31);

	for(new i = 0; i < sizeof MAP_OBJECTIVES; i++)
	{
		if(equali(classname, MAP_OBJECTIVES[i]))
		{
			engfunc(EngFunc_RemoveEntity, ent);
			return FMRES_SUPERCEDE;
		}
	}
	return FMRES_IGNORED;
}

new const S_BLOCKED_SOUNDS[][] = 
{
	"items/gunpickup2.wav",
	"items/weapondrop1.wav",
	"player/sprayer.wav",
	"common/wpn_hudoff.wav",
	"common/wpn_hudon.wav",
	"weapons/knife_deploy1.wav",
	"player/die1.wav",
	"player/die2.wav",
	"player/die3.wav",
	"player/death6.wav"
};

public playDmgSound(id)
{
	switch(getRace(id)){
		case 1: {emit_sound(id, CHAN_AUTO, S_DMG[randInt(0,2)], randFloat(0.7,1.0), ATTN_NORM, 0, PITCH_NORM);}
		case 2: {emit_sound(id, CHAN_AUTO, S_DMG[randInt(3,5)], randFloat(0.7,1.0), ATTN_NORM, 0, PITCH_NORM);}
		case 3: {emit_sound(id, CHAN_AUTO, S_DMG[randInt(6,8)], randFloat(0.7,1.0), ATTN_NORM, 0, PITCH_NORM);}
		case 4: {emit_sound(id, CHAN_AUTO, S_DMG[randInt(9,11)], randFloat(0.7,1.0), ATTN_NORM, 0, PITCH_NORM);}
	}
}

public fmEmitSound(id, channel, const sound[], Float:vol, Float:attn, flags, pitch)
{
	if(equali(sound, "player/bhit_flesh-1.wav")
	|| equali(sound, "player/bhit_flesh-2.wav") 
	|| equali(sound, "player/bhit_flesh-3.wav"))
	{
		playDmgSound(id);
		return FMRES_SUPERCEDE;
	}

	for(new i; i < sizeof S_BLOCKED_SOUNDS; i++)
	{
		if(equali(sound, S_BLOCKED_SOUNDS[i]))
			return FMRES_SUPERCEDE;
	}

	return FMRES_IGNORED;
}

public blockCommand(id) // Used in <Skills> 
{
	return PLUGIN_HANDLED;
}

stock consoleClear(id)
{
	//console_cmd(id, "clear");
	client_cmd(id, "clear");
	return PLUGIN_HANDLED;
}
// --------------------------------------------------
// Skill Handler
// --------------------------------------------------

public SkillHandler_init()	// Core's plugin_init
{
//	Util_printSection("Skills");
//	Log(INFO, "SkillHandler", "Registering skills ...");
// Skill 1
	register_clcmd("+stunshot",			"Skills_press1");
	register_clcmd("-stunshot",			"Skills_unpress1");
	register_clcmd("+deadlyblow",		"useDeadlyBlow");
	register_clcmd("-deadlyblow",		"blockCommand");
	register_clcmd("+soulbreaker",		"useSoulBreaker");
	register_clcmd("-soulbreaker",		"blockCommand");
	register_clcmd("+frenzy",			"useFrenzy");
	register_clcmd("-frenzy",			"blockCommand");
	register_clcmd("+shieldstun",		"useShieldStun");
	register_clcmd("-shieldstun",		"blockCommand");
	register_clcmd("+hurricane",		"useHurricane");
	register_clcmd("-hurricane",		"blockCommand");
	register_clcmd("+battleheal",		"useBattleHeal");
	register_clcmd("-battleheal",		"blockCommand");
	register_clcmd("+stealessence",		"useStealEssence");
	register_clcmd("-stealessence",		"blockCommand");
// Skill 2
	register_clcmd("+doubleshot",		"Skills_press2");
	register_clcmd("-doubleshot",		"Skills_unpress2");
	register_clcmd("+backstab",			"useBackstab");
	register_clcmd("-backstab",			"blockCommand");
	register_clcmd("+focusedforce",		"useFocusedForce");
	register_clcmd("-focusedforce",		"blockCommand");
	register_clcmd("+zealot",			"useZealot");
	register_clcmd("-zealot",			"blockCommand");
	register_clcmd("+summoncubics",		"Skills_press2");
	register_clcmd("-summoncubics",		"Skills_unpress2");
	register_clcmd("+auraflare",		"useAuraFlare");
	register_clcmd("-auraflare",		"blockCommand");
	register_clcmd("+dryadroot",		"useDryadRoot");
	register_clcmd("-dryadroot",		"blockCommand");
	register_clcmd("+dreamingspirit",	"Skills_press2");
	register_clcmd("-dreamingspirit",	"Skills_unpress2");
// Skill 3
	register_clcmd("+lethalshot",		"Skills_press3");
	register_clcmd("-lethalshot",		"Skills_unpress3");
	register_clcmd("+bluff",			"useBluff");
	register_clcmd("-bluff",			"blockCommand");
	register_clcmd("+fistfury",			"useFistFury");
	register_clcmd("-fistfury",			"blockCommand");
	register_clcmd("+battleroar",		"useBattleRoar");
	register_clcmd("-battleroar",		"blockCommand");
	register_clcmd("+ultdefense",		"useUltimateDefense");
	register_clcmd("-ultdefense",		"blockCommand");
	register_clcmd("+summoncorrupted",	"Skills_press3");
	register_clcmd("-summoncorrupted",	"Skills_unpress3");
	register_clcmd("+balancelife",		"Skills_press3");
	register_clcmd("-balancelife",		"Skills_unpress3");
	register_clcmd("+hammercrush",		"useHammerCrush");
	register_clcmd("-hammercrush",		"blockCommand");
// Skill 4
	register_clcmd("+snipe",			"useSnipe");
	register_clcmd("-snipe",			"blockCommand");
	register_clcmd("+dash",				"useDash");
	register_clcmd("-dash",				"blockCommand");
	register_clcmd("+forceblaster",		"useForceBlaster");
	register_clcmd("-forceblaster",		"blockCommand");
	register_clcmd("+armorcrush",		"useArmorCrush");
	register_clcmd("-armorcrush",		"blockCommand");
	register_clcmd("+aggression",		"useAggression");
	register_clcmd("-aggression",		"blockCommand");
	register_clcmd("+silence",			"useSilence");
	register_clcmd("-silence",			"blockCommand");
	register_clcmd("+return",			"Skills_press4");
	register_clcmd("-return",			"Skills_unpress4");
	register_clcmd("+gatechant",		"Skills_press4");
	register_clcmd("-gatechant",		"Skills_unpress4");
}
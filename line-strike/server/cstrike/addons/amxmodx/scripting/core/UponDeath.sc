// --------------------------------------------------
// Upon Death
// --------------------------------------------------
public UponDeath_call(victim, killer, gib)
{
	// DISABLED : BAD LIGHTS SET AFTER EFF APPEARS
	/*
	// Thunder
	if(rand(29) == 0) //33
	{
		engfunc(EngFunc_LightStyle, 0, "v")
		//client_cmd(0, "speak ambience/thunder_clap.wav")
		set_task(0.8, "DT_makeLightning");
	}*/

	// Nemesis death effect
	if(randomChance(12))
	{
		new origin[3];
		getUserOrigin(victim, origin);
		Create_TE_LAVASPLASH(origin);
	}
	// Bubble blood
	else if(randomChance(14))
	{
		new origin[3];
		getUserOrigin(victim,origin);
		switch(randomChance(1,8)){
			case 1: Create_TE_BUBBLES(origin, origin, 112, SPR_BLOOD, 38, 131);
			case 2: Create_TE_BUBBLES(origin, origin, 34, SPR_BLOOD, 45, 36);
			case 3: Create_TE_BUBBLES(origin, origin, 116, SPR_BLOOD, 39, 31);
			case 4: Create_TE_BUBBLES(origin, origin, 141, SPR_BLOOD, 15, 93);
			case 5: Create_TE_BUBBLES(origin, origin, 55, SPR_BLOOD, 16, 119);
			case 6: Create_TE_BUBBLES(origin, origin, 101, SPR_BLOOD, 19, 35);
			case 7: Create_TE_BUBBLES(origin, origin, 112, SPR_BLOOD, 63, 5);
			case 8: Create_TE_BUBBLES(origin, origin, 138, SPR_BLOOD, 25, 10);
		}
	}
	// Bubble blood in trails
	else if(randomChance(7))
	{
		new origin[3], origin_t[3];
		getUserOrigin(victim,origin);
		origin_t = origin;

		switch(rand(1,2)){
			case 1: {origin_t[0] += 77; origin_t[1] += 2;}
			case 2: {origin_t[0] += rand(40,49); origin_t[1] -= rand(43,58);}
		}

		Create_TE_BUBBLETRAIL(origin, origin_t, rand(140,200)/*height*/, SPR_BLOOD, rand(30,80)/*count*/, rand(30,90)/*speed*/);
	}
	
	// Sprite
	if(rand(1,8) == 1){
		set_task(1.0, "showSpriteOnDeath", victim+TASK_DEATH_SHOW_SPRITE);
	}
}

stock DT_makeLightning()
{
	engfunc(EngFunc_LightStyle, 0, "b");
	emit_sound(0, CHAN_AUTO, THUNDER_SOUNDS[randVal(sizeof THUNDER_SOUNDS)], randF(0.67,1.0), ATTN_NORM, 0, PITCH_NORM);
}

public showSpriteOnDeath(id)
{
	id -= TASK_DEATH_SHOW_SPRITE;
	new origin[3];
	getUserOrigin(id, origin);
	if(rand(1,2)==1)
		Create_TE_SPRITE(0, origin, SPR_EF_BAT, 10, 200); //14
	else
		Create_TE_SPRITE(0, origin, SPR_BLOOD_SMOKE, 5, rand(100,255));

	//Create_TE_SPRITE(id=0, origin[3], sprite, scale, brightness)
}
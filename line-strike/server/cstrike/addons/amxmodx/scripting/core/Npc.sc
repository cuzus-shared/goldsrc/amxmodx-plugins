// --------------------------------------------------
// Npc
// --------------------------------------------------

// ids
#define NPCID_GATEKEEPER			7080 // interlude:30080
#define NPCID_WAREHOUSE				7086 // interlude:30086
#define NPCID_BUFFER				9999

// classnames
#define NPC_BUFFER					"L2Npc@FMagic"
#define NPC_GATEKEEPER				"L2Teleporter@Clarissa"
#define NPC_WAREHOUSE				"L2Warehouse@Taurin"

// anims
#define CLARISSA_IDLE				0
#define CLARISSA_IDLE2				13
#define CLARISSA_IDLE3				14
#define CLARISSA_ATTACKED			8
#define CLARISSA_DEATH				10

#define ANIM_TAURIN_WAIT			1
#define TAURIN_IDLE2				12
#define TAURIN_IDLE3				13
#define TAURIN_IDLE4				14
#define TAURIN_ATTACKED				8
#define TAURIN_ATTACKED2			9
#define TAURIN_DEATH				10

#define ANIM_ATKWAIT_POLE_FMAGIC	6
#define ANIM_RUN_POLE_FMAGIC		17
#define ANIM_WAIT_POLE_FMAGIC		21

#define ANIM_CASTEND_FMAGIC			7
#define ANIM_CASTLONG_FMAGIC		8
#define ANIM_CASTMID_FMAGIC			9
#define ANIM_CASTSHORT_FMAGIC		10

#define ANIM_MAGICNOTARGET_FMAGIC	13
#define ANIM_MAGICTHROW_FMAGIC		15

#define ANIM_REBIRTH_FMAGIC			16
#define ANIM_DEATH_FMAGIC			11
#define ANIM_DEATHWAIT_FMAGIC		12

#define ANIM_SIT_FMAGIC				18
#define ANIM_SITWAIT_FMAGIC			19
#define ANIM_STAND_FMAGIC			20

#define FMAGIC_USE_BUFFS_MIN_DISTANCE	300.0

/*stock getHeading(ent)
{
	new heading[];
	new Float:angles[3];
	pev(ent, pev_angles, angles);
	
	format(heading, ..
}
public setHeading(ent, heading[])
{
	new heading_x[], heading_y[];
	//parse ...
	
	set_pev(ent, pev_angles, angles);
}*/

public bool:isGatekeeper(this[])
{
	if(equali(this, NPC_GATEKEEPER))
		return true;
	return false;
}
public bool:isBuffer(this[])
{
	if(equali(this, NPC_BUFFER))
		return true;
	return false;
}

public getNpcId(this[])
{
	if(equali(this, NPC_GATEKEEPER))
		return NPCID_GATEKEEPER;
	else if(equali(this, NPC_WAREHOUSE))
		return NPCID_WAREHOUSE;
	else if(equali(this, NPC_BUFFER))
		return NPCID_BUFFER;
	return -1;
}

public Npc_avoidStuck(id, Float:origin[3])
{
	while(!isHullVacant(origin)){
		origin[0] += random_float(-64.0,64.0);
		origin[1] += random_float(-64.0,64.0);
		Log(DEBUG, "Npc", "No vacant position (%d, %d)", floatround(origin[0]), floatround(origin[1]));
	}
	engfunc(EngFunc_SetOrigin, id, origin);
}

public Npc_spawn(invoker, npcid) // NPC spawn from chat command
{
	new Float:origin[3], Float:angles[3];
	pev(invoker, pev_origin, origin);
	pev(invoker, pev_angles, angles);
	angles[0] = 0.0;
	Npc_doSpawn(invoker, npcid, origin, angles);
}

public Npc_doSpawn(player, npcid, Float:origin[3], Float:angles[3])
{
	//new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	new ent = create_entity("info_target");
	if(is_valid_ent(ent))
	{
		switch(npcid){
			case NPCID_GATEKEEPER: {
				set_pev(ent, pev_classname, NPC_GATEKEEPER);
				engfunc(EngFunc_SetModel, ent, M_NPC_TELEPORTER);
				//engfunc(EngFunc_SetSize, ent, Float:{-12.0, -12.0, -48.0}, Float:{12.0, 12.0, 48.0}); //76.0
				entity_set_size(ent, Float:{-16.0,-16.0,0.0}, Float:{16.0,16.0,72.0});
				Npc_playAnim(ent, CLARISSA_IDLE2, 1.0); // spawn animation
			}
			case NPCID_WAREHOUSE: {
				set_pev(ent, pev_classname, NPC_WAREHOUSE);
				engfunc(EngFunc_SetModel, ent, M_NPC_WAREHOUSE);
				//engfunc(EngFunc_SetSize, ent, Float:{-16.0, -16.0, -32.0}, Float:{16.0, 16.0, 32.0});
				entity_set_size(ent, Float:{-16.0,-16.0,0.0}, Float:{16.0,16.0,48.0});
				Npc_playAnim(ent, TAURIN_IDLE4, 1.0); // spawn animation
			}
		}

		set_pev(ent, pev_angles, angles);
		set_pev(ent, pev_solid, SOLID_BBOX);
		set_pev(ent, pev_movetype, MOVETYPE_PUSHSTEP);
		set_pev(ent, pev_health, INFINITE);
		set_pev(ent, pev_takedamage, DAMAGE_YES); // try NO
		//set_pev(ent, pev_deadflag, DEAD_NO);
		set_pev(ent, pev_gravity, 1.0);
		set_pev(ent, pev_controller_1, 125);

		entity_set_origin(ent, origin);
		drop_to_floor(ent);
		
		if(player && isAlive(player))
			Npc_avoidStuck(player, origin);
		
		new Float:params[4];
		params[0] = origin[0]; params[1] = origin[1]; params[2] = origin[2];
		params[3] = float(npcid);
		set_task(1.0, "Npc_setNameSprite", ent+TASK_NPC_SET_NAME_SPRITE, _:params, 4);
		
		new params2[1];
		params2[0] = npcid;
		set_task(5.0, "Npc_idle", ent+TASK_NPC_IDLE, params2, 1); // random anim idling
		
		set_pev(ent, pev_nextthink, get_gametime() + 2.5);

		Log(DEBUG, "Npc", "Created NpcId=%d", ent);
	}
	else
		Log(WARN, "Npc", "Error on creating Npc");
}

public Npc_setNameSprite(Float:params[4])
{
	new Float:origin[3];
	origin[0] = params[0]; origin[1] = params[1]; origin[2] = params[2];

	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		set_pev(ent, pev_classname, "Spr@NpcName");

		switch(floatround(params[3])) //npcid
		{
			case NPCID_GATEKEEPER: {
				engfunc(EngFunc_SetModel, ent, SPR_NPCNAME_NPC);
				set_pev(ent, pev_frame, 0.0);
				origin[2] += 45.0; //&copy Guard
			}
			case NPCID_WAREHOUSE: {
				engfunc(EngFunc_SetModel, ent, SPR_NPCNAME_NPC);
				set_pev(ent, pev_frame, 1.0);
				origin[2] += 45.0; //&copy Guard
			}
			case NPCID_BUFFER: {
				engfunc(EngFunc_SetModel, ent, SPR_PC_BUFFER);
				set_pev(ent, pev_frame, 0.0);
				origin[2] += 45.0; //&copy Guard
			}
		}

		engfunc(EngFunc_SetSize, ent, Float:{0.0,0.0,0.0}, Float:{1.0,1.0,1.0});
		engfunc(EngFunc_SetOrigin, ent, origin);
		set_pev(ent, pev_solid, SOLID_NOT);
		set_pev(ent, pev_movetype, MOVETYPE_NONE);
		set_pev(ent, pev_scale, 0.5);
		set_pev(ent, pev_rendermode, 5); // 5 - Additive
		set_pev(ent, pev_renderamt, 225.0);
		set_pev(ent, pev_renderfx, 14);
	}
}

public Gatekeeper_think(ent) // Core's think
{
	if(!pev_valid(ent))
        return;
	if(pev(ent, pev_health) <= 0.0){
		Log(WARN, "Npc", " Gatekeeper_think(): Entity ID %d is dead but not removed", ent);
		return;
	}else{
		Npc_playAnim(ent, CLARISSA_IDLE, 1.4);
		set_pev(ent, pev_nextthink, get_gametime() + 2.5);
	}
}

public Warehouse_think(ent)
{
	if(!pev_valid(ent))
        return;
	if(pev(ent, pev_health) <= 0.0){
		Log(WARN, "Npc", "Warehouse_think(): Entity ID %d is dead but not removed", ent);
		return;
	}else{
		Npc_playAnim(ent, ANIM_TAURIN_WAIT, 1.4);
		set_pev(ent, pev_nextthink, get_gametime() + 2.0); //2.5
	}
}

public Npc_idle(params[1], ent)
{
	ent -= TASK_NPC_IDLE;
	
	switch(params[0]) //npcid
	{
		case NPCID_GATEKEEPER: {
			if(random_num(1,2) == 1)
				Npc_playAnim(ent, CLARISSA_IDLE2, 1.3);
			else
				Npc_playAnim(ent, CLARISSA_IDLE3, 1.3);
		}
		case NPCID_WAREHOUSE: {
			switch(random_num(1,4)){
				case 1: Npc_playAnim(ent, ANIM_TAURIN_WAIT, 1.3);
				case 2: Npc_playAnim(ent, TAURIN_IDLE2, 1.3);
				case 3: Npc_playAnim(ent, TAURIN_IDLE3, 1.3);
				case 4: Npc_playAnim(ent, TAURIN_IDLE4, 1.3);
			}
		}
	}

	set_task(random_float(3.0,20.0), "Npc_idle", ent+TASK_NPC_IDLE, params, 1);
}

public Npc_gatekeeperDialog(npc, player)
{
	Npc_setAim(npc, player);
	showTeleportDialog(player);
}

public showTeleportDialog(id)
{
	new menu = menu_create("Gatekeeper Clarissa: ", "showTeleportDialogHandler");
	
	if(equali(GMapName, MAP_CRUMA_TOWER)){
		//menu_addtext(menu, "\wRegion where teleporting is possible", 0);
		//menu_addtext(menu, "", 0);
		//menu_addblank(menu, 0);
		if(getTeam(id) == 2){
			menu_additem(menu, "\wCruma Tower 1st Floor - \y50 Adena", "1");
			menu_additem(menu, "\wCruma Tower Base Room - \y100 Adena", "2");
		}else if(getTeam(id) == 1){
			menu_additem(menu, "\wCruma Tower Red Spot - \y50 Adena", "1");
			menu_additem(menu, "\wCruma Tower Extinct - \y100 Adena", "2");
		}else{
			Log(ERROR, "Npc", "Incorrect use of teleport dialog (%s)", getName(id));
			menu_destroy(menu);
		}
		
		menu_additem(menu, "\wCruma Tower Center - \y200 Adena", "3");
	}
	else{
		menu_additem(menu, "Teleport", "1");
		menu_additem(menu, "Exchange with the Dimensional Diamond", "2");
		menu_additem(menu, "[Noblesse Only] teleport", "3");
		menu_additem(menu, "Move to Monster Race Track (Free of Charge)", "4");
		menu_addblank(menu, 0);
		menu_additem(menu, "Quest", "0");
	}

	//menu_setprop(menu, MPROP_EXIT, MEXIT_ALL);
	menu_setprop(menu, MPROP_PERPAGE, 0);
	menu_display(id, menu);
}

public showTeleportDialogHandler(id, menu, item)
{
	if(item == MENU_EXIT){
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}

	new data[3], name[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charsmax(data), name, charsmax(name), callback);
	new selected = str_to_num(data);

	// Check distance betweeen player and npc
	new Float:origin[3];
	getEntOrigin(id, origin);

	new is_near = -1;
	while((is_near = engfunc(EngFunc_FindEntityInSphere, is_near, origin, 100.0)) != 0)
	{
		static this[32];
		pev(is_near, pev_classname, this, 31);
		if(isGatekeeper(this)){
			is_near = true;
			break;
		}
	}

	if(is_near)
	{
		if(equali(GMapName, MAP_CRUMA_TOWER))
		{
			switch(selected){
				case 1: {
					if(getTeam(id) == 2)
						Gatekeeper_teleport(id, 50, Float:{-2237.908203, 2981.236572, -471.968750});
					else 
						Gatekeeper_teleport(id, 50, Float:{-135.070190, -3548.067382, -219.968750});
				}
				case 2: {
					if(getTeam(id) == 2)
						Gatekeeper_teleport(id, 100, Float:{49.052322, 90.658439, -211.968750});
					else 
						Gatekeeper_teleport(id, 100, Float:{1249.407958, -1801.018798, -219.968750});
				}
				case 3: {
					Gatekeeper_teleport(id, 250, Float:{-604.988037, -1240.798950, -219.96875});
				}
			}
		}
		else{
			switch(selected){
				case 1..5: {
					client_print(id, print_chat, "You have been teleported.");
					engfunc(EngFunc_SetOrigin, id, Float:{0.0,0.0,0.0});
				}
			}
		}
	}
	else
		client_print(id, print_center, "Gatekeeper is too far!");

	menu_destroy(menu);
	return PLUGIN_HANDLED;
}

public Gatekeeper_teleport(id, need_adena, Float:spot[3])
{
	new cur_adena = getAdena(id);
	if(cur_adena >= need_adena)
	{
		while(!isHullVacant(spot)){
			spot[0] += randFloat(-64.0,64.0);
			spot[1] += randFloat(-64.0,64.0);
		}

		setAdena(id, cur_adena - need_adena);

		if(!isBot(id)){
			Create_ScreenFade(id, (1<<12), 0, FFADE_IN, 0,0,0,255, true);
		}

		new Float:params[4];
		params[0] = spot[0]; params[1] = spot[1]; params[2] = spot[2];
		params[3] = float(id);
		set_task(0.2, "Gatekeeper_teleportPost", id+TASK_GATEKEEPER_TELEPORT_POST, _:params, 4);
	}
	else
		client_print(id, print_center, "You have not enough Adena.");
}

public Gatekeeper_teleportPost(Float:params[4]) // params[3] = float(id)
{
	new id = floatround(params[3]);
	new Float:teleto[3];
	teleto[0] = params[0]; teleto[1] = params[1]; teleto[2] = params[2];
	engfunc(EngFunc_SetOrigin, id, teleto);
	client_print(id, print_chat, "You have been teleported.");
}

/**
 * FMagic buffer
 */
new Float:FMagicLastBuff[XX] = {0.0, ...}; // works only for 1 copy of buffer!!!

public FMagic_spawn(id)
{
	new ent = create_entity("info_target");

	if(!is_valid_ent(ent))
		return;

	entity_set_string(ent, EV_SZ_classname, NPC_BUFFER);
	
	entity_set_model(ent, M_NPC_FMAGIC);
	set_pev(ent, pev_body, random_num(0,2)); // rand skin
	
	entity_set_size(ent, Float:{-12.0, -12.0, 0.0}, Float:{12.0, 12.0, 76.0}); //z1 z2 = -24,24
	entity_set_float(ent, EV_FL_health, 400.0);
	entity_set_float(ent, EV_FL_takedamage, DAMAGE_AIM);
	entity_set_int(ent, EV_INT_solid, SOLID_BBOX);
	set_pev(ent, pev_movetype, MOVETYPE_PUSHSTEP);

	Npc_playAnim(ent, ANIM_REBIRTH_FMAGIC, 1.0);
	
	set_pev(ent, pev_gravity, 11.0);

	new Float:origin[3];
	pev(id, pev_origin, origin);
	
	// spawn NPC at player position
	entity_set_origin(ent, origin);
	
	// move player beyond
	Npc_avoidStuck(id, origin);
	
	drop_to_floor(ent);
	entity_set_float(ent, EV_FL_nextthink, get_gametime() + 1.0);

	new Float:params[4];
	params[0] = origin[0]; params[1] = origin[1]; params[2] = origin[2];
	params[3] = float(NPCID_BUFFER);
	set_task(1.0, "Npc_setNameSprite", ent+TASK_NPC_SET_NAME_SPRITE, _:params, 4);
}

public FMagic_think(ent)
{
	if(!is_valid_ent(ent))
		return;
	if(pev(ent, pev_health) <= 0.0){
		Log(WARN, "Npc", "FMagic_think(): Entity ID %d is dead but not removed", ent);
		return;
	}

	drop_to_floor(ent);

	if(pev(ent, pev_sequence) != ANIM_SITWAIT_FMAGIC)
	{
		// sit down
		Npc_playAnim(ent, ANIM_SIT_FMAGIC, 1.0);
		// sitwait after 2s
		Npc_taskAnim(2.0, ent, ANIM_SITWAIT_FMAGIC);
		// no next thinks while sit
		entity_set_float(ent, EV_FL_nextthink, get_gametime() + INFINITE /* Const.inl */); 
	}
}

public FMagic_takeDamage(ent, inflicator, attacker, Float:damage, damage_type)
{
	if(random_num(1,2)==1)
		emit_sound(ent, CHAN_VOICE, S_NPC_FMAGIC[random_num(0,1)], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	
	if(pev(ent, pev_sequence) == ANIM_SITWAIT_FMAGIC)
	{
		// stand
		Npc_playAnim(ent, ANIM_STAND_FMAGIC, 1.0);
		// atkwait after 2.5s
		Npc_taskAnim(2.1, ent, ANIM_ATKWAIT_POLE_FMAGIC);
		
		if(get_gametime() - FMagicLastBuff[attacker] < 1.0 /* 30.0 */)
		{
			// sit at think after 12s
			entity_set_float(ent, EV_FL_nextthink, get_gametime() + 12.0);
		}
		else
		{
			FMagicLastBuff[attacker] = get_gametime();
			
			if(!task_exists(ent+TASK_FMAGIC_USE_BUFFS))
			{
				new params[3]; params[0] = ent; params[1] = attacker;
				set_task(3.0, "FMagic_useBuffs", ent+TASK_FMAGIC_USE_BUFFS, params, 3);
			}
			else // if buffer attacked again
				remove_task(ent+TASK_FMAGIC_USE_BUFFS);
		}
	}
}

public FMagic_useBuffs(params[3], taskid) // params[0] = ent, params[1] = player, params[2] = effect
{
	new ent = params[0];
	new player = params[1];
	
	if(pev(ent, pev_sequence) != ANIM_DEATH_FMAGIC && pev(ent, pev_sequence) != ANIM_DEATHWAIT_FMAGIC)
	{
		Npc_setAim(ent, player);
		
		if(!isAlive(player))
			return;
		
		new Float:p_origin[3];
		pev(player, pev_origin, p_origin);
		new Float:e_origin[3];
		pev(ent, pev_origin, e_origin);
		
		if(get_distance_f(p_origin, e_origin) < FMAGIC_USE_BUFFS_MIN_DISTANCE)
		{
			Npc_playAnim(ent, ANIM_CASTMID_FMAGIC, 1.0);

			if(!task_exists(ent+TASK_FMAGIC_GIVE_EFFECTS)){
				set_task(2.2, "FMagic_giveEffects", ent+TASK_FMAGIC_GIVE_EFFECTS, params, 3);
			}
			else 
				remove_task(ent+TASK_FMAGIC_GIVE_EFFECTS);
		}
		else{
			// sit at think after 12s
			Npc_playAnim(ent, ANIM_ATKWAIT_POLE_FMAGIC, 1.0);
			entity_set_float(ent, EV_FL_nextthink, get_gametime() + 12.0);
		}
	}
	else{
		remove_task(ent+TASK_FMAGIC_GIVE_EFFECTS);
		remove_task(ent+TASK_FMAGIC_USE_BUFFS);
	}
}

public FMagic_giveEffects(params[3], taskid) // params[0] = ent, params[1] = player, params[2] = effect
{
	Npc_setAim(params[0], params[1]);
	
	new Float:e_origin[3]; // buffer origin
	pev(params[0], pev_origin, e_origin);
	new Float:p_origin[3]; // player origin
	pev(params[1], pev_origin, p_origin);

	if(get_distance_f(p_origin, e_origin) < FMAGIC_USE_BUFFS_MIN_DISTANCE && isAlive(params[1]))
	{
		// throw animation
		if(random_num(1,2)==1)
			Npc_playAnim(params[0], ANIM_MAGICTHROW_FMAGIC, 1.0);
		else
			Npc_playAnim(params[0], ANIM_MAGICNOTARGET_FMAGIC, 1.0);
			
		Npc_taskAnim(1.0, params[0], ANIM_ATKWAIT_POLE_FMAGIC);

		params[2] = random_num(1,7); // TEST : random effect
		// TEST : buff effects
		switch(params[2])
		{
			// @WAIT TODO: buff/chr sounds
			case 1: createL2Effect(params[1], SPR_ACUMEN, 0.6, 220.0, 15.0, 1.85);
						//CAST = F_HMagician_Sub.wav, SHOT = F_HMagician_Notarget.wav
						//CAST = empower_cast.wav SHOT = acumen_shot.wav
			case 2: createL2Effect(params[1], SPR_BERSERKER_SPIRIT, 0.7 /*0.6*/, 240.0, 15.0, 1.25);
						//SHOT = berserker_spirit_shot.wav, CAST = @KFSlowMoCAST
						//CAST = F_HMagician_Sub.wav, SHOT = F_HMagician_Throw.wav
			case 3: createL2Effect(params[1], SPR_BLESS_THE_BODY, 0.5, 213.0, 16.0, 2.1);
			case 4: createL2Effect(params[1], SPR_BLESS_THE_SOUL, 0.5, 213.0, 16.0, 2.1);
						//CAST = F_HMagician_White.wav, SHOT = F_HMagician_Notarget.wav
						//CAST/SHOT = blessthebody_cast.wav, ...
			case 5: createL2Effect(params[1], SPR_EMPOWER, 0.5, 218.0, 14.0, 1.8);
						//SHOT/CAST = empower_cast.wav, ...
						//chr == might
			case 6: createL2Effect(params[1], SPR_MIGHT, 0.4, 221.0, 16.0 /*15.0*/, 1.3);
						//SHOT/CAST = might_cast.wav, ...
						//chr == ACUMEN
			case 7: createL2Effect(params[1], SPR_PROPHECY, 0.6, 180.0, 18.0 /*17.0*/, 2.2);
						//CAST/ SHOT == Profish_cast.wav,...
						//chr == no sound

			//createL2Effect(target, effect, size, opacity, framerate, showtime)
		}
		// TEST : not stop give buffs
		set_task(1.5, "FMagic_useBuffs", params[0]+TASK_FMAGIC_USE_BUFFS, params, 3);

		// sit down after %ALL_BUFFS_TIME%
		entity_set_float(params[0], EV_FL_nextthink, get_gametime() + 6.0);
	}
	else{
		Npc_playAnim(params[0], ANIM_ATKWAIT_POLE_FMAGIC, 1.0);
		entity_set_float(params[0], EV_FL_nextthink, get_gametime() + 12.0);
	}
}

// FMagic_doDie()
public FMagic_killed(ent, killer)
{
	emit_sound(ent, CHAN_VOICE, S_NPC_FMAGIC[2], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	
	// death animation
	Npc_playAnim(ent, ANIM_DEATH_FMAGIC, 1.0);

	entity_set_int(ent, EV_INT_solid, SOLID_NOT);
//	emit_sound(ent, CHAN_VOICE, SNOW_MAN_SOUND_DIE, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	
	// deathwait after 3.3s
	Npc_taskAnim(3.3, ent, ANIM_DEATHWAIT_FMAGIC);
	
	entity_set_float(ent, EV_FL_nextthink, get_gametime() + INFINITE /* Const.inl */); 
}

/**
 * Use NPC animation after presented time.
 * 	@TODO: If cpl --> move to Util & rework all 'playNpcAnim'
 */
Npc_taskAnim(Float:tasktime, ent, anim, Float:framerate=1.0)
{
	new Float:params[3];
	params[0] = float(ent);
	params[1] = float(anim);
	params[2] = framerate;
	
	set_task(tasktime, "Npc_taskAnimUse", ent+TASK_NPC_ANIN_USE, _:params, 3); // float task
}
public Npc_taskAnimUse(Float:params[3], taskid)
{
	Npc_playAnim(floatround(params[0]), floatround(params[1]), params[2]);
}

stock Npc_playAnim(ent, sequence, Float:speed)
{
	set_pev(ent, pev_animtime, get_gametime());
	set_pev(ent, pev_sequence, sequence);
	//set_pev(ent, pev_gaitsequence, 0);
	set_pev(ent, pev_framerate, speed);
	//set_pev(ent, pev_frame, 0.0);
}

stock Npc_setAim(npc, target)
{
	new Float:target_ori[3], Float:npc_ori[3];
	pev(target, pev_origin, target_ori);
	pev(npc, pev_origin, npc_ori);
	
	new Float:new_angles[3]

	pev(npc, pev_angles, new_angles);
	new Float:x = target_ori[0] - npc_ori[0];
	new Float:z = target_ori[1] - npc_ori[1];

	new Float:radians = floatatan(z/x, radian);
	new_angles[1] = radians * (180 / 3.14);
	
	if(target_ori[0] < npc_ori[0])
		new_angles[1] -= 180.0;

	set_pev(npc, pev_angles, new_angles);
}
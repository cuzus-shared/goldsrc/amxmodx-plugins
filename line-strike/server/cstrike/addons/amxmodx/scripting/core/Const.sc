// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

// Player & single tasks
enum (+= 33)
{
	TASK_RESPAWN_ME = 100,
	TASK_REMOVE_PROTECTION,
	TASK_WARMUP_CHECK,
	TASK_PLAYER_SPAWNED_POST,
	TASK_TEAM_SCORE,
	TASK_RESPAWN_COUNTDOWN,
	TASK_TO_VILLAGE,
	TASK_VALIDATE_RESPAWN,
	TASK_RESPAWN_AT_FLAG,
	TASK_FORCE_CHOOSE_TEAM,
	TASK_FORCE_AUTO_JOIN,
	TASK_LOGIN_CHANGE_TEAM_COUNTDN,
	TASK_LOGIN_TASK_SPAWN,
	TASK_JOIN_WELCOME_HUD,
	TASK_ATTACH_ACCESSORY,
	TASK_SET_WEAPON_ENCHANT,
	TASK_SET_DEATH_CAM,
	TASK_SET_ARROW_TRAIL,
	TASK_SET_WEBKEY,
	TASK_L2_FONT_DECO,
	TASK_LOGIN_WAIT_JOIN,
	TASK_RESET_ADENA_PICK,
	TASK_NEWS_FEED,
	TASK_ROUND_TIME_UPDATE,
	TASK_JOIN_TRANSFER,
	TASK_WARMUP_FULL_END,
	TASK_PLAYER_PUT_ON_MAP,
	TASK_DATABASE_SAVE_DATA,
	TASK_MYSQL_AUTO_SAVE,
	TASK_GAME_MODE_NEXTMAP_COUNTDN,
	TASK_GAME_MODE_TIME_NOTIFY,
	TASK_DROP_DEATH_CAM,
	TASK_PODBOT_AUTO_JOIN,
	TASK_PODBOT_UNJOIN,
	TASK_TVT_COUNT_WIN,
	TASK_CAMERA_ARROW_CHASE,
	TASK_PLAYER_SIT_IDLE,
	TASK_PLAYER_NO_ACT,
	TASK_LASTHERO_NO_WINNER,
	//TASK_LASTHERO_MAKE_POSITIONS,
	TASK_SAY2_SHUTDOWN,
	TASK_DEATH_SHOW_SPRITE,
	TASK_LASTHERO_COUNTDOWN,
	TASK_LASTHERO_UNBLOCK,
	TASK_SAY2_HERO_GLOW,
	TASK_SAY2_AMBPOINTS,
	TASK_SAY2_DYNLIGHTS,
	TASK_SLOWMOTION_END,
	TASK_SLOWMOTION_CREATECLONE,
	TASK_SLOWMOTION_REMOVECLONE,
	TASK_LOGIN_CHANGE_TEAM,
	TASK_BOT_SELECT_TEAM,
	TASK_STATUS_TEXT,
	TASK_WARMUP_TIME,
	TASK_WARMUP_IDLE,
	TASK_EFFECT_LEVEL_UP,
	TASK_LASTHERO_CHECK_LAST,
	TASK_LASTHERO_END,
	TASK_GATEKEEPER_TELEPORT_POST,
	TASK_GAMEMODE_COUNTDOWN_MUS
	// ~ 1981 + 33
};

// Skills tasks
enum (+= 33)
{
	TASK_SKILLS_USE_DASH = 3000,
	TASK_SKILLS_DASH_TIME,
	TASK_SKILLS_END_DASH,
	//TASK_SKILLS_FADE_DASH,
	//TASK_COOLTIME_DASH,
	//TASK_COOLTIME_STUN,
	TASK_SKILLS_USE_STUN_SHOT,
	TASK_SKILLS_STUNNED,
	TASK_SKILLS_TOGGLE_FIST_FURY,
	TASK_SKILLS_USE_RETURN,
	TASK_SKILLS_CORRUPTED_MAN_UNSUM,
	TASK_SKILLS_MAKE_DOUBLE_SHOT_A1,
	TASK_SKILLS_MAKE_DOUBLE_SHOT_A2,
	TASK_SKILLS_MAKE_HURRICANE,
	TASK_SKILLS_END_FRENZY,
	TASK_SKILLS_END_ZEALOT,
	TASK_COOLTIME_END,
	TASK_SKILLS_CAST_SUMMON_CORRUPT,
	TASK_SKILLS_MAKE_SUMMON_CORRUPT,
	TASK_SKILLS_CAST_BATTLE_HEAL,
	TASK_SKILLS_CAST_FOCUSED_FORCE,
	TASK_SKILLS_ENDCAST_DRYAD_ROOT,
	TASK_SKILLS_DETACH_DRYAD_ROOT,
	TASK_SKILLS_CAST_STUN_SHOT,
	TASK_SKILLS_SUMMON_IDLE,
	TASK_SKILLS_END_CAST_SNIPE,
	TASK_SKILLS_END_ULT_DEFENSE,
	TASK_SKILLS_CAST_ULT_DEFENSE,
	//TASK_SKILLS_COOLDOWN_ICON_SET,
	TASK_SKILLS_END_EFFECT_SNIPE,
	//TASK_SKILLS_COOLDOWN_ICON_SET_1,
	//TASK_SKILLS_COOLDOWN_ICON_SET_2,
	//TASK_SKILLS_COOLDOWN_ICON_SET_3,
	//TASK_SKILLS_COOLDOWN_ICON_SET_4,
	TASK_SKILLS_CAST_REFLECT_DAMAGE,
	TASK_SKILLS_END_REFLECT_DAMAGE,
	TASK_SKILLS_SLEEP_EFFECT,
	TASK_SKILLS_SLEEPED,
	TASK_SKILLS_USE_DOUBLE_SHOT,
	TASK_SKILLS_CAST_DOUBLE_SHOT,
	TASK_SKILLS_USE_ARMOR_CRUSH,
	TASK_SKILLS_CAST_HAMMER_CRUSH,
	TASK_SKILLS_USE_SUMMON_CUBICS,
	TASK_SKILLS_CAST_SUMMON_CUBICS,
	TASK_SKILLS_CAST_AURA_FLARE,
	TASK_SKILLS_USE_DREAMING_SPIRIT,
	TASK_SKILLS_CAST_DREAMING_SPIRI,
	TASK_SKILLS_HIT_DREAMING_SPIRIT,
	TASK_SKILLS_CAST_SNIPE,
	TASK_SKILLS_CAST_RETURN,
	TASK_SKILLS_CAST_SILENCE,
	TASK_SKILLS_REMOVE_SILENCE,
	TASK_SKILLS_CAST_BACKSTAB,
	TASK_SKILLS_USE_LETHAL_SHOT,
	TASK_SKILLS_CAST_LETHAL_SHOT,
	TASK_SKILLS_EFFS_DEADLY_BLOW,
	TASK_SKILLS_CAST_BATTLE_ROAR,
	TASK_SKILLS_CAST_STEAL_ESSSENCE,
	TASK_SKILLS_CAST_GATE_CHANT,
	TASK_SKILLS_USE_GATE_CHANT,
	TASK_SKILLS_SUCCESS_LETHAL_SHOT,
	TASK_SKILLS_PUSH_SHIELD_STUN,
	TASK_SKILLS_CAST_BLUFF,
	TASK_SKILLS_CAST_BALANCE_LIFE,
	TASK_SKILLS_USE_BALANCE_LIFE,
	TASK_SKILLS_CAST_AGGRESSION,
	TASK_SKILLS_PLAYER_MOVE_BY_AGGR
	// ~ 5244
};

/*
Task RANGES from NPCs 2 fix:
	Outpost ids :	194,208
	clarissa ids :	222,229,231,233
	taurin ids :	226,228
	spirit ids : 	238,239
*/
// NPC tasks
enum (+= 2000)
{
	TASK_NPC_IDLE = 25000,
	//TASK_MONSTER_IDLE,
	TASK_MONSTER_ATTACK,
	TASK_MONSTER_FALL,
	TASK_MONSTER_REMOVE_CORPSE,
	TASK_MONSTER_RESPAWN,
	TASK_GUARD_SPAWNED,
	TASK_GUARD_ATTACK_POST,
	TASK_GUARD_REMOVE_CORPSE,
	TASK_GUARD_RESPAWN,
	TASK_AVANPOST_DESTROYED,
	TASK_FMAGIC_USE_BUFFS,
	TASK_FMAGIC_GIVE_EFFECTS,
	TASK_NPC_ANIN_USE,
	TASK_NPC_SET_NAME_SPRITE,
	TASK_NPC_SPAWN
	// ~ 55000
};

// Effects tasks (+5000)
#define TASK_L2EFFECT_REMOVE					90000

// System consts
#define ID_NEXT_PRIMARY_ATK			46
#define ID_NEXT_SECONDARY_ATK		47

// Linux offsets
#if cellbits == 32
	#define OFFSET_CSMONEY		115
#else
	#define OFFSET_CSMONEY		140
#endif
#define OFFSET_LINUX			5
#define OFFSET_LINUX_WEAPONS	4
#define OFFSET_WEAPONOWNER		41
#define OFFSET_TEAM				114

// Main
#define XX 33					// max players const :D (used everywhere)
#define FLAG_RESPAWN_MIN		6.0		//30.0
#define FLAG_RESPAWN_MAX		18.0	//120.0
#define MAX_PLAYER_HEALTH		255 //128
#define MAX_PLAYER_LEVEL		78
#define ADENA_MAX_AMOUNT		99999 // due to client limit	// 2147483647
#define ADENA_MIN_DROP_AMOUNT	1
#define ADENA_MAX_DROP_AMOUNT	1
#define MAX_CHARNAME_LENGTH		33
#define MAX_PASSWORD_LENGTH		21
#define MAX_MAPNAME_LENGTH		49
#define MAX_IP_ADDR_LENGTH		17

// Class Damages
#define CLASSDMG_A_SHOT_10_MIN		1.0		// 1 < D < 10
#define CLASSDMG_A_SHOT_10_MAX		3.0
#define CLASSDMG_A_SHOT_30_MIN		10.0	// 10 < D < 30
#define CLASSDMG_A_SHOT_30_MAX		15.0
#define CLASSDMG_A_SHOT_60_MIN		15.0	// 30 < D < 60
#define CLASSDMG_A_SHOT_60_MAX		20.0
#define CLASSDMG_A_SHOT_90_MIN		25.0	// 60 < D < 90
#define CLASSDMG_A_SHOT_90_MAX		25.0
#define CLASSDMG_A_SHOT_MAX			30.0	// D > 90

#define DAMAGE_ADD_WITH_ZOOM		10.0

#define	CLASSDMG_A_MIN		1.0		// hands
#define CLASSDMG_A_MAX		2.0
#define CLASSDMG_B_MIN		3.0
#define CLASSDMG_B_MAX		7.0
#define CLASSDMG_C_MIN		2.0
#define CLASSDMG_C_MAX		5.0
#define CLASSDMG_D_MIN		4.0
#define CLASSDMG_D_MAX		8.0
#define CLASSDMG_E_MIN		5.0
#define CLASSDMG_E_MAX		8.0
#define CLASSDMG_F_MIN		1.0
#define CLASSDMG_F_MAX		3.0
#define CLASSDMG_G_MIN		1.0
#define CLASSDMG_G_MAX		3.0
#define CLASSDMG_H_MIN		3.0
#define CLASSDMG_H_MAX		7.0

// Class Atk.Speed ( > low,  < fast )
#define CLASS_ATK_SPEED_ARCHER		1.3		// hands
#define CLASS_ATK_DELAY_ARCHER		0.7		// bow
#define CLASS_ATK_SPEED_ROGUE		1.1
#define CLASS_ATK_SPEED_TYRANT		1.0
#define CLASS_ATK_SPEED_RAIDER		1.9 //1.8
#define CLASS_ATK_SPEED_KNIGHT		0.9
#define CLASS_ATK_SPEED_WIZARD		2.2
#define CLASS_ATK_SPEED_PRIEST		1.9
#define CLASS_ATK_SPEED_SHAMAN		1.6
// TODO : primary and secondary NOT SAME by atk spd --> rework it

// <Effects>
// ScreenFade
#define FFADE_IN			0x0000		// Just here so we don't pass 0 into the function
#define FFADE_OUT			0x0001		// Fade out (not in)
#define FFADE_MODULATE		0x0002		// Modulate (don't blend)
#define FFADE_STAYOUT		0x0004		// ignores the duration, stays faded out until new ScreenFade message received

new const MAP_OBJECTIVES[][] = 
{
	"weaponbox",
	"armoury_entity",
	"grenade",
	"func_bomb_target",
	"info_bomb_target",
	"info_vip_start",
	"func_vip_safetyzone",
	"func_escapezone",
	"hostage_entity",
	"monster_scientist",
	"func_hostage_rescue",
	"info_hostage_rescue",
	"func_vehicle",
	"func_vehiclecontrols"
};
new var_fwSpawn;

// Sync objects
new SkillsSyncHud, /*ArrowSyncHud,*/ KillStreakSyncHud1, KillStreakSyncHud2;

// <Player>
new bool:SetClassReq[XX];		// true/false
new SetClassReqInd[XX];			// classId

new PlayerFirstSpawn[XX] = {true, ...};

// <Warmup>
new WarmupTime;
new bool:WarmupStarted = false;
new WarmupIsEnded;

// <GameMode>
new bool:EndOfMap;

// <Respawn>
new Float:corpse_origin[XX][3];	// player corpse XYZ
new respawn_timeleft[XX];
//new spawn_protected[33];

// <Camera>
new JoinCameraEnt; // 1 for all
new CamDist[XX] = {10, ...};

// <Shoot>, <Core>
new Float:CD_BowShot[XX];
new Float:BowNextAtk[XX];

new bool:Atk1BtnPressed[XX];
new bool:PlayerHasZoom[XX];

new Float:ShotDamage[XX];
new ShotPowerAmt[XX] = {0, ...};

// <Player>
#define WTYPE				0
#define WTYPE_HANDS			1
#define WTYPE_KNIFE_CLASS	2
#define WTYPE_KNIFE_OTHER	3
#define WTYPE_BOW			4
new bool:WTYPE_OTHER_ALLOWED = false;
new PlayerWeapon[XX][1];				// id, wtype
new PlayerWeapEncEnt[XX] = {0, ...};	// attached entity for weapon enchant

// <Core> (Adena from kills)
#define cs_set_money_value(%1,%2)	set_pdata_int(%1, 115, %2, OFFSET_LINUX)
new MsgHookMoney;
new GNewAdena[XX] = {0, ...};

// <Login> (globals)
new WestPlayers = 0, EastPlayers = 0;
new SelectedTeam[XX] = {0, ...};

// <Database>
//"8996634af6505c525ca2128ae2544361" // config.cfg --> setinfo %CONNECTION_STRING% %password%
#define CONNECTION_STRING		"`"
#define MYSQL_AUTO_SAVE_TIME	600.0
new Handle:SQL_Tuple;

/**
 * Captured Messages
 */
//new gmsgRoundTime;
new gmsgCurWeapon;
new gmsgCrosshair;
//new gmsgShowMenu;
new gmsgMoney;
new gmsgSayText;
new gmsgHideWeapon;
new gmsgScreenFade;
new gmsgScreenShake;
new gmsgBarTime;
//new gmsgSetFOV;
new gmsgStatusIcon;
new gmsgStatusText;
//new gmsgDamage;
//new gmsgDeathMsg;
new gmsgTeamInfo;
new gmsgScoreInfo;
new gmsgTeamScore;
//new gmsgBlinkAcct;
//new gmsgStatusValue;
//new gmsgHudText;
//new gmsgClCorpse;

/**
 * Single Variables
 */
new GMaxPlayers;

//new bool:FONT_DECO_ENABLED = false;
//new bool:FontDecoShow[XX];

enum _:MENU_VALUES
{
	MAIN_MENU = 0,
	CLASS_MENU,
	RESPAWN_MENU
};
new bool:isMenuOpened[XX][MENU_VALUES];

new RespawnBlocked[XX]			= {false, ...}; 		// <LastHero>, ...
new AllMenusBlocked[XX]			= {false, ...};

// Event condition variables
new bool:IsEventReady = false;
new bool:EventStarted = false;	// global var (prevents join after game started - LastHero, ...)

#define FOR_ALL 0	// <Skills>, etc

#define COLOR_EAST		"#c97777"	//FF3F3F
#define COLOR_WEST		"#5686aa"	//"#77c97c" //99CCFF
#define COLOR_L2_LINK	"#689afd"
#define COLOR_L2_LEVEL	"#ffc900"
#define COLOR_L2_LRED	"#F0197A"
#define COLOR_L2_LBLUE	"#3CD4F0"
#define COLOR_L2_GREEN	"#00FB00"
#define COLOR_L2_LGREEN	"#3CE373"

#define TEAMID_EAST		1
#define TEAMID_WEST		2
#define TEAMID_SPEC		3
#define TEAMID_NULL		0

#define TNAME_EAST	"East"
#define TNAME_WEST	"West"
#define TNAME_SPEC	"Spectator"
#define TNAME_NULL	"Unassiged"

#define NPC_OWNER_EAST	1337
#define NPC_OWNER_WEST	1338

/**
 * @see <MapFactory>
 * Short map names (A-Z)
 */
#define MAP_BAIUMS_LAIR			"baiums_lair_v1_arc"
#define MAP_CRUMA_TOWER			"cruma_tower_v1_arc"
#define MAP_DE_GLUDIN			"de_gludin_unfin_arc"
#define MAP_GIRAN_ARENA			"giran_arena_v1_arc"

new GMapName[MAX_MAPNAME_LENGTH];
new GMapAuthor[MAX_MAPNAME_LENGTH];

// Max float (NPC AI), Max health, Max damage
#define INFINITE	999999999.9

// Configs file path
#define CFG_FILE_JOINCAM	"addons/amxmodx/configs/head/joincam.ini"
#define CFG_FILE_LASTHERO	"addons/amxmodx/configs/head/lasthero.ini"
#define CFG_FILE_BLACKLIST	"addons/amxmodx/configs/head/blacklist.ini"
#define LOGS_DIR			"addons/amxmodx/logs"

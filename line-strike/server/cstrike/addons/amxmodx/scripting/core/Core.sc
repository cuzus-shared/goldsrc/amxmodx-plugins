// --------------------------------------------------
// Core
// --------------------------------------------------

public pluginFail(reason[], fromId)
{
	if(fromId){
		server_cmd("kick #%d ^"Server under maintenance. \nPlease try again later.^"", get_user_userid(fromId));
	}

	/* Pauses function or plugin so it won't be executed.
	* In most cases param1 is name of function and
	* param2 name of plugin (all depends on flags).
	* Flags:
	* "a" - pause whole plugin.
	* "c" - look outside the plugin (by given plugin name).
	* "d" - set "stopped" status when pausing whole plugin.
	*       In this status plugin is unpauseable.
	* Example: pause("ac","myplugin.amxx") 
	*
	* Note: There used to be the b and e flags as well,
	* which have been deprecated and are no longer used.

	native pause(const flag[], const param1[]="",const param2[]="");
	*/
	
	Util_printMona();
	
	for(new i=0; i<5; i++)
		Log(FATAL, "Core", "SERVER TERMINATED ABNORMALLY!!!");
	
	set_fail_state(reason);	
}

public plugin_end()
{
	Database_pluginEnd(); // close connection
	Login_pluginEnd();
}

public plugin_precache()
{
	Config_load();

	// Util_printSection("Engine");
	out("====================================================================-[ Engine ]");

	// webkey gen : http://www.sethcardoza.com/tools/random-password-generator/
	new count[1];
	count[0] = 0;
	set_task(0.5, "setWebkey", TASK_SET_WEBKEY, count, 1);

	
	// Util_printSection("GameMode");
	out("=================================================================-[ Game Mode ]");
	
	get_mapname(GMapName, MAX_MAPNAME_LENGTH-1);

	set_task(0.1, "Database_init");

	if(strlen(GMapName) > 2){
		GameMode_init();
		MapFactory_setAuthor(); // set map author to global variable
	}
	else{
		Log(FATAL, "Core", "plugin_precache() : Could not get map name!");
		Log(FATAL, "Core", "plugin_precache() : Plugin load aborted ...");
		pluginFail("", 0);
	}

	Cache_pluginPrecache();

	var_fwSpawn = register_forward(FM_Spawn, "fwEntitySpawn"); // no objectives
	
	// additional info
	Log(INFO, "Core", "Total maps (OD)  = %d [0-9]", sizeof MAPFACTORY_OUTPOST_DEF - 1);
	Log(INFO, "Core", "Total maps (LH)  = %d [0-14]", sizeof MAPFACTORY_LAST_HERO - 1);
	Log(INFO, "Core", "Total maps (TVT) = %d [0-26]", sizeof MAPFACTORY_TVT - 1);
	
	new _dir, _file[128], _ttl = 0;
	_dir = open_dir("maps", _file, 127); //	/cstrike/maps
	if(_dir){
		while(next_file(_dir, _file, 127)){
			if(_file[0] == '.')
				continue;
			if(containi(_file, ".bsp") != -1)
				_ttl++;
		}
	}
	Log(INFO, "Core", "Total *bsp maps  = %d", _ttl);
}

public setWebkey(count[1], this)
{
	new WEBKEY_DATA[128];
	new WEBKEY_PATH[] = "addons/amxmodx/data/webkey.dat";

	if(file_exists(WEBKEY_PATH)){
		new _null;
		if(read_file(WEBKEY_PATH, 0, WEBKEY_DATA, charsmax(WEBKEY_DATA), _null))
			Log(INFO, "Core", "Readed webkey from file: %s", WEBKEY_DATA);
		else
			Log(INFO, "Core", "Can't read webkey from file: %s", WEBKEY_PATH);
	}
	else
		Log(ERROR, "Core", "Webkey file does not exists: %s", WEBKEY_PATH);

	if(strlen(WEBKEY_DATA) && !equal(WEBKEY_DATA, ""))
	{
		Log(INFO, "Core", "Setting webkey to: %s", WEBKEY_DATA);
		server_cmd("sv_password ^"%s^"", WEBKEY_DATA);
	}
	else if(count[0] <= 5)
	{
		count[0]++;
		set_task(5.0, "setWebkey", this, count, 1);
		Log(WARN, "Core", "Webkey data is not set (%s). Attempts: %d/5", WEBKEY_DATA, count[0]);
	}
	//else if(count[0] > 5) 
	//	FAILSTATE
	//	Server will be shutting down NOW
}

public plugin_init()
{
//	Util_printSection("Init");
	out("======================================================================-[ Init ]");
	
	register_plugin(PLUGIN, VERSION, AUTHOR);
// =======================================================================
// Events
// =======================================================================
	//register_event("ResetHUD", "evResetHud", "be");
	//register_event("HLTV", "evNewRound", "a", "1=0", "2=0");
	//register_event("TextMsg", "evRoundRestart", "a", "2=#Game_Commencing", "2=#Game_will_restart_in");
	//register_logevent("evRoundStart", 2, "0=World triggered", "1=Round_Start");
	//register_logevent("evRoundEnd", 2, "1=Round_End");

	register_event("TextMsg", "evTextMsg", "b", "1=4", "2=#Spec_Mode3");

	register_event("StatusValue", "evShowStatus", "be", "1=2", "2!0"); // Если игрока смотрит на какого то человека 
	register_event("StatusValue", "evHideStatus", "be", "1=1", "2=0"); // Если туда куда он смотрит нету человека

	//register_event("CurWeapon", "evCurWeapon", "be"); // Now, used only for L2FontDeco
// =======================================================================
// Thinks
// =======================================================================
	register_think(PET_CORRUPTED_MAN, "CorruptedMan_think");		// <Summon>
//	register_think(PET_STRIDER, "Strider_think");					// <Summon>

	register_think(MON_IMP, "Imp_think");							// <Monster>
	register_think(MON_TREASURE_CHEST, "TreasureChest_think");		// <Monster>
	
	// opt registers - only if current map need it
	if(mapIs(MAP_BAIUMS_LAIR))
	{
		register_think(MON_GUARDIAN_ANGEL, "GuardianArchangel_think");	// <Monster>
	}
	else if(mapIs(MAP_CRUMA_TOWER))
	{
		register_think(NPC_GATEKEEPER, "Gatekeeper_think");				// <Npc>
		register_think(MON_PORTA, "Porta_think");						// <Monster>
		register_think(MON_EXCURO, "Excuro_think");						// <Monster>
		register_think(MON_MORDEO, "Mordeo_think");						// <Monster>
		register_think(MON_CATHEROK, "Catherok_think");					// <Monster>
	}
	/*else if(mapIs(MAP_DECK17))
	{
		register_think(MON_BONE_PUPPETEER, "BonePuppeteer_think");		// <Monster>
		register_think(MON_BEHEMOTH_ZOMBIE, "BehemothZombie_think");	// <Monster>
	}*/
	
	register_think(NPC_WAREHOUSE, "Warehouse_think");				// <Npc>
	register_think("L2Guard@East", "thinkTGuard");					// <Guard>
	register_think("L2Guard@West", "thinkCTGuard");					// <Guard>
	register_think(NPC_BUFFER, "FMagic_think");						// <Npc>
	
	register_think("L2Effect@ForceBlaster", "Skills_thinkForceBlaster");
// =======================================================================
// Ham's
// =======================================================================
	RegisterHam(Ham_Spawn, "player", "hamPlayerSpawnPost", 1);
//	RegisterHam(Ham_TraceAttack, "player", "hamTraceAttackPlayerPre", 0);
	RegisterHam(Ham_TakeDamage, "player", "hamTakeDamagePlayerPre", 0);
	RegisterHam(Ham_Killed, "player", "hamPlayerKilledPre", 0);
	RegisterHam(Ham_Killed, "player", "hamPlayerKilledPost", 1);

	RegisterHam(Ham_TakeDamage, "info_target", "hamTakeDamageEntity"); //, 1
	RegisterHam(Ham_TraceAttack, "info_target", "hamTraceAttackEntity");
	RegisterHam(Ham_Killed, "info_target", "hamKilledEntity");

	RegisterHam(Ham_Weapon_PrimaryAttack, "weapon_mp5navy", "hamBowPrimaryAttack");
	RegisterHam(Ham_Weapon_SecondaryAttack, "weapon_mp5navy", "hamBowPrimaryAttack");
	RegisterHam(Ham_Item_Deploy, "weapon_mp5navy", "hamBowDeployPost", 1);
	//RegisterHam(Ham_Weapon_Reload, "weapon_mp5navy", "hamBowReloadPost", 1);

	RegisterHam(Ham_Item_Deploy, "weapon_knife", "hamKnifeDeployPost", 1);
	RegisterHam(Ham_Weapon_PrimaryAttack, "weapon_knife", "hamKnifePrimaryAttackPost", 1);
	RegisterHam(Ham_Weapon_SecondaryAttack, "weapon_knife", "hamKnifePrimaryAttackPost", 1);
	
	RegisterHam(Ham_ObjectCaps, "player", "hamObjectCaps"); // +USE hook
// =======================================================================
// FakeMeta forwards
// =======================================================================
	unregister_forward(FM_Spawn, var_fwSpawn); // no objectives
	
	register_forward(FM_CmdStart, "fwCmdStart");
	register_forward(FM_UpdateClientData, "fwUpdateClientData", 1);
	register_forward(FM_PlayerPreThink, "fwPlayerPreThink");		// Arrow Power, Skills
	
	register_forward(FM_ClientKill, "fwClientKillPre", false);		// block "kill" client cmd

	register_forward(FM_SetModel, "fmSetModel");
	register_forward(FM_Touch, "fmTouch");

	register_forward(FM_EmitSound, "fmEmitSound");
	//register_forward(FM_EmitSound, "fwEmitSoundPost", 1);			// <DLLFix>

	//register_forward(FM_TraceLine, "fwTraceLinePost", 1);			// <TargetGlow>
	//register_forward(FM_AddToFullPack, "fwAddToFullPackPost", 1);	// <TargetGlow>
	
	register_forward(FM_AddToFullPack, "fwAddToFullPackPost", 1);	// Double-handed weapon animations

	//register_forward(FM_TraceLine, "fmTraceLine");
	//register_forward(FM_TraceHull, "fmTraceHull");

	register_forward(FM_Sys_Error, "fwServerDown");
	register_forward(FM_GameShutdown, "fwServerDown");
	//register_forward(FM_ServerDeactivate, "fwServerDown"); /* [META] ERROR: Plugin didn't set meta_result: fakemeta_amxx.dll:ServerDeactivate() */
// =======================================================================
// Message hooks
// =======================================================================
	//gmsgShowMenu = get_user_msgid("ShowMenu");
	//gmsgScenario = get_user_msgid("Scenario");
	//gmsgBombDrop = get_user_msgid("BombDrop");
	//gmsgBombPickup = get_user_msgid("BombPickup");
	gmsgCurWeapon = get_user_msgid("CurWeapon");
	gmsgHideWeapon = get_user_msgid("HideWeapon");
	//gmsgRoundTime	= get_user_msgid("RoundTime");
	//set_msg_block(get_user_msgid("RoundTime"), BLOCK_SET);
	gmsgCrosshair	= get_user_msgid("Crosshair");
	gmsgMoney		= get_user_msgid("Money");			// <DropItem>
	//gmsgSetFOV	= get_user_msgid("SetFOV");			// <Lifestone> (Adrenaline) @WAIT
	gmsgBarTime 	= get_user_msgid("BarTime");
	gmsgScreenFade	= get_user_msgid("ScreenFade");
	gmsgScreenShake	= get_user_msgid("ScreenShake");
	gmsgStatusIcon	= get_user_msgid("StatusIcon");		// HUD icon
	gmsgStatusText	= get_user_msgid("StatusText");
	gmsgSayText		= get_user_msgid("SayText");
	register_message(gmsgSayText, "hookedSayText");		// <DLLFix>
	//gmsgDamage	= get_user_msgid("Damage");
	//gmsgDeathMsg	= get_user_msgid("DeathMsg");
	gmsgTeamScore	= get_user_msgid("TeamScore");
	gmsgTeamInfo	= get_user_msgid("TeamInfo");
	//register_message(gmsgTeamInfo, "msgTeamInfo");		// <Stats>
	gmsgScoreInfo	= get_user_msgid("ScoreInfo");
	register_message(gmsgScoreInfo, "msgScoreInfo");		// <Stats>
	//set_msg_block(get_user_msgid("ScoreInfo"), BLOCK_SET);
	//gmsgBlinkAcct = get_user_msgid("BlinkAcct")
	//gmsgStatusValue = get_user_msgid("StatusValue");
	//gmsgClCorpse = get_user_msgid("ClCorpse");
	register_message(get_user_msgid("ClCorpse"), "msgClCorpse");
	
	register_message(SVC_TEMPENTITY, "msgTempEntity");

	// <Login>
	//register_message(get_user_msgid("ShowMenu"), "messageShowMenu");
	set_msg_block(get_user_msgid("ShowMenu"), BLOCK_SET);
	//register_message(get_user_msgid("VGUIMenu"), "messageVGUIMenu");
	set_msg_block(get_user_msgid("VGUIMenu"), BLOCK_SET);

	register_message(gmsgHideWeapon, "msgHideWeapon");

	register_message(get_user_msgid("RoundTime"), "msgRoundTime");	// <GameMode>

	set_msg_block(get_user_msgid("AmmoPickup"), BLOCK_SET);
	set_msg_block(get_user_msgid("DeathMsg"), BLOCK_SET);

	// <DLLFix>
	//register_message(get_user_msgid("SendAudio"), "msgSendAudio"); // T, CT win messages 
// =======================================================================
// Commands
// =======================================================================
	// Try to block default client commands
	DLLFix_init();
	// <DropItem>
	register_clcmd("moneydrop", "usrcmd_MoneyDrop");
	// <Say2>
	register_clcmd("say", "Say2_hookSay");
	register_clcmd("say_team", "Say2_hookSay");
	// <Camera>
	register_clcmd("camdistup", "Camera_distUp");
	register_clcmd("camdistdn", "Camera_distDn");
	register_clcmd("camres", "Camera_reset");
	// <Player>
	register_clcmd("nextweapon", "Player_nextWeapon");

	// TESTED
	//register_clcmd("say /sell", "Cmd_slashSell");	// @wait
	//register_clcmd("say /buy", "Cmd_slashBuy");
	//register_clcmd("say /inventory", "cmd_slashInventory");
	//register_clcmd("say /inv", "cmd_slashInventory");
// =======================================================================
// Etc
// =======================================================================
	// Global variables
	GMaxPlayers = get_maxplayers();
	// Synchronized HUDs
	KillStreakSyncHud1 = CreateHudSyncObj();
	KillStreakSyncHud2 = CreateHudSyncObj();
//	ArrowSyncHud = CreateHudSyncObj();			// unused due to shot power moved to armor
	SkillsSyncHud = CreateHudSyncObj();

	// Inits
	Login_init();
	Warmup_init();

	// Skills
	if(isTeamGameMode() || isDmGameMode()){
		SkillHandler_init();
	}

	Bot_init(); // initialize bots params and organize auto-join

	// Endless Tasks
	set_task(float(Config[GLOBAL_WARMUP_TIME]), "autoHpMpRegeneration");

// =======================================================================
// TestSuite (always at end!)
// =======================================================================
	register_clcmd("+test1", "cmd__test1");			// TE_BEAMDISK #2

	register_clcmd("+enchant", "test__enchant_me");
	register_clcmd("+strsp", "test__streaksp");

	// Plugin 100% load ==> Open login
	Login_setOpened(true);
}

public client_connect(id)
{
	if(is_user_hltv(id)) // && name != ZombieTV
		server_cmd("kick #%d ^"HLTV not allowed on this server!^"", get_user_userid(id));

	// Check player for flood before login
	if(Login_clientConnect(id))
	{
		if(!isBot(id))
		{
			client_cmd(id, "mp3 play cache/music/nt_oren"); // connect sound

			// KF-like connect message
			new _addr[32];
			get_cvar_string("net_address", _addr, 31);

			new _map[MAX_MAPNAME_LENGTH];
			get_mapname(_map, MAX_MAPNAME_LENGTH-1);
			replace(_map, MAX_MAPNAME_LENGTH-1, "_arc", "");
			replace(_map, MAX_MAPNAME_LENGTH-1, "_ls", "");

			//http://forum.vingrad.ru/forum/act-ST/f-92/t-381059/unread-1.html
			//http://programmersforum.ru/showthread.php?p=1444102#post1444102
			//http://www.cyberforum.ru/c-beginners/thread1317579.html#post6931695
			//http://goo.gl/OU4gYl

			const MAX_MSG_LENGTH = 79;
			new msg1[MAX_MSG_LENGTH+1];
			new msg2[MAX_MSG_LENGTH+1];
			new msg3[MAX_MSG_LENGTH+1];
			format(msg1, MAX_MSG_LENGTH, "Connecting %s", _addr);
			if(strlen(GMapAuthor) > 1)
				format(msg2, MAX_MSG_LENGTH, "Map: %s by %s", _map, GMapAuthor);
			else
				format(msg2, MAX_MSG_LENGTH, "Map: %s", _map);
			format(msg3, MAX_MSG_LENGTH, "Game Mode: %s", GameMode_getName());

			new len1 = strlen(msg1); //ex: Connecting 255.255.255.255:00000
			new len2 = strlen(msg2); //ex: Map: cruma_tower by Geekrainian
			new len3 = strlen(msg3); //ex: Game Mode: Team vs Team

			new maxlen = max(len1, len2);
			maxlen = max(maxlen, len3);

			new i, tmp = (maxlen-len1)/2;
			for(i=0; i<tmp; i++){
				format(msg1, MAX_MSG_LENGTH, " %s", msg1);
			}
			tmp = (maxlen-len2)/2;
			for(i=0; i<tmp; i++){
				format(msg2, MAX_MSG_LENGTH, " %s", msg2);
			}
			tmp = (maxlen-len3)/2;
			for(i=0; i<tmp; i++){
				format(msg3, MAX_MSG_LENGTH, " %s", msg3);
			}

			client_cmd(id, "scr_connectmsg ^"%s^"", msg1);
			client_cmd(id, "scr_connectmsg1 ^"%s^"", msg2);
			client_cmd(id, "scr_connectmsg2 ^"%s^"", msg3);

			// Geo IP
			//https://forums.alliedmods.net/showthread.php?t=95665
			//https://forums.alliedmods.net/showthread.php?t=118108
			//https://forums.alliedmods.net/showthread.php?t=141015
			if(Config[LOGIN_GEO_CHECK]){
				copyc(_addr, charsmax(_addr), _addr, ':');
				new u_ip[17], country[45]; //, city[45];
				get_user_ip(id, u_ip, 16, 1);
				geoip_country(u_ip, country, 44);
				//geoip_city(u_ip, city, 44);
				Log(INFO, "Core", "Connection (%s | %s | %d km) '%s'", 
				u_ip, country, /*city,*/ floatround(geoip_distance(geoip_latitude(u_ip), geoip_longitude(u_ip), geoip_latitude(_addr), geoip_longitude(_addr))), getName(id));
			}
		}

		Player_statusTextClear(id);

		// Check player register
		Player_flush(id);
		Player_tryLoad(id);

		JoinLeave_clientConnect(id);
	}
}

public client_disconnect(id)
{
	Player_classUpdate(id);
	Player_saveData(id); // database updates
	Player_flush(id);
	updateOnlineStatus(id, false);

	// Clear map stats
	Stats_flush(id);

	if(warmupConfirmed())
	{
		Skills_clientDisconnect(id);		// reset effects
		KillStreak_clientDisconnect(id);	// clear variables
		//killassist_clientDisconnect(id);
	}

	// Remove attached ents
	removeWeaponEnchant(id);
	Accessory_checkedRemove(id);
	
	// Clear common variables
	ShotPowerAmt[id] = 0;
	GNewAdena[id] = 0;
	PlayerFirstSpawn[id] = true;

//	FontDecoShow[id] = false; // TODO: move to global flush

	Respawn_clientDisconnect(id);

	if(!isBot(id))
		Camera_clientDisconnect(id);
	
	Login_clientDisconnect(id);
	Say2_clientDisconnect(id);
	
	JoinLeave_clientDisconnect(id);
}

public client_putinserver(id)
{
	Login_clientPutInServer(id);
	
	updateOnlineStatus(id, true);

//	Stats_clientPutInServer(id);

//	if(warmupConfirmed())
//		Skills_clientPutInServer(id);
	
	//killassist_clientPutInServer(id);
	
	if(!isBot(id))
	{
		Camera_clientPutInServer(id);
//		FontDecoShow[id] = true;

		// test
		// avoid of non-gm players using Super Haste
		client_cmd(id, "cl_forwardspeed 320");
		client_cmd(id, "cl_backspeed 320");
		client_cmd(id, "cl_sidespeed 320");
	}
	
	JoinLeave_clientPutInServer(id);
}

public client_infochanged(id)
{
	// prevent change name during play
	if(isConnected(id))
	{
		new newname[MAX_CHARNAME_LENGTH], oldname[MAX_CHARNAME_LENGTH];

		get_user_info(id, "name", newname, MAX_CHARNAME_LENGTH-1);
		get_user_name(id, oldname, MAX_CHARNAME_LENGTH-1);

		if(!equali(oldname, newname))
			set_user_info(id, "name", oldname);
	}
}

public autoHpMpRegeneration()
{
	if(warmupConfirmed()){
		for(new id=1; id<=GMaxPlayers; id++){
			if(isAlive(id)){
				new hp = getHp(id);
				if(hp < MAX_PLAYER_HEALTH)
					if(hp + 1 <= MAX_PLAYER_HEALTH)
						setHp(id, hp + 1);
			}
		}
	}
	set_task(2.0, "autoHpMpRegeneration");
	return PLUGIN_HANDLED;
}

// --------------------------------------------------
// Events
// --------------------------------------------------
//public evResetHud(id){}
//public evRoundEnd(){}
//public evNewRound(){}
//public evRoundStart(){}
//public evRoundRestart(){}

public evTextMsg(id) // Free-look crosshair fix
{
	message_begin(MSG_ONE, gmsgCrosshair, _, id);
	write_byte(1);
	message_end();
}

public evShowStatus(id)	
{
	if(isPlayerValid(id) && !isBot(id))
	{
		if(GameMode != MODE_LAST_HERO)
		{
			new target = read_data(2);
			new Float:origin[3];
			pev(target, pev_origin, origin);
			origin[2] += 73.0;
			if(getTeam(id) == getTeam(target))
				createL2Effect(target, SPR_BLUE_SHIELD_ICON, 0.25, 100.0, 1.0, 2.0);
			else
				createL2Effect(target, SPR_RED_CROSS_ICON, 0.25, 100.0, 1.0, 2.0);
		}
	}

	return PLUGIN_CONTINUE;
}

public evHideStatus(id){}

/*public evCurWeapon(id){}

stock fontDecoHide(id) //taskid
{
	id -= TASK_L2_FONT_DECO;
	FontDecoShow[id] = false;
	Util_removeWeapons(id);
	giveEquip(id);
	evCurWeapon(id);
}*/

// --------------------------------------------------
// HAM's
// --------------------------------------------------
public hamBowPrimaryAttack(weapon)
{
	new id = getEntOwner(weapon);
	//new Player = get_pdata_cbase(weapon, 41, 4);
	if(playerHasBow(id))
		return HAM_SUPERCEDE;
	return HAM_IGNORED;
}

public hamBowDeployPost(weapon)
{
	static id; id = get_pdata_cbase(weapon, OFFSET_WEAPONOWNER, OFFSET_LINUX_WEAPONS /* <Const> */);

	if(isPlayerValid(id) && getClass(id) == CLASS_A)
	{
		set_pev(id, pev_viewmodel2, V_BOW);
		set_pev(id, pev_weaponmodel2, P_BOW);
		Util_playAnimV(id, 4, false, true); // draw
		
		client_cmd(id, "spk %s", S_ITEMEQUIP[0]);

		// Hide ammo for bow
		message_begin(MSG_ONE, gmsgCurWeapon, {0,0,0}, id);
		write_byte(1);
		write_byte(CSW_KNIFE);
		write_byte(0);
		message_end();
	}
}

//public hamBowReloadPost(weapon){}

public hamKnifeDeployPost(weapon)
{
	static id; id = get_pdata_cbase(weapon, OFFSET_WEAPONOWNER, OFFSET_LINUX_WEAPONS /* <Const> */);

	if(isPlayerValid(id))
	{
		new model[32];
		pev(id, pev_viewmodel2, model, 31);

		/*if(FONT_DECO_ENABLED && !isBot(id) && FontDecoShow[id])
		{
			set_pev(id, pev_viewmodel2, M_L2FONT_DECO);
			new origin[3]; get_user_origin(id, origin);
			Create_TE_ELIGHT(id, origin, 25, 210,210,210, 39, 0); //radius=60
			//Create_TE_ELIGHT(entity, start[3], radius, red, green, blue, life, decayRate)
			set_dhudmessage(150,150,150, 0.40,0.26, 0, 6.0, 2.5, 0.1, 1.5, true); //L x=0.42
			//set_dhudmessage(r,g,b,     x,   y, effects, fxtime, holdtime, fadeintime, fadeouttime, reliable)
			new map[MAX_MAPNAME_LENGTH];
			get_mapname(map, MAX_MAPNAME_LENGTH-1);
			replace(map, MAX_MAPNAME_LENGTH-1, "_arc", "");
			show_dhudmessage(id, map);
			return;
		}*/

		if(equali(model, "models/v_knife.mdl"))
		{
			switch(PlayerWeapon[id][WTYPE])
			{
				case WTYPE_HANDS: 
				{
					switch(getClass(id))
					{
						case CLASS_A, CLASS_B, CLASS_C, CLASS_D, CLASS_E, CLASS_F, CLASS_G, CLASS_H: // all classes
						{ // hands
							set_pev(id, pev_viewmodel2, V_HANDS);
							set_pev(id, pev_weaponmodel2, P_HANDS);
						}
					}
				}
				case WTYPE_KNIFE_CLASS: 
				{
					switch(getClass(id))
					{
						//case CLASS_A: {}
						case CLASS_B: { // Dagger
							set_pev(id, pev_viewmodel2, V_DAGGER);
							set_pev(id, pev_weaponmodel2, P_DAGGER);
						}
						case CLASS_C: { // Cestus
							set_pev(id, pev_viewmodel2, V_CESTUS);
							set_pev(id, pev_weaponmodel2, P_CESTUS);
						}
						case CLASS_D: { // Double-handed
							set_pev(id, pev_viewmodel2, V_DHANDED);
							set_pev(id, pev_weaponmodel2, P_DHANDED);
						}
						//case CLASS_E: {}
						case CLASS_F: { // Magic Staff
							set_pev(id, pev_viewmodel2, V_STAFF);
							set_pev(id, pev_weaponmodel2, P_STAFF);
						}
						case CLASS_G: { // Magic Sword
							set_pev(id, pev_viewmodel2, V_MAGICSWORD);
							set_pev(id, pev_weaponmodel2, P_MAGICSWORD);
						}
						case CLASS_H: { // Blunt
							set_pev(id, pev_viewmodel2, V_BLUNT);
							set_pev(id, pev_weaponmodel2, P_BLUNT);
						}
					}

					if(getClass(id) != CLASS_A)
						client_cmd(id, "spk %s", S_ITEMEQUIP[1]);
				}
				case WTYPE_KNIFE_OTHER: 
				{
					// event logic here
				}
			}

			Util_playAnimV(id, 3, false, true); // draw (hands or knife)
		}

		if(getClass(id) == CLASS_E)
		{
			if(cs_get_user_shield(id)) //if(equali(model, "models/shield/v_shield_knife.mdl"))
			{
				switch(randInt(1,4)){
					case 1: {
						set_pev(id, pev_viewmodel2, V_SHIELD_KNIFE_AV);
						set_pev(id, pev_weaponmodel2, P_SHIELD_KNIFE_AV);
					}
					case 2: {
						set_pev(id, pev_viewmodel2, V_SHIELD_KNIFE_DC);
						set_pev(id, pev_weaponmodel2, P_SHIELD_KNIFE_DC);
					}
					case 3: {
						set_pev(id, pev_viewmodel2, V_SHIELD_KNIFE_IC);
						set_pev(id, pev_weaponmodel2, P_SHIELD_KNIFE_IC);
					}
					case 4: {
						set_pev(id, pev_viewmodel2, V_SHIELD_KNIFE_NM);
						set_pev(id, pev_weaponmodel2, P_SHIELD_KNIFE_NM);
					}
				}
				
				//set_pev(read_data(2), pev_body, random_num(0,3));
				//new trash, weapon = get_user_weapon(id,trash,trash);
				//set_pev(weapon, pev_body, random_num(0,3));
				//set_pev(get_pdata_cbase(id, 373), pev_body, random_num(0,3)); // IT WORKS for V model!
				
				Util_playAnimV(id, 3, false, true); // deploy
				
				client_cmd(id, "spk %s", S_ITEMEQUIP[1]);
			}
		}
	}
}

public hamKnifePrimaryAttackPost(weapon)
{
	static attacker; attacker = ham_cs_get_weapon_ent_owner(weapon);

	if(warmupConfirmed())
	{
		// Set attack spd by classes
		static Float:primary, Float:secondary;
		new Float:g_pdata_primary = get_pdata_float(weapon, ID_NEXT_PRIMARY_ATK, OFFSET_LINUX_WEAPONS);
		new Float:g_pdata_secondary = get_pdata_float(weapon, ID_NEXT_SECONDARY_ATK, OFFSET_LINUX_WEAPONS);

		if(slowMotion){
			primary = g_pdata_primary * SLOWMOTION_ATK_SPEED;
			secondary = g_pdata_secondary * SLOWMOTION_ATK_SPEED;
		}
		else{
			switch(getClass(attacker))
			{
				case CLASS_A: {
					primary = g_pdata_primary * CLASS_ATK_SPEED_ARCHER;
					secondary = g_pdata_secondary * CLASS_ATK_SPEED_ARCHER;
				}
				case CLASS_B: {
					primary = g_pdata_primary * CLASS_ATK_SPEED_ROGUE;
					secondary = g_pdata_secondary * CLASS_ATK_SPEED_ROGUE;
				}
				case CLASS_C: 
				{
					if(PIN_FistFury[attacker]){
						primary = g_pdata_primary * SKILLS_FIST_FURY_ATK_SPEED;
						secondary = g_pdata_secondary * SKILLS_FIST_FURY_ATK_SPEED;
					}else{
						primary = g_pdata_primary * CLASS_ATK_SPEED_TYRANT;
						secondary = g_pdata_secondary * CLASS_ATK_SPEED_TYRANT;
					}
				}
				case CLASS_D: 
				{
					if(PIN_Zealot[attacker]){
						primary = g_pdata_primary * SKILLS_ZEALOT_ATK_SPEED;
						secondary = g_pdata_secondary * SKILLS_ZEALOT_ATK_SPEED;
					}else{
						primary = g_pdata_primary * CLASS_ATK_SPEED_RAIDER;
						secondary = g_pdata_secondary * CLASS_ATK_SPEED_RAIDER;
					}

					// slash damage (NOT TESTED)
					new Float:attacker_pos[3], Float:victim_pos[3];
					pev(attacker, pev_origin, attacker_pos);

					for(new Float:vReturn[3], hit_ent, victim=1; victim <= GMaxPlayers; victim++)
					{
						if(victim != attacker){
							if(isAlive(victim)){
								if(getTeam(victim) != getTeam(attacker))
								{
									pev(victim, pev_origin, victim_pos);
									if(get_distance_f(attacker_pos, victim_pos) <= 80.0) // distance: ~100.0 - 150.0
									{
										hit_ent = fm_trace_line(attacker, attacker_pos, victim_pos, vReturn);
										if(hit_ent == victim && fm_is_in_viewcone(attacker, victim_pos))
										{
											ExecuteHam(Ham_TakeDamage, victim, weapon, attacker, randFloat(CLASSDMG_D_MIN, CLASSDMG_D_MAX), DMG_SLASH);
											//return HAM_SUPERCEDE;
										}
									}
								}
							}
						}
					}
				}
				case CLASS_E: {
					primary = g_pdata_primary * CLASS_ATK_SPEED_KNIGHT;
					secondary = g_pdata_secondary * CLASS_ATK_SPEED_KNIGHT;
				}
				case CLASS_F: {
					primary = g_pdata_primary * CLASS_ATK_SPEED_WIZARD;
					secondary = g_pdata_secondary * CLASS_ATK_SPEED_WIZARD;
				}
				case CLASS_G: {
					primary = g_pdata_primary * CLASS_ATK_SPEED_PRIEST;
					secondary = g_pdata_secondary * CLASS_ATK_SPEED_PRIEST;
				}
				case CLASS_H: {
					primary = g_pdata_primary * CLASS_ATK_SPEED_SHAMAN;
					secondary = g_pdata_secondary * CLASS_ATK_SPEED_SHAMAN;
				}
			}
		}

		if(primary > 0.0 && secondary > 0.0){
			set_pdata_float(weapon, ID_NEXT_PRIMARY_ATK, primary, OFFSET_LINUX_WEAPONS);
			set_pdata_float(weapon, ID_NEXT_SECONDARY_ATK, secondary, OFFSET_LINUX_WEAPONS);
//			Log(DEBUG, "Core", "hamKnifePrimaryAttack(): attack speed changed");
		}

//		Log(DEBUG, "Core", "hamKnifePrimaryAttack(): primary=%.3f secondary=%.3f", primary, secondary);

		return HAM_SUPERCEDE;
	}
	
	return HAM_IGNORED;
}

/*
	that only works for real players!!!
	not for bots... u must register ham on the bot 1 time!
	and when the enemy have a armor, reduce the armor the damage 
	RegisterHamFromEntity(Ham_TakeDamage, id (the bot), "hook_TakeDamage")
*/
public hamTakeDamagePlayerPre(victim, inflictor, attacker, Float:damage, damagebits)
{
	if(/*attacker != inflictor ||*/ !isAlive(attacker) || !haveTeam(attacker))
		return HAM_IGNORED;

	//killassist_hamTakeDamagePlayer(victim, damage);

	if(warmupConfirmed())
	{
		// Team attack
		if(getTeam(attacker) == getTeam(victim))
		{
			if(!isDmGameMode()){
				SetHamParamFloat(4, 0.0);
				return HAM_HANDLED;
			}
		}

		// Last Hero checks
		if(GameMode == MODE_LAST_HERO) // 2 players take damage check
		{
			if(task_exists(TASK_LASTHERO_NO_WINNER))
			{
				if(isAlive(attacker) && isAlive(victim))
					remove_task(TASK_LASTHERO_NO_WINNER);
			}
		}

		new bool:isMagicAttack = false;
		new bool:isArrowAttack = false;
		new bool:critdmg = false;
		new target = victim; // who is result target (if damage reflected, etc)

		// Calculate damage
		if(damagebits & DMG_ARROW) // Arrows
		{
			Log(DEBUG, "Core", "DMG_ARROW");
			isArrowAttack = true;
			
			damage += getEnchantedWeaponDamage(attacker);
			
			if(randomChance(Config[CRITICAL_CHANCE_ARROW])){
				critdmg = true;
				damage *= randInt(2,5);
			}
		}
		else if(damagebits & DMG_MAGIC) // Magic damage
		{
			Log(DEBUG, "Core", "DMG_MAGIC");
			isMagicAttack = true;

			damage += getEnchantedWeaponDamage(attacker);
			
			if(randomChance(Config[CRITICAL_CHANCE_MAGIC]))
			{
				critdmg = true;
				switch(getClass(attacker)){
					case CLASS_A: {damage *= 2;}			/* Stun Shot, Double Shot, Lethal Shot */
					case CLASS_B: {critdmg = false;}		/* Backstab, Deadly Blow*/
					case CLASS_C: {damage *= randInt(2,3);}	/* Force Blaster, Soul Breaker */
					case CLASS_D: {damage *= randInt(2,3);}	/* Armor Crush */
					case CLASS_E: {damage *= randInt(2,3);}	/* Shield Stun */
					case CLASS_F: {damage *= randInt(6,10);}	/* Hurricane, Aura Flare */
					case CLASS_G: {critdmg = false;}
					case CLASS_H: {damage *= randInt(3,4);}	/* Steal Essence, Hammer Crush */
				}
			}
		}
		else if(damagebits & (DMG_BULLET | DMG_NEVERGIB)) // Melee damage
		{
			Log(DEBUG, "Core", "DMG_BULLET | DMG_NEVERGIB");
			
			Skills_inSleepCheck(target); // remove Sleep if player in

			// TODO: switch by weapon (hands/knife/etc)
			
			if(/*inflictor == attacker &&*/ get_user_weapon(attacker) == CSW_KNIFE)
			{
				switch(getClass(attacker))
				{
					case CLASS_A: {damage = randFloat(CLASSDMG_A_MIN, CLASSDMG_A_MAX);}
					case CLASS_B: {damage = randFloat(CLASSDMG_B_MIN, CLASSDMG_B_MAX);}
					case CLASS_C: {damage = randFloat(CLASSDMG_C_MIN, CLASSDMG_C_MAX);}
					case CLASS_D: {
						if(PIN_Frenzy[attacker]){
							damage = randFloat(SKILLDAMAGE_FRENZY_MIN, SKILLDAMAGE_FRENZY_MAX);
						}else{
							damage = randFloat(CLASSDMG_D_MIN, CLASSDMG_D_MAX);
						}
					}
					case CLASS_E: {damage = randFloat(CLASSDMG_E_MIN, CLASSDMG_E_MAX);}
					case CLASS_F: {damage = randFloat(CLASSDMG_F_MIN, CLASSDMG_F_MAX);}
					case CLASS_G: {damage = randFloat(CLASSDMG_G_MIN, CLASSDMG_G_MAX);}
					case CLASS_H: {damage = randFloat(CLASSDMG_H_MIN, CLASSDMG_H_MAX);}
				}

				/*
				// buttons check
				if(pev(attacker, pev_button) & IN_ATTACK)
				else if(pev(attacker, pev_button) & IN_ATTACK2)
				*/
			}

			damage += getEnchantedWeaponDamage(attacker);

			if(randomChance(Config[CRITICAL_CHANCE_MELEE])){
				critdmg = true;
				damage *= randInt(5,7); // multiply x5-x7
			}

			// Try to reflect melee damage
			if(Skills_reflectOrDamage(attacker, target, damage, false /*isStun*/, false /*isMagic*/)) 
			{
				SetHamParamEntity(1, attacker);
				target = attacker;
			}

			// L2 Hit (melee)
			if(randInt(1,10) > 3)
			{
				new origin[3];
				getUserOrigin(target, origin);

				if(critdmg) // TODO: use orig crit dmg sprite
					Create_TE_SPRITE(0, origin, SPR_L2HIT, 5, randInt(125,200));
				else{
					origin[2] += randInt(20,30); // to fix
					origin[1] += randInt(5,25);				
					Create_TE_SPRITE(0, origin, SPR_L2DAMAGE, randInt(2,5), randInt(15,150));
				}
			}
		}

		// Transfer Pain
		if(getClass(target) == CLASS_F && haveSummon(target))
		{
			damage /= 2.0;
			// Transfer half damage to summoned unit
			// TODO: NO CRIT CHANCE for this dmg
			takeDamage(attacker, getSummonedUnit(target), damage);
			showSkillHud(victim, "Returned %d dmg to summon", floatround(damage));

			Log(DEBUG, "Core", "TakeDmg(): haveSummon(): dmg=%d", floatround(damage));
		}

		// 1st person damage effects to target
		if(!isBot(target)){
			Create_ScreenShake(target, 255<<14, 2500, 255<<14, true);
			Create_ScreenFade(target, 25, 25, 9, randInt(60,200), 0, 0, randInt(60,200), true);
		}

		// L2 Soulshots @WAIT rework only for admins use
		/*
		new origin[3];
		get_user_origin(victim, origin);
		if(random_num(1,2)==1)
			Create_TE_SPRITE(0, origin, SPR_KMST_SS01, random_num(6,8), random_num(50,150));
		else
			Create_TE_SPRITE(0, origin, SPR_KMST_SS02, random_num(4,6), random_num(15,100));
		*/

		//Log(DEBUG, "Core", "takeDamagePre(): fin dmg = %.1f", damage)
		// Final damage set
		SetHamParamFloat(4, damage);

		if(critdmg)
		{
			showSkillHud(attacker, "Critical hit!");
			
			if(!isMagicAttack){
				emit_sound(target, CHAN_VOICE, randInt(1,2)==1 ? S_CRITICAL_HIT : S_CRITICAL_HIT2, randFloat(0.75,1.0), ATTN_STATIC, 0, randInt(95,110) /*PITCH*/);
			}
			
			if(isArrowAttack){
				new origin[3];
				getUserOrigin(target, origin);
				Create_TE_SPRITE(0, origin, SPR_L2HIT, 5, randInt(125,200)); // @WAIT l2 crit ori spr
				if(randomChance(30)){
					Log(DEBUG, "Core", "takeDmg(): isArrowAttack ==> hitKnockback")
					new Float:vec[3], Float:oldvelo[3];
					get_user_velocity(target, oldvelo);
					createVelocityVector(target, attacker, vec); //<Stocks>
					vec[0] += oldvelo[0];
					vec[1] += oldvelo[1];
					set_user_velocity(target, vec);
				}
			}
		}
		else
			showSkillHud(attacker, "You hit for %d damage", floatround(damage));
		
		// Also, if you're changing a parameter you have to return HAM_HANDLED. Otherwise it will be ignored by Ham.
		return HAM_HANDLED;
	}
	return HAM_IGNORED;
}

// All moved to TakeDamagePre, DELETE after CPL
/*
public hamTraceAttackPlayerPre(victim, attacker, Float:damage, Float:direction[3], trace, damagebits) 
{
	if(victim == attacker || !victim || !isConnected(attacker))
		return HAM_IGNORED;

	if(warmupConfirmed())
	{
		if(Skills_reflectOrDamage(attacker, victim, damage, false, false)) 
		{ // make damage or try to Reflect Damage
			SetHamParamEntity(1, attacker);
		}

		if(task_exists(victim+TASK_SKILLS_SLEEPED) && PIN_Sleep[victim])
		{ // If player in Sleep ==> remove Sleep effect
			removeSleep(victim+TASK_SKILLS_SLEEPED);
		}
	}
	
	return HAM_IGNORED;
}*/

public hamObjectCaps(id)
{
	if(!isConnected(id) || !isAlive(id) || !haveTeam(id))
		return HAM_IGNORED;

	if(get_pdata_int(id, 246, 5) & IN_USE) // if USE pressed
	{
		static target, body;
		get_user_aiming(id, target, body, 75);
		if(target)
		{
			static this[32];
			pev(target, pev_classname, this, 31);
			if(isGatekeeper(this)){
				Npc_gatekeeperDialog(target, id);
			}
		}
	}
	//if(get_pdata_int(id, 247, 5) & IN_USE) // if USE released
	
	return HAM_IGNORED;
}

public hamTakeDamageEntity(victim, inflicator, attacker, Float:damage, damage_type)
{
	if(!isConnected(attacker) || !victim)
		return HAM_IGNORED;

	if(warmupConfirmed())
	{
		new this[32];
		getEntClass(victim, this, 31);

		Log(DEBUG, "Core", "hamTakeDamageEntity(): ObjId=%d (%s)", victim, this);

		if(isGuard(this))
		{
			Guard_takeDamage(victim, inflicator, attacker, damage, damage_type);
		}
		else if(isMonster(this))
		{
			Monster_takeDamage(attacker, victim, getMonId(this) /*<Monster>*/);
		}
		else if(isBuffer(this))
		{ // FMagic
			FMagic_takeDamage(victim, inflicator, attacker, Float:damage, damage_type);
			if(randInt(1,2)==1) //&copy Clarissa
				emit_sound(victim, CHAN_VOICE, S_NPC_TELEPORTER[randVal(2)], randFloat(0.7,1.0), ATTN_NORM, 0, PITCH_NORM);
		}
		else if(isGatekeeper(this))
		{ // Clarissa
			if(randInt(1,2)==1)
				emit_sound(victim, CHAN_VOICE, S_NPC_TELEPORTER[randVal(2)], randFloat(0.7,1.0), ATTN_NORM, 0, PITCH_NORM);
		}
		else if(isPet(this))
		{
			Summon_takeDamage(victim, attacker, getPetId(this));
		}
		else if(isItem(this))
		{ // Adena, Etc, Potions
			if(isEntValid(victim))
				entityDispose(victim);
		}
		else if(isAvanpost(this) && GameMode == MODE_OUTPOST_DEFENSE)
		{
			emit_sound(victim, CHAN_VOICE, S_ARMOR_LEATHER[randInt(0,7)], randFloat(0.5,0.9), ATTN_NORM, 0, PITCH_NORM);

			if(equali(this, "L2Avanpost@Red") && getTeam(attacker) != TEAMID_EAST){
				Avanpost_takeDamage(victim, attacker, TEAMID_EAST);
				Stats_addAvanpostDmg(attacker, damage);
			}
			else if(equali(this, "L2Avanpost@Blu") && getTeam(attacker) != TEAMID_WEST){
				Avanpost_takeDamage(victim, attacker, TEAMID_WEST);
				Stats_addAvanpostDmg(attacker, damage);
			}
			else{
				//Log(DEBUG, "Core", "Avanpost team attack by %s", getName(attacker));
				SetHamParamFloat(4, 0.0);
				return HAM_HANDLED;
			}
		}
	}
	
	return HAM_IGNORED;
}

public hamTraceAttackEntity(ent, attacker, Float:damage, Float:direction[3], trace, damage_type)
{
	if(!isEntValid(ent))
		return HAM_IGNORED;

	if(warmupConfirmed())
	{
		new this[32];
		pev(ent, pev_classname, this, 31);

		if(isMonster(this) || isGuard(this) || isBuffer(this) || isGatekeeper(this))
		{
			// L2 Hit &copy hamTakeDamagePlayerPre()
			if(randInt(1,4)==1)
			{
				new origin[3], Float:forigin[3];
				pev(ent, pev_origin, forigin);
				FVecIVec(Float:forigin, origin);
				origin[2] += randInt(20,30); // to fix
				origin[1] += randInt(5,25);
				Create_TE_SPRITE(0, origin, SPR_L2DAMAGE, randInt(2,5), randInt(15,150));
			}

			//new Float:end[3];
			//get_tr2(trace, TR_vecEndPos, end);
			//Create_TE_BLOODSPRITE(end, SPR_BLOOD, SPR_BLOOD, 248, randInt(3,8)); //color=247, scale=randInt(5,10)
			//Create_TE_BLOODSPRITE(Float:end[3], bloodspray, blooddrop, color, scale)			
		}
	}
	
	return HAM_IGNORED;
}

public hamKilledEntity(victim, killer, shouldgib) // info_target
{
	if(warmupConfirmed())
	{
		static this[32];
		pev(victim, pev_classname, this, 31);

		if(isGuard(this))
		{
			Guard_killed(victim);
		}
		else if(isMonster(this))
		{
			Monster_killed(victim, killer, getMonId(this));
		}
		else if(isBuffer(this))
		{
			FMagic_killed(victim, killer);
		}
		else if(isPet(this))
		{
			Summon_killed(getEntOwner(victim)); // kill Corrupted Man
		}
		else if(isAvanpost(this))
		{
			Avanpost_destroyed(victim, killer);
		}
	}

	return HAM_SUPERCEDE;
}

/**
 * This function anyway calls 2 times by the engine.
 */
public hamPlayerSpawnPost(id)
{
	//Log(DEBUG, "Core", "hamPlayerSpawnPost(): %s", getName(id));
	Util_removeWeapons(id);
	
	if(isAlive(id) && !warmupConfirmed()) // bugfix: joins in warmup
		user_silentkill(id);
	
	if(warmupConfirmed())
	{
		if(isAlive(id) && haveTeam(id) && !RespawnBlocked[id] /* LastHero protect */)
		{
			// LastHero: spawn points
			if(GameMode == MODE_LAST_HERO && !EventStarted)
			{
				if(LastHero_dontMakeSpawns())
				{
					// nothing
				}
				else if(IsEventReady && LastHero_getSpawnsNum() > 0)
				{
					Log(DEBUG, "Core", "hamPlayerSpawnPost(): Total LastHero spawns: %d", LastHero_getSpawnsNum());
					
					new bool:rightSpawn = false;
					
					for(new ex = 1; ex <= LastHero_getSpawnsNum(); ex++)
					{
						if(isHullVacant(LH_SpawnPoint[ex]))
						{
							engfunc(EngFunc_SetOrigin, id, LH_SpawnPoint[ex]);
							Util_setEntityAngles(id, LH_CenterPoint);
							Log(DEBUG, "Core", "hamPlayerSpawnPost(): Point %d free -> '%s' spawned", ex, getName(id));
							rightSpawn = true;
							break;
						}
						else{
							Log(DEBUG, "Core", "hamPlayerSpawnPost(): Point %d is busy -> try next", ex);
							continue;
						}
					}
					
					if(!rightSpawn){
						Log(WARN, "Core", "hamPlayerSpawnPost(): Couldn't find any free spawn points!");
						Log(WARN, "Core", "hamPlayerSpawnPost(): '%s' not spawned");
						user_silentkill(id);
						return HAM_IGNORED; // dont spawn this player (spawn block)
					}
				}
				else{
					Log(ERROR, "Core", "hamPlayerSpawnPost(): Last Hero spawns not assigned!");
					Log(ERROR, "Core", "hamPlayerSpawnPost(): %s game mode aborted", GameModeString);
					
					user_silentkill(id);
					return HAM_IGNORED; // dont spawn this player (spawn block)
				}
			}

			if(!task_exists(id+TASK_PLAYER_SPAWNED_POST))
				set_task(randFloat(0.1,0.6), "playerSpawnedPost", id+TASK_PLAYER_SPAWNED_POST);

			//killassist_hamPlayerSpawn(id);
		}
		else
			user_silentkill(id);
	}

	return HAM_IGNORED;
}

// TODO: rename func
public playerSpawnedPost(id) // taskid
{
	id -= TASK_PLAYER_SPAWNED_POST;

//	Log(DEBUG, "Core", "playerSpawnedPost(): %s", getName(id));

	if(warmupConfirmed())
	{
		if(GameMode == MODE_LAST_HERO && !task_exists(id+TASK_PLAYER_SIT_IDLE)){
			Player_sit(id);
		}

		if(!isBot(id))
		{
			/*if(FONT_DECO_ENABLED && FontDecoShow[id] && !task_exists(id+TASK_L2_FONT_DECO)){
				set_task(3.8, "fontDecoHide", id+TASK_L2_FONT_DECO);
			}*/

			Util_hideRadar(id);
//			Util_hideTimer(id);

			if(haveClass(id)){
				client_cmd(id, "exec skills_%s.cfg", getClassName(id));
				//Log(DEBUG, "Core", "ClassMenuHandler(): id=%d exec skills_%s.cfg", id, getClassName(id));
			}else{
				Log(WARN, "Core", "Player '%s' spawned without class!", getName(id));
			}

			setAdena(id, Stats_getEarnedAdena(id));

			if(GameMode == MODE_OUTPOST_DEFENSE || GameMode == MODE_TEAM_VS_TEAM){
				Camera_unsetDeathCam(id);
			}
		}

		if(!haveClass(id)){
			setClassRequest(id, randInt(MIN_CLASS_ID, MAX_CLASS_ID));
			Log(DEBUG, "Core", "playerSpawnedPost(): Forced class %s for player '%s'", getClassName(id), getName(id));
		}
		
		if(PlayerFirstSpawn[id]){
			setHp(id, MAX_PLAYER_HEALTH);
			PlayerFirstSpawn[id] = false;
		}else{
			setHp(id, 180); // 70%(255)=178.5
		}

		giveEquip(id);
		setWeaponEnchant(id);

		set_task(randFloat(0.2,0.6), "Accessory_attach", id+TASK_ATTACH_ACCESSORY);
	}
}

public giveEquip(id)
{
	switch(getClass(id)){
		case CLASS_A: {	fm_give_item(id, "weapon_knife");	fm_give_item(id, "weapon_mp5navy");	}
		case CLASS_B: {	fm_give_item(id, "weapon_knife");										}
		case CLASS_C: {	fm_give_item(id, "weapon_knife");										}
		case CLASS_D: {	fm_give_item(id, "weapon_knife");										}
		case CLASS_E: {	fm_give_item(id, "weapon_knife");	fm_give_item(id, "weapon_shield");	}
		case CLASS_F: {	fm_give_item(id, "weapon_knife");										}
		case CLASS_G: {	fm_give_item(id, "weapon_knife");										}
		case CLASS_H: {	fm_give_item(id, "weapon_knife");										}
	}
}

public setWeaponEnchant(id)
{
	if(PlayerWeapEncEnt[id] <= 0)
	{
		static g_cache;
		if((g_cache || (g_cache = engfunc(EngFunc_AllocString, "env_sprite"))) && isEntValid((PlayerWeapEncEnt[id] = engfunc(EngFunc_CreateNamedEntity, g_cache))))
		{
			// TODO: switch wide or not wide
			engfunc(EngFunc_SetModel, PlayerWeapEncEnt[id], SPR_LIGHT64);

			// $attachment NUMBER - from *qc
			// index from 1 but in *qc from 0
			switch(getClass(id))
			{
				// TODO: устанавливать эти параметры уже после создания энтити (после смерти сейчас не меняет на другой)
				case CLASS_A: set_pev(PlayerWeapEncEnt[id], pev_body, 2); // bow(fist-L)
				case CLASS_B: set_pev(PlayerWeapEncEnt[id], pev_body, 1); // dagger
				case CLASS_C: set_pev(PlayerWeapEncEnt[id], pev_body, 1); //2 // fist-L
				case CLASS_D: set_pev(PlayerWeapEncEnt[id], pev_body, 3); // dhanded  
				case CLASS_E: set_pev(PlayerWeapEncEnt[id], pev_body, 4); //3 // sword(dhanded)
				case CLASS_F: set_pev(PlayerWeapEncEnt[id], pev_body, 4); // staff
				case CLASS_G: set_pev(PlayerWeapEncEnt[id], pev_body, 3); // msword(dhanded)
				case CLASS_H: set_pev(PlayerWeapEncEnt[id], pev_body, 1); //3 // blunt(dhanded)
				
				/*case CLASS_A: set_pev(PlayerWeapEncEnt[id], pev_body, 3);
				case CLASS_B: set_pev(PlayerWeapEncEnt[id], pev_body, 4);
				case CLASS_C: set_pev(PlayerWeapEncEnt[id], pev_body, 5); //6	// && left hand TOO CREATE 2 ent 
				case CLASS_D: set_pev(PlayerWeapEncEnt[id], pev_body, 7);
				case CLASS_E: set_pev(PlayerWeapEncEnt[id], pev_body, 8);
				case CLASS_F: set_pev(PlayerWeapEncEnt[id], pev_body, 9);
				case CLASS_G: set_pev(PlayerWeapEncEnt[id], pev_body, 10);
				case CLASS_H: set_pev(PlayerWeapEncEnt[id], pev_body, 11);*/
				
				//set_pev(PlayerWeapEncEnt[id], pev_body, 1); // 1 - rhand, 2 - lhand, 3+ body(if not exists attach)
			}

			set_pev(PlayerWeapEncEnt[id], pev_movetype, MOVETYPE_FOLLOW);
			set_pev(PlayerWeapEncEnt[id], pev_aiment, id);
			set_pev(PlayerWeapEncEnt[id], pev_skin, id | 1 << 7);
			set_pev(PlayerWeapEncEnt[id], pev_scale, 0.4);
			set_pev(PlayerWeapEncEnt[id], pev_solid, SOLID_NOT);

			dllfunc(DLLFunc_Spawn, PlayerWeapEncEnt[id]);
		}
	}

	if(PlayerWeapEncEnt[id] > 0)
	{
		// not for hands
		if(PlayerWeapon[id][WTYPE] == WTYPE_HANDS || !isAlive(id) /* if player dead */)
		{
			fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,0,0, kRenderTransAdd, 0);
			return;
		}

		// Enchant levels
		switch(PlayerWeaponEnchant[id][getClass(id)]) // 0-16
		{
			// L2 clear
			case 0:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,0,0, kRenderTransAdd, 0);
			case 1:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,0,0, kRenderTransAdd, 0);
			case 2:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,0,0, kRenderTransAdd, 0);
			case 3:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,0,0, kRenderTransAdd, 0);
			case 4:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 30,30,40, kRenderTransAdd, 60);
			case 5:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 35,45,60, kRenderTransAdd, 70);
			case 6:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 35,65,90, kRenderTransAdd, 90);
			case 7:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 40,87,126, kRenderTransAdd, 90);
			case 8:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 30,80,135, kRenderTransAdd, 100);
			case 9:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 20,70,145, kRenderTransAdd, 110);
			case 10:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 10,60,160, kRenderTransAdd, 120);
			case 11:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,50,180, kRenderTransAdd, 130);
			case 12:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,40,200, kRenderTransAdd, 140);
			case 13:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,30,220, kRenderTransAdd, 145);
			case 14:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,30,240, kRenderTransAdd, 150);
			case 15:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,30,255, kRenderTransAdd, 155);
			case 16:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 160,20,20, kRenderTransAdd, 185);
			case 17:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 180,10,10, kRenderTransAdd, 190);
			case 18:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 200,0,0, kRenderTransAdd, 200);
			case 19:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 210,0,0, kRenderTransAdd, 210);
			case 20:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 220,0,0, kRenderTransAdd, 220);
			case 21..99:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 220,0,0, kRenderTransAdd, 230);
			/*
			// C4 Trifle colors
			case 0:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,0,0, kRenderTransAdd, 0);					// NULL
			case 1:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,30,255, kRenderTransAdd, 180);				// blue
			case 2:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 220,0,0, kRenderTransAdd, 180);				// red
			case 3:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 0,220,0, kRenderTransAdd, 180);				// green
			case 4:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 255,235,0, kRenderTransAdd, 180);				// yellow
			case 5:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 128,0,255, kRenderTransAdd, 180);				// purple
			case 6:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxNone, 255,128,64, kRenderTransAdd, 180);			// orange
			case 7:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlow, 255,255,255, kRenderTransAdd, 180);		// white
			case 8:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlow, 255,50,255, kRenderTransAdd, 180);		// pink
			case 9:		fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlow, 0,255,255, kRenderTransAdd, 180);		// cyan
			case 10:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlow, 111,255,111, kRenderTransAdd, 180);		// light-green
			case 11:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlowWide, 0,30,255, kRenderTransAdd, 200);		// blue wide
			case 12:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlowWide, 220,0,0, kRenderTransAdd, 200);		// red wide
			case 13:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlowWide, 0,220,0, kRenderTransAdd, 200);		// green wide
			case 14:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlowWide, 255,235,0, kRenderTransAdd, 200);	// yellow wide
			case 15:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlowWide, 128,0,255, kRenderTransAdd, 200);	// purple wide
			case 16:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlowWide, 255,128,64, kRenderTransAdd, 200);	// orange wide
			case 17:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlowWide, 255,255,255, kRenderTransAdd, 200);	// white wide
			case 18:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlowWide, 255,50,255, kRenderTransAdd, 200);	// pink wide
			case 19:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlowWide, 0,255,255, kRenderTransAdd, 200);	// cyan wide
			case 20:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxPulseSlowWide, 111,255,111, kRenderTransAdd, 200);	// light-green wide
			case 21..99:	fm_set_rendering(PlayerWeapEncEnt[id], kRenderFxHologram, 0,139,139, kRenderTransAdd, 180);		// cyan (dark) holo
			*/
			//kRenderFxHologram
		}
	}
}

public removeWeaponEnchant(id)
{
	if(PlayerWeapEncEnt[id] > 0 && isEntValid(PlayerWeapEncEnt[id])) // <Const>
		entityDispose(PlayerWeapEncEnt[id]);
	PlayerWeapEncEnt[id] = 0;
}

public getEnchantedWeaponDamage(id)
{
	if(PlayerWeaponEnchant[id][getClass(id)] <= 0)
		return 0;

	new dmg = 0;

	switch(PlayerWeaponEnchant[id][getClass(id)])
	{
		case 0:	dmg = randInt(1,5);
		case 1: dmg = randInt(2,6);
		case 2: dmg = randInt(3,7);
		case 3: dmg = randInt(4,8);
		case 4: dmg = randInt(5,9);
		case 5: dmg = randInt(6,10);
		case 6: dmg = randInt(7,11);
		case 7: dmg = randInt(8,12);
		case 8: dmg = randInt(9,13);
		case 9: dmg = randInt(10,14);
		case 10: dmg = randInt(11,15);
		case 11: dmg = randInt(12,16);
		case 12: dmg = randInt(13,17);
		case 13: dmg = randInt(14,18);
		case 14: dmg = randInt(15,19);
		case 15: dmg = randInt(16,20);
		case 16: dmg = randInt(17,21);
		case 17: dmg = randInt(18,22);
		case 18: dmg = randInt(19,23);
		case 19: dmg = randInt(20,24);
		case 20: dmg = randInt(21,25);
		case 21..99: dmg = randInt(22,26);
	}

	return dmg;
}

public hamPlayerKilledPre(victim, killer, gib)
{
	if(!isConnected(victim))
		return HAM_IGNORED;

	if(warmupConfirmed())
	{
		// Rotate head on death (fall-like effect)
		new Float:vec[3];
		vec[0] = randFloat(-180.0,180.0); //(-360.0, 360.0);
		vec[1] = randFloat(-180.0,180.0); //(-360.0, 360.0);
		vec[2] = randFloat(-180.0,180.0); //(-360.0, 360.0);
		setPunchAngle(victim, vec);

		if(SetClassReq[victim]){ // Change class request
			setClassRequest(victim, SetClassReqInd[victim]);
			SetClassReq[victim] = false;
		}

		//Skills_updateSkillIcons(victim); // Hide skill icons on death @WAIT
		Skills_hamPlayerKilledPre(victim); // Reset effects on death
		
		//markDeathXYZ(victim); // <Respawn> headquarters respawn @WAIT

		if(isTeamGameMode()){ // Respawn player
			Respawn_hamPlayerKilledPre(victim, killer);
		}

		/*
			Чем больше "прокачен" ваш враг, тем больше адены вы получите за его убийство. Влияют такие 
		параметры: грейд оружия, брони, бижутерии, уровень заточки предметов, пвп, геройство и 
		прочее. К примеру за убийство новичка в Б грейде награда составит 12 аден, а за топовых 
		игроков награда может составлять до 100 аден.
			На сервере установлена система анти-накрутки адены (если вы создадите твинка или попросите 
		друзей поумирать от вас ради заработка адены - врятли у вас что то получится).
		Если за последние 15 секунд перед смертью чар был атакован двумя или более игроками, то 
		награда распределится так: последнему ударившему 60% от награды, предпоследнему ударившему 40% от награды.
		*/
		// Give adena to killer
		if(isConnected(killer) && victim != killer && (getTeam(victim) != getTeam(killer)))
		{
			// TEST & REMOVE
			/*
			new default_kills = randInt(1,30);
			new headshot_kills = randInt(30,55);
			new team_kills = 0;
			GNewAdena[killer] = clamp(cs_get_user_money(killer) + (cs_get_user_team(victim) == cs_get_user_team(killer) ? team_kills : (gib ? headshot_kills : default_kills)), 0, ADENA_MAX_AMOUNT);
			*/
			
			if(!isBot(killer))
				Log(DEBUG, "Core", "takeDmg(): same IP = %s", haveSameIp(killer, victim) ? "true" : "false");

			if(!Config[SAME_IP_KILLS_ENABLED] && !haveSameIp(killer, victim))
			{
				new adena_add = 0;

				switch(getLevel(victim)){
					case 1..10:		adena_add += randInt(1,10);
					case 11..20:	adena_add += randInt(11,20);
					case 21..30:	adena_add += randInt(21,30);
					case 31..40:	adena_add += randInt(31,40);
					case 41..50:	adena_add += randInt(41,50);
					case 51..60:	adena_add += randInt(51,60);
					case 61..70:	adena_add += randInt(61,70);
					case 71..80:	adena_add += randInt(71,80);
				}

				switch(getWeaponEnchant(victim)){
					case 1..4:		adena_add += randInt(1,4);
					case 5..7:		adena_add += randInt(5,7);
					case 8..11:		adena_add += randInt(6,11);
					case 12..14:	adena_add += randInt(8,14);
					case 15..17: 	adena_add += randInt(10,17);
					case 18..20:	adena_add += randInt(12,18);
					case 21..99:	adena_add += randInt(14,20);
				}
				
				new killer_adena = getAdena(killer);
				
				if(killer_adena + adena_add <= ADENA_MAX_AMOUNT)
				{
					GNewAdena[killer] = killer_adena + adena_add;

					Stats_addToTotalAdena(killer, adena_add);
					Stats_setEarnedAdena(killer, GNewAdena[killer]);

					MsgHookMoney = register_message(gmsgMoney, "msgMoney");
				}else{
					sendMessage(killer, print_center, "You reached maximum Adena.");
				}
			}
		}
	}

	return HAM_IGNORED;
}

public hamPlayerKilledPost(victim, killer, gib)
{
	if(!isConnected(victim))
		return HAM_IGNORED;
	/*if(!killer || killer > GMaxPlayers || victim == killer)
		return HAM_IGNORED;*/

	if(warmupConfirmed())
	{
		// Hide weapon enchant on death
		setWeaponEnchant(victim);

		// Death sound by race
		switch(PlayerData[victim][RACE])
		{
			case RACE_HUMAN: {
				emit_sound(victim, CHAN_VOICE, S_DEATH[0], randFloat(0.8,1.0), ATTN_NORM, 0, PITCH_NORM); // human fighter
			}
			case RACE_ELF: {
				if(random_num(1,2)==1)
					emit_sound(victim, CHAN_VOICE, S_DEATH[2], randFloat(0.8,1.0), ATTN_NORM, 0, PITCH_NORM); // elf
				else
					emit_sound(victim, CHAN_VOICE, S_DEATH[1], randFloat(0.8,1.0), ATTN_NORM, 0, PITCH_NORM); // human mage
			}
			case RACE_DARKELF: {
				emit_sound(victim, CHAN_VOICE, S_DEATH[3], randFloat(0.8,1.0), ATTN_NORM, 0, PITCH_NORM); // dark elf
			}
			case RACE_ORC: {
				emit_sound(victim, CHAN_VOICE, S_DEATH[4], randFloat(0.8,1.0), ATTN_NORM, 0, PITCH_NORM); // dwarf
			}
		}

		UponDeath_call(victim, killer, gib);
		Say2_hamPlayerKilledPost(victim, killer, gib); // clear Say2 variables

		SlowMo_start();
		
		// Kill streak HUD message
		KillStreak_evDeathMsg(killer, victim, gib);

		// 1st person death effects
		if(!isBot(victim)){
			Create_ScreenFade(victim, 2<<12, 1<<11, 0, randInt(60, 255),0,0, randInt(60, 255), true);
			Create_ScreenShake(victim, 255<<14, 2500, 255<<14, true);

			/*	iuser1 - (для спектаторов в CS) номер типа режима наблюдения: 
				1 - Locked Chase Cam, 2 - Free Chase Cam, 3 - Free Look, 4 - First Person, 5 - Free Overview, 6 - Chase Overview	*/
			set_pev(victim, pev_iuser1, 0); //SPECMODE_ALIVE

			if(GameMode == MODE_OUTPOST_DEFENSE || GameMode == MODE_TEAM_VS_TEAM)
				set_task(2.0, "Camera_setDeathCam", victim+TASK_SET_DEATH_CAM);
		}

		// TODO: review & bugfix
		if(Config[REMOVE_EXP_ON_DEATH]) // <Const>
		{
			Log(DEBUG, "Core", "Config[REMOVE_EXP_ON_DEATH]=true");
			// Remove exp (-4%) from victim if level > 4
			if(getExp(victim) >= 4 /*_LEVELS[4][TOTAL_EXP]*/ && getLevel(victim) > 4 /*&& killer != victim*/)
			{
				setExp(victim, getExp(victim) - 4); //(4*RATES_MULTIPLIER));
				// BUGFIX: if (exp <= prev_level_exp) setLevel(id, level-1)
				showExpHud(victim, "-4 exp");

				Player_classUpdate(victim);
				//bugfix: checkLevel DECREASE
			}
		}

		// 
		// STATS 
		// 
		Stats_addDeath(victim);
		
		// LastHero team kill bugfix @MOVED TO <Stats>	<-- NEED TEST
		/*else if(isDmGameMode())
		{
			Log(ERROR, "Core", "hamPlayerKilledPre(): isDmGameMode(): set user frags");
			new val = get_user_frags(killer) + 2;
			set_user_frags(killer, val);
			Stats_updateScoreBoard(killer, val, get_user_deaths(killer));
		}*/
		
		if(isConnected(killer) && killer != victim && haveTeam(killer))
		{
			if(isTeamGameMode() && getTeam(killer) == getTeam(victim))
				Stats_addPK(killer);
			else
				Stats_addPvP(killer);
			
			// Calculate experience/level for killer
			
			//if(!SAY2_DATA[killer][S2_EXP_GAIN])
			//{

			new reward = calculateRewards(killer, false, haveSummon(killer)); // <Player>
			if(reward){
				showExpHud(killer, "+%d exp", reward);
			}

			//Player_checkLevel(killer);
			//}
		}

		// LastHero: move killed player to spectators
		if(GameMode == MODE_LAST_HERO){
			cs_set_user_team(victim, CS_TEAM_SPECTATOR);
			// TODO: delay ~ 3-10s
		}
		else{
			// bugfix: set to basic weapon before next spawn
			switch(getClass(victim))
			{
				case CLASS_A: { PlayerWeapon[victim][WTYPE] = WTYPE_BOW; }
				case CLASS_B .. CLASS_H: {
					PlayerWeapon[victim][WTYPE] = WTYPE_KNIFE_CLASS;
				}
			}
		}

	}

	return HAM_IGNORED;
}

// --------------------------------------------------
// Message hooks
// --------------------------------------------------
public msgClCorpse() // a corpse is to be set, stop player shells bug (thanks sawce)
{
	// If there is not 12 args something is wrong
	if(get_msg_args() < 12)
		return PLUGIN_CONTINUE;

	return PLUGIN_HANDLED;

	// Arg 12 is the player id the corpse is for
	//new id = get_msg_arg_int(12)
	// If the corpse should be hidden block this message
	//if (body_hidden[id]) return PLUGIN_HANDLED
}

public msgTempEntity()
{
	static arg1; arg1 = get_msg_arg_int(1);

	if(arg1 == TE_BLOODSPRITE 
	|| arg1 == TE_BLOODSTREAM 
	|| arg1 == TE_BLOOD
	|| arg1 == TE_DECAL
	|| arg1 == TE_WORLDDECAL
	|| arg1 == TE_WORLDDECALHIGH
	|| arg1 == TE_DECALHIGH)
		return PLUGIN_HANDLED;

	return PLUGIN_CONTINUE;
}

public msgHideWeapon()
{
	set_msg_arg_int(1, ARG_BYTE, get_msg_arg_int(1) | (1<<4)); // hide timer
}

public msgMoney(msg_id, msg_dest, id)
{
	unregister_message(gmsgMoney, MsgHookMoney);
	cs_set_money_value(id, GNewAdena[id]); /* <Const> bit-func */
	set_msg_arg_int(1, ARG_LONG, GNewAdena[id]);
}

// --------------------------------------------------
// Forwards
// --------------------------------------------------
public fwServerDown()
{
	Log(WARN, "Core", "fwServerDown(): Server is shutting down NOW!");
	if(warmupConfirmed()){
		dbAutoSaveInstant();	/* <Database> */	// Save all players data
		updateOnlineStatus(0);	/* <Database> */	// Clear `online` table
	}
}

public fwCmdStart(id, uc_handle, random_seed)
{
	static CurButton, OldButton;
	CurButton = get_uc(uc_handle, UC_Buttons);

	// Alt+T sound
	OldButton = (pev(id, pev_oldbuttons) & IN_SCORE);
	
	if(CurButton & IN_SCORE && !OldButton){ // press
		client_cmd(id, "spk %s", CHARSTAT_OPEN);
	}
	/*
	if(CurButton & IN_SCORE && OldButton){} // hold
	*/
	if(!(CurButton & IN_SCORE) && OldButton){ // release
		client_cmd(id, "spk %s", CHARSTAT_CLOSE);
	}
	
	if(!isAlive(id))
		return FMRES_IGNORED;
	
	// Check blockers
	if(isDuckJumpBlocked(id))
	{
		if((CurButton & IN_DUCK) && (entity_get_int(id, EV_INT_flags) & FL_ONGROUND))
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_DUCK);
		if((CurButton & IN_JUMP) && (entity_get_int(id, EV_INT_flags) & FL_ONGROUND))
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_JUMP);
	}
	if(isMoveBlocked(id))
	{
		if(CurButton & IN_FORWARD)
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_FORWARD);
		if(CurButton & IN_BACK)
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_BACK);
		if(CurButton & IN_LEFT)
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_LEFT);
		if(CurButton & IN_RIGHT)
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_RIGHT);
		if(CurButton & IN_MOVELEFT)
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_MOVELEFT);
		if(CurButton & IN_MOVERIGHT)
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_MOVERIGHT);
		if(CurButton & IN_RUN)
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_RUN);
		if(CurButton & IN_ALT1)
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_ALT1);
	}
	if(isAttackBlocked(id))
	{
		if(CurButton & IN_ATTACK)
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_ATTACK);
		if(CurButton & IN_ATTACK2)
			set_uc(uc_handle, UC_Buttons, CurButton & ~IN_ATTACK2);
	}
	
	// Bow shot
	if(!isAttackBlocked(id) && getClass(id) == CLASS_A)
	{
		OldButton = (pev(id, pev_oldbuttons) & IN_ATTACK);
		
		if(get_gametime() - CD_BowShot[id] > CLASS_ATK_DELAY_ARCHER)
		{
		// Press (Atk1)
			if(CurButton & IN_ATTACK && !OldButton) 
			{
				if(get_user_weapon(id) != CSW_MP5NAVY) // TODO: rework with func playerHasBow(id)
					return FMRES_IGNORED;
				
				Atk1BtnPressed[id] = true;
				emit_sound(id, CHAN_WEAPON, S_BOW_STRING, 0.8, ATTN_NORM, 0, PITCH_NORM);
			}
		// Hold
			if(CurButton & IN_ATTACK && OldButton)
			{
				if(!Atk1BtnPressed[id]) 
					return FMRES_IGNORED;
				
				if(get_user_weapon(id) != CSW_MP5NAVY)
				{
					CD_BowShot[id] = get_gametime();
					Atk1BtnPressed[id] = false;
				}
			}
		// Release
			if(!(CurButton & IN_ATTACK) && OldButton) 
			{
				if(get_user_weapon(id) != CSW_MP5NAVY) 
					return FMRES_IGNORED;
				if(!Atk1BtnPressed[id]) 
					return FMRES_IGNORED;
				
				CD_BowShot[id] = get_gametime();
				makeBowShot(id);
				Atk1BtnPressed[id] = false;
				ShotPowerAmt[id] = 0;
			}
		}
		// Press (Atk2)
		if((CurButton & IN_ATTACK2) && !(pev(id, pev_oldbuttons) & IN_ATTACK2))
		{
			if(get_user_weapon(id) != CSW_MP5NAVY) 
				return FMRES_IGNORED;
			
			if(!Atk1BtnPressed[id])
			{
				if(!PlayerHasZoom[id])
					setZoom(id);
				else
					removeZoom(id);
			}
		}
	}

	return FMRES_IGNORED;
}

public fwUpdateClientData(id, sendweapons, cd_handle)
{
	if(!isAlive(id))
		return FMRES_IGNORED;
	
	// this function overrides the client side sounds and stuff
	if(playerHasBow(id) && cd_handle) // TODO: if(getClass( ...
	{
		//set_cd(cd_handle, CD_flNextAttack, halflife_time() + 0.001 );
		set_cd(cd_handle, CD_ID, 1);
		get_cd(cd_handle, CD_flNextAttack, BowNextAtk[id]);
		return FMRES_HANDLED;
	}
	return FMRES_IGNORED;
}

public fmSetModel(ent, model[])
{
	if(!isEntValid(ent))
		return FMRES_IGNORED;

	/*new owner = getEntOwner(ent);

	if(!isConnected(owner))
		return FMRES_IGNORED;*/
	
	// dropped weapons set model (temporary disabled)
	/*if(equali(model, "models/w_mp5.mdl"))
	{
		engfunc(EngFunc_SetModel, ent, W_BOW);
		return FMRES_SUPERCEDE;
	}
 	else if(equali(model, "models/w_knife.mdl"))
	{
		engfunc(EngFunc_SetModel, ent, W_DAGGER);
		return FMRES_SUPERCEDE;
	}*/
	
	// if owner is dead, remove it if we need to
	/*if(getHp(owner) <= 0)
	{
		dllfunc(DLLFunc_Think, ent);
		return FMRES_SUPERCEDE;
	}*/
	
	return FMRES_IGNORED;
}

public fwClientKillPre(id) // hook for console command 'kill'
{
	if(isGM(id)){
		Log(DEBUG, "Core", "Player %s used 'kill' command", getName(id));
		return FMRES_IGNORED;
		
	}
	return FMRES_SUPERCEDE;
}

public fmTouch(entity, toucher)
{
	if(!isEntValid(entity) /*|| !isEntValid(toucher)*/)
		return FMRES_IGNORED;

	if(warmupConfirmed())
	{
		//	entity		- entity object
		//	_entity		- entity classname
		//	toucher		- toucher object
		//	_toucher	- toucher classname
		
		new _entity[32], _toucher[32];
		getEntClass(entity, _entity, 31);
		getEntClass(toucher, _toucher, 31);
		new owner = getEntOwner(entity);

		new bool:toucher_is_valid = isPlayerValid(toucher); //<Player>

		// entity XYZ on touch
		new iorigin[3], Float:origin[3];
		getEntOrigin(entity, origin);
		FVecIVec(Float:origin, iorigin);
		
		// ==========================================================
		// 							Items
		// ==========================================================
		/**
		 * Adena, Etc
		 */
		if(equali(_entity, "Item@Adena") || equali(_entity, "Item@Etc"))
		{
			if(isPlayer(_toucher) && toucher_is_valid)
			{
				//if(!CanPickup[toucher])
				//	return FMRES_IGNORED;
				
				if(toucher == owner){
					return FMRES_IGNORED;
				}

				new adena = getAdena(toucher);
				new amount = randInt(ADENA_MIN_DROP_AMOUNT, ADENA_MAX_DROP_AMOUNT);

				if(adena + amount >= ADENA_MAX_AMOUNT){
					sendMessage(toucher, print_center, "You reached maximum Adena.");
					return FMRES_IGNORED;
				}else{
					setAdena(toucher, adena + amount);
					emit_sound(toucher, CHAN_VOICE, S_PICKUP, VOL_NORM, ATTN_STATIC, 0, PITCH_NORM);
					Util_itemPickup(entity); // test
					entityDispose(entity);
				}
			}
			else /*if(pev(entity, pev_flags) & FL_ONGROUND)*/
			{
				Util_destroyEffect(entity);
				entityDispose(entity);
			}
		}
		/**
		 * Potions
		 */
		else if(equali(_entity, "Item@Potion"))
		{
			if(isPlayer(_toucher) && toucher_is_valid)
			{
				new hp = getHp(toucher);
				if(!(hp >= MAX_PLAYER_HEALTH))
				{
					new hp_add = randInt(Config[POTION_HP_ADD_MIN], Config[POTION_HP_ADD_MAX]);

					if(hp_add + hp >= MAX_PLAYER_HEALTH)
						setHp(toucher, MAX_PLAYER_HEALTH);
					else
						setHp(toucher, hp + hp_add);

					createL2Effect(toucher, SPR_POTION_HP, 0.4, 204.0, 18.0, 0.6);
					//createL2Effect(target, effect, size, opacity, framerate, showtime)

					emit_sound(toucher, CHAN_ITEM, S_POTION, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);

					if(!isBot(toucher))
						Create_ScreenFade(toucher, 1<<10, 1<<10, FFADE_IN, 150,75,0,75, false);

					entityDispose(entity);
				}
			}
			else if(isEffect(_toucher))
			{
				Util_destroyEffect(entity);
				entityDispose(entity);
			}
		}
		// ==========================================================
		// 							Arrows
		// ==========================================================
		/**
		 * Guard shot
		 */
		else if(equali(_entity, "guard_arrow"))  // arrow shot from guards
		{
			if(isEffect(_toucher))
			{
				Util_destroyEffect(entity);
			}
			else if(isPlayer(_toucher) && toucher_is_valid)
			{
//				Create_TE_BLOODSPRITE(origin, SPR_BLOOD, SPR_BLOOD, 248, random_num(1,5));
			
				Skills_inSleepCheck(toucher); // remove Sleep if player in
				
				// HOW TO DETECT OWNER OF ENTITY AND GET HIM CLASSNAME?
				/*
				static _guard;
				pev(owner, pev_classname, _guard, 31);
				if(equali(_guard, "L2Guard@East") && getTeam(toucher) == 1)
					return FMRES_IGNORED;
				else if(equali(_guard, "L2Guard@West") && getTeam(toucher) == 2)
					return FMRES_IGNORED;
				*/
				
				// Reflect Damage is not working on guard attacks!
				ExecuteHamB(Ham_TakeDamage, toucher, 0, 0, randFloat(GUARD_DAMAGE_MIN, GUARD_DAMAGE_MAX), 4098);	
			}
			//else if(isAvanpost(_toucher)){/*nothing or reflect*/}
			else if(isMonster(_toucher) || isPet(_toucher))
			{
				ExecuteHam(Ham_TakeDamage, toucher, 0, 0, randFloat(GUARD_DAMAGE_MIN, GUARD_DAMAGE_MAX), 4098);	
			}
			else // shot to walls or ground
			{
				Create_Decal(iorigin, DECAL_BPROOF1, true, entity);
			}

			emit_sound(entity, CHAN_AUTO, S_ARMOR_LEATHER[randInt(0,7)], randFloat(0.6,1.0), ATTN_NORM, 0, PITCH_NORM);

			entityDispose(entity);
		}
		/**
		 * Player shot
		 */
		else if(equali(_entity, "arrow")) // arrow shot from players
		{
			if(isEffect(_toucher)) // <Util>
			{
				Util_destroyEffect(entity);
			}
			else if(isPlayer(_toucher) && toucher_is_valid)
			{
				// self shot check ?
				//if(owner == toucher || pev(toucher, pev_takedamage) == DAMAGE_NO)
				//	return FMRES_IGNORED;

				if(GameMode == MODE_OUTPOST_DEFENSE)
				{
					if(getTeam(owner) == getTeam(toucher))
						if(!get_cvar_pointer("mp_friendlyfire")) // TODO: move to config
							return FMRES_IGNORED;
				}

				if(Skills_reflectOrDamage(owner, toucher, ShotDamage[owner], false /*isStun*/, false /*isMagic*/))
				{
					Skills_inSleepCheck(toucher);
					return FMRES_IGNORED;
				}else{
					takeArrowDamage(owner, toucher, ShotDamage[owner]);
					//takeDamage(owner, toucher, ShotDamage[owner]);
				}
			}
			else if(isAvanpost(_toucher))
			{
				if((equali(_toucher, "L2Avanpost@Red") && getTeam(owner) != TEAMID_EAST) // no team attack
				|| (equali(_toucher, "L2Avanpost@Blu") && getTeam(owner) != TEAMID_WEST)){
					iorigin[2] += 24;
					Create_TE_BREAKMODEL(iorigin, 5, 5, 5, randInt(-50,50), randInt(-50,50), 25, 10, M_WOODGIBS, randInt(1,3), randInt(10,25), 0x08);
					ExecuteHam(Ham_TakeDamage, toucher, owner, owner, ShotDamage[owner], 4098);
					Stats_addAvanpostDmg(owner, ShotDamage[owner]);
				}
			}
			else if(isMonster(_toucher) || isGuard(_toucher) || isBuffer(_toucher) || isPet(_toucher))
			{
				ExecuteHam(Ham_TakeDamage, toucher, owner, owner, ShotDamage[owner], 4098);
				
				if(isBuffer(_toucher))
					FMagic_takeDamage(toucher, owner, owner, ShotDamage[owner], 4098);
			}
			else if(isBreakable(_toucher) && pev(toucher, pev_solid) != SOLID_NOT) /*entity_get_int(toucher, EV_INT_solid)*/
			{
				dllfunc(DLLFunc_Use, toucher, entity);
			}
			else // wall or ground
			{
				Create_Decal(iorigin, DECAL_BPROOF1, true, entity);
			}

			emit_sound(entity, CHAN_AUTO, S_ARMOR_LEATHER[randInt(0,7)], randFloat(0.6,1.0), ATTN_NORM, 0, PITCH_NORM);

			if(!isBot(owner) && SAY2_DATA[owner][S2_ARROW_CAM]){
				set_task(1.5, "Say2_cameraBack", owner+TASK_CAMERA_ARROW_CHASE);
			}

			entityDispose(entity);
		}
		// ==========================================================
		// 							Skills
		// ==========================================================
		/**
		 * Stun Shot
		 */
		else if(equali(_entity, "stun_shot"))
		{
			Skills_hitStunShot(entity); // effect on hit wall, ground or player

			new Float:new_damage = randFloat(SKILLDAMAGE_STUN_SHOT_MIN, SKILLDAMAGE_STUN_SHOT_MAX);

			if(isEffect(_toucher))
			{
				Util_destroyEffect(entity);
			}
			else if(isPlayer(_toucher) && toucher_is_valid)
			{
				if(Skills_reflectOrDamage(owner, toucher, new_damage, true /*isStun*/, true /*isMagic*/))
				{
					Skills_inSleepCheck(toucher);
					return FMRES_IGNORED;
				}else{
					takeDamage(owner, toucher, new_damage);
				}
			}
			else if(isAvanpost(_toucher))
			{
				if((equali(_toucher, "L2Avanpost@Red") && getTeam(owner) != TEAMID_EAST) // no team attack
				|| (equali(_toucher, "L2Avanpost@Blu") && getTeam(owner) != TEAMID_WEST)){
					ExecuteHam(Ham_TakeDamage, toucher, owner, owner, new_damage, 4098);
					Stats_addAvanpostDmg(owner, new_damage);
				}
			}
			else if(isMonster(_toucher) || isGuard(_toucher))
			{
				ExecuteHam(Ham_TakeDamage, toucher, owner, owner, new_damage, 4098);
			}
			else if(isBreakable(_toucher) && pev(toucher, pev_solid) != SOLID_NOT) /*entity_get_int(toucher, EV_INT_solid)*/
			{
				dllfunc(DLLFunc_Use, toucher, entity);
			}

			entityDispose(entity);
		}
		/**
		 * Double Shot
		 */
		else if(equali(_entity, "double_shot"))
		{
			Skills_hitDoubleShot(entity); // effect on hit wall, ground or player
			
			new Float:new_damage = randFloat(SKILLDAMAGE_DOUBLE_SHOT_MIN, SKILLDAMAGE_DOUBLE_SHOT_MAX);

			if(isEffect(_toucher))
			{
				Util_destroyEffect(entity);
			}
			else if(isPlayer(_toucher) && toucher_is_valid)
			{
				if(Skills_reflectOrDamage(owner, toucher, new_damage, false /*isStun*/, true /*isMagic*/))
				{
					Skills_inSleepCheck(toucher);
					return FMRES_IGNORED;
				}else{
					takeDamage(owner, toucher, new_damage);
				}
			}
			else if(isAvanpost(_toucher))
			{
				if((equali(_toucher, "L2Avanpost@Red") && getTeam(owner) != TEAMID_EAST) // no team attack
				|| (equali(_toucher, "L2Avanpost@Blu") && getTeam(owner) != TEAMID_WEST)){
					ExecuteHam(Ham_TakeDamage, toucher, owner, owner, new_damage, 4098);
					Stats_addAvanpostDmg(owner, new_damage);
				}
			}
			else if(isMonster(_toucher) || isGuard(_toucher))
			{
				ExecuteHam(Ham_TakeDamage, toucher, owner, owner, new_damage, 4098);
			}
			else if(isBreakable(_toucher) && pev(toucher, pev_solid) != SOLID_NOT) /*entity_get_int(toucher, EV_INT_solid)*/
			{
				dllfunc(DLLFunc_Use, toucher, entity);
			}

			entityDispose(entity);
		}
		/**
		 * Lethal Shot
		 */
		else if(equali(_entity, "lethal_shot"))
		{
			Skills_hitLethalShot(entity); // effect on hit wall, ground or player

			new Float:new_damage = randFloat(SKILLDAMAGE_LETHAL_SHOT_MIN, SKILLDAMAGE_LETHAL_SHOT_MAX);

			if(isEffect(_toucher))
			{
				Util_destroyEffect(entity);
			}
			else if(isPlayer(_toucher) && toucher_is_valid)
			{
				Skills_inSleepCheck(toucher); // remove Sleep if player in

				if(!(isTeamGameMode() && getTeam(owner) == getTeam(toucher)) || getTeam(owner) != getTeam(toucher))
				{
					if(randomChance(SKILLCHANCE_LETHAL_SHOT)) // chance to instant kill
					{
						if(getHp(toucher) > 1)
							setHp(toucher, 1);
						Skills_successLethalShot(toucher);
						showSkillHud(owner, "Lethal success"); 
					}
					else // normal damage (CAN'T BE REFLECT by Reflect Damage)
					{
						takeDamage(owner, toucher, new_damage);
					}
				}else{
					showSkillHud(owner, "Attack failed");
				}
			}
			else if(isAvanpost(_toucher))
			{
				if((equali(_toucher, "L2Avanpost@Red") && getTeam(owner) != TEAMID_EAST) // no team attack
				|| (equali(_toucher, "L2Avanpost@Blu") && getTeam(owner) != TEAMID_WEST)){
					ExecuteHam(Ham_TakeDamage, toucher, owner, owner, new_damage, 4098);
					Stats_addAvanpostDmg(owner, new_damage);
				}
			}
			else if(isMonster(_toucher) || isGuard(_toucher))
			{
				ExecuteHam(Ham_TakeDamage, toucher, owner, owner, new_damage, 4098);
			}
			else if(isBreakable(_toucher) && pev(toucher, pev_solid) != SOLID_NOT) /*entity_get_int(toucher, EV_INT_solid)*/
			{
				dllfunc(DLLFunc_Use, toucher, entity);
			}

			entityDispose(entity);
		}
		/**
		 * Hurricane
		 */
		else if(equali(_entity, "hurricane"))
		{
			Skills_hitHurricane(entity, toucher);

			if(isEffect(_toucher))
			{
				Util_destroyEffect(entity);
			}
			else if(isPlayer(_toucher) && toucher_is_valid)
			{
				Create_TE_DLIGHT(toucher, 8, 200,15,15, 5, 0);

				if(Skills_reflectOrDamage(owner, toucher, float(SKILLDAMAGE_HURRICANE), false /*isStun*/, true /*isMagic*/))
				{
					Skills_inSleepCheck(toucher);
					return FMRES_IGNORED;
				}else{
					takeDamage(owner, toucher, float(SKILLDAMAGE_HURRICANE));
				}
			}
			else if(isAvanpost(_toucher))
			{
				if((equali(_toucher, "L2Avanpost@Red") && getTeam(owner) != TEAMID_EAST) // no team attack
				|| (equali(_toucher, "L2Avanpost@Blu") && getTeam(owner) != TEAMID_WEST)){
					ExecuteHam(Ham_TakeDamage, toucher, owner, owner, float(SKILLDAMAGE_HURRICANE), 4098);
					Stats_addAvanpostDmg(owner, float(SKILLDAMAGE_HURRICANE));
				}
			}
			else if(isMonster(_toucher) || isGuard(_toucher) || isBuffer(_toucher) || isPet(_toucher))
			{
				if(isPet(_toucher) && owner == getEntOwner(toucher)){
					return FMRES_IGNORED;
				}

				ExecuteHam(Ham_TakeDamage, toucher, owner, owner, float(SKILLDAMAGE_HURRICANE), 4098);
				
				if(isBuffer(_toucher))
					FMagic_takeDamage(toucher, owner, owner, float(SKILLDAMAGE_HURRICANE), 4098);
			}
			else if(isBreakable(_toucher) && pev(toucher, pev_solid) != SOLID_NOT) /*entity_get_int(toucher, EV_INT_solid)*/
			{
				dllfunc(DLLFunc_Use, toucher, entity);
			}

			entityDispose(entity);
		}
	}

	return FMRES_IGNORED;
}

public fwPlayerPreThink(id)
{
	if(isAlive(id) && haveTeam(id)) /* <Player> */
	{
		if(warmupConfirmed() && haveClass(id))
		{	
			// Bow Shot
			if(!isAttackBlocked(id) && getClass(id) == CLASS_A && !isBot(id))
			{
				if(get_gametime() - CD_BowShot[id] > CLASS_ATK_DELAY_ARCHER)
				{
					/*if(pev(id, pev_oldbuttons) & IN_ATTACK)
					{ // TODO: fix for post shoot
					}*/
					if(pev(id,pev_button) & IN_ATTACK)
					{
						if(isActiveWeaponBow(id)) // bow now in hands
						{
							ShotPowerAmt[id] += 2;
							setShotPower(id, ShotPowerAmt[id]);
						}
					}else{
						ShotPowerAmt[id] = ShotPowerAmt[id] - 1;
					}
					// Set arrow power
					if(ShotPowerAmt[id] < 0){
						ShotPowerAmt[id] = 0;
					}
					if(ShotPowerAmt[id] > 98){ // max power
						ShotPowerAmt[id] = 98; //99
					}
				}
			}
			
			// Move block control
			if(isMoveBlocked(id) 
			|| PIN_Stun[id]
			|| PIN_Sleep[id]
			|| PIN_Snipe[id]
			|| PIN_Return[id]
			|| PIN_SummonCorrupted[id]
			//|| PIN_SummonCubics[id]
			|| PIN_DryadRoot[id]
			|| PIN_UltDefense[id]
			|| PIN_GateChant[id])
			{
				//last good method (sliding)
				set_pev(id, pev_velocity, Float:{0.0,0.0,0.0});
				//set_pev(id, pev_maxspeed, 0.01);
			}
			
			// Move speed control
			if(isMoveStopped(id) || isCastingNow(id))
			{
				set_user_maxspeed(id, 0.01);

				/* // DUCK/JUMP block on PRETH @ TRY
				    if(entity_get_int(id,EV_INT_button) & IN_JUMP)) 
					{ 
						new Float:vVelocity[3] 
						entity_get_vector(id,EV_VEC_velocity,vVelocity) 
						vVelocity[2] = float(-abs(round(vVelocity[2]))) 
						entity_set_vector(id,EV_VEC_velocity,vVelocity) 
						entity_set_int(id,EV_INT_button,entity_get_int(id,EV_INT_button) & ~IN_JUMP) 
					} 
				*/
			}
			else if(slowMotion)
			{
				set_user_maxspeed(id, 80.0);
				set_user_gravity(id, 0.2);
				entity_set_float(id, EV_FL_framerate, 3 * 0.1);
			}
			else
			{
				if(getClass(id) == CLASS_A){
					if(!isActiveWeaponBow(id))
						removeZoom(id);
					else if(PlayerHasZoom[id])
						set_user_maxspeed(id, 150.0);
					else if(!PlayerHasZoom[id])
						set_user_maxspeed(id, 250.0);
				}
				else if(getClass(id) == CLASS_B && PIN_Dash[id] && pev(id, pev_flags) & FL_ONGROUND){
					set_user_maxspeed(id, 375.0); //400.0
				}
				else if(getClass(id) == CLASS_D && PIN_Frenzy[id]){
					set_user_maxspeed(id, 225.0);
				}
				else if(isGM(id) && SAY2_DATA[id][S2_SUPER_HASTE]){ // Say2 Admin
					set_user_maxspeed(id, 1000.0);
				}
				else
					set_user_maxspeed(id, 250.0); // default move spd
					//set_pev(id, pev_maxspeed, 250.0);
			}
			
			if(isGM(id) && SAY2_DATA[id][S2_SQUEAK])
			{
				set_pev(id, pev_flTimeStepSound, 999);
				if(Say2_SqueakNextStep[id] < get_gametime())
				{
					if(fm_get_ent_speed(id) && (pev(id, pev_flags) & FL_ONGROUND)){
						emit_sound(id, CHAN_BODY, S_STEP_NOISE[randInt(0,2)], VOL_NORM, ATTN_STATIC, 0, PITCH_NORM);
					}
					// next step delay
					if(SAY2_DATA[id][S2_SUPER_HASTE])
						Say2_SqueakNextStep[id] = get_gametime() + 0.05;
					else
						Say2_SqueakNextStep[id] = get_gametime() + 0.2;
				}
			}

			// TODO: also some skills block move
			//if(PIN_DryadRoot[id] /*|| PIN_UltDefense[id]*/ || PIN_Snipe[id]){
			//	set_user_maxspeed(id, 0.01);
			//}
			
			if(isCameraLocked(id)){
				set_pev(id, pev_v_angle, CameraAngles[id]);
				set_pev(id, pev_fixangle, 1);
			}

			// Check blocked skills (only for Pre-think skills)
			if(!isSkillsBlocked(id)) // <Skills>
			{
				Skills_checkUse1(id);
				Skills_checkUse2(id);
				Skills_checkUse3(id);
				Skills_checkUse4(id);

				if(isBot(id)){
					Bot_preThink(id); // Available bots to use skills
				}
			}
		}
	}
	return FMRES_IGNORED;
}

public fwAddToFullPackPost(es_handle, e, id, host, hostflags, player, pset)
{
	if(player && isPlayerValid(id))
	{
		if(PlayerAct[id] > ACT_NONE) /* <Player> */
		{
			switch(PlayerAct[id])
			{
				case ACT_SIT:		set_es(es_handle, ES_Sequence, 111); // sit down
				case ACT_SIT_IDLE:	set_es(es_handle, ES_Sequence, 112); // sit idle
				case ACT_STAND:		set_es(es_handle, ES_Sequence, 113); // sit up
			}
		}
		else if(get_user_weapon(id) == CSW_KNIFE && getClass(id) == CLASS_D && PlayerWeapon[id][WTYPE] == WTYPE_KNIFE_CLASS)
		{
			static _anim;
			_anim = get_es(es_handle, ES_Sequence);
			switch(_anim)
			{
				//case 73: //crouch_aim_knife
				//case 74: //crouch_shoot_knife
				//case 75: //ref_aim_knife
				//case 76: //ref_shoot_knife
				case 73, 74, 75, 76: {
					set_es(es_handle, ES_Sequence, _anim -= 57);
				}
			}
		}
	}
	return FMRES_IGNORED;
}
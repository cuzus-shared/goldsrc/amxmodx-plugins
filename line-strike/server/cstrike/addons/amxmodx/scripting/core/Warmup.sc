// --------------------------------------------------
// Warmup
// --------------------------------------------------

public bool:warmupConfirmed()
{
	if(WarmupTime <= 0 && WarmupIsEnded)
		return true;
	return false;
}

/**
 * Warmup init and abort function.
 */
public Warmup_init()
{
	WarmupIsEnded = false; // var for warmupConfirmed()
	WarmupTime = 999;

	if(!task_exists(TASK_WARMUP_IDLE))
		set_task(1.0, "Warmup_idle", TASK_WARMUP_IDLE);
}

public Warmup_abort(reason[])
{
	WarmupStarted = false;
	
	for(new id=1; id<=GMaxPlayers; id++)
		if(isConnected(id) && !isBot(id))
			client_print(id, print_center, "%s - Countdown aborted!", reason);

	if(task_exists(TASK_WARMUP_CHECK))
		remove_task(TASK_WARMUP_CHECK);
	if(task_exists(TASK_WARMUP_TIME))
		remove_task(TASK_WARMUP_TIME);
	
	Warmup_init();
	
	Log(INFO, "Warmup", "Not enough players for event. Min Requested %d, Participating %d", Config[MIN_PLAYERS_TO_START], Login_getRegisteredPlayers());
	Log(INFO, "Warmup", "%s: Match aborted!", GameModeString);
}

public Warmup_start()
{
	remove_task(TASK_WARMUP_IDLE);
	// bugfix
	remove_task(TASK_WARMUP_CHECK);
	remove_task(TASK_WARMUP_TIME);

	Log(INFO, "Warmup", "Event notification start");

	if(Config[GLOBAL_WARMUP_TIME] > 0 && WarmupTime > 0)
	{
		WarmupStarted = true;
		WarmupTime = Config[GLOBAL_WARMUP_TIME];
		server_cmd("sv_gravity 0.1");
		set_task(0.1, "Warmup_check", TASK_WARMUP_CHECK); // check for start game
		set_task(0.2, "Warmup_time", TASK_WARMUP_TIME); // HUD timer
	}
}

public Warmup_idle(taskid)
{
	set_hudmessage(90,90,90, -1.0, 0.22, 2, 0.1, 0.66, 0.05, 0.4, 2); /* channel 2 */
	ShowSyncHudMsg(0, KillStreakSyncHud1, GameModeString);
	set_dhudmessage(90,90,90, -1.0, 0.25, 2, 0.1, 0.66, 0.05, 0.4, true);
	show_dhudmessage(0, "Waiting for other players...");
	set_task(1.0, "Warmup_idle", taskid);
}

public Warmup_check(taskid)
{
	WarmupTime--;

	if(WarmupTime >= 10){
		set_hudmessage(90,90,90, -1.0, 0.22, 2, 0.1, 0.66, 0.05, 0.4, 2); /* channel 2 */
		set_dhudmessage(90,90,90, -1.0, 0.25, 2, 0.1, 0.66, 0.05, 0.4, true); // 0 230 0
	}
	if(WarmupTime < 10){
		set_hudmessage(90,90,90, -1.0, 0.22, 2, 0.1, 0.66, 0.05, 0.4, 2); /* channel 2 */
		set_dhudmessage(90,90,90, -1.0, 0.25, 2, 0.1, 0.66, 0.05, 0.4, true); // 0 230 0		
	}
	if(WarmupTime == 2)
	{
		if(Login_checkNotEnough())
			return;

		switch(GameMode){
			case MODE_OUTPOST_DEFENSE:	{client_cmd(0, "spk %s", S_SIEGE_START);}
			case MODE_CASTLE_SIEGE:		{client_cmd(0, "spk %s", S_SIEGE_START);}
			case MODE_LAST_HERO:		{client_cmd(0, "spk %s", S_SYS_BATTLE_START);}
			case MODE_TEAM_VS_TEAM:		{client_cmd(0, "spk %s", S_RACE_START);}
		}

		for(new id=1; id<=GMaxPlayers; id++)
			if(isConnected(id) && !isBot(id))
				Create_ScreenFade(id, (1<<12)*4, (1<<12)*1, FFADE_OUT, 255,255,255,255, true); // white fade
	}
	if(WarmupTime <= 0)
	{
		if(Login_checkNotEnough())
			return;

		WarmupTime = -999;
		WarmupIsEnded = true;
		server_cmd("sv_gravity 800");

		if(task_exists(TASK_WARMUP_CHECK))
			remove_task(TASK_WARMUP_CHECK);

		set_task(2.5, "Player_statusText", TASK_STATUS_TEXT, _, _, "b");
		set_task(10.0, "Stats_updater", TASK_TEAM_SCORE, _, _, "b");

		Log(DEBUG, "Warmup", "GameMode_start()");
		GameMode_start();

		Login_warmupEnds();

		return;
	}

	ShowSyncHudMsg(0, KillStreakSyncHud1, GameModeString);
	show_dhudmessage(0, "The game starts in"); // WOT: The battle starts in

	set_task(1.0, "Warmup_check", taskid);
}

public Warmup_time(taskid)
{
	if(WarmupTime >= 10){
		set_dhudmessage(130,130,130, -1.0, 0.28, 2, 0.1, 0.66, 0.05, 0.4, true);
	}
	if(WarmupTime < 10){
		set_dhudmessage(130,130,130, -1.0, 0.28, 2, 0.1, 0.66, 0.05, 0.4, true);
	}
	if(WarmupTime <= 0){
		if(task_exists(TASK_WARMUP_TIME))
			remove_task(TASK_WARMUP_TIME);
		return;
	}

	show_dhudmessage(0, "00 : %02d", WarmupTime);
	client_cmd(0, "spk %s", S_CLICK_3);

	set_task(1.0, "Warmup_time", taskid);
}
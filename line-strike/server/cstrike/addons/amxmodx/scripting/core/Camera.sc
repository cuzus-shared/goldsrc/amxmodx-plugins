// --------------------------------------------------
// Camera
// --------------------------------------------------

#define USE_TOGGLE 3

new PlayerCamera[XX];
new PlayerCamFlag;

//#define MarkUserInCamera(%0)		PlayerCamFlag |= 1<<(%0&31)
#define clearUserInCamera(%0)		PlayerCamFlag &= ~(1<<(%0&31))
#define isUserInCamera(%0)			PlayerCamFlag & 1<<(%0&31)
#define toggleUserCameraState(%0)	PlayerCamFlag ^= 1<<(%0&31)

public Camera_change(id)
{ 
    if(!isAlive(id))
        return; //PLUGIN_HANDLED;

    new ent = PlayerCamera[id];
    if(!pev_valid(ent))
    {
        static triggercam;
        if(!triggercam){
            triggercam = engfunc(EngFunc_AllocString, "trigger_camera");
        }
        ent = engfunc(EngFunc_CreateNamedEntity, triggercam);
        set_kvd(0, KV_ClassName, "trigger_camera");
        set_kvd(0, KV_fHandled, 0);
        set_kvd(0, KV_KeyName, "wait");
        set_kvd(0, KV_Value, "999999");
        dllfunc(DLLFunc_KeyValue, ent, 0);
        set_pev(ent, pev_spawnflags, SF_CAMERA_PLAYER_TARGET | SF_CAMERA_PLAYER_POSITION);
        set_pev(ent, pev_flags, pev(ent, pev_flags) | FL_ALWAYSTHINK);
        dllfunc(DLLFunc_Spawn, ent);
        PlayerCamera[id] = ent;
    }
    toggleUserCameraState(id);
    Camera_CheckForward();

    new Float:flMaxSpeed, iFlags = pev(id, pev_flags);
    pev(id, pev_maxspeed, flMaxSpeed);

    ExecuteHam(Ham_Use, ent, id, id, USE_TOGGLE, 1.0);

    set_pev(id, pev_flags, iFlags);
    // depending on mod, you may have to send SetClientMaxspeed here.
    // engfunc(EngFunc_SetClientMaxspeed, id, flMaxSpeed)
    set_pev(id, pev_maxspeed, flMaxSpeed);
	
	//return PLUGIN_HANDLED;
} 

public SetView(id, ent)
{
    if(isUserInCamera(id) && isAlive(id))
    {
        new cam = PlayerCamera[id];
        if(cam && ent != cam)
        { 
            new this[16];
            pev(ent, pev_classname, this, 15);
            if(!equal(this, "trigger_camera")) // should let real cams enabled 
            {
                engfunc(EngFunc_SetView, id, cam); // shouldn't be always needed
                return FMRES_SUPERCEDE;
            }
        }
    }
    return FMRES_IGNORED;
}

public Camera_clientDisconnect(id)
{
    new ent = PlayerCamera[id];
    if(pev_valid(ent)){
        engfunc(EngFunc_RemoveEntity, ent);
    }
    PlayerCamera[id] = 0;
    clearUserInCamera(id);
    Camera_CheckForward();
}

public Camera_clientPutInServer(id)
{
    PlayerCamera[id] = 0;
    clearUserInCamera(id);
}

get_cam_owner(ent)
{
    static id;
    for(id = 1; id <= GMaxPlayers; id++)
    {
        if(PlayerCamera[id] == ent){
            return id;
        }
    }
    return 0;
}

public Camera_distUp(id)
{
	if(get_user_flags(id) & ADMIN_IMMUNITY){
		if(CamDist[id] < 999)
			CamDist[id]++;
	}else{
		if(CamDist[id] < 46)
			CamDist[id]++;
	}
	return PLUGIN_HANDLED;
}
public Camera_distDn(id)
{
	if(get_user_flags(id) & ADMIN_IMMUNITY){
		if(CamDist[id] > -999)
			CamDist[id]--;
	}else{
		if(CamDist[id] > 1)
			CamDist[id]--;
	}
	return PLUGIN_HANDLED;
}

public Camera_reset(id)
{
	CamDist[id] = 10; // default distance for 3rd person
	return PLUGIN_HANDLED;
}

public hamThinkCamera(ent)
{
    static id;
    if(!(id = get_cam_owner(ent)))
        return;
	
    static Float:fVecPlayerOrigin[3], Float:fVecCameraOrigin[3], Float:fVecAngles[3], Float:fVecBack[3];

    pev(id, pev_origin, fVecPlayerOrigin);
    pev(id, pev_view_ofs, fVecAngles);
    fVecPlayerOrigin[2] += fVecAngles[2];

    pev(id, pev_v_angle, fVecAngles);

    // See player from front?
    //fVecAngles[0] = 15.0 
    //fVecAngles[1] += fVecAngles[1] > 180.0 ? -180.0 : 180.0 

    angle_vector(fVecAngles, ANGLEVECTOR_FORWARD, fVecBack);

    // Move back to see ourself (150 units)
    fVecCameraOrigin[0] = fVecPlayerOrigin[0] + (-fVecBack[0] * float(CamDist[id]) * 10.0); //150.0);
    fVecCameraOrigin[1] = fVecPlayerOrigin[1] + (-fVecBack[1] * float(CamDist[id]) * 10.0); //150.0);
    fVecCameraOrigin[2] = fVecPlayerOrigin[2] + (-fVecBack[2] * float(CamDist[id]) * 10.0); //150.0);

    engfunc(EngFunc_TraceLine, fVecPlayerOrigin, fVecCameraOrigin, IGNORE_MONSTERS, id, 0);
    static Float:flFraction;
    get_tr2(0, TR_flFraction, flFraction);
	
    if(flFraction != 1.0) // adjust camera place if close to a wall 
	{
        flFraction *= float(CamDist[id]) * 10.0; //150.0;
        fVecCameraOrigin[0] = fVecPlayerOrigin[0] + (-fVecBack[0] * flFraction);
        fVecCameraOrigin[1] = fVecPlayerOrigin[1] + (-fVecBack[1] * flFraction);
        fVecCameraOrigin[2] = fVecPlayerOrigin[2] + (-fVecBack[2] * flFraction);
    }
	
    set_pev(ent, pev_origin, fVecCameraOrigin);
    set_pev(ent, pev_angles, fVecAngles);
}

public Camera_CheckForward()
{
    static HamHook:HhCameraThink, FhSetView;
    if(PlayerCamFlag)
    {
        if(!FhSetView){
            FhSetView = register_forward(FM_SetView, "SetView");
        }
        if(!HhCameraThink){
            HhCameraThink = RegisterHam(Ham_Think, "trigger_camera", "hamThinkCamera");
        }
        else{
            EnableHamForward(HhCameraThink);
        }
    }else{
        if(FhSetView){
            unregister_forward(FM_SetView, FhSetView);
            FhSetView = 0;
        }
        if(HhCameraThink){
            DisableHamForward(HhCameraThink);
        }
    }
}

/**
 * Death Camera
 */
public Camera_setDeathCam(id)
{
	id -= TASK_SET_DEATH_CAM;

	/*
	// edict->deadflag values
	#define DEAD_NO				0 // alive
	#define DEAD_DYING			1 // playing death animation or still falling off of a ledge waiting to hit ground
	#define DEAD_DEAD			2 // dead. lying still.
	#define DEAD_RESPAWNABLE	3
	#define DEAD_DISCARDBODY	4
	*/
	static dFlags;
	dFlags = pev(id,pev_deadflag);
	if(dFlags & DEAD_DEAD){
		set_pev(id,pev_deadflag,dFlags&~DEAD_DEAD);
	}
	
	/* OTHER SOLVES
	
		https://forums.alliedmods.net/showthread.php?t=8221
		http://amx-x.ru/viewtopic.php?f=8&t=26552&start=10

		После смерти получай угол, координаты игрока, и в чинке присваивай...
		
		В плагине используется Think, но только первые 2 секунды после смерти игрока, 
		чтобы отловить нужный момент и произвести 'настройки'. Никаких многократных действий, 
		типа присвоения координат. Переключение с клавиатуры блокируется полностью.
	*/

	/*new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		set_pev(ent, pev_classname, "death_camera");
		
		engfunc(EngFunc_SetModel, ent, M_L2DROP_ITEM);
		set_pev(ent, pev_body, 11);
		engfunc(EngFunc_SetSize, ent, Float:{-3.0, -3.0, -3.0}, Float:{3.0, 3.0, 3.0});
		
		new Float:origin[3]; //, Float:angles[3]
		pev(id, pev_origin, origin);
		//origin[2] = 56.0;
		set_pev(ent, pev_origin, origin);
		
		pev(id, pev_angles, origin); // OPT: angles
		set_pev(ent, pev_angles, origin);
		
		pev(id, pev_v_angle, origin);
		set_pev(ent, pev_v_angle, origin);
		
		fm_set_rendering(ent, kRenderFxGlowShell, 0, 0, 0, kRenderTransAdd, 0); // invisible
		
		//set_task(2.1, "dropDeathCam", ent+TASK_DROP_DEATH_CAM);
		
		engfunc(EngFunc_SetView, id, ent);
	}*/
}

/*public dropDeathCam(taskid)
{
	new ent = taskid-TASK_DROP_DEATH_CAM;
	
	engfunc(EngFunc_DropToFloor, ent);
	
	new Float:origin[3];
	pev(ent, pev_origin, origin);
	origin[2] += 10.0; // 6.0
	set_pev(ent, pev_origin, origin);
	
//	new Float:angles[3];
	pev(ent, pev_angles, origin); // OPT: angles
	origin[0] += random_float(-360.0, 360.0);
	origin[1] += random_float(0.0, 180.0);
//	origin[2] += random_float(0.0, 180.0);
	set_pev(ent, pev_angles, origin);
	
	remove_task(taskid);
}*/

public Camera_unsetDeathCam(id)
{
	engfunc(EngFunc_SetView, id, id);
}

/**
 * Join Camera
 */
public Camera_setJoinCam(id)
{
	if(!JoinCameraEnt) // if entity is not created, create once for map
	{
		Log(INFO, "Camera", "Creating join camera (id=%d) for map: %s", JoinCameraEnt, GMapName);

		JoinCameraEnt = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));

		set_pev(JoinCameraEnt, pev_classname, "join_camera");
		engfunc(EngFunc_SetModel, JoinCameraEnt, M_L2DROP_ITEM);
		set_pev(JoinCameraEnt, pev_body, 11);
		engfunc(EngFunc_SetSize, JoinCameraEnt, Float:{-1.0, -1.0, -1.0}, Float:{1.0, 1.0, 1.0});

		new Float:forigin[3] = {0.0, 0.0, 0.0};
		new Float:fangles[3] = {0.0, 0.0, 0.0};

		if(file_exists(CFG_FILE_JOINCAM))
		{
			new _file = fopen(CFG_FILE_JOINCAM, "rt");
			if(!_file){
				Log(ERROR, "Camera", "Open file error: %s", CFG_FILE_JOINCAM);
				return;
			}
			else
			{
				new line[256];
				// get all lines from file
//				for(new a=0; a < !feof(_file); a++)
				while(!feof(_file))
				{
					fgets(_file, line, 255);
					
					// skip comments if line starts with certain symbol
					if(line[0] == '#' || !line[0] || line[0] == ' ' || line[0] == 10)
					{ // line is not counted
//						a--;
						continue;
					}
					else
					{
						new fmapname[MAX_MAPNAME_LENGTH];
						copyc(fmapname, MAX_MAPNAME_LENGTH-1, line, '/');
						//Log(DEBUG, "Camera", "copyc(%s, %d, %s, '/')", fmapname, MAX_MAPNAME_LENGTH-1, line);
						
						if(equali(fmapname, GMapName))
						{
							//Log(DEBUG, "Camera", "if(equali(%s, %s)) -- TRUE", fmapname, GMapName);
							
							replace(line, 255, fmapname, "");
							//Log(DEBUG, "Camera", "replace(-fmapname): %s", line);
							replace(line, 255, "/", "");
							//Log(DEBUG, "Camera", "replace(-/): %s", line);
							
							new str_origin[64], str_angles[64];
							strtok(line, str_origin, 63, str_angles, 63, '/');
							
							//Log(DEBUG, "Camera", "str_origin: %s", str_origin);
							//Log(DEBUG, "Camera", "str_angles: %s", str_angles);
							
							new str_x[12], str_y[12], str_z[12];
							
							parse(str_origin, str_x, 11, str_y, 11, str_z, 11);
							forigin[0] = floatstr(str_x);
							forigin[1] = floatstr(str_y);
							forigin[2] = floatstr(str_z);

							//Log(DEBUG, "Camera", "str_origin(parse): %f %f %f", forigin[0], forigin[1], forigin[2]);
							
							parse(str_angles, str_x, 11, str_y, 11);
							fangles[0] = floatstr(str_x);
							fangles[1] = floatstr(str_y);
							
							//Log(DEBUG, "Camera", "str_angles(parse): %f %f", fangles[0], fangles[1]);
							
							break;
						}
						else
							continue;
					}
				}
			}
			fclose(_file);
		}
		else{
			Log(ERROR, "Camera", "Failed to load coords file");
			Log(ERROR, "Camera", "%s is missing!", CFG_FILE_JOINCAM);
		}

		engfunc(EngFunc_SetOrigin, JoinCameraEnt, forigin);
		set_pev(JoinCameraEnt, pev_angles, fangles);

		fm_set_rendering(JoinCameraEnt, kRenderFxNone, 0,0,0, kRenderTransAdd, 0); // invisible in game
	}

	engfunc(EngFunc_SetView, id, JoinCameraEnt);
}

public Camera_unsetJoinCam(id)
{
	engfunc(EngFunc_SetView, id, id);
}
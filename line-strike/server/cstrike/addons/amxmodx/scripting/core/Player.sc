// ----------------------------------------------------------------------
// Player
// ----------------------------------------------------------------------

// Class numbers
#define CLASS_A		1		// Bow
#define CLASS_B 	2		// Dagger
#define CLASS_C		3		// Fists
#define CLASS_D		4		// Two-handed sword
#define CLASS_E		5		// Sword and Shield
#define CLASS_F		6		// Staff
#define CLASS_G		7		// Magic Sword
#define CLASS_H		8		// Blunt

new const MIN_CLASS_ID = CLASS_A;
new const MAX_CLASS_ID = CLASS_H;

// Class names
#define MAX_CLASSNAME_LENGTH	8	// max symbols + 1
#define CLASSNAME_A		"Archer"
#define CLASSNAME_B		"Rogue"
#define CLASSNAME_C		"Tyrant"
#define CLASSNAME_D		"Raider"
#define CLASSNAME_E		"Knight"
#define CLASSNAME_F		"Wizard"
#define CLASSNAME_G		"Priest"
#define CLASSNAME_H		"Shaman"		//alt: Warden, Overseer

#define RACE_HUMAN		1
#define RACE_ELF		2
#define RACE_DARKELF	3
#define RACE_ORC		4

enum _:DATA_VALUES
{
    CLASS_ID = 0,
    RACE,			// 1 - human, 2 - elf, 3 - darkelf, 4 - orc
	HAIR,
	CUR_LEVEL,
	CUR_EXP,
	CUR_PVP,
	CUR_PK,
	CUR_DEATHS,
	CUR_ADENA
};
new PlayerData[XX][DATA_VALUES];

new PlayerExp[XX][10]; // VALUE = [PlayerId][ClassId]
new PlayerLvl[XX][10]; // VALUE = [PlayerId][ClassId]

new PlayerWeaponEnchant[XX][9]; //[33][MAX_CLASS_ID + 1]

public getWeaponEnchant(id)
{
	return PlayerWeaponEnchant[id][getClass(id)];
}

#define ACT_NONE		0
#define ACT_SIT			1
#define ACT_SIT_IDLE	2
#define ACT_STAND		3
new PlayerAct[XX];

/**
 * Returns player current class ID.
 */
public getClass(id)
{
	return PlayerData[id][CLASS_ID];
}
public setClass(id, classId)
{
	PlayerData[id][CLASS_ID] = classId;
}

/**
 * Returns player name.
 */
public getName(id)
{
	new name[MAX_CHARNAME_LENGTH];
	get_user_name(id, name, MAX_CHARNAME_LENGTH-1);
	return name;
}

public getClassName(player)
{
	new classname[MAX_CLASSNAME_LENGTH];
	switch(getClass(player)){
		case 1: classname = CLASSNAME_A;
		case 2: classname = CLASSNAME_B;
		case 3: classname = CLASSNAME_C;
		case 4: classname = CLASSNAME_D;
		case 5: classname = CLASSNAME_E;
		case 6: classname = CLASSNAME_F;
		case 7: classname = CLASSNAME_G;
		case 8: classname = CLASSNAME_H;
	}
	return classname;
}

public getClassNameFromInd(index)
{
	new classname[MAX_CLASSNAME_LENGTH];
	switch(index){
		case 1: classname = CLASSNAME_A;
		case 2: classname = CLASSNAME_B;
		case 3: classname = CLASSNAME_C;
		case 4: classname = CLASSNAME_D;
		case 5: classname = CLASSNAME_E;
		case 6: classname = CLASSNAME_F;
		case 7: classname = CLASSNAME_G;
		case 8: classname = CLASSNAME_H;
	}
	return classname;
}

public getRace(id)
{
	return PlayerData[id][RACE];
}
public setRace(id, val)
{
	PlayerData[id][RACE] = val;
}

public getExp(id)
{
	return PlayerData[id][CUR_EXP];
}
public setExp(id, exp)
{
	PlayerData[id][CUR_EXP] = exp;
}

public getLevel(id)
{
	return PlayerData[id][CUR_LEVEL];
}
public setLevel(id, level)
{
	PlayerData[id][CUR_LEVEL] = level;
}

public effectLevelUp(id)
{
	id -= TASK_EFFECT_LEVEL_UP;
	new Float:xyz[3], Float:axis[3];
	getEntOrigin(id, xyz);
	axis = xyz;
	axis[0] += 150.0;
	axis[1] += 180.0;
	axis[2] += 25.0; //45.0
	Create_TE_BEAMCYLINDER(xyz, axis, SPR_SHOCKWAVE, 0, 0, 1, 400, 0, 253,244,200,140, 0); //155,155,55
	Create_TE_DLIGHT(id, 11, 253,244,200, 1, 0);
}

/**
 * Calculate rewards for kill.
 * id : killer ID
 * monster : killed unit (false - player, true - monster)
 * summon : killer have summoned unit (give less experience)
 */
public calculateRewards(id /*killer*/, bool:monster, bool:summon)
{
	new lvl = getLevel(id);

	if(lvl >= MAX_PLAYER_LEVEL)
		return 0;

	new exp_add = 0;

	// Calculate experience
	switch(lvl){
		case 1..20: {
			if(monster)
				exp_add = summon ? 10 : 20;
			else // player
				exp_add = summon ? 5 : 10;
		}
		case 21..40: {
			if(monster)
				exp_add = summon ? 5 : 10;
			else
				exp_add = summon ? 3 : 5;
		}
		case 41..60: {
			if(monster)
				exp_add = summon ? 3 : 5;
			else
				exp_add = summon ? 1 : 2;
		}
		case 61..78: {
			if(monster)
				exp_add = summon ? 1 : 2;
			else
				exp_add = summon ? 1 : 1;
		}
	}

	if(Config[RATES_MULTIPLIER]){
		exp_add *= Config[RATES_MULTIPLIER];
	}

	new exp = getExp(id) + exp_add;

	// Check next level
	if(exp >= 100)
	{
		setLevel(id, lvl + 1);
		
		// Exp more than 100
		new diff = exp - 100;
		if(diff > 0){
			setExp(id, diff);
		}else{
			setExp(id, 0);
		}

		setHp(id, MAX_PLAYER_HEALTH);

		// L2 Effect
		set_task(0.1, "effectLevelUp", id+TASK_EFFECT_LEVEL_UP, _, _, "a", 45); //40
		emitSound(id, S_LEVEL_UP);
		printCenter(id, "Your level has increased!");

		// Message to all players (28.09.2014 ZombieManiya)
		for(new i = 0; i < 8; i++){
			show_dhudmessage(0, ""); // clear all DHUDs
		}
		//164,42,191
		set_dhudmessage(188,40,185, -1.0,0.10, 0, 0.3, 1.5, 0.3, 1.0, false); //prev x=-1.0,y=0.16
		show_dhudmessage(0, "%s has earned %s level %d!", getName(id), getClassName(id), getLevel(id));
	}
	else{
		setExp(id, exp);
	}

	Player_classUpdate(id);

	return exp_add;
}

/**
 * This method update base (lvl, exp) and current (lvl, exp) of class.
 */
public setLevelWithExp(id, level)
{
	PlayerData[id][CUR_LEVEL] = level;
//	PlayerData[id][CUR_EXP] = _LEVELS[level][NEXT_LVL];

	Player_classUpdate(id);
}

public Player_flush(id)
{
	PlayerData[id][CLASS_ID] = -1; // NOTE: put class number for test this class on all players(bots)
	PlayerData[id][RACE] = 1;
	PlayerData[id][HAIR] = 0;

	PlayerData[id][CUR_LEVEL] = 1;
	PlayerData[id][CUR_EXP] = 0;
	PlayerData[id][CUR_PVP] = 0;
	PlayerData[id][CUR_PK] = 0;
	PlayerData[id][CUR_DEATHS] = 0;
	PlayerData[id][CUR_ADENA] = 0;

	PlayerExp[id][CLASS_A] = 0; PlayerLvl[id][CLASS_A] = 1;
	PlayerExp[id][CLASS_B] = 0; PlayerLvl[id][CLASS_B] = 1;
	PlayerExp[id][CLASS_C] = 0; PlayerLvl[id][CLASS_C] = 1;
	PlayerExp[id][CLASS_D] = 0; PlayerLvl[id][CLASS_D] = 1;
	PlayerExp[id][CLASS_E] = 0; PlayerLvl[id][CLASS_E] = 1;
	PlayerExp[id][CLASS_F] = 0; PlayerLvl[id][CLASS_F] = 1;
	PlayerExp[id][CLASS_G] = 0; PlayerLvl[id][CLASS_G] = 1;
	PlayerExp[id][CLASS_H] = 0; PlayerLvl[id][CLASS_H] = 1;

	for(new _cid = 1; _cid <= MAX_CLASS_ID; _cid++)
	{
		PlayerWeaponEnchant[id][_cid] = 0;
	}
}

public bool:haveClass(id)
{
	if(getClass(id) < MIN_CLASS_ID || getClass(id) > MAX_CLASS_ID /*|| getClass(id) == CL_UNDEF*/)
		return false;
	return true;
}

public canShowMenu(id) // doubtful
{
	if(isAlive(id) && warmupConfirmed() && !isMenuOpened[id][RESPAWN_MENU] && !EndOfMap)
		return true;
	return false;
}

public blockAllMenus(id)
{
	if(id > 0 && id <= 32){
		if(isConnected(id) && !is_user_bot(id))
			show_menu(id, 0, "\n", 1);	// destroy all menus
		AllMenusBlocked[id] = true;
	}else if(id == FOR_ALL){
		for(id=1; id<=32; id++){
			if(isConnected(id) && !is_user_bot(id))
				show_menu(id, 0, "\n", 1);	// destroy all menus
			AllMenusBlocked[id] = true;
		}
	}
}
public unblockAllMenus(id)
{
	if(id > 0 && id <= 32)
		AllMenusBlocked[id] = false;
	else if(id == FOR_ALL)
		for(id=1; id<=32; id++)
			AllMenusBlocked[id] = false;
}

public showMainMenu(id)
{
	isMenuOpened[id][MAIN_MENU] = true; // unused now
	
	//Skills_updateSkillIcons(id); // remove skill icons while in menu

	new menu = menu_create("\yMenu: ", "mainMenuHandler");
	new menu1[65], menu3[65];
	
	formatex(menu1, 64, "\wChange class \y[%s Lv %d]", getClassName(id), getLevel(id));
	formatex(menu3, 64, "\rGame Mode: %s", GameModeString);
	
	//if OutpostDef || Siege || TvT
	//formatex(menu4, 64, "\dTime left: %s", 	// @TODO

	menu_additem(menu, menu1, "1");
	//menu_addblank(menu, 0);
	menu_additem(menu, menu3, "2");
	menu_addblank(menu, 0);
	menu_additem(menu, "\wClose", "0");
	menu_setprop(menu, MPROP_PERPAGE, 0);
	//menu_setprop(menu, MPROP_EXIT, MEXIT_ALL);
	//menu_setprop(menu, MEXIT_ALL, 0);

	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}

public mainMenuHandler(id, menu, item)
{
	if(item == MENU_EXIT){
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}

	new data[3], name[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charsmax(data), name, charsmax(name), callback);
	new selected = str_to_num(data);
	switch(selected)
	{
		case 1: {
			menu_destroy(menu);
			if(canShowMenu(id))
				classMenuShow(id);
			return PLUGIN_HANDLED;
		}
		case 2: {
			showMainMenu(id);
		}
	}

	return PLUGIN_HANDLED;
}

public classMenuShow(id)
{
	isMenuOpened[id][CLASS_MENU] = true;
	
	//Skills_updateSkillIcons(id); // remove skill icons while in menu

	new menu = menu_create("\yClass menu: ", "classMenuHandler")
	new menu1[64],menu2[64],menu3[64],menu4[64],menu5[64],menu6[64],menu7[64],menu8[64];

	if(getClass(id) == CLASS_A)
		formatex(menu1, charsmax(menu1), "\wLv %2d  \y%s", PlayerLvl[id][CLASS_A], CLASSNAME_A);
	else
		formatex(menu1, charsmax(menu1), "\dLv %2d  \d%s", PlayerLvl[id][CLASS_A], CLASSNAME_A);
	if(getClass(id) == CLASS_B)
		formatex(menu2, charsmax(menu2), "\wLv %2d  \y%s", PlayerLvl[id][CLASS_B], CLASSNAME_B);
	else
		formatex(menu2, charsmax(menu2), "\dLv %2d  \d%s", PlayerLvl[id][CLASS_B], CLASSNAME_B);
	if(getClass(id) == CLASS_C)
		formatex(menu3, charsmax(menu3), "\wLv %2d  \y%s", PlayerLvl[id][CLASS_C], CLASSNAME_C);
	else
		formatex(menu3, charsmax(menu3), "\dLv %2d  \d%s", PlayerLvl[id][CLASS_C], CLASSNAME_C);
	if(getClass(id) == CLASS_D)
		formatex(menu4, charsmax(menu4), "\wLv %2d  \y%s", PlayerLvl[id][CLASS_D], CLASSNAME_D);
	else
		formatex(menu4, charsmax(menu4), "\dLv %2d  \d%s", PlayerLvl[id][CLASS_D], CLASSNAME_D);
	if(getClass(id) == CLASS_E)
		formatex(menu5, charsmax(menu5), "\wLv %2d  \y%s", PlayerLvl[id][CLASS_E], CLASSNAME_E);
	else
		formatex(menu5, charsmax(menu5), "\dLv %2d  \d%s", PlayerLvl[id][CLASS_E], CLASSNAME_E);
	if(getClass(id) == CLASS_F)
		formatex(menu6, charsmax(menu6), "\wLv %2d  \y%s", PlayerLvl[id][CLASS_F], CLASSNAME_F);
	else
		formatex(menu6, charsmax(menu6), "\dLv %2d  \d%s", PlayerLvl[id][CLASS_F], CLASSNAME_F);
	if(getClass(id) == CLASS_G)
		formatex(menu7, charsmax(menu7), "\wLv %2d  \y%s", PlayerLvl[id][CLASS_G], CLASSNAME_G);
	else
		formatex(menu7, charsmax(menu7), "\dLv %2d  \d%s", PlayerLvl[id][CLASS_G], CLASSNAME_G);
	if(getClass(id) == CLASS_H)
		formatex(menu8, charsmax(menu8), "\wLv %2d  \y%s", PlayerLvl[id][CLASS_H], CLASSNAME_H); //^n
	else
		formatex(menu8, charsmax(menu8), "\dLv %2d  \d%s", PlayerLvl[id][CLASS_H], CLASSNAME_H); //^n
	
	menu_additem(menu, menu1, "1");
	menu_additem(menu, menu2, "2");
	menu_additem(menu, menu3, "3");
	menu_additem(menu, menu4, "4");
	menu_additem(menu, menu5, "5");
	menu_additem(menu, menu6, "6");
	menu_additem(menu, menu7, "7");
	menu_additem(menu, menu8, "8");
	
	//if(GameMode != MODE_LAST_HERO){
	menu_addblank(menu, 0);
	menu_additem(menu, "\wBack", "9");
	//}
	
	menu_setprop(menu, MPROP_PERPAGE, 0);

	//menu_setprop(menu, MPROP_EXIT, MEXIT_ALL);
	//menu_setprop(menu, MEXIT_ALL, 0);

	menu_display(id, menu, 0);

	return PLUGIN_HANDLED;
}

public classMenuHandler(id, menu, item)
{
	if(item == MENU_EXIT){
		menu_destroy(menu);
		//showMainMenu(id);
		return PLUGIN_HANDLED;
	}

	new data[3], name[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charsmax(data), name, charsmax(name), callback);
	new selected = str_to_num(data);
	switch(selected)
	{
		case 1..8: {
			switch(GameMode){
				case MODE_OUTPOST_DEFENSE, MODE_CASTLE_SIEGE, MODE_TEAM_VS_TEAM: 
				{
					if(!is_user_bot(id)){
						setClassById(id, selected);
						classMenuShow(id);
					}else{
						setClassRequest(id, random_num(1,8));
					}
				}
				case MODE_LAST_HERO:
				{
					if(!is_user_bot(id)){
						setClassRequest(id, selected);
						classMenuShow(id);
						client_cmd(id, "spk %s", S_CLICK);
					}else{
						setClassRequest(id, random_num(1,8));
					}
				}
			}
		}
		case 9: 
		{
			menu_destroy(menu);
			isMenuOpened[id][CLASS_MENU] = false;

			if(GameMode == MODE_LAST_HERO)
				showEventMenu(id);
			else
				showMainMenu(id);
		}
	}

	return PLUGIN_HANDLED;
}

public setClassById(id, index)
{
	if(getClass(id) == index){ // if previous class equals selected
		client_print(id, print_chat, "You already is %s.", getClassName(id));
		client_cmd(id, "spk %s", SYS_IMPOSSIBLE);
		SetClassReq[id] = false;
	}
	else{
		client_print(id, print_chat, "Your class will be changed to %s at next respawn.", getClassNameFromInd(index));
		client_cmd(id, "spk %s", S_CLICK);
		SetClassReq[id] = true;
		SetClassReqInd[id] = index;
	}

	return PLUGIN_HANDLED;
}

public setClassRequest(id, index)
{
	//	@TODO: if index > 0 && index <= MAX_CLASSID

	PlayerData[id][CLASS_ID] = index;
	PlayerData[id][CUR_LEVEL] = PlayerLvl[id][index];
	PlayerData[id][CUR_EXP] = PlayerExp[id][index];
}

public Player_tryLoad(id)
{
//	if(isConnected(id))
//	{
		new ErrorCode;
		new SQL_ErrorString[512];
		new Handle:SqlConnection = SQL_Connect(SQL_Tuple, ErrorCode, SQL_ErrorString, 511);

		if(SqlConnection == Empty_Handle){
			Log(FATAL, "Player", "MySQL connection failed!");
			pluginFail(SQL_ErrorString, id);
		}

//		SQL_QuoteString(SqlConnection, player, 32, player);
//		http://amxxmodx.ru/sqlx/383-sql_quotestring-funkciya-ekraniruet-odinarnye-kavychki-v-zaprose.html

		new player[MAX_CHARNAME_LENGTH]
		//get_user_info(id, "name", player, MAX_CHARNAME_LENGTH-1);
		get_user_name(id, player, MAX_CHARNAME_LENGTH-1);

		new Handle:SelectExistFromCharacters = SQL_PrepareQuery(SqlConnection, "SELECT * FROM `characters` WHERE char_name = '%s';", player);
		if(!SQL_Execute(SelectExistFromCharacters)){
			SQL_QueryError(SelectExistFromCharacters, SQL_ErrorString, 511);
			Log(FATAL, "Player", "Error on loading player %s", player);
			pluginFail(SQL_ErrorString, id);
		}

		// if character with this name exists
		if(SQL_NumResults(SelectExistFromCharacters) > 0) 
		{
			new Handle:SelectFromOnline = SQL_PrepareQuery(SqlConnection, "SELECT char_name FROM `online` WHERE char_name='%s'", player);
			if(!SQL_Execute(SelectFromOnline)){
				SQL_QueryError(SelectFromOnline, SQL_ErrorString, 511);
				Log(FATAL, "Player", "Error on loading player %s", player);
				pluginFail(SQL_ErrorString, id);
			}

			if(SQL_NumResults(SelectFromOnline) > 0)
			{
				server_cmd("kick #%d ^"This account is already in use. \nAccess denied.^"", get_user_userid(id));
			}
			else
			{
				if(isBot(id))
				{
					nextLoadData(id, "", 0);
					Log(DEBUG, "Player", "Loading data for bot '%s'", player);
				}
				else
				{
					new password[MAX_PASSWORD_LENGTH];
					get_user_info(id, CONNECTION_STRING, password, MAX_PASSWORD_LENGTH-1)
					//Log(DEBUG, "Player", "get_user_info(%s, %s)", CONNECTION_STRING, password);

					new Handle:SelectFromCharacters = SQL_PrepareQuery(SqlConnection, "SELECT password FROM `characters` WHERE char_name = '%s';", player);
					if(SQL_Execute(SelectFromCharacters))
					{
						//Log(DEBUG, "Player", "SELECT password FROM characters WHERE char_name = %s", player);
						
						if(SQL_NumResults(SelectFromCharacters) > 0)
						{
							//Log(DEBUG, "Player", "SQL_NumResults(SelectFromCharacters) > 0");
							
							new db_password[MAX_PASSWORD_LENGTH];
							SQL_ReadResult(SelectFromCharacters, 0, db_password, MAX_PASSWORD_LENGTH-1);

							if(equal(password, db_password))
							{
								//Log(DEBUG, "Player", "if(equal(%s, %s))", password, db_password);

								new Handle:SelectBannedCharacter = SQL_PrepareQuery(SqlConnection, "SELECT ban_acc, ban_acc_reason FROM `characters` WHERE char_name='%s'", player);
								if(SQL_Execute(SelectBannedCharacter))
								{
									if(SQL_NumResults(SelectBannedCharacter) > 0)
									{
										new ban_acc = SQL_ReadResult(SelectBannedCharacter, 0);
										new ban_acc_reason[256];
										SQL_ReadResult(SelectBannedCharacter, 1, ban_acc_reason, 255);
										
										// F/TODO:	"This account has been suspended for %d day(s)."
										//	USE: ban_acc value for days amount
										if(ban_acc > 0)
										{
											server_cmd("kick #%d ^"This account has been banned. Reason: %s^"", get_user_userid(id), ban_acc_reason);
											//server_cmd("kick #%d ^"This account has been banned.\nReason: %s^"", get_user_userid(id), ban_acc_reason);
										}
										else{
											Log(DEBUG, "Player", "Loading data for player %s", player);
											nextLoadData(id, "", 0);
										}
									}
								}
								else{
									Log(DEBUG, "Player", "Error on loading player %s", player);
									SQL_QueryError(SelectBannedCharacter, SQL_ErrorString, 511);
									pluginFail(SQL_ErrorString, id);
								}
								SQL_FreeHandle(SelectBannedCharacter);
							}
							else{
								server_cmd("kick #%d ^"Password does not match this account.^"", get_user_userid(id));
							}
						}
					}
					else{
						SQL_QueryError(SelectFromCharacters, SQL_ErrorString, 511);
						pluginFail(SQL_ErrorString, id);
					}

					SQL_FreeHandle(SelectFromCharacters);
				}
			}
			SQL_FreeHandle(SelectFromOnline);
		}
		else if(is_user_bot(id))
		{
			Log(DEBUG, "Player", "Register bot '%s'", player);
			registerPlayer(id, "");
		}
		else{
			//Log(DEBUG, "Player", "kick #%d Account does not exist.", get_user_userid(id));
			server_cmd("kick #%d ^"Account does not exist.^"", get_user_userid(id));
		}

		SQL_FreeHandle(SelectExistFromCharacters);
		SQL_FreeHandle(SqlConnection);
//	}
}

public weaponEnchantFormatter(str1[], str2[]) // num1, num2
{
	new res[3];

	if(equali(str1, "0"))
		format(res, 2, "%s", str2); // aN
	else
		format(res, 2, "%s%s", str1, str2); // aNN

	return str_to_num(res);
}
stock encR()
{
	new str[3];
	format(str, 2, "%d%d", random_num(0,2), random_num(0,9));
	return str;
}
public weaponEnchantRandomizer()
{
	new str[26];
	format(str, 25, "a%sb%sc%sd%se%sf%sg%sh%s", encR(),encR(),encR(),encR(),encR(),encR(),encR(),encR()); // OMG!
	return str;
}
public weaponEnchantSaverStub(id, class_id)
{
	new out[4], temp[2], enc_level;
	enc_level = PlayerWeaponEnchant[id][class_id];

	switch(class_id)
	{
		case CLASS_A: { format(temp, 1, "a"); add(out, 3, temp, 1); }
		case CLASS_B: { format(temp, 1, "b"); add(out, 3, temp, 1); }
		case CLASS_C: { format(temp, 1, "c"); add(out, 3, temp, 1); }
		case CLASS_D: { format(temp, 1, "d"); add(out, 3, temp, 1); }
		case CLASS_E: { format(temp, 1, "e"); add(out, 3, temp, 1); }
		case CLASS_F: { format(temp, 1, "f"); add(out, 3, temp, 1); }
		case CLASS_G: { format(temp, 1, "g"); add(out, 3, temp, 1); }
		case CLASS_H: { format(temp, 1, "h"); add(out, 3, temp, 1); }
	}	
	if(enc_level > 9)
		format(out, 3, "%s%d", out, enc_level); // xx
	else
		format(out, 3, "%s0%d", out, enc_level); // 0x

	return out;
}
public weaponEnchantSaver(id)
{
	new str[26];
	// first element
	format(str, 25, "%s", weaponEnchantSaverStub(id, CLASS_A));
	// others
	format(str, 25, "%s%s", str, weaponEnchantSaverStub(id, CLASS_B));
	format(str, 25, "%s%s", str, weaponEnchantSaverStub(id, CLASS_C));
	format(str, 25, "%s%s", str, weaponEnchantSaverStub(id, CLASS_D));
	format(str, 25, "%s%s", str, weaponEnchantSaverStub(id, CLASS_E));
	format(str, 25, "%s%s", str, weaponEnchantSaverStub(id, CLASS_F));
	format(str, 25, "%s%s", str, weaponEnchantSaverStub(id, CLASS_G));
	format(str, 25, "%s%s", str, weaponEnchantSaverStub(id, CLASS_H));

	return str;
}

public nextLoadData(id, data[], len)
{
	new ErrorCode;
	new SQL_ErrorString[512];
	new Handle:SqlConnection = SQL_Connect(SQL_Tuple, ErrorCode, SQL_ErrorString, 511);

	if(SqlConnection == Empty_Handle){
		Log(FATAL, "Player", "MySQL connection failed!");
		pluginFail(SQL_ErrorString, id);
	}

	new player[MAX_CHARNAME_LENGTH];
	get_user_name(id, player, MAX_CHARNAME_LENGTH-1);

	new Handle:Query = SQL_PrepareQuery(SqlConnection, "SELECT * FROM `characters` WHERE char_name='%s'; ", player);
	
	if(!SQL_Execute(Query)){
		Log(FATAL, "Player", "nextLoadData(): Error on loading player %s", player);
		SQL_QueryError(Query, SQL_ErrorString, 511);
		pluginFail(SQL_ErrorString, id);
	}
	
	if(SQL_NumResults(Query) > 0)
	{
		//http://amxxmodx.ru/sqlx/191-sql_readresult-funkcya-poluchaet-rezultaty-sql-zaprosa.html
		//SQL_ReadResult (Обратите внимание, что нумерация колонок идет с 0, а не с 1.)

		/*	0	id					11	pvp				22	last_visit		33	e_lvl
			1	charname			12	pk				23	weapon_enchant	34	f_exp
			2	pass				13	deaths			24	a_exp			35	f_lvl
			3	email				14	hero			25	a_lvl			36	g_exp
			4	reg_ip				15	noble			26	b_exp			37	g_lvl
			5	last_Ip				16	online_time		27	b_lvl			38	h_exp
			6	title				17	played_maps		28	c_exp			39	h_lvl
			7	clan_tag			18	maps_win		29	c_lvl			40	ban_chat
			8	race				19	maps_lose		30	d_exp			41	ban_chat_reason
			9	classid				20	access_evel		31	d_lvl			42	ban_acc
			10	hair				21	birthday		32	e_exp			43	ban_acc_reason	*/

		PlayerData[id][RACE] = SQL_ReadResult(Query, 8);
		PlayerData[id][CLASS_ID] = SQL_ReadResult(Query, 9);
		PlayerData[id][HAIR] = SQL_ReadResult(Query, 10);
		
		PlayerData[id][CUR_PVP] = SQL_ReadResult(Query, 11);
		PlayerData[id][CUR_PK] = SQL_ReadResult(Query, 12);
		PlayerData[id][CUR_DEATHS] = SQL_ReadResult(Query, 13);

		/* Weapon enchant reading */
		new _enchants[26];
		SQL_ReadResult(Query, 23, _enchants, 25);
		trim(_enchants); // remove spaces
		//Log(DEBUG, "Player", "nextLoadData(): _enchants=%s (DB)", _enchants);

		// 0  1  2  3  4  5  6  7  8  9  10  11  12  13  14  15  16  17  18  19  20  21  22  23
		// a  0  0  b  0  0  c  0  0  d  0   0   e   0   0   f   0   0   g   0   0   h   0   0
		PlayerWeaponEnchant[id][CLASS_A] = weaponEnchantFormatter(_enchants[1], _enchants[2]); // a 00
		PlayerWeaponEnchant[id][CLASS_B] = weaponEnchantFormatter(_enchants[4], _enchants[5]); // b 00
		PlayerWeaponEnchant[id][CLASS_C] = weaponEnchantFormatter(_enchants[7], _enchants[8]); // c 00
		PlayerWeaponEnchant[id][CLASS_D] = weaponEnchantFormatter(_enchants[10], _enchants[11]); // d 00
		PlayerWeaponEnchant[id][CLASS_E] = weaponEnchantFormatter(_enchants[13], _enchants[14]); // e 00
		PlayerWeaponEnchant[id][CLASS_F] = weaponEnchantFormatter(_enchants[16], _enchants[17]); // f 00
		PlayerWeaponEnchant[id][CLASS_G] = weaponEnchantFormatter(_enchants[19], _enchants[20]); // g 00
		PlayerWeaponEnchant[id][CLASS_H] = weaponEnchantFormatter(_enchants[22], _enchants[23]); // h 00
		/* */

		PlayerExp[id][CLASS_A] = SQL_ReadResult(Query, 24);
		PlayerLvl[id][CLASS_A] = SQL_ReadResult(Query, 25);
		PlayerExp[id][CLASS_B] = SQL_ReadResult(Query, 26);
		PlayerLvl[id][CLASS_B] = SQL_ReadResult(Query, 27);
		PlayerExp[id][CLASS_C] = SQL_ReadResult(Query, 28);
		PlayerLvl[id][CLASS_C] = SQL_ReadResult(Query, 29);
		PlayerExp[id][CLASS_D] = SQL_ReadResult(Query, 30);
		PlayerLvl[id][CLASS_D] = SQL_ReadResult(Query, 31);
		PlayerExp[id][CLASS_E] = SQL_ReadResult(Query, 32);
		PlayerLvl[id][CLASS_E] = SQL_ReadResult(Query, 33);
		PlayerExp[id][CLASS_F] = SQL_ReadResult(Query, 34);
		PlayerLvl[id][CLASS_F] = SQL_ReadResult(Query, 35);
		PlayerExp[id][CLASS_G] = SQL_ReadResult(Query, 36);
		PlayerLvl[id][CLASS_G] = SQL_ReadResult(Query, 37);
		PlayerExp[id][CLASS_H] = SQL_ReadResult(Query, 38);
		PlayerLvl[id][CLASS_H] = SQL_ReadResult(Query, 39);
		
		// After data loads - set CUR_EXP, CUR_LEVEL values
		switch(getClass(id)){
			case CLASS_A: { setExp(id, PlayerExp[id][CLASS_A]);	setLevel(id, PlayerLvl[id][CLASS_A]); }
			case CLASS_B: {	setExp(id, PlayerExp[id][CLASS_B]);	setLevel(id, PlayerLvl[id][CLASS_B]); }
			case CLASS_C: { setExp(id, PlayerExp[id][CLASS_C]);	setLevel(id, PlayerLvl[id][CLASS_C]); }
			case CLASS_D: { setExp(id, PlayerExp[id][CLASS_D]);	setLevel(id, PlayerLvl[id][CLASS_D]); }
			case CLASS_E: { setExp(id, PlayerExp[id][CLASS_E]);	setLevel(id, PlayerLvl[id][CLASS_E]); }
			case CLASS_F: { setExp(id, PlayerExp[id][CLASS_F]);	setLevel(id, PlayerLvl[id][CLASS_F]); }
			case CLASS_G: { setExp(id, PlayerExp[id][CLASS_G]);	setLevel(id, PlayerLvl[id][CLASS_G]); }
			case CLASS_H: { setExp(id, PlayerExp[id][CLASS_H]);	setLevel(id, PlayerLvl[id][CLASS_H]); }
		}
		
		// TODO: db field 'last_weapon'
		if(is_user_bot(id) && getClass(id) != CLASS_A)
			PlayerWeapon[id][WTYPE] = WTYPE_KNIFE_CLASS;
		else 
			PlayerWeapon[id][WTYPE] = WTYPE_HANDS;
	}
	else{
		Log(ERROR, "Player", "nextLoadData(): No query results for player %s", player);
		server_cmd("kick #%d ^"Internal server error. Please report to admin!^"", get_user_userid(id));
	}

	SQL_FreeHandle(Query);
	SQL_FreeHandle(SqlConnection);
}

public registerPlayer(id, data[]) // now, only for register bots
{
	new SQL_ErrorCode;
	new SQL_ErrorString[512];
	new Handle:SqlConnection = SQL_Connect(SQL_Tuple, SQL_ErrorCode, SQL_ErrorString, 511);
	if(SqlConnection == Empty_Handle){
		Log(FATAL, "Database", "MySQL connection failed!");
		pluginFail(SQL_ErrorString, id);
	}
	else
	{
		new player[MAX_CHARNAME_LENGTH]
		get_user_name(id, player, MAX_CHARNAME_LENGTH-1);
		
		static query[2048];
		static len; len = 0;
		
		len += formatex(query[len], charsmax(query)-len, "INSERT INTO `characters` (`id`, ");
		len += formatex(query[len], charsmax(query)-len, "`char_name`, ");
		len += formatex(query[len], charsmax(query)-len, "`password`, ");
		len += formatex(query[len], charsmax(query)-len, "`email`, ");
		len += formatex(query[len], charsmax(query)-len, "`reg_ip`, ");
		len += formatex(query[len], charsmax(query)-len, "`last_ip`, ");
		len += formatex(query[len], charsmax(query)-len, "`title`, ");
		len += formatex(query[len], charsmax(query)-len, "`clan_tag`, ");
		len += formatex(query[len], charsmax(query)-len, "`race`, ");
		len += formatex(query[len], charsmax(query)-len, "`classid`, ");
		len += formatex(query[len], charsmax(query)-len, "`hair`, ");
		len += formatex(query[len], charsmax(query)-len, "`pvp`, ");
		len += formatex(query[len], charsmax(query)-len, "`pk`, ");
		len += formatex(query[len], charsmax(query)-len, "`deaths`, ");
		len += formatex(query[len], charsmax(query)-len, "`hero`, ");
		len += formatex(query[len], charsmax(query)-len, "`noble`, ");
		len += formatex(query[len], charsmax(query)-len, "`online_time`, ");
		len += formatex(query[len], charsmax(query)-len, "`played_maps`, ");
		len += formatex(query[len], charsmax(query)-len, "`maps_win`, ");
		len += formatex(query[len], charsmax(query)-len, "`maps_lose`, ");
		len += formatex(query[len], charsmax(query)-len, "`access_level`, ");
		len += formatex(query[len], charsmax(query)-len, "`birthday`, ");
		len += formatex(query[len], charsmax(query)-len, "`last_visit`, ");
		len += formatex(query[len], charsmax(query)-len, "`weapon_enchant`, ");
		len += formatex(query[len], charsmax(query)-len, "`a_exp`, `a_lvl`, ");
		len += formatex(query[len], charsmax(query)-len, "`b_exp`, `b_lvl`, ");
		len += formatex(query[len], charsmax(query)-len, "`c_exp`, `c_lvl`, ");
		len += formatex(query[len], charsmax(query)-len, "`d_exp`, `d_lvl`, ");
		len += formatex(query[len], charsmax(query)-len, "`e_exp`, `e_lvl`, ");
		len += formatex(query[len], charsmax(query)-len, "`f_exp`, `f_lvl`, ");
		len += formatex(query[len], charsmax(query)-len, "`g_exp`, `g_lvl`, ");
		len += formatex(query[len], charsmax(query)-len, "`h_exp`, `h_lvl`, ");
		len += formatex(query[len], charsmax(query)-len, "`ban_chat`, ");
		len += formatex(query[len], charsmax(query)-len, "`ban_chat_reason`, ");
		len += formatex(query[len], charsmax(query)-len, "`ban_acc`, ");
		len += formatex(query[len], charsmax(query)-len, "`ban_acc_reason`");
		len += formatex(query[len], charsmax(query)-len, ") VALUES ");
		
		new botpsw[] = "a16cc34c751f75adb82a8a93366391e9";
		new botmail[] = "bot@botmail.com";
		new botregip[] = "0.0.0.0";
		new botrace = random_num(1,4);
		new botclass = random_num(MIN_CLASS_ID, MAX_CLASS_ID);
		
		// hats in db (1-20), in *mdl submodel (0-19)
		// 0 = no hair (in db only)
		new _hair = random_num(0, 20/*H_WIZARD_HAT+1*/);

		new _time[128];
		format_time(_time, 127, "%d.%m.%Y %H:%M"); //:%S

		len += formatex(query[len], charsmax(query)-len, "(NULL, '%s', ", player);				// id, char_name
		len += formatex(query[len], charsmax(query)-len, "'%s', '%s', ", botpsw, botmail);		// password, email
		len += formatex(query[len], charsmax(query)-len, "'%s', '%s', ", botregip, botregip);	// reg_ip, last_ip
		len += formatex(query[len], charsmax(query)-len, "'', '', ");							// title, clan_tag
		len += formatex(query[len], charsmax(query)-len, "'%d', '%d', ", botrace, botclass);	// race, classid
		len += formatex(query[len], charsmax(query)-len, "'%d', '0', '0', '0', ", _hair);		// hair, pvp, pk, deaths
		len += formatex(query[len], charsmax(query)-len, "'0', '0', ");							// hero, noble
		len += formatex(query[len], charsmax(query)-len, "'0', '0', ");							// online_time, played_maps
		len += formatex(query[len], charsmax(query)-len, "'0', '0', '0', ");					// maps_win, maps_lose, accesslevel
		len += formatex(query[len], charsmax(query)-len, "'%s', '%s', ", _time, _time);			// birthday, last_visit
		len += formatex(query[len], charsmax(query)-len, "'%s', ", weaponEnchantRandomizer());	// weapon_enchant
		len += formatex(query[len], charsmax(query)-len, "'0', '1', ");							// a_exp, a_lvl
		len += formatex(query[len], charsmax(query)-len, "'0', '1', ");							// b_exp, b_lvl
		len += formatex(query[len], charsmax(query)-len, "'0', '1', ");							// c_exp, c_lvl
		len += formatex(query[len], charsmax(query)-len, "'0', '1', ");							// d_exp, d_lvl
		len += formatex(query[len], charsmax(query)-len, "'0', '1', ");							// e_exp, e_lvl
		len += formatex(query[len], charsmax(query)-len, "'0', '1', ");							// f_exp, f_lvl
		len += formatex(query[len], charsmax(query)-len, "'0', '1', ");							// g_exp, g_lvl
		len += formatex(query[len], charsmax(query)-len, "'0', '1', ");							// h_exp, h_lvl
		len += formatex(query[len], charsmax(query)-len, "'0', '', ");							// ban_chat, ban_chat_reason
		len += formatex(query[len], charsmax(query)-len, "'0', ''); ");							// ban_acc, ban_acc_reason

		new Handle:InsertIntoCharacters = SQL_PrepareQuery(SqlConnection, query);
		
		if(!SQL_Execute(InsertIntoCharacters)){
			SQL_QueryError(InsertIntoCharacters, SQL_ErrorString, 511);
			pluginFail(SQL_ErrorString, id);
		}else{
			Log(INFO, "Player", "Bot account '%s' created", player);
		}

		SQL_FreeHandle(InsertIntoCharacters);

		// bugfix: bot account created but data not loaded
		Log(DEBUG, "Player", "Loading data for bot '%s'", player);
		nextLoadData(id, "", 0);
	}
	
	SQL_FreeHandle(SqlConnection);
}

public Player_classUpdate(id)
{
	new _class = getClass(id);
	
	if(getExp(id) > PlayerExp[id][_class])
		PlayerExp[id][_class] = getExp(id);
	if(getLevel(id) > PlayerLvl[id][_class])
		PlayerLvl[id][_class] = getLevel(id);
}

public Player_saveData(id)
{
	new ErrorCode;
	new SQL_ErrorString[512];
	new Handle:SqlConnection = SQL_Connect(SQL_Tuple, ErrorCode, SQL_ErrorString, 511);
	if(SqlConnection == Empty_Handle){
		Log(FATAL, "Player", "MySQL connection failed!");
		pluginFail(SQL_ErrorString, id);
	}

	new player[MAX_CHARNAME_LENGTH];
	get_user_name(id, player, MAX_CHARNAME_LENGTH-1);
	
	new Handle:SelectFromChars = SQL_PrepareQuery(SqlConnection, "SELECT * FROM `characters` WHERE char_name = '%s';", player);
	
	if(!SQL_Execute(SelectFromChars)){
		SQL_QueryError(SelectFromChars, SQL_ErrorString, 511);
		pluginFail(SQL_ErrorString, id);
	}
	else if(SQL_NumResults(SelectFromChars) > 0)
	{
		// Getting previous pvp,pk,deaths for add to current
		new _pvp = SQL_ReadResult(SelectFromChars, 11);
		new _pk = SQL_ReadResult(SelectFromChars, 12);
		new _deaths = SQL_ReadResult(SelectFromChars, 13);
		
		/*	0	id					11	pvp				22	last_visit		33	e_lvl
			1	charname			12	pk				23	weapon_enchant	34	f_exp
			2	pass				13	deaths			24	a_exp			35	f_lvl
			3	email				14	hero			25	a_lvl			36	g_exp
			4	reg_ip				15	noble			26	b_exp			37	g_lvl
			5	last_Ip				16	online_time		27	b_lvl			38	h_exp
			6	title				17	played_maps		28	c_exp			39	h_lvl
			7	clan_tag			18	maps_win		29	c_lvl			40	ban_chat
			8	race				19	maps_lose		30	d_exp			41	ban_chat_reason
			9	classid				20	access_evel		31	d_lvl			42	ban_acc
			10	hair				21	birthday		32	e_exp			43	ban_acc_reason	*/

		/* Update Query */
		static query[1024];
		static len; len = 0;
		len += formatex(query[len], charsmax(query)-len, "UPDATE `characters` SET ");
		
		new last_ip[MAX_IP_ADDR_LENGTH];
		get_user_ip(id, last_ip, MAX_IP_ADDR_LENGTH-1, 1);
		len += formatex(query[len], charsmax(query)-len, "`last_ip`='%s', ", last_ip);
		
		len += formatex(query[len], charsmax(query)-len, "`classid`='%d', ", PlayerData[id][CLASS_ID]);
		
		len += formatex(query[len], charsmax(query)-len, "`pvp`='%d', ", Stats_getPvP(id) + _pvp);
		len += formatex(query[len], charsmax(query)-len, "`pk`='%d', ", Stats_getPK(id) + _pk);	// now this is deaths!
		len += formatex(query[len], charsmax(query)-len, "`deaths`='%d', ", Stats_getDeaths(id) + _deaths);
		
		new _time[128];
		format_time(_time, 127, "%d.%m.%Y %H:%M"); //:%S
		len += formatex(query[len], charsmax(query)-len, "`last_visit`='%s', ", _time);

		len += formatex(query[len], charsmax(query)-len, "`weapon_enchant`='%s', ", weaponEnchantSaver(id));

		len += formatex(query[len], charsmax(query)-len, "`a_exp`='%d', ", PlayerExp[id][CLASS_A]);
		len += formatex(query[len], charsmax(query)-len, "`a_lvl`='%d', ", PlayerLvl[id][CLASS_A]);
		len += formatex(query[len], charsmax(query)-len, "`b_exp`='%d', ", PlayerExp[id][CLASS_B]);
		len += formatex(query[len], charsmax(query)-len, "`b_lvl`='%d', ", PlayerLvl[id][CLASS_B]);
		len += formatex(query[len], charsmax(query)-len, "`c_exp`='%d', ", PlayerExp[id][CLASS_C]);
		len += formatex(query[len], charsmax(query)-len, "`c_lvl`='%d', ", PlayerLvl[id][CLASS_C]);
		len += formatex(query[len], charsmax(query)-len, "`d_exp`='%d', ", PlayerExp[id][CLASS_D]);
		len += formatex(query[len], charsmax(query)-len, "`d_lvl`='%d', ", PlayerLvl[id][CLASS_D]);
		len += formatex(query[len], charsmax(query)-len, "`e_exp`='%d', ", PlayerExp[id][CLASS_E]);
		len += formatex(query[len], charsmax(query)-len, "`e_lvl`='%d', ", PlayerLvl[id][CLASS_E]);
		len += formatex(query[len], charsmax(query)-len, "`f_exp`='%d', ", PlayerExp[id][CLASS_F]);
		len += formatex(query[len], charsmax(query)-len, "`f_lvl`='%d', ", PlayerLvl[id][CLASS_F]);
		len += formatex(query[len], charsmax(query)-len, "`g_exp`='%d', ", PlayerExp[id][CLASS_G]);
		len += formatex(query[len], charsmax(query)-len, "`g_lvl`='%d', ", PlayerLvl[id][CLASS_G]);
		len += formatex(query[len], charsmax(query)-len, "`h_exp`='%d', ", PlayerExp[id][CLASS_H]);
		len += formatex(query[len], charsmax(query)-len, "`h_lvl`='%d' ", PlayerLvl[id][CLASS_H]);

		len += formatex(query[len], charsmax(query)-len, "WHERE `char_name`='%s'; ", player);

//		SQL_ThreadQuery(SQL_Tuple, "QueryHandler", query);
		new Handle:UpdateCharacters = SQL_PrepareQuery(SqlConnection, query);
		if(!SQL_Execute(UpdateCharacters)){
			SQL_QueryError(UpdateCharacters, SQL_ErrorString, 511);
			pluginFail(SQL_ErrorString, id);
		}else{
			Log(DEBUG, "Player", "Saving data for player %s", player);
		}

		SQL_FreeHandle(UpdateCharacters);
	}
	else{
		Log(ERROR, "Player", "[MySQL] Player '%s' not found.", player);
	}

	SQL_FreeHandle(SelectFromChars);
	SQL_FreeHandle(SqlConnection);
}

public Player_statusText()
{
	for(new id=1; id<=GMaxPlayers; id++)
	{
		if(!isBot(id) /*&& isConnected(id)*/ && haveTeam(id) && isAlive(id))
		{
			if(haveClass(id))
			{
				static buf[128];
				new percent[3] = "%%";
				// F / TODO: apply format for exp, eg (0.05%)
				formatex(buf, 127, "%s  Lvl %d  Exp %d%s", getClassName(id), getLevel(id), getExp(id), percent);
				//formatex(buf, 127, "%s  Lvl %d  Exp %.1f%s", getClassName(id), getLevel(id), getExpPercent(getLevel(id), getExp(id)), percent);
				message_begin(MSG_ONE_UNRELIABLE, gmsgStatusText, _, id);
				write_byte(0);
				write_string(buf);
				message_end();
			}
		}
	}
}

public Player_statusTextClear(id)
{
	message_begin(MSG_ONE_UNRELIABLE, gmsgStatusText, _, id);
	write_byte(0);
	write_string(" ");
	message_end();
}
public Player_statusTextClearAll()
{
	for(new id=1; id<=GMaxPlayers; id++){
		if(!isBot(id) && isAlive(id))
			Player_statusTextClear(id);
	}
}

public Player_nextWeapon(id)
{
	switch(PlayerWeapon[id][WTYPE]) // current weapon
	{
		case WTYPE_HANDS: 
		{
			if(getClass(id) != CLASS_A){
				engclient_cmd(id, "weapon_knife");
				PlayerWeapon[id][WTYPE] = WTYPE_KNIFE_CLASS;
			}else{
				engclient_cmd(id, "weapon_mp5navy");
				PlayerWeapon[id][WTYPE] = WTYPE_BOW;
			}
		}
		case WTYPE_KNIFE_CLASS: 
		{
			if(WTYPE_OTHER_ALLOWED) // Const.inl
			{
				// @WAIT for events only
			}else{
				PlayerWeapon[id][WTYPE] = WTYPE_KNIFE_OTHER;
				Player_nextWeapon(id);
			}
		}
		case WTYPE_KNIFE_OTHER: 
		{
			if(getClass(id) == CLASS_A){ // if its archer class
				engclient_cmd(id, "weapon_mp5navy");
				PlayerWeapon[id][WTYPE] = WTYPE_BOW;
			}else{
				engclient_cmd(id, "weapon_knife");
				PlayerWeapon[id][WTYPE] = WTYPE_HANDS;
			}
		}
		case WTYPE_BOW: 
		{
			engclient_cmd(id, "weapon_knife");
			PlayerWeapon[id][WTYPE] = WTYPE_HANDS;
		}
	}

	ExecuteHamB(Ham_Item_Deploy, get_pdata_cbase(id, 373 /* m_pActiveItem */, OFFSET_LINUX));
	
	setWeaponEnchant(id);

	return PLUGIN_HANDLED;
}

/**
 * Player sit/sit_idle/stand animations.
 */
public Player_sit(id)
{
	PlayerAct[id] = ACT_SIT;
	set_task(1.8, "Player_sitIdle", id+TASK_PLAYER_SIT_IDLE);
}
public Player_sitIdle(id)
{
	id -= TASK_PLAYER_SIT_IDLE;
	PlayerAct[id] = ACT_SIT_IDLE;
}
public Player_stand(id)
{
	PlayerAct[id] = ACT_STAND;
	set_task(1.4 /*1.7*/, "Player_noAct", id+TASK_PLAYER_NO_ACT);
}
public Player_noAct(id)
{
	id -= TASK_PLAYER_NO_ACT;
	PlayerAct[id] = ACT_NONE;
}

/**
 * Player can be valid only if ...
 */
public bool:isPlayerValid(id)
{
	// You only need to do is_user_alive() as it has a built-in is_user_connected() check.
	if(/*warmupConfirmed() && isPlayerEnt(id) && isConnected(id) &&*/ isAlive(id) && haveTeam(id))
		return true;
	return false;
}
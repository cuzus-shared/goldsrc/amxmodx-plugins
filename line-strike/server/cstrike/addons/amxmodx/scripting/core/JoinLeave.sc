// --------------------------------------------------
// JoinLeave
// --------------------------------------------------

public JoinLeave_clientConnect(id)
{
	if(get_user_flags(id) & ADMIN_IMMUNITY){ // admin connect sound
		client_cmd(0, "spk %s", S_SOIL_SHOT);
		//emit_sound(0, CHAN_AUTO, S_SOIL_SHOT, 0.8, ATTN_STATIC, 0, PITCH_NORM);
	}
	
	new message[128], name[MAX_CHARNAME_LENGTH];
	get_user_name(id, name, MAX_CHARNAME_LENGTH-1);
	formatex(message, 127, "%s is connecting to server.", name);
	set_hudmessage(225,225,0, 0.8, -1.0, 0, 6.0, 6.0, 0.5, 0.15, 3); /* channel 3 */
	ShowSyncHudMsg(0, SkillsSyncHud, message);
	
	return PLUGIN_CONTINUE;
}

public JoinLeave_clientDisconnect(id)
{
	if(warmupConfirmed())
		for(new pl=1; pl<=GMaxPlayers; pl++)
			if(!is_user_bot(pl))
				if(get_user_team(pl) == get_user_team(id))
					client_cmd(pl, "spk %s", SYS_PARTY_LEAVE);

	new message[128], name[MAX_CHARNAME_LENGTH];
	get_user_name(id, name, MAX_CHARNAME_LENGTH-1);
	formatex(message, 127, "%s has left the game.", name);
	set_hudmessage(225,0,0, 0.8, -1.0, 0, 6.0, 6.0, 0.5, 0.15, 3); /* channel 3 */
	ShowSyncHudMsg(0, SkillsSyncHud, message);

	return PLUGIN_CONTINUE;
}

public JoinLeave_clientPutInServer(id)
{
	if(warmupConfirmed())
		for(new pl=1; pl<=GMaxPlayers; pl++)
			if(!is_user_bot(pl))
				if(get_user_team(pl) == get_user_team(id))
					client_cmd(pl, "spk %s", SYS_PARTY_INVITE);
	
	new message[128], name[MAX_CHARNAME_LENGTH];
	get_user_name(id, name, MAX_CHARNAME_LENGTH-1);
	formatex(message, 127, "%s joined the game.", name);
	set_hudmessage(0,225,0, 0.8, -1.0, 0, 6.0, 6.0, 0.5, 0.15, 3); /* channel 3 */
	ShowSyncHudMsg(0, SkillsSyncHud, message);

	return PLUGIN_CONTINUE;
}
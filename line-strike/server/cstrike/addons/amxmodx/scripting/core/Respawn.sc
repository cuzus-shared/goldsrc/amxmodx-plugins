// --------------------------------------------------
// Respawn
// --------------------------------------------------

public Respawn_clientDisconnect(id)
{
	//respawn_timeleft[id] = 0;
	remove_task(TASK_RESPAWN_ME+id);
	remove_task(TASK_REMOVE_PROTECTION+id);
}

public Respawn_me(id)
{
	if(task_exists(TASK_RESPAWN_ME+id) || isAlive(id) || !haveTeam(id))
		return;
	respawnVillage(TASK_RESPAWN_ME+id);
}

public Respawn_hamPlayerKilledPre(victim, killer)
{
	remove_task(TASK_RESPAWN_ME+victim);
	remove_task(TASK_REMOVE_PROTECTION+victim);

	if(warmupConfirmed() && !EndOfMap)
	{
		if(haveTeam(victim) && !isAlive(victim))
		{
			if(GameMode == MODE_OUTPOST_DEFENSE)
			{
				if(!isBot(victim))
				{
					showRespawnMenu(victim);
					isMenuOpened[victim][RESPAWN_MENU] = true;
				}
				else // bots
				{
					if(randomChance(10)){ // chance to long resp
						set_task(Config[TO_VILLAGE_TIME] + randF(10.0,30.0), "respawnVillage", TASK_RESPAWN_ME+victim);
					}
					else{
						set_task(Config[TO_VILLAGE_TIME] + randF(0.1,5.0), "respawnVillage", TASK_RESPAWN_ME+victim);
					}
					//else
					//	respawnAtFlagPre(victim);
				}
			}
			else if(GameMode == MODE_TEAM_VS_TEAM) // TvT's instant respawn for all
			{
				destroyAllMenus(victim);
				set_task(Config[TO_VILLAGE_TIME], "respawnVillage", TASK_RESPAWN_ME+victim);
			}
		}
	}
}

public Respawn_countdown(id)
{
	id -= TASK_RESPAWN_COUNTDOWN;

	if(isAlive(id)){
		respawn_timeleft[id] = 0;
		return;
	}

	client_print(id, print_center, "Respawn in %i seconds", respawn_timeleft[id]);

	if(--respawn_timeleft[id] >= 1){
		set_task(1.0, "Respawn_countdown", id+TASK_RESPAWN_COUNTDOWN);
	}else{
		client_print(id, print_center," ");
		remove_task(id+TASK_RESPAWN_COUNTDOWN);
	}
}

public respawnAtFlagPre(id)
{
	if(!haveTeam(id) || (isAlive(id) && !pev(id, pev_iuser1)))
		return;

	new Float:delay = random_float(FLAG_RESPAWN_MIN, FLAG_RESPAWN_MAX);

	respawn_timeleft[id] = floatround(delay);
	Respawn_countdown(id+TASK_RESPAWN_COUNTDOWN);

	set_task(delay, "respawnAtFlag", TASK_RESPAWN_AT_FLAG+id);
}

public respawnAtFlag(id)	// TODO: no spawn protection & hp/mp 70%
{
	id -= TASK_RESPAWN_AT_FLAG;

	if(!haveTeam(id) || isAlive(id))
		return;

	ExecuteHamB(Ham_CS_RoundRespawn, id);
	engfunc(EngFunc_SetOrigin, id, corpse_origin[id]);	// TODO: HeadquartersOrigin[id]

	remove_task(TASK_RESPAWN_AT_FLAG+id);
}

public respawnVillage(id)
{
	id -= TASK_RESPAWN_ME;
	
	if(!haveTeam(id) || (isAlive(id) && !pev(id,pev_iuser1)))
		return;
	
	ExecuteHamB(Ham_CS_RoundRespawn, id);
	
	spawnProtectAdd(id);
	
	if(GameMode == MODE_OUTPOST_DEFENSE || GameMode == MODE_CASTLE_SIEGE)
		set_task(2.5, "spawnProtectRemove", TASK_REMOVE_PROTECTION+id); // 3 seconds spawn protected
	else if(GameMode == MODE_TEAM_VS_TEAM)
		set_task(1.0, "spawnProtectRemove", TASK_REMOVE_PROTECTION+id); // 1 second spawn protected
	else if(GameMode == MODE_LAST_HERO)
		set_task(float(LASTHERO_COUNTDOWN_TIME), "spawnProtectRemove", TASK_REMOVE_PROTECTION+id);

	remove_task(id+TASK_RESPAWN_ME);
}

public spawnProtectAdd(id)
{
	fm_set_user_godmode(id, 1);
	fm_set_rendering(id, kRenderFxGlowShell, 100, 100, 100, kRenderNormal, 8);
}

public spawnProtectRemove(id)
{
	id -= TASK_REMOVE_PROTECTION;
	
	if(!isAlive(id))
		return;
	
	fm_set_user_godmode(id, 0);
	fm_set_rendering(id); // reset
}

// TODO : headquarters respawn @wait game mode
/*public markDeathXYZ(id)
{
	new Float:origin[3], Float:angles[3], Float:v_angle[3];
	pev(id, pev_origin, origin);
	pev(id, pev_angles, angles);
	pev(id, pev_v_angle, v_angle);
	
	origin[2] += 32.0; // 0.0
	corpse_origin[id][0] = origin[0];
	corpse_origin[id][1] = origin[1];
	corpse_origin[id][2] = origin[2];
	
	corpse_angles[id] = angles;
	corpse_v_angle[id] = v_angle;
}*/

public showRespawnMenu(id)
{
	new _menu = menu_create("\yRespawn: ", "respMenu");

	// @l2(C1+ : To nearest village)
	// @l2(C3+ : To Village)
	
	menu_additem(_menu, "\wTo nearest village", "1", 0); // \y(instantly)
	
//	menu_additem(_menu, "\dYou have no choice :D", "2", 0);
	//menu_addblank(_menu, 0);
	
	//if OutpostDef
	//menu_additem(_menu, "\dFaction Portal \d(NONE)", "2", ADMIN_ADMIN);		//X: Y: Z: 
	// if Siege
	//menu_additem(_menu, "\dHeadquarters \d(NONE)", "3", ADMIN_ADMIN);
	
	menu_setprop(_menu, MPROP_EXIT, MEXIT_NEVER); // MEXIT_ALL
	
	menu_display(id, _menu, 0);
}

public respMenu(id, menu, item)
{
	if(item == MENU_EXIT){
		menu_destroy(menu)
		return PLUGIN_HANDLED;
	}
	
	new key[3], name[64], access, callback; // sz_data[6]
	menu_item_getinfo(menu, item, access, key, charsmax(key), name, charsmax(name), callback);
	
	switch(str_to_num(key))
	{
		case 1:	{
			ToVillage(id);
		}
		/*case 2: { // to near flag
			//respawnAtFlagPre(id);
		}
		case 3: { // headquarters at siege
		}*/
		default: {
			//ToVillage(id);
			showRespawnMenu(id);
		}
	}

	menu_destroy(menu);
	return PLUGIN_HANDLED;
}

public ToVillage(id)
{
	Create_ScreenFade(id, 3000, 10000, FFADE_OUT, 0,0,0,255, true);
	set_task(Config[TO_VILLAGE_TIME], "respawnVillage", TASK_RESPAWN_ME+id);
	isMenuOpened[id][RESPAWN_MENU] = false;
}
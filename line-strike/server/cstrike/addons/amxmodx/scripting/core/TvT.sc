// --------------------------------------------------
// Game Mode: Team vs Team
// --------------------------------------------------

public TvT_start()
{
	client_print(0, print_center, "Event started. Go to kill your enemies!");
	set_task(float(EVENT_TIME_TEAM_VS_TEAM) * 60.0, "TvT_eventEnd", TASK_TVT_COUNT_WIN);
}

public TvT_eventEnd()
{
	client_print(0, print_center, "Time is up!");
	GameMode_end(Stats_getTopTeam(), Stats_getTopStat("killsid"));
}

public TvT_endHtml(params[2] /*, taskid*/ ) //params[0] = winning_team, params[1] = top_player
{
	new html[MAX_HTML_CONTENT_LENGTH];
	new len = MAX_HTML_CONTENT_LENGTH-1;
	
	if(!getHtm(HTML_TVT_END, html))
		return;
	
	new buf[128], player; // format

	replace(html, len, "{winner_color}", params[0] == 1 ? COLOR_EAST : COLOR_WEST);
	replace(html, len, "{winner_faction}", params[0] == 1 ? TNAME_EAST : TNAME_WEST);

	player = Stats_getTopStat("killsid");
	replace(html, len, "{top_kills_teamcolor}", getTeam(player) == 1 ? COLOR_EAST : COLOR_WEST);
	replace(html, len, "{top_kills_player}", getName(player));
	format(buf, 127, "%d", Stats_getTopStat("killsnum"));
	replace(html, len, "{top_kills_amount}", buf);

	player = Stats_getTopStat("deathsid");
	replace(html, len, "{top_deaths_teamcolor}", getTeam(player) == 2 ? COLOR_WEST : COLOR_EAST);
	replace(html, len, "{top_deaths_player}", getName(player));
	format(buf, 127, "%d", Stats_getTopStat("deathsnum"));
	replace(html, len, "{top_deaths_amount}", buf);

	replace(html, len, "{team1_name}", TNAME_EAST);
	replace(html, len, "{team2_name}", TNAME_WEST);
	replace(html, len, "{color_team1}", COLOR_EAST);
	replace(html, len, "{color_team2}", COLOR_WEST);

	format(buf, 127, "%d", Stats_getTeamSco(TEAMID_EAST, "kills"));
	replace(html, len, "{team1_kills}", buf);
	format(buf, 127, "%d", Stats_getTeamSco(TEAMID_WEST, "kills"));
	replace(html, len, "{team2_kills}", buf);
	format(buf, 127, "%d", Stats_getTeamSco(TEAMID_EAST, "deaths"));
	replace(html, len, "{team1_deaths}", buf);
	format(buf, 127, "%d", Stats_getTeamSco(TEAMID_WEST, "deaths"));
	replace(html, len, "{team2_deaths}", buf);
	
	replace(html, len, "{nextmap}", GMapName); // <MapFactory> (OPT)
	replace(html, len, "{nextgamemode}", GMapAuthor);
	
	for(new id=1; id<=GMaxPlayers; id++)
	{
		if(!isConnected(id))
			continue;
		
		format(buf, 127, getTeam(id) == params[0] ? "Your team wins the event." : "Your team had a tie in the event.");
		replace(html, len, "{activechar_message}", buf);
		
		format(buf, 127, "%d", Stats_getPvP(id));
		replace(html, len, "{activechar_pvp}", buf);
		format(buf, 127, "%d", Stats_getPK(id));
		replace(html, len, "{activechar_pk}", buf);
		format(buf, 127, "%d", Stats_getDeaths(id));
		replace(html, len, "{activechar_deaths}", buf);
		format(buf, 127, "%d", Stats_getAvanpostDmg(id));
		replace(html, len, "{activechar_avandmg}", buf);
		format(buf, 127, "%d", Stats_getEarnedAdena(id));
		replace(html, len, "{activechar_adena}", buf);

		sendHtml(id, html, ""); // <HtmlMessage>
	}
}
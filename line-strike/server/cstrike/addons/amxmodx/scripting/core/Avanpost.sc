// --------------------------------------------------
// Game Mode: Outpost Defense
// --------------------------------------------------

#define AVANPOST_HEALTH		3000.0	//50000.0
new EastOutpost, WestOutpost;

new Float:AtkNotifierCooldown[2] = {0.0, ...}; // 0-red, 1-blue
#define COOLDOWN_ATK_NOTIFIER	30.0

//new Avanpost_ActionsCount = 0;

public Avanpost_run()
{
	Avanpost_appears(); // shake players
	Avanpost_prepare(); // setting positions
}

public Avanpost_appears()
{
	for(new id=1; id<=GMaxPlayers; id++)
		if(isConnected(id) && !isBot(id))
			Create_ScreenShake(id, 15000, 17500, 255<<14, true);
}

public Avanpost_prepare()
{
	Log(INFO, "Avanpost", "Preparing spawns for map: %s", GMapName);

	if(mapIs(MAP_CIRCLE_R))
	{
		Avanpost_spawn(Float:{2.736252, -3439.625488, -27.96875}, TEAMID_WEST);
		Guard_posAround(Float:{2.736252, -3439.625488, -27.96875}, 160, 6, TEAMID_WEST);
		Avanpost_spawn(Float:{-1.188248, 3473.097656, -27.968750}, TEAMID_EAST);
		Guard_posAround(Float:{-1.188248, 3473.097656, -27.968750}, 160, 6, TEAMID_EAST);
	}
	else if(mapIs(MAP_CRUMA_TOWER))
	{
		Avanpost_spawn(Float:{2875.559326, -2016.715698, -218.968750}, TEAMID_EAST);
		Guard_posAround(Float:{2875.559326, -2016.715698, -218.968750}, 170, 6, TEAMID_EAST); //L size=150
		Avanpost_spawn(Float:{2995.849853, 2971.060302, 292.031250}, TEAMID_WEST);
		Guard_posAround(Float:{2995.849853, 2971.060302, 292.031250}, 170, 6, TEAMID_WEST);
		Avanpost_rotate(WestOutpost, 90.0);
		// NPCs
		Npc_doSpawn(-1, NPCID_GATEKEEPER, Float:{2880.836914, -1413.461791, -219.968750}, Float:{-0.560302, -90.560302, 0.0}); // East
		Npc_doSpawn(-1, NPCID_GATEKEEPER, Float:{3449.959472, 2976.042968, 292.031250}, Float:{-0.296630, -179.143066, 0.0}); // West
	}
	/*else if(mapIs(MAP_CS_ASSAULT))
	{
		Avanpost_spawn(Float:{543.61, 2528.36, 68.03}, TEAMID_EAST);
		Guard_posAround(Float:{543.61, 2528.36, 68.03}, 110, 6, TEAMID_EAST); //L size=150
		Avanpost_spawn(Float:{-1638.6, -264.45, 36.03}, TEAMID_WEST);
		Guard_posAround(Float:{-1638.6, -264.45, 36.03}, 150, 6, TEAMID_WEST);
	}
	else if(mapIs(MAP_DE_DUST2_X))
	{
		Avanpost_spawn(Float:{-1266.661376, -1791.233154, 4.03125}, TEAMID_EAST);
		Guard_posAround(Float:{-1266.661376, -1791.233154, 4.03125}, 150, 6, TEAMID_EAST);
		Avanpost_spawn(Float:{1729.834594, 850.058044, -115.613868}, TEAMID_WEST);
		Guard_posAround(Float:{1729.834594, 850.058044, -115.613868}, 150, 6, TEAMID_WEST);
	}
	else if(mapIs(MAP_DE_DUST_AZTEC))
	{
		Avanpost_spawn(Float:{2735.0, 1526.0, -11.97}, TEAMID_EAST);
		Guard_posAround(Float:{2735.0, 1526.0, -11.97}, 150, 6, TEAMID_EAST);
		Avanpost_spawn(Float:{-2449.96, 1237.48, -75.97}, TEAMID_WEST);
		Guard_posAround(Float:{-2449.96, 1237.48, -75.97}, 150, 6, TEAMID_WEST);
	}
	else if(mapIs(MAP_DE_OFFICE_RATS))
	{
		Avanpost_spawn(Float:{-272.931732, -951.707092, -1049.968750}, TEAMID_EAST);
		Guard_posAround(Float:{-272.931732, -951.707092, -1049.968750}, 180, 6, TEAMID_EAST);
		Avanpost_spawn(Float:{-52.183353, 2221.807373, -1049.968750}, TEAMID_WEST);
		Guard_posAround(Float:{-52.183353, 2221.807373, -1049.968750}, 180, 6, TEAMID_WEST);
	}
	else if(mapIs(MAP_DE_QUETZALCOATL))
	{
		Avanpost_spawn(Float:{-1481.0, -1995.0, -462.0}, TEAMID_EAST);
		Guard_posAround(Float:{-1481.0, -1995.0, -462.0}, 150, 6, TEAMID_EAST);
		Avanpost_spawn(Float:{1281.0, 1380.0, -206.0}, TEAMID_WEST);
		Guard_posAround(Float:{1281.0, 1380.0, -206.0}, 150, 6, TEAMID_WEST);
	}
	else if(mapIs(MAP_DE_SAINTCAYEUX_RATS))
	{
		Avanpost_spawn(Float:{3400.837646, 1208.139038, -866.968750}, TEAMID_EAST);
		Guard_posAround(Float:{3400.837646, 1208.139038, -866.968750}, 150, 6, TEAMID_EAST);
		Avanpost_spawn(Float:{-2371.404296, -1387.935424, -866.968750}, TEAMID_WEST);
		Guard_posAround(Float:{-2371.404296, -1387.935424, -866.968750}, 150, 6, TEAMID_WEST);
	}
	else if(mapIs(MAP_DE_WESTWOOD))
	{
		Avanpost_spawn(Float:{235.0, 1662.0, -92.0}, TEAMID_EAST);
		Guard_posAround(Float:{235.0, 1662.0, -92.0}, 150, 6, TEAMID_EAST);
		Avanpost_spawn(Float:{2400.0, -172.0, -92.0}, TEAMID_WEST);
		Avanpost_rotate(WestOutpost, 90.0);
		Guard_posAround(Float:{2400.0, -172.0, -92.0}, 150, 6, TEAMID_WEST);
	}
	else if(mapIs(MAP_GG_ANUBIS))
	{
		Avanpost_spawn(Float:{259.197845, 1585.599121, 36.031250}, TEAMID_EAST);
		Guard_posAround(Float:{259.197845, 1585.599121, 36.031250}, 150, 6, TEAMID_EAST);
		Avanpost_spawn(Float:{253.650604, -1336.902343, 36.031250}, TEAMID_WEST);
		Guard_posAround(Float:{253.650604, -1336.902343, 36.031250}, 150, 6, TEAMID_WEST);
	}
	else if(mapIs(MAP_HANG_EM_HIGH))
	{
		Avanpost_spawn(Float:{-614.686523, 1467.767700, 300.031250}, TEAMID_EAST);
		Guard_posAround(Float:{-614.686523, 1467.767700, 300.031250}, 150, 6, TEAMID_EAST);
		Avanpost_spawn(Float:{1282.126953, -1491.948364, 60.031250}, TEAMID_WEST);
		Guard_posAround(Float:{1282.126953, -1491.948364, 60.031250}, 150, 6, TEAMID_WEST);
	}
	else if(mapIs(MAP_STARWARS))
	{
		Avanpost_spawn(Float:{-913.425048, 2303.042236, -283.968750}, TEAMID_EAST);
		Guard_posAround(Float:{-913.425048, 2303.042236, -283.968750}, 150, 6, TEAMID_EAST);
		Avanpost_spawn(Float:{65.486106, -1259.555786, -283.968750}, TEAMID_WEST);
		Guard_posAround(Float:{65.486106, -1259.555786, -283.968750}, 150, 6, TEAMID_WEST);
	}
	else if(mapIs(MAP_SUPERCRAZYCARS))
	{
		Avanpost_spawn(Float:{-236.004470, 2625.166015, -155.968750}, TEAMID_EAST);
		Guard_posAround(Float:{-236.004470, 2625.166015, -155.968750}, 150, 6, TEAMID_EAST);
		Avanpost_spawn(Float:{-270.188842, -2594.035888, -155.968750}, TEAMID_WEST);
		Guard_posAround(Float:{-270.188842, -2594.035888, -155.968750}, 150, 6, TEAMID_WEST);
	}*/
	else
		Log(ERROR, "Avanpost", "Map %s is not valid for game mode %s", GMapName, GameMode_getName());
}

/*
public Avanpost_colorEvent(ent)
{
	Avanpost_ActionsCount++;
	if(Avanpost_ActionsCount < 6){
		return;
	}else{
		Avanpost_ActionsCount = 0;
	}
	
	// Check hp and change rendering color
	new Float:hp; pev(ent, pev_health, hp);
	
	Log(DEBUG, "Avanpost", "takeDamage(): HP percent: %.1f", hp / AVANPOST_HEALTH);

	if(hp <= 10 * AVANPOST_HEALTH / 100)
	{ // HP <= 10%
		fm_set_rendering(ent, kRenderFxGlowShell, 25,5,5, kRenderNormal, 1);
	}
	else if(hp > (10 * AVANPOST_HEALTH / 100) && hp <= (20 * AVANPOST_HEALTH / 100))
	{ // HP > 10% and <= 20%
		fm_set_rendering(ent, kRenderFxGlowShell, 45,5,5, kRenderNormal, 1);
	}
	else if(hp > (20 * AVANPOST_HEALTH / 100) && hp <= (30 * AVANPOST_HEALTH / 100))
	{ // HP > 20% and <= 30%
		//fm_set_rendering(ent, kRenderFxGlowShell, 249,49,52, kRenderNormal, 1);
		fm_set_rendering(ent, kRenderFxGlowShell, 65,25,5, kRenderNormal, 1);
	}
	else if(hp > (30 * AVANPOST_HEALTH / 100) && hp <= (40 * AVANPOST_HEALTH / 100))
	{ // HP > 30% and <= 40%
		//fm_set_rendering(ent, kRenderFxGlowShell, 252,176,30, kRenderNormal, 1);
		fm_set_rendering(ent, kRenderFxGlowShell, 180,100,5, kRenderNormal, 1);
	}
	else if(hp > (40 * AVANPOST_HEALTH / 100) && hp <= (50 * AVANPOST_HEALTH / 100))
	{ // HP > 40% and <= 50%
		//fm_set_rendering(ent, kRenderFxGlowShell, 255,225,29, kRenderNormal, 1);
		fm_set_rendering(ent, kRenderFxGlowShell, 180,140,5, kRenderNormal, 1);
	}
	else if(hp > (50 * AVANPOST_HEALTH / 100) && hp <= (60 * AVANPOST_HEALTH / 100))
	{ // HP > 50% and <= 60%
		//fm_set_rendering(ent, kRenderFxGlowShell, 253,247,29, kRenderNormal, 1);
		fm_set_rendering(ent, kRenderFxGlowShell, 120,100,5, kRenderNormal, 1);
	}
	else if(hp > (60 * AVANPOST_HEALTH / 100) && hp <= (70 * AVANPOST_HEALTH / 100))
	{ // HP > 60% and <= 70%
		//fm_set_rendering(ent, kRenderFxGlowShell, 217,255,14, kRenderNormal, 1);
		fm_set_rendering(ent, kRenderFxGlowShell, 100,100,5, kRenderNormal, 1);
	}
	else if(hp > (70 * AVANPOST_HEALTH / 100) && hp <= (80 * AVANPOST_HEALTH / 100))
	{ // HP > 70% and <= 80%
		//fm_set_rendering(ent, kRenderFxGlowShell, 217,255,14, kRenderNormal, 1);
		fm_set_rendering(ent, kRenderFxGlowShell, 80,80,5, kRenderNormal, 1);
	}
	else if(hp > (80 * AVANPOST_HEALTH / 100) && hp <= (90 * AVANPOST_HEALTH / 100))
	{ // HP > 80% and <= 90%
		//fm_set_rendering(ent, kRenderFxGlowShell, 179,247,48, kRenderNormal, 1);
		fm_set_rendering(ent, kRenderFxGlowShell, 40,80,5, kRenderNormal, 1);
	}
	else if(hp > (90 * AVANPOST_HEALTH / 100) && hp <= (100 * AVANPOST_HEALTH / 100))
	{ // HP > 90% and <= 100%
		//fm_set_rendering(ent, kRenderFxGlowShell, 61,205,9, kRenderNormal, 1);
		fm_set_rendering(ent, kRenderFxGlowShell, 5,80,5, kRenderNormal, 1);
	}
}
*/

new bool:AV_InSmoke = false;

public Avanpost_takeDamage(ent, attacker, team)
{
	switch(team)
	{
		case TEAMID_EAST: 
		{
			if(get_gametime() - AtkNotifierCooldown[0] < COOLDOWN_ATK_NOTIFIER){
				// nothing
			}else{
				AtkNotifierCooldown[0] = get_gametime();
				for(new id=1; id<=GMaxPlayers; id++){
					if(!isBot(id) && getTeam(id) == 1){ // T
						Util_colorPrint(id, "/ctrEast outpost under attack!"); /* @IO : Fellemere Lake (Dark) - аванпост атакуется! */
						sendMessage(id, print_console, "East outpost under attack!");
					}
				}
			}
		}
		case TEAMID_WEST: 
		{
			if(get_gametime() - AtkNotifierCooldown[1] < COOLDOWN_ATK_NOTIFIER){
				// nothing
			}else{
				AtkNotifierCooldown[1] = get_gametime();
				for(new id=1; id<=GMaxPlayers; id++){
					if(!isBot(id) && getTeam(id) == 2){ // CT
						Util_colorPrint(id, "/ctrWest outpost under attack!"); /* @IO : Fellemere Lake (Dark) - аванпост атакуется! */
						sendMessage(id, print_console, "West outpost under attack!");
					}
				}
			}
		}
	}

	new Float:hp;
	pev(ent, pev_health, hp);
	Log(DEBUG, "Avanpost", "takeDamage(%d, %s, %d) HP=%.1f", ent, getName(attacker), team, hp);

	// test
	if(hp <= AVANPOST_HEALTH / 5.0 && !AV_InSmoke){
		AV_InSmoke = true;
		new Float:origin[3];
		getEntOrigin(ent, origin);
		origin[2] += 160.0;
		Create_TE_SMOKE(origin, SPR_STUNSHOT_HIT, 50, 12);
	}

//	Avanpost_colorEvent(ent);
}

public Avanpost_heal(ent)
{
	new Float:hp; pev(ent, pev_health, hp);
	Log(DEBUG, "Avanpost", "Avanpost_heal(): OutpostId=%d, HP=%.1f", ent, hp);
	
	if(hp < AVANPOST_HEALTH){
		set_pev(ent, pev_health, hp + 1.0);
		Log(DEBUG, "Avanpost", "Avanpost_heal(): (%.1f < %.1f) ==> hp + 1.0", hp, AVANPOST_HEALTH);
	}

//	Avanpost_colorEvent(ent);
}

public Avanpost_destroyed(outpost, killer) // killer unused ? MAYBE for topkiller
{
	if(outpost == EastOutpost){
		Log(DEBUG, "Avanpost", "destroyed(): %s", TNAME_EAST);
		GameMode_end(TEAMID_WEST, killer);
	}
	else if(outpost == WestOutpost){
		Log(DEBUG, "Avanpost", "destroyed(): %s", TNAME_WEST);
		GameMode_end(TEAMID_EAST, killer);
	}

	sendMessage(0, print_chat, "%s has destroyed a tower.", getName(killer));

	set_pev(outpost, pev_solid, SOLID_NOT);
	set_pev(outpost, pev_takedamage, 0.0);

	emit_sound(outpost, CHAN_VOICE, S_CRITICAL_HIT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	fm_set_rendering(outpost); //, kRenderFxGlowShell, 100, 0, 0, kRenderGlow, 80);

	new Float:forigin[3], origin[3];
	getEntOrigin(outpost, forigin);
	FVecIVec(Float:forigin, origin);

	new Float:woffset[3];
	woffset = forigin;
	woffset[2] += 50.0;	// test
	// Explosion
	Create_TE_EXPLOSION(woffset, SPR_ZEROGXPLODE, 50, 15, TE_EXPLFLAG_NOSOUND);
	Create_TE_WORLDDECAL(origin, 47); // SCORCH
	
	// Circle beams
	new Float:axis[3];
	axis = forigin;
	axis[2] += 400.0
	Create_TE_BEAMCYLINDER(forigin, axis, SPR_LIHIKIN_GLOW, 0, 0, 4, 60, 0, 255,255,255,200, 0);
	axis[2] -= 100.0;	// +300.0
	Create_TE_BEAMCYLINDER(forigin, axis, SPR_LIHIKIN_GLOW, 0, 0, 4, 60, 0, 255,255,255,200, 0);
	axis[2] -= 100.0;	// +200.0
	Create_TE_BEAMCYLINDER(forigin, axis, SPR_LIHIKIN_GLOW, 0, 0, 4, 60, 0, 255,255,255,200, 0);	

	// Burning wood gibs
	Create_TE_EXPLODEMODEL(origin, randInt(350,450)/*veloc*/, M_WOODGIBS, randInt(100,200)/*count*/, 200/*life*/);

	// Wood gibs break
	origin[2] += 90; //96
	Create_TE_BREAKMODEL(origin, randInt(25,100), randInt(25,100), randInt(25,100), randInt(-50,150), randInt(-50,150), randInt(-50,150), randVal(100), M_WOODGIBS, randInt(50,150), randInt(50,300), 0x08);	
	origin[2] -= 20;
	Create_TE_BREAKMODEL(origin, randInt(80,100), randInt(80,100), randInt(80,100), randInt(10,50), randInt(10,50), randInt(10,50), randVal(400), M_WOODGIBS, randInt(10,40), randInt(50,300), 0x08);	
	//               (pOrigin[3], sizex,          sizey,           sizez,           velx,           vely,           velz,           velrnd,       spr,        count,          life,            flags)

	set_task(5.0, "Avanpost_remove", outpost+TASK_AVANPOST_DESTROYED);
}

public Avanpost_remove(outpost)
{
	outpost -= TASK_AVANPOST_DESTROYED;
	if(outpost == EastOutpost && pev_valid(EastOutpost)){
		entityDispose(EastOutpost);
	}
	else if(outpost == WestOutpost && pev_valid(WestOutpost)){
		entityDispose(WestOutpost);
	}
}

public Avanpost_setNameSprite(Float:origin[3], team)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		setEntClass(ent, "Spr@NpcName");

		if(team == TEAMID_EAST)
			setEntModel(ent, SPR_NPCNAME_EAST);
		else if(team == TEAMID_WEST)
			setEntModel(ent, SPR_NPCNAME_WEST);

		origin[2] += 280.0; //260.0
		setEntOrigin(ent, origin);
		setEntSize(ent, Float:{0.0,0.0,0.0}, Float:{0.0,0.0,0.0});

		set_pev(ent, pev_solid, SOLID_NOT);
		set_pev(ent, pev_movetype, MOVETYPE_NONE);
		set_pev(ent, pev_scale, 0.8); //0.5
		set_pev(ent, pev_rendermode, 5); // 5 - Additive
		set_pev(ent, pev_renderamt, 255.0);
		set_pev(ent, pev_renderfx, 14);
		set_pev(ent, pev_frame, 0.0);
	}
	else
		Log(ERROR, "Avanpost", "setNameSprite(): Can't create outpost name ent for team=%d", team);
}

public Avanpost_spawn(Float:origin[3], team)
{
	switch(team)
	{
		case TEAMID_EAST: // T
		{
			EastOutpost = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
			if(EastOutpost)
			{
				Log(DEBUG, "Avanpost", "Spawning %s Outpost (ID=%d)", TNAME_EAST, EastOutpost);

				setEntClass(EastOutpost, "L2Avanpost@Red");
				setEntModel(EastOutpost, M_OUTPOST);
				setEntSkin(EastOutpost, 0); //1
				setEntSize(EastOutpost, Float:{-64.0, -32.0, 0.0}, Float:{64.0, 32.0, 234.0}); //224.0
				setEntOrigin(EastOutpost, origin);

				set_pev(EastOutpost, pev_solid, SOLID_BBOX);
				set_pev(EastOutpost, pev_movetype, MOVETYPE_NONE);
				set_pev(EastOutpost, pev_health, AVANPOST_HEALTH);
				set_pev(EastOutpost, pev_takedamage, DAMAGE_YES);
				set_pev(EastOutpost, pev_owner, NPC_OWNER_EAST);
				set_pev(EastOutpost, pev_nextthink, 1.0);
				set_pev(EastOutpost, pev_sequence, 0);
				set_pev(EastOutpost, pev_gaitsequence, 0);
				set_pev(EastOutpost, pev_framerate, 1.0);
				
				set_pev(EastOutpost, pev_gamestate, 1);
				//fm_set_kvd(EastOutpost, "material", "1");
				
				fm_drop_to_floor(EastOutpost);
				fm_set_rendering(EastOutpost, kRenderFxGlowShell, 25,5,5, kRenderNormal, 1);
				
				Avanpost_setNameSprite(origin, 1);
			}
			else
				Log(FATAL, "Avanpost", "spawn(): Can't create outpost ent for team=%d", team);
		}
		case TEAMID_WEST: // CT
		{
			WestOutpost = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
			if(WestOutpost)
			{
				Log(DEBUG, "Avanpost", "Spawning %s Outpost (ID=%d)", TNAME_WEST, WestOutpost);
				
				setEntClass(WestOutpost, "L2Avanpost@Blu");
				setEntModel(WestOutpost, M_OUTPOST);
				setEntSkin(WestOutpost, 1); //2
				setEntSize(WestOutpost, Float:{-64.0, -32.0, 0.0}, Float:{64.0, 32.0, 234.0});
				setEntOrigin(WestOutpost, origin);

				set_pev(WestOutpost, pev_solid, SOLID_BBOX);
				set_pev(WestOutpost, pev_movetype, MOVETYPE_NONE);
				set_pev(WestOutpost, pev_health, AVANPOST_HEALTH);
				set_pev(WestOutpost, pev_takedamage, DAMAGE_YES);
				set_pev(WestOutpost, pev_owner, NPC_OWNER_WEST);
				set_pev(WestOutpost, pev_nextthink, 1.0);
				set_pev(WestOutpost, pev_sequence, 0);
				set_pev(WestOutpost, pev_gaitsequence, 0);
				set_pev(WestOutpost, pev_framerate, 1.0);
				
				set_pev(WestOutpost, pev_gamestate, 1);
				//fm_set_kvd(WestOutpost, "material", "1");

				fm_drop_to_floor(WestOutpost);
				fm_set_rendering(WestOutpost, kRenderFxGlowShell, 5,5,25, kRenderNormal, 1);
				
				Avanpost_setNameSprite(origin, 2);
			}
			else
				Log(FATAL, "Avanpost", "spawn(): Can't create outpost ent for team=%d", team);
		}
	}
}

public Avanpost_rotate(ent, Float:angle)
{
	new Float:angles[3];
	pev(ent, pev_angles, angles);
	angles[1] += angle;
	set_pev(ent, pev_angles, angles);
	engfunc(EngFunc_SetSize, ent, Float:{-32.0,-64.0,0.0}, Float:{32.0,64.0,234.0}); // CONST 90* degrees
}

public Avanpost_endHtml(params[2] /*, taskid*/) // params[0] = winning_team; params[1] = last_attacker;
{
	new html[MAX_HTML_CONTENT_LENGTH];
	new len = MAX_HTML_CONTENT_LENGTH-1;
	
	if(!getHtm(HTML_OD_END, html))
		return;

	new buf[128], player; // format

	replace(html, len, "{loser_color}", params[0] == 2 ? COLOR_EAST : COLOR_WEST);
	
	replace(html, len, "{last_attacker_color}", getTeam(params[1]) == 1 ? COLOR_EAST : COLOR_WEST);
	replace(html, len, "{last_attacker}", getName(params[1]));
	
	replace(html, len, "{winner_color}", params[0] == 1 ? COLOR_EAST : COLOR_WEST);
	
	replace(html, len, "{destroyed_outpost}", params[0] == 2 ? TNAME_EAST : TNAME_WEST);
	replace(html, len, "{winner_faction}", params[0] == 1 ? TNAME_EAST : TNAME_WEST);

	player = Stats_getTopStat("killsid");
	replace(html, len, "{top_kills_teamcolor}", getTeam(player) == 1 ? COLOR_EAST : COLOR_WEST);
	replace(html, len, "{top_kills_player}", getName(player));
	format(buf, 127, "%d", Stats_getTopStat("killsnum"));
	replace(html, len, "{top_kills_amount}", buf);

	player = Stats_getTopStat("deathsid");
	replace(html, len, "{top_deaths_teamcolor}", getTeam(player) == 2 ? COLOR_WEST : COLOR_EAST);
	replace(html, len, "{top_deaths_player}", getName(player));
	format(buf, 127, "%d", Stats_getTopStat("deathsnum"));
	replace(html, len, "{top_deaths_amount}", buf);

	replace(html, len, "{team1_name}", TNAME_EAST);
	replace(html, len, "{team2_name}", TNAME_WEST);
	replace(html, len, "{color_team1}", COLOR_EAST);
	replace(html, len, "{color_team2}", COLOR_WEST);

	format(buf, 127, "%d", Stats_getTeamSco(TEAMID_EAST, "kills"));
	replace(html, len, "{team1_kills}", buf);
	format(buf, 127, "%d", Stats_getTeamSco(TEAMID_WEST, "kills"));
	replace(html, len, "{team2_kills}", buf);
	format(buf, 127, "%d", Stats_getTeamSco(TEAMID_EAST, "deaths"));
	replace(html, len, "{team1_deaths}", buf);
	format(buf, 127, "%d", Stats_getTeamSco(TEAMID_WEST, "deaths"));
	replace(html, len, "{team2_deaths}", buf);
	
	replace(html, len, "{nextmap}", GMapName); // <MapFactory> (OPT)
	replace(html, len, "{nextgamemode}", GMapAuthor);
	
	for(new id=1; id<=GMaxPlayers; id++)
	{
		if(!isConnected(id))
			continue;
		
		format(buf, 127, "%d", Stats_getPvP(id));
		replace(html, len, "{activechar_pvp}", buf);
		format(buf, 127, "%d", Stats_getPK(id));
		replace(html, len, "{activechar_pk}", buf);
		format(buf, 127, "%d", Stats_getDeaths(id));
		replace(html, len, "{activechar_deaths}", buf);
		format(buf, 127, "%d", Stats_getAvanpostDmg(id));
		replace(html, len, "{activechar_avandmg}", buf);
		format(buf, 127, "%d", Stats_getEarnedAdena(id));
		replace(html, len, "{activechar_adena}", buf);

		sendHtml(id, html, ""); // <HtmlMessage>
	}
}
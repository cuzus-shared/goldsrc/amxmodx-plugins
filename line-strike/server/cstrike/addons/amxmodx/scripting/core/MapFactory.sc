// --------------------------------------------------
// Map Factory 
// --------------------------------------------------

public mapIs(map[])
{
	return equali(GMapName, map) ? true : false;
}

new MAPFACTORY_OUTPOST_DEF[][] = {
	MAP_CRUMA_TOWER
};

new MAPFACTORY_LAST_HERO[][] = {
	MAP_BAIUMS_LAIR,
	MAP_DE_GLUDIN
};

new MAPFACTORY_TVT[][] = {
	MAP_GIRAN_ARENA
};

public bool:MapFactory_isOutpostDefense()
{
	for(new i=0; i <= sizeof MAPFACTORY_OUTPOST_DEF - 1; i++)
		if(equali(GMapName, MAPFACTORY_OUTPOST_DEF[i]))
			return true;
	return false;
}
public bool:MapFactory_isLastHero()
{
	for(new i=0; i <= sizeof MAPFACTORY_LAST_HERO - 1; i++)
		if(equali(GMapName, MAPFACTORY_LAST_HERO[i]))
			return true;
	return false;
}
public bool:MapFactory_isTeamVsTeam()
{
	for(new i=0; i <= sizeof MAPFACTORY_TVT - 1; i++)
		if(equali(GMapName, MAPFACTORY_TVT[i]))
			return true;
	return false;
}

new NEXT_MAP[MAX_MAPNAME_LENGTH] = "null";
new NEXT_GAME_MODE = 0;

public assumeNextMap()
{
	NEXT_GAME_MODE = randNum(1,4);
	// game mode IDs (can't use defined consts from GameMode 'cause MapFactory include locaded before GameMode)
	switch(NEXT_GAME_MODE){
		case 1: // Outpost Defense
			formatex(NEXT_MAP, MAX_MAPNAME_LENGTH-1, "%s", MAPFACTORY_OUTPOST_DEF[randVal(charsmax(MAPFACTORY_OUTPOST_DEF))]);
		//case 2: // Castle Siege																					
		case 2, 3: // Last Hero
			formatex(NEXT_MAP, MAX_MAPNAME_LENGTH-1, "%s", MAPFACTORY_LAST_HERO[randVal(charsmax(MAPFACTORY_LAST_HERO))]);		
		case 4: // Team vs Team
			formatex(NEXT_MAP, MAX_MAPNAME_LENGTH-1, "%s", MAPFACTORY_TVT[randVal(charsmax(MAPFACTORY_TVT))]);
	}
}

public MapFactory_selectNext()
{
	new SQL_ErrorCode;
	new SQL_ErrorString[512];
	new Handle:SqlConnection = SQL_Connect(SQL_Tuple, SQL_ErrorCode, SQL_ErrorString, 511);
	if(SqlConnection == Empty_Handle){
		Log(FATAL, "Database", "MySQL connection failed!");
		pluginFail(SQL_ErrorString, 0);
	}

	new Handle:SelectFromMaps = SQL_PrepareQuery(SqlConnection, "SELECT map_name FROM `maps`; ");
	if(!SQL_Execute(SelectFromMaps)){
		SQL_QueryError(SelectFromMaps, SQL_ErrorString, 511);
		pluginFail(SQL_ErrorString, 0);
	}

	new i = 0;
	static LASTMAP[5][65];

	if(SQL_NumResults(SelectFromMaps) > 0)
	{
		while(SQL_MoreResults(SelectFromMaps) /*&& i <= 4*/ )
		{
			SQL_ReadResult(SelectFromMaps, 0, LASTMAP[i], 64);
			Log(DEBUG, "MapFactory", "while(SQL_MoreResults() : LASTMAP[%d]=%s", i, LASTMAP[i]);
			i++;

			Log(DEBUG, "MapFactory", "INDEX=%d", i);

			if(i < 5){
				Log(DEBUG, "MapFactory", "SQL_NextRow()");
				SQL_NextRow(SelectFromMaps);
			}
			else{
				Log(DEBUG, "MapFactory", "while() ... break");
				break;
			}
		}
	}

	SQL_FreeHandle(SelectFromMaps);

	new map[MAX_MAPNAME_LENGTH];
	get_mapname(map, MAX_MAPNAME_LENGTH-1);
	
	assumeNextMap();
	
	while(equali(LASTMAP[0], NEXT_MAP) 
	|| equali(LASTMAP[1], NEXT_MAP)
	|| equali(LASTMAP[2], NEXT_MAP)
	|| equali(LASTMAP[3], NEXT_MAP)
	|| equali(LASTMAP[4], NEXT_MAP)
	|| equali(map, NEXT_MAP))
	{
		assumeNextMap();
		Log(DEBUG, "MapFactory", "while(lastmap[i]/map, nextmap) ... assumed: %s", NEXT_MAP);
	}
	
	// save current map as last played (id=1) and push out map with id=5
	static query[256];
	static len; len = 0;
	len += formatex(query[len], charsmax(query)-len, "DELETE FROM `maps` WHERE id=5; ");
	len += formatex(query[len], charsmax(query)-len, "UPDATE `maps` SET id=5 WHERE id=4; ");
	len += formatex(query[len], charsmax(query)-len, "UPDATE `maps` SET id=4 WHERE id=3; ");
	len += formatex(query[len], charsmax(query)-len, "UPDATE `maps` SET id=3 WHERE id=2; ");
	len += formatex(query[len], charsmax(query)-len, "UPDATE `maps` SET id=2 WHERE id=1; ");
	len += formatex(query[len], charsmax(query)-len, "INSERT INTO `maps` VALUES ('1','%s'); ", map);

	new Handle:PushMaps = SQL_PrepareQuery(SqlConnection, query);
	if(!SQL_Execute(PushMaps)){
		SQL_QueryError(PushMaps, SQL_ErrorString, 511);
		pluginFail(SQL_ErrorString, 0);
	}
	SQL_FreeHandle(PushMaps);
	
	// close connection
	SQL_FreeHandle(SqlConnection);
	
	// announce
	new tmp[128];
	formatex(tmp, 127, "%s", NEXT_MAP);
	replace(tmp, 127, "_arc", "");
	
	// next game mode info
	new tmp2[32];
	switch(NEXT_GAME_MODE){
		case 1: tmp2 = STRING_OUTPOST_DEFENSE;
		case 2: tmp2 = STRING_CASTLE_SIEGE;
		case 3: tmp2 = STRING_LAST_HERO;
		case 4: tmp2 = STRING_TEAM_VS_TEAM;
	}

//	Util_colorPrint(0, "/ctrNext map will be %s (%s)", tmp, tmp2);
//	client_print(0, print_console, "Next map will be %s (%s)", tmp, tmp2);
	
	// (OPT) write next map, next game mode to global vars for show at html
	format(GMapName, MAX_MAPNAME_LENGTH-1, "%s", tmp);
	format(GMapAuthor, MAX_MAPNAME_LENGTH-1, "%s", tmp2);
	Log(INFO, "MapFactory", "Next map will be %s (%s)", GMapName, GMapAuthor);

	// countdown
	new params[1];
	params[0] = 31; // seconds
	set_task(1.0, "startCountdown", TASK_GAME_MODE_NEXTMAP_COUNTDN, params, 1);
}

public startCountdown(params[1], taskid)
{
	params[0]--;
	
	if(params[0] == 120){
		Util_colorPrint(0, "/ctrNext map in 2 minutes ...");
		client_print(0, print_console, "Next map in 2 minutes ...");
	}
	if(params[0] == 60){
		Util_colorPrint(0, "/ctrNext map in 1 minute ...");
		client_print(0, print_console, "Next map in 1 minute ...");
	}
	if(params[0] <= 30 /*&& params[0] > 10*/){
		//Util_colorPrint(0, "/ctrNext map in %d second(s) ...", params[0]);
		client_print(0, print_center, "Next map in %d second(s) ...", params[0]);
		client_print(0, print_console, "Next map in %d second(s) ...", params[0]);
	}
	/*if(params[0] <= 10){ //<=10
		Util_colorPrint(0, "/ctrNext map in %d second(s) ...", params[0]);
		client_print(0, print_console, "Next map in %d second(s) ...", params[0]);
	}*/
	if(params[0] == 0)
	{
		remove_task(taskid);
		params[0] = 180; // reset to default val
		server_cmd("changelevel %s", NEXT_MAP);
	}
	else{
		set_task(1.0, "startCountdown", taskid, params, 1);
	}
}

public MapFactory_setAuthor()
{
	new map[MAX_MAPNAME_LENGTH];
	new len = MAX_MAPNAME_LENGTH-1;
	format(map, len, "%s", GMapName);

//	replace(map, len, "_arc", ""); // cut _arc at end of map name
//	replace(map, len, "_ls", "");

	format(GMapAuthor, len, "-");
}
// --------------------------------------------------
// Config
// --------------------------------------------------

enum _:CONFIG_VALUES {
// Logger
	bool:LOGGER_ENABLED,
	bool:LOGGER_DEBUG,
	bool:LOGGER_SHOW_TIME,
	bool:LOGGER_SHOW_LEVEL,
	bool:LOGGER_SHOW_CLASS,
// General
	Float:TO_VILLAGE_TIME,
	bool:REMOVE_EXP_ON_DEATH,
	RATES_MULTIPLIER,
	CRITICAL_CHANCE_MELEE,
	CRITICAL_CHANCE_ARROW,
	CRITICAL_CHANCE_MAGIC,
	bool:SAME_IP_KILLS_ENABLED,
	POTION_HP_ADD_MIN,
	POTION_HP_ADD_MAX,
// Login
	bool:ENABLE_JOIN_CAM,
	bool:LOGIN_GEO_CHECK,
	GLOBAL_WARMUP_TIME,
	MIN_PLAYERS_TO_START,
// Podbot
	bool:BOTS_ENABLED,
	MAX_BOTS_AMOUT,
	bool:BOT_UNJOINS_ENABLED,
	MAX_BOT_UNJOIN_TASKS,
	Float:BOTS_TIME_TO_JOIN_MIN,
	Float:BOTS_TIME_TO_JOIN_MAX
};
new Config[CONFIG_VALUES];

// TODO: config read from files
public Config_load()
{
// Logger
	Config[LOGGER_ENABLED] = true;
	Config[LOGGER_DEBUG] = true;			// If disabled - debug-level messages will not shown
	Config[LOGGER_SHOW_TIME] = true;		// If enabled - server time will be shown
	Config[LOGGER_SHOW_LEVEL] = true;		// If enabled - debug level will be shown
	Config[LOGGER_SHOW_CLASS] = true;		// If enabled - the *sc module name will be shown
// General
	Config[TO_VILLAGE_TIME]	= _:1.7;
	Config[REMOVE_EXP_ON_DEATH] = false;
	Config[RATES_MULTIPLIER] = 0;
	Config[CRITICAL_CHANCE_MELEE] = 11;
	Config[CRITICAL_CHANCE_ARROW] = 22;
	Config[CRITICAL_CHANCE_MAGIC] = 5;
	Config[SAME_IP_KILLS_ENABLED] = false;
	Config[POTION_HP_ADD_MIN] = 5;
	Config[POTION_HP_ADD_MAX] = 30;
// Login
	Config[ENABLE_JOIN_CAM] = true;
	Config[LOGIN_GEO_CHECK] = false;
	Config[GLOBAL_WARMUP_TIME] = 10;
	Config[MIN_PLAYERS_TO_START] = 1;
// Podbot
	Config[BOTS_ENABLED] = true; // for joins normal work need 60 sec warmup
	Config[MAX_BOTS_AMOUT] = 20;
	Config[BOT_UNJOINS_ENABLED] = false;
	Config[MAX_BOT_UNJOIN_TASKS] = 12;
	Config[BOTS_TIME_TO_JOIN_MIN] = _:5.0;
	Config[BOTS_TIME_TO_JOIN_MAX] = _:60.0; //180.0
}

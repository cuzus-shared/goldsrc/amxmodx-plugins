// ----------------------------------------------------------------------
// Test Suite
// ----------------------------------------------------------------------

public test__enchant_me(id)
{
	removeWeaponEnchant(id);
	
	if(PlayerWeaponEnchant[id][getClass(id)] <= 25)
		PlayerWeaponEnchant[id][getClass(id)] += 1;
	else
		PlayerWeaponEnchant[id][getClass(id)] = 0;
	
	setWeaponEnchant(id);
	
	client_print(id, print_chat, "+%d", PlayerWeaponEnchant[id][getClass(id)]);
}

public test__streaksp(id)
{
	new Float:origin[3], Float:vect[3];
	entity_get_vector(id, EV_VEC_origin, origin);
	entity_get_vector(id, EV_VEC_velocity, vect);

	// GOOD SETS:
	// color=8, count=11, speed=226, randvel=162

	new count = randInt(2,50);//randInt(1,150);
	new speed = randInt(0,255);
	new randveloc = randInt(0,255);

	//new color = randInt(0,255);
	new colors[] = {59, 26, 214, 14, 6, 101, 77, 111, 206, 235, 115, 223, 202, 149, 201, 150, 247, 140};
	new color = colors[randVal(charsmax(colors))];

	Create_TE_STREAK_SPLASH(origin, vect /*направление*/, color, count, speed, randveloc);
	
	client_print(id, print_chat, "color=%d | count=%d | speed=%d | rand_veloc=%d",color,count,speed,randveloc);
}

/*
// VERY LONG TE_BEAMDISK (FULLMAP)

	// Effect
	new Float:origin[3], Float:axis[3];
	pev(id, pev_origin, origin);
	origin[2] -= 35.0;
	axis = origin;
	axis[2] += 70.0; //120.0
	Create_TE_BEAMDISK(origin, axis, SPR_L2HIT, 0, 0, 10, 32, random_num(0,1), 149,102,47,200, 10);
	//Create_TE_BEAMDISK(Float:origin[3], Float:axis[3], sprite, frame, framerate, life, width, noise, r,g,b,a, speed)
*/

/**
 * 3D Skyboxes Test
new ent3d;
public cmd__test10(id)
{
	if(pev_valid(ent3d))
		engfunc(EngFunc_RemoveEntity, ent3d);
	
	ent3d = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	
	if(ent3d)
	{
		set_pev(ent3d, pev_classname, "sky3d");
		new r = random_num(1,4);
		switch(r)
		{
			//case 1: engfunc(EngFunc_SetModel, ent3d, sky3d);
			//case 2: engfunc(EngFunc_SetModel, ent3d, msky);
			case 1: engfunc(EngFunc_SetModel, ent3d, mantsky);
			//case 4: engfunc(EngFunc_SetModel, ent3d, sk_y);
			//case 5: engfunc(EngFunc_SetModel, ent3d, skybox);
			case 2: engfunc(EngFunc_SetModel, ent3d, terw);
			case 3: engfunc(EngFunc_SetModel, ent3d, ferr);
		}

		client_print(id, print_chat, "skyID: %d", r);

		engfunc(EngFunc_SetSize, ent3d, Float:{0.0,0.0,0.0}, Float:{0.0,0.0,0.0});
		new Float:origin[3];
		pev(id, pev_origin, origin);
		origin[0] += random_float(0.0,300.0);
		origin[1] += random_float(0.0,300.0);
		origin[2] += random_float(150.0,250.0);
		engfunc(EngFunc_SetOrigin, ent3d, origin);
		set_pev(ent3d, pev_solid, SOLID_NOT);
		set_pev(ent3d, pev_movetype, MOVETYPE_NONE);
	//	set_pev(ent3d, pev_aiment3d, id);
	//	set_pev(ent3d, pev_rendermode, 3); // 3 - Glow
	//	set_pev(ent3d, pev_renderamt, 200.0);
	//	set_pev(ent3d, pev_renderfx, 0); // 0 - Normal
		set_pev(ent3d, pev_scale, 1.0);
		set_pev(ent3d, pev_frame, 1);
		set_pev(ent3d, pev_framerate, 1.0);
		set_pev(ent3d, pev_sequence, 0);
		set_pev(ent3d, pev_gaitsequence, 0);
		set_pev(ent3d, pev_nextthink, get_gametime() + 0.1);		
	}
}*/

/**
 * BF-like flares test 
 *
public cmd__test5(id)
{
	new sun = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	set_pev(sun, pev_classname, "awsun");
	
	new r = random_num(1,15);
	client_print(id, print_chat, "sprID: %d", r);
	switch(r)
	{
		case 1: engfunc(EngFunc_SetModel, sun, SPR_LIGHT);
		//case 2: engfunc(EngFunc_SetModel, sun, SPR_FLAREBF); // disabled while taken by deathspike
		//case 3: engfunc(EngFunc_SetModel, sun, spr_jmflat);
		//case 4: engfunc(EngFunc_SetModel, sun, one);
		//case 5: engfunc(EngFunc_SetModel, sun, blackhole);
		case 6: engfunc(EngFunc_SetModel, sun, reac);
		//case 7: engfunc(EngFunc_SetModel, sun, sunfl);
		//case 8: engfunc(EngFunc_SetModel, sun, sunfl2);
		//case 9: engfunc(EngFunc_SetModel, sun, mediamilitia33_256);
		//case 10: engfunc(EngFunc_SetModel, sun, mediamilitia43_128);
		//case 11: engfunc(EngFunc_SetModel, sun, mediamilitia43_256);
		//case 12: engfunc(EngFunc_SetModel, sun, mediamilitia46_256);
		case 13: engfunc(EngFunc_SetModel, sun, mediamilitia48_256);
		case 14: engfunc(EngFunc_SetModel, sun, vortexflare128);
		//case 15: engfunc(EngFunc_SetModel, sun, vortexflare256);
	}

	engfunc(EngFunc_SetSize, sun, Float:{0.0,0.0,0.0}, Float:{0.0,0.0,0.0});
	new Float:origin[3];
	pev(id, pev_origin, origin);
	origin[0] += random_float(0.0,300.0);
	origin[1] += random_float(0.0,300.0);
	origin[2] += random_float(150.0,250.0);
	engfunc(EngFunc_SetOrigin, sun, origin);
	set_pev(sun, pev_solid, SOLID_NOT);
	set_pev(sun, pev_movetype, MOVETYPE_NONE);
//	set_pev(sun, pev_aiment, id);
	set_pev(sun, pev_rendermode, 3); // 3 - Glow
	set_pev(sun, pev_renderamt, 200.0);
	set_pev(sun, pev_renderfx, 0); // 0 - Normal
	set_pev(sun, pev_scale, random_float(5.0, 25.0));
}*/

// TODO: придумать как можно использовать dbit'ы, где мигающие иконки (? для дебаффов и тд)
public cmd__test3(id, bit)
{
	switch(bit)
	{
		case 1: { ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<0)); }
		case 2: { ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<1)); }
		case 3: {ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<2)); }
		case 4: {ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<3)); }		// (frezflame) 2 раза мигает hud иконка над HP, не зацикливает повторение
		case 5: {ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<4)); }		// так же (гасмаска)
		case 6: {ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<5)); }
		case 7: {ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<6)); }
		case 8: {ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<7)); }
		case 9: {ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<8)); }		// так же (цифра 0)
		case 10:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<9)); }
		case 11:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<10)); }
		case 12:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<11)); }
		case 13:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<12)); }
		case 14:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<13)); }
		case 15:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<14)); }	// так же (снежинка)
		case 16:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<15)); }	// BUG @ мигает 4 раза (снежинка) -- если использовать другой dbit она зависает и потом появляется со всеми и так пока не умрет чар
		case 17:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<16)); }
		case 18:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<17)); }
		case 19:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<18)); }
		case 20:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<19)); }
		case 21:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<20)); }	// так же (О2)
		case 22:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<21)); }	// так же (freezflame)
		case 23:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<22)); }	// так же (гасмаска)
		case 24:{ExecuteHam(Ham_TakeDamage, id, id, id, 1.0, (1<<23)); }
	}
	
	//new val = r-1;
	client_print(id, print_chat, "damagebit : %d", bit);
	
	/*
	// Instant damage values for use with gmsgDamage 3rd value write_long(BIT)
	#define DMG_GENERIC                     0           // Generic damage was done
	#define DMG_CRUSH                       (1<<0)      // Crushed by falling or moving object
	#define DMG_BULLET                      (1<<1)      // Shot
	#define DMG_SLASH                       (1<<2)      // Cut, clawed, stabbed
	#define DMG_BURN                        (1<<3)      // Heat burned
	#define DMG_FREEZE                      (1<<4)      // Frozen
	#define DMG_FALL                        (1<<5)      // Fell too far
	#define DMG_BLAST                       (1<<6)      // Explosive blast damage
	#define DMG_CLUB                        (1<<7)      // Crowbar, punch, headbutt
	#define DMG_SHOCK                       (1<<8)      // Electric shock
	#define DMG_SONIC                       (1<<9)      // Sound pulse shockwave
	#define DMG_ENERGYBEAM                  (1<<10)     // Laser or other high energy beam 
	#define DMG_NEVERGIB                    (1<<12)     // With this bit OR'd in, no damage type will be able to gib victims upon death
	#define DMG_ALWAYSGIB                   (1<<13)     // With this bit OR'd in, any damage type can be made to gib victims upon death.
	#define DMG_DROWN                       (1<<14)     // Drowning
	#define DMG_PARALYZE                    (1<<15)     // Slows affected creature down
	#define DMG_NERVEGAS                    (1<<16)     // Nerve toxins, very bad
	#define DMG_POISON                      (1<<17)     // Blood poisioning
	#define DMG_RADIATION                   (1<<18)     // Radiation exposure
	#define DMG_DROWNRECOVER                (1<<19)     // Drowning recovery
	#define DMG_ACID                        (1<<20)     // Toxic chemicals or acid burns
	#define DMG_SLOWBURN                    (1<<21)     // In an oven
	#define DMG_SLOWFREEZE                  (1<<22)     // In a subzero freezer
	#define DMG_MORTAR                      (1<<23)     // Hit by air raid (done to distinguish grenade from mortar)
	#define DMG_TIMEBASED                   (~(0x3fff)) // Mask for time-based damage
	*/
}

public cmd__test1(id) // TE_BEAMDISK
{
	new Float:origin[3];
	pev(id, pev_origin, origin);
	origin[2] -= 35.0;

	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, origin, 0)
	write_byte(TE_BEAMDISK)
	engfunc(EngFunc_WriteCoord, origin[0])
	engfunc(EngFunc_WriteCoord, origin[1])
	engfunc(EngFunc_WriteCoord, origin[2])
	engfunc(EngFunc_WriteCoord, origin[0])
	engfunc(EngFunc_WriteCoord, origin[1])
	
	new rd = random_num(1,9);
	new xadd, width, r,g,b,a;
	new life = rand(3,8);
	switch(rd)
	{
		case 1: {
			xadd = 10; /* радиус окружности */
			width = 32; /* количество граней диска */
			r = random_num(0,255);
			g = random_num(0,255);
			b = random_num(0,255);
			a = random_num(0,255);
		}
		case 2: {
			xadd = 30; /* радиус окружности */
			width = 32; /* количество граней диска */
			r = random_num(0,255);
			g = random_num(0,255);
			b = random_num(0,255);
			a = random_num(0,255);
		}
		case 3: {
			xadd = 30; /* радиус окружности */
			width = 64; /* количество граней диска */
			r = random_num(0,255);
			g = random_num(0,255);
			b = random_num(0,255);
			a = random_num(0,255);
		}	
		case 4: {
			xadd = 60; /* радиус окружности */
			life = 10;
			width = 32; /* количество граней диска */
			r = random_num(0,255);
			g = random_num(0,255);
			b = random_num(0,255);
			a = random_num(0,255);
		}
		case 5: {
			xadd = 120; /* радиус окружности */
			width = 32; /* количество граней диска */
			r = random_num(0,255);
			g = random_num(0,255);
			b = random_num(0,255);
			a = random_num(0,255);
		}
		case 6: {
			xadd = 240; /* радиус окружности */
			life = 10;
			width = 32; /* количество граней диска */
			r = random_num(0,255);
			g = random_num(0,255);
			b = random_num(0,255);
			a = random_num(0,255);
		}
		case 7: {
			xadd = 320; /* радиус окружности */
			width = 32; /* количество граней диска */
			r = random_num(0,255);
			g = random_num(0,255);
			b = random_num(0,255);
			a = random_num(0,255);
		}
		case 8: {
			xadd = 240; /* радиус окружности */
			width = 64; /* количество граней диска */
			r = random_num(0,255);
			g = random_num(0,255);
			b = random_num(0,255);
			a = random_num(0,255);
		}
		case 9: {
			xadd = 240; /* радиус окружности */
			width = 124; /* количество граней диска */
			r = random_num(0,255);
			g = random_num(0,255);
			b = random_num(0,255);
			a = random_num(0,255);
		}
	}

	engfunc(EngFunc_WriteCoord, origin[2] + float(xadd));

	/*new st = rand(1,9);
	switch(st){
		case 1: write_short(SPR_L2HIT);
		case 2: write_short(SPR_L2DAMAGE);
		case 3: write_short(SPR_DEATH_SPIKE_HIT);
		case 4: write_short(SPR_ZEROGXPLODE);
		case 5: write_short(SPR_LETHAL_SHOT);
		case 6: write_short(SPR_LETHALSHOT_TRAIL);
		case 7: write_short(SPR_NUC_RING);
		case 8: write_short(SPR_FX_M_T1014_2);
		case 9: write_short(SPR_DEADLYBLOW_TRAIL);
	}*/
	write_short(SPR_LETHALSHOT_TRAIL);
	
//	write_short(SPR_L2HIT)
	write_byte(0)
	write_byte(0)
	write_byte(life);
	write_byte(width);
	write_byte(random_num(0,1));
	write_byte(r)
	write_byte(g)
	write_byte(b)
	write_byte(a)
	write_byte(0)
	message_end()

	client_print(id, print_chat, "life: %d | width: %d | RGBA:%d,%d,%d,%d | z_add: %d | CASE=%d", life,width,r,g,b,a,xadd, rd/*, st*/);
	client_print(id, print_console, "life: %d | width: %d | RGBA:%d,%d,%d,%d | z_add: %d | CASE=%d", life,width,r,g,b,a,xadd, rd/*, st*/);
}

/**
 * Teleport behind player
 *
stock cmd__test2(id)
{
	new victim
	new body
	get_user_aiming(id, victim, body)
 
	if(body <= 0)
		return; //HAM_IGNORED
	
	if(cs_get_user_team(id) == cs_get_user_team(victim))
		return; //HAM_IGNORED
	
	new Float:fAngles[3]
	pev(victim, pev_angles, fAngles)
	
	new Float:fOrigin[3]
	pev(victim, pev_origin, fOrigin)
	
	new Float:fPlayerOrigin[3]						// Позиция
	new iPlayerOrigin[3]							// игрока
	pev(id, pev_origin, fPlayerOrigin)				// до
	FVecIVec(Float:fPlayerOrigin, iPlayerOrigin)	// телепортации
	
	angle_vector(fAngles, ANGLEVECTOR_FORWARD, fAngles)
	
	new Float:fSetAngles[3]
	vector_to_angle(fAngles, fSetAngles)
	
	xs_vec_neg(fAngles, fAngles)
	
	new Float:fDistance[3]
	xs_vec_mul_scalar(fAngles, 42.0, fDistance)
	
	xs_vec_add(fOrigin, fDistance, fDistance)
	
	fDistance[2] += 17.0

	//if(isHullVacant(fDistance, pev(id, pev_flags) & FL_DUCKING ? HULL_HEAD : HULL_HUMAN, id))
	if(isHullVacant(fDistance, pev(id, pev_flags) & FL_DUCKING ? HULL_HEAD : HULL_HUMAN))
	{
		set_pev(id, pev_angles, fSetAngles)
		set_pev(id, pev_fixangle, 1)
		set_pev(id, pev_origin, fDistance)
		
		//g_gametime[id] = get_gametime() + RESET_KNIFE_DELAY
		
		//engfunc(EngFunc_EmitSound, id, CHAN_STATIC, knife_teleport_sound, 1.0, ATTN_NORM, 0, PITCH_NORM)
		
		new iDistance[3]
		FVecIVec(Float:fDistance, iDistance)
		
		Create_TE_TELEPORT(iPlayerOrigin)
		Create_TE_IMPLOSION(iDistance, 100, 45, 3)
		//set_shake(id, 14, 10, 12)
	}
}*/

/**
 * TE_BEAMPOINTS by aim
 *
stock test__aim_TE_BEAMPOINTS(id)
{
	new iOrigin[3];
	get_user_origin(id, iOrigin);
	client_print(id, print_chat, "origin: %d, %d, %d", iOrigin[0], iOrigin[1], iOrigin[2]);
	new Float:fOrigin[3];

	// fm_get_aim_origin() from fakemet_util.inc
	new Float:start[3], Float:view_ofs[3];
	pev(id, pev_origin, start);
	pev(id, pev_view_ofs, view_ofs);
	xs_vec_add(start, view_ofs, start);

	new Float:dest[3];
	pev(id, pev_v_angle, dest);
	engfunc(EngFunc_MakeVectors, dest);
	global_get(glb_v_forward, dest);
	xs_vec_mul_scalar(dest, 9999.0, dest);
	xs_vec_add(start, dest, dest);

	new pTrace = create_tr2();
	engfunc(EngFunc_TraceLine, start, dest, 0, id, pTrace);
	get_tr2(pTrace, TR_vecEndPos, fOrigin);
	new hitent = get_tr2(pTrace, TR_pHit);
	free_tr2(pTrace);

	if(pev_valid(hitent)){
		new szClassname[32];
		pev(hitent, pev_classname, szClassname, charsmax(szClassname));
		client_print(id, print_chat, szClassname);
	}

	message_begin(MSG_BROADCAST ,SVC_TEMPENTITY)
	write_byte(TE_BEAMPOINTS)
	write_coord(iOrigin[0])	// start position
	write_coord(iOrigin[1])
	write_coord(iOrigin[2])
	write_coord(floatround(fOrigin[0]))	// end position
	write_coord(floatround(fOrigin[1]))
	write_coord(floatround(fOrigin[2]))
	write_short(sLKGlow)	// sprite index
	write_byte(0)	// starting frame
	write_byte(0)	// frame rate in 0.1's
	write_byte(50)	// life in 0.1's
	write_byte(10)	// line width in 0.1's
	write_byte(0)	// noise amplitude in 0.01's
	write_byte(255)	// Red
	write_byte(0)	// Green
	write_byte(0)	// Blue
	write_byte(127)	// brightness
	write_byte(0)	// scroll speed in 0.1's
	message_end()
}*/

/*public test__TE_BEAMDISK(id)
{
	new Float:origin[3];
	pev(id, pev_origin, origin);
	origin[2] -= 35.0;
	
	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, origin, 0)
	write_byte(TE_BEAMDISK)
	engfunc(EngFunc_WriteCoord, origin[0])
	engfunc(EngFunc_WriteCoord, origin[1])
	engfunc(EngFunc_WriteCoord, origin[2])
	engfunc(EngFunc_WriteCoord, origin[0])
	engfunc(EngFunc_WriteCoord, origin[1])
	new xadd = random_num(0,255); // радиус окружности
	engfunc(EngFunc_WriteCoord, origin[2] + float(xadd));
	write_short(SPR_SHOCKWAVE)
	write_byte(0)
	write_byte(0)
	new life = random_num(1,40);
	write_byte(life);
	new width = random_num(1,60); // количество граней диска
	write_byte(width);
	write_byte(random_num(0,1));
	new r = random_num(0,255);
	write_byte(r)
	new g = random_num(0,255);
	write_byte(g)
	new b = random_num(0,255);
	write_byte(b)
	new a = random_num(0,255);
	write_byte(a)
	write_byte(0)
	message_end()
	
	client_print(id, print_chat, "life: %d | width: %d | RGBA:%d,%d,%d,%d | z_add: %d", life,width,r,g,b,a,xadd);
	client_print(id, print_console, "life: %d | width: %d | RGBA:%d,%d,%d,%d | z_add: %d", life,width,r,g,b,a,xadd);
	
	// NOT WORKS!!
	//new origin[3];
	//get_user_origin(id, origin);
	//Create_TE_BEAMDISK(origin, SPR_FLAREBF, 0, 0, random_num(5,15), random_num(30,100), random_num(0,1), random_num(0,255),random_num(0,255),random_num(0,255),random_num(0,255), 0);
}*/

/*public test__TE_BEAMCYLINDER(id)
{
	new Float:origin[3], Float:axis[3];
	pev(id, pev_origin, origin);
	origin[2] -= 25.0;
	axis = origin;

	//new Float:aX = random_float(0.0, 200.0);
	new Float:aY = random_float(0.0, 200.0);
	//new Float:aZ = random_float(0.0, 200.0);

	//axis[0] -= aX;
	axis[0] += aY;
	//axis[2] += aZ;

	Create_TE_BEAMCYLINDER(origin, axis, SPR_SHOCKWAVE, 0, 0, 4, 23, 0, 130,61,67,255, 0);
	//Create_TE_BEAMCYLINDER(Float:coord[3], Float:axis[3], spr, startframe, framerate, life, width, noise, r,g,b,alpha, speed)

	client_print(id, print_chat, "axis add: %f ", aY);
	client_print(id, print_console, "axis add: %f ", aY);
}*/
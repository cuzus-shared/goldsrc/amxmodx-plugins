// --------------------------------------------------
// Stats
// --------------------------------------------------

enum _:MAP_STATS_VALUES
{
    MAP_STATS_PVP = 0,
    MAP_STATS_PK, 			//1
	MAP_STATS_DEATHS,		//2
	MAP_STATS_AVANPOST_DMG,	//3
	MAP_STATS_EARNED_ADENA,	//4
	MAP_STATS_TOTAL_ADENA	//5
};
new PlayerMapStats[XX][MAP_STATS_VALUES];

new GTeamScore[3]; // [0] - unused
/*
	get_user_team(id) == 1 // t
	get_user_team(id) == 2 // ct
	get_user_team(id) == 3 // spec
	get_user_team(id) == 0 // unnasigned
*/

/**
 * Updates team score.
 * Infinite task, called from <Warmup> (every 10s)
 */
public Stats_updater()
{
	setTeamScore(TEAMID_EAST, GTeamScore[TEAMID_EAST]);
	setTeamScore(TEAMID_WEST, GTeamScore[TEAMID_WEST]);
}

public Stats_getTeamScore(teamId)
{
	switch(teamId){
		case 1: return GTeamScore[TEAMID_EAST];
		case 2: return GTeamScore[TEAMID_WEST];
	}
	return 0;
}

// Reworked
public Stats_getTeamSco(teamId, retval[])
{
	new total = 0;
	if(equali(retval, "kills")){
		for(new id=1; id<=GMaxPlayers; id++){
			if(!isConnected(id) || getTeam(id) != teamId)
				continue;
			total += Stats_getPvP(id);
		}
		return total;
	}
	else if(equali(retval, "deaths")){
		for(new id=1; id<=GMaxPlayers; id++){
			if(!isConnected(id) || getTeam(id) != teamId)
				continue;
			total += Stats_getDeaths(id);
		}
		return total;
	}
	//else if(equali(retval, "pk"))
	
	return 0;
}

public Stats_getTopTeam()
{
	if(GTeamScore[TEAMID_EAST] > GTeamScore[TEAMID_WEST])
		return TEAMID_EAST;	// T
	return TEAMID_WEST;		// CT
}

/*public Stats_getTopPlayer() //CsTeams:Team)
{
	new max_kills = -999, max_deaths = 999;
	new top_killer //, prev_player;
	new frags[XX], deaths[XX];
	
	for(new id=1; id<=GMaxPlayers; id++)
	{
		if(!isConnected(id)) //|| cs_get_user_team(id) != Team)
			continue;
		
		frags[id] = get_user_frags(id);
		deaths[id] = get_user_deaths(id);
		
		if(frags[id] == max_kills)
		{
			if(deaths[id] < max_deaths)
			{
				max_kills = frags[id];
				max_deaths = deaths[id];
				top_killer = id;
			}
			//else : top_killer is prev player
		}
		else if(frags[id] > max_kills)
		{
			max_kills = frags[id];
			max_deaths = deaths[id];
			top_killer = id;
			//prev_player = id;
		}
	}
	return top_killer;
}*/

public Stats_getTopStat(getval[])
{
	new top_kills = 0, top_deaths = 0, top_killer = -1, top_loser = -1;

	for(new id=1; id<=GMaxPlayers; id++)
	{
		if(!isConnected(id))
			continue;
		
		new kills = Stats_getPvP(id);
		new deaths = Stats_getDeaths(id);
		
		if(kills > top_kills){
			top_killer = id;
			top_kills = kills;
		}
		if(deaths > top_deaths){
			top_loser = id;
			top_deaths = deaths;
		}
	}
	if(equali(getval, "killsnum")){
		return top_kills;
	}
	else if(equali(getval, "deathsnum")){
		return top_deaths;
	}
	else if(equali(getval, "killsid")){
		return top_killer;
	}
	else if(equali(getval, "deathsid")){
		return top_loser;
	}
	return 0;
}

public countScores()
{
	for(new team=1; team<3; team++){ //1,2
		GTeamScore[team] = 0;
	}
	for(new id=1; id<=GMaxPlayers; id++)
	{
		if(isConnected(id) && haveTeam(id)){
			GTeamScore[getTeam(id)] += PlayerMapStats[id][MAP_STATS_PVP];
			//GTeamScore[_:cs_get_user_team(id)] += PlayerMapStats[id][MAP_STATS_PVP];
			
			//Log(DEBUG, "Stats", "countScores(): GTeamScore[%d] += %d", getTeam(id), PlayerMapStats[id][MAP_STATS_PVP]);
		}
	}
}

/**
 * Function uses every team join or change.
 *
public msgTeamInfo(msg_id, msg_dest, msg_entity)
{
	if(warmupConfirmed())
	{
		new sztmp[2]; // OPT: ... , team
		get_msg_arg_string(2, sztmp, 1);
		switch(sztmp[0])
		{
			case 'T':	msg_entity = 1;
			case 'C':	msg_entity = 2;
			default:	msg_entity = 3;
		}
		msg_id = get_msg_arg_int(1); // OPT: id
		//if(msg_entity != _:cs_get_user_team(msg_id))
		if(msg_entity != getTeam(msg_id))
		{
			Log(DEBUG, "Stats", "msgTeamInfo(): %d != %d", msg_entity, getTeam(msg_id));
			
//			GPlayerTeam[msg_id] = msg_entity;
			countScores();
		}
	}
}
*/

//https://forums.alliedmods.net/showthread.php?t=165607

public msgScoreInfo(/*msg_id, msg_dest, msg_entity*/)
{
	if(warmupConfirmed()){
		//new frags = get_msg_arg_int(2);
		//new deaths = get_msg_arg_int(3);
		set_msg_arg_int(2, ARG_SHORT, Stats_getPvP(get_msg_arg_int(1)));
		set_msg_arg_int(3, ARG_SHORT, Stats_getPK(get_msg_arg_int(1)));
	}
	// if(warmupConfirmed())
	// {
		// msg_id = get_msg_arg_int(1);	// OPT: id
		// msg_dest = get_msg_arg_int(2);	// OPT: frags
		
		// Log(DEBUG, "Stats", "msgScoreInfo(): msg_id=%d msg_dest=%d", msg_id, msg_dest);
		
		// if(isConnected(msg_id) /*&& is_user_alive(msg_id)*/)
			// GTeamScore[getTeam(msg_id)] += (msg_dest - PlayerMapStats[msg_id][MAP_STATS_PVP]);
			// //GTeamScore[_:cs_get_user_team(msg_id)] += (msg_dest - PlayerMapStats[msg_id][MAP_STATS_PVP]);
		
		// // added @TEST
		// //PlayerMapStats[msg_id][MAP_STATS_PVP] = msg_dest;
	// }
}

public Stats_flush(id)
{
	countScores();
	
	PlayerMapStats[id][MAP_STATS_PVP]			= 0;
	PlayerMapStats[id][MAP_STATS_PK]			= 0;
	PlayerMapStats[id][MAP_STATS_DEATHS]		= 0;
	PlayerMapStats[id][MAP_STATS_AVANPOST_DMG]	= 0;
	PlayerMapStats[id][MAP_STATS_EARNED_ADENA]	= 0;
	PlayerMapStats[id][MAP_STATS_TOTAL_ADENA]	= 0;

//	GPlayerTeam[id] = -1;
}

// --------------------------------------------------
// setters, adders
// --------------------------------------------------
public Stats_addPK(id)
{
	if(PlayerMapStats[id][MAP_STATS_PK] <= 0)
		PlayerMapStats[id][MAP_STATS_PK] = 1;
	else
		PlayerMapStats[id][MAP_STATS_PK]++; 
}

public Stats_addPvP(id)
{
	if(PlayerMapStats[id][MAP_STATS_PVP] <= 0)
		PlayerMapStats[id][MAP_STATS_PVP] = 1;
	else
		PlayerMapStats[id][MAP_STATS_PVP]++; 
}

public Stats_addDeath(id)
{
	if(PlayerMapStats[id][MAP_STATS_DEATHS] <= 0)
		PlayerMapStats[id][MAP_STATS_DEATHS] = 1;
	else
		PlayerMapStats[id][MAP_STATS_DEATHS]++;
}

public Stats_addAvanpostDmg(id, Float:damage)
{
	PlayerMapStats[id][MAP_STATS_AVANPOST_DMG] += floatround(damage, floatround_round);
}

public Stats_addToTotalAdena(id, adena)
{
	PlayerMapStats[id][MAP_STATS_TOTAL_ADENA] += adena;
}

public Stats_addToEarnedAdena(id, adena)
{
//	if(Stats_getEarnedAdena(id) + adena <= ADENA_MAX_AMOUNT)
	PlayerMapStats[id][MAP_STATS_EARNED_ADENA] += adena;
//	else
//		Log(WARN, "Stats", "addEarnedAdena(): value > ADENA_MAX_AMOUNT");
}

public Stats_setEarnedAdena(id, new_adena)
{
	if(new_adena > 0 && new_adena <= ADENA_MAX_AMOUNT)
		PlayerMapStats[id][MAP_STATS_EARNED_ADENA] = new_adena;
	else
		Log(WARN, "Stats", "addEarnedAdena(): incorrect value to set");
}

// --------------------------------------------------
// getters
// --------------------------------------------------
public Stats_getPK(id)
{
	return PlayerMapStats[id][MAP_STATS_PK];
}

public Stats_getPvP(id)
{
	return PlayerMapStats[id][MAP_STATS_PVP];
}

public Stats_getDeaths(id)
{
	return PlayerMapStats[id][MAP_STATS_DEATHS];
}

public Stats_getAvanpostDmg(id)
{
	return PlayerMapStats[id][MAP_STATS_AVANPOST_DMG];
}

public Stats_getTotalAdena(id)
{
	return PlayerMapStats[id][MAP_STATS_TOTAL_ADENA];
}

public Stats_getEarnedAdena(id)
{
	return PlayerMapStats[id][MAP_STATS_EARNED_ADENA];
}

stock updateScoreBoard(id, kills, deaths)
{
	message_begin(MSG_ONE_UNRELIABLE, gmsgScoreInfo, _, id);
	write_byte(id);
	write_short(kills);
	write_short(deaths);
	write_short(0);
	write_short(getTeam(id));
	message_end();
}

stock setTeamScore(team, score)	// thanks to LYlink
{
	static const szteam[][] = {"", "TERRORIST", "CT"};
	message_begin(MSG_BROADCAST, gmsgTeamScore);
	write_string(szteam[team]);
	write_short(score);
	message_end();
}
// --------------------------------------------------
// Kill Streak
// --------------------------------------------------

new KS_FirstBlood = false;
new KS_TotalKills[XX];
new KS_RevengeKill[XX][MAX_CHARNAME_LENGTH];

public KillStreak_clientDisconnect(id)
{
	KS_TotalKills[id] = 0;
	KS_RevengeKill[id] = "";
}

public KillStreak_evDeathMsg(killer, victim, headshot)
{
	KS_TotalKills[victim] = 0;

	if(!killer)
		return;

	new victim_name[MAX_CHARNAME_LENGTH];
	get_user_name(victim, victim_name, MAX_CHARNAME_LENGTH-1);

	set_hudmessage(randInt(0,255),randInt(0,255),randInt(0,255), -1.0, 0.24, 0, 6.0, 5.0);

	new this[32];
	getEntClass(killer, this, 31);

	if(isGuard(this)){
		new arg1[12], arg2[32];
		strtok(this, arg1, 11, arg2, 31, '@');
		ShowSyncHudMsg(0, KillStreakSyncHud2, "%s was slayed by %s guards", victim_name, arg2);
	}
	else if(isMonster(this) && randInt(1,2)==1){
		new arg1[12], arg2[32];
		strtok(this, arg1, 11, arg2, 31, '@');
		ShowSyncHudMsg(0, KillStreakSyncHud2, "%s was killed by %s", victim_name, arg2);
	}
	// suicide
	else if(victim == killer){
		ShowSyncHudMsg(0, KillStreakSyncHud2, "%s blew up.", victim_name);
	}
	else if(isPlayer(this) /*isConnected(killer)*/)
	{
		new killer_name[MAX_CHARNAME_LENGTH];
		get_user_name(killer, killer_name, MAX_CHARNAME_LENGTH-1);

		KS_TotalKills[killer]++;
		KS_RevengeKill[victim] = killer_name;

		// revenge
		if(equal(victim_name, KS_RevengeKill[killer]))
		{
			KS_RevengeKill[killer] = "";
			ShowSyncHudMsg(killer, KillStreakSyncHud2, "You got avenged on %s!", victim_name);
			ShowSyncHudMsg(victim, KillStreakSyncHud2, "%s got avenged on you!", killer_name);
		}

		/*	
		// headshot (critical hit)
		if(headshot && g_iHeadshot){
			ShowSyncHudMsg(0, KillStreakSyncHud2, " ", killer_name, victim_name);
		}
		*/
		
		// First blood
		if(!KS_FirstBlood)
		{
			if(isDmGameMode() || (getTeam(victim) == getTeam(killer)))
				return;

			KS_FirstBlood = true;

			set_hudmessage(255,0,0, -1.0, 0.24, 0, 6.0, 5.0);
			ShowSyncHudMsg(0, KillStreakSyncHud2, "%s made First Blood!!! (+200 Adena)", killer_name);

			new adena_reward = getAdena(killer) + 200;
			setAdena(killer, adena_reward);
			Stats_addToTotalAdena(killer, 200);
			Stats_setEarnedAdena(killer, adena_reward);

			return;
		}

		// Team kill
		if(getTeam(victim) == getTeam(killer)){
			ShowSyncHudMsg(0, KillStreakSyncHud2, "%s is probably delusional...", killer_name);
			return;
		}

		static streak_msg[256];
		switch(KS_TotalKills[killer])
		{
			case 2: streak_msg = "%s made a double kill!";
			case 3: streak_msg = "%s is on triple kill!";
			case 5: streak_msg = "%s is on multikill!";
			case 6: streak_msg = "%s is on rampage!";
			case 7: streak_msg = "%s is on a killing spree!";
			case 9: streak_msg = "%s is dominating!";
			case 11: streak_msg = "%s is unstoppable!";
			case 13: streak_msg = "%s made a mega kill!";
			case 15: streak_msg = "%s made an ultra kill!";
			case 16: streak_msg = "%s has an eagle eye!";
			case 17: streak_msg = "%s owns!";
			case 18: streak_msg = "%s made a ludicrous kill!";
			case 19: streak_msg = "%s is a head hunter!";
			case 20: streak_msg = "%s is whicked sick!";
			case 21: streak_msg = "%s made a monster kill!";
			case 23: streak_msg = "Holy shit! %s got another one!";
			case 24: streak_msg = "%s is G o d L i k e !!!";
		}

		if(!equali(streak_msg, "") && strlen(streak_msg) > 6)
			streakDisplay(killer, streak_msg);
	}
}

streakDisplay(killer, msg[])
{
	set_hudmessage(randVal(255),randVal(255),randVal(255), -1.0, 0.27, 0, 6.0, 5.0);
	ShowSyncHudMsg(0, KillStreakSyncHud1, msg, getName(killer));
}
// --------------------------------------------------
// KF-like Slow Motion
// --------------------------------------------------

new bool:SLOWMO_COPIES_ENABLED = false;

#define SLOWMOTION_CHANCE			4
#define SLOWMOTION_CLONES_INTENS	55
#define SLOWMOTION_ATK_SPEED		3.5

new bool:slowMotion = false;

new SlowMotionClonesIntens[XX] = {SLOWMOTION_CLONES_INTENS, ...};

public SlowMo_start()
{
	if(!slowMotion && randomChance(SLOWMOTION_CHANCE))
	{
		Log(DEBUG, "SlowMo", "Slow Motion started");

		slowMotion = true;
		emit_sound(0, CHAN_AUTO, S_ZEDTIME_ENTER /*S_VAMPIRIC_TOUCH_CAST*/, randFloat(0.8,1.0), ATTN_NORM, 0, 95);
		
		set_task(3.4, "SlowMo_end", TASK_SLOWMOTION_END);
		
		if(SLOWMO_COPIES_ENABLED){
			for(new id=1; id<=GMaxPlayers; id++){
				if(isAlive(id)){
					SlowMotion_createCopies(id);
				}
			}
		}
	}
}

public SlowMotion_createCopies(id)
{
	set_task(0.2, "SlowMotion_newDummy", id+TASK_SLOWMOTION_CREATECLONE);
}

public SlowMotion_newDummy(id)
{
	id -= TASK_SLOWMOTION_CREATECLONE;

	if(SlowMotionClonesIntens[id] > 1)
		SlowMotionClonesIntens[id]--;
	else
		return;

	if(isAlive(id))
	{
		new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
		if(ent)
		{
			set_pev(ent, pev_classname, "slowmo_dummy");

			//new CsTeams:team;
			new CsInternalModel:model;
			//team = cs_get_user_team(id, model);
			cs_get_user_team(id, model);
			new ent_model[64];
			switch(model)
			{
				case 1: { ent_model = "models/player/urban/urban.mdl"; }		// CT urban
				case 2: { ent_model = "models/player/terror/terror.mdl"; }		// T terror
				case 3: { ent_model = "models/player/leet/leet.mdl"; }			// T leet
				case 4: { ent_model = "models/player/arctic/arctic.mdl"; }		// T arctic
				case 5: { ent_model = "models/player/gsg9/gsg9.mdl"; }			// CT gsg9
				case 6: { ent_model = "models/player/gign/gign.mdl"; }			// CT gign
				case 7: { ent_model = "models/player/sas/sas.mdl"; }			// CT sas
				case 8: { ent_model = "models/player/guerilla/guerilla.mdl"; }	// T guerilla
			}

			engfunc(EngFunc_SetModel, ent, ent_model);
			engfunc(EngFunc_SetSize, ent, Float:{-16.0, -16.0, -76.0}, Float:{16.0, 16.0, 76.0});

			new Float:origin[3];
			pev(id, pev_origin, origin);
			set_pev(ent, pev_origin, origin);
			pev(id, pev_angles, origin); //angles
			set_pev(ent, pev_angles, origin);

			set_pev(ent, pev_solid, SOLID_NOT);
			set_pev(ent, pev_sequence, 75); // TODO: get curr player seq
			set_pev(ent, pev_animtime, 2.0);
			set_pev(ent, pev_framerate, 1.0);
			set_pev(ent, pev_nextthink, 1.0);

			engfunc(EngFunc_DropToFloor, ent);
			fm_set_user_rendering(ent, kRenderFxGlowShell, 0,0,0, kRenderTransAlpha, SlowMotionClonesIntens[id]);

			set_task(0.7, "SlowMotion_delDummy", ent+TASK_SLOWMOTION_REMOVECLONE);
		}
	}

	set_task(0.4 /*0.2*/, "SlowMotion_newDummy", id+TASK_SLOWMOTION_CREATECLONE);
}

public SlowMotion_delDummy(ent)
{
	ent -= TASK_SLOWMOTION_REMOVECLONE;
	if(pev_valid(ent)){
		entityDispose(ent);
	}
}

public SlowMotion_reset(id)
{
	if(isAlive(id))
		set_user_gravity(id, 1.0);

	if(SLOWMO_COPIES_ENABLED)
	{
		SlowMotionClonesIntens[id] = SLOWMOTION_CLONES_INTENS;
		
		if(task_exists(id+TASK_SLOWMOTION_CREATECLONE))
			remove_task(id+TASK_SLOWMOTION_CREATECLONE);
	}
}

public SlowMo_end(taskid)
{
	if(slowMotion)
	{
		emit_sound(0, CHAN_AUTO, S_ZEDTIME_EXIT /*S_VAMPIRIC_TOUCH_SHOT*/, random_float(0.8,1.0), ATTN_NORM, 0, PITCH_NORM);

		for(new id=1; id<=GMaxPlayers; id++){
			SlowMotion_reset(id);
		}
		slowMotion = false;
		
		Log(DEBUG, "SlowMo", "Slow Motion ended");
	}
}
// ----------------------------------------------------------------------
// Effects
// ----------------------------------------------------------------------

//	Entity draw effects
// #define EF_BRIGHTFIELD 1 // swirling cloud of particles
// #define EF_MUZZLEFLASH 2 // single frame ELIGHT on entity attachment 0
// #define EF_BRIGHTLIGHT 4 // DLIGHT centered at entity origin
// #define EF_DIMLIGHT 8 // player flashlight
// #define EF_INVLIGHT 16 // get lighting from ceiling
// #define EF_NOINTERP 32 // don't interpolate the next frame
// #define EF_LIGHT 64 // rocket flare glow sprite
// #define EF_NODRAW 128 // don't draw entity

/*
Render Modes (fakemeta):
	0 - Normal
	1 - Color
	2 - Texture
	3 - Glow
	4 - Solid
	5 - Additive

Render FX ( set_pev(ent, pev_renderfx, [INT]) )
	0 - Normal
	1 - Slow Pulse
	2 - Fast Pulse
	3 - Slow Wide Pulse
	4 - Fast Wide Pulse
	9 - Slow Strobe
	10 - Fast Strobe
	11 - Faster Strobe
	12 - Slow Flicker
	13 - Fast Flicker
	5 - Slow Fade Away
	6 - Fast Fade Away
	7 - Slow Become Solid
	8 - Fast Become Solid
	14 - Constant Glow
	15 - Distort
	16 - Hologram (Distort + fade)

Render Modes (engine):
	kRenderNormal
	kRenderTransColor
	kRenderTransTexture	
	kRenderGlow	
	kRenderTransAlpha
	kRenderTransAdd

Render FX (engine):
	kRenderFxNone = 0,
	kRenderFxPulseSlow,
	kRenderFxPulseFast,
	kRenderFxPulseSlowWide,
	kRenderFxPulseFastWide,
	kRenderFxFadeSlow,
	kRenderFxFadeFast,
	kRenderFxSolidSlow,
	kRenderFxSolidFast,
	kRenderFxStrobeSlow,
	kRenderFxStrobeFast,
	kRenderFxStrobeFaster,
	kRenderFxFlickerSlow,
	kRenderFxFlickerFast,
	kRenderFxNoDissipation,
	kRenderFxDistort, // Distort/scale/translate flicker
	kRenderFxHologram, // kRenderFxDistort + distance fade
	kRenderFxDeadPlayer, // kRenderAmt is the player index
	kRenderFxExplode, // Scale up really big!
	kRenderFxGlowShell, // Glowing Shell
	kRenderFxClampMinScale, // Keep this sprite from getting very small (SPRITES only!)
*/

stock Create_SingleLine(Float:from[3], Float:to[3], r,g,b, sprite) // used in Last Hero
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(0);
	write_coord(floatround(from[0]));
	write_coord(floatround(from[1]));
	write_coord(floatround(from[2]));
	write_coord(floatround(to[0]));
	write_coord(floatround(to[1]));
	write_coord(floatround(to[2]));
	write_short(sprite);
	write_byte(1);
	write_byte(1);
	write_byte(10);
	write_byte(10);
	write_byte(0);
	write_byte(r);
	write_byte(g);
	write_byte(b);
	write_byte(100);
	write_byte(0);
	message_end();
}

/*
Create_ScreenFade(player, 1<<0, 1<<0, 1<<2, 0,0,0,255, true);		// instant fade to black
Create_ScreenFade(player, 1<<0, 1<<0, 1<<0, 0,0,0,0, true);			// clear
*/
stock Create_ScreenFade(id, duration, holdtime, fadetype, r,g,b,a, bool:reliable)
{
	if(id > 0)
	{
		new MSG_DEST;
		switch(id)
		{
			/*case 0: // to all
			{
				if(reliable) MSG_DEST = MSG_ALL;
				else MSG_DEST = MSG_BROADCAST;
			}*/
			default: // playerid
			{
				if(reliable) MSG_DEST = MSG_ONE;
				else MSG_DEST = MSG_ONE_UNRELIABLE;
			}
		}

		message_begin(MSG_DEST, gmsgScreenFade, _, id);
		write_short(duration); //(1<<12)*duration
		write_short(holdtime); //(1<<12)*holdtime
		write_short(fadetype);
		write_byte(r);
		write_byte(g);
		write_byte(b);
		write_byte(a);
		message_end();
	}
}

/*
stock set_shake(const player, iAmount = 14, iLong = 10, iFreq = 13)
{	
	message_begin(MSG_ONE, get_user_msgid("ScreenShake"), {0,0,0}, player)
	write_short(1<<iAmount) 	// shake amount
	write_short(1<<iLong) 		// shake lasts this long
	write_short(1<<iFreq) 		// shake noise frequency
	message_end()
}
*/
stock Create_ScreenShake(id, amount, duration, freq, bool:reliable)
{
	if(id > 0)
	{
		new MSG_DEST;
		switch(id)
		{
			/*case 0: // to all
			{
				if(reliable) MSG_DEST = MSG_ALL;
				else MSG_DEST = MSG_BROADCAST;
			}*/
			default: // playerid
			{
				if(reliable) MSG_DEST = MSG_ONE;
				else MSG_DEST = MSG_ONE_UNRELIABLE;
			}
		}

		message_begin(MSG_DEST, gmsgScreenShake, {0,0,0}, id);
		write_short(amount);
		write_short(duration);
		write_short(freq);
		message_end();
	}
}
// TODO: rework to int values
/*
stock set_shake(const player, iAmount = 14, iLong = 10, iFreq = 13)
{	
	message_begin(MSG_ONE, get_user_msgid("ScreenShake"), {0,0,0}, player)
	write_short(1<<iAmount) 	// shake amount
	write_short(1<<iLong) 		// shake lasts this long
	write_short(1<<iFreq) 		// shake noise frequency
	message_end()
}
*/

stock Create_TE_KILLPLAYERATTACH(id)
{
	message_begin(MSG_ALL, SVC_TEMPENTITY);
	write_byte(TE_KILLPLAYERATTACHMENTS);
	write_byte(id);
	message_end();
}

// Kill all beams attached to entity
stock Create_TE_KILLBEAM(id)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_KILLBEAM);
	write_short(id);
	message_end();
}

// Moving model from id to origin while not touch
stock Create_TE_PROJECTILE(id, origin[3], velocity[3], model, unknown)
{
	message_begin(MSG_PVS, SVC_TEMPENTITY, origin);
	write_byte(TE_PROJECTILE);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2]);
	write_coord(velocity[0]);
	write_coord(velocity[1]);
	write_coord(velocity[2]);
	write_short(model);
	write_byte(unknown) // might have to adjust this	// ???
	write_byte(id);
	message_end();
}

stock Create_TE_SPARKS(origin[3])
{
	message_begin(MSG_ALL, SVC_TEMPENTITY, origin);
	write_byte(TE_SPARKS);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2]);
	message_end();
}

stock Create_Decal(iOrigin[3], decal_index, bool:create_sparks = false, entity = 0)
{
	if(decal_index && !entity)
	{
		message_begin(MSG_ALL, SVC_TEMPENTITY);
		write_byte(TE_WORLDDECAL);
		write_coord(iOrigin[0]);
		write_coord(iOrigin[1]);
		write_coord(iOrigin[2]);
		write_byte(decal_index);
		message_end();
	}
	else if(decal_index && !is_user_alive(entity) && pev_valid(entity))
	{
		message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
		write_byte(TE_DECAL);
		write_coord(iOrigin[0]);
		write_coord(iOrigin[1]);
		write_coord(iOrigin[2]);
		write_byte(decal_index);
		write_short(entity);
		message_end();
	}
	if(create_sparks)
	{
		Create_TE_SPARKS(iOrigin);
	}
	return 1
}

// Black ground after smoke
/*
new iorigin[3];
FVecIVec(Float:target_origin, iorigin);
Create_TE_WORLDDECAL(iorigin, 47); // test
*/
stock Create_TE_WORLDDECAL(origin[3], decal)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_WORLDDECAL);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2]);
	write_byte(decal);
	message_end();
}

/*
#define TE_EXPLFLAG_NONE            0        // Все Флаги по умолчанию
#define TE_EXPLFLAG_NOADDITIVE      1        // Спрайт будет не прозрачным(указывается при создании спрайта)
#define TE_EXPLFLAG_NODLIGHTS       2        // Не создовать подсветку 
#define TE_EXPLFLAG_NOSOUND         4        // Не проигрывать звук взрыва
#define TE_EXPLFLAG_NOPARTICLES     8        // Не создовать частиц
*/
stock Create_TE_EXPLOSION(Float:origin[3], sprite, scale, framerate, explflag)
{
	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, origin, 0);
	write_byte(TE_EXPLOSION);
	engfunc(EngFunc_WriteCoord, origin[0]);
	engfunc(EngFunc_WriteCoord, origin[1]);
	engfunc(EngFunc_WriteCoord, origin[2]);
	write_short(sprite);
	write_byte(scale);
	write_byte(framerate);
	write_byte(explflag);
	message_end();
}

// Quake1 colormaped (base palette) particle explosion with sound)
stock Create_TE_EXPLOSION2(origin[3], startcolor, numcolors)
{
	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, origin, 0);
	write_byte(TE_EXPLOSION2);
	engfunc(EngFunc_WriteCoord, origin[0]);
	engfunc(EngFunc_WriteCoord, origin[1]);
	engfunc(EngFunc_WriteCoord, origin[2]);
	write_byte(startcolor);	// start color
	write_byte(numcolors);	// num colors
	message_end();
}

// Additive sprite, plays 1 cycle
stock Create_TE_SPRITE(id=0, origin[3], sprite, scale, brightness)
{
	if(id>0 && id<33) // show only to 1 player
		message_begin(MSG_ONE, SVC_TEMPENTITY, .player = id);
	else // show to all
		message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	
	write_byte(TE_SPRITE);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2]);
	write_short(sprite);
	write_byte(scale);
	write_byte(brightness);
	message_end();
}

// Line of moving glow sprites with gravity, fadeout, and collisions
stock Create_TE_SPRITETRAIL(start[3], end[3], sprite, count, life, scale, velocity, random)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_SPRITETRAIL);
	write_coord(start[0]);
	write_coord(start[1]);
	write_coord(start[2]);
	write_coord(end[0]);
	write_coord(end[1]);
	write_coord(end[2]);
	write_short(sprite);
	write_byte(count);
	write_byte(life);		// life in 0.1's
	write_byte(scale);		// scale in 0.1's
	write_byte(velocity);	// velocity along vector in 10's
	write_byte(random);		// randomness of velocity in 10's
	message_end();
}

// Dynamic light, effect world, minor entity effect
stock Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)
{
	// only for players
	new origin[3];
	get_user_origin(id, origin);

	message_begin(MSG_PVS, SVC_TEMPENTITY, origin);
	write_byte(TE_DLIGHT);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2]);
	write_byte(radius);
	write_byte(r);
	write_byte(g);
	write_byte(b);
	write_byte(life);
	write_byte(decayrate);
	message_end();
}
stock Create_TE_DLIGHT_AT_LOC(origin[3], radius, r,g,b, life, decayrate)
{
	message_begin(MSG_PVS, SVC_TEMPENTITY, origin);
	write_byte(TE_DLIGHT);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2]);
	write_byte(radius);
	write_byte(r);
	write_byte(g);
	write_byte(b);
	write_byte(life);
	write_byte(decayrate);
	message_end();
}

/*
	selection            
	bucket1               
	bucket2               
	bucket3               
	bucket4                
	bucket5                
	bucket0                
	dmg_bio                
	dmg_poison           
	dmg_chem            
	dmg_cold        
	dmg_drown            
	dmg_heat           
	dmg_gas               
	dmg_rad                
	dmg_shock            
	number_0            
	number_1            
	number_2          
	number_3            
	number_4            
	number_5           
	number_6            
	number_7            
	number_8           
	number_9            
	divider                
	cross                
	dollar
	minus
	plus
	c4
	defuser
	stopwatch           
	smallskull              
	smallc4             
	smallvip               
	buyzone
	rescue
	escape
	vipsafety
	suit_full            
	suit_empty
	suithelmet_full
	suithelmet_empty
	flash_full
	flash_empty
	flash_beam
	train_back
	train_stop
	train_forward1
	train_forward2
	train_forward3
	autoaim_c            
	title_half
	title_life
	d_knife
	d_ak47
	d_awp
	d_deagle
	d_flashbang
	d_fiveseven
	d_g3sg1
	d_glock18
	d_grenade
	d_m249
	d_m3
	d_m4a1
	d_mp5navy
	d_p228
	d_p90
	d_scout
	d_sg550
	d_sg552
	d_ump45
	d_usp
	d_tmp
	d_xm1014
	d_skull
	d_tracktrain
	d_aug
	d_mac10
	d_elite
	d_headshot
	item_battery
	item_healthkit
	item_longjump
	radar
*/
#define ICON_HIDE	0
#define ICON_SHOW	1
#define ICON_FLASH	2
stock Create_StatusIcon(id, status, sprite[], red, green, blue)
{
	message_begin(MSG_ONE, gmsgStatusIcon, {0,0,0}, id);
	write_byte(status);		// 0 - hide, 1 - show, 2 - flash
	write_string(sprite);	// sprite name
	write_byte(red);
	write_byte(green);
	write_byte(blue);
	message_end();
}

stock Create_BarTime(id, duration)
{
	message_begin(MSG_ONE, gmsgBarTime, _, id); //MSG_ONE_UNRELIABLE
	write_short(duration);
	message_end();
}

stock Create_BarTime2(id, duration, start_percent)
{
	message_begin(MSG_ONE, get_user_msgid("BarTime2"), _, id);
	write_short(duration);
	write_short(start_percent);
	message_end();
}
/*
Make_BarTime2(id, Float:flSeconds) 
{ 
    new iRoundedSeconds = floatround(flSeconds, floatround_ceil) 
    new iStartPercent = floatround((1.0-(flSeconds / iRoundedSeconds))*100) 
    message_begin(MSG_ONE_UNRELIABLE, g_iBarTime2, .player=id) 
    write_short(iRoundedSeconds) 
    write_short(iStartPercent) 
    message_end() 
}

BarTime2(id, Float:flTime = 0.0)
{
    if( flTime < -0.0 )
    {
        return
    }
    static BarTime2 = 0
    if( !BarTime2 )
    {
        BarTime2 = get_user_msgid("BarTime2")
    }

    new iSeconds = floatround(flTime, floatround_ceil)

    message_begin(MSG_ONE, BarTime2, .player=id)
    write_long( iSeconds | ( ( 100 - floatround( (flTime/ iSeconds ) * 100 ) ) << 8 ) )
    message_end()
}
*/

// Tracers moving toward a point
stock Create_TE_IMPLOSION(position[3], radius, count, life)
{
	message_begin(MSG_PVS, SVC_TEMPENTITY, position);
	write_byte(TE_IMPLOSION);
	write_coord(position[0]);
	write_coord(position[1]);
	write_coord(position[2]);
	write_byte(radius);
	write_byte(count);
	write_byte(life); // life in 0.1's
	message_end();
}

stock Create_TE_BEAMFOLLOW(entity, spr, life, width, red, green, blue, alpha)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_BEAMFOLLOW);
	write_short(entity);
	write_short(spr);
	write_byte(life);
	write_byte(width);
	write_byte(red);
	write_byte(green);
	write_byte(blue);
	write_byte(alpha);
	message_end();
}

/*
// nice example
new pOrigin[3];
new Float:pOriginF[3];
//fm_get_aim_origin(touched, pOriginF);
pev(touched, pev_origin, pOriginF)
pOrigin[0] = floatround(pOriginF[0], floatround_round);
pOrigin[1] = floatround(pOriginF[1], floatround_round);
pOrigin[2] += 24;
pOrigin[2] = floatround(pOriginF[2], floatround_round);
Create_TE_BREAKMODEL(pOrigin, 6, 6, 6, random_num(-50,50), random_num(-50,50), 25, 10, mDeathSpikeHit, 10, 25, 0x01);
*/
/*
#define BREAK_TYPEMASK 0x4F
#define BREAK_GLASS 0x01
#define BREAK_METAL 0x02
#define BREAK_FLESH 0x04
#define BREAK_WOOD 0x08

#define BREAK_SMOKE 0x10
#define BREAK_TRANS 0x20
#define BREAK_CONCRETE 0x40
#define BREAK_2 0x80
*/
stock Create_TE_BREAKMODEL(pOrigin[3], sizex, sizey, sizez, velx, vely, velz, velrnd, spr, count, life, flags)
{
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY, pOrigin);
	write_byte(TE_BREAKMODEL);
	write_coord(pOrigin[0]);			// x
	write_coord(pOrigin[1]);			// y
	write_coord(pOrigin[2]);			// z
	write_coord(sizex);					// size x
	write_coord(sizey);					// size y
	write_coord(sizez);					// size z
	write_coord(velx);					// velocity x
	write_coord(vely);					// velocity y
	write_coord(velz);					// velocity z
	write_byte(velrnd);					// random velocity
	write_short(spr);
	write_byte(count);					// count
	write_byte(life);					// life
	write_byte(flags);					// BREAK_GLASS
	message_end();
}

// Quake1 lava splash
stock Create_TE_LAVASPLASH(position[3])
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_LAVASPLASH);
	write_coord(position[0]);
	write_coord(position[1]);
	write_coord(position[2]);
	message_end();
}

stock Create_TE_PARTICLEBURST(origin[3], radius, color, duration)
{
	message_begin(MSG_PVS, SVC_TEMPENTITY, origin)
	write_byte(TE_PARTICLEBURST)
	write_coord(origin[0])	// x
	write_coord(origin[1])	// y
	write_coord(origin[2])	// z
	write_short(radius)		// radius
	write_byte(color)		// color
	write_byte(duration)	// duration (will be randomized a bit)
	message_end()
}

// create a funnel of sprites
stock Create_TE_LARGEFUNNEL(position[3], iSprite, flags)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)	
	write_byte(TE_LARGEFUNNEL)
	write_coord(position[0])		// origin, x
	write_coord(position[1])		// origin, y
	write_coord(position[2])		// origin, z
	write_short(iSprite)			// sprite (0 for none)
	write_short(flags)				// 0 for collapsing, 1 for sending outward
	message_end()
}

stock Create_TE_TAREXPLOSION(position[3])
{
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(TE_TAREXPLOSION);
	write_coord(position[0]);
	write_coord(position[1]);
	write_coord(position[2]);
	message_end();
}

// Quake1 teleport splash
stock Create_TE_TELEPORT(position[3])
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_TELEPORT);
	write_coord(position[0]);
	write_coord(position[1]);
	write_coord(position[2]);
	message_end();
}

/*
stock createDeathMsg(killer_id,victim_id,headshot,weaponname[])
{
	message_begin(MSG_ALL,get_user_msgid("DeathMsg"))
	write_byte(killer_id)
	write_byte(victim_id)
	write_byte(headshot)
	write_string(weaponname)
	message_end()
}

stock Create_ScreenShake(id, amount, duration, frequency)
{
	message_begin(MSG_ONE,gmsgScreenShake,{0,0,0},id) 
	write_short( amount )				// ammount 
	write_short( duration )				// lasts this long 
	write_short( frequency )			// frequency
	message_end()
}

stock Create_ScoreInfo(id,frags,deaths,playerClass,team)
{
	message_begin(MSG_ALL,gmsgScoreInfo)
	write_byte(id)
	write_short(frags)
	write_short(deaths)
	write_short(playerClass)
	write_short(team)
	message_end()
}

stock Create_StatusText(id, linenumber, text[])
{
	message_begin(MSG_ONE, gmsgStatusText, {0,0,0}, id)
	write_byte(linenumber)	// line number of status bar text
	write_string(text)		// status bar text
	message_end()
}
*/

// Beam effect between two points
stock Create_TE_BEAMPOINTS(start[3], end[3], spr, startframe=0, framerate=0, life, width, noise=0, red, green, blue, alpha, speed=0)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_BEAMPOINTS);
	write_coord(start[0]);
	write_coord(start[1]);
	write_coord(start[2]);
	write_coord(end[0]);
	write_coord(end[1]);
	write_coord(end[2]);
	write_short(spr);				// sprite
	write_byte(startframe);			// start frame
	write_byte(framerate);			// framerate (frame rate in 0.1's)
	write_byte(life);				// life (life in 0.1's)
	write_byte(width);				// width (line width in 0.1's)
	write_byte(noise);				// noise (noise amplitude in 0.01's)
	write_byte(red);				// red
	write_byte(green);				// green
	write_byte(blue);				// blue
	write_byte(alpha);				// brightness
	write_byte(speed);				// speed (scroll speed in 0.1's)
	message_end();
}

// (!) bugged
// A beam with a sprite at the end
/*stock Create_TE_BEAMSPRITE(Float:start[3], Float:end[3], spr_beam, spr_end, reliable=0)
{
	message_begin(reliable ? MSG_ALL : MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_BEAMSPRITE);
	engfunc(EngFunc_WriteCoord, start[0]);
	engfunc(EngFunc_WriteCoord, start[1]);
	engfunc(EngFunc_WriteCoord, start[2]);
	engfunc(EngFunc_WriteCoord, end[0]);
	engfunc(EngFunc_WriteCoord, end[1]);
	engfunc(EngFunc_WriteCoord, end[2]);
	write_short(spr_beam);
	write_short(spr_end);
	message_end();
}*/

// Beam effect between point and entity
stock Create_TE_BEAMENTPOINT(entid, origin[3], sprite, startframe, framerate, life, width, amplitude, r,g,b, brightness)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
	write_byte(TE_BEAMENTPOINT)
	write_short(entid)
	write_coord(origin[0])
	write_coord(origin[1])
	write_coord(origin[2])
	write_short(sprite);
	write_byte(startframe)
	write_byte(framerate)
	write_byte(life)
	write_byte(width)
	write_byte(amplitude)
	write_byte(r)
	write_byte(g)
	write_byte(b)
	write_byte(brightness)
	write_byte(0)
	message_end()
}

stock Create_TE_BEAMENTS(startEntity, endEntity, iSprite, startFrame, frameRate, life, width, noise, red, green, blue, alpha, speed)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
	write_byte(TE_BEAMENTS)
	write_short(startEntity)		// start entity
	write_short(endEntity)			// end entity
	write_short(iSprite)			// model
	write_byte(startFrame)			// starting frame
	write_byte(frameRate)			// frame rate
	write_byte(life)				// life
	write_byte(width)				// line width
	write_byte(noise)				// noise amplitude
	write_byte(red)					// red
	write_byte(green)				// green
	write_byte(blue)				// blue
	write_byte(alpha)				// brightness
	write_byte(speed)				// scroll speed
	message_end()
}

// Point entity light, no world effect
stock Create_TE_ELIGHT(entity, start[3], radius, red, green, blue, life, decayRate)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
	write_byte(TE_ELIGHT)
	write_short(entity)				// entity
	write_coord(start[0])			// initial position
	write_coord(start[1])			// initial position
	write_coord(start[2])			// initial position
	write_coord(radius)				// radius
	write_byte(red)					// red
	write_byte(green)				// green
	write_byte(blue)				// blue
	write_byte(life)				// life
	write_coord(decayRate)			// decay rate
	message_end()
}

stock Create_TE_SPRAY(position[3], direction[3], sprite, count, speed, noise, rendermode)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_SPRAY);
	write_coord(position[0]);	// Position
	write_coord(position[1]);
	write_coord(position[2]);
	write_coord(direction[0]);	// Direction 
	write_coord(direction[1]);
	write_coord(direction[2]);
	write_short(sprite);
	write_byte(count);
	write_byte(speed);
	write_byte(noise);
	write_byte(rendermode);
	message_end();
}

stock Create_TE_PLAYERATTACHMENT(id, entity, vOffset, iSprite, life)
{
	message_begin(MSG_ONE, SVC_TEMPENTITY, {0, 0, 0}, id)
	write_byte(TE_PLAYERATTACHMENT)
	write_byte(entity)			// entity index of player
	write_coord(vOffset)		// vertical offset ( attachment origin.z = player origin.z + vertical offset )
	write_short(iSprite)		// model index
	write_short(life)			// (life * 10 )
	message_end()
}

stock Create_TE_BUBBLES(start[3], end[3], height, iSprite, count, speed)
{
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY) 
	write_byte(TE_BUBBLES)
	write_coord(start[0])			// start position
	write_coord(start[1]) 
	write_coord(start[2]) 
	write_coord(end[0])				// end position
	write_coord(end[1]) 
	write_coord(end[2]) 
	write_coord(height)				// float height
	write_short(iSprite)			// Sprite Index
	write_byte(count)				// count
	write_coord(speed)				// speed
	message_end()
}

/**
 * Автоматически определяет количество углов цилиндра
 * Расширяется всегда из центральной точки coord
 * Радиус раширения - axis[2] (чем больше тем шире будет расширение цилиндра)
 */
stock Create_TE_BEAMCYLINDER(Float:coord[3], Float:axis[3], spr, startframe, framerate, life, width, noise, r,g,b,alpha, speed)
{
	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, coord, 0);
	write_byte(TE_BEAMCYLINDER)
	engfunc(EngFunc_WriteCoord, coord[0]) // x
	engfunc(EngFunc_WriteCoord, coord[1]) // y
	engfunc(EngFunc_WriteCoord, coord[2]) // z
	engfunc(EngFunc_WriteCoord, axis[0]) // x axis
	engfunc(EngFunc_WriteCoord, axis[1]) // y axis
	engfunc(EngFunc_WriteCoord, axis[2]) // z axis
	write_short(spr) // sprite
	write_byte(startframe) // startframe
	write_byte(framerate) // framerate
	write_byte(life) // life
	write_byte(width) // width
	write_byte(noise) // noise
	write_byte(r) // red
	write_byte(g) // green
	write_byte(b) // blue
	write_byte(alpha) // brightness
	write_byte(speed) // speed
	message_end();
}

stock Create_TE_BEAMDISK(Float:origin[3], Float:axis[3], sprite, frame, framerate, life, width, noise, r,g,b,a, speed)
{
	//message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, origin, 0);
	write_byte(TE_BEAMDISK);
	engfunc(EngFunc_WriteCoord, origin[0]);
	engfunc(EngFunc_WriteCoord, origin[1]);
	engfunc(EngFunc_WriteCoord, origin[2]);
	engfunc(EngFunc_WriteCoord, axis[0]);
	engfunc(EngFunc_WriteCoord, axis[1]);
	engfunc(EngFunc_WriteCoord, axis[2]);
	write_short(sprite);
	write_byte(frame);
	write_byte(framerate);
	write_byte(life);
	write_byte(width);
	write_byte(noise);
	write_byte(r);
	write_byte(g);
	write_byte(b);
	write_byte(a);
	write_byte(speed);
	message_end();
}

stock Create_TE_BUBBLETRAIL(min[3], max[3], height, sprite, count, speed)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_BUBBLETRAIL);
	write_coord(min[0]);	// min start position
	write_coord(min[1]);
	write_coord(min[2]);
	write_coord(max[0]);	// max start position
	write_coord(max[1]);
	write_coord(max[2]);
	write_coord(height);	// float height
	write_short(sprite);	// model index
	write_byte(count);		// count
	write_coord(speed);		// speed
	message_end();
}

// Spherical shower of models, picks from set
stock Create_TE_EXPLODEMODEL(origin[3], velocity, model, count, life)
{
	//message_begin(MSG_BROADCAST,SVC_TEMPENTITY, {0,0,0}, id)
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY, {0,0,0}, 0);
	write_byte(TE_EXPLODEMODEL);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2]);
	write_coord(velocity);
	write_short(model);
	write_short(count);
	write_byte(life);
	message_end();
}

stock Create_TE_STREAK_SPLASH(Float:origin[3], Float:vect[3] = {-1.0, 1.0, 0.0}, color, count, speed, random_velocity, reliable=0)
{
	engfunc(EngFunc_MessageBegin, reliable ? MSG_PVS_R : MSG_PVS, SVC_TEMPENTITY, origin, 0);
	write_byte(TE_STREAK_SPLASH);
	engfunc(EngFunc_WriteCoord, origin[0]);
	engfunc(EngFunc_WriteCoord, origin[1]);
	engfunc(EngFunc_WriteCoord, origin[2]);
	engfunc(EngFunc_WriteCoord, vect[0]);
	engfunc(EngFunc_WriteCoord, vect[1]);
	engfunc(EngFunc_WriteCoord, vect[2]);
	write_byte(color);
	//write_byte(min(iColor, 255));
	write_short(count);
	write_short(speed);
	write_short(random_velocity);
	message_end();
}

/* Tested+works but not edited for stock */
// Need re-test : blood color can be set?
stock Create_TE_BLOODSTREAM(id)
{
	new origin[3];
	get_user_origin(id, origin);

	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(TE_BLOODSTREAM);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2]+10);
	write_coord(random_num(-360,360));	// ???
	write_coord(random_num(-360,360));	// ???
	write_coord(-10);					// ???
	write_byte(70);						// ???
	write_byte(random_num(50,100));		// ???
	message_end();
}

// Alphablend sprite, move vertically 30 pps
stock Create_TE_SMOKE(Float:origin[3], spr, scale, framerate=0)
{
	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, origin, 0);
	write_byte(TE_SMOKE);
	engfunc(EngFunc_WriteCoord, origin[0]);	// x
	engfunc(EngFunc_WriteCoord, origin[1]);	// y
	engfunc(EngFunc_WriteCoord, origin[2]);	// z
	write_short(spr);						// sprite
	write_byte(scale);						// scale in 0.1's
	write_byte(framerate);					// framerate
	message_end();
}

// Spray of opaque sprite1's that fall, single sprite2 for 1..2 secs (this is a high-priority tent)
stock Create_TE_BLOODSPRITE(Float:end[3], bloodspray, blooddrop, color, scale)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_BLOODSPRITE);
	engfunc(EngFunc_WriteCoord, end[0]);
	engfunc(EngFunc_WriteCoord, end[1]);
	engfunc(EngFunc_WriteCoord, end[2]);
	write_short(bloodspray);
	write_short(blooddrop);
	write_byte(color);
	write_byte(scale);
	message_end();
}

// Particle effect plus ricochet sound
stock Create_TE_GUNSHOT(iOrigin[3])
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_GUNSHOT);
	write_coord(iOrigin[0]);
	write_coord(iOrigin[1]);
	write_coord(iOrigin[2]);
	message_end();
}

// TE_BEAMPOINTS with simplified parameters
stock Create_TE_LIGHTNING(Float:start[3], Float:end[3], life, width, amplitude=0, sprite, reliable=0)
{
	message_begin(reliable ? MSG_ALL : MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_LIGHTNING);
	engfunc(EngFunc_WriteCoord, start[0]);
	engfunc(EngFunc_WriteCoord, start[1]);
	engfunc(EngFunc_WriteCoord, start[2]);
	engfunc(EngFunc_WriteCoord, end[0]);
	engfunc(EngFunc_WriteCoord, end[1]);
	engfunc(EngFunc_WriteCoord, end[2]);
	write_byte(life);		// 0.1's
	write_byte(width);
	write_byte(amplitude);	// 0.01's
	write_short(sprite);
	message_end();
}

stock Create_TE_LINE(Float:fStartOrigin[3], Float:fEndOrigin[3], iLife, iRed, iGreen, iBlue, iReliable = 0)
{
	message_begin(iReliable ? MSG_ALL : MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_LINE);
	engfunc(EngFunc_WriteCoord, fStartOrigin[0]);
	engfunc(EngFunc_WriteCoord, fStartOrigin[1]);
	engfunc(EngFunc_WriteCoord, fStartOrigin[2]);
	engfunc(EngFunc_WriteCoord, fEndOrigin[0]);
	engfunc(EngFunc_WriteCoord, fEndOrigin[1]);
	engfunc(EngFunc_WriteCoord, fEndOrigin[2]);
	write_short(iLife); // 0.1's
	write_byte(iRed);
	write_byte(iGreen);
	write_byte(iBlue);
	message_end();
}

/*
// Flags
//#define TEFIRE_FLAG_ALLFLOAT        1        // All sprites will drift upwards as they animate
//#define TEFIRE_FLAG_SOMEFLOAT       2        // Some of the sprites will drift upwards. (50% chance)
//#define TEFIRE_FLAG_LOOP            4        // If set, sprite plays at 15 fps, otherwise plays at whatever rate stretches the animation over the sprite's duration.
//#define TEFIRE_FLAG_ALPHA           8        // If set, sprite is rendered alpha blended at 50% else, opaque
//#define TEFIRE_FLAG_PLANAR          16       // If set, all fire sprites have same initial Z instead of randomly filling a cube. 
*/
stock Create_TE_FIREFIELD(Float:origin[3], radius, sprite, count, flags, duration)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_FIREFIELD);
	engfunc(EngFunc_WriteCoord, origin[0]);
	engfunc(EngFunc_WriteCoord, origin[1]);
	engfunc(EngFunc_WriteCoord, origin[2]);
	write_short(radius);
	write_short(sprite);
	write_byte(count);
	write_byte(flags);
	write_byte(duration); // duration (in seconds) * 10
	message_end();
}
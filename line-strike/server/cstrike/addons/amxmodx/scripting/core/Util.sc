// --------------------------------------------------
// Util
// --------------------------------------------------

#define out(%1)		server_print(%1)

#define getIdFromName(%1)		get_user_index(%1)

#define getEntOrigin(%1,%2)		pev(%1,pev_origin,%2)
#define getUserOrigin(%1,%2)	get_user_origin(%1,%2)
#define getViewAngle(%1,%2)		pev(%1,pev_v_angle,%2)
#define setViewAngle(%1,%2)		set_pev(%1,pev_v_angle,%2)
#define setPunchAngle(%1,%2)	set_pev(%1,pev_punchangle,%2)

#define getEntClass(%1,%2,%3)	pev(%1,pev_classname,%2,%3)
#define setEntClass(%1,%2)		set_pev(%1,pev_classname,%2)
#define setEntModel(%1,%2)		engfunc(EngFunc_SetModel,%1,%2)
#define setEntSkin(%1,%2)		set_pev(%1,pev_skin,%2)
#define setEntSize(%1,%2,%3)	engfunc(EngFunc_SetSize,%1,%2,%3)
#define setEntOrigin(%1,%2)		engfunc(EngFunc_SetOrigin,%1,%2)
#define getEntOwner(%1)			pev(%1,pev_owner)
#define setEntOwner(%1,%2)		set_pev(%1,pev_owner,%2) //entity,owner
//#define setEntMoveType(%1,%2)	set_pev(%1,pev_movetype,%2)
//#define setEntSolid(%1,%2)		set_pev(%1,pev_solid,%2)

#define isEntValid(%1)			pev_valid(%1)

//!(1 <= attacker <= g_iMaxPlayers) would be faster than is_user_connected.
#define isPlayerEnt(%1) (1 <= %1 <= GMaxPlayers)

#define printCenter(%1,%2)	(client_print(%1,print_center,%2))

/* Random integer between %1 and %2 */
#define rand(%1,%2)		(random_num(%1,%2))
#define randNum(%1,%2)	random_num(%1,%2)
#define randInt(%1,%2)	random_num(%1,%2)
/* Random float between %1 and %2 */
#define randF(%1,%2)	(random_float(%1,%2))
#define randFloat(%1,%2)	random_float(%1,%2)
/* Random value between 0 and %1 (for arrays, etc) */
#define randVal(%1)		(random(%1))
/* Random chance with %1 percents success between 1 and 100 */
#define randomChance(%1)	(random_num(1,100) <= %1)

// THINK
//#define flToNum(%1)	str_to_num(%1)
//#define flToStr(%1)	num_to_str(%1)
//#define numToStr(%1)	

// think --> takeDamageMagic / Normal
#define DMG_MAGIC	(1<<28)//(1<<24)
#define DMG_ARROW	DMG_BULLET//(1<<32)//(1<<23)
// @TODO
//takeMagicDamage
//takeMeleeDamage
#define takeArrowDamage(%1,%2,%3)	(ExecuteHamB(Ham_TakeDamage, %2,%1,%1,%3, DMG_ARROW))
#define takeDamage(%1,%2,%3)	(ExecuteHamB(Ham_TakeDamage, %2,%1,%1,%3, DMG_MAGIC)) //takeDamage(attacker,target,Float:damage)

//ExecuteHamB(Ham_TakeDamage, target, id, id, damage, 4098);

#define emitSound(%1,%2)	(emit_sound(%1, CHAN_VOICE, %2, random_float(0.75,1.0), ATTN_NORM, 0, PITCH_NORM))
#define speakSound(%1,%2)	(client_cmd(%1, "spk %s", %2))

#define soundPlay(%1,%2)	(emit_sound(%1, CHAN_VOICE, %2, random_float(0.75,1.0), ATTN_NORM, 0, PITCH_NORM))
#define soundSpeak(%1,%2)	(client_cmd(%1, "spk %s", %2))

#define getHp(%1)		get_user_health(%1)
#define setHp(%1,%2)	set_user_health(%1,%2)

#define destroyAllMenus(%1)	(show_menu(%1,0,"\n",1))

#define Util_hideRadar(%1)	(client_cmd(%1,"hideradar"))

#define setShotPower(%1,%2)	(cs_set_user_armor(%1,%2,CS_ARMOR_NONE))

#define entityDispose(%1)	entity_set_int(%1, EV_INT_flags, entity_get_int(%1, EV_INT_flags) | FL_KILLME)

stock Util_printOutpostDefense()
{
	out(" ");
	out("   ,---.     |                   |       ,--.      ,---.                    ");
	out("   |   |.   .|--- ,---.,---.,---.|---    |   |,---.|__. ,---.,---.,---.,---.");
	out("   |   ||   ||    |   ||   |`---.|       |   ||---'|    |---'|   |`---.|---'");
	out("   `---'`---'`---'|---'`---'`---'`---'   `--' `---'`    `---'`   '`---'`---'");
	out("                  |                                                         ");
	out(" ");
}
stock Util_printTeamVsTeam()
{
	out(" ");
	out("           --.--                                  --.--               ");
	out("             |  ,---.,---.,-.-.    .    ,,---.      |  ,---.,---.,-.-.");
	out("             |  |---',---|| | |     \  / `---.      |  |---',---|| | |");
	out("             `  `---'`---'` ' '      `'  `---'      `  `---'`---'` ' '");
	out(" ");
}
stock Util_printLastHero()
{
	out(" ");
	out("                  |              |        |   |               ");
	out("                  |    ,---.,---.|---     |---|,---.,---.,---.");
	out("                  |    ,---|`---.|        |   ||---'|    |   |");
	out("                  `---'`---'`---'`---'    `   '`---'`    `---'");
	out(" ");
}
stock Util_printCastleSiege()
{
	out(" ");
	out("              ,---.          |    |             ,---.o               ");
	out("              |    ,---.,---.|--- |    ,---.    `---..,---.,---.,---.");
	out("              |    ,---|`---.|    |    |---'        |||---'|   ||---'");
	out("              `---'`---'`---'`---'`---'`---'    `---'``---'`---|`---'");
	out("                                                           `---'     ");
	out(" ");
}
stock Util_printPvPArena()
{
	out(" ");
	out("                   ,---.      ,---.    ,---.                    ");
	out("                   |---'.    ,|---'    |---|,---.,---.,---.,---.");
	out("                   |     \  / |        |   ||    |---'|   |,---|");
	out("                   `      `'  `        `   '`    `---'`   '`---'");
	out(" ");
}
stock Util_printOlympiad()
{
	out(" ");
	out("                       ,---.|                   o         |");
	out("                       |   ||    ,   .,-.-.,---..,---.,---|");
	out("                       |   ||    |   || | ||   ||,---||   |");
	out("                       `---'`---'`---|` ' '|---'``---'`---'");
	out("                                 `---'     |               ");
	out(" ");
}
stock Util_printCaptureTheFlag()
{
	out(" ");
	out(" ,---.          |                       |    |             ,---.|              ");
	out(" |    ,---.,---.|--- .   .,---.,---.    |--- |---.,---.    |__. |    ,---.,---.");
	out(" |    ,---||   ||    |   ||    |---'    |    |   ||---'    |    |    ,---||   |");
	out(" `---'`---'|---'`---'`---'`    `---'    `---'`   '`---'    `    `---'`---'`---|");
	out("           |                                                              `---'");
	out(" ");
}
stock Util_printRaceWar()
{
	out(" ");
	out("                      ,---.                   . . .          ");
	out("                      |---',---.,---.,---.    | | |,---.,---.");
	out("                      |  \ ,---||    |---'    | | |,---||    ");
	out("                      `   ``---'`---'`---'    `-'-'`---'`    ");
	out(" ");
}
stock Util_printBattleTournament()
{
	out(" ");
	out(",---,     |    |    |        --.--                                        |    ");
	out("|---.,---.|--- |--- |    ,---. |  ,---..   .,---.,---.,---.,-.-.,---.,---.|--- ");
	out("|   |,---||    |    |    |---' |  |   ||   ||    |   |,---|| | ||---'|   ||    ");
	out("`---'`---'`---'`---'`---'`---' `  `---'`---'`    `   '`---'` ' '`---'`   '`---'");
	out(" ");
}

stock Util_printGreekBorder()
{
	server_print(" _____   _____   _____   _____   _____   _____   _____   _____   _____   _____ ");
	server_print("|  _  | |  _  | |  _  | |  _  | |  _  | |  _  | |  _  | |  _  | |  _  | |  _  |");
	server_print("| | |_| | | |_| | | |_| | | |_| | | |_| | | |_| | | |_| | | |_| | | |_| | | |_|");
	server_print("| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |____");
}

stock Util_printMona()
{
	server_print("             ____              ");
	server_print("           o8%8888,            ");   
	server_print("         o88%8888888.          ");  
	server_print("        8'-    -:8888b         ");
	server_print("       8'         8888         ");
	server_print("      d8.-=. ,==-.:888b        ");
	server_print("      >8 `~` :`~' d8888        ");
	server_print("      88         ,88888        ");
	server_print("      88b. `-~  ':88888        ");
	server_print("      888b v=v~ .:88888        ");
	server_print("      88888o--:':::8888        ");
	server_print("      `88888| :::' 8888b       ");
	server_print("      8888^^'       8888b      ");
	server_print("    d888           ,%888b.     ");
	server_print("    d88%            %%%8--'-.  ");
	server_print("   /88:.__ ,       _%-' ---  - ");
	server_print("       '''::===..-'   =  --.  `");
}

public Util_printSection(s[])
{
	const maxlength = 79; /* max console window length */
	new out[maxlength];	
	format(out, maxlength, "-[ %s ]", s);

	new slen = strlen(out);
	
	if(slen >= maxlength){
		server_print("%s", out);
		return;
	}

	for(new i = 0; i < ((maxlength-1) - (slen-1)) - 1; i++)
	{
		format(out, maxlength, "=%s", out);
	}
	
	format(out, maxlength, "=%s", out);
	
	server_print("%s", out);
}

/**
 * Basic functions (overwritten into camel case style).
 */
stock bool:isAlive(id)
{
	if(is_user_alive(id))
		return true;
	return false;
}
stock bool:isConnected(id)
{
	if(is_user_connected(id))
		return true;
	return false;
}
stock bool:isBot(id)
{
	if(is_user_bot(id))
		return true;
	return false;
}
stock bool:haveTeam(id)
{
	id = get_user_team(id);
	if(id == 1 || id == 2)
		return true;
	return false;
}

stock getTeam(id)
{
	return get_user_team(id);
}

stock getTeamName(id)
{
	new teamname[16];
	switch(getTeam(id)){
		case TEAMID_NULL: teamname = TNAME_NULL;
		case TEAMID_EAST: teamname = TNAME_EAST;
		case TEAMID_WEST: teamname = TNAME_WEST;
		case TEAMID_SPEC: teamname = TNAME_SPEC;
	}
	return teamname;
}

// Default client_print works faster
#define sendMessage(%1,%2,%3)	client_print(%1,%2,%3)
/*stock sendMessage(id, chan[], message[], any:...)
{
	new msg[512];
	vformat(msg, 511, message, 4);

	if(equali(chan, "chat"))
		client_print(id, print_chat, msg);
	else if(equali(chan, "center"))
		client_print(id, print_center, msg);
	else if(equali(chan, "console"))
		client_print(id, print_console, msg);
}*/

stock getTotalPlayers(/*CsTeams:iTeam*/)
{
	new total;
	for(new i=1; i<=GMaxPlayers; i++)
		if(isConnected(i) && haveTeam(i) /*cs_get_user_team(i) == team*/)
			total++;
	return total;
}

stock Player_playAnimV(const id, const anim, bool:reliable=true)
{
	if(!id || !anim)
		return;
	
	new MSG_DEST;
	if(reliable) 
		MSG_DEST = MSG_ONE;
	else 
		MSG_DEST = MSG_ONE_UNRELIABLE;
	
	set_pev(id, pev_weaponanim, anim);
	message_begin(MSG_DEST, SVC_WEAPONANIM, .player = id);	//message_begin(MSG_ONE, SVC_WEAPONANIM, {0, 0, 0}, id);
	write_byte(anim); // sequence
	write_byte(0); //write_byte(pev(id, pev_body));
	message_end();
}

stock Player_playAnimP(const id, const seq[])
{
	#define ACT_RANGE_ATTACK1		28
	// Linux extra offsets
	#define extra_offset_player		5
	#define extra_offset_animating	4
	// CBaseAnimating
	#define m_flFrameRate			36
	#define m_flGroundSpeed			37
	#define m_flLastEventCheck		38
	#define m_fSequenceFinished		39
	#define m_fSequenceLoops		40
	// CBaseMonster
	#define m_Activity				73
	#define m_IdealActivity			74
	// CBasePlayer
	#define m_flLastAttackTime		220

	new iAnimDesired, Float: flFrameRate, Float: flGroundSpeed, bool: bLoops;
	if((iAnimDesired = lookup_sequence(id, seq, flFrameRate, bLoops, flGroundSpeed)) == -1)
	{
		iAnimDesired = 0;
	}

	new Float:flGametime = get_gametime();

	set_pev(id, pev_frame, 0.0);
	set_pev(id, pev_framerate, 1.0);
	set_pev(id, pev_animtime, flGametime);
	set_pev(id, pev_sequence, iAnimDesired);

	set_pdata_int(id, m_fSequenceLoops, bLoops, extra_offset_animating);
	set_pdata_int(id, m_fSequenceFinished, 0, extra_offset_animating);

	set_pdata_float(id, m_flFrameRate, flFrameRate, extra_offset_animating);
	set_pdata_float(id, m_flGroundSpeed, flGroundSpeed, extra_offset_animating);
	set_pdata_float(id, m_flLastEventCheck, flGametime , extra_offset_animating);

	set_pdata_int(id, m_Activity, ACT_RANGE_ATTACK1, extra_offset_player);
	set_pdata_int(id, m_IdealActivity, ACT_RANGE_ATTACK1, extra_offset_player);   
	set_pdata_float(id, m_flLastAttackTime, flGametime , extra_offset_player);
}

stock bool:Util_moveEntity(ent, Float:target_origin[3], Float:speed) //const Float:target_origin[3] = {0.0,0.0,0.0}
{
	//if(!pev_valid(ent))
	//	return false;

	new Float:entity_origin[3];
	pev(ent, pev_origin, entity_origin);

	new Float:diff[3];
	diff[0] = target_origin[0] - entity_origin[0];
	diff[1] = target_origin[1] - entity_origin[1];
	diff[2] = target_origin[2] - entity_origin[2];

	new Float:length = floatsqroot(floatpower(diff[0], 2.0) + floatpower(diff[1], 2.0) + floatpower(diff[2], 2.0));

	new Float:velocity[3];
	velocity[0] = diff[0] * (speed / length);
	velocity[1] = diff[1] * (speed / length);
	velocity[2] = diff[2] * (speed / length);

	set_pev(ent, pev_velocity, velocity);

	return true;
}

/* tested */
stock get_velocity_to_origin(ent, Float:fOrigin[3], Float:fSpeed, Float:fVelocity[3])
{
	new Float:fEntOrigin[3];
	entity_get_vector(ent, EV_VEC_origin, fEntOrigin);

	//Velocity = Distance / Time
	
	new Float:fDistance[3];
	fDistance[0] = fOrigin[0] - fEntOrigin[0];
	fDistance[1] = fOrigin[1] - fEntOrigin[1];
	fDistance[2] = fOrigin[2] - fEntOrigin[2];

	new Float:fTime = (vector_distance( fEntOrigin,fOrigin ) / fSpeed);

	fVelocity[0] = fDistance[0] / fTime;
	fVelocity[1] = fDistance[1] / fTime;
	fVelocity[2] = fDistance[2] / fTime;

	return (fVelocity[0] && fVelocity[1] && fVelocity[2]);
}
stock set_velocity_to_origin(ent, Float:fOrigin[3], Float:fSpeed)
{
	new Float:fVelocity[3];
	get_velocity_to_origin(ent, fOrigin, fSpeed, fVelocity);
	entity_set_vector(ent, EV_VEC_velocity, fVelocity);
	return 1;
}
/* */

stock Util_setEntityAngles(ent, Float:origin[3])
{
	static Float:vector[3];
	pev(ent, pev_origin, vector); 
	new i; i = -1;
	while(++i < 3)
		vector[i] = origin[i] - vector[i];
	vector_to_angle(vector, vector);
	set_pev(ent, pev_angles, vector);
}

stock Util_destroyEffect(item)
{
	if(pev_valid(item)){
		new iorigin[3], Float:origin[3];
		getEntOrigin(item, origin);
		origin[2] += 6.0;
		FVecIVec(Float:origin, iorigin);
		Create_TE_SPRITE(0, iorigin, SPR_ITEM_DESTROY, randInt(3,4), randInt(80,200));
	}
}

stock Util_itemPickup(ent)
{
	if(pev_valid(ent)){
		fm_set_rendering(ent, kRenderFxNone, 255,255,255, kRenderTransAdd, 200);
		set_pev(ent, pev_solid, SOLID_NOT);
		set_pev(ent, pev_takedamage, 0.0);
		set_pev(ent, pev_movetype, MOVETYPE_FLY);
		set_pev(ent, pev_deadflag, DEAD_DYING);
		set_pev(ent, pev_gravity, 0.5);
		set_pev(ent, pev_velocity, {0.0,0.0,45.0});
	}
}

stock Util_getAimOriginDist(id, Float:endloc[3], Float:distance, bool:ignorewalls = true) /* by WaterBall */
{
	new Float:Location[3], Float:Angles[3], Float:p_Angle[3];
	new Float:ViewOfs[3], Float:vForward[3];

	entity_get_vector(id, EV_VEC_origin, Location);
	entity_get_vector(id, EV_VEC_view_ofs, ViewOfs);
	xs_vec_add(Location, ViewOfs , Location);
	entity_get_vector(id, EV_VEC_v_angle, Angles);
	entity_get_vector(id, EV_VEC_punchangle, p_Angle);
	xs_vec_add(Angles, p_Angle, Angles);
	engfunc(EngFunc_MakeVectors, Angles);
	global_get(glb_v_forward, vForward);
	xs_vec_mul_scalar(vForward, distance, vForward);
	xs_vec_add(vForward, Location, endloc);

	if(!ignorewalls){
		new trace = create_tr2();
		engfunc(EngFunc_TraceLine, Location, endloc, IGNORE_MONSTERS, id, trace);
		get_tr2(trace, TR_vecEndPos, endloc);
		free_tr2(trace);
	}
}

stock Util_removeWeapons(id)
{
	fm_strip_user_weapons(id);
	set_pdata_int(id, 116, 0);
	// Name	 W. Offset	 L. Offset	 L. Diff	 Type
	// m_fHasPrimary	 116	 121	 +5	 int	 
	// If set to 1, player can't pickup an armoury entity holding a primary weapon, may be used to check for shield pickup
}

stock Util_hideTimer(id)
{
	message_begin(MSG_ONE, gmsgHideWeapon, _, id);
	write_byte(1<<4);
	message_end();
}

public Util_hideHUD(id)
{
	if(id>0) // hide for player
	{
		message_begin(MSG_ONE, gmsgHideWeapon, _, id);
		write_byte((1<<0)|(1<<1)|(1<<3)|(1<<4)|(1<<5)|(1<<6)); // can't use (1<<2) or text disappears
		message_end();
		
		message_begin(MSG_ONE, gmsgCrosshair, _, id);
		write_byte(0);
		message_end();
	}
	else // hide for all
	{
		message_begin(MSG_ALL, gmsgHideWeapon);
		write_byte((1<<0)|(1<<1)|(1<<3)|(1<<4)|(1<<5)|(1<<6)); // can't use (1<<2) or text disappears
		message_end();
		
		message_begin(MSG_ALL, gmsgCrosshair);
		write_byte(0);
		message_end();
	}
}

public Util_endFadeOut()
{
	for(new id=1; id<=GMaxPlayers; id++)
		if(isConnected(id) && !is_user_bot(id))
			Create_ScreenFade(id, (1<<12)*4, (1<<12)*1, FFADE_OUT, 0,0,0,255, true);
}

public Util_endFullBlack()
{
	for(new id=1; id<=GMaxPlayers; id++)
		if(isConnected(id) && !is_user_bot(id))
			Create_ScreenFade(id, 1<<0, 1<<0, 1<<2, 0,0,0,255, true);
}

public spkSound(id, const sound[]) /* to REMOVE */
{
	client_cmd(id, "spk %s", sound);
}

// 
// http://amxmodx.su/forum/viewtopic.php?f=10&t=2177	установка нужной субмодели
/*
// Принцип работы
	new body = pev(ent, pev_body)
	new current = (body / base) % numModels
	set_pev(ent, pev_body, body - (current * base) + value * base)
// Это связано с тем, что в одной модельке хранится множество подмоделек, комбинации всех возможных вариантов.
// Те в даной модельке сначала идут по возрастающей group 0 субмодельки, потом ++ у второй группы group 1 и опять по возрастающей group1 субмодельки и так далее.	
*/
// Устанавливает бодигруппе с именем group[] модель с именем model[]
stock setBodyGroupByName(ent, group[], model[])
{
	new modelFile[64];
	pev(ent, pev_model, modelFile, charsmax(modelFile));
	
	new file = fopen(modelFile, "rt");
	if(!file)
		return -1;
	
	fseek(file, 204, SEEK_SET);
	new numBodyParts;
	fread(file, numBodyParts, BLOCK_INT);
	
	new bodyPartIndex;
	fread(file, bodyPartIndex, BLOCK_INT);
	
	new i, bodyPartName[64], numModels, base, modelIndex;
	new j, modelName[64];
	
	fseek(file, bodyPartIndex, SEEK_SET);
	for(i = 0; i < numBodyParts; i++)
	{
		fseek(file, bodyPartIndex + i*76, SEEK_SET);
		fread_blocks(file, bodyPartName, 64, BLOCK_CHAR);
		
		fread(file, numModels, BLOCK_INT);
		fread(file, base, BLOCK_INT);
		fread(file, modelIndex, BLOCK_INT);
		if(!equal(group, bodyPartName))
			continue;
		
		for(j = 0; j < numModels; j++)
		{
			fseek(file, modelIndex + j*112, SEEK_SET);
			fread_blocks(file, modelName, 64, BLOCK_CHAR);
			
			if(!equal(model, modelName))
				continue;
			
			new body = pev(ent, pev_body);
			new current = (body / base) % numModels;
			
			set_pev(ent, pev_body, body - (current * base) + j * base);
			
			fclose(file);
			return 1;
		}
		
		fclose(file);
		return -1;
	}

	fclose(file);
	return -1;
}
// тоже самое, но по номерам
stock setBodyGroup(ent, group, value)
{
	new model[64];
	pev(ent, pev_model, model, charsmax(model));
	new file = fopen(model, "rt");
	if(!file)
		return -1;
		
	fseek(file, 204, SEEK_SET);
	new numBodyParts;
	fread(file, numBodyParts, BLOCK_INT);
	
	if(group >= numBodyParts)
	{
		fclose(file);
		return -1;
	}
	
	new bodyPartIndex;
	fread(file, bodyPartIndex, BLOCK_INT);
	
	new numModels, base, modelIndex;
	
	fseek(file, bodyPartIndex + group*76 + 64, SEEK_SET);
	fread(file, numModels, BLOCK_INT);
	fread(file, base, BLOCK_INT);
	fread(file, modelIndex, BLOCK_INT);
	
	if(value >= numModels)
	{
		fclose(file);
		return -1;
	}

	new body = pev(ent, pev_body);
	new current = (body / base) % numModels;
	
	set_pev(ent, pev_body, body - (current * base) + value * base);
	
	fclose(file);
	return 1;
}

/*
	MSG_ONE - отправка "сообщения" определенному челу с гарантией доставки.
	MSG_ONE_UNRELIABLE - отправка "сообщения" определенному челу без гарантии доставки. 
	(т.е. если сервер считает, что у него переполнен буффер или клиент не сможет его принять 
	(малый rate и/или cl_updaterate), такая мессага отправляться не будет)
	(Хотя возможно сетевые потери сюда надо тоже включить, там есть механизм 
	подтверждения "сообщений", но как он работает я не изучал, что он подтверждает как часто итп итд)

	Если мы хотим чтобы сообщение точно дошло до игрока (редко отправляемые данные, 
	например деньги или HP выставить в Худе на определнное значение), то стоит использовать MSG_ONE.

	А если например делаем таймер для Кридза (через стандартный раунд таймер КСовский), 
	или отправляем HUD текст каждые 0,05 секунды, то лучше MSG_ONE_UNRELIABLE.
*/
stock Util_playAnimV(const id, const seq, bool:punch=false, bool:reliable=true)
{
	if(punch)
		set_pev(id, pev_punchangle, Float:{-0.5, 0.0, 0.0}); // little punch to v_
	
	new MSG_DEST;
	if(reliable) MSG_DEST = MSG_ONE;
	else MSG_DEST = MSG_ONE_UNRELIABLE;

	set_pev(id, pev_weaponanim, seq);
	message_begin(MSG_DEST, SVC_WEAPONANIM, .player = id);
	write_byte(seq);
	write_byte(0);
	message_end();
}

public showExpHud(playerId, const str[], any:...)
{
	new msg[128];
	vformat(msg, 127, str, 3);
	
	set_hudmessage(200, 100, 0, -1.0, 0.3, 0, 1.50, 1.50, 0.0, 0.0, 4); // channel 4
	ShowSyncHudMsg(playerId, SkillsSyncHud, "%s", msg);
}

public showSkillHud(playerId, const str[], any:...)
{
	new msg[128];
	vformat(msg, 127, str, 3);
	//set_hudmessage(200,100,0, 0.68,0.96 /*0.91*/, 0, 1.0, 1.1, 0.0, 0.0, 4);
	set_hudmessage(200,100,0, 0.86,0.96, 0, 1.0, 1.1, 0.0, 0.0, 4 /*channel*/);
	ShowSyncHudMsg(playerId, SkillsSyncHud, "%s", msg);
}

stock clearDHUDMessages(id, clear=8)
{
	for(new i=0; i < clear; i++)
		show_dhudmessage(id, "");
}

public Util_shortage(id)
{
	//emit_sound(id, CHAN_VOICE, S_SYS_SHORTAGE, 0.5, ATTN_STATIC, 0, PITCH_NORM);
	client_cmd(id, "spk %s", S_SYS_SHORTAGE);
}

stock Util_colorPrint(const id, const input[], any:...)
{
	new count = 1, players[32];

	new msg[191];
	vformat(msg, 190, input, 3);

	replace_all(msg, 190, "/g", "^4");		// green color
	replace_all(msg, 190, "/y", "^1");		// default chat color (yellow)
	replace_all(msg, 190, "/ctr", "^3");	// team color

	if(id) 
		players[0] = id; 
	else 
		get_players(players, count, "ch");
	
	for(new i = 0; i < count; i++)
		if(isConnected(players[i]) && !is_user_bot(i))
		{
			message_begin(MSG_ONE_UNRELIABLE, gmsgSayText, _, players[i]);
			write_byte(players[i]);
			write_string(msg);
			message_end();
		}
}

stock bool:isHullVacant(Float:origin[3], hull = HULL_HUMAN)
{
	engfunc(EngFunc_TraceHull, origin, origin, 0, hull, 0, 0);
	if(!get_tr2(0, TR_StartSolid) && !get_tr2(0, TR_AllSolid) && get_tr2(0, TR_InOpen))
		return true;
	return false;
}

/*stock bool:is_hull_vacant(const Float:origin[3], hull, id)
{
	static tr
	engfunc(EngFunc_TraceHull, origin, origin, 0, hull, id, tr)
	if(!get_tr2(tr, TR_StartSolid) || !get_tr2(tr, TR_AllSolid))
		return true
	return false
}*/


stock fm_find_sphere_class(aroundent, const _lookforclassname[], Float:radius, entlist[], maxents, const Float:_origin[3])
{
	static Float:origin[3];
	
	if(!pev_valid(aroundent))
	{
		origin[0] = _origin[0];
		origin[1] = _origin[1];
		origin[2] = _origin[2];
	}
	else
		pev(aroundent, pev_origin, origin);

	new ent = 0, foundents = 0;
	
	while((ent = engfunc(EngFunc_FindEntityInSphere, aroundent, origin, radius)) != 0)
	{
		if(!pev_valid(ent))
			continue;

		if(foundents >= maxents)
			break;

		static classname[32];
		pev(ent, pev_classname, classname, 31);

		if(equal(classname, _lookforclassname))
			entlist[foundents++] = ent;
	}
	
	return foundents;
}

/**
 * Class names compare functions.
 */
stock bool:isAvanpost(const s[])
{
	if(equali(s, "L2Avanpost@Red") || equali(s, "L2Avanpost@Blu"))
		return true;
	return false;
}

stock bool:isEffect(const s[])
{
	if(equali(s, "arrow") 
	|| equali(s, "stun_shot") 
	|| equali(s, "double_shot") 
	|| equali(s, "lethal_shot") 
	|| equali(s, "hurricane"))
	//|| equali(s, "freezing_flame"))
		return true;
	return false;
}

stock bool:isArrow(s[])
{
	return equali(s, "arrow") || equali(s, "guard_arrow") ? true : false;
}

stock bool:isPlayer(s[])
{
	return equali(s, "player") ? true : false;
}

stock bool:isGuard(const s[])
{
	return equali(s, "L2Guard@East") || equali(s, "L2Guard@West") ? true : false;
}

stock bool:isItem(const s[])
{
	return equali(s, "Item@Etc") || equali(s, "Item@Adena") || equali(s, "Item@Potion") ? true : false;
}

stock bool:isBreakable(const s[])
{
	return equali(s, "func_breakable") ? true : false;
}

stock bool:isGM(id)
{
	return get_user_flags(id) & ADMIN_IMMUNITY ? true : false;
}

// tested ? to remove
/*stock set_user_team(index, team)
{
	set_pdata_int(index, 114, team)
}*/

/* From CHR Engine */
stock entity_set_aim(ent, const Float:origin2[3], bone=0) // Set bone to a positive value to detect a specific bone the function should aim from.
{
	if(!pev_valid(ent))
		return 0; 

	static Float:origin[3];
	origin[0] = origin2[0];
	origin[1] = origin2[1];
	origin[2] = origin2[2];

	static Float:ent_origin[3], Float:angles[3];

	if(bone) 
		engfunc(EngFunc_GetBonePosition, ent, bone, ent_origin, angles);
	else 
		pev(ent,pev_origin,ent_origin);

	origin[0] -= ent_origin[0];
	origin[1] -= ent_origin[1];
	origin[2] -= ent_origin[2];

	static Float:v_length;
	v_length = vector_length(origin);

	static Float:aim_vector[3];
	aim_vector[0] = origin[0] / v_length;
	aim_vector[1] = origin[1] / v_length;
	aim_vector[2] = origin[2] / v_length;

	static Float:new_angles[3];
	vector_to_angle(aim_vector, new_angles);

	new_angles[0] *= -1;

	if(new_angles[1] > 180.0)
		new_angles[1] -= 360;
	if(new_angles[1] < -180.0)
		new_angles[1] += 360;
	if(new_angles[1] == 180.0 || new_angles[1] == -180.0)
		new_angles[1] =- 179.999999; 

	set_pev(ent, pev_angles, new_angles);
	set_pev(ent, pev_fixangle, 1);

	return 1; 
}

stock GetCenter(const iEntity, Float:vecSrc[3])
{
	new Float:vecAbsMax[3];
	new Float:vecAbsMin[3];

	pev(iEntity, pev_absmax, vecAbsMax);
	pev(iEntity, pev_absmin, vecAbsMin);

	xs_vec_add(vecAbsMax, vecAbsMin, vecSrc);
	xs_vec_mul_scalar(vecSrc, 0.5, vecSrc);
}

stock bool:Util_canSee(entity, target)
{
	if(!entity || !target)
		return false;
	
	if(!pev_valid(entity)) /*&& !pev_valid(target))*/
		return false;

	new flags = pev(entity, pev_flags);
	if(flags & EF_NODRAW || flags & FL_NOTARGET)
		return false;

	new Float:lookerOrig[3];
	new Float:targetBaseOrig[3];
	new Float:targetOrig[3];
	new Float:temp[3];

	pev(entity, pev_origin, lookerOrig);
	pev(entity, pev_view_ofs, temp);
	lookerOrig[0] += temp[0];
	lookerOrig[1] += temp[1];
	lookerOrig[2] += temp[2];

	pev(target, pev_origin, targetBaseOrig);
	pev(target, pev_view_ofs, temp);
	targetOrig[0] = targetBaseOrig [0] + temp[0];
	targetOrig[1] = targetBaseOrig [1] + temp[1];
	targetOrig[2] = targetBaseOrig [2] + temp[2];

	engfunc(EngFunc_TraceLine, lookerOrig, targetOrig, 0, entity, 0); // checks the had of seen player
	
	if(get_tr2(0, TraceResult:TR_InOpen) && get_tr2(0, TraceResult:TR_InWater)){
		return false;
	}
	else
	{
		new Float:flFraction;
		get_tr2(0, TraceResult:TR_flFraction, flFraction);
		
		if(flFraction == 1.0 || (get_tr2(0, TraceResult:TR_pHit) == target)){
			return true;
		}
		else
		{
			targetOrig[0] = targetBaseOrig[0];
			targetOrig[1] = targetBaseOrig[1];
			targetOrig[2] = targetBaseOrig[2];
			engfunc(EngFunc_TraceLine, lookerOrig, targetOrig, 0, entity, 0); // checks the body of seen player
			get_tr2(0, TraceResult:TR_flFraction, flFraction);
			
			if(flFraction == 1.0 || (get_tr2(0, TraceResult:TR_pHit) == target)){
				return true;
			}
			else
			{
				targetOrig[0] = targetBaseOrig[0];
				targetOrig[1] = targetBaseOrig[1];
				targetOrig[2] = targetBaseOrig[2] - 17.0;
				
				engfunc(EngFunc_TraceLine, lookerOrig, targetOrig, 0, entity, 0); // checks the legs of seen player
				get_tr2(0, TraceResult:TR_flFraction, flFraction);
				
				if(flFraction == 1.0 || (get_tr2(0, TraceResult:TR_pHit) == target)){
					return true;
				}
			}
		}
	}

	return false;
}

// finds the yaw used if 'client' looks at 'origin'
stock Float:getPlayerYawAtAim(client, Float:origin[3])
{
    new Float:client_origin[3], Float:angles[3];
    engfunc(EngFunc_GetBonePosition, client, 8, client_origin, angles);
    
    new Float:aim_origin[3];
    xs_vec_copy(origin, aim_origin);
    xs_vec_sub(aim_origin, client_origin, aim_origin);
    xs_vec_normalize(aim_origin, aim_origin);
    
    return floatasin(aim_origin[1] / floatsqroot((aim_origin[0]*aim_origin[0]) + (aim_origin[1]*aim_origin[1])), degrees);
}

stock fm_get_user_money(id)
{
	return get_pdata_int(id, OFFSET_CSMONEY, OFFSET_LINUX);
}
stock fm_set_user_money(id, money, flash=0)
{
	set_pdata_int(id, OFFSET_CSMONEY, money, OFFSET_LINUX);
	message_begin(MSG_ONE, gmsgMoney, {0,0,0}, id);
	write_long(money);
	write_byte(flash);
	message_end();
}

#define getAdena(%1)		fm_get_user_money(%1)
#define setAdena(%1,%2)		fm_set_user_money(%1,%2,0)

stock haveSameIp(user1, user2)
{
	new ip1[MAX_IP_ADDR_LENGTH];
	new ip2[MAX_IP_ADDR_LENGTH];
	get_user_ip(user1, ip1, MAX_IP_ADDR_LENGTH-1, 1);
	get_user_ip(user2, ip2, MAX_IP_ADDR_LENGTH-1, 1);

	if(equali(ip1, ip2))
		return true;

	return false;
}
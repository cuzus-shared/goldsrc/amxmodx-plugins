// --------------------------------------------------
// Summon
// --------------------------------------------------

new SummonedUnit[XX] = {0, ...};

// ids
#define NPCID_CORRUPTED_MAN				14038

// classnames
#define PET_CORRUPTED_MAN	"L2Pet@Corrupted Man"
#define PET_CUBICS			"L2Pet@Cubics"
#define PET_STRIDER			"L2Pet@Strider"

#define ANIM_ZOMBIE_WAIT				0
#define ANIM_ZOMBIE_WALK				1
#define ANIM_ZOMBIE_RUN					2
#define ANIM_ZOMBIE_ATK					3
#define ANIM_ZOMBIE_ATKWAIT				4
#define ANIM_ZOMBIE_DEATH				5
#define ANIM_ZOMBIE_DEATHWAIT			6
#define ANIM_ZOMBIE_SPWAIT				7

#define SUMMON_CORRUPTED_RUN_SPEED		6.5 //2.5
#define SUMMON_CORRUPTED_IDLE_SPEED		1.0 //1.2

#define SUMMON_MIN_DISTANCE				150.0
#define SUMMON_MAX_DISTANCE				210.0

#define SUMMON_CORRUPTED_HEALTH			186	//344.0
#define SUMMON_TRANSFER_PAIN_DMG		10	// 10% (l2off)										//UNUSED
#define SUMMON_CORRUPTED_MAN_EXP		90	// 90% take exp from player earned exp (l2off)		//UNUSED

public bool:isPet(this[])
{
	if(equali(this, PET_CORRUPTED_MAN) 
	|| equali(this, PET_CUBICS) 
	/*|| equali(this, PET_STRIDER)*/)
		return true;
	return false;
}

public getPetId(this[])
{
	if(equali(this, PET_CORRUPTED_MAN))
		return NPCID_CORRUPTED_MAN;
	return -1;
}

public bool:haveSummon(id)
{
	return (SummonedUnit[id] > 0 && isEntValid(SummonedUnit[id])) ? true : false;
}

public removeSummonedUnit(id)
{
	if(haveSummon(id)){
		entityDispose(SummonedUnit[id]);
		SummonedUnit[id] = 0;
		showSkillHud(id, "Summon disappeared");
	}
	else
		Log(DEBUG, "Summon", "removeSummonedUnit(%s): Invalid entity ID=%d or not exists", getName(id), SummonedUnit[id]);
}

public getSummonedUnit(id)
{
	return SummonedUnit[id];
}

public Summon_killed(id)
{
	if(SummonedUnit[id])
	{
		if(task_exists(SummonedUnit[id] + TASK_SKILLS_SUMMON_IDLE)) // remove idle sound task
			remove_task(SummonedUnit[id] + TASK_SKILLS_SUMMON_IDLE);

		set_pev(SummonedUnit[id], pev_health, 0.0);
		set_pev(SummonedUnit[id], pev_solid, SOLID_NOT);
		set_pev(SummonedUnit[id], pev_takedamage, 0.0);

		Npc_playAnim(SummonedUnit[id], ANIM_ZOMBIE_DEATH, 1.0 /* corrupted_die_speed */);

		emit_sound(SummonedUnit[id], CHAN_VOICE, S_CORRUPTED_MAN[1], VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // zombie_Death.wav
		set_task(1.8, "Summon_fallTask", SummonedUnit[id]);
		set_task(randFloat(4.0, 6.0), "Summon_removeTask", id+TASK_SKILLS_CORRUPTED_MAN_UNSUM);
		
		//client_print(id, print_chat, "Summon died");
		showSkillHud(id, "Summon died");
	}
	
	SummonedUnit[id] = 0;
}

public Summon_fallTask(ent)
{
	emit_sound(ent, CHAN_VOICE, S_CORRUPTED_MAN[5], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
}

public Summon_removeTask(id)
{
	removeSummonedUnit(id-TASK_SKILLS_CORRUPTED_MAN_UNSUM);
}

public Summon_ReanimatedMan(id)
{
	// @totally rework TODO
	
	SummonedUnit[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));

	set_pev(SummonedUnit[id], pev_classname, "L2Pet@CorruptedMan");
	engfunc(EngFunc_SetModel, SummonedUnit[id], M_CORRUPTED_MAN);
	engfunc(EngFunc_SetSize, SummonedUnit[id], Float:{-16.0, -16.0, 0.0}, Float:{16.0, 16.0, 64.0});

	// TODO: spawn behind or in front ?L2
	
	new Float:origin[3];
	pev(id, pev_origin, origin);
	origin[0] += randNum(40,80);
	origin[1] += randNum(40,80);
	origin[2] += 6.0;
	engfunc(EngFunc_SetOrigin, SummonedUnit[id], origin);

	set_pev(SummonedUnit[id], pev_solid, SOLID_BBOX);
	set_pev(SummonedUnit[id], pev_health, float(SUMMON_CORRUPTED_HEALTH));
	set_pev(SummonedUnit[id], pev_takedamage, DAMAGE_YES);
	set_pev(SummonedUnit[id], pev_movetype, MOVETYPE_PUSHSTEP); //MOVETYPE_FLY
	set_pev(SummonedUnit[id], pev_gravity, 1.0);
	set_pev(SummonedUnit[id], pev_owner, id);

	set_pev(SummonedUnit[id], pev_animtime, 2.0);
	
	Npc_playAnim(SummonedUnit[id], ANIM_ZOMBIE_SPWAIT, 1.0);
	
	set_pev(SummonedUnit[id], pev_nextthink, 1.0);
	
	emit_sound(SummonedUnit[id], CHAN_VOICE, S_SUMMON_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // shot sound
	
	engfunc(EngFunc_DropToFloor, SummonedUnit[id]);
	dllfunc(DLLFunc_Think, SummonedUnit[id]);
	
	emit_sound(SummonedUnit[id], CHAN_VOICE, S_CORRUPTED_MAN[0], VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // npc breath sound
	
	set_task(1.0, "Summon_idle", SummonedUnit[id]+TASK_SKILLS_SUMMON_IDLE);
	
	//todo AvoidStuck spawn @Npc
}

public Summon_idle(ent) //taskid
{
	ent -= TASK_SKILLS_SUMMON_IDLE;
	
	emit_sound(ent, CHAN_VOICE, S_CORRUPTED_MAN[8], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	
	if(randNum(1,3) == 1)
		Npc_playAnim(ent, ANIM_ZOMBIE_SPWAIT, 1.2);
	
	set_task(randFloat(3.0,15.0), "Summon_idle", ent+TASK_SKILLS_SUMMON_IDLE);
}

public Summon_takeDamage(summon, attacker, npcid)
{
	//switch(npcid) // npcid @ wait todo
	// play glitch anim?

	if(randNum(1,3) == 2)
		emit_sound(summon, CHAN_VOICE, S_CORRUPTED_MAN[randNum(2,4)], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
}

public CorruptedMan_think(ent) // Core's think
{
	if(!isEntValid(ent))
        return;
	else if(pev(ent, pev_health) <= 0.0)
	{
		Log(WARN, "Summon", "CorruptedMan_think(ID=%d) is dead but not removed", ent);
		return;
	}
	else
	{
		new owner = pev(ent, pev_owner);
		if(owner && isAlive(owner))
		{
			static Float:origin_player[3];
			static Float:origin_npc[3];
			pev(ent, pev_origin, origin_npc);
			pev(owner, pev_origin, origin_player);
			
			//get_offset_origin_body(owner, Float:{50.0,0.0,0.0}, origin_player);

			if(get_distance_f(origin_player, origin_npc) > SUMMON_MAX_DISTANCE)
			{
				set_pev(ent, pev_origin, origin_player);
			}
			else if(get_distance_f(origin_player, origin_npc) > SUMMON_MIN_DISTANCE)
			{
				new Float:velocity[3];
				get_speed_vector(origin_npc, origin_player, 250.0, velocity);
				set_pev(ent, pev_velocity, velocity);
				if(pev(ent, pev_sequence) != ANIM_ZOMBIE_RUN || pev(ent, pev_framerate) != SUMMON_CORRUPTED_RUN_SPEED)
				{
					set_pev(ent, pev_frame, 1);
					set_pev(ent, pev_sequence, ANIM_ZOMBIE_RUN);
					set_pev(ent, pev_gaitsequence, ANIM_ZOMBIE_RUN);
					set_pev(ent, pev_framerate, SUMMON_CORRUPTED_RUN_SPEED);
				}
			}
			else if(get_distance_f(origin_player, origin_npc) < SUMMON_MIN_DISTANCE - 5.0)
			{
				if(pev(ent, pev_sequence) != ANIM_ZOMBIE_WAIT || pev(ent, pev_framerate) != SUMMON_CORRUPTED_IDLE_SPEED)
				{
					set_pev(ent, pev_frame, 1);
					set_pev(ent, pev_sequence, ANIM_ZOMBIE_WAIT);
					set_pev(ent, pev_gaitsequence, ANIM_ZOMBIE_WAIT);
					set_pev(ent, pev_framerate, SUMMON_CORRUPTED_IDLE_SPEED);
				}
				set_pev(ent, pev_velocity, Float:{0.0,0.0,0.0});
			}

			Npc_setAim(ent, owner);

			set_pev(ent, pev_nextthink, get_gametime() + 0.01); //0.1	// makes the zombie think every 1/100 second
		}
	}
}

public bool:Summon_cubics(id)
{
	SummonedUnit[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "env_sprite"));
	if(SummonedUnit[id])
	{
		set_pev(SummonedUnit[id], pev_classname, PET_CUBICS);
		engfunc(EngFunc_SetModel, SummonedUnit[id], SPR_SUMMON_CUBICS);
		engfunc(EngFunc_SetSize, SummonedUnit[id], Float:{0.0,0.0,0.0}, Float:{0.0,0.0,0.0});
		set_pev(SummonedUnit[id], pev_scale, 0.4);
		set_pev(SummonedUnit[id], pev_solid, SOLID_NOT);
		set_pev(SummonedUnit[id], pev_movetype, MOVETYPE_FOLLOW);
		set_pev(SummonedUnit[id], pev_aiment, id);
		set_pev(SummonedUnit[id], pev_owner, id);

		set_pev(SummonedUnit[id], pev_rendermode, 5); // 5 - Additive
		set_pev(SummonedUnit[id], pev_renderfx, kRenderFxNone);
		set_pev(SummonedUnit[id], pev_renderamt, 255.0);

		set_pev(SummonedUnit[id], pev_animtime, 1.0);
		set_pev(SummonedUnit[id], pev_frame, 0);
		set_pev(SummonedUnit[id], pev_framerate, 15.0);
		set_pev(SummonedUnit[id], pev_spawnflags, SF_SPRITE_STARTON);
		dllfunc(DLLFunc_Spawn, SummonedUnit[id]);
		
		return true;
	}
	else
		Log(WARN, "Summon", "cubics(): Can't create ent for player %s", getName(id));
	
	return false;
}

/**
 * Summon Strider.
 *
enum (+= 1){
	ANIM_STRIDER_IDLE = 0,
	ANIM_STRIDER_RUN,
	ANIM_STRIDER_SWIM,
	ANIM_STRIDER_ATK1,
	ANIM_STRIDER_ATK2,
	ANIM_STRIDER_ATKWAIT,
	ANIM_STRIDER_DEATH,
	ANIM_STRIDER_DEATHWAIT,
	ANIM_PICITEM
};
public Strider_think(ent)
{
	if(!isEntValid(ent))
        return;
	else if(pev(ent, pev_health) <= 0.0)
	{
		// TODO remove strider/deattach
		Log(WARN, "Summon", "Strider_think(ID=%d) is dead but not removed", ent);
		return;
	}
	else
	{
		new id = pev(ent, pev_owner);
		if(!isAlive(id))
			return;
		
		new btn = entity_get_int(id, EV_INT_button);

		if((entity_get_int(id, EV_INT_flags) & FL_ONGROUND) && 
		(btn & IN_FORWARD || btn & IN_BACK
		|| btn & IN_LEFT || btn & IN_RIGHT
		|| btn & IN_MOVELEFT || btn & IN_MOVERIGHT
		|| btn & IN_RUN))
		{
			Npc_playAnim(ent, ANIM_STRIDER_RUN, 1.0);
		}
		else if(btn & IN_JUMP)
		{
			Npc_playAnim(ent, ANIM_STRIDER_SWIM, 1.0);
		}
		else if(btn & IN_ATTACK || btn & IN_ATTACK2)
		{
			Npc_playAnim(ent, (random_num(1,2)==1 ? ANIM_STRIDER_ATK1 : ANIM_STRIDER_ATK2), 1.0);
		}

		set_pev(ent, pev_nextthink, get_gametime() + 0.01); //0.1	// makes the zombie think every 1/100 second
	}
}

// TODO: on takeDmg() ==> return damage to owner
public bool:Summon_strider(id)
{
	SummonedUnit[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(SummonedUnit[id])
	{
		set_pev(SummonedUnit[id], pev_classname, PET_STRIDER);
		engfunc(EngFunc_SetModel, SummonedUnit[id], M_STRIDER);
		engfunc(EngFunc_SetSize, SummonedUnit[id], Float:{-36.0,-36.0,0.0}, Float:{36.0,36.0,72.0});
		//set_pev(SummonedUnit[id], pev_scale, 1.0);
		set_pev(SummonedUnit[id], pev_solid, SOLID_BBOX);
		set_pev(SummonedUnit[id], pev_movetype, MOVETYPE_FOLLOW);
		set_pev(SummonedUnit[id], pev_health, INFINITE); // <Const>
		set_pev(SummonedUnit[id], pev_takedamage, DAMAGE_YES);
		set_pev(SummonedUnit[id], pev_aiment, id);
		set_pev(SummonedUnit[id], pev_owner, id);

		//new Float:params[3];
		//pev(id, pev_origin, params);
		//engfunc(EngFunc_SetOrigin, SummonedUnit[id], params);
		//pev(id, pev_angles, params);
		//set_pev(SummonedUnit[id]), pev_angles, params);

		dllfunc(DLLFunc_Spawn, SummonedUnit[id]);

		Npc_playAnim(SummonedUnit[id], ANIM_STRIDER_IDLE, 1.0);

		//emit_sound(SummonedUnit[id], CHAN_VOICE, S_SUMMON_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // shot sound
		
		//engfunc(EngFunc_DropToFloor, SummonedUnit[id]);
		
		set_pev(SummonedUnit[id], pev_nextthink, 0.1);
		dllfunc(DLLFunc_Think, SummonedUnit[id]);
		
		fm_set_user_rendering(id, kRenderFxGlowShell, 0,0,0, kRenderTransAlpha, 0); // 100% transparent

		return true;
	}
	else
		Log(WARN, "Summon", "strider(): Can't create ent for player %s", getName(id));
	
	return false;
}
*/
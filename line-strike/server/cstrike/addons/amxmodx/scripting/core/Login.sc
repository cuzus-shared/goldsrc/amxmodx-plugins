// --------------------------------------------------
// Login System
// --------------------------------------------------

new LoginStatus = false;
static Array:BannedIPs;
new Trie:TFloodList;
new GFloodControl[XX] = {0, ...};

public Login_init()
{
	BannedIPs = ArrayCreate(16);
	readBannedList();

	TFloodList = TrieCreate();
}

public Login_pluginEnd()
{
	ArrayDestroy(BannedIPs);
	TrieDestroy(TFloodList);
}

public Login_isOpened()
{
	if(LoginStatus)
		return true;
	return false;
}
public Login_setOpened(bool:param)
{
	Log(INFO, "Login", "Login status: %s", param ? "Opened" : "Closed");
	LoginStatus = param;
}

public bool:readBannedList()
{
	// @WAIT
	//	TODO: //ban //unban + write to file (SAY2)
	// 
	if(file_exists(CFG_FILE_BLACKLIST))
	{
		new _file = fopen(CFG_FILE_BLACKLIST, "rt");
		if(!_file){
			Log(ERROR, "Login", "Open file error: %s", CFG_FILE_BLACKLIST);
			return false;
		}
		else
		{
			new line[17];
			while(!feof(_file))
			{
				fgets(_file, line, 16);
				if(line[0] == '#' || !line[0] || line[0] == ' ' || line[0] == 10)
				{
					continue; // skip comments, empty lines, etc
				}
				else{
					trim(line);
					ArrayPushString(BannedIPs, line);
				}
			}
			Log(INFO, "Login", "Loaded %d banned IP(s)", ArraySize(BannedIPs));
		}
		fclose(_file);
	}
	else{
		Log(ERROR, "LastHero", "Failed to load banned IP's file.");
		Log(ERROR, "LastHero", "%s is missing!", CFG_FILE_BLACKLIST);
		return false;
	}

	return true;
}

public bool:Login_isBannedIp(id)
{
	new _ip[MAX_IP_ADDR_LENGTH], buf[MAX_IP_ADDR_LENGTH];
	get_user_ip(id, _ip, MAX_IP_ADDR_LENGTH-1, 1);
	trim(_ip);

	for(new i=0; i<ArraySize(BannedIPs); i++)
	{
		ArrayGetString(BannedIPs, i, buf, MAX_IP_ADDR_LENGTH-1);
		if(equali(_ip, buf)){
			return true;
		}
	}
	return false;
}

public Login_addFlooder(id)
{
	new _ip[MAX_IP_ADDR_LENGTH];
	get_user_ip(id, _ip, MAX_IP_ADDR_LENGTH-1, 1);
	//native TrieSetCell(Trie:handle, const key[], any:value);
	trim(_ip);
	TrieSetCell(TFloodList, _ip, get_gametime() + 300.0 /* wait time */);
	Log(WARN, "Login", "Potential flood from %s!", _ip);
}

public bool:Login_clientConnect(id)
{
	// @l2
	//	Your usage term has expired.
	//	There are too many users on the server. Please try again later.
	//		_log.warn("Wrong checksum from client: " + toString());
	//		_log.warn("Drop connection from IP " + ip + " because of BruteForce!");
	
	if(Login_isBannedIp(id))
	{
		server_cmd("kick #%d ^"This account has been permanently banned.^"", get_user_userid(id));
	}
	else if(Login_isOpened())
	{
		new _ip[MAX_IP_ADDR_LENGTH];
		get_user_ip(id, _ip, MAX_IP_ADDR_LENGTH-1, 1);
		trim(_ip);

		if(TrieKeyExists(TFloodList, _ip))
		{
			//http://amx-x.ru/viewtopic.php?f=8&t=33577
			//https://forums.alliedmods.net/showthread.php?t=169437
			
			new Float:jointime = 0.0;
			TrieGetCell(TFloodList, _ip, jointime);

			if(jointime > get_gametime())
			{
				//server_cmd("kick #%d ^"This account has been suspended for %d seconds.^"", get_user_userid(id), floatround(jointime - get_gametime(), floatround_ceil));
				server_cmd("kick #%d ^"You're kicked for flood. Wait %d seconds.^"", get_user_userid(id), floatround(jointime - get_gametime(), floatround_ceil));
				return false;
			}else{
				TrieDeleteKey(TFloodList, _ip); 
			}
		}
	}
	else
		server_cmd("kick #%d ^"System error, please log in again later.^"", get_user_userid(id));

	return true;
}

public Login_clientDisconnect(id)
{
	switch(SelectedTeam[id]){
		case 1: EastPlayers--;
		case 2: WestPlayers--;
	}

	Login_checkNotEnough();

	SelectedTeam[id] = 0;
	GFloodControl[id] = 0;

	//Log(DEBUG, "Login", "%s disconnected (East: %d | West: %d)", getName(id), EastPlayers, WestPlayers);
	Log(DEBUG, "Login", "(%d | %d) %s disconnected ", EastPlayers, WestPlayers, getName(id));
}

public Login_clientPutInServer(id)
{
	set_task(randFloat(0.2, 0.7), "Login_handle", id+TASK_FORCE_AUTO_JOIN);
}

public Login_getRegisteredPlayers()
{
	return EastPlayers + WestPlayers;
}

public bool:Login_checkWarmupStart()
{
	if(!warmupConfirmed())
		if(Login_getRegisteredPlayers() >= Config[MIN_PLAYERS_TO_START])
		{
			if(!WarmupStarted)
				Warmup_start(); // Removes warmup idle and run warmup countdown
			return true;
		}

	return false;
}

/**
 * Returns true if not enough players to start.
 */
public bool:Login_checkNotEnough()
{
	if(!warmupConfirmed())
		if(Login_getRegisteredPlayers() < Config[MIN_PLAYERS_TO_START])
		{
			Warmup_abort("Not enough players"); // Removes warmup countdown and run warmup idle
			return true;
		}

	return false;
}

public Login_handle(id)
{
	id -= TASK_FORCE_AUTO_JOIN;

	if(isConnected(id))
	{
		if(!isBot(id))
		{
			Util_hideHUD(id);
			if(Config[ENABLE_JOIN_CAM])
				Camera_setJoinCam(id);
		}

		if(EndOfMap){ // for joins after map end: GameMode_end()
			Create_ScreenFade(id, 1<<0, 1<<0, 1<<2, 0,0,0,255, true);
		}
		else
		{
			if(isBot(id)) // prevent bots join after event start
			{
				engclient_cmd(id, "jointeam", "6");
				if(isAlive(id)){
					fm_set_user_rendering(id, kRenderFxGlowShell, 0,0,0, kRenderTransAlpha, 0); // 100% transparent
					user_silentkill(id);
				}
				cs_set_user_team(id, CS_TEAM_SPECTATOR);
				
				if(!EventStarted)
					set_task(randFloat(0.5, 46.5), "autoSelectTeam", id+TASK_BOT_SELECT_TEAM);
			}
			else
			{
				Create_ScreenFade(id, 4000, 600, FFADE_IN, 0,0,0,255, true);
				set_task(0.6, "joinWelcomeHud", id+TASK_JOIN_WELCOME_HUD);
				
				if(isTeamGameMode())
				{
					showFactionMenu(id);
				}
				else if(isDmGameMode())
				{
					if(!EventStarted){
						showEventMenu(id);
					}
					else{ // event in progress
						cs_set_user_team(id, CS_TEAM_SPECTATOR);
						Camera_unsetJoinCam(id); // TEST
						client_print(id, print_center, "You can't join this game, match is in progress.");
					}
				}
			}
		}
	}
}

public autoSelectTeam(id) // Auto-Select team for bots
{
	id -= TASK_BOT_SELECT_TEAM;

	switch(GameMode)
	{
		case MODE_OUTPOST_DEFENSE, MODE_CASTLE_SIEGE, MODE_TEAM_VS_TEAM:
		{
			if(EastPlayers > WestPlayers)
			{
				SelectedTeam[id] = 2;
				WestPlayers++;
			}
			else if(WestPlayers > EastPlayers)
			{
				SelectedTeam[id] = 1;
				EastPlayers++;
			}
			else
			{
				SelectedTeam[id] = random_num(1,2);
				if(SelectedTeam[id] == 1)
					EastPlayers++;
				else
					WestPlayers++;
			}
			
			updateFactionMenu(); // update faction-menu for real players
		}
		case MODE_LAST_HERO:
		{
			SelectedTeam[id] = 1;
			EastPlayers++;
			
			updateEventMenu(); // update event-menu for real players
		}
	}

	Login_checkWarmupStart();
}

public joinWelcomeHud(id)
{
	id -= TASK_JOIN_WELCOME_HUD;
	set_dhudmessage(random(255),random(255),random(255), -1.0, 0.16, 1, 5.0, 5.0, 0.5, 1.2, true);	
	show_dhudmessage(id, "Welcome to the World of Line-Strike!");
	remove_task(id+TASK_JOIN_WELCOME_HUD);
}

public showFactionMenu(id)
{
	new menu = menu_create("\yCurrently participated: ", "handleFactionMenu");
	new option[25];
	
	if(SelectedTeam[id] == 1)
		formatex(option, 24, "\y[%d players] \wEast", EastPlayers);
	else
		formatex(option, 24, "\d[%d players] \dEast", EastPlayers);
	
	menu_additem(menu, option, "1");	
	
	if(SelectedTeam[id] == 2)
		formatex(option, 24, "\y[%d players] \wWest", WestPlayers);
	else
		formatex(option, 24, "\d[%d players] \dWest", WestPlayers);
	
	menu_additem(menu, option, "2");

	menu_setprop(menu, MPROP_PERPAGE, 0);
	menu_setprop(menu, MPROP_EXIT, MEXIT_NEVER);
	menu_display(id, menu, 0);

	return PLUGIN_CONTINUE;
}

public handleFactionMenu(id, menu, item)
{
	if(item == MENU_EXIT){
		//menu_destroy(menu);
		return PLUGIN_HANDLED;
	}

	new name[64], selected[3], access, callback;
	menu_item_getinfo(menu, item, access, selected, charsmax(selected), name, charsmax(name), callback);

	switch(str_to_num(selected))
	{
		case 1:
		{
			if(!checkFloods(id))
			{
				if(EastPlayers > WestPlayers)
				{
					client_print(id, print_center, "Too many players in East Team!");
					client_cmd(id, "spk %s", SYS_IMPOSSIBLE);
				}
				else
				{
					if(SelectedTeam[id] == 2) // reselect (CT ==> T)
					{
						WestPlayers--;
						EastPlayers++;
						SelectedTeam[id] = 1;
					}
					else if(SelectedTeam[id] == 1){
						client_print(0, print_center, "This team already selected.");
					}
					else // select T
					{
						EastPlayers++;
						SelectedTeam[id] = 1;
					}
					client_cmd(id, "spk %s", S_CLICK);
				}
				
				menu_destroy(menu);
				if(!warmupConfirmed())
					updateFactionMenu();
			}
			else
				return PLUGIN_HANDLED;
		}
		case 2:
		{
			if(!checkFloods(id))
			{
				if(WestPlayers > EastPlayers)
				{
					client_print(id, print_center, "Too many players in West Team!");
					client_cmd(id, "spk %s", SYS_IMPOSSIBLE);
				}
				else
				{
					if(SelectedTeam[id] == 1) // reselect (T ==> CT)
					{
						EastPlayers--;
						WestPlayers++;
						SelectedTeam[id] = 2;
					}
					else if(SelectedTeam[id] == 2){
						client_print(0, print_center, "This team already selected.");
					}
					else // select CT
					{
						WestPlayers++;
						SelectedTeam[id] = 2;
					}
					client_cmd(id, "spk %s", S_CLICK);
				}
				
				menu_destroy(menu);
				if(!warmupConfirmed())
					updateFactionMenu();
			}
			else
				return PLUGIN_HANDLED;
		}
	}
	
	// For players which joined after Warmup
	if(warmupConfirmed())
		Login_joinAfterWarmup(id);
	
	Login_checkWarmupStart();

	return PLUGIN_HANDLED;
}

public updateFactionMenu()
{
	for(new id=1; id<=GMaxPlayers; id++)
		if(isConnected(id) && !haveTeam(id) && !isBot(id))
			showFactionMenu(id);
}

public showEventMenu(id)
{
	new menu = menu_create("\yCurrently participated: ", "handleEventMenu");
	new option[64], option2[64];

	if(SelectedTeam[id] == 1) // if player joined
	{
		formatex(option, 63, "\w[X] \y[%d players]^t^t\rClick cancels", EastPlayers); // ''
		formatex(option2, 63, "\wChange class \y[%s Lv %d]", getClassName(id), getLevel(id));
		
		menu_additem(menu, option, "1");
		//menu_addblank(menu, 0);
		menu_additem(menu, option2, "2");
	}
	else{
		formatex(option, 63, "\d[^t] [%d players]^t^t\rClick for join", EastPlayers);
		menu_additem(menu, option, "1");
	}

	menu_setprop(menu, MPROP_PERPAGE, 0);
	menu_setprop(menu, MPROP_EXIT, MEXIT_NEVER);
	menu_display(id, menu, 0);

	return PLUGIN_CONTINUE;
}

public handleEventMenu(id, menu, item)
{
	if(item == MENU_EXIT){
		//menu_destroy(menu);
		return PLUGIN_HANDLED;
	}

	new name[64], selected[3], access, callback;
	menu_item_getinfo(menu, item, access, selected, charsmax(selected), name, charsmax(name), callback);

	switch(str_to_num(selected))
	{
		case 1: 
		{
			if(!checkFloods(id))
			{
				if(SelectedTeam[id] == 1) // already joined ==> cancel participation
				{
					EastPlayers--;
					SelectedTeam[id] = -1;
					client_cmd(id, "spk %s", S_CLICK);
					client_print(0, print_center, "Participation is canceled.");
				}
				else{ // join
					EastPlayers++;
					SelectedTeam[id] = 1;
					client_cmd(id, "spk %s", S_CLICK);
					client_print(0, print_center, "You participated to the event.");
					
					//menu_destroy(menu);
					//classMenuShow(id);
				}

				menu_destroy(menu);
				if(!warmupConfirmed())
					updateEventMenu();
			}
			else
				return PLUGIN_HANDLED;
		}
		case 2:
		{
			menu_destroy(menu);
			
			if(!warmupConfirmed()) // prevent use after warmup
				if(SelectedTeam[id] == 1){ // if joined ==> can change class
					classMenuShow(id);
				}
		}
	}

	Login_checkWarmupStart();

	return PLUGIN_HANDLED;
}

public updateEventMenu()
{
	for(new id=1; id<=GMaxPlayers; id++)
		if(isConnected(id) /*&& !haveTeam(id)*/ && !isBot(id))
			if(!isMenuOpened[id][CLASS_MENU])
				showEventMenu(id);
}

/**
 * Checks player keystrokes and punish for flood.
 */
public bool:checkFloods(id)
{
	GFloodControl[id]++;
	new val = GFloodControl[id];
	
	if(val >= 6 && val < 9){ //>=10 <13
		client_print(id, print_center, "You can't change team more.");
		return true;
	}
	else if(val >= 9 && val < 12){ //>=13 <16
		client_print(id, print_center, "Stop flood or you will be kicked!");
		return true;
	}
	else if(val >= 12){ //>=16
		Login_addFlooder(id);
		server_cmd("kick #%d ^"You kicked for flood.^"", get_user_userid(id));
		return true;
	}
	return false;
}

/**
 * Change all players team after warmup ends and run spawn tasks.
 */
public Login_warmupEnds()
{
	Log(DEBUG, "Login", "warmupEnds(): EastPlayers=%d, WestPlayers=%d", EastPlayers, WestPlayers);

	new Float:join_after = 0.0, Float:spawn_after = 0.0;

	for(new id=1; id<=GMaxPlayers; id++){
		if(isConnected(id) /*&& !haveTeam(id)*/ && SelectedTeam[id] != 0)
		{
			join_after += 0.4;
			set_task(join_after, "Login_changeTeamTask", id+TASK_LOGIN_CHANGE_TEAM);
			spawn_after = 1.0 + join_after;
			new params[2]; params[0] = id; params[1] = floatround(spawn_after)
			set_task(0.1, "Login_changeTeamCountdown", id+TASK_LOGIN_CHANGE_TEAM_COUNTDN, params, 2);
		}
		else{
			Log(WARN, "Login", "warmupEnds(): Player '%s' try to join w/o selected team", getName(id));
			if(isAlive(id)) user_silentkill(id);
		}
	}

	if(GameMode != MODE_LAST_HERO)
		client_print(0, print_center, "Warmup is ended. Prepare to fight!");
}

public Login_joinAfterWarmup(id)
{
	if(isConnected(id) && SelectedTeam[id] != 0)
	{
		new Float:join_after = randFloat(0.4, 4.1);
		set_task(join_after, "Login_changeTeamTask", id+TASK_LOGIN_CHANGE_TEAM);
		new Float:spawn_after = 1.0 + join_after;
		new params[2]; params[0] = id; params[1] = floatround(spawn_after)
		set_task(0.1, "Login_changeTeamCountdown", id+TASK_LOGIN_CHANGE_TEAM_COUNTDN, params, 2);		
	}
	else{
		Log(WARN, "Login", "warmupEnds(): Player '%s' try to join w/o selected team", getName(id));
		if(isAlive(id)) user_silentkill(id);
	}
}

public Login_changeTeamCountdown(params[2], taskid) //params[0] = id; params[1] = spawn_after
{
	client_print(params[0], print_center, "Your turn in %d sec", params[1]);
	params[1]--;
	if(params[1] > 0)
		set_task(1.0, "Login_changeTeamCountdown", taskid, params, 2);
	else
		client_print(params[0], print_center, " ");
}

public Login_changeTeamTask(id)
{
	id -= TASK_LOGIN_CHANGE_TEAM;
	Login_changeTeam(id);
}

public Login_changeTeam(id)
{
	Log(DEBUG, "Login", "changeTeam(): %d|%s to %d", id, getName(id), SelectedTeam[id]);

	switch(SelectedTeam[id])
	{
		case 1: { // T
			if(isBot(id)){
				fm_set_user_team(id, 1);
				setRaceByData(id, 1);
			}else{
				engclient_cmd(id, "jointeam", "1");
				engclient_cmd(id, "joinclass", "5");
				setRaceByData(id, 1);
			}
		}
		case 2: { // CT
			if(isBot(id)){
				fm_set_user_team(id, 2);
				setRaceByData(id, 2);
			}else{
				engclient_cmd(id, "jointeam", "2");
				engclient_cmd(id, "joinclass", "5");
				setRaceByData(id, 2);
			}
		}
		default: { // move to spec (cant join without retry?)
			fm_set_user_team(id, 3);
		}
	}

	if(!isBot(id))
		destroyAllMenus(id);

	if(isConnected(id) && SelectedTeam[id] != 0) // begin spawn
		set_task(1.0, "Login_spawnTask", id+TASK_LOGIN_TASK_SPAWN);
}

public Login_spawnTask(id)
{
	id -= TASK_LOGIN_TASK_SPAWN;
	if(isPlayerEnt(id)){
		Log(DEBUG, "Login", "Spawning player '%s'", getName(id));
		Respawn_me(id);
		Camera_unsetJoinCam(id);
	}
	remove_task(id+TASK_LOGIN_TASK_SPAWN);
}

public setRaceByData(id, team)
{
	switch(team)
	{
		case 1: { // T
			switch(getRace(id)){
				case 1: cs_set_user_team(id, CS_TEAM_T, CS_T_TERROR);	// human
				case 2: cs_set_user_team(id, CS_TEAM_T, CS_T_ARCTIC);	// elf
				case 3: cs_set_user_team(id, CS_TEAM_T, CS_T_LEET);		// dark elf
				case 4: cs_set_user_team(id, CS_TEAM_T, CS_T_GUERILLA);	// orc
			}
		}
		case 2: { // CT
			switch(getRace(id)){
				case 1: cs_set_user_team(id, CS_TEAM_CT, CS_CT_GSG9);	// human
				case 2: cs_set_user_team(id, CS_TEAM_CT, CS_CT_GIGN);	// elf
				case 3: cs_set_user_team(id, CS_TEAM_CT, CS_CT_SAS);	// dark elf
				case 4: cs_set_user_team(id, CS_TEAM_CT, CS_CT_URBAN);	// orc
			}
		}
	}
}

/**
 * UNASSIGNED	0
 * TERROR		1
 * COUNTER		2
 * SPECTATOR	3
 */
fm_set_user_team(id, team)
{
	set_pdata_int(id, OFFSET_TEAM /* <Const> */, team);
	dllfunc(DLLFunc_ClientUserInfoChanged, id, engfunc(EngFunc_GetInfoKeyBuffer, id));
	
	/*switch(team){
		case 3: dllfunc(DLLFunc_ClientKill, id);
		default: dllfunc(DLLFunc_Spawn, id);
	}*/

	message_begin(MSG_ALL, gmsgTeamInfo); // Message dest should be MSG_[ALL | BROADCAST] and not MSG_ONE...
	write_byte(id);
	static _teams[][] = 
	{
		"", // LOL NULL
		"TERRORIST",
		"CT",
		"SPECTATOR"
	};
	write_string(_teams[team]);
	message_end();

	return 1;
}

/**
 * Fakemeta's set team stock by ConnorMcLeod.
 */
/*
#define PEV_PDATA_SAFE			2 
#define OFFSET_TEAM				114
//#define OFFSET_DEFUSE_PLANT		193
//#define HAS_DEFUSE_KIT			(1<<16)
//#define OFFSET_INTERNALMODEL	126

stock fm_cs_set_user_team(id, team)
{ 
	if(!(1 <= id <= get_maxplayers()) || pev_valid(id) != PEV_PDATA_SAFE)
		return 0;

	switch(team)
	{
		case 1: 
		{
//			new iDefuser = get_pdata_int(id, OFFSET_DEFUSE_PLANT);
//			if(iDefuser & HAS_DEFUSE_KIT){
//				iDefuser -= HAS_DEFUSE_KIT;
//				set_pdata_int(id, OFFSET_DEFUSE_PLANT, iDefuser);
//			}

			set_pdata_int(id, OFFSET_TEAM, 1);
			//set_pdata_int(id, OFFSET_INTERNALMODEL, 4);
		}
		case 2: 
		{
//			if(pev(id, pev_weapons) & (1<<CSW_C4)){
//				engclient_cmd(id, "drop", "weapon_c4");
//			}
			
			set_pdata_int(id, OFFSET_TEAM, 2);
			//set_pdata_int(id, OFFSET_INTERNALMODEL, 6);
		}
	}

	dllfunc(DLLFunc_ClientUserInfoChanged, id, engfunc(EngFunc_GetInfoKeyBuffer, id));

	return 1;
}
*/
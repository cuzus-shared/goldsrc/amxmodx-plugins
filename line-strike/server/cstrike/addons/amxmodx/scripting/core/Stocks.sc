// --------------------------------------------------
// Stocks
// --------------------------------------------------

stock bool:isActiveWeaponBow(id) /** weapon in hands is bow */
{
	new tmp;
	if(get_user_weapon(id, tmp, tmp) == CSW_MP5NAVY)
		return true;
	return false;
}

stock bool:isActiveWeaponKnife(id) /** weapon in hands is knife */
{
	new tmp;
	if(get_user_weapon(id, tmp, tmp) == CSW_KNIFE)
		return true;
	return false;
}

stock bool:playerHasBow(id) /** player has bow in weapon list */
{
	new weapons[32], num;
	get_user_weapons(id, weapons, num);

	for(new i = 0; i < num; i++)
		if(weapons[i] == CSW_MP5NAVY)
			return true;

	return false;
}

/**
 * Scoreboard update
 */
stock Create_MSG_ScoreInfo(player, frags, deaths, unk1, team)
{
	message_begin(MSG_ALL, gmsgScoreInfo);
	write_byte(player);
	write_short(frags);
	write_short(deaths);
	write_short(unk1);
	write_short(team);
	message_end();
}

/* originally from messages_stocks.inc, but simplified */
/* gmsgDeathMsg - already blocked */
stock Create_MSG_DeathMsg(killer, victim, headshot, const weapon[])
{
	message_begin(MSG_ALL, gmsgDeathMsg);
	write_byte(killer);
	write_byte(victim);
	write_byte(headshot);
	write_string(weapon);
	message_end();
}

stock crosshairRemove(id)
{
	message_begin(MSG_ONE_UNRELIABLE, gmsgHideWeapon, _, id);
	write_byte(1<<6);
	message_end();
}

stock set_user_clip(id, ammo)
{
	new weaponname[32], weaponid = -1, weapon = get_user_weapon(id, _, _);
	get_weaponname(weapon, weaponname, 31);
	
	while((weaponid = engfunc(EngFunc_FindEntityByString, weaponid, "classname", weaponname)) != 0)
	{
		if(pev(weaponid, pev_owner) == id) 
		{
			set_pdata_int(weaponid, 51, ammo, 4);
			return weaponid;
		}
	}
	return 0;
}

/*
stock UTIL_FixedUnsigned16( Float:flValue, iScale )
{
	new iOutput = floatround( flValue * iScale );

	if( iOutput < 0 )
		iOutput = 0;

	if( iOutput > 0xFFFF )
		iOutput = 0xFFFF;

	return iOutput;
}

stock Float:UTIL_Distance2D(Float:X, Float:Y)
{
	return floatsqroot(( X * X ) + ( Y * Y ));
}

stock Float:UTIL_Radian2Degree(Float:Radian)
{
	return Radian * 360.0 / ( 2 * M_PI );
}
*/

/**
 * Skills Stocks
 */
stock ham_cs_get_weapon_ent_owner(entity)
{
	return get_pdata_cbase(entity, OFFSET_WEAPONOWNER, OFFSET_LINUX_WEAPONS);
}

stock Float:fm_get_ent_speed(id)
{
	if(!pev_valid(id))
		return 0.0;

	static Float:vVelocity[3];
	pev(id, pev_velocity, vVelocity);

	vVelocity[2] = 0.0;

	return vector_length(vVelocity);
}

stock get_user_eye_position(id, Float:flOrigin[3])
{
	static Float:flViewOffs[3];
	//entity_get_vector(id, EV_VEC_view_ofs, flViewOffs)
	pev(id, pev_view_ofs, flViewOffs);
	//entity_get_vector(id, EV_VEC_origin, flOrigin)
	pev(id, pev_origin, flOrigin);
	xs_vec_add(flOrigin, flViewOffs, flOrigin);
}

stock make_vector(Float:flVec[3])
{
	flVec[0] -= 30.0;
	engfunc(EngFunc_MakeVectors, flVec);
	flVec[0] = -(flVec[0] + 30.0);
}

stock bool:isOnGround(index)
{
	return pev(index, pev_flags) & FL_ONGROUND ? true : false;
}

// Hit knockback
// Stock by the one and only, Chronic :P
stock createVelocityVector(victim, attacker, Float:velocity[3])
{
	if(!is_user_alive(victim) || !is_user_alive(attacker))
		return 0;

	new Float:vicorigin[3];
	new Float:attorigin[3];
	entity_get_vector(victim, EV_VEC_origin, vicorigin);
	entity_get_vector(attacker, EV_VEC_origin, attorigin);

	new Float:origin2[3]
	origin2[0] = vicorigin[0] - attorigin[0];
	origin2[1] = vicorigin[1] - attorigin[1];

	new Float:largestnum = 0.0;

	if(floatabs(origin2[0])>largestnum)
		largestnum = floatabs(origin2[0]);
	if(floatabs(origin2[1])>largestnum)
		largestnum = floatabs(origin2[1]);

	origin2[0] /= largestnum;
	origin2[1] /= largestnum;
	
	new pump_force = 2; // knockback power

	velocity[0] = (origin2[0] * (get_pcvar_float(pump_force) * 2000)) / get_entity_distance(victim, attacker);
	velocity[1] = (origin2[1] * (get_pcvar_float(pump_force) * 2000)) / get_entity_distance(victim, attacker);
	if(velocity[0] <= 20.0 || velocity[1] <= 20.0)
		velocity[2] = random_float(200.0 , 275.0);

	return 1;
}

/**
 * Summon */
stock get_offset_origin_body(ent, const Float:offset[3], Float:origin[3])
{
	if(!pev_valid(ent))
		return 0;

	new Float:angle[3];
	pev(ent,pev_angles,angle);
	pev(ent,pev_origin,origin);

	origin[0] += floatcos(angle[1],degrees) * offset[0];
	origin[1] += floatsin(angle[1],degrees) * offset[0];

	origin[1] += floatcos(angle[1],degrees) * offset[1];
	origin[0] += floatsin(angle[1],degrees) * offset[1];

	return 1;
}

/**
 *  Determines velocity (new_velocity) that
 *  you would set an entity to in order for
 *  it to go at "speed" from "origin1" to
 *  "origin2".
 */
stock get_speed_vector(const Float:origin1[3],const Float:origin2[3],Float:speed, Float:new_velocity[3])
{
	new_velocity[0] = origin2[0] - origin1[0];
	new_velocity[1] = origin2[1] - origin1[1];
	new_velocity[2] = origin2[2] - origin1[2];
	new Float:num = floatsqroot(speed*speed / (new_velocity[0]*new_velocity[0] + new_velocity[1]*new_velocity[1] + new_velocity[2]*new_velocity[2]));
	new_velocity[0] *= num;
	new_velocity[1] *= num;
	new_velocity[2] *= num;

	return 1;
}

/**
 *  Determines velocity (new_velocity) that
 *  you would set "ent1" to in order for it
 *  to go at "speed" from "ent1"'s origin
 *  to "ent2"'s origin.
 */
stock get_speed_vector2(ent1, ent2, Float:speed, Float:new_velocity[3])
{
	if(!pev_valid(ent1) || !pev_valid(ent2))
		return 0;

	static Float:origin1[3];
	pev(ent1,pev_origin,origin1);
	static Float:origin2[3];
	pev(ent2,pev_origin,origin2);

	new_velocity[0] = origin2[0] - origin1[0];
	new_velocity[1] = origin2[1] - origin1[1];
	new_velocity[2] = origin2[2] - origin1[2];
	new Float:num = floatsqroot(speed*speed / (new_velocity[0]*new_velocity[0] + new_velocity[1]*new_velocity[1] + new_velocity[2]*new_velocity[2]));
	new_velocity[0] *= num;
	new_velocity[1] *= num;
	new_velocity[2] *= num;

	return 1;
}

/**
 *  Determines if a player is crouching or not.
 *  Return 1 if crouching and 0 if not.
 *
 *  Set ignoreplayer to 1 if you are using on
 *  a HL monster that can crouch.
 */
stock is_user_crouching(ent, ignoreplayer=0)
{
	if(!isAlive(ent) && !ignoreplayer)
		return 0;

	new Float:minsize[3];
	pev(ent, pev_mins, minsize);

	if(minsize[2] == -18.0)
		return 1;

	return 0;
}

// tested
stock is_wall_between_points(Float:start[3], Float:end[3], ignore_ent)
{
	static ptr
	ptr = create_tr2()

	engfunc(EngFunc_TraceLine, start, end, IGNORE_MONSTERS, ignore_ent, ptr)
	
	static Float:EndPos[3]
	get_tr2(ptr, TR_vecEndPos, EndPos)

	free_tr2(ptr)
	return floatround(get_distance_f(end, EndPos))
}

stock set_player_nextattackx(id, Float:nexttime)
{
	if(!isAlive(id))
		return
		
	set_pdata_float(id, m_flNextAttack, nexttime, 5)
}

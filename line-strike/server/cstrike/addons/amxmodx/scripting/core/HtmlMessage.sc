// --------------------------------------------------
// HtmlMessage
// --------------------------------------------------

// MAX L2 HTML 308x424
#define MIN_HTML_LENGTH
#define MAX_HTML_ROW_LENGTH			512
#define MAX_HTML_CONTENT_LENGTH		2048 //8192b

// Html paths for actual game modes
#define HTML_OD_END		"addons/amxmodx/data/html/outpostdefense.html"
#define HTML_TVT_END	"addons/amxmodx/data/html/teamvsteam.html"
#define HTML_LH_END		"addons/amxmodx/data/html/lasthero.html"
#define HTML_CS_END		"addons/amxmodx/data/html/castlesiege.html"

/**
 * Send html content by MOTD to player.
 */
public sendHtml(id, html[], title[])
{
	if(isConnected(id) && !isBot(id))
		show_motd(id, html, title);
}

/**
 * Load html content from a file.
 *
 * @return content length
 */
public getHtm(path[], content[])
{
	if(equali(path, "") || strlen(path) < 4){
		Log(ERROR, "HtmlMessage", "getHtm(): Invalid path: %s", path);
	}
	else if(file_exists(path))
	{
		Log(DEBUG, "HtmlMessage", "Begin read HTML file: %s", path);
		
		// html body (l2-like: default for all)
		format(content, MAX_HTML_CONTENT_LENGTH-1, "<html><body bgcolor='#050505' style='max-width: 296px; max-height: 412px; line-height: 1.0; font-family: Tahoma; font-size: 13px; color: #DCD9DC; text-decoration: none;'>");
		Log(DEBUG, "HtmlMessage", "##: %s", content);
		
		new row[MAX_HTML_ROW_LENGTH];
		new ln, th;
		for(ln=0; read_file(path, ln, row, MAX_HTML_ROW_LENGTH-1, th); ln++)
		{
			Log(DEBUG, "HtmlMessage", "%02d: %s", ln, row);
			
			// TODO: skip html comments (now by 1 line only)
			if(equali(row[0],"<") && equali(row[1],"!") && equali(row[2],"-") && equali(row[3],"-"))
				continue;
			
			add(content, MAX_HTML_CONTENT_LENGTH-1, row, MAX_HTML_ROW_LENGTH-1);
		}
		
		format(row, MAX_HTML_ROW_LENGTH-1, "</body></html>");
		Log(DEBUG, "HtmlMessage", "##: %s", row);
		add(content, MAX_HTML_CONTENT_LENGTH-1, row, MAX_HTML_ROW_LENGTH-1);
		
		Log(DEBUG, "HtmlMessage", "End read HTML file: %s", path);
	}else{
		Log(ERROR, "HtmlMessage", "Missing HTML file: %s", path);
	}

	if(equali(content, "") || strlen(content) < 4) //&copy html body
		format(content, MAX_HTML_CONTENT_LENGTH-1, "<html><body bgcolor='#050505' style='max-width: 296px; max-height: 412px; line-height: 1.0; font-family: Tahoma; font-size: 13px; color: #DCD9DC; text-decoration: none;'>I have nothing to say to you.</body></html>");
	
	return strlen(content);
}
// ----------------------------------------------------------------------
// Skills
// ----------------------------------------------------------------------

// Cooldowns (variables)
new Float:CD_BattleHeal[XX]			= {0.0, ...};
new Float:CD_BalanceLife[XX]		= {0.0, ...};
new Float:CD_DryadRoot[XX]			= {0.0, ...};
new Float:CD_Return[XX]				= {0.0, ...};
new Float:CD_Dash[XX]				= {0.0, ...};
new Float:CD_StunShot[XX]			= {0.0, ...};
new Float:CD_DoubleShot[XX]			= {0.0, ...};
new Float:CD_LethalShot[XX]			= {0.0, ...};
new Float:CD_Snipe[XX]				= {0.0, ...};
new Float:CD_Bluff[XX]				= {0.0, ...};
new Float:CD_DeadlyBlow[XX]			= {0.0, ...};
new Float:CD_Backstab[XX]			= {0.0, ...};
new Float:CD_ShieldStun[XX]			= {0.0, ...};
new Float:CD_Aggression[XX]			= {0.0, ...};
new Float:CD_UltDefense[XX]			= {0.0, ...};
new Float:CD_Hurricane[XX]			= {0.0, ...};
new Float:CD_AuraFlare[XX]			= {0.0, ...};
//new Float:CD_Sleep[XX]			= {0.0, ...};
new Float:CD_Silence[XX]			= {0.0, ...};
new Float:CD_SummonCorrupted[XX]	= {0.0, ...};
new Float:CD_SummonCubics[XX]		= {0.0, ...};
new Float:CD_FocusedForce[XX]		= {0.0, ...};
new Float:CD_SoulBreaker[XX]		= {0.0, ...};
new Float:CD_ForceBlaster[XX]		= {0.0, ...};
new Float:CD_Frenzy[XX]				= {0.0, ...};
new Float:CD_ArmorCrush[XX]			= {0.0, ...};
new Float:CD_Zealot[XX]				= {0.0, ...};
new Float:CD_BattleRoar[XX]			= {0.0, ...};
new Float:CD_HammerCrush[XX]		= {0.0, ...};
new Float:CD_DreamingSpirit[XX]		= {0.0, ...};
new Float:CD_StealEssence[XX]		= {0.0, ...};
new Float:CD_GateChant[XX]			= {0.0, ...};
// 
// Player in ... effect
new bool:PIN_Stun[XX]				= {false, ...};
new bool:PIN_Snipe[XX]				= {false, ...};
new bool:PIN_Dash[XX]				= {false, ...};
new bool:PIN_FistFury[XX]			= {false, ...};
new bool:PIN_Frenzy[XX]				= {false, ...};
new bool:PIN_Zealot[XX]				= {false, ...};
new bool:PIN_UltDefense[XX]			= {false, ...};
new bool:PIN_SummonCubics[XX]		= {false, ...};
new bool:PIN_Silence[XX]			= {false, ...};
new bool:PIN_SummonCorrupted[XX]	= {false, ...};
new bool:PIN_DryadRoot[XX]			= {false, ...};
new bool:PIN_BalanceLife[XX]		= {false, ...};
new bool:PIN_Return[XX]				= {false, ...};
new bool:PIN_DreamingSpirit[XX]		= {false, ...};
new bool:PIN_Sleep[XX]				= {false, ...};
new bool:PIN_GateChant[XX]			= {false, ...};
// 
// Cooldowns (config time)
#define COOLDOWN_DASH					7.0 //30.0
#define COOLDOWN_BLUFF					2.0	//?
#define COOLDOWN_DEADLY_BLOW			2.0 //?
#define COOLDOWN_BACKSTAB				4.0
#define COOLDOWN_STUN_SHOT				3.0 //25.0
#define COOLDOWN_DOUBLE_SHOT			3.0 //?
#define COOLDOWN_LETHAL_SHOT			3.0 //?
#define COOLDOWN_SNIPE					2.0
#define COOLDOWN_PUMA_TOTEM_SPIRIT		3.0 //30.0
#define COOLDOWN_HURRICANE				0.9
#define COOLDOWN_AURA_FLARE				0.2 //0.3
//#define COOLDOWN_SLEEP					2.0
#define COOLDOWN_SILENCE				3.0
#define COOLDOWN_SUMMON_CORRUPTED		3.0 //30.0
#define COOLDOWN_SUMMON_CUBICS			5.0
#define COOLDOWN_SHIELD_STUN			2.0 // ?
#define COOLDOWN_AGGRESSION				3.0
#define COOLDOWN_ULTIMATE_DEFENSE		3.0 // ?
#define COOLDOWN_BATTLE_HEAL			0.5
#define COOLDOWN_BALANCE_LIFE			3.0 //@l2: 30.0
#define COOLDOWN_DRYAD_ROOT				3.0
#define COOLDOWN_RETURN					10.0
#define COOLDOWN_FRENZY					6.0 //180.0
#define COOLDOWN_ARMOR_CRUSH			6.0 //?
#define COOLDOWN_ZEALOT					6.0 //180.0
#define COOLDOWN_BATTLE_ROAR			5.0
#define COOLDOWN_FOCUSED_FORCE			1.1
#define COOLDOWN_SOUL_BREAKER			3.0
#define COOLDOWN_FORCE_BLASTER			5.0
#define COOLDOWN_HAMMER_CRUSH			3.0
#define COOLDOWN_DREAMING_SPIRIT		4.0
#define COOLDOWN_STEAL_ESSENCE			1.1
#define COOLDOWN_GATE_CHANT				9.0
//=======================================================================
//							Skill time
//=======================================================================
#define SKILLTIME_DASH					6.0
#define SKILLTIME_STUN_SHOT				3.4	//3.3
#define SKILLTIME_SNIPE					12.0 //120.0 @truel2
//#define SKILLTIME_PUMA_SPIRIT_TOTEM		20.0
#define SKILLTIME_FRENZY				15.0 //120.0 @truel2
//#define SKILLTIME_GUTS					15.0 // ?
#define SKILLTIME_ZEALOT				15.0 //60.0 @truel2
#define SKILLTIME_SLEEP					8.0 // ?
#define SKILLTIME_SILENCE				10.0
#define SKILLTIME_DRYAD_ROOT			10.0
#define SKILLTIME_ULTIMATE_DEFENSE		10.0 //30.0 @truel2
//=======================================================================
//							Cast time
//=======================================================================
#define CASTTIME_SLEEP					0.4
#define CASTTIME_SUMMON_REANIMATED		4
#define CASTTIME_SUMMON_CUBICS			5
#define CASTTIME_GATE_CHANT				3
#define CASTTIME_RETURN					1.1
#define CASTTIME_DRAIN_HEALTH			1.1
#define CASTTIME_ULTIMATE_DEFENSE		0.6
#define CASTTIME_SNIPE					0.6
#define CASTTIME_HAMMER_CRUSH			0.3
//=======================================================================
//								Damages
//=======================================================================
#define SKILLDAMAGE_DEADLY_BLOW			30.0
#define SKILLDAMAGE_BACKSTAB			50.0
#define SKILLDAMAGE_STUN_SHOT_MIN		5.0
#define SKILLDAMAGE_STUN_SHOT_MAX		25.0
#define SKILLDAMAGE_DOUBLE_SHOT_MIN		20.0	// todo: calc default min/max * 2 or 3
#define SKILLDAMAGE_DOUBLE_SHOT_MAX		30.0
#define SKILLDAMAGE_LETHAL_SHOT_MIN		25.0
#define SKILLDAMAGE_LETHAL_SHOT_MAX		35.0
#define SKILLDAMAGE_SNIPE_MIN			15.0
#define SKILLDAMAGE_SNIPE_MAX			30.0
#define SKILLDAMAGE_FRENZY_MIN			20.0
#define SKILLDAMAGE_FRENZY_MAX			35.0
#define SKILLDAMAGE_AURA_FLARE			7.0 //5.0
#define	SKILLDAMAGE_HURRICANE			20
#define SKILLDAMAGE_STEAL_ESSENCE_MIN	15.0
#define SKILLDAMAGE_STEAL_ESSENCE_MAX	20.0
//=======================================================================
//								Range
//=======================================================================
#define SKILLRANGE_BLUFF				90
#define SKILLRANGE_DEADLY_BLOW			75.0
#define SKILLRANGE_BACKSTAB				60.0
#define SKILLRANGE_AURA_FLARE			45.0 //60
//#define SKILLRANGE_SLEEP				350
#define SKILLRANGE_SILENCE				300
#define SKILLRANGE_SHIELD_STUN			80
#define SKILLRANGE_AGGRESSION			230.0
#define SKILLRANGE_DRYAD_ROOT			280
#define SKILLRANGE_BATTLE_HEAL			300
#define SKILLRANGE_BALANCE_LIFE			400.0
#define SKILLRANGE_DREAMING_SPIRIT		380.0
#define SKILLRANGE_HAMMER_CRUSH			50
#define SKILLRANGE_SOUL_BREAKER			65
#define SKILLRANGE_FORCE_BLASTER		500
#define SKILLRANGE_ARMOR_CRUSH			75
#define SKILLRANGE_STEAL_ESSSENCE		400
//=======================================================================
//								Atk spd
//=======================================================================
#define SKILLS_FIST_FURY_ATK_SPEED		0.36	// Core_inl hamKnifePrimaryAttack
#define SKILLS_ZEALOT_ATK_SPEED			1.0 //0.9
//=======================================================================
//								Fly spd
//=======================================================================
#define FLYSPEED_STUN_SHOT				1520
#define FLYSPEED_LETHAL_SHOT			2250
#define FLYSPEED_HURRICANE				1360 //1395
#define FLYSPEED_FORCE_BLASTER			1200
//=======================================================================
//								Chances
//=======================================================================
#define SKILLCHANCE_REFLECT_DAMAGE			20	//@l2off : 10%
#define SKILLCHANCE_STUN_SHOT				45
#define SKILLCHANCE_LETHAL_SHOT				30	//2
#define SKILLCHANCE_DEADLY_BLOW_KILL		8	//@l2off : 10%
#define SKILLCHANCE_BLUFF					25
#define SKILLCHANCE_SHIELD_STUN				40
#define SKILLCHANCE_HAMMER_CRUSH			45
#define SKILLCHANCE_SOUL_BREAKER_STUN		40
#define SKILLCHANCE_SOUL_BREAKER_RETURN		15
#define SKILLCHANCE_SLEEP					35
#define SKILLCHANCE_SILENCE					45	//20
#define SKILLCHANCE_DRYAD_ROOT				45
//=======================================================================
//								Other
//=======================================================================
#define SKILLS_BATTLE_ROAR_HP_MIN			80
#define SKILLS_BATTLE_ROAR_HP_MAX			95
#define SKILLS_BATTLE_HEAL_MIN				6
#define SKILLS_BATTLE_HEAL_MAX				18
//=======================================================================
//							PreThink control
//=======================================================================
new Float:LastClickTime[XX]				= {0.0, ...};
#define PRE_THINK_CLICK_TIME			1.5
//=======================================================================
//								Etc
//=======================================================================
new bool:SyncGateChant[3];	// 1=T, 2=CT
new bool:SyncReturn[3];

new FocusedForces[XX]					= {0, ...};

new bool:PIN_Cast[XX]					= {false, ...}; // FOR ALL : skills sync protect var

new ForceBlasterTarget[XX];
new Float:ForceBlasterDamage[XX];

new ReflectDamageChance[XX] = {SKILLCHANCE_REFLECT_DAMAGE, ...};

new bool:IsCastingNow[XX]				= {false, ...};
new bool:AllSkillsBlocked[XX]			= {false, ...};
new bool:AttackBlocked[XX]				= {false, ...};
new bool:MoveBlocked[XX]				= {false, ...};
new bool:MoveStopped[XX]				= {false, ...};
new bool:DuckJumpBlocked[XX]			= {false, ...};
new bool:CameraLocked[XX]				= {false, ...};
new Float:CameraAngles[XX][3];

new BuffAura[XX];
new DebuAura[XX];

#define DotProduct(%0,%1)	(%0[0] * %1[0] + %0[1] * %1[1] + %0[2] * %1[2])
#define MULTIPLY 30.0
//=======================================================================

public bool:isValidUse(id, needed_class)
{
	if(warmupConfirmed())
		if(getClass(id) == needed_class && !isSkillsBlocked(id) && isAlive(id) && haveTeam(id) && !PIN_Cast[id])
			return true;
	return false;
}

public bool:isCastingNow(id)
{
	if(IsCastingNow[id])
		return true;
	return false;
}
public Skills_castStart(id)
{
	IsCastingNow[id] = true;
	blockAttack(id);
	//blockMove(id);
}
public Skills_castEnd(id)
{
	IsCastingNow[id] = false;
	unblockAttack(id);
	//unblockMove(id);
}

/**
 * Camera lock functions
 */
public bool:isCameraLocked(id)
{
	if(CameraLocked[id])
		return true;
	return false;
}
public lockCamera(id)
{
	if(id > 0 && id <= 32)
	{
		/*if(~ pev(id, pev_flags) & FL_FROZEN){
			set_pev(id, pev_flags, pev(id, pev_flags) | FL_FROZEN);
			pev(id, pev_v_angle, CameraAngles[id]);
		}*/
		CameraLocked[id] = true;
	}
	else if(id == FOR_ALL)
	{
		for(id=1; id<=32; id++){
			/*if(~ pev(id, pev_flags) & FL_FROZEN){
				set_pev(id, pev_flags, pev(id, pev_flags) | FL_FROZEN);
				pev(id, pev_v_angle, CameraAngles[id]);
			}*/
			CameraLocked[id] = true;
		}
	}
}
public unlockCamera(id)
{
	if(id > 0 && id <= 32)
	{
		/*if(pev(id, pev_flags) & FL_FROZEN)
			set_pev(id, pev_flags, pev(id, pev_flags) & ~FL_FROZEN);*/
		CameraLocked[id] = false;
	}
	else if(id == FOR_ALL)
	{
		for(id=1; id<=32; id++){
			/*if(pev(id, pev_flags) & FL_FROZEN)
				set_pev(id, pev_flags, pev(id, pev_flags) & ~FL_FROZEN);*/
			CameraLocked[id] = false;
		}
	}
}

/**
 * Duck, jump block.
 */
public bool:isDuckJumpBlocked(id)
{
	if(DuckJumpBlocked[id])
		return true;
	return false;
}
public blockDuckJump(id)
{
	if(id > 0 && id <= 32){
		DuckJumpBlocked[id] = true;
	}else if(id == FOR_ALL){
		for(id=1; id<=32; id++){
			DuckJumpBlocked[id] = true;
		}
	}
}
public unblockDuckJump(id)
{
	if(id > 0 && id <= 32){
		DuckJumpBlocked[id] = false;
	}else if(id == FOR_ALL){
		for(id=1; id<=32; id++){
			DuckJumpBlocked[id] = false;
		}
	}
}

/**
 * Blocks player move.
 */
public bool:isMoveBlocked(id)
{
	if(MoveBlocked[id])
		return true;
	return false;
}
public blockMove(id)
{
	if(id > 0 && id <= 32){
		MoveBlocked[id] = true;
	}else if(id == FOR_ALL){
		for(id=1; id<=32; id++){
			MoveBlocked[id] = true;
		}
	}
}
public unblockMove(id)
{
	if(id > 0 && id <= 32){
		MoveBlocked[id] = false;
	}else if(id == FOR_ALL){
		for(id=1; id<=32; id++){
			MoveBlocked[id] = false;
		}
	}
}

/**
 * Stop player move.
 */
public bool:isMoveStopped(id)
{
	if(MoveStopped[id])
		return true;
	return false;
}
public stopMove(id)
{
	if(id > 0 && id <= 32){
		MoveStopped[id] = true;
	}else if(id == FOR_ALL){
		for(id=1; id<=32; id++){
			MoveStopped[id] = true;
		}
	}
}
public resumeMove(id)
{
	if(id > 0 && id <= 32){
		MoveStopped[id] = false;
	}else if(id == FOR_ALL){
		for(id=1; id<=32; id++){
			MoveStopped[id] = false;
		}
	}
}
 
/**
 * Blocks player attack.
 */
public bool:isAttackBlocked(id)
{
	if(AttackBlocked[id])
		return true;
	return false;
}
public blockAttack(id)
{
	if(id > 0 && id <= 32){
		AttackBlocked[id] = true;
	}else if(id == FOR_ALL){
		for(id=1; id<=32; id++){
			AttackBlocked[id] = true;
		}
	}
}
public unblockAttack(id)
{
	if(id > 0 && id <= 32){
		AttackBlocked[id] = false;
	}else if(id == FOR_ALL){
		for(id=1; id<=32; id++){
			AttackBlocked[id] = false;
		}
	}
}

/**
 * Blocks skills using.
 */
public bool:isSkillsBlocked(id)
{
	if(AllSkillsBlocked[id])
		return true;
	return false;
}
public blockSkills(id)
{
	if(id > 0 && id <= 32){
		AllSkillsBlocked[id] = true;
	}else if(id == FOR_ALL){
		for(id=1; id<=32; id++){
			AllSkillsBlocked[id] = true;
		}
	}
}
public unblockSkills(id)
{
	if(id > 0 && id <= 32){
		AllSkillsBlocked[id] = false;
	}else if(id == FOR_ALL){
		for(id=1; id<=32; id++){
			AllSkillsBlocked[id] = false;
		}
	}
}

//=======================================================================

/*public Skills_clientPutInServer(id)
{
	Skills_reset(id);
}*/

public Skills_clientDisconnect(id)
{
	Skills_reset(id);
	
	Skills_hamPlayerKilledPre(id);
}

public Skills_hamPlayerKilledPre(id) // Core's func (on death)
{
	resetEffects(id);
	removeSummonedUnit(id); // <Summon>

	//if(PIN_Stun[id])	// test dis
	//	removeStun(id);
	
	// TODO: BUGFIX: abort skills cast on death
	/*switch(getClass(id))
	{
		case CLASS_A: {
			abortStunShot(id);
			
		}
	}*/
}

public Skills_reset(id)
{
	resetEffects(id);
	resetCooldowns(id);
}

public Skills_resetAll(id)
{
	for(new id=1; id <= GMaxPlayers; id++)
	{
		//if(isConnected && alive && ...)
		resetEffects(id);
		resetCooldowns(id);
	}
}

public resetEffects(id) /* @truel2 : player.stopAllEffects(); */
{
	// TODO : add all effects @wait FIN
	
	PIN_Stun[id] 				= false;
	PIN_Snipe[id]				= false;
	PIN_Dash[id]				= false;
	PIN_FistFury[id]			= false;
	PIN_Frenzy[id]				= false;
	PIN_Zealot[id]				= false;
	PIN_UltDefense[id]			= false;
	PIN_SummonCorrupted[id]		= false;
	PIN_Sleep[id]				= false;
	PIN_Silence[id]				= false;
	PIN_DryadRoot[id]			= false;
	PIN_Return[id]				= false;
	PIN_BalanceLife[id]			= false;
	PIN_DreamingSpirit[id]		= false;
	PIN_GateChant[id]			= false;
	PIN_SummonCubics[id]		= false;
	FocusedForces[id] 			= 0;

	Log(DEBUG, "Skills", "Effects has been cleared from %s", getName(id));
}

public resetCooldowns(id)
{
	CD_StunShot[id]				= 0.0;
	CD_DoubleShot[id]			= 0.0;
	CD_LethalShot[id]			= 0.0;
	CD_Snipe[id]				= 0.0;
	
	CD_DeadlyBlow[id]			= 0.0;
	CD_Backstab[id]				= 0.0;
	CD_Bluff[id]				= 0.0;
	CD_Dash[id]					= 0.0;
	
	CD_SoulBreaker[id]			= 0.0;
	CD_FocusedForce[id]			= 0.0;
	CD_ForceBlaster[id]			= 0.0;
	
	CD_Frenzy[id]				= 0.0;
	CD_Zealot[id]				= 0.0;
	CD_ArmorCrush[id]			= 0.0;
	CD_BattleRoar[id]			= 0.0;
	
	CD_Aggression[id]			= 0.0;
	CD_ShieldStun[id]			= 0.0;
	CD_UltDefense[id]			= 0.0;
	CD_SummonCubics[id]			= 0.0;

	CD_Hurricane[id]			= 0.0;
	CD_AuraFlare[id]			= 0.0;
	CD_Silence[id]				= 0.0;
	CD_SummonCorrupted[id]		= 0.0;
	
	CD_DryadRoot[id]			= 0.0;
	CD_BattleHeal[id]			= 0.0;
	CD_BalanceLife[id]			= 0.0;
	CD_Return[id]				= 0.0;

	CD_StealEssence[id]			= 0.0;
	CD_HammerCrush[id]			= 0.0;
	CD_DreamingSpirit[id]		= 0.0;
	CD_GateChant[id]			= 0.0;
}

public cooltimeEnd(id)
{
	id -= TASK_COOLTIME_END;
	emit_sound(id, CHAN_AUTO, S_COOLDOWN_END, 0.5, ATTN_STATIC, 0, PITCH_NORM);
}

//=======================================================================
//							Aura attachments
//=======================================================================
#define AURA_DIVINE		0
#define AURA_ESCAPE		1
#define AURA_ROOT_C		2
#define AURA_ROOT_T		3
#define AURA_SUMMON		4
#define AURA_VAMPIRIC	5
#define AURA_DRAIN		6
#define AURA_SLEEP		7
#define AURA_FLARE		8
#define AURA_SNIPE		9

public showBuffAura(id, submodel, seq)
{
	if(!BuffAura[id])
	{
		BuffAura[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
		set_pev(BuffAura[id], pev_movetype, MOVETYPE_FOLLOW);
		set_pev(BuffAura[id], pev_aiment, id);
		set_pev(BuffAura[id], pev_rendermode, kRenderNormal);
		engfunc(EngFunc_SetModel, BuffAura[id], LineageEffectA);
		set_pev(BuffAura[id], pev_body, submodel);
//		set_pev(BuffAura[id], pev_animtime, 2.0);
		set_pev(BuffAura[id], pev_frame, 1);
		set_pev(BuffAura[id], pev_framerate, 1.0);
		set_pev(BuffAura[id], pev_sequence, seq);
		set_pev(BuffAura[id], pev_gaitsequence, seq);
		set_pev(BuffAura[id], pev_nextthink, get_gametime() + 0.1);
	}else{
		set_pev(BuffAura[id], pev_body, submodel);
		fm_set_entity_visibility(BuffAura[id], 1); /* Util.inl */
	}
}
public hideBuffAura(id)
{
	if(BuffAura[id])
		fm_set_entity_visibility(BuffAura[id], 0);
}

public showDebuffAura(id, submodel, seq)
{
	if(!DebuAura[id])
	{
		DebuAura[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
		set_pev(DebuAura[id], pev_movetype, MOVETYPE_FOLLOW);
		set_pev(DebuAura[id], pev_aiment, id);
		set_pev(DebuAura[id], pev_rendermode, kRenderNormal);
		engfunc(EngFunc_SetModel, DebuAura[id], LineageEffectA);
		set_pev(DebuAura[id], pev_body, submodel);
//		set_pev(DebuAura[id], pev_animtime, 2.0);
		set_pev(DebuAura[id], pev_frame, 1);
		set_pev(DebuAura[id], pev_framerate, 1.0);
		set_pev(DebuAura[id], pev_sequence, seq);
		set_pev(DebuAura[id], pev_gaitsequence, seq);
		set_pev(DebuAura[id], pev_nextthink, get_gametime() + 0.1);
	}else{
		set_pev(DebuAura[id], pev_body, submodel);
		fm_set_entity_visibility(DebuAura[id], 1); /* Util.inl */
	}
}
public hideDebuffAura(id)
{
	if(DebuAura[id])
		fm_set_entity_visibility(DebuAura[id], 0);
}

//createL2Effect(id, effect, size, opacity, framerate, showtime)
public createL2Effect(const player, const effect[], Float:size, Float:opacity, Float:framerate, const Float:time)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "env_sprite"));
	if(ent)
	{
		set_pev(ent, pev_classname, "dummy");
		engfunc(EngFunc_SetModel, ent, effect);
		engfunc(EngFunc_SetSize, ent, Float:{0.0,0.0,0.0}, Float:{0.0,0.0,0.0});
//		engfunc(EngFunc_SetOrigin, ent, origin); // not works with pev_aiment
		set_pev(ent, pev_scale, size);
		set_pev(ent, pev_solid, SOLID_NOT);
		set_pev(ent, pev_movetype, MOVETYPE_FOLLOW);
		set_pev(ent, pev_aiment, player);

		set_pev(ent, pev_rendermode, 5); // 5 - Additive
		set_pev(ent, pev_renderfx, kRenderFxNone);
		set_pev(ent, pev_renderamt, opacity);

		set_pev(ent, pev_animtime, 1.0); //time
		set_pev(ent, pev_frame, 0);
		set_pev(ent, pev_framerate, framerate);
		set_pev(ent, pev_spawnflags, SF_SPRITE_STARTON);
		dllfunc(DLLFunc_Spawn, ent);

		new params[2];
		params[0] = player;
		params[1] = ent;
		set_task(time, "removeL2Effect", ent+TASK_L2EFFECT_REMOVE, params, 2);
	}
}

public removeL2Effect(params[2], taskid) // params[0] = player, params[1] = effect
{
	if(pev_valid(params[1]))
		entityDispose(params[1]);
	else
		Log(WARN, "Skills", "removeL2Effect(): Can't remove effect ID=%d from player '%s'", params[1], getName(params[0]));
}

//=======================================================================
//							Click Protector
//=======================================================================
public getClickProtector(id)
{
	/*if(!(get_gametime() - LastClickTime[id] < PRE_THINK_CLICK_TIME))
		Util_shortage(id);*/
	
	static Float:gametime; gametime = get_gametime();
	
	if(LastClickTime[id] > gametime)
		return;
	
	Util_shortage(id);
	LastClickTime[id] = gametime + 1.5;
}
public setClickProtector(id)
{
	//LastClickTime[id] = get_gametime();
}

//=======================================================================
//							Skills Handler
//=======================================================================
new SkillBtnPress[6][XX];

public Skills_press1(id){
	SkillBtnPress[1][id] = true;
	return PLUGIN_HANDLED;
}
public Skills_unpress1(id){
	SkillBtnPress[1][id] = false;
	return PLUGIN_HANDLED;
}
public Skills_checkUse1(id)
{
	if(SkillBtnPress[1][id]) // USE skill 1
	{
		switch(getClass(id))
		{
			case 1:	{altStunShot(id);}				// Archer
			case 2: {/* Single: Deadly Blow */}		// Rogue
			case 3:	{/* Single: Soul Breaker*/}		// Tyrant
			case 4:	{/* Single: Frenzy */}			// Raider
			case 5:	{/* Single: Shield Stun */} 	// Knight
			case 6:	{/* Single: Death Spike */}		// Wizard
			case 7: {/* Single: Battle Heal */}		// Priest
			case 8:	{/* Single: StealEssence */} 	// Shaman
		}
	}
	if(!SkillBtnPress[1][id]) // ABORT skill 1
	{
		switch(getClass(id))
		{
			case 1: {abortStunShot(id);}			// Archer
			case 2: {/* Single: Deadly Blow */}		// Rogue
			case 3:	{/* Single: Soul Breaker*/}		// Tyrant
			case 4:	{/* Single: Frenzy */}			// Raider
			case 5:	{/* Single: Shield Stun */} 	// Knight
			case 6:	{/* Single: Death Spike */}		// Wizard
			case 7: {/* Single: Battle Heal */}		// Priest
			case 8:	{/* Single: StealEssence */} 	// Shaman
		}
	}
}
//=======================================================================
public Skills_press2(id){
	SkillBtnPress[2][id] = true;
	return PLUGIN_HANDLED;
}
public Skills_unpress2(id){
	SkillBtnPress[2][id] = false;
	return PLUGIN_HANDLED;
}
public Skills_checkUse2(id)
{
	if(SkillBtnPress[2][id]) // USE skill 2
	{
		switch(getClass(id))
		{
			case 1: {altDouleShot(id);}				// Archer
			case 2: {/* Single: Backstab */}		// Rogue
			case 3: {/* Single: Focused Force */}	// Tyrant
			case 4:	{/* Single: Zealot */}			// Raider
			case 5:	{altSummonCubics(id);}			// Knight
			case 6:	{/* Single: Aura Flare */}		// Wizard
			case 7: {/* Single: Dryad Root */}		// Priest
			case 8:	{altDreamingSpirit(id);}		// Shaman
		}
	}
	if(!SkillBtnPress[2][id]) // ABORT skill 2
	{
		switch(getClass(id))
		{
			case 1: {abortDoubleShot(id);}			// Archer
			case 2: {/* Single: Backstab */}		// Rogue
			case 3: {/* Single: Focused Force */}	// Tyrant
			case 4:	{/* Single: Zealot */}			// Raider
			case 5:	{abortSummonCubics(id);}		// Knight
			case 6:	{/* Single: Aura Flare */}		// Wizard
			case 7: {/* Single: Dryad Root */}		// Priest
			case 8:	{abortDreamingSpirit(id);}		// Shaman
		}
	}
}
//=======================================================================
public Skills_press3(id){
	SkillBtnPress[3][id] = true;
	return PLUGIN_HANDLED;
}
public Skills_unpress3(id){
	SkillBtnPress[3][id] = false;
	return PLUGIN_HANDLED;
}
public Skills_checkUse3(id)
{
	if(SkillBtnPress[3][id]) // USE skill 3
	{
		switch(getClass(id))
		{
			case 1: {altLethalShot(id);}			// Archer
			case 2: {/* Single: Bluff */}			// Rogue
			case 3: {/* Single: Fist Fury */}		// Tyrant
			case 4:	{/* Single: Battle Roar */}		// Raider
			case 5:	{/* Single: Ult Defense */}		// Knight
			case 6:	{altSummonCorruptedMan(id);}	// Wizard
			case 7: {altBalanceLife(id);}			// Priest
			case 8:	{/* Single: Hammer Crush */} 	// Shaman
		}
	}
	if(!SkillBtnPress[3][id]) // ABORT skill 3
	{
		switch(getClass(id))
		{
			case 1: {abortLethalShot(id);}			// Archer
			case 2: {/* Single: Bluff */}			// Rogue
			case 3: {/* Single: Fist Fury */}		// Tyrant
			case 4:	{/* Single: Battle Roar */}		// Raider
			case 5:	{/* Single: Ult Defense */}		// Knight
			case 6:	{abortSummonCorruptedMan(id);}	// Wizard
			case 7: {abortBalanceLife(id);}			// Priest
			case 8:	{/* Single: Hammer Crush */} 	// Shaman
		}
	}
}
//=======================================================================
public Skills_press4(id){
	SkillBtnPress[4][id] = true;
	return PLUGIN_HANDLED;
}
public Skills_unpress4(id){
	SkillBtnPress[4][id] = false;
	return PLUGIN_HANDLED;
}
public Skills_checkUse4(id)
{
	if(SkillBtnPress[4][id]) // USE skill 4
	{
		setClickProtector(id);

		switch(getClass(id))
		{
			case 1: {/* Single: Snipe */}			// Archer
			case 2: {/* Single: Dash */}			// Rogue
			case 3: {/* Single: Puma Totem */}		// Tyrant
			case 4:	{/* Single: Guts */}			// Raider
			case 5:	{/* Single: Aggression */}		// Knight
			case 6:	{/* Single: Sleep */}			// Wizard
			case 7: {altReturn(id);}				// Priest
			case 8:	{altGateChant(id);} 			// Shaman
		}
	}
	if(!SkillBtnPress[4][id]) // ABORT skill 4
	{
		switch(getClass(id))
		{
			case 1: {/* Single: Snipe */}			// Archer
			case 2: {/* Single: Dash */}			// Rogue
			case 3: {/* Single: Puma Totem */}		// Tyrant
			case 4:	{/* Single: Guts */}			// Raider
			case 5:	{/* Single: Aggression */}		// Knight
			case 6:	{/* Single: Sleep */}			// Wizard
			case 7: {abortReturn(id);}				// Priest
			case 8:	{abortGateChant(id);} 			// Shaman
		}
	}
}
//=======================================================================
//	1	CLASS_A
//=======================================================================
/**
 * Stun Shot 
 */
public altStunShot(id)
{
	if(isValidUse(id, CLASS_A))
	{
		if(!isActiveWeaponBow(id)){
			return;
		}
		else if(get_gametime() - CD_StunShot[id] < COOLDOWN_STUN_SHOT && !inWtfMode(id) /* test */){
			return;
		}
		else
		{
			PIN_Cast[id] = true;
			
			Skills_castStart(id);
			CD_StunShot[id] = get_gametime();
			Create_BarTime(id, 1);
			Create_TE_DLIGHT(id, 12, 157,157,151, 3, 0);
			emit_sound(id, CHAN_VOICE, S_STUN_CAST, 1.0, ATTN_NORM, 0, PITCH_NORM);
			
			set_task(1.0, "useStunShot", id+TASK_SKILLS_USE_STUN_SHOT);
			set_task(0.1, "castStunShot", id+TASK_SKILLS_CAST_STUN_SHOT, _, _, "b"); // spr effect on body
			
			showSkillHud(id, "Use Stun Shot");
		}
	}
}

public abortStunShot(id)
{
	Skills_castEnd(id);
	Create_BarTime(id, 0);

	if(task_exists(id+TASK_SKILLS_USE_STUN_SHOT))
		CD_StunShot[id] = 0.0;

	remove_task(id+TASK_SKILLS_USE_STUN_SHOT);
	remove_task(id+TASK_SKILLS_CAST_STUN_SHOT);

	PIN_Cast[id] = false;
}

public useStunShot(id) //taskid
{
	id -= TASK_SKILLS_USE_STUN_SHOT;

	if(pev(id, pev_flags) & FL_DUCKING)
		Player_playAnimP(id, "crouch_shoot_shotgun");
	else
		Player_playAnimP(id, "ref_shoot_shotgun");

	Player_playAnimV(id, 1); // shoot1
	set_pev(id, pev_punchangle, Float:{-1.9,0.0,0.0}); // punch V

	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		set_pev(ent, pev_classname, "stun_shot");
		
		engfunc(EngFunc_SetModel, ent, W_ARROW);
		engfunc(EngFunc_SetSize, ent, Float:{-1.5, -1.5, -1.5}, Float:{1.5, 1.5, 1.5});

		new vorigin[3], Float:voriginf[3];

		get_user_origin(id, vorigin, 1); // 1 - position from eyes (weapon aiming)
		IVecFVec(vorigin, voriginf);
		voriginf[2] -= 16.0;
		engfunc(EngFunc_SetOrigin, ent, voriginf);

		/*первый угол нужно умножить angle[1] *=-1
		просто когда например стреляешь вверх, то модель противоположно смотрит вниз*/
		pev(id, pev_angles, voriginf); // OPT: vangles
		//voriginf[0] = random_float(-180.0, 180.0);
		//voriginf[1] += -90.0;
		voriginf[2] += 180.0;
		set_pev(ent, pev_angles, voriginf);

		set_pev(ent, pev_solid, SOLID_BBOX);
		set_pev(ent, pev_scale, 1.0);
		set_pev(ent, pev_spawnflags, SF_SPRITE_STARTON);
		set_pev(ent, pev_framerate, 25.0);
		set_pev(ent, pev_movetype, MOVETYPE_FLY);
		set_pev(ent, pev_owner, id);

		velocity_by_aim(id, FLYSPEED_STUN_SHOT, voriginf);
		set_pev(ent, pev_velocity, voriginf);

		Create_TE_BEAMFOLLOW(ent, SPR_BULLET_TRAIL[1], 5, random_num(2,4), 172,194,245, 210);
		//Create_TE_BEAMFOLLOW(_, spr,                life, width,         r,  g,  b,   a  )
		
		//fm_set_rendering(ent, kRenderFxNone, 0, 0, 0, kRenderTransAdd, 0); // invisible model

		emit_sound(id, CHAN_VOICE, S_STUN_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// cast sound
		// TODO : shot sound (after cast + castline) [power_shot_shot.wav] @truel2
		emit_sound(ent, CHAN_ITEM, S_STUN_FLY, 0.4, ATTN_NORM, 0, PITCH_NORM);			// fly sound
		
		remove_task(id+TASK_SKILLS_CAST_STUN_SHOT);
		remove_task(id+TASK_SKILLS_USE_STUN_SHOT);

		set_task(COOLDOWN_STUN_SHOT, "cooltimeEnd", id+TASK_COOLTIME_END);

		// cooldown icons @ wait
		/*		
		new params[4];
		params[0] = -1;	// state
		params[1] = 1;	// skillid (1-4)
		params[2] = floatround(COOLDOWN_STUN_SHOT,floatround_round);
		params[3] = TASK_SKILLS_COOLDOWN_ICON_SET_1; // task skillid
		set_task(0.1, "taskCooldownIcon", id+params[3], params, 4);
		*/
	}
	
	PIN_Cast[id] = false;
}

public castStunShot(id) //taskid
{
	id -= TASK_SKILLS_CAST_STUN_SHOT;

	new origin[3], Float:forigin[3];
	pev(id, pev_origin, forigin);
	Util_getAimOriginDist(id, forigin, 10.0, false); //20.0
	FVecIVec(Float:forigin, origin);
	Create_TE_SPRITE(0, origin, SPR_STUN_CASTER, 1, random_num(10,100));
}

public Skills_hitStunShot(target) // Core's fwTouch (effect)
{
	new origin[3], Float:forigin[3];
	pev(target, pev_origin, forigin);
	Create_TE_EXPLOSION(forigin, SPR_STUNSHOT_HIT, 5, 10, TE_EXPLFLAG_NOSOUND);
	FVecIVec(Float:forigin, origin);
	Create_TE_SPRITE(0, origin, SPR_L2HIT, 5, random_num(150,255));
	//Create_TE_SPRITE(id=0, origin[3], sprite, scale, brightness)
	
	emit_sound(target, CHAN_VOICE, S_STUN_HIT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// hit sound
}

public Skills_makeStunned(target) // Core's fwTouch (actions)
{
	// NOTE: this method used in any fighter stuns
	
	if(!isBot(target))
		Create_ScreenFade(target, (1<<12), 0, FFADE_IN, 255,255,255,118, false); // little blind on hit

	Skills_inSleepCheck(target); // if already sleeped - remove Sleep 

	if(PIN_Stun[target]) // If already stunned - remove Stun
	{
		set_task(0.1, "removeStun", target+TASK_SKILLS_STUNNED);
	}
	else
	{
		// L2 Effect
		createL2Effect(target, SPR_STUN_EFFECT, 0.3, 255.0, 10.0, SKILLTIME_STUN_SHOT);

		lockCamera(target);
		blockMove(target);
		blockDuckJump(target);
		blockAttack(target);
		blockSkills(target);
		
		PIN_Stun[target] = true;
		
		//SKILLTIME_STUN_SHOT -- now this skilltime used for all stuns
		set_task(SKILLTIME_STUN_SHOT, "removeStun", target+TASK_SKILLS_STUNNED);
	}
}

public removeStun(target) //taskid
{
	target -= TASK_SKILLS_STUNNED;

	Create_TE_KILLPLAYERATTACH(target);

	unlockCamera(target);
	unblockMove(target);
	unblockDuckJump(target);
	unblockAttack(target);
	unblockSkills(target);

	remove_task(target+TASK_SKILLS_STUNNED);

	PIN_Stun[target] = false;
}

/**
 * Double Shot
 */
public altDouleShot(id)
{
	if(isValidUse(id, CLASS_A))
	{
		if(!isActiveWeaponBow(id)){
			return;
		}
		else if(get_gametime() - CD_DoubleShot[id] < COOLDOWN_DOUBLE_SHOT){
			return;
		}
		else
		{
			PIN_Cast[id] = true;
			
			Skills_castStart(id);
			CD_DoubleShot[id] = get_gametime();
			Create_BarTime(id, 1);
			Player_playAnimV(id, 4); // draw
			Create_TE_DLIGHT(id, 12, 157,157,151, 3, 0);
			emit_sound(id, CHAN_VOICE, S_POWER_SHOT_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // cast sound
			set_task(1.0, "useDoubleShot", id+TASK_SKILLS_USE_DOUBLE_SHOT);
			
			//set_task(0.1, "castDoubleShot", id+TASK_SKILLS_CAST_DOUBLE_SHOT, _, _, "b"); // TODO
			
			showSkillHud(id, "Use Double Shot");
		}
	}
}

public abortDoubleShot(id)
{
	Skills_castEnd(id);
	Create_BarTime(id, 0);

	if(task_exists(id+TASK_SKILLS_USE_DOUBLE_SHOT))
		CD_DoubleShot[id] = 0.0;
	remove_task(id+TASK_SKILLS_USE_DOUBLE_SHOT);

	//remove_task(id+TASK_SKILLS_CAST_DOUBLE_SHOT); // TODO

	PIN_Cast[id] = false;
}

public useDoubleShot(id)
{
	id -= TASK_SKILLS_USE_DOUBLE_SHOT;
	new params[1];
	params[0] = id;
	set_task(0.1, "makeDoubleShotA1", id+TASK_SKILLS_MAKE_DOUBLE_SHOT_A1, params, 1);
	set_task(0.3, "makeDoubleShotA1", id+TASK_SKILLS_MAKE_DOUBLE_SHOT_A2, params, 1);
	//remove_task(id+TASK_SKILLS_USE_DOUBLE_SHOT);

	PIN_Cast[id] = false;
}

public makeDoubleShotA1(params[1], taskid) // params[0] = player
{
	if(pev(params[0], pev_flags) & FL_DUCKING)
		Player_playAnimP(params[0], "crouch_shoot_shotgun");
	else
		Player_playAnimP(params[0], "ref_shoot_shotgun");

	Player_playAnimV(params[0], 1); // shoot1
	set_pev(params[0], pev_punchangle, Float:{-1.0,0.0,0.0}); // punch V

	makeDoubleShot(params[0]);
}

public makeDoubleShot(id)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		setEntClass(ent, "double_shot");
		setEntModel(ent, W_ARROW);
		setEntSize(ent, Float:{-1.5,-1.5,-1.5}, Float:{1.5,1.5,1.5});

		//new Float:vangles[3], Float:veloc[3];
		new vorigin[3], Float:voriginf[3];

		get_user_origin(id, vorigin, 1); // 1 - position from eyes (weapon aiming)
		IVecFVec(vorigin, voriginf);
		voriginf[2] -= 16.0;
		setEntOrigin(ent, voriginf);

		/*первый угол нужно умножить angle[1] *=-1
		просто когда например стреляешь вверх, то модель противоположно смотрит вниз*/
		pev(id, pev_angles, voriginf); // OPT: vangles
		//voriginf[0] = randFloat(-180.0, 180.0);
		//voriginf[1] += -90.0;
		voriginf[2] += 180.0;
		set_pev(ent, pev_angles, voriginf);

		set_pev(ent, pev_solid, SOLID_BBOX);
		set_pev(ent, pev_scale, 0.5); //0.3
		set_pev(ent, pev_spawnflags, SF_SPRITE_STARTON);
		set_pev(ent, pev_framerate, 25.0);
		set_pev(ent, pev_movetype, MOVETYPE_FLY);
		set_pev(ent, pev_owner, id);

		velocity_by_aim(id, FLYSPEED_STUN_SHOT, voriginf);	// OPT: veloc	// used : Stun Shot fly speed
		set_pev(ent, pev_velocity, voriginf);

		//new fx = random_num(0,16);
		//new mode = random_num(0,5);
		//new amt = random(255);

		//sendMessage(id, print_chat, "// renderamt: %d | renderfx: %d | rendermode: %d | te: %f", amt, fx, mode);
		//sendMessage(id, print_console, "// renderamt: %d | renderfx: %d | rendermode: %d | te: %f", amt, fx, mode);
		
		//fm_set_rendering(entity, fx=kRenderFxNone, r=255, g=255, b=255, render=kRenderNormal, amount=16)
		//fm_set_rendering(ent, fx, 157,179,221, mode, amt); //215

		Create_TE_BEAMFOLLOW(ent, SPR_BULLET_TRAIL[2], randInt(2,3), 1, 172, 194, 245, 210);
		//Create_TE_BEAMFOLLOW(entity, spr,            life,            width, red, green, blue, alpha)

		emit_sound(id, CHAN_VOICE, S_POWER_SHOT_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // shot sound
	}
}

public Skills_hitDoubleShot(target) // Core's fwTouch
{
	new origin[3], Float:forigin[3];
	pev(target, pev_origin, forigin);
	FVecIVec(Float:forigin, origin);
	Create_TE_SPRITE(0, origin, SPR_L2HIT, 5, random_num(150,255));
	emit_sound(target, CHAN_VOICE, S_POWER_SHOT_EXPLOTION, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// hit sound
}

/**
 * Lethal Shot
 */
public altLethalShot(id)
{
	if(isValidUse(id, CLASS_A))
	{
		if(!isActiveWeaponBow(id)){
			return;
		}
		else if(get_gametime() - CD_LethalShot[id] < COOLDOWN_LETHAL_SHOT){
			return;
		}
		else
		{
			PIN_Cast[id] = true;
			
			showSkillHud(id, "Use Lethal Shot");
			
	//		Skills_castStart(id);
			CD_LethalShot[id] = get_gametime();
			Create_BarTime(id, 1);

			// 	@truel2 : sounds == stunshot, BUT USE sounds == double shot  (1 shot)
			
			//emit_sound(id, CHAN_VOICE, S_STUN_CAST, 1.0, ATTN_NORM, 0, PITCH_NORM);
			emit_sound(id, CHAN_VOICE, S_POWER_SHOT_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // cast sound	// &copy Double Shot
			
			set_task(1.0, "useLethalShot", id+TASK_SKILLS_USE_LETHAL_SHOT);
			set_task(0.1, "castLethalShot", id+TASK_SKILLS_CAST_LETHAL_SHOT, _, _, "b");
		}
	}
}

public abortLethalShot(id)
{
//	Skills_castEnd(id);
	Create_BarTime(id, 0);

	if(task_exists(id+TASK_SKILLS_USE_LETHAL_SHOT))
		CD_LethalShot[id] = 0.0;
	
	remove_task(id+TASK_SKILLS_USE_LETHAL_SHOT);
	remove_task(id+TASK_SKILLS_CAST_LETHAL_SHOT);
	
	PIN_Cast[id] = false;
} 
 
public useLethalShot(id)
{
	id -= TASK_SKILLS_USE_LETHAL_SHOT;
	remove_task(id+TASK_SKILLS_CAST_LETHAL_SHOT); // bugfix: forced remove cast spr even btn still pressed

	// animations
	if(pev(id, pev_flags) & FL_DUCKING)
		Player_playAnimP(id, "crouch_shoot_shotgun");
	else
		Player_playAnimP(id, "ref_shoot_shotgun");

	Player_playAnimV(id, 1); // shoot1

	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		set_pev(ent, pev_classname, "lethal_shot");
		engfunc(EngFunc_SetModel, ent, W_ARROW);
		engfunc(EngFunc_SetSize, ent, Float:{-1.5, -1.5, -1.5}, Float:{1.5, 1.5, 1.5});
		
		new vorigin[3], Float:voriginf[3];
		get_user_origin(id, vorigin, 1); // 1 - position from eyes (weapon aiming)
		IVecFVec(vorigin, voriginf);
		voriginf[2] -= 16.0;
		engfunc(EngFunc_SetOrigin, ent, voriginf);

		/*первый угол нужно умножить angle[1] *=-1
		просто когда например стреляешь вверх, то модель противоположно смотрит вниз*/
		pev(id, pev_angles, voriginf); // OPT: vangles
		//voriginf[0] = random_float(-180.0, 180.0);
		//voriginf[1] += -90.0;
		voriginf[2] += 180.0;
		set_pev(ent, pev_angles, voriginf);
		
		set_pev(ent, pev_solid, SOLID_BBOX);
		set_pev(ent, pev_scale, 0.3);
		set_pev(ent, pev_spawnflags, SF_SPRITE_STARTON);
		set_pev(ent, pev_framerate, 25.0);
		set_pev(ent, pev_movetype, MOVETYPE_FLY);
		set_pev(ent, pev_owner, id);

		velocity_by_aim(id, FLYSPEED_LETHAL_SHOT, voriginf);	// OPT: veloc
		set_pev(ent, pev_velocity, voriginf);

		Create_TE_BEAMFOLLOW(ent, SPR_LETHALSHOT_TRAIL, random_num(2,3), random_num(2,4), 97,91,97,250);
		//Create_TE_BEAMFOLLOW(entity, spr,             life,            width,           r, g, b,  a)

		emit_sound(id, CHAN_VOICE, S_POWER_SHOT_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // shot sound
	}
	
	PIN_Cast[id] = false;
}

public castLethalShot(id)
{
	id -= TASK_SKILLS_CAST_LETHAL_SHOT;

	new origin[3], Float:forigin[3];
	pev(id, pev_origin, forigin);
	Util_getAimOriginDist(id, forigin, 20.0, false);
	FVecIVec(Float:forigin, origin);
	Create_TE_SPRITE(0, origin, SPR_LETHAL_SHOT, 1, random_num(50,100));
	//Create_TE_SPRITE(id=0, origin[3], sprite, scale, brightness)
}

public Skills_hitLethalShot(target) // Core's fwTouch
{
	new origin[3], Float:forigin[3];
	pev(target, pev_origin, forigin);
	FVecIVec(Float:forigin, origin);
	Create_TE_SPRITE(0, origin, SPR_LETHAL_SHOT, 3, random_num(100,150));
	//Create_TE_SPRITE(id=0, origin[3], sprite, scale, brightness)
	emit_sound(target, CHAN_VOICE, S_STUN_HIT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// hit sound	// &copy from StunShot
}

public Skills_successLethalShot(id)
{
	fm_set_user_rendering(id, kRenderFxDistort, 0, 0, 0, kRenderTransAdd, 150);
	set_task(1.5, "resetLethalShotEffects", id+TASK_SKILLS_SUCCESS_LETHAL_SHOT);
}

public resetLethalShotEffects(id)
{
	id -= TASK_SKILLS_SUCCESS_LETHAL_SHOT;
	fm_set_user_rendering(id);
}

/**
 * Snipe
 */
public useSnipe(id)
{
	if(isValidUse(id, CLASS_A))
	{
		if(!isActiveWeaponBow(id)){
			return PLUGIN_HANDLED;
		}
		else if(get_gametime() - CD_Snipe[id] < COOLDOWN_SNIPE){
			Util_shortage(id);
			return PLUGIN_HANDLED;
		}
		else if(!PIN_Snipe[id])
		{
			CD_Snipe[id] = get_gametime();
			Skills_castStart(id);

			Create_TE_DLIGHT(id, 11, 198,165,105, 4, 0); //life=2
			//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)
			
			Player_playAnimP(id, "ref_shoot_shieldgren");
			
			// L2Effect
			new origin[3], Float:forigin[3];
			pev(id, pev_origin, forigin);
			Util_getAimOriginDist(id, forigin, 20.0, false);
			FVecIVec(Float:forigin, origin);
			Create_TE_SPRITE(0, origin, SPR_RAPID_SHOT, 6, 255); //prev: scale=7

			emit_sound(id, CHAN_AUTO, S_DASH_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // cast skill

			set_task(CASTTIME_SNIPE, "endCastSnipe", id+TASK_SKILLS_END_CAST_SNIPE);
			set_task(SKILLTIME_SNIPE, "endEffectSnipe", id+TASK_SKILLS_END_EFFECT_SNIPE);
			
//			set_task(0.1, "castSnipe", id+TASK_SKILLS_CAST_SNIPE, _, _, "b");
			
			showSkillHud(id, "Use Snipe");
		}
		else
			showSkillHud(id, "Already in Snipe");
	}

	return PLUGIN_HANDLED;
}

/*public castSnipe(id)
{
	id -= TASK_SKILLS_CAST_SNIPE;

	new Float:origin[3], Float:axis[3];
	pev(id, pev_origin, origin);
	axis = origin;
	origin[2] -= 30.0;
	axis[2] += 60.0; //140.0
	Create_TE_BEAMCYLINDER(origin, axis, SPR_SHOCKWAVE, 0, 0, 3, 4, 1, 254,200,159,200, 0); //width=4
	//Create_TE_BEAMCYLINDER(coord[3], axis[3], spr, _, _, life, width, noise, r,g,b,alpha, speed)
}*/

public endCastSnipe(id)
{
	id -= TASK_SKILLS_END_CAST_SNIPE;
	
	Player_playAnimV(id, 3); //deploy
	Player_playAnimP(id, "ref_shoot_knife");
	set_pev(id, pev_punchangle, Float:{-1.0,0.0,0.0}); // punch V

	emit_sound(id, CHAN_AUTO, S_RAPID_SHOT_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // shot skill (not @truel2, but used)
	client_cmd(id, "spk %s", S_MDELF_2H_S_POLE_ATK_1);
	
	Skills_castEnd(id);
/*
	hideBuffAura(id);
	
	new Float:origin[3], Float:axis[3];
	pev(id, pev_origin, origin);
	axis = origin;
	axis[2] += 40.0; //+=140.0
	Create_TE_BEAMCYLINDER(origin, axis, SPR_SHOCKWAVE, 0, 0, 3, 25, 1, 117,128,107,100, 0); //width=25
	axis[2] += 30.0; //+=50.0
	Create_TE_BEAMCYLINDER(origin, axis, SPR_SHOCKWAVE, 0, 0, 3, 35, 1, 207,251,243,100, 0); //width=35
	axis[2] += 20.0; //-=20.0
	Create_TE_BEAMCYLINDER(origin, axis, SPR_SHOCKWAVE, 0, 0, 3, 45, 1, 59,100,133,100, 0); //width=45
	//Create_TE_BEAMCYLINDER(coord[3], axis[3], spr, _, _, life, width, noise, r,g,b,alpha, speed)
*/
	blockMove(id);
	PIN_Snipe[id] = true;

//	remove_task(id+TASK_SKILLS_CAST_SNIPE);
	remove_task(id+TASK_SKILLS_END_CAST_SNIPE);
}

public endEffectSnipe(id)
{
	id -= TASK_SKILLS_END_EFFECT_SNIPE;
	
	unblockMove(id);
	PIN_Snipe[id] = false;
	
	showSkillHud(id, "Snipe removed");

	remove_task(id+TASK_SKILLS_END_EFFECT_SNIPE);
}

//=======================================================================
//	2	CLASS_B
//=======================================================================
/**
 * Dash
 */
public useDash(id)
{
	if(isValidUse(id, CLASS_B))
	{
		if(get_gametime() - CD_Dash[id] < COOLDOWN_DASH){
			Util_shortage(id);
		}
		else if(PIN_Dash[id]){
			showSkillHud(id, "Already in effect");
		}
		else{
			showSkillHud(id, "Use Dash");

			CD_Dash[id] = get_gametime();

			createL2Effect(id, SPR_DASH_CAST, 0.4 /*L_good:0.5*/, 200.0, 18.0, 0.7); //0.8
			
			set_task(0.2, "castDash", id+TASK_SKILLS_USE_DASH);
			set_task(COOLDOWN_DASH, "cooltimeEnd", id+TASK_COOLTIME_END);
		}
	}

	return PLUGIN_HANDLED;
}

public castDash(id)
{
	id -= TASK_SKILLS_USE_DASH;
	
	Create_TE_DLIGHT(id, 11, 198,165,105, 5, 33);
	//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)

	Player_playAnimV(id, 7); // midslash2
	Player_playAnimP(id, "ref_shoot_knife");
	set_pev(id, pev_punchangle, Float:{0.0,1.7,0.0}); // punch V	

	emit_sound(id, CHAN_AUTO, S_DASH_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	if(random_num(1,2)==1)
		client_cmd(id, "spk %s", S_MHFIGHTER_2H_ATK_4);
	
//	set_task(0.1, "getDashFade", id+TASK_SKILLS_FADE_DASH);

	set_task(SKILLTIME_DASH, "removeDash", id+TASK_SKILLS_END_DASH);
	
	PIN_Dash[id] = true;
	
	new params[2]; params[0] = id; params[1] = floatround(SKILLTIME_DASH);
	set_task(0.1, "timeDash", id+TASK_SKILLS_DASH_TIME, params, 2);
}

public timeDash(params[2], taskid)
{
	showSkillHud(params[0], "Dash: %d sec", params[1]);
	if(params[1] > 1){
		params[1]--;
		set_task(1.0, "timeDash", taskid, params, 2);
	}
}

public removeDash(id)
{
	id -= TASK_SKILLS_END_DASH;

	PIN_Dash[id] = false;
	showSkillHud(id, "Dash effect removed");

	//remove_task(id+TASK_SKILLS_END_DASH);
	//remove_task(id+TASK_SKILLS_FADE_DASH);
}

stock getDashFade(id)
{
	id -= TASK_SKILLS_FADE_DASH;
	// 2^12*1=4096  -->  1<<12
	Create_ScreenFade(id, 4096, floatround(4096*SKILLTIME_DASH, floatround_round), FFADE_IN, 0, 50, 200, 30, true);	//50, true
}

/**
 * Bluff 
 */
public useBluff(id)
{
	if(isValidUse(id, CLASS_B))
	{
		if(get_gametime() - CD_Bluff[id] < COOLDOWN_BLUFF){
			Util_shortage(id);
		}
		else{
			new target, body;
			get_user_aiming(id, target, body, SKILLRANGE_BLUFF); //Float: get_user_aiming ( index, &id, &body, [ distance = 9999 ] )
			
			if(isPlayerValid(target))
			{
				if(!(isTeamGameMode() && getTeam(id) == getTeam(target)) || (getTeam(id) != getTeam(target)))
				{
					CD_Bluff[id] = get_gametime();
					
					emit_sound(id, CHAN_VOICE, S_BLUFF_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
					if(random_num(1,2)==1)
						client_cmd(id, "spk %s", S_MHFIGHTER_2H_ATK_4);
					
					// L2 Effect
					createL2Effect(target, SPR_BLUFF, 0.5, 230.0, 16.0, 0.6);
					
					new Float:forigin[3];
					pev(target, pev_origin, forigin);
					Create_TE_EXPLOSION(forigin, SPR_STUNSHOT_HIT, 9, 10, TE_EXPLFLAG_NOSOUND);
					
					// Turn player back (w/o chance)
					new Float:fAnglesVictim[3], Float:fAnglesAtt[3];
					pev(target, pev_v_angle, fAnglesVictim);
					pev(id, pev_v_angle, fAnglesAtt);
					fAnglesVictim[1] = fAnglesAtt[1];
					set_pev(target, pev_angles, fAnglesVictim);
					set_pev(target, pev_v_angle, fAnglesVictim);
					set_pev(target, pev_fixangle, 1);
					
					// Chance to stun
					if(random_num(0,100) <= SKILLCHANCE_BLUFF){
						Skills_makeStunned(target);
					}
					
					set_task(0.2, "castBluff", target+TASK_SKILLS_CAST_BLUFF);
				}
				else{
					showSkillHud(id, "Can't be used on ally");
					soundSpeak(id,SYS_IMPOSSIBLE);
				}
			}
			else
				showSkillHud(id, "Invalid target");
		}
	}

	return PLUGIN_HANDLED;
}

public castBluff(id)
{
	id -= TASK_SKILLS_CAST_BLUFF;
	emit_sound(id, CHAN_VOICE, S_BLUFF_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
}

/**
 * Deadly Blow
 */
public useDeadlyBlow(id)
{
	if(isValidUse(id, CLASS_B))
	{
		if(get_gametime() - CD_DeadlyBlow[id] < COOLDOWN_DEADLY_BLOW){
			Util_shortage(id);
		}
		else
		{
			CD_DeadlyBlow[id] = get_gametime();

			Player_playAnimV(id, 5); //stab_miss
			Player_playAnimP(id, "ref_shoot_knife");
			
			// prev -- not bad
			/*new Float:how_much[3];
			how_much[0] = random_float(1.0,1.5);
			how_much[1] = random_float(1.0,1.5);
			how_much[2] = random_float(0.0,0.0);*/
			set_pev(id, pev_punchangle, Float:{2.6,2.6,0.0}); // punch V

			createL2Effect(id, SPR_DEADLY_BLOW_HIT, 0.5 /*0.6*/, 240.0, 16.0, 0.5);
			//createL2Effect(id, effect, size, opacity, framerate, showtime)

			// Step 1: push
			new coord[3], aiming[3];
			get_user_origin(id, coord);
			get_user_origin(id, aiming, 3);

			new Float:origin[3];
			new Float:len = floatabs(floatsqroot(float((aiming[0]-coord[0])*(aiming[0]-coord[0])+(aiming[1]-coord[1])*(aiming[1]-coord[1])+(aiming[2]-coord[2])*(aiming[2]-coord[2]))))/10;

			for(new i=0; i<3; i++){
				origin[i] = float(aiming[i] - coord[i]) / len;
			}

			new Float:vel[3];
			vel[0] = origin[0] * 40.0;	// add more pumping
			vel[1] = origin[1] * 40.0;
			vel[2] = 50.0;

			set_pev(id, pev_velocity, vel);
			
			// Step 2: use blow effect
			pev(id, pev_origin, origin); // OPT: origin
			FVecIVec(Float:origin, coord); // OPT: coord

			// use effects
			Create_TE_BEAMFOLLOW(id, SPR_DEADLYBLOW_TRAIL, 4, 18, 255,255,200, 200); //width=20
			//Create_TE_BEAMFOLLOW(entity, spr,         life, width, red,green,blue, alpha)

			//entAddDeadlyBlow(id);
			//Create_TE_SPRITE(0, coord, SPR_DEADLYBLOW_HIT2, 10, random_num(150,255));
			//Create_TE_SPRITE(id=0, origin[3], sprite, scale, brightness)
			
			// Step 3: finding a target
			new Float:viewofs[3];
			pev(id, pev_view_ofs, viewofs);
			viewofs[0] += origin[0];
			viewofs[1] += origin[1];
			viewofs[2] += origin[2];
			new Float:vangle[3];
			pev(id, pev_v_angle, vangle);
			angle_vector(vangle, ANGLEVECTOR_FORWARD, vangle);
			new Float:fov = floatcos(float(pev(id, pev_fov) / 2), degrees) * MULTIPLY;
			
			new ent = -1;
			while((ent = engfunc(EngFunc_FindEntityInSphere, ent, origin, SKILLRANGE_DEADLY_BLOW)) != 0)
			{
				//if(ent == id || /*!is_user_valid(ent) ||*/ !isAlive(ent))
				//	continue;

				new Float:e_origin[3];
				pev(ent, pev_origin, e_origin);
				e_origin[0] -= viewofs[0];
				e_origin[1] -= viewofs[1];
				e_origin[2] -= viewofs[2];

				if(DotProduct(e_origin, vangle) < fov)
					continue;
				
				static this[32];
				pev(ent, pev_classname, this, 31);

				if(isPlayer(this) && isPlayerValid(ent))
				{
					// Make effects
					//if(pev_valid(ent))
					hitDeadlyBlow(id, ent);

					// Make damage
					if(!(isTeamGameMode() && getTeam(id) == getTeam(ent)) || getTeam(id) != getTeam(ent))
					{
						// chance to instant kill
						if(randomChance(SKILLCHANCE_DEADLY_BLOW_KILL)){
							takeDamage(id, ent, INFINITE);
							showSkillHud(id, "Over-hit!");
						}
						else{
							takeDamage(id, ent, SKILLDAMAGE_DEADLY_BLOW);
							showSkillHud(id, "You hit for %d damage", floatround(SKILLDAMAGE_DEADLY_BLOW));
						}
					}

					break;
				}
				else if(isAvanpost(this) || isGuard(this))
				{
					if((equali(this, "L2Avanpost@Red") && getTeam(id) == 2) || (equali(this, "L2Avanpost@Blu") && getTeam(id) == 1)
					|| (equali(this, "L2Guard@East") && getTeam(id) == 2) || (equali(this, "L2Guard@West") && getTeam(id) == 1))
					{
						hitDeadlyBlow(id, ent);
						takeDamage(id, ent, SKILLDAMAGE_DEADLY_BLOW);
						break;
					}
				}
				else{
					showSkillHud(id, "Attack failed");
					break;
				}
			}

			soundPlay(id, S_MORTAL_BLOW_CAST); // cast skill
			if(randInt(1,2) == 1)
				soundSpeak(id, S_MHFIGHTER_POLE_ATK_1);

			set_task(0.4, "effsDeadlyBlow", id+TASK_SKILLS_EFFS_DEADLY_BLOW);
		}
	}

	return PLUGIN_HANDLED;
}

public effsDeadlyBlow(id)
{
	id -= TASK_SKILLS_EFFS_DEADLY_BLOW;
	Create_TE_KILLBEAM(id);
}

public hitDeadlyBlow(attacker, target)
{
	new origin[3], Float:forigin[3];
	if(isAlive(target))
	{ // if enemy is another player
		pev(target, pev_origin, forigin);
		FVecIVec(Float:forigin, origin);
	}else{ // avanpost, etc
		Util_getAimOriginDist(attacker, forigin, 40.0, false);
		FVecIVec(Float:forigin, origin);
	}
	Create_TE_SPRITE(0, origin, SPR_L2HIT, 5, randNum(150,255));
	soundPlay(target, S_MORTAL_BLOW_SHOT); // shot skill
	
	new Float:vec[3];
	entity_get_vector(target, EV_VEC_velocity, vec);
	Create_TE_STREAK_SPLASH(forigin, vec, 248, randInt(10,16) /*count*/, 51 /*speed*/, 226 /*rand_veloc*/);
}

/**
 * Backstab
 */
public useBackstab(id)
{
	if(isValidUse(id, CLASS_B))
	{
		if(get_gametime() - CD_Backstab[id] < COOLDOWN_BACKSTAB){
			Util_shortage(id);
		}
		else
		{
			CD_Backstab[id] = get_gametime();
			Skills_castStart(id);

			Player_playAnimV(id, 1); //slash1
			Player_playAnimP(id, "ref_shoot_knife");
			set_pev(id, pev_punchangle, Flaot:{0.0,4.0,1.0}); // punch V

			createL2Effect(id, SPR_BACKSTAB_CAST, 0.5, 200.0, 17.0, 1.1);
			//createL2Effect(id, effect, size, opacity, framerate, showtime)

			// Finding a target
			new Float:viewofs[3], Float:origin[3];
			Util_getAimOriginDist(id, origin, 15.0, false); // overwrite
			pev(id, pev_view_ofs, viewofs);
			viewofs[0] += origin[0];
			viewofs[1] += origin[1];
			viewofs[2] += origin[2];
			new Float:vangle[3];
			pev(id, pev_v_angle, vangle);
			angle_vector(vangle, ANGLEVECTOR_FORWARD, vangle);
			new Float:fov = floatcos(float(pev(id, pev_fov) / 2), degrees) * MULTIPLY;
			
			new ent = -1;
			while((ent = engfunc(EngFunc_FindEntityInSphere, ent, origin, SKILLRANGE_BACKSTAB)) != 0)
			{
				new Float:e_origin[3];
				pev(ent, pev_origin, e_origin);
				e_origin[0] -= viewofs[0];
				e_origin[1] -= viewofs[1];
				e_origin[2] -= viewofs[2];

				if(DotProduct(e_origin, vangle) < fov)
					continue;
				
				static this[32];
				pev(ent, pev_classname, this, 31);

				if(isPlayer(this) && isAlive(ent) /*&& haveTeam(ent)*/)
				{
					// Make effects and use sound
					hitBackstab(id, ent);

					// Make damage
					if(!(isTeamGameMode() && getTeam(id) == getTeam(ent)) || getTeam(id) != getTeam(ent))
					{
						//http://amx-x.ru/viewtopic.php?f=8&p=62852#p62852	Как отловить урон в спину
						
						new Float:vecSrc[3], Float:vecAngles[3], Float:vecForward[3], Float:vecAttackDir[3];
						
						GetCenter(ent, vecSrc);			// <Util>
						GetCenter(id, vecAttackDir);	// <Util>

						xs_vec_sub(vecAttackDir, vecSrc, vecAttackDir);
						xs_vec_normalize(vecAttackDir, vecAttackDir);

						pev(ent, pev_angles, vecAngles);
						engfunc(EngFunc_MakeVectors, vecAngles);

						global_get(glb_v_forward, vecForward);
						xs_vec_mul_scalar(vecAttackDir, -1.0, vecAttackDir);

						// behind: 100% damage, default: 50% damage
						new Float:resdmg = (xs_vec_dot(vecForward, vecAttackDir) > 0.3) ? SKILLDAMAGE_BACKSTAB : SKILLDAMAGE_BACKSTAB / 2.0;
						takeDamage(id, ent, resdmg);
						showSkillHud(id, "You hit for %d damage", floatround(resdmg));
					}

					break;
				}
				else if(isAvanpost(this) || isGuard(this))
				{
					if((equali(this, "L2Avanpost@Red") && getTeam(id) == 2) || (equali(this, "L2Avanpost@Blu") && getTeam(id) == 1)
					|| (equali(this, "L2Guard@East") && getTeam(id) == 2) || (equali(this, "L2Guard@West") && getTeam(id) == 1))
					{
						hitBackstab(id, ent);
						takeDamage(id, ent, SKILLDAMAGE_BACKSTAB/2.0);
						showSkillHud(id, "You hit for %d damage", floatround(SKILLDAMAGE_BACKSTAB/2.0));
						break;
					}
				}
				else{
					showSkillHud(id, "Attack failed");
					break;
				}
			}

			soundPlay(id, S_FATAL_STRIKE_CAST); // cast skill
			if(rand(1,2) == 1)
				soundSpeak(id, S_MHFIGHTER_PREATK_1);

			set_task(0.4, "castBackstab", id+TASK_SKILLS_CAST_BACKSTAB);
		}
	}

	return PLUGIN_HANDLED;
}

public castBackstab(id)
{
	id -= TASK_SKILLS_CAST_BACKSTAB;
	Skills_castEnd(id);
	remove_task(id+TASK_SKILLS_CAST_BACKSTAB);
}

public hitBackstab(attacker, target)
{
	// THINK: new eff on hit?
	/*
	new origin[3], Float:forigin[3];
	if(isAlive(target))
	{ // if enemy is another player
		pev(target, pev_origin, forigin);
		FVecIVec(Float:forigin, origin);
	}else{ // avanpost, etc
		Util_getAimOriginDist(attacker, forigin, 40.0, false);
		FVecIVec(Float:forigin, origin);
	}
	Create_TE_SPRITE(0, origin, SPR_L2HIT, 5, random_num(150,255));
	*/

	createL2Effect(target, SPR_BACKSTAB_HIT, 0.7, 195.0, 15.0, 0.34 /*0.4*/);
	//createL2Effect(id, effect, size, opacity, framerate, showtime)

	soundPlay(attacker, S_MHFIGHTER_1H_ATK_4); // shot char

	new Float:forigin[3];
	pev(target, pev_origin, forigin);
	new Float:vec[3];
	entity_get_vector(target, EV_VEC_velocity, vec);
	Create_TE_STREAK_SPLASH(forigin, vec, 248, randInt(10,16) /*count*/, 51 /*speed*/, 226 /*rand_veloc*/);

	soundPlay(target, S_FATAL_STRIKE_SHOT); // shot skill
}

//=======================================================================
//	3	CLASS_C
//=======================================================================
/**
 * Fist Fury 
 */
public useFistFury(id)
{
	if(isValidUse(id, CLASS_C))
	{
		if(!PIN_FistFury[id])
		{
			PIN_FistFury[id] = true;
			set_task(0.4, "toggleFistFury", id+TASK_SKILLS_TOGGLE_FIST_FURY, _, _, "b");
			showSkillHud(id, "Fist Fury toggled"); /* You use Fist Fury */
		}else{
			PIN_FistFury[id] = false;
			remove_task(id+TASK_SKILLS_TOGGLE_FIST_FURY);
			showSkillHud(id, "Fist Fury aborted"); /* Fist Fury has been aborted. */
		}
	}

	return PLUGIN_HANDLED;
}

public toggleFistFury(id)
{
	id -= TASK_SKILLS_TOGGLE_FIST_FURY;

	new HP = getHp(id);
	if(HP < 2){
		PIN_FistFury[id] = false;
		remove_task(id+TASK_SKILLS_TOGGLE_FIST_FURY);
		showSkillHud(id, "Fist Fury aborted");
	}
	else
		setHp(id, HP-1);
}

/**
 * Force Blaster (25/11/2014)
 */
public useForceBlaster(id)
{
	if(isValidUse(id, CLASS_C))
	{
		if(get_gametime() - CD_ForceBlaster[id] < COOLDOWN_FORCE_BLASTER){
			Util_shortage(id);
		}
		else if(FocusedForces[id] < 2){
			showSkillHud(id, "Not enough charges");
		}
		else{
			// TODO: check not teammate
			new target, body; get_user_aiming(id, target, body, SKILLRANGE_FORCE_BLASTER);
			if(isPlayerValid(target))
			{
				Skills_castStart(id);
				showSkillHud(id, "Use Force Blaster");
				CD_ForceBlaster[id] = get_gametime();

				Player_playAnimV(id, 7); //midslash2
				Player_playAnimP(id, "ref_shoot_grenade");
				set_pev(id, pev_punchangle, Float:{0.0,-1.9,1.6}); // punch V
				
				// &copy Backstab
				createL2Effect(id, SPR_BACKSTAB_CAST, 0.5, 200.0, 17.0, 1.1);
				//createL2Effect(id, effect, size, opacity, framerate, showtime)

				soundPlay(id, S_FATAL_STRIKE_CAST);

				ForceBlasterTarget[id] = target;

				switch(FocusedForces[id])
				{
					case 2: { ForceBlasterDamage[id] = randFloat(14.0,18.0); FocusedForces[id] -= 2; updateFocusedForces(id,2); }
					case 3: { ForceBlasterDamage[id] = randFloat(18.0,22.0); FocusedForces[id] -= 3; updateFocusedForces(id,3); }
					case 4: { ForceBlasterDamage[id] = randFloat(22.0,26.0); FocusedForces[id] -= 4; updateFocusedForces(id,4); }
					case 5: { ForceBlasterDamage[id] = randFloat(26.0,30.0); FocusedForces[id] -= 5; updateFocusedForces(id,5); }
					case 6: { ForceBlasterDamage[id] = randFloat(34.0,38.0); FocusedForces[id] -= 6; updateFocusedForces(id,6); }
					case 7: { ForceBlasterDamage[id] = randFloat(38.0,42.0); FocusedForces[id] -= 7; updateFocusedForces(id,7); }
				}

				new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
				if(ent)
				{
					set_pev(ent, pev_classname, "L2Effect@ForceBlaster");
					engfunc(EngFunc_SetModel, ent, SPR_FORCE_BLASTER);
					engfunc(EngFunc_SetSize, ent, Float:{-1.0, -1.0, -1.0}, Float:{1.0, 1.0, 1.0});
					
					new Float:origin[3];
					pev(id, pev_origin, origin);
					engfunc(EngFunc_SetOrigin, ent, origin);
					//new Float:vangle[3];
					pev(id, pev_v_angle, origin); // OPT: vangle
					origin[0] = randFloat(-180.0, 180.0); // test
					origin[1] = randFloat(-180.0, 180.0);
					set_pev(ent, pev_v_angle, origin);
					
					set_pev(ent, pev_solid, SOLID_NOT);
					set_pev(ent, pev_rendermode, 5); // 5 - Additive
					set_pev(ent, pev_renderamt, 230.0); //110.0
					set_pev(ent, pev_renderfx, 16); // 16 - Hologram (Distort + fade)

					set_pev(ent, pev_scale, 0.3);
					set_pev(ent, pev_movetype, MOVETYPE_FLY);
					set_pev(ent, pev_owner, id);

					//new Float:velocity[3];
					velocity_by_aim(id, FLYSPEED_FORCE_BLASTER, origin); // OPT: velocity
					set_pev(ent, pev_velocity, origin);

					//&copy Death Spike
					Create_TE_BEAMFOLLOW(ent, SPR_A3MAGIC, 5, 13, 255, 255, 255, 255);
					Create_TE_BEAMFOLLOW(ent, SPR_FX_M_T0084, 6, 9, 170, 170, 172, 255);
					//Create_TE_BEAMFOLLOW(ent, SPR_EGONBEAM, random_num(3,10), 6, 170, 170, 172, random_num(15,50)); // from Hurricane
					//Create_TE_BEAMFOLLOW(entity, spr, life, width, red, green, blue, alpha)
					
					emit_sound(id, CHAN_VOICE, S_WIND_STRIKE_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
					
					set_pev(ent, pev_nextthink, get_gametime() + 0.01);					
				}
				
				Skills_castEnd(id);
			}
			else
				showSkillHud(id, "Invalid target");
		}
	}
	
	return PLUGIN_HANDLED;
}

public Skills_thinkForceBlaster(ent) // Core.inl
{
	if(!pev_valid(ent) || pev(ent, pev_health) <= 0.0)
	{
		Log(WARN, "Skills", "thinkForceBlaster(): entity (ID=%d) not removed", ent);
		return;
	}
	else
	{
		new id = pev(ent, pev_owner);
		new target = ForceBlasterTarget[id];
		
		if(target && isPlayerValid(target))
		{
			Log(DEBUG, "Skills", "thinkForceBlaster(): if(target && isPlayerValid(target))");
			
			new Float:origin_t[3], Float:origin_e[3];
			pev(target, pev_origin, origin_t);
			pev(ent, pev_origin, origin_e);
			
			if(get_distance_f(origin_t, origin_e) > 5)
			{
				Log(DEBUG, "Skills", "thinkForceBlaster(): if(get_distance_f(origin_t, origin_e) > 5)");
				
				new Float:velocity[3];
				get_speed_vector(origin_e, origin_t, 950.0 /*speed*/, velocity);
				set_pev(ent, pev_velocity, velocity);
				set_pev(ent, pev_nextthink, get_gametime() + 0.01);
			}
			else if(get_distance_f(origin_t, origin_e) <= 5)
			{
				Log(DEBUG, "Skills", "thinkForceBlaster(): else if(get_distance_f(origin_t, origin_e) <= 5)");
				
				Skills_hitHurricane(ent, target); //&copy Death Spike
				
				set_pev(ent, pev_health, 0.0);
				entityDispose(ent);
				ForceBlasterTarget[id] = 0;
				
				takeDamage(id, target, ForceBlasterDamage[id]);
				ForceBlasterDamage[id] = 0.0;
			}
			else{
				entityDispose(ent);
			}
		}
		else{
			entityDispose(ent);
		}
	}
}

/**
 * Focused Force
 */
public useFocusedForce(id)
{
	if(isValidUse(id, CLASS_C))
	{
		if(get_gametime() - CD_FocusedForce[id] < COOLDOWN_FOCUSED_FORCE){
			Util_shortage(id);	
		}
		else
		{
			if(FocusedForces[id] == 7){
				showSkillHud(id, "Reached maximum force"); /* Your force has reached maximum capacity. */
				//return;
			}
			else if(FocusedForces[id] <= 6)
			{
				CD_FocusedForce[id] = get_gametime();
				Skills_castStart(id);

				Player_playAnimV(id, 6); //midslash1
				Player_playAnimP(id, "ref_shoot_grenade");
				set_pev(id, pev_punchangle, Float:{0.0,1.9,1.6}); // punch V

				soundPlay(id, S_MOFIGHTER_2H_S);
				// @l2 : war_cry_cast.wav (really not need to use it?!)

				Create_TE_DLIGHT(id, 7, 165,165,0, 2, 0);

				// L2 Effect
				createL2Effect(id, SPR_FOCUSED_FORCE_SHOT, 0.4, 150.0, 13.0, 0.5);

				new LocalHud_1[33], LocalHud_2[33];

				if(FocusedForces[id] > 0){
					switch(FocusedForces[id]) // before
					{
						case 1: format(LocalHud_1, 32, "d_deagle");
						case 2: format(LocalHud_1, 32, "d_ak47");
						case 3: format(LocalHud_1, 32, "d_m4a1");
						case 4: format(LocalHud_1, 32, "d_awp");
						case 5: format(LocalHud_1, 32, "d_elite");
						case 6: format(LocalHud_1, 32, "d_mac10");
						case 7: format(LocalHud_1, 32, "d_aug");
					}
					Create_StatusIcon(id, ICON_HIDE, LocalHud_1, 0,0,0); //@l2 colors : 249, 181, 65
				}

				FocusedForces[id]++;

				switch(FocusedForces[id]) // after
				{
					case 1: format(LocalHud_2, 32, "d_deagle");
					case 2: format(LocalHud_2, 32, "d_ak47");
					case 3: format(LocalHud_2, 32, "d_m4a1");
					case 4: format(LocalHud_2, 32, "d_awp");
					case 5: format(LocalHud_2, 32, "d_elite");
					case 6: format(LocalHud_2, 32, "d_mac10");
					case 7: format(LocalHud_2, 32, "d_aug");
				}

				Create_StatusIcon(id, ICON_SHOW, LocalHud_2, 255,255,255);

				set_task(0.4, "castFocusedForce", id+TASK_SKILLS_CAST_FOCUSED_FORCE);
			}
		}
	}

	return PLUGIN_HANDLED;
}

public castFocusedForce(id)
{
	id -= TASK_SKILLS_CAST_FOCUSED_FORCE;
	showSkillHud(id, "Force has increased to %d", FocusedForces[id]); /* Your force has increased to %d level */
	Skills_castEnd(id);
}

public updateFocusedForces(id, reduce) // used in Soul Breaker
{	
	new LocalHud_1[33], LocalHud_2[33];
	
	//if(FocusedForces[id] <= 0){
	//	Create_StatusIcon(id, ICON_HIDE, "d_m4a1", 0,0,0);
	//}
	//else{
	switch(FocusedForces[id] + reduce){ // before HIDE
		case 1: format(LocalHud_1, 32, "d_deagle");
		case 2: format(LocalHud_1, 32, "d_ak47");
		case 3: format(LocalHud_1, 32, "d_m4a1");
		case 4: format(LocalHud_1, 32, "d_awp");
		case 5: format(LocalHud_1, 32, "d_elite");
		case 6: format(LocalHud_1, 32, "d_mac10");
		case 7: format(LocalHud_1, 32, "d_aug");
	}
	Create_StatusIcon(id, ICON_HIDE, LocalHud_1, 0,0,0);
	
	switch(FocusedForces[id]){ // after SHOW
		case 1: format(LocalHud_2, 32, "d_deagle");
		case 2: format(LocalHud_2, 32, "d_ak47");
		case 3: format(LocalHud_2, 32, "d_m4a1");
		case 4: format(LocalHud_2, 32, "d_awp");
		case 5: format(LocalHud_2, 32, "d_elite");
		case 6: format(LocalHud_2, 32, "d_mac10");
		case 7: format(LocalHud_2, 32, "d_aug");
	}
	Create_StatusIcon(id, ICON_SHOW, LocalHud_2, 255,255,255);
	//}
}

/**
 * Soul Breaker
 */
public useSoulBreaker(id)
{
	if(isValidUse(id, CLASS_C))
	{
		if(get_gametime() - CD_SoulBreaker[id] < COOLDOWN_SOUL_BREAKER){
			Util_shortage(id);
		}
		else
		{
			if(FocusedForces[id] >= 3)
			{
				new target, body; get_user_aiming(id, target, body, SKILLRANGE_SOUL_BREAKER);

				if(isPlayerValid(target))
				{
					if(!(isTeamGameMode() && getTeam(id) == getTeam(target)) || (getTeam(id) != getTeam(target)))
					{
						CD_SoulBreaker[id] = get_gametime();
						
						Player_playAnimV(id, 7); //midslash2
						Player_playAnimP(id, "ref_shoot_grenade");
						set_pev(id, pev_punchangle, Float:{-0.2,-3.9,0.6}); // punch V

						// &copy Backstab
						createL2Effect(id, SPR_BACKSTAB_CAST, 0.5, 200.0, 17.0, 1.0/*1.1*/);
						//createL2Effect(id, effect, size, opacity, framerate, showtime)

						// Claws effect
						new origin[3], Float:forigin[3];
						Util_getAimOriginDist(id, forigin, 30.0, false);
						forigin[2] += 5.0;
						FVecIVec(Float:forigin, origin);
						Create_TE_SPRITE(0, origin, SPR_QUADRUPLE_THROW, 1, 255);
						
						// Streaks eff
						new Float:vec[3];
						pev(target, pev_origin, forigin);
						entity_get_vector(target, EV_VEC_velocity, vec);
						Create_TE_STREAK_SPLASH(forigin, vec, 90, 3, 234, 234);

						soundPlay(id, S_MOFIGHTER_2H_ATK_5); // char shot

						new Float:damage = 10.0; // to const
						switch(FocusedForces[id])
						{
							case 3: { damage += 2.0;	FocusedForces[id] -= 3;	updateFocusedForces(id,3); }
							case 4: { damage += 4.0;	FocusedForces[id] -= 4;	updateFocusedForces(id,4); }
							case 5: { damage += 8.0;	FocusedForces[id] -= 5;	updateFocusedForces(id,5); }
							case 6: { damage += 10.0;	FocusedForces[id] -= 6;	updateFocusedForces(id,6); }
							case 7: { damage += 12.0;	FocusedForces[id] -= 7;	updateFocusedForces(id,7); }
						}
						takeDamage(id, target, damage);
						
						soundPlay(id, S_CRITICAL_HIT);

						Skills_hitStunShot(target); //&copy stun shot

						if(randomChance(SKILLCHANCE_SOUL_BREAKER_STUN))
						{  // no damage to self, only stun
							randomChance(SKILLCHANCE_SOUL_BREAKER_RETURN) ? Skills_makeStunned(id) : Skills_makeStunned(target);
						}
						showSkillHud(id, "Use Soul Breaker");
					}
					else{
						showSkillHud(id, "Can't be used on ally");
						soundSpeak(id,SYS_IMPOSSIBLE);
					}
				}
				else
					showSkillHud(id, "Invalid target");
			}
			else
				showSkillHud(id, "Not enough charges");
		}
	}

	return PLUGIN_HANDLED;
}

//=======================================================================
//	4	CLASS_D
//=======================================================================
/**
 * Splash Damage (passive) @WAIT TEST
 */
/*
UTIL_AngryAttack( id, flAngryAttackRadius, flAngryAttackDamage );

stock UTIL_AngryAttack( id, Float:flRadius, Float:flDamage )
{
	new Float:flEntityOrigin[ 3 ];
    	pev( id, pev_origin, flEntityOrigin );
    
    	new iClient = FM_NULLENT, Float:flClientOrigin[ 3 ], Float:flDistance;

    	while( ( iClient = engfunc( EngFunc_FindEntityInSphere, iClient, flEntityOrigin, flRadius ) ) )
    	{
		if( IsPlayer( iClient ) && isAlive( iClient ) && UTIL_GetClientTeam( id ) != UTIL_GetClientTeam( iClient ) )
		{
			pev( iClient, pev_origin, flClientOrigin );
       			flDistance = get_distance_f( flEntityOrigin, flClientOrigin );

			if( flDistance <= flRadius )
			{
       				ExecuteHam( Ham_TakeDamage, iClient, gAngryBlastEnt, id, flDamage, DMG_BLAST );
			}
    		}
	}
}

stock HamRadiusDamage(ent, Float:radius, Float:damage, bits)  
{  
    new target = -1, Float:origin[3]  
    pev(ent, pev_origin, origin)  
      
    while(( target = find_ent_in_sphere(target, origin, radius) ))  
    {  
        static Float:o[3]  
        pev(target, pev_origin, o)  

        xs_vec_sub(origin, o, o)  

        // Recheck if the entity is in radius  
        if (xs_vec_len(o) > radius)  
            continue  
          
        Ham_ExecDamageB(target, ent, pev(ent, pev_owner), damage * (xs_vec_len(o) / radius), HIT_GENERIC, bits)  
    }  
}  

stock radius_damage(attacker, const Float:origin[3], Float:max_damage, damage_type, Float:radius)
{
	new id = -1
	static Float:player_origin[3], Float:distance, Float:damage
	while((id = engfunc(EngFunc_FindEntityInSphere, id, origin, radius)) != 0)
	{
		if(!isAlive(id) || id == attacker) 
			continue

		pev(id, pev_origin, player_origin)

		distance = get_distance_f(origin, player_origin)
		damage = max_damage - ((max_damage / radius) * distance)

		new entity = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "trigger_hurt"))
		if(entity)
		{
			new value[16]
			float_to_str(damage * 2, value, 15)
			dispatch_keyvalue(entity, "dmg", value)

			num_to_str(damage_type, value, 15)
			dispatch_keyvalue(entity, "damagetype", value)
			dispatch_keyvalue(entity, "origin", "8192 8192 8192")

			dllfunc(DLLFunc_Spawn, entity)
			dllfunc(DLLFunc_Touch, entity, id)

			engfunc(EngFunc_RemoveEntity, entity)
		}
	}
}
stock dispatch_keyvalue(entity, const key[], const value[])
{
	static classname[32]
	pev(entity, pev_classname, classname, 31)

	set_kvd(0, KV_ClassName, classname)
	set_kvd(0, KV_KeyName, key)
	set_kvd(0, KV_Value, value)
	set_kvd(0, KV_fHandled, 0)

	return dllfunc(DLLFunc_KeyValue, entity, 0)
}
*/

/**
 * Frenzy
 */
public useFrenzy(id)
{
	if(isValidUse(id, CLASS_D))
	{
		if(get_gametime() - CD_Frenzy[id] < COOLDOWN_FRENZY){
			Util_shortage(id);
		}
		else if(getHp(id) <= float(MAX_PLAYER_HEALTH/4) && !PIN_Frenzy[id]) // 30%(255) = 76.5;	255/4 = 63.75
		{
			CD_Frenzy[id] = get_gametime();

			Util_playAnimV(id, 5); //stab_miss
			Player_playAnimP(id, "ref_shoot_knife");

			makeFrenzyEff(id);

			PIN_Frenzy[id] = true;
			
			set_task(SKILLTIME_FRENZY, "endFrenzy", id+TASK_SKILLS_END_FRENZY);
			
			showSkillHud(id, "Use Frenzy");
		}
		else
			showSkillHud(id, "Cant use while HP > 30%"); /* @l2: Frenzy cannot be used due to unsuitable terms. */
	}

	return PLUGIN_HANDLED;
}

public makeFrenzyEff(id)
{
	// @l2: Frenzy, Zealot == S_WAR_CRY_CAST, S_WAR_CRY_SHOT
	//speakSound(id, S_WAR_CRY_CAST); // cast skill
	emitSound(id, S_WAR_CRY_CAST);

	createL2Effect(id, SPR_FRENZY_CAST, 0.4, 240.0, 13.0, 0.5 /*0.8*/);

	// Floor beam eff
	new Float:forigin[3], Float:axis[3];
	getEntOrigin(id, forigin);
	forigin[2] -= 35.0;
	axis = forigin;
	axis[2] += 240.0;
	Create_TE_BEAMDISK(forigin, axis, SPR_LETHALSHOT_TRAIL/*SPR_NUC_RING*/, 0, 0, 4/*life*/, 64/*width*/, 0, 198,165,105,10, 0);
	//Create_TE_BEAMDISK(Float:origin[3], Float:axis[3], sprite, frame, framerate, life, width, noise, r,g,b,a, speed)

	speakSound(id, S_MOFIGHTER_2H_S); // &copy FocusedForce, PumaTotemSpirit
}

public endFrenzy(id)
{
	id -= TASK_SKILLS_END_FRENZY;
	PIN_Frenzy[id] = false;	
}

/**
 * Zealot
 */
public useZealot(id)
{
	if(isValidUse(id, CLASS_D))
	{
		if(get_gametime() - CD_Zealot[id] < COOLDOWN_ZEALOT){
			Util_shortage(id);
			//return;
		}
		else if(getHp(id) <= float(MAX_PLAYER_HEALTH/4) && !PIN_Zealot[id]) // 30%(255) = 76.5;	255/4 = 63.75
		{
			CD_Zealot[id] = get_gametime();
			
			Util_playAnimV(id, 5); //stab_miss
			Player_playAnimP(id, "ref_shoot_knife");
			
			makeFrenzyEff(id);
			
			PIN_Zealot[id] = true;
			
			set_task(SKILLTIME_ZEALOT, "endZealot", id+TASK_SKILLS_END_ZEALOT);
			
			showSkillHud(id, "Use Zealot");
		}
		else
			showSkillHud(id, "Cant use while HP > 30%");	/* Zealot cannot be used due to unsuitable terms. */
	}

	return PLUGIN_HANDLED;
}

public endZealot(id)
{
	id -= TASK_SKILLS_END_ZEALOT;
	PIN_Zealot[id] = false;
}

/**
 * Battle Roar
 */
public useBattleRoar(id) /* @l2 : Increase and restore max HP (effect time = 10 min) */
{
	if(isValidUse(id, CLASS_D))
	{
		if(get_gametime() - CD_BattleRoar[id] < COOLDOWN_BATTLE_ROAR){
			Util_shortage(id);
		}
		else if(getHp(id) == MAX_PLAYER_HEALTH)
		{
			showSkillHud(id, "HP is full");
		}
		else
		{
			CD_BattleRoar[id] = get_gametime();

			Skills_castStart(id);

			showBuffAura(id, AURA_SNIPE, 0); //none //1 - idle1
			Create_TE_DLIGHT(id, 12 /*10*/, 198,165,105, 5, 0);
			//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)

			emitSound(id, S_BATTLE_ROAR_CAST);	// skill cast
//			speakSound(id, );					// char cast

			new calcHP = rand(SKILLS_BATTLE_ROAR_HP_MIN, SKILLS_BATTLE_ROAR_HP_MAX);
			new resultHP = getHp(id) + calcHP;

			setHp(id, resultHP > MAX_PLAYER_HEALTH ? MAX_PLAYER_HEALTH : resultHP);

			//new params[1];
			//params[0] = calcHP;
			set_task(0.5, "castBattleRoar", id+TASK_SKILLS_CAST_BATTLE_ROAR /*, params, 1*/ );
			
			showSkillHud(id, "Use Battle Roar");
		}
	}

	return PLUGIN_HANDLED;
}

public castBattleRoar( /*params[1],*/ id)
{
	id -= TASK_SKILLS_CAST_BATTLE_ROAR;

	Skills_castEnd(id);
	hideBuffAura(id);

	Util_playAnimV(id, 5); //stab_miss
	Player_playAnimP(id, "ref_shoot_knife");

	//showSkillHud(id, "Restored %d HP", params[0]);
}

/**
 * Armor Crush
 */
public useArmorCrush(id)
{
	if(isValidUse(id, CLASS_D))
	{
		if(get_gametime() - CD_ArmorCrush[id] < COOLDOWN_ARMOR_CRUSH){
			Util_shortage(id);
		}
		else
		{
			new target, body;
			get_user_aiming(id, target, body, SKILLRANGE_ARMOR_CRUSH);

			if(isPlayerValid(target))
			{
				if(!(isTeamGameMode() && getTeam(id) == getTeam(target)) || (getTeam(id) != getTeam(target)))
				{
					CD_ArmorCrush[id] = get_gametime();
					
					Player_playAnimV(id, 4); //stab
					Player_playAnimP(id, "ref_aim_onehanded");
					set_pev(id, pev_punchangle, Float:{-0.2,-3.9,0.6}); // punch V

					emit_sound(id, CHAN_VOICE, S_BLUFF_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // skill shot

					showSkillHud(id, "Use Armor Crush");
					
					new params[2]; params[0] = id; params[1] = target;
					set_task(0.4, "useArmorCrushPost", id+TASK_SKILLS_USE_ARMOR_CRUSH, params, 2);
				}
				else{
					showSkillHud(id, "Can't be used on ally");
					soundSpeak(id,SYS_IMPOSSIBLE);
				}
			}
			else
				showSkillHud(id, "Invalid target");
		}
	}
	
	return PLUGIN_HANDLED;
}

public useArmorCrushPost(params[2]) // params[0] = id; params[1] = target;
{
	emit_sound(params[0], CHAN_VOICE, S_MOFIGHTER_2H_ATK_5, random_float(0.7,1.0), ATTN_NORM, 0, PITCH_NORM);

	new Float:damage = 10.0;
	new hp = getHp(params[0]);

	if(hp < MAX_PLAYER_HEALTH)
	{
		new hpmax1 = floatround(MAX_PLAYER_HEALTH / 1.2);	// =212.5
		new hpmax2 = floatround(MAX_PLAYER_HEALTH / 1.5);	// =170
		new hpmax3 = floatround(MAX_PLAYER_HEALTH / 2.0);	// =127.5
		new hpmax4 = MAX_PLAYER_HEALTH / 3;					// ~85
		new hpmax5 = floatround(MAX_PLAYER_HEALTH / 8.0);	// =31.8

		if(hp >= hpmax1){
			damage += randFloat(5.0,8.0);
		}else if(hp >= hpmax2 && hp < hpmax1){
			damage += randFloat(10.0,15.0);
		}else if(hp >= hpmax3 && hp < hpmax2){
			damage += randFloat(15.0,20.0);
		}else if(hp >= hpmax4 && hp < hpmax3){
			damage += randFloat(20.0,26.0);
		}else if(hp >= hpmax5 && hp < hpmax4){
			damage += randFloat(26.0,31.0);
		}else if(hp < hpmax5){
			damage += randFloat(31.0,39.0);
		}
	}

	createL2Effect(params[1], SPR_ARMOR_CRUSH, 0.5 /*0.6*/, 220.0, 14.0, 1.0);
	takeDamage(params[0], params[1], damage);
	emit_sound(params[1], CHAN_VOICE, S_BLUFF_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
}

//=======================================================================
//	5	CLASS_E
//=======================================================================
/**
 * Shield Stun
 */
public useShieldStun(id)
{
	if(isValidUse(id, CLASS_E))
	{
		if(get_gametime() - CD_ShieldStun[id] < COOLDOWN_SHIELD_STUN){
			Util_shortage(id);
		}
		else
		{
			new target, body;
			get_user_aiming(id, target, body, SKILLRANGE_SHIELD_STUN);
			
			if(target && isConnected(target) && isAlive(target))
			{
				if(!(isTeamGameMode() && getTeam(id) == getTeam(target)) || (getTeam(id) != getTeam(target)))
				{
					CD_ShieldStun[id] = get_gametime();

					Player_playAnimV(id, 6); //shield_down
					Player_playAnimP(id, "ref_shoot_shieldknife");
					set_pev(id, pev_punchangle, Flaot:{-0.4,4.0,0.4}); // punch V
					
					// push trail effect
					Create_TE_BEAMFOLLOW(target, SPR_NUC_RING, 4, rand(8,15), 255,255,200, 200);
					//Create_TE_BEAMFOLLOW(ent, spr,      life, width,          red,green,blue, alpha)

					// push target
					// @TODO: F / to adm jokes: isGM() inWtfMode() --> push val over 9999
					new Float:retval[3];
					new Float:vel[3];
					velocity_by_aim(id, 500, retval);
					vel[0] = retval[0];
					vel[1] = retval[1];
					vel[2] = retval[2];
					set_pev(target, pev_velocity, vel);

					// L2 Effect
					createL2Effect(target, SPR_SHIELD_STUN_SHOT, 0.6, 160.0, 11.0, 1.3);

					//Skills_hitStunShot(target);
					//&copy StunShot
					//Create_TE_EXPLOSION(retval, SPR_SHOCKWAVE, 5, 10, TE_EXPLFLAG_NOSOUND); // OPT: forigin
					emit_sound(target, CHAN_VOICE, S_STUN_HIT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// hit sound

					if(rand(0,100) <= SKILLCHANCE_SHIELD_STUN){
						Skills_makeStunned(target);
					}

					set_task(0.5, "pushShieldStun", target+TASK_SKILLS_PUSH_SHIELD_STUN);

					showSkillHud(id, "Use Shield Stun");
				}
				else{
					showSkillHud(id, "Can't be used on ally");
					soundSpeak(id,SYS_IMPOSSIBLE);
				}
			}
			else
				showSkillHud(id, "Invalid target");
		}
	}

	return PLUGIN_HANDLED;
}

public pushShieldStun(id)
{
	id -= TASK_SKILLS_PUSH_SHIELD_STUN;
	Create_TE_KILLBEAM(id); // remove beamfollow (trail)
}

/**
 * Reflect Damage (passive)
 */
public bool:Skills_reflectOrDamage(attacker, victim, Float:damage, bool:isStun, bool:isMagic)
{
	if(getClass(victim) == CLASS_E && randomChance(ReflectDamageChance[victim]))
	{
		effectReflectDamage(victim);

		if(rand(1,2) == 1){
			emit_sound(victim, CHAN_VOICE, isMagic ? S_REFLECT_DAMAGE_SHOT : S_SHIELD_SLAM_SHOT, randF(0.6,1.0), ATTN_NORM, 0, rand(95,110) /*PITCH_NORM*/);
		}

		showSkillHud(victim, "Damage reflected");

//		showSkillHud(victim, "Reflected %d damage", floatround(damage));

		return true;
	}
	else{
		if(isStun && randomChance(SKILLCHANCE_STUN_SHOT)){
			Skills_makeStunned(victim);
		}

//		showSkillHud(attacker, "You hit for %d damage", floatround(damage));
	}
	return false;
}

public effectReflectDamage(id)
{
	new origin[3], Float:forigin[3];
	getEntOrigin(id, forigin);
	FVecIVec(Float:forigin, origin);
	Create_TE_SPRITE(0, origin, SPR_FX_M_T1014_2, 5, rand(50,150));
}

/**
 * Summon Cubics
 */
public altSummonCubics(id)
{
	if(isValidUse(id, CLASS_E))
	{
		if(!PIN_SummonCubics[id])
		{
			if(get_gametime() - CD_SummonCubics[id] < COOLDOWN_SUMMON_CUBICS){
				Util_shortage(id);
				return;
			}
			else if(haveSummon(id) /* <Summon> */){
				showSkillHud(id, "You have summoned unit"); /* You already have "+count+" cubic(s). */
				return;
			}
			else
			{
				PIN_SummonCubics[id] = true;
				CD_SummonCubics[id] = get_gametime();
				Create_BarTime(id, CASTTIME_SUMMON_CUBICS);

				Skills_castStart(id);
				showBuffAura(id, AURA_SUMMON, 1); // idle1

				showSkillHud(id, "Use Summon Cubics");

				soundPlay(id, S_SUMMON_CAST); // cast sound
				// TODO: @l2gets true HFighter snd
				soundSpeak(id, S_M_HMAGICIAN_WHITE); // cast char						

				set_task(0.3, "castSummonCubics", id+TASK_SKILLS_CAST_SUMMON_CUBICS, _, _, "b");
				set_task(float(CASTTIME_SUMMON_CUBICS), "useSummonCubics", id+TASK_SKILLS_USE_SUMMON_CUBICS);
			}
		}
	}
}

public abortSummonCubics(id)
{
	Create_BarTime(id, 0);
	if(PIN_SummonCubics[id])
	{
		remove_task(id+TASK_SKILLS_CAST_SUMMON_CUBICS);
		remove_task(id+TASK_SKILLS_USE_SUMMON_CUBICS);
		
		//if(!haveSummon(id))
		//	showSkillHud(id, "Casting stopped");
		
		PIN_SummonCubics[id] = false;
		
		Skills_castEnd(id);
		hideBuffAura(id);
	}
}

public castSummonCubics(id)
{
	id -= TASK_SKILLS_CAST_SUMMON_CUBICS;

	//&fullcopy Summon Corrupted
	new Float:origin[3], Float:axis[3];
	pev(id, pev_origin, origin);
	axis = origin;
	axis[2] += 70.0;
	Create_TE_BEAMCYLINDER(origin, axis, SPR_LIHIKIN_GLOW, 0, 0, 4, 28, 0, 57,78,180, random_num(100,255), 0);
	
	Util_playAnimV(id, 6);
	Player_playAnimP(id, "ref_shoot_shieldknife");
	
	new Float:how_much[3];
	how_much[0] = random_float(-0.5,0.5);
	how_much[1] = random_float(-0.9,0.9);
	how_much[2] = random_float(-1.8,1.8);
	set_pev(id, pev_punchangle, how_much); // punch V
}

public useSummonCubics(id)
{
	id -= TASK_SKILLS_USE_SUMMON_CUBICS;

	Player_playAnimV(id, 4);
	Player_playAnimP(id, "ref_shoot_shieldgren");

	removeSummonedUnit(id); // <Summon>

	if(Summon_cubics(id)) // <Summon>
		showSkillHud(id, "Cubics are summoned");
	
	abortSummonCubics(id);
}

/**
 * Ultimate Defense
 */
public useUltimateDefense(id)
{
	if(isValidUse(id, CLASS_E))
	{
		if(get_gametime() - CD_UltDefense[id] < COOLDOWN_ULTIMATE_DEFENSE){
			Util_shortage(id);
		}
		else if(!PIN_UltDefense[id])
		{
			CD_UltDefense[id] = get_gametime();
			
			Skills_castStart(id);
			
			Util_playAnimV(id, 6);
			Player_playAnimP(id, "ref_shoot_shieldknife");

			createL2Effect(id, SPR_ULT_DEFENSE_CAST, 0.5, 210.0, 15.0, CASTTIME_ULTIMATE_DEFENSE);
			//createL2Effect(id, effect, size, opacity, framerate, showtime)
			
			// @l2: no chrsound
			emit_sound(id, CHAN_VOICE, S_WAR_CRY_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// skill cast

			set_task(CASTTIME_ULTIMATE_DEFENSE, "castUltimateDefense", id+TASK_SKILLS_CAST_ULT_DEFENSE);

			showSkillHud(id, "Use Ultimate Defense");
		}
	}

	return PLUGIN_HANDLED;
}

public castUltimateDefense(id)
{
	id -= TASK_SKILLS_CAST_ULT_DEFENSE;

	emit_sound(id, CHAN_VOICE, S_WAR_CRY_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // shot skill

	Skills_castEnd(id);
	
	//set_user_godmode(id, 1); // THINK: not godmode only no dmg --> entity_set_float(id, EV_FL_takedamage, 0.0)
	blockMove(id);
	PIN_UltDefense[id] = true;
	ReflectDamageChance[id] = 100;

//	set_pev(index, pev_takedamage, godmode == 1 ? DAMAGE_NO : DAMAGE_AIM)
	
//	new flags = pev(id, pev_flags);
//	if(~flags & FL_FROZEN){
//		set_pev(id, pev_flags, flags | FL_FROZEN);
//	}

	createL2Effect(id, SPR_ULT_DEFENSE_USE, 0.4 /*0.5*/, 195.0, 13.0 /*14.0*/, SKILLTIME_ULTIMATE_DEFENSE);

	set_task(SKILLTIME_ULTIMATE_DEFENSE, "endUltimateDefense", id+TASK_SKILLS_END_ULT_DEFENSE);

}

public endUltimateDefense(id)
{
	id -= TASK_SKILLS_END_ULT_DEFENSE;

	//set_user_godmode(id, 0);
	unblockMove(id);
	PIN_UltDefense[id] = false;
	ReflectDamageChance[id] = SKILLCHANCE_REFLECT_DAMAGE;
	
//	new flags = pev(id, pev_flags);
//	if(flags & FL_FROZEN){
//		set_pev(id, pev_flags, flags & ~FL_FROZEN);
//	}
	
	showSkillHud(id, "Ultimate Defense removed");
}

/**
 * Aggression
 */
/*	
	http://amx-x.ru/viewtopic.php?f=8&t=32508
	https://forums.alliedmods.net/showthread.php?p=476094
	https://forums.alliedmods.net/showthread.php?p=138824
	http://next21.ru/2013/07/%D0%BF%D0%BB%D0%B0%D0%B3%D0%B8%D0%BD-realism-push-player/
*/
public useAggression(id)
{
	if(isValidUse(id, CLASS_E))
	{
		if(get_gametime() - CD_Aggression[id] < COOLDOWN_AGGRESSION){
			Util_shortage(id);
		}
		else
		{
			CD_Aggression[id] = get_gametime();

			Skills_castStart(id);

			//&copy Drain Health
			Util_playAnimV(id, 1); //slash
			Player_playAnimP(id, "ref_shoot_shieldgren");
			
			//if(random_num(1,2)==1)
			//	client_cmd(id, "spk %s", S_H_HFIGHTER_SUB);

			showSkillHud(id, "Use Aggression");
			
			set_task(0.4, "castAggression", id+TASK_SKILLS_CAST_AGGRESSION);
			
			new Float:origin[3];
			pev(id, pev_origin, origin);
			new ent = -1;
			while((ent = engfunc(EngFunc_FindEntityInSphere, ent, origin, SKILLRANGE_AGGRESSION)) != 0)
			{
				static this[32];
				pev(ent, pev_classname, this, 31);

				if(isPlayer(this) && isPlayerValid(ent)) // TODO: for Monster/Guard
				{
					if(getTeam(ent) != getTeam(id) /*&& !isDmGameMode()*/)
					{
						// L2 effect
						createL2Effect(ent, SPR_AGGRESSION, 0.4, 230.0, 15.0, 0.7);
						// Set aim to caster
						entity_set_aim(ent, origin);
						
						new params[2];
						params[0] = id;
						params[1] = ent;
						set_task(0.3, "movePlayerByAggr", ent+TASK_SKILLS_PLAYER_MOVE_BY_AGGR, params, 2, "a", 3);
					}
				}
			}
			
			emit_sound(id, CHAN_VOICE, S_HATE_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
		}
	}

	return PLUGIN_HANDLED;
}

public castAggression(id)
{
	id -= TASK_SKILLS_CAST_AGGRESSION;
	if(random_num(1,2)==1)
		client_cmd(id, "spk %s", S_M_HFIGHTER_THROW);
	emit_sound(id, CHAN_VOICE, S_HATE_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	Skills_castEnd(id);
}

public movePlayerByAggr(obj[2], taskid) // obj[0] = caster, obj[1] = target
{
	new Float:origin[3];
	pev(obj[0], pev_origin, origin);
	entity_set_aim(obj[1], origin);
	client_cmd(obj[1], "+forward;wait;wait;wait;-forward;+forward;wait;wait;wait;-forward");
	client_cmd(obj[1], "wait;+attack;wait;wait;-attack;+attack;wait;wait;wait;-attack");
	
	/*new button = pev(id, pev_button);
	if(~button & IN_FORWARD){
		button |= IN_FORWARD;
		set_pev(id, pev_button, button);
	}*/
}

//=======================================================================
//	6	CLASS_F
//=======================================================================
/**
 * Hurricane
 */
public useHurricane(id)
{
	if(isValidUse(id, CLASS_F))
	{
		if(get_gametime() - CD_Hurricane[id] < COOLDOWN_HURRICANE){
			Util_shortage(id);
		}
		else
		{
			PIN_Cast[id] = true;

			Skills_castStart(id);
			CD_Hurricane[id] = get_gametime();

//			showBuffAura(id, AURA_VAMPIRIC, 1); //idle1

			Create_TE_DLIGHT(id, 8, 225,15,15, 5, 0);
			//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)

			Player_playAnimV(id, random_num(4,5));
			Player_playAnimP(id, "ref_shoot_grenade");
			// no duck anim needed?

			//if(random_num(1,2) == 2)
			soundPlay(id, S_M_HMAGICIAN_BLACK); // cast char

			set_task(0.5, "makeHurricane", id+TASK_SKILLS_MAKE_HURRICANE);

			showSkillHud(id, "Use Hurricane");
		}
	}

	return PLUGIN_HANDLED;
}

public makeHurricane(id)
{
	id -= TASK_SKILLS_MAKE_HURRICANE;

	setPunchAngle(id, Float:{0.6,0.6,0.6}); // punch V

	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
	if(ent)
	{
		setEntClass(ent, "hurricane");
		setEntModel(ent, M_HURRICANE);
		setEntSize(ent, Float:{-16.0, -16.0, 0.0}, Float:{16.0, 16.0, 4.0});

		new Float:origin[3];
		getEntOrigin(id, origin);
		setEntOrigin(ent, origin);

		//new Float:vangle[3];
		getViewAngle(id, origin); // OPT: vangle
		origin[0] *= -1.0 // или так vangle[0] = -vangle[0]
		//origin[0] = randFloat(-180.0, 180.0); // test
		//origin[1] = randFloat(-180.0, 180.0);
		setViewAngle(ent, origin);

		set_pev(ent, pev_solid, 2);
		set_pev(ent, pev_rendermode, 5); // 5 - Additive
		set_pev(ent, pev_renderamt, 255.0); //110.0
		//set_pev(ent, pev_renderfx, 16); // 16 - Hologram (Distort + fade)

		set_pev(ent, pev_scale, 0.8); //0.7
		set_pev(ent, pev_movetype, MOVETYPE_FLY);
		set_pev(ent, pev_owner, id);

		//new Float:velocity[3];
		velocity_by_aim(id, FLYSPEED_HURRICANE, origin); // OPT: velocity
		set_pev(ent, pev_velocity, origin);

		Create_TE_BEAMFOLLOW(ent, SPR_A3MAGIC, 5, 13, 255, 255, 255, 255);
		Create_TE_BEAMFOLLOW(ent, SPR_FX_M_T0084, 6, 9, 170, 170, 172, 255);
		Create_TE_BEAMFOLLOW(ent, SPR_EGONBEAM, rand(3,10), 6, 170, 170, 172, rand(15,50)); // from Hurricane
		//Create_TE_BEAMFOLLOW(entity, spr, life, width, red, green, blue, alpha)

		// HURRICANE MODEL ANIMS
		set_pev(ent, pev_frame, 1);
		set_pev(ent, pev_sequence, 0);
		set_pev(ent, pev_gaitsequence, 0);
		set_pev(ent, pev_framerate, 1.2);

		soundPlay(id, S_HURRICANE_SHOT);	// shot skill
		soundSpeak(id, S_M_HMAGICIAN_SHOT);	// shot char

		Skills_castEnd(id);
//		hideBuffAura(id);
	}
	
	PIN_Cast[id] = false;
}

public Skills_hitHurricane(ent, target) // Core's fwTouch
{
	new origin[3], Float:forigin[3];
	pev(ent, pev_origin, forigin); //target
	FVecIVec(Float:forigin, origin);
	Create_TE_SPRITE(0, origin, SPR_DEATH_SPIKE_HIT, 8, rand(20,70)); //30,80

	soundPlay(ent, S_HURRICANE_EXPLOSION); // hit sound
}

/**
 * Summon Corrupted Man
 */
public altSummonCorruptedMan(id)
{
	if(isValidUse(id, CLASS_F))
	{
		if(!PIN_SummonCorrupted[id])
		{
			if(get_gametime() - CD_SummonCorrupted[id] < COOLDOWN_SUMMON_CORRUPTED){
				Util_shortage(id);
				return;
			}
			else if(haveSummon(id)){
				showSkillHud(id, "You have a summon");
				return;
			}
			else
			{
				CD_SummonCorrupted[id] = get_gametime();
				
				PIN_SummonCorrupted[id] = true;
				
				Skills_castStart(id);
				
				showBuffAura(id, AURA_SUMMON, 1); // idle1
				
				Create_BarTime(id, CASTTIME_SUMMON_REANIMATED);
			
				showSkillHud(id, "Use Summon Corrupted Man");

				soundPlay(id, S_SUMMON_CAST);		// cast sound
				soundSpeak(id, S_M_HMAGICIAN_WHITE);	// cast char

				set_task(0.3, "castSummonCorruptedMan", id+TASK_SKILLS_CAST_SUMMON_CORRUPT, _, _, "b");
				set_task(float(CASTTIME_SUMMON_REANIMATED), "makeCorruptedMan", id+TASK_SKILLS_MAKE_SUMMON_CORRUPT);
			}
		}
	}
}

public abortSummonCorruptedMan(id)
{
	Create_BarTime(id, 0);
	if(PIN_SummonCorrupted[id])
	{
		remove_task(id+TASK_SKILLS_CAST_SUMMON_CORRUPT);
		remove_task(id+TASK_SKILLS_MAKE_SUMMON_CORRUPT);
		
		if(!haveSummon(id))
			showSkillHud(id, "Casting stopped");
		
		PIN_SummonCorrupted[id] = false;
		
		Skills_castEnd(id);
		hideBuffAura(id);
	}
}
 
public castSummonCorruptedMan(id)
{
	id -= TASK_SKILLS_CAST_SUMMON_CORRUPT;

	new Float:origin[3], Float:axis[3];
	pev(id, pev_origin, origin);
	axis = origin;
	axis[2] += 70.0;
	Create_TE_BEAMCYLINDER(origin, axis, SPR_LIHIKIN_GLOW, 0, 0, 4, 28, 0, 57,78,180, rand(100,255), 0);
	
	Util_playAnimV(id, 6);
	Player_playAnimP(id, "ref_shoot_shieldknife");
	
	new Float:how_much[3];
	how_much[0] = randF(-0.5,0.5);
	how_much[1] = randF(-0.9,0.9);
	how_much[2] = randF(-1.8,1.8);
	set_pev(id, pev_punchangle, how_much); // punch V
}

// rename to : useCorrup...
public makeCorruptedMan(id)
{
	id -= TASK_SKILLS_MAKE_SUMMON_CORRUPT;

	Summon_ReanimatedMan(id);

	Player_playAnimV(id, 4);
	Player_playAnimP(id, "ref_shoot_shieldgren");
	
	abortSummonCorruptedMan(id);
}

/**
 * Aura Flare
 */
public useAuraFlare(id)
{
	if(isValidUse(id, CLASS_F))
	{
		if(get_gametime() - CD_AuraFlare[id] < COOLDOWN_AURA_FLARE){
			Util_shortage(id);
		}
		else
		{
			CD_AuraFlare[id] = get_gametime();
			Skills_castStart(id);
			
//			showBuffAura(id, AURA_FLARE, 2); //idle6
			
			set_task(0.3, "castAuraFlare", id+TASK_SKILLS_CAST_AURA_FLARE);
			
			Create_TE_DLIGHT(id, 8, 255,255,255, 4, 0);
			Create_TE_DLIGHT(id, 7, 255,255,255, 4, 0);
			//Create_TE_DLIGHT(id, 8, 150,150,150, 4, 0);
			//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)
			
			Player_playAnimV(id, random_num(4,5));
			Player_playAnimP(id, "ref_shoot_shieldgren");

			if(random_num(1,3) == 1)
				soundSpeak(id, S_M_ELF_SHOT);

			new Float:origin[3]; //, Float:axis[3];
			pev(id, pev_origin, origin);
			origin[2] -= 25.0; //15
			/*
			axis = origin;
			axis[2] += 140.0; //70.0
			//Create_TE_BEAMCYLINDER(origin, axis, SPR_LIHIKIN_GLOW, 0, 0, 4, 28, 0, 215,215,215,random_num(100,255), 0);
			Create_TE_BEAMCYLINDER(origin, axis, SPR_LIHIKIN_GLOW, 0, 0, 2, 10, 0, 255,255,255,255, 0); //width=28
			//Create_TE_BEAMCYLINDER(const Float:coord[3], const Float:axis[3], spr, startframe, framerate, life, width, noise, r,g,b,alpha, speed)
			*/
			
			// Finding a target
			new Float:viewofs[3];
			pev(id, pev_view_ofs, viewofs);
			// получаем координаты от положение глаз игрока
			viewofs[0] += origin[0];
			viewofs[1] += origin[1];
			viewofs[2] += origin[2];

			new Float:vangle[3];
			pev(id, pev_v_angle, vangle);
			// преобразования угла в радианах смещенный вперед
			angle_vector(vangle, ANGLEVECTOR_FORWARD, vangle);

			// угол обзора pev_fov к примеру равен 90, 90/2 = 45
			// косинус угла 45 градусов по идее floatcos должен вернуть ~0.707
			new Float:fov = floatcos(float(pev(id, pev_fov) / 2), degrees) * MULTIPLY;
			
			new ent = -1;

			while((ent = engfunc(EngFunc_FindEntityInSphere, ent, origin, SKILLRANGE_AURA_FLARE)) != 0)
			{
				//if(ent == id || /*!is_user_valid(ent) ||*/ !isAlive(ent))
				//	continue;
				
				static this[32];
				pev(ent, pev_classname, this, 31);
				
				//if(ent != id && isAlive(ent))
				if(isPlayer(this) && isAlive(ent))
				{
					// gets vector of player
					new Float:e_origin[3];
					pev(ent, pev_origin, e_origin);

					// gets difference vector
					e_origin[0] -= viewofs[0];
					e_origin[1] -= viewofs[1];
					e_origin[2] -= viewofs[2];

					// scalar calculation of DotProduct	http://ru.wikipedia.org/wiki/Скалярное_произведение
					if(DotProduct(e_origin, vangle) < fov)
						continue;

					// make effects (target)
					hitAuraFlare(id, ent);
					// remove Sleep if player in
					Skills_inSleepCheck(ent);
					
					if(getTeam(id) != getTeam(ent))
						takeDamage(id, ent, SKILLDAMAGE_AURA_FLARE);
					
					break;
				}
				else if(isAvanpost(this)) // Util.inl
				{
					hitAuraFlare(id, ent);
					takeDamage(id, ent, SKILLDAMAGE_AURA_FLARE);
					Stats_addAvanpostDmg(id, SKILLDAMAGE_AURA_FLARE);
					break;
				}
				else if(isMonster(this) || isGuard(this) || isBuffer(this) || isPet(this))
				{
					hitAuraFlare(id, ent);
					if(!(isPet(this) && id == pev(ent, pev_owner)))
						takeDamage(id, ent, SKILLDAMAGE_AURA_FLARE);
					break;
				}
				else // no target
				{
					hitAuraFlare(id, id);
					//showSkillHud(id, "Invalid target");
					break;
				}
			}
		}
	}

	return PLUGIN_HANDLED;
}

public castAuraFlare(id)
{
	id -= TASK_SKILLS_CAST_AURA_FLARE;
	Skills_castEnd(id);
//	hideBuffAura(id);
}

public hitAuraFlare(caster, target)
{
	//Log(DEBUG, "Skills", "hitAuraFlare(targetId=%d)", target);
	
	emit_sound(target, CHAN_AUTO, S_AURA_FLARE_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // hit
	
	new origin[3], Float:forigin[3];

	if(isAlive(target)) // effects to players
	{
		if(caster == target) // no target
		{
			Util_getAimOriginDist(caster, forigin, 50.0, false);
			FVecIVec(Float:forigin, origin);
		}
		else // enemy target
		{
			Create_TE_DLIGHT(target, 8, 100,100,100, 4, 0);
			get_user_origin(target, origin);
			//Create_TE_SPRITETRAIL(origin, origin, SPR_FLAREFLAKE, random_num(3,7), 1, random_num(1,2), random_num(10,30), 10);
			//Create_TE_SPRITETRAIL(start[3], end[3], iSprite,    count,         life,    scale,       velocity,          random)
			if(!isBot(target))
				Create_ScreenFade(target, (1<<12), 0, FFADE_IN, 255,255,255,118, false);
		}
	}
	else{ // entity target
		pev(target, pev_origin, forigin);
		FVecIVec(Float:forigin, origin);
	}
	
	Create_TE_SPRITE(0, origin, SPR_AURA_FLARE, 7, 198);
	//Create_TE_SPRITE(id=0, origin[3], sprite, scale, brightness)
}

/**
 * Silence (25/11/2014)
 */
public useSilence(id)
{
	if(isValidUse(id, CLASS_F))
	{
		if(get_gametime() - CD_Silence[id] < COOLDOWN_SILENCE){
			Util_shortage(id);
		}
		else
		{
			new target, body; get_user_aiming(id, target, body, SKILLRANGE_SILENCE);

			if(target && isPlayerValid(target))
			{
				if(isTeamGameMode()) /* GameMode.inl */
				{
					if(getTeam(id) != getTeam(target)){ // enemy
						castSilence(id, target);
					}
					else{ // teammate
						showSkillHud(id, "Can't be used on ally");
						soundSpeak(id,SYS_IMPOSSIBLE);
					}
				}
				else{ // FFA game modes
					castSilence(id, target);
				}
			}
			else
				showSkillHud(id, "Invalid target");
		}
	}

	return PLUGIN_HANDLED;
}

public castSilence(attacker, target)
{
	showSkillHud(attacker, "Use Silence");

	CD_Silence[attacker] = get_gametime();

	Skills_castStart(attacker);
	
	emit_sound(attacker, CHAN_VOICE, S_SLEEP_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// cast sound
	if(random_num(1,2) == 1)
		client_cmd(attacker, "spk %s", S_M_HMAGICIAN_SUB); /* @truel2 */
	
	showBuffAura(attacker, AURA_SLEEP, 3); //idle12
	Create_TE_DLIGHT(attacker, 8, 100,100,180, 4, 0);

	Util_playAnimV(attacker, random_num(4,5));
	Player_playAnimP(attacker, "ref_shoot_grenade");

	new params[2]; params[0] = attacker; params[1] = target;
	set_task(0.6, "hitSilence", attacker+TASK_SKILLS_CAST_SILENCE, params, 2);
}

public hitSilence(params[2]) //params[0] = attacker; params[1] = target;
{
	client_cmd(params[0], "spk %s", S_M_HMAGICIAN_THROW);
	Skills_castEnd(params[0]);
	hideBuffAura(params[0]);
	
	new target = params[1];
	
	emit_sound(target, CHAN_VOICE, S_SLEEP_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// skill shot

	if(!is_user_bot(target))
		Create_ScreenFade(target, (1<<12), 0, FFADE_IN, 52,182,238,126, false);

	//&copy Sleep (hit)
	createL2Effect(target, SPR_SLEEP_SHOT, 0.6 /*0.5*/, 245.0, 18.0 /*15.0*/, 1.0);

	if(PIN_Silence[target])
	{
		return;
	}
	else if(randomChance(SKILLCHANCE_SILENCE))
	{
		createL2Effect(target, SPR_SILENCED, 0.3, 255.0, 16.0, SKILLTIME_SILENCE);
		//createL2Effect(id, effect, size, opacity, framerate, showtime)
		
		blockSkills(target);
		PIN_Silence[target] = true;
		showSkillHud(target, "You can feel Silence");

		//bugfix: remove ent too? create it not from createL2Effect()
		set_task(SKILLTIME_SILENCE, "removeSilence", target+TASK_SKILLS_REMOVE_SILENCE);
	}
	else
		showSkillHud(params[0], "Silence failed");
}

public removeSilence(id)
{
	id -= TASK_SKILLS_REMOVE_SILENCE;
	unblockSkills(id);
	PIN_Silence[id] = false;
}

/**
 * @Deprecated
 * Sleep
 *
public useSleep(id)
{
	if(isValidUse(id, CLASS_F))
	{
		if(get_gametime() - SKILLS_DATA[CD_Sleep][id] < COOLDOWN_SLEEP){
			Util_shortage(id);
		}
		else
		{
			new target, body;
			get_user_aiming(id, target, body, SKILLRANGE_SLEEP);

			if(target && isAlive(target))
			{
				if(isTeamGameMode())
				{
					if(getTeam(id) != getTeam(target)){ // enemy
						castSleep(id, target);
					}
					else{ // teammate
						showSkillHud(id, "Can't be used on ally");
						soundSpeak(id,SYS_IMPOSSIBLE);
					}
				}
				else{ // FFA game modes
					castSleep(id, target);
				}
			}
			else
				showSkillHud(id, "Invalid target");
		}
	}

	return PLUGIN_HANDLED;
}
public castSleep(caster, target)
{
	showSkillHud(caster, "Use Sleep");

	SKILLS_DATA[CD_Sleep][caster] = _:get_gametime();

	Skills_castStart(caster);
	
	emit_sound(caster, CHAN_VOICE, S_SLEEP_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// cast sound
	if(random_num(1,2) == 1)
		client_cmd(caster, "spk %s", S_M_HMAGICIAN_SUB);
	
	showBuffAura(caster, AURA_SLEEP, 3); //idle12
	Create_TE_DLIGHT(caster, 8, 100,100,180, 4, 0);

	Util_playAnimV(caster, random_num(4,5));
	Player_playAnimP(caster, "ref_shoot_grenade");

	new params[1]; params[0] = target;
	set_task(0.6, "hitSleep", caster+TASK_SKILLS_CAST_DREAMING_SPIRI, params, 1);
}*/

// Re-edited for Dreaming Spirit
public hitSleep(params[1]) //params[0] = target;
{
	//id -= TASK_SKILLS_CAST_DREAMING_SPIRI;
	
	/*client_cmd(id, "spk %s", S_M_HMAGICIAN_THROW);
	Skills_castEnd(id);
	hideBuffAura(id);*/
	
	new target = params[0];
	
	//emit_sound(target, CHAN_VOICE, S_SLEEP_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// skill shot

	if(!isBot(target))
		Create_ScreenFade(target, (1<<12), 0, FFADE_IN, 133,146,211,126, false);

	//createL2Effect(target, SPR_SLEEP_SHOT, 0.6 /*0.5*/, 245.0, 18.0 /*15.0*/, 1.0);

	if(PIN_Sleep[target])
	{
		return;
	}
	else if(randomChance(SKILLCHANCE_SLEEP))
	{
		set_task(0.3, "sleepEffect", target+TASK_SKILLS_SLEEP_EFFECT, _, _, "b");

		lockCamera(target);
		blockMove(target);
		stopMove(target);
		blockDuckJump(target);
		blockAttack(target);
		blockSkills(target);
		
		PIN_Sleep[target] = true;
		
		showSkillHud(target, "You can feel Sleep"); /* @l2: You can feel Sleep's effect. */
		
		set_task(SKILLTIME_SLEEP, "removeSleep", target+TASK_SKILLS_SLEEPED);
	}
	//else
	//	showSkillHud(id, "Sleep failed"); /* @l2: %s has resisted your Sleep */
}

public sleepEffect(id)
{
	id -= TASK_SKILLS_SLEEP_EFFECT;
	createL2Effect(id, SPR_SLEEP_TARGET, 0.4, 240.0, 15.0, 0.4);
	/*
	new origin[3];
	get_user_origin(id, origin);
	origin[2] += 35;
	Create_TE_SPRITE(0, origin, SPR_SLEEP_TARGET, 2, 255);
	*/
}

public removeSleep(id)
{
	id -= TASK_SKILLS_SLEEPED;

	unlockCamera(id);
	unblockMove(id);
	resumeMove(id);
	unblockDuckJump(id);
	unblockAttack(id);
	unblockSkills(id);
	
	remove_task(id+TASK_SKILLS_SLEEPED);
	remove_task(id+TASK_SKILLS_SLEEP_EFFECT);
	
	PIN_Sleep[id] = false;
}

public bool:Skills_inSleepCheck(id)
{
	if(task_exists(id+TASK_SKILLS_SLEEPED) && PIN_Sleep[id]){
		removeSleep(id+TASK_SKILLS_SLEEPED);
		return true;
	}
	return false;
}

//=======================================================================
//	7	CLASS_G
//=======================================================================
/**
 * Battle Heal
 */
public useBattleHeal(id)
{
	if(isValidUse(id, CLASS_G))
	{
		if(get_gametime() - CD_BattleHeal[id] < COOLDOWN_BATTLE_HEAL){
			Util_shortage(id);
		}
		else
		{
			new target, body;
			get_user_aiming(id, target, body, SKILLRANGE_BATTLE_HEAL); //Float: get_user_aiming ( index, &id, &body, [ distance = 9999 ] )
			static this[32];
			pev(target, pev_classname, this, 31);

			if(target)
			{
				if(isPlayer(this) && isPlayerValid(target))
				{
					if(getTeam(id) == getTeam(target))
					{
						if(getHp(target) == MAX_PLAYER_HEALTH)
							showSkillHud(id, "Target HP is full");
						else{
							makeBattleHeal(id);
							beamsBattleHeal(target);
							effectBattleHeal(id, target);
						}
					}
					else{ // if enemy ==> heal self
						if(getHp(id) == MAX_PLAYER_HEALTH)
							showSkillHud(id, "HP is full");
						else{
							makeBattleHeal(id);
							beamsBattleHeal(id);
							effectBattleHeal(id, id);
						}
					}
				}
				else // target is non-player object
				{
					if(GameMode == MODE_OUTPOST_DEFENSE && isAvanpost(this))
					{
						if((getTeam(id) == 1 && target == EastOutpost) || (getTeam(id) == 2 && target == WestOutpost))
						{
							makeBattleHeal(id);
							beamsBattleHeal(target);
							Avanpost_heal(target);
						}
						else
							showSkillHud(id, "Don't betray!");

						Log(DEBUG, "Skills", "useBattleHeal(): target is AvanpostId=%d", target);
					}
					else if(isGuard(this) || isBuffer(this) || isMonster(this))
					{
						showSkillHud(id, "Can't be used to");
					}
				}
			}
			else // no target ==> heal self
			{
				if(getHp(id) == MAX_PLAYER_HEALTH)
					showSkillHud(id, "HP is full");
				else{
					makeBattleHeal(id);
					beamsBattleHeal(id);
					effectBattleHeal(id, id);
				}
			}
		}
	}

	return PLUGIN_HANDLED;
}

public makeBattleHeal(id) // caster
{
	showSkillHud(id, "Use Battle Heal");
	
	CD_BattleHeal[id] = get_gametime();
	
	Skills_castStart(id);
	
	showBuffAura(id, AURA_DIVINE, 1); //idle1
	Create_TE_DLIGHT(id, 8, 150,150,150, 4, 0);
	
	if(random_num(1,2) == 1){
		Player_playAnimV(id, 1); //slash1
		set_pev(id, pev_punchangle, Flaot:{0.0,-1.3,1.0}); // punch V
	}else{
		Player_playAnimV(id, 2); //slash2
		set_pev(id, pev_punchangle, Flaot:{0.0,1.3,1.0}); // punch V
	}
	Player_playAnimP(id, "ref_shoot_shieldknife");
	
	emit_sound(id, CHAN_VOICE, S_HEAL_CAST, random_float(0.7,1.0), ATTN_NORM, 0, PITCH_NORM); // cast skill
	if(random_num(1,2) == 1)
		client_cmd(id, "spk %s", S_M_HMAGICIAN_NOTARGET);

	set_task(0.3, "castBattleHeal", id+TASK_SKILLS_CAST_BATTLE_HEAL);
}

public castBattleHeal(id)
{
	id -= TASK_SKILLS_CAST_BATTLE_HEAL;
	Skills_castEnd(id);
	hideBuffAura(id);
	remove_task(id+TASK_SKILLS_CAST_BATTLE_HEAL);
}

public beamsBattleHeal(target)
{
	new Float:f_s_origin[3], Float:f_e_origin[3];
	pev(target, pev_origin, f_s_origin);	

	Create_TE_EXPLOSION(f_s_origin, SPR_STUNSHOT_TRAIL, 10, 30, TE_EXPLFLAG_NOSOUND);

	f_e_origin = f_s_origin;
	f_s_origin[2] -= 35.0; //65.0
	f_e_origin[2] += 180.0; //250.0
	
	new s_origin[3], e_origin[3];
	FVecIVec(Float:f_s_origin, s_origin);
	FVecIVec(Float:f_e_origin, e_origin);
	
	for(new j=0; j<=12; j++)
	{
		new AddToX = random_num(-4,4);
		new AddToY = random_num(-4,4);
		s_origin[0] += AddToX;	s_origin[1] += AddToY;
		e_origin[0] += AddToX;	e_origin[1] += AddToY;
		Create_TE_BEAMPOINTS(s_origin, e_origin, SPR_XBEAM3, 0, 1, random_num(4,12), random_num(7,45), random_num(0,1), 255, 255, 200, random_num(0,210), 0);
		//					(start[3],end[3],iSprite,   x, x,  life, 	         width, 		   noise,          red, green, blue, alpha,      speed)
	}
	for(new j=0; j<=4; j++)
	{
		new AddToX = random_num(-4,4);
		new AddToY = random_num(-4,4);
		s_origin[0] += AddToX;	s_origin[1] += AddToY;
		e_origin[0] += AddToX;	e_origin[1] += AddToY;
		Create_TE_BEAMPOINTS(s_origin, e_origin, SPR_XBEAM3, 0, 1, random_num(4,8), random_num(7,45), 0, 239, 211, 213, random_num(100,255), 0);
	}
}

public effectBattleHeal(caster, target)
{
	emit_sound(target, CHAN_VOICE, S_HEAL_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // shot skill

	new hp = getHp(target);
	if(!(hp == MAX_PLAYER_HEALTH)) // if HP is not full
	{
		new resultHP = hp + randNum(SKILLS_BATTLE_HEAL_MIN, SKILLS_BATTLE_HEAL_MAX);
		if(resultHP > MAX_PLAYER_HEALTH)
			resultHP = MAX_PLAYER_HEALTH;
		
		if(PIN_Zealot[target]) // block full heal to Destroyers in Zeal
			resultHP = 1;
		
		setHp(target, resultHP);
		
		showSkillHud(caster, "%d HP restored", (resultHP - hp)); /* %d HP has been restored */
	}
}

/**
 * Balance Life
 */
public altBalanceLife(id)
{
	if(isValidUse(id, CLASS_G))
	{
		if(!PIN_BalanceLife[id])
		{
			if(get_gametime() - CD_BalanceLife[id] < COOLDOWN_BALANCE_LIFE){
				//Util_shortage(id);
				return;
			}
			else{
				PIN_BalanceLife[id] = true;
				showSkillHud(id, "Use Balance Life");
				
				CD_BalanceLife[id] = get_gametime();
				Create_BarTime(id, 1);

				Skills_castStart(id);
				showBuffAura(id, AURA_DIVINE, 1); //idle1
				
				client_cmd(id, "spk %s", S_M_HMAGICIAN_ELEMENT);										// char cast
				emit_sound(id, CHAN_VOICE, S_BALANCE_LIFE_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// skill cast
				
				set_task(0.5, "castBalanceLife", id+TASK_SKILLS_CAST_BALANCE_LIFE, _, _, "b");
				set_task(1.2, "useBalanceLife", id+TASK_SKILLS_USE_BALANCE_LIFE);
			}
		}
	}
}

public abortBalanceLife(id)
{
	if(PIN_BalanceLife[id])
	{
		Create_BarTime(id, 0);
		
		remove_task(id+TASK_SKILLS_CAST_BALANCE_LIFE);
		remove_task(id+TASK_SKILLS_USE_BALANCE_LIFE);

		showSkillHud(id, "Casting aborted");
		
		PIN_BalanceLife[id] = false;
		
		Skills_castEnd(id);
		hideBuffAura(id);
	}
}

public castBalanceLife(id)
{
	id -= TASK_SKILLS_CAST_BALANCE_LIFE;
	
	Create_TE_DLIGHT(id, 8, 150,150,150, 4, 0);
	//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)

	if(random_num(1,2) == 1){
		Player_playAnimV(id, 1); //slash1
		set_pev(id, pev_punchangle, Flaot:{-0.1,-0.8,-2.0}); // punch V
	}else{
		Player_playAnimV(id, 2); //slash2
		set_pev(id, pev_punchangle, Flaot:{0.1,0.8,2.0}); // punch V
	}

	Player_playAnimP(id, "ref_shoot_knife");
}

public useBalanceLife(id)
{
	id -= TASK_SKILLS_USE_BALANCE_LIFE;
	
	remove_task(id+TASK_SKILLS_CAST_BALANCE_LIFE);
//	remove_task(id+TASK_SKILLS_USE_BALANCE_LIFE);
	
	Util_playAnimV(id, 5); //stab_miss
	Player_playAnimP(id, "ref_shoot_shieldgren");
	
	client_cmd(id, "spk %s", S_M_HMAGICIAN_NOTARGET);										// char shot
	emit_sound(id, CHAN_VOICE, S_BALANCE_LIFE_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// skill shot
	
	new Float:origin[3];
	getEntOrigin(id, origin);

	const MAX_TARGETS = 8; // max targets is 8 (7 party memb + self)
	new count = 0;
	new healthsum = 0;
	new target[MAX_TARGETS];
	
	if(isTeamGameMode())
	{
		new ent = -1;
		while((ent = engfunc(EngFunc_FindEntityInSphere, ent, origin, SKILLRANGE_BALANCE_LIFE)) != 0)
		{
			static this[32];
			pev(ent, pev_classname, this, 31);

			if(isPlayer(this) && isAlive(ent))
			{
				if(getTeam(id) == getTeam(ent) /*&& !isDmGameMode()*/)
				{
					count++;
					healthsum += getHp(ent);
					
					if(count < MAX_TARGETS)
						target[count] = ent;
					else
						break;
					
					Log(DEBUG, "Skills", "useBalanceLife(): count=%d | ent=%d | healthsum=%d", count,ent,healthsum);
				}
			}
		}
		
		healthsum = healthsum / count;
		
		for(new i = 1; i <= count; i++)
		{
			if(isConnected(target[i]) && isAlive(target[i]))
			{
				if(!isDmGameMode() || target[i] == id)
					getBalanceLife(target[i], healthsum);
				
				Log(DEBUG, "Skills", "getBalanceLife(): %s -> healthsum = %d", getName(target[i]), healthsum);
			}
		}
	}
	else if(isDmGameMode())
	{
		getBalanceLife(id, getHp(id)); // no effect to self
	}
}

public getBalanceLife(id, res_hp)
{
	setHp(id, res_hp);
	createL2Effect(id, SPR_BALANCE_LIFE_SHOT, 0.9, 220.0, 14.0, 1.6);
}

/**
 * Dryad Root
 */
public useDryadRoot(id)
{
	if(isValidUse(id, CLASS_G))
	{
		if(get_gametime() - CD_DryadRoot[id] < COOLDOWN_DRYAD_ROOT){
			Util_shortage(id);
		}
		else
		{
			new target, body;
			get_user_aiming(id, target, body, SKILLRANGE_DRYAD_ROOT); //Float: get_user_aiming ( index, &id, &body, [ distance = 9999 ] )
			
			if(target && isAlive(target))
			{
				CD_DryadRoot[id] = get_gametime();
				
				if(isTeamGameMode()) /* GameMode.inl */
				{
					if(getTeam(id) != getTeam(target)){ // enemy
						castDryadRoot(id, target);
					}
					else{ // teammate
						showSkillHud(id, "Can't be used on ally");
						soundSpeak(id,SYS_IMPOSSIBLE);
					}
				}
				else{ // FFA game modes
					castDryadRoot(id, target);
				}
			}
			else
				showSkillHud(id, "Invalid target");
		}
	}

	return PLUGIN_HANDLED;
}

public castDryadRoot(caster, target)
{
	emit_sound(caster, CHAN_VOICE, S_M_HMAGICIAN_ELEMENT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// cast char
	emit_sound(caster, CHAN_VOICE, S_DRYAD_ROOT_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);		// cast skill
	
	Skills_castStart(caster);
	Create_TE_DLIGHT(caster, 8, 246,170,203, 5, 0);
	showBuffAura(caster, AURA_ROOT_C, 3); //idle24

	Player_playAnimP(caster, "ref_shoot_shieldgren");
	Player_playAnimV(caster, 5); //stab_miss

	new params[1];
	params[0] = target;
	set_task(0.5, "endCastDryadRoot", caster+TASK_SKILLS_ENDCAST_DRYAD_ROOT, params, 1);
}

public endCastDryadRoot(params[1], id) //params[0] = target
{
	id -= TASK_SKILLS_ENDCAST_DRYAD_ROOT;

	set_pev(id, pev_punchangle, Float:{-1.9,0.0,0.0}); // punch V

	Skills_castEnd(id);
	hideBuffAura(id);
	
	client_cmd(id, "spk %s", S_M_HMAGICIAN_THROW); // shot char
	
	// L2 Effect
	createL2Effect(params[0], SPR_DRYAD_ROOT_SHOT, 0.4, 200.0, 17.0, 1.0);

	if(randomChance(SKILLCHANCE_DRYAD_ROOT))
	{
		if(params[0] && !PIN_DryadRoot[params[0]])
		{
			attachDryadRoot(params[0]);
			
			Create_TE_DLIGHT(params[0], 8, 200,0,0, 100, 0); //life=120
			//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)

			set_task(SKILLTIME_DRYAD_ROOT, "detachDryadRoot", params[0]+TASK_SKILLS_DETACH_DRYAD_ROOT);

			showSkillHud(id, "Rooted %s", getName(params[0]));

			showSkillHud(params[0], "You rooted");
		}
	}
	else
		showSkillHud(id, "Failed to root");
}

public attachDryadRoot(target)
{
	emit_sound(target, CHAN_VOICE, S_DRYAD_ROOT_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // shot skill
	//blockMove(target);
	PIN_DryadRoot[target] = true;
	showDebuffAura(target, AURA_ROOT_T, 1); //idle1
}

public detachDryadRoot(target)
{
	target -= TASK_SKILLS_DETACH_DRYAD_ROOT;
	//unblockMove(target);
	PIN_DryadRoot[target] = false;
	hideDebuffAura(target);
}

/**
 * Return
 */
public altReturn(id)
{
	if(isValidUse(id, CLASS_G))
	{
		if(!PIN_Return[id])
		{
			if(get_gametime() - CD_Return[id] < COOLDOWN_RETURN){
				getClickProtector(id);
				return;
			}
			else if(SyncReturn[getTeam(id)])	// 1=T, 2=CT
			{ /* BUGFIX: avoid of using together at the same time */
				showSkillHud(id, "Teammate already cast");
				Log(DEBUG, "Skills", "altReturn(): Teammate already cast (sync)");
				return;
			}
			else{
				CD_Return[id] = get_gametime();
				Create_BarTime(id, 1);
				
				showSkillHud(id, "Use Return");
				
				Skills_castStart(id);
				showBuffAura(id, AURA_ESCAPE, 1); //idle1
				
				emit_sound(id, CHAN_VOICE, S_MIGHT_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // cast skill

				set_task(0.4, "castReturn", id+TASK_SKILLS_CAST_RETURN, _, _, "b");
				set_task(CASTTIME_RETURN, "useReturn", id+TASK_SKILLS_USE_RETURN);

				PIN_Return[id] = true;
				SyncReturn[getTeam(id)] = true;
			}
		}
	}
}

public abortReturn(id)
{
	if(PIN_Return[id])
	{
		Create_BarTime(id, 0);
		
		remove_task(id+TASK_SKILLS_CAST_RETURN);
		remove_task(id+TASK_SKILLS_USE_RETURN);

		showSkillHud(id, "Casting aborted");
		
		PIN_Return[id] = false;
		SyncReturn[getTeam(id)] = false;
		
		Skills_castEnd(id);
		hideBuffAura(id);
	}
}

public castReturn(id) //taskid
{
	id -= TASK_SKILLS_CAST_RETURN;

	Util_playAnimV(id, 1);
	Player_playAnimP(id, "ref_reload_shieldgun");
	new Float:how_much[3];
	how_much[0] = random_float(-0.5,0.5);
	how_much[1] = random_float(-0.9,0.9);
	how_much[2] = random_float(-1.8,1.8);
	set_pev(id, pev_punchangle, how_much); // punch V

	Create_TE_DLIGHT(id, 11, 133,133,136, 4, 0);
	//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)
}

public useReturn(id) //taskid
{
	id -= TASK_SKILLS_USE_RETURN;

	new iorigin[3];
	get_user_origin(id, iorigin);
	createEffectBSOE(iorigin);

	new Float:origin[3];
	new ent;
	if(getTeam(id) == 1) // T
	{
		while((ent = engfunc(EngFunc_FindEntityByString, ent, "classname", "info_player_deathmatch")))
		{
			pev(ent, pev_origin, origin);
			if(pev_valid(ent) && isHullVacant(origin))
			{
				engfunc(EngFunc_SetOrigin, id, origin);
				break;
			}
			//else
			//	continue;
		}
	}
	else if(getTeam(id) == 2) // CT
	{
		while((ent = engfunc(EngFunc_FindEntityByString, ent, "classname", "info_player_start")))
		{
			pev(ent, pev_origin, origin);
			if(pev_valid(ent) && isHullVacant(origin))
			{
				engfunc(EngFunc_SetOrigin, id, origin);
				break;
			}
			//else
			//	continue;
		}
	}
	
	emit_sound(id, CHAN_VOICE, "common/null.wav", VOL_NORM, ATTN_NORM, 0, PITCH_NORM); //CHAN_WEAPON
	emit_sound(id, CHAN_VOICE, S_TELEPORT_OUT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	
	if(!isBot(id))
		Create_ScreenFade(id, (1<<12), 0, FFADE_IN, 0,0,0,255, true);
}

public createEffectBSOE(origin[3])
{
	Create_TE_DLIGHT_AT_LOC(origin, 11, 236,241,243, 1, 0);
	origin[2] += 40; //35
	Create_TE_SPRITE(0, origin, SPR_BSOE, 8, 194);
}

//=======================================================================
//	8	CLASS_H
//=======================================================================
/**
 * Dreaming Spirit (25/11/2014)
 */
public altDreamingSpirit(id)
{
	if(isValidUse(id, CLASS_H))
	{
		if(get_gametime() - CD_DreamingSpirit[id] < COOLDOWN_DREAMING_SPIRIT){
			//Util_shortage(id);
			//return;
		}
		else if(!PIN_DreamingSpirit[id]) // if not already casting
		{
			PIN_DreamingSpirit[id] = true;
			CD_DreamingSpirit[id] = get_gametime();
			
			Create_BarTime(id, 1);
			Skills_castStart(id);
			//Create_TE_DLIGHT(id, 12, 157,157,151, 3, 0);
			
			//new Float:origin[3];
			//Util_getAimOriginDist(id, origin, 265.0, false); //25.0
			createL2Effect(id, SPR_DREAMING_SPIRIT_CAST, 0.2, 200.0, 10.0, 1.2 /*1.3*/);

			client_cmd(id, "spk %s", S_M_ORC_MAGICIAN_TYPE_B);								// char cast
			emit_sound(id, CHAN_VOICE, S_BIND_WILL_CAST, 0.9, ATTN_NORM, 0, PITCH_NORM);	// skill cast

			if(!task_exists(id+TASK_SKILLS_CAST_DREAMING_SPIRI))
				set_task(0.5, "castDreamingSpirit", id+TASK_SKILLS_CAST_DREAMING_SPIRI, _, _, "b");
			if(!task_exists(id+TASK_SKILLS_USE_DREAMING_SPIRIT))
				set_task(1.0, "useDreamingSpirit", id+TASK_SKILLS_USE_DREAMING_SPIRIT);
		}
	}
}

public abortDreamingSpirit(id)
{
	PIN_DreamingSpirit[id] = false;
	
	Skills_castEnd(id);
	Create_BarTime(id, 0);
	
	if(task_exists(id+TASK_SKILLS_USE_DREAMING_SPIRIT))
		CD_DreamingSpirit[id] = 0.0;

	remove_task(id+TASK_SKILLS_CAST_DREAMING_SPIRI);
	remove_task(id+TASK_SKILLS_USE_DREAMING_SPIRIT);
}

public castDreamingSpirit(id)
{
	id -= TASK_SKILLS_CAST_DREAMING_SPIRI;

	/*
	new origin[3], Float:forigin[3];
	pev(id, pev_origin, forigin);
	
	Util_getAimOriginDist(id, forigin, 25.0, false);
	FVecIVec(Float:forigin, origin);
	Create_TE_SPRITE(0, origin, SPR_FREEZING_FLAME_CAST, 2, 150);
	//Create_TE_SPRITE(id=0, ori[3], sprite,    scale,  brightness)
	*/
}

public useDreamingSpirit(id)
{
	id -= TASK_SKILLS_USE_DREAMING_SPIRIT;

	client_cmd(id, "spk %s", S_M_ORC_MAGICIAN_NOTARGET);
	emit_sound(id, CHAN_VOICE, S_BIND_WILL_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	
	createL2Effect(id, SPR_DREAMING_SPIRIT_SHOT, 0.4, 255.0, 14.0, 0.65 /*0.7*/);
	
	Player_playAnimV(id, randInt(1,2));
	Player_playAnimP(id, "ref_shoot_grenade");
	set_pev(id, pev_punchangle, Float:{-1.0,0.0,0.0}); // punch V

	new Float:origin[3];
	pev(id, pev_origin, origin);
	new target = -1, counter = 0;
	while((target = engfunc(EngFunc_FindEntityInSphere, target, origin, SKILLRANGE_DREAMING_SPIRIT)) != 0)
	{
		if(counter > 4) // max 4 targets
			break;
		
		if(isPlayerValid(target))
		{
			if((getTeam(id) != getTeam(target)) || isDmGameMode())
			{
				createL2Effect(target, SPR_DREAMING_SPIRIT_HIT, 0.4, 200.0, 15.0, 3.4 /*3.6*/);
				//createL2Effect(id, effect, size, opacity, framerate, showtime)

				new params[1]; params[0] = target;
				set_task(0.6, "hitSleep", id+TASK_SKILLS_HIT_DREAMING_SPIRIT, params, 1);

				counter++;
			}
		}
	}

	//&copy from abort()...
	remove_task(id+TASK_SKILLS_USE_DREAMING_SPIRIT);
	remove_task(id+TASK_SKILLS_CAST_DREAMING_SPIRI);
}

/**
 * Gate Chant
 */
public altGateChant(id)
{
	if(isValidUse(id, CLASS_H))
	{
		if(!PIN_GateChant[id])
		{
			if(isDmGameMode()){
				showSkillHud(id, "Skill is blocked for this game mode");
				return;
			}
			else if(get_gametime() - CD_GateChant[id] < COOLDOWN_GATE_CHANT){
				Util_shortage(id);
				return;
			}
			else if(SyncGateChant[getTeam(id)])	// 1=T, 2=CT
			{ /* avoid of using together at the same time */
				showSkillHud(id, "Teammate already cast");
				Log(DEBUG, "Skills", "altGateChant(): Teammate already cast");
				return;
			}
			else
			{
				CD_GateChant[id] = get_gametime();
				Create_BarTime(id, CASTTIME_GATE_CHANT);
				
				showSkillHud(id, "Use Gate Chant");
				
				Skills_castStart(id);
				showBuffAura(id, AURA_ESCAPE, 1); //idle1

				emit_sound(id, CHAN_VOICE, S_MIGHT_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM); // cast skill

				set_task(0.3, "castGateChant", id+TASK_SKILLS_CAST_GATE_CHANT, _, _, "b"); // casting
				set_task(float(CASTTIME_GATE_CHANT), "useGateChant", id+TASK_SKILLS_USE_GATE_CHANT);
				
				PIN_GateChant[id] = true;
				SyncGateChant[getTeam(id)] = true;
			}
		}
	}
}

public abortGateChant(id)
{
	Create_BarTime(id, 0);
	Skills_castEnd(id);
	hideBuffAura(id);
	
	if(PIN_GateChant[id])
	{
		remove_task(id+TASK_SKILLS_CAST_GATE_CHANT);
		remove_task(id+TASK_SKILLS_USE_GATE_CHANT);
		
		showSkillHud(id, "Casting stopped");
		
		PIN_GateChant[id] = false;
		SyncGateChant[getTeam(id)] = false;
	}
}

public castGateChant(id)
{
	id -= TASK_SKILLS_CAST_GATE_CHANT;
	
	Util_playAnimV(id, 1);
	Player_playAnimP(id, "ref_reload_shieldgun");
	
	//&copy Return
	Create_TE_DLIGHT(id, 11, 133,133,136, 4, 0);
	//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)
}

public useGateChant(id) // TODO: Телепортирует 7 самых дальних по расстоянию от себя
{
	id -= TASK_SKILLS_USE_GATE_CHANT;

	new Float:caster_origin[3], icaster_origin[3];
	pev(id, pev_origin, caster_origin);
	
	new teleported = 0;
	
	for(new mate=1; mate <= GMaxPlayers; mate++)
	{
		if(teleported > 7) // bugfix: @l2 (max 8 party members (7+self))
			break;
		
		if(getTeam(id) != getTeam(mate) || mate == id){
			continue;
		}
		else if(isAlive(mate))
		{
			new Float:new_origin[3];
			new_origin = caster_origin;

			while(!isHullVacant(new_origin)){
				new_origin[0] = caster_origin[0] + randFloat(-256.0,256.0);
				new_origin[1] = caster_origin[1] + randFloat(-256.0,256.0);
			}
			// L2 Effect
			//new iorigin[3];
			get_user_origin(mate, icaster_origin);
			createEffectBSOE(icaster_origin);

			engfunc(EngFunc_SetOrigin, mate, new_origin);

			teleported++;
		}
	}

	FVecIVec(Float:caster_origin, icaster_origin);
	createEffectBSOE(icaster_origin);

	emit_sound(id, CHAN_VOICE, S_TELEPORT_OUT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);

	abortGateChant(id);
}

/**
 * Steal Essence
 */
public useStealEssence(id) /* @truel2: sounds and effects == Death Spike */
{
	if(isValidUse(id, CLASS_H))
	{
		if(get_gametime() - CD_StealEssence[id] < COOLDOWN_STEAL_ESSENCE){
			Util_shortage(id);
		}
		else
		{
			new target, body;
			get_user_aiming(id, target, body, SKILLRANGE_STEAL_ESSSENCE);
			new this[32];
			pev(target, pev_classname, this, 31);

			if(isPlayer(this) && isPlayerValid(target))
			{
				if(!(isTeamGameMode() && getTeam(id) == getTeam(target)) || (getTeam(id) != getTeam(target)))
				{
					throwStealEssence(id, target, 0 /*is_npc*/);
				}
				else{
					showSkillHud(id, "Can't be used on ally");
					soundSpeak(id, SYS_IMPOSSIBLE);
				}
			}
			else if(isAvanpost(this))
			{
				if((equali(this, "L2Avanpost@Red") && getTeam(id) == TEAMID_WEST) 
				|| (equali(this, "L2Avanpost@Blu") && getTeam(id) == TEAMID_EAST))
				{
					throwStealEssence(id, target, 1 /*is_npc*/);
				}
			}
			else if(isGuard(this))
			{
				if((equali(this, "L2Guard@East") && getTeam(id) == TEAMID_WEST) 
				|| (equali(this, "L2Guard@West") && getTeam(id) == TEAMID_EAST))
				{
					throwStealEssence(id, target, 1 /*is_npc*/);
				}
			}
			else
				showSkillHud(id, "Invalid target");
		}
	}

	return PLUGIN_HANDLED;
}

public throwStealEssence(id, target, is_npc)
{
	CD_StealEssence[id] = get_gametime();

	Skills_castStart(id);
	showBuffAura(id, AURA_VAMPIRIC, 1); //idle1

	Create_TE_DLIGHT(id, 7, 200,15,15, 5, 0);
	//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)

	Player_playAnimV(id, randInt(4,5));
	Player_playAnimP(id, "ref_shoot_grenade");

	emit_sound(id, CHAN_VOICE, S_DRAIN_ENERGY_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// skill cast
	//if(randInt(1,2) == 1)
	//	client_cmd(id, "spk %s", S_M_ORC_MAGICIAN_TYPE_B);									// char cast

	new params[2];
	params[0] = target;
	params[1] = is_npc; // npc or not
	set_task(0.5, "castStealEssence", id+TASK_SKILLS_CAST_STEAL_ESSSENCE, params, 2);

	showSkillHud(id, "Use Steal Essence");
}

public castStealEssence(params[2], id) //params[0] = target
{
	id -= TASK_SKILLS_CAST_STEAL_ESSSENCE;
	new target = params[0];

	if(randInt(1,2) == 1)
		client_cmd(id, "spk %s", S_MOMAGICIAN_1H_ATK_2);									// char shot
	emit_sound(id, CHAN_VOICE, S_DRAIN_ENERGY_SHOT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);	// skill shot

	new Float:damage = randFloat(SKILLDAMAGE_STEAL_ESSENCE_MIN, SKILLDAMAGE_STEAL_ESSENCE_MAX);
	takeDamage(id, target, damage);

	// TESTED @returned balls
	new origin3[3], Float:lookNormal[3], velocity[3];
	get_user_origin(id, origin3);
	//get_user_origin(id, origin3, 1); // from eyes
	velocity_by_aim(target, randInt(50,200), lookNormal);
	FVecIVec(lookNormal, velocity);
	Create_TE_PROJECTILE(target, origin3, velocity, SPR_BLOOD, randInt(1,100));

	// Effects
	if(!params[1]) // if is not NPC
		Create_TE_DLIGHT(target, 8, 130,61,67, 4, 0);
		//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)
	
	new origin[3], Float:forigin[3];
	pev(target, pev_origin, forigin);
	FVecIVec(Float:forigin, origin);

	origin[2] += (params[1] == 1 ? 85 : 35); // 85 - npc

	Create_TE_SPRITE(0, origin, SPR_STEAL, 2, randInt(150,255));
	//Create_TE_SPRITE(id=0, origin[3], sprite, scale, brightness)

	new idamage = floatround(damage);
	new hp = getHp(id);
	setHp(id, hp + idamage >= MAX_PLAYER_HEALTH ? MAX_PLAYER_HEALTH : hp + idamage);

	showSkillHud(id, "Received %d health", floatround(damage));

	Player_playAnimV(id, 1); //slash
	Player_playAnimP(id, "ref_shoot_shieldgren");
	set_pev(id, pev_punchangle, Float:{0.6,1.6,0.3}); // punch V

	hideBuffAura(id);
	Skills_castEnd(id);

	if(task_exists(id+TASK_SKILLS_CAST_STEAL_ESSSENCE))
		remove_task(id+TASK_SKILLS_CAST_STEAL_ESSSENCE);
}

/**
 * Hammer Crush
 */
public useHammerCrush(id)
{
	if(isValidUse(id, CLASS_H))
	{
		if(get_gametime() - CD_HammerCrush[id] < COOLDOWN_HAMMER_CRUSH){
			Util_shortage(id);
		}
		else
		{
			new target, body;
			get_user_aiming(id, target, body, SKILLRANGE_HAMMER_CRUSH);

			if(target && isAlive(target))
			{
				if(!(isTeamGameMode() && getTeam(id) == getTeam(target)) || (getTeam(id) != getTeam(target)))
				{
					CD_HammerCrush[id] = get_gametime();
					
					Player_playAnimV(id, random_num(4,7));
					Player_playAnimP(id, "ref_shoot_shieldgren");
					
					Create_TE_DLIGHT(id, 12, 157,157,151, 3, 0);

					createL2Effect(id, SPR_BACKSTAB_CAST, 0.5, 200.0, 17.0, 1.1); //&copy Backstab @truel2
					//createL2Effect(id, effect, size, opacity, framerate, showtime)
					emit_sound(id, CHAN_VOICE, S_STUN_CAST, 1.0, ATTN_NORM, 0, PITCH_NORM);

					new params[1];
					params[0] = target;
					set_task(CASTTIME_HAMMER_CRUSH, "castHammerCrush", id+TASK_SKILLS_CAST_HAMMER_CRUSH, params, 1);

					showSkillHud(id, "Use Hammer Crush");
				}
				else{
					showSkillHud(id, "Can't be used on ally");
					soundSpeak(id,SYS_IMPOSSIBLE);
				}
			}
			else
				showSkillHud(id, "Invalid target");
		}
	}

	return PLUGIN_HANDLED;
}

public castHammerCrush(params[1], id) // params[0] = target
{
	id -= TASK_SKILLS_CAST_HAMMER_CRUSH;

	client_cmd(id, "spk %s", S_MOMAGICIAN_POLE_S_ATK_1);
	emit_sound(params[0], CHAN_VOICE, S_BLUFF_CAST, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);

	Skills_hitStunShot(params[0]); //&copy stun shot

	if(randomChance(SKILLCHANCE_HAMMER_CRUSH)){
		Skills_makeStunned(params[0]);
	}

	// TODO:
	//showSkillHud(id, "Stunned %s");
}
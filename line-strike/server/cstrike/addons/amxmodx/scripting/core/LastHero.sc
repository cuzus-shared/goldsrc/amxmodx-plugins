// --------------------------------------------------
// Game Mode: Last Hero
// --------------------------------------------------

#define LASTHERO_COUNTDOWN_TIME	30

new	LH_NumSpawns;
new Float:LH_SpawnPoint[XX][3];
new Float:LH_CenterPoint[3]; // = {0.0,0.0,0.0};

public LastHero_dontMakeSpawns()
{
	//if(mapIs(MAP_RAYISH_HELL)){
	//	Log(DEBUG, "LastHero", "Don't make spawns for this map. Use default ...");
	//	return true;
	//}
	return false;
}

public LastHero_getSpawnsNum()
{
	return LH_NumSpawns;
}

public LastHero_start()
{
	if(file_exists(CFG_FILE_LASTHERO))
	{
		new row[72];
		new ln, th=-1;
		for(ln=0; read_file(CFG_FILE_LASTHERO, ln, row, 71, th); ln++)
		{
			//Log(DEBUG, "LastHero", "%02d: %s", ln, row);
			
			if(!row[0] || equali(row[0],"#") || equali(row[1]," "))
				continue;
			
			new fmap[MAX_MAPNAME_LENGTH];
			copyc(fmap, MAX_MAPNAME_LENGTH-1, row, '/');
			
			if(equali(fmap, GMapName))
			{
				replace(row, 71, fmap, "");
				replace(row, 71, "/", "");
				new str_x[12], str_y[12], str_z[12];
				Log(DEBUG, "LastHero", "(string) origin: %s", row);
				parse(row, str_x, 11, str_y, 11, str_z, 11);
				Log(DEBUG, "LastHero", "(string) mid point: %s %s %s", str_x,str_y,str_z);

				LH_CenterPoint[0] = str_to_float(str_x);
				LH_CenterPoint[1] = str_to_float(str_y);
				LH_CenterPoint[2] = str_to_float(str_z);
			}
		}
	}
	else{
		Log(ERROR, "LastHero", "Failed to load mid points file!");
		Log(ERROR, "LastHero", "%s is missing!", CFG_FILE_LASTHERO);
	}

	if(LastHero_createSpawns()){
		IsEventReady = true;
	}

	// Start countdown
	if(!task_exists(TASK_LASTHERO_COUNTDOWN)){
		new timer[1];
		timer[0] = LASTHERO_COUNTDOWN_TIME;
		set_task(0.1, "LastHero_countdown", TASK_LASTHERO_COUNTDOWN, timer, 1);
	}

	// Block move, attack and skills while countdown
	LastHero_getReady();

	// Start checking the winner
	set_task(float(LASTHERO_COUNTDOWN_TIME)+15.0, "LastHero_checkLast", TASK_LASTHERO_CHECK_LAST);

	Log(DEBUG, "LastHero", "Countdown started (%ds)", LASTHERO_COUNTDOWN_TIME);
}

public bool:LastHero_createSpawns()
{
	if(LastHero_dontMakeSpawns())
		return true;
	
	new Float:center[3];
	center = LH_CenterPoint;
	
	center[2] += 16.0; // avoid stuck

	new buddies = Login_getRegisteredPlayers();
	new Float:angle_diff = 360.0 / float(buddies);
	new size = 0;
	
	switch(buddies)
	{
		case 1,2:		size = 90; //45
		case 3,4:		size = 120; //60
		case 5,6:		size = 150; //90
		case 7,8:		size = 150; //120
		case 9,10:		size = 180; //150
		case 11,12:		size = 210; //180
		case 13,14:		size = 240; //210
		case 15,16:		size = 270; //240
		case 17,18:		size = 300; //270
		case 19,20:		size = 330; //300
		case 21,22:		size = 360; //330
		case 23,24:		size = 390; //360
		case 25,26:		size = 410; //390
		case 27,28:		size = 440; //410
		case 29,30:		size = 460; //440
		case 31,32:		size = 490; //460
	}
	
	if(size <= 0){
		Log(ERROR, "LastHero", "createSpawns(): Error on getting area size");
		return false;
	}
	
	Log(INFO, "LastHero", "createSpawns(): Total players: %d", buddies);
	Log(DEBUG, "LastHero", "createSpawns(): Ring size: %d", size);
	
	new Float:angle[3];
	new Float:v1[3], Float:v2[3];

	LH_NumSpawns = 0;
	
	while(angle[1] < 360.0)
	{
		angle_vector(angle, ANGLEVECTOR_FORWARD, v1);
		angle[1] += angle_diff;
		angle_vector(angle, ANGLEVECTOR_FORWARD, v2);

		xs_vec_mul_scalar(v1, float(size), v1);
		xs_vec_mul_scalar(v2, float(size), v2);
		
		xs_vec_add(v1, center, v1);
		xs_vec_add(v2, center, v2);

		LH_NumSpawns++;
		
		//LH_SpawnPoint[LH_NumSpawns] = v1;
		// bugfix: z coord (?dont works)
		LH_SpawnPoint[LH_NumSpawns][0] = v1[0];
		LH_SpawnPoint[LH_NumSpawns][1] = v1[1];
		LH_SpawnPoint[LH_NumSpawns][2] = LH_CenterPoint[2];
		
		Log(DEBUG, "LastHero", "createSpawns(): Created point %d (%f,%f,%f)", LH_NumSpawns, LH_SpawnPoint[LH_NumSpawns][0],LH_SpawnPoint[LH_NumSpawns][1],LH_SpawnPoint[LH_NumSpawns][2]);
	}

	return true;
}

public LastHero_end(taskid)
{
	Log(INFO, "LastHero", "Event ended");
	GameMode_end(LastHero_getLastVictim(), LastHero_getWinner()); //win_team, last_attacker
}

public LastHero_countdown(timer[1], taskid)
{
	timer[0]--;

	if(timer[0] <= 0)
	{
		EventStarted = true;
		client_print(0, print_center, "Fight!");
		set_task(0.1, "LastHero_unblock", TASK_LASTHERO_UNBLOCK);
		remove_task(taskid);
		return;
	}
	else{
		//set_hudmessage(90, 90, 90, -1.0, 0.22, 2, 0.1, 0.66, 0.05, 0.4, -1);
		//show_hudmessage(0, GameModeString);
		//set_dhudmessage(90, 90, 90, -1.0, 0.25, 2, 0.1, 0.66, 0.05, 0.4, true);
		//show_dhudmessage(0, "Get ready to fight: %d seconds", timer[0]);
		
		client_print(0, print_center, "Get ready: %d sec", timer[0]);
		set_task(1.0, "LastHero_countdown", taskid, timer, 1);
	}
}

public LastHero_getReady()
{
	blockMove(FOR_ALL);			//<Skills>
	blockDuckJump(FOR_ALL);		//<Skills>
	blockAttack(FOR_ALL);		//<Skills>
	blockSkills(FOR_ALL);		//<Skills>
	
	// NOT WORKS
	/*for(new id=1; id<=GMaxPlayers; id++){
		if(isAlive(id) && haveTeam(id))
		{
			new flags = pev(id, pev_flags);
			if(~flags & FL_FROZEN){
				set_pev(id, pev_flags, flags | FL_FROZEN);
			}
		}
	}*/

	blockAllMenus(FOR_ALL);		// prevent menu uses
}

public LastHero_unblock(taskid)
{
	for(new id=1; id<=GMaxPlayers; id++){
		RespawnBlocked[id] = true;	// prevent next respawns
		Player_stand(id);
	}

	// NOT WORKS
	/*for(new id=1; id<=GMaxPlayers; id++){
		if(isAlive(id) && haveTeam(id))
		{
			new flags = pev(id, pev_flags);
			if(flags & FL_FROZEN){
				set_pev(id, pev_flags, flags & ~FL_FROZEN);
			}
		}
	}*/

	unblockMove(FOR_ALL);		//<Skills>
	unblockDuckJump(FOR_ALL);	//<Skills>
	unblockAttack(FOR_ALL);		//<Skills>
	unblockSkills(FOR_ALL);		//<Skills>
}

public LastHero_checkLast(taskid)
{
	new alive;

	for(new id=1; id<=GMaxPlayers; id++) 
		if(haveTeam(id) && isAlive(id))
			alive++;

	if(alive >= 2){
		set_task(10.0, "LastHero_checkLast", TASK_LASTHERO_CHECK_LAST);
		
		if(alive == 2)
		{
			set_task(180.0, "LastHero_noWinner", TASK_LASTHERO_NO_WINNER); //300.0
			
			// (OPT) write last hero and last victim id for final html
			new p=1;
			for(new id=1; id<=GMaxPlayers; id++)
				if(haveTeam(id) && isAlive(id) && p < 3)
					LH_CenterPoint[p] = float(id);
		}
	}
	else if(alive == 1){
		client_print(FOR_ALL, print_center, "Event ended!");
		remove_task(TASK_LASTHERO_NO_WINNER);
		set_task(3.0, "LastHero_end", TASK_LASTHERO_END);
	}
	else if(alive == 0){
		client_print(FOR_ALL, print_center, "No one won!");
		remove_task(TASK_LASTHERO_NO_WINNER);
		set_task(3.0, "LastHero_end", TASK_LASTHERO_END);
	}
	
	Log(DEBUG, "LastHero", "Alive players: %d/%d", alive, getTotalPlayers());
}

public LastHero_noWinner(taskid)
{
	client_print(FOR_ALL, print_center, "Event ended!");
	
	/*
		// TODO
		get player1, player2 kills
			if(player1Kills == player2Kills)
				getPlayer1HP, Player2HP
					if(player1HP > player2HP)
						winner = player1
					else
						winner = player2
			else if(player1Kills > player2Kills)
				winner = player1
			else
				winner player2
	*/
	
	set_task(3.0, "LastHero_end", TASK_LASTHERO_END);
	
	Log(DEBUG, "LastHero", "LastHero_noWinner()");
}

public LastHero_getLastVictim()
{
	new id = floatround(LH_CenterPoint[1]);
	if(!isAlive(id))
		return id;
	
	id = floatround(LH_CenterPoint[2]);
	if(!isAlive(id))
		return id;
	
	Log(INFO, "LastHero", "getLastVictim(): Last victim not found on server!");
	return -1;
}

public LastHero_getWinner()
{
	new rechk_total = 0, id;
	for(id=1; id<=GMaxPlayers; id++) 
		if(haveTeam(id) && isAlive(id))
			rechk_total++;
	
	if(rechk_total == 1)
		return id;
	
	Log(INFO, "LastHero", "getLastVictim(): Last hero not found on server!");
	return -1;
}

public LastHero_endHtml(params[2]) // params[0] = last_victim; params[1] = last_hero;
{
	/*
		_eventName + ": " + winners + " win the match! " + _topKills + " kills."
		_eventName + ": No players win the match (nobody killed)."
	*/
	new html[MAX_HTML_CONTENT_LENGTH];
	new len = MAX_HTML_CONTENT_LENGTH-1;
	
	if(!getHtm(HTML_LH_END, html))
		return;
	
	new buf[128], player; // format

	replace(html, len, "{victim_color}", COLOR_EAST);
	replace(html, len, "{last_victim}", getName(params[0]));
	replace_all(html, len, "{hero_color}", COLOR_EAST);
	replace_all(html, len, "{last_hero}", getName(params[1]));
	
	//&copy Avanpost endHtml()
	player = Stats_getTopStat("killsid");
	replace(html, len, "{top_kills_teamcolor}", /*getTeam(player) == 1 ?*/ COLOR_EAST /*: COLOR_WEST*/);
	replace(html, len, "{top_kills_player}", getName(player));
	format(buf, 127, "%d", Stats_getTopStat("killsnum"));
	replace(html, len, "{top_kills_amount}", buf);

	player = Stats_getTopStat("deathsid");
	replace(html, len, "{top_deaths_teamcolor}", /*getTeam(player) == 2 ? COLOR_WEST :*/ COLOR_EAST);
	replace(html, len, "{top_deaths_player}", getName(player));
	format(buf, 127, "%d", Stats_getTopStat("deathsnum"));
	replace(html, len, "{top_deaths_amount}", buf);

	replace(html, len, "{nextmap}", GMapName); // <MapFactory> (OPT)
	replace(html, len, "{nextgamemode}", GMapAuthor);	

	for(new id=1; id<=GMaxPlayers; id++)
	{
		if(!isConnected(id))
			continue;
		
		format(buf, 127, "%d", Stats_getPvP(id));
		replace(html, len, "{activechar_pvp}", buf);
		format(buf, 127, "%d", Stats_getDeaths(id));
		replace(html, len, "{activechar_deaths}", buf);

		sendHtml(id, html, ""); // <HtmlMessage>
	}
}
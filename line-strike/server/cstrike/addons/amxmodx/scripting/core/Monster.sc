// --------------------------------------------------
// Monster
// --------------------------------------------------


// TODO: move to Util + todo like this short macrofuncs!
#define calcPAtk(%1,%2,%3)	(float(%1)+random_float(float(%2),float(%3)))

//#define setNpcType(%1,%2)	(set_pev(%1,pev_classname,%2))
#define setNpcType(%1,%2)	(entity_set_string(%1,EV_SZ_classname,%2))

// commons
#define MONSTER_MIN_ATTACK_RANGE		100.0 //75.0
#define MONSTER_MAX_FOLLOW_DIST			1500.0
#define MONSTER_RANDOM_WALK_CHANCE		5
#define MONSTER_SP_WAIT_CHANCE			15

// ids
#define MONID_IMP						20004
#define MONID_PORTA						20213
#define MONID_EXCURO					20214
#define MONID_MORDEO					20215
#define MONID_CATHEROK					21035
#define MONID_TREASURE_CHEST			21808
#define MONID_GUARDIAN_ANGEL			21067
#define MONID_BONE_PUPPETEER			21581
#define MONID_BEHEMOTH_ZOMBIE			21578
#define MONID_DWARF_GHOST				20636
#define MONID_HUMAN_GHOST				20638
#define MONID_MIRROR					20639

#define MONID_SQUASH					12774		// @wait move to <Squash>

// classnames (max 31 chars!)
#define MON_IMP					"L2Monster@Imp"
#define MON_PORTA				"L2Monster@Porta"
#define MON_EXCURO				"L2Monster@Excuro"
#define MON_MORDEO				"L2Monster@Mordeo"
#define MON_CATHEROK			"L2Monster@Catherok"
#define MON_TREASURE_CHEST		"L2Monster@Treasure Chest"
#define MON_GUARDIAN_ANGEL		"L2Monster@Guardian Archangel"
#define MON_BONE_PUPPETEER		"L2Monster@Bone Puppeteer"
#define MON_BEHEMOTH_ZOMBIE		"L2Monster@Behemoth Zombie"
#define MON_DWARF_GHOST			"L2Monster@Dwarf Ghost"
#define MON_HUMAN_GHOST			"L2Monster@Human Ghost"
#define MON_MIRROR				"L2Monster@Mirror"

// stats
// common monsters
#define IMP_HP					100
#define	IMP_PATK				5

#define TREASURE_CHEST_HP		1660	//interlude:1660
#define TREASURE_CHEST_PATK		311		//interlude:311

// cruma tower
#define PORTA_HP			354		//interlude:1527
#define PORTA_PATK			20		//interlude:248

#define EXCURO_HP			186		//interlude:1593
#define EXCURO_PATK			12		//interlude:265

#define MORDEO_HP			166		//interlude:1660
#define MORDEO_PATK			32		//interlude:342
#define MORDEO_AGGR			500		//interlude:500

#define CATHEROK_HP			166		//interlude:1660
#define CATHEROK_PATK		24		//interlude:342
#define CATHEROK_AGGR		500		//interlude:500

// baiums lair
#define GUARDIAN_ANGEL_HP		415		//interlude:4158
#define GUARDIAN_ANGEL_PATK		18		//interlude:1831
#define GUARDIAN_ANGEL_AGGR		500

// MAP NOT ASSIGNED
#define BONE_PUPPETEER_HP		193		//3938
#define BONE_PUPPETEER_PATK		14		//1499
#define BONE_PUPPETEER_AGGR		150		//interlude:150

#define BEHEMOTH_ZOMBIE_HP		338		//3938
#define BEHEMOTH_ZOMBIE_PATK	23		//1239

// animations
#define ANIM_IMP_WAIT			0
#define ANIM_IMP_WALK			1
#define ANIM_IMP_RUN			2
#define ANIM_IMP_ATK			3
#define ANIM_IMP_ATK_WAIT		4
#define ANIM_IMP_DEATH			5
#define ANIM_IMP_DEATH_WAIT		6
#define ANIM_IMP_SPWAIT			7	// idle2

#define ANIM_PORTA_WAIT			0
#define ANIM_PORTA_WALK			1
#define ANIM_PORTA_RUN			2
#define ANIM_PORTA_ATK			3
#define ANIM_PORTA_ATKWAIT		4
#define ANIM_PORTA_SPATK		5
#define ANIM_PORTA_SPWAIT		6
#define ANIM_PORTA_DEATH		7
#define ANIM_PORTA_DEATHWAIT	8

#define ANIM_EXCURO_WAIT		0
#define ANIM_EXCURO_WALK		1
#define ANIM_EXCURO_RUN			2
#define ANIM_EXCURO_ATK			3
#define ANIM_EXCURO_ATKWAIT		4
#define ANIM_EXCURO_DEATH		5
#define ANIM_EXCURO_DEATHWAIT	6

#define ANIM_MORDEO_WAIT		0
#define ANIM_MORDEO_WALK		1
#define ANIM_MORDEO_RUN			2
#define ANIM_MORDEO_ATK			3
#define ANIM_MORDEO_ATKWAIT		4
#define ANIM_MORDEO_SPWAIT		5
#define ANIM_MORDEO_DEATH		6
#define ANIM_MORDEO_DEATHWAIT	7

#define ANIM_CATHEROK_WAIT		0
#define ANIM_CATHEROK_WALK		1
#define ANIM_CATHEROK_RUN		2
#define ANIM_CATHEROK_ATK		3
#define ANIM_CATHEROK_ATKWAIT	4
#define ANIM_CATHEROK_SPATK		5
#define ANIM_CATHEROK_SPWAIT	6
#define ANIM_CATHEROK_DEATH		7
#define ANIM_CATHEROK_DEATHWAIT	8
#define ANIM_CATHEROK_SOCIAL1	9
#define ANIM_CATHEROK_SOCIAL2	10

#define ANIM_CHEST_WAIT			0
#define ANIM_CHEST_WALK			1
#define ANIM_CHEST_RUN			2
#define ANIM_CHEST_ATK			3
#define ANIM_CHEST_ATKWAIT		4
#define ANIM_CHEST_SPATK1		5
#define ANIM_CHEST_SPATK2		6
#define ANIM_CHEST_DEATH		7
#define ANIM_CHEST_DEATHWAIT	8

#define ANIM_ANGEL_WAIT			0
#define ANIM_ANGEL_WALK			1
#define ANIM_ANGEL_RUN			2
#define ANIM_ANGEL_ATK1			3
#define ANIM_ANGEL_ATK2			4
#define ANIM_ANGEL_ATKWAIT		5
#define ANIM_ANGEL_SPATK1		6
#define ANIM_ANGEL_SPATK2		7
#define ANIM_ANGEL_SPWAIT		8
#define ANIM_ANGEL_DEATH		9
#define ANIM_ANGEL_DEATHWAIT	10

#define ANIM_BONE_PUPPETEER_WAIT		0
#define ANIM_BONE_PUPPETEER_WALK		1
#define ANIM_BONE_PUPPETEER_RUN			2
#define ANIM_BONE_PUPPETEER_ATK			3
#define ANIM_BONE_PUPPETEER_ATKWAIT		4
#define ANIM_BONE_PUPPETEER_SPATK		5
#define ANIM_BONE_PUPPETEER_SPWAIT		6
#define ANIM_BONE_PUPPETEER_DEATH		7
#define ANIM_BONE_PUPPETEER_DEATHWAIT	8

#define ANIM_BEHEMOTH_ZOMBIE_WAIT		0
#define ANIM_BEHEMOTH_ZOMBIE_WALK		1
#define ANIM_BEHEMOTH_ZOMBIE_RUN		2
#define ANIM_BEHEMOTH_ZOMBIE_ATK		3
#define ANIM_BEHEMOTH_ZOMBIE_ATKWAIT	4
#define ANIM_BEHEMOTH_ZOMBIE_SPATK		5
#define ANIM_BEHEMOTH_ZOMBIE_SPWAIT		6
#define ANIM_BEHEMOTH_ZOMBIE_DEATH		7
#define ANIM_BEHEMOTH_ZOMBIE_DEATHWAIT	8

//=======================================================================

public bool:isMonster(this[])
{
	if(equali(this, MON_IMP) 
	|| equali(this, MON_PORTA) 
	|| equali(this, MON_EXCURO) 
	|| equali(this, MON_MORDEO) 
	|| equali(this, MON_CATHEROK) 
	|| equali(this, MON_TREASURE_CHEST)
	|| equali(this, MON_GUARDIAN_ANGEL)
	|| equali(this, MON_BONE_PUPPETEER)
	|| equali(this, MON_BEHEMOTH_ZOMBIE)
	|| equali(this, MON_DWARF_GHOST)
	|| equali(this, MON_HUMAN_GHOST)
	|| equali(this, MON_MIRROR))
		return true;
	return false;
}

public getMonId(this[])
{
	if(equali(this, MON_IMP))
		return MONID_IMP;
	else if(equali(this, MON_PORTA))
		return MONID_PORTA;
	else if(equali(this, MON_EXCURO))
		return MONID_EXCURO;
	else if(equali(this, MON_MORDEO))
		return MONID_MORDEO;
	else if(equali(this, MON_CATHEROK))
		return MONID_CATHEROK;
	else if(equali(this, MON_TREASURE_CHEST))
		return MONID_TREASURE_CHEST;
	else if(equali(this, MON_GUARDIAN_ANGEL))
		return MONID_GUARDIAN_ANGEL;
	else if(equali(this, MON_BONE_PUPPETEER))
		return MONID_BONE_PUPPETEER;
	else if(equali(this, MON_BEHEMOTH_ZOMBIE))
		return MONID_BEHEMOTH_ZOMBIE;
	else if(equali(this, MON_DWARF_GHOST))
		return MONID_DWARF_GHOST;
	else if(equali(this, MON_HUMAN_GHOST))
		return MONID_HUMAN_GHOST;
	else if(equali(this, MON_MIRROR))
		return MONID_MIRROR;
	return -1;
}

public Monster_taskedSpawn(Float:params[7], taskid) // params[0] = npcid; params[1..3] = origin; params[4..6] = angles;
{
	new Float:origin[3], Float:angles[3];
	origin[0] = params[1]; origin[1] = params[2]; origin[2] = params[3];
	angles[0] = params[4]; angles[1] = params[5]; angles[2] = params[6];
	Monster_doSpawn(-1, floatround(params[0]), origin, angles);
}

public Monster_spawn(invoker, npcid) // NPC spawn by invoker (near)
{
	new Float:origin[3], Float:angles[3];
	pev(invoker, pev_origin, origin);
	pev(invoker, pev_angles, angles);
	angles[0] = 0.0;
	Monster_doSpawn(invoker, npcid, origin, angles);	
}

// if cpl --> move to <Npc> and create 'default' struct for Monster/Guard/Npc/Summon(mb) ...
public Monster_setParams(ent, type[], model[], hp, spawn_anim)
{
	setNpcType(ent, type);
	entity_set_model(ent, model);
	entity_set_float(ent, EV_FL_health, float(hp));
	Npc_playAnim(ent, spawn_anim, 1.0);
}

public Monster_doSpawn(player, npcid, Float:origin[3], Float:angles[3])
{
	new ent = create_entity("info_target");
	if(is_valid_ent(ent))
	{
		switch(npcid)
		{
			case MONID_IMP: {
				Monster_setParams(ent, MON_IMP, M_IMP, IMP_HP, ANIM_IMP_SPWAIT);
				entity_set_size(ent, Float:{-16.0,-16.0,-36.0}, Float:{16.0,16.0,48.0});
				Log(DEBUG, "Monster", "Created Imp (ID=%d)", ent);
			}
			case MONID_PORTA: {
				setNpcType(ent, MON_PORTA);
				entity_set_model(ent, M_PORTA);
				entity_set_size(ent, Float:{-32.0,-32.0,0.0}, Float:{32.0,32.0,64.0});
				entity_set_float(ent, EV_FL_health, float(PORTA_HP));
				Npc_playAnim(ent, ANIM_PORTA_SPATK, 1.0);
				Log(DEBUG, "Monster", "Created Porta (ID=%d)", ent);
			}
			case MONID_EXCURO: {
				setNpcType(ent, MON_EXCURO);
				entity_set_model(ent, M_EXCURO);
				entity_set_size(ent, Float:{-32.0,-32.0,-36.0}, Float:{32.0,32.0,48.0});
				entity_set_float(ent, EV_FL_health, float(EXCURO_HP));
				Npc_playAnim(ent, ANIM_EXCURO_ATK, 1.0);
				Log(DEBUG, "Monster", "Created Excuro (ID=%d)", ent);
			}
			case MONID_MORDEO: {
				setNpcType(ent, MON_MORDEO);
				entity_set_model(ent, M_MORDEO);
				entity_set_size(ent, Float:{-16.0,-16.0,0.0}, Float:{16.0,16.0,48.0});
				entity_set_float(ent, EV_FL_health, float(MORDEO_HP));
				Npc_playAnim(ent, ANIM_MORDEO_SPWAIT, 1.0);
				Log(DEBUG, "Monster", "Created Mordeo (ID=%d)", ent);
			}
			case MONID_CATHEROK: {
				setNpcType(ent, MON_CATHEROK);
				entity_set_model(ent, M_CATHEROK);
				entity_set_size(ent, Float:{-24.0,-24.0,0.0}, Float:{24.0,24.0,64.0});
				entity_set_float(ent, EV_FL_health, float(CATHEROK_HP));
				Npc_playAnim(ent, ANIM_CATHEROK_SOCIAL1, 1.0);
				Log(DEBUG, "Monster", "Created Catherok (ID=%d)", ent);
			}
			case MONID_TREASURE_CHEST: {
				setNpcType(ent, MON_TREASURE_CHEST);
				entity_set_model(ent, M_TREASURE_CHEST);
				entity_set_size(ent, Float:{-24.0,-32.0,0.0}, Float:{24.0,32.0,24.0});
				entity_set_float(ent, EV_FL_health, float(TREASURE_CHEST_HP));
				Npc_playAnim(ent, ANIM_CHEST_SPATK1, 1.0);
				Log(DEBUG, "Monster", "Created Treasure Chest (ID=%d)", ent);
			}
			case MONID_GUARDIAN_ANGEL: {
				setNpcType(ent, MON_GUARDIAN_ANGEL);
				entity_set_model(ent, M_GUARDIAN_ANGEL);
				entity_set_size(ent, Float:{-36.0,-36.0,0.0}, Float:{36.0,36.0,72.0});
				entity_set_float(ent, EV_FL_health, float(GUARDIAN_ANGEL_HP));
				Npc_playAnim(ent, random_num(1,2)==1 ? ANIM_ANGEL_SPATK1 : ANIM_ANGEL_SPATK2, 1.0);
				Log(DEBUG, "Monster", "Created Guardian Archangel (ID=%d)", ent);
			}
			case MONID_BONE_PUPPETEER: {
				setNpcType(ent, MON_BONE_PUPPETEER);
				entity_set_model(ent, M_BONE_PUPPETEER);
				entity_set_size(ent, Float:{-24.0,-24.0,0.0}, Float:{24.0,24.0,64.0});
				entity_set_float(ent, EV_FL_health, float(BONE_PUPPETEER_HP));
				Npc_playAnim(ent, ANIM_BONE_PUPPETEER_SPATK, 1.0);
				Log(DEBUG, "Monster", "Created Bone Puppeteer (ID=%d)", ent);
			}
			case MONID_BEHEMOTH_ZOMBIE: {
				setNpcType(ent, MON_BEHEMOTH_ZOMBIE);
				entity_set_model(ent, M_BEHEMOTH_ZOMBIE);
				entity_set_size(ent, Float:{-32.0,-32.0,0.0}, Float:{32.0,32.0,80.0});
				entity_set_float(ent, EV_FL_health, float(BEHEMOTH_ZOMBIE_HP));
				Npc_playAnim(ent, ANIM_BEHEMOTH_ZOMBIE_SPATK, 1.0);
				Log(DEBUG, "Monster", "Created Behemoth Zombie (ID=%d)", ent);
			}
		}

		set_pev(ent, pev_movetype, MOVETYPE_PUSHSTEP);
		entity_set_int(ent, EV_INT_solid, SOLID_BBOX);
		entity_set_float(ent, EV_FL_takedamage, DAMAGE_AIM);
		set_pev(ent, pev_gravity, 1.0);

		entity_set_origin(ent, origin);
		set_pev(ent, pev_angles, angles);
		drop_to_floor(ent);

		if(player && isAlive(player))
			Npc_avoidStuck(player, origin);
		
		entity_set_float(ent, EV_FL_nextthink, get_gametime() + 1.0);
	}
	else
		Log(ERROR, "Monster", "doSpawn(%d, %d, _, _) Can't create entity!", player, npcid);
}

public Monster_onAttack(npc, npcid, attacker, anim_idle, anim_spwait, Float:nextattack)
{
	if(pev(npc, pev_sequence) == anim_idle || pev(npc, pev_sequence) == anim_spwait)
	{
		entity_set_float(npc, EV_FL_nextthink, get_gametime() + INFINITE); // Stop NPC Think

		Log(DEBUG, "Monster", "onAttack(MonID %d | HP %.1f): new victim '%s'", npc, getName(attacker), pev(npc, pev_health));
		new params[4];
		params[0] = npc; params[1] = npcid; params[2] = attacker; 
		params[3] = -1; // unused
		set_task(nextattack, "Monster_attack", npc+TASK_MONSTER_ATTACK, params, 4);
	}
}

public Monster_takeDamage(attacker, npc, npcid)
{
	Log(DEBUG, "Monster", "takeDamage(Attacker=%s, ObjId=%d, NpcId=%d)", getName(attacker), npc, npcid);

	// L2 Hit &copy <Core>
	if(rand(1,2) == 1){
		new origin[3], Float:forigin[3];
		pev(npc, pev_origin, forigin);
		FVecIVec(Float:forigin, origin);
		origin[2] += rand(20,30);
		origin[1] += rand(5,25);
		Create_TE_SPRITE(0, origin, SPR_L2DAMAGE, random_num(2,5), random_num(15,150));
	}

	switch(npcid){
		case MONID_IMP: {
			Monster_onAttack(npc, npcid, attacker, ANIM_IMP_WAIT, ANIM_IMP_SPWAIT, 0.3);
			if(random_num(1,2) == 1)
				emit_sound(npc, CHAN_VOICE, S_IMP[random_num(3,5)], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
		}
		case MONID_PORTA: {
			Monster_onAttack(npc, npcid, attacker, ANIM_PORTA_WAIT, ANIM_PORTA_SPWAIT, 0.2);
			// TODO: sounds
		}
		case MONID_EXCURO: {
			Monster_onAttack(npc, npcid, attacker, ANIM_EXCURO_WAIT, ANIM_EXCURO_WAIT, 0.3);
			// TODO: sounds
		}
		case MONID_MORDEO: {
			Monster_onAttack(npc, npcid, attacker, ANIM_MORDEO_WAIT, ANIM_MORDEO_SPWAIT, 0.3);
			// TODO: sounds
		}
		case MONID_CATHEROK: {
			Monster_onAttack(npc, npcid, attacker, ANIM_CATHEROK_WAIT, ANIM_CATHEROK_SPWAIT, 0.3);
			// TODO: sounds
		}
		case MONID_TREASURE_CHEST: {
			//Monster_onAttack(npc, npcid, attacker, ANIM_CHEST_WAIT, ANIM_CHEST_WAIT, 0.3);
			// TODO: sounds
			// TODO: logic
		}
		case MONID_GUARDIAN_ANGEL: {
			Monster_onAttack(npc, npcid, attacker, ANIM_ANGEL_WAIT, ANIM_ANGEL_SPWAIT, 0.3);
			// TODO: sounds
		}
		case MONID_BONE_PUPPETEER: {
			Monster_onAttack(npc, npcid, attacker, ANIM_BONE_PUPPETEER_WAIT, ANIM_BONE_PUPPETEER_SPWAIT, 0.3);
			// TODO: sounds
		}
		case MONID_BEHEMOTH_ZOMBIE: {
			Monster_onAttack(npc, npcid, attacker, ANIM_BEHEMOTH_ZOMBIE_WAIT, ANIM_BEHEMOTH_ZOMBIE_SPWAIT, 0.3);
			// TODO: sounds
		}
	}
}

public Monster_killed(ent, killer, npcid)
{
	entity_set_float(ent, EV_FL_nextthink, get_gametime() + INFINITE); // Stop Think

	if(task_exists(ent+TASK_MONSTER_ATTACK))
		remove_task(ent+TASK_MONSTER_ATTACK);

	new reward = calculateRewards(killer, true, haveSummon(killer)); // <Player>
	if(reward){
		showExpHud(killer, "+%d exp", reward);
	}

	switch(npcid){
		case MONID_IMP: {
			Npc_playAnim(ent, ANIM_IMP_DEATH, 1.0);
			emit_sound(ent, CHAN_VOICE, S_IMP[2], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
			DropList_drop(ent, true,70,1, false,0,0, false,0,0);
			//DropList_drop(npc, bool:adena, adena_chance, adena_amount, bool:etcitem, etcitem_chance, etcitem_amount, bool:potion, potion_chance, potion_amount)
		}
		case MONID_PORTA: {
			Npc_playAnim(ent, ANIM_PORTA_DEATH, 1.0);
			//sounds @WAIT
			DropList_drop(ent, true,70,1, true,15,2, true,15,1);
		}
		case MONID_EXCURO: {
			Npc_playAnim(ent, ANIM_EXCURO_DEATH, 1.0);
			//sounds @WAIT
			DropList_drop(ent, true,70,1, true,15,2, true,15,1);
		}
		case MONID_MORDEO: {
			Npc_playAnim(ent, ANIM_MORDEO_DEATH, 1.0);
			//sounds @WAIT
			DropList_drop(ent, true,70,1, true,15,2, true,15,1);
		}
		case MONID_CATHEROK: {
			Npc_playAnim(ent, ANIM_CATHEROK_DEATH, 1.0);
			//sounds @WAIT
			DropList_drop(ent, true,70,1, true,15,2, true,15,1);
		}
		case MONID_TREASURE_CHEST: {
			Npc_playAnim(ent, ANIM_CHEST_DEATH, 1.0);
			//sounds @WAIT
			// TODO: droplist --> enchant scrolls IMPL
		}
		case MONID_GUARDIAN_ANGEL: {
			Npc_playAnim(ent, ANIM_ANGEL_DEATH, 1.0);
			//sounds @WAIT
			DropList_drop(ent, true,70,1, true,15,2, true,15,1);
		}
		case MONID_BONE_PUPPETEER: {
			Npc_playAnim(ent, ANIM_BONE_PUPPETEER_DEATH, 1.0);
			//sounds @WAIT
			DropList_drop(ent, true,70,1, true,15,2, true,15,1);
		}
		case MONID_BEHEMOTH_ZOMBIE: {
			Npc_playAnim(ent, ANIM_BEHEMOTH_ZOMBIE_DEATH, 1.0);
			//sounds @WAIT
			DropList_drop(ent, true,70,1, true,15,2, true,15,1);
		}
	}

	fm_set_rendering(ent, kRenderFxNone, 255,255,255, kRenderTransAdd, 200);

	set_pev(ent, pev_solid, SOLID_NOT);
	set_pev(ent, pev_takedamage, 0.0);
	set_pev(ent, pev_movetype, MOVETYPE_FLY);
	set_pev(ent, pev_deadflag, DEAD_DYING);
	set_pev(ent, pev_gravity, 0.5);
	set_pev(ent, pev_velocity, {0.0,0.0,25.0});

	new params[2]; params[0] = ent; params[1] = npcid;
	set_task(1.0, "Monster_fall", ent+TASK_MONSTER_FALL, params, 2);
	set_task(random_float(3.5, 5.0), "Monster_removeCorpse", ent+TASK_MONSTER_REMOVE_CORPSE, params, 2);
}

public Monster_fall(params[2]) //params[0] = ent; params[1] = npcid;
{
	switch(params[1]){ //npcid
		case MONID_IMP: {emit_sound(params[0], CHAN_VOICE, S_IMP[6], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);}
		/*case MONID_PORTA: {}
		case MONID_EXCURO: {}
		case MONID_MORDEO: {}
		case MONID_CATHEROK: {}
		case MONID_TREASURE_CHEST: {}
		case MONID_GUARDIAN_ANGEL: {}
		case MONID_BONE_PUPPETEER: {}
		case MONID_BEHEMOTH_ZOMBIE: {}*/
	}
}

public Monster_removeCorpse(params[2]) //params[0] = ent; params[1] = npcid;
{
	new ent = params[0];

	new Float:origin[3];
	getEntOrigin(ent, origin);

	new iorigin[4];
	iorigin[0] = params[1]; //npcid
	iorigin[1] = floatround(origin[0]);
	iorigin[2] = floatround(origin[1]);
	iorigin[3] = floatround(origin[2]);

	set_task(randFloat(15.0,25.0), "Monster_respawn", ent+TASK_MONSTER_RESPAWN, iorigin, 4);

	if(pev_valid(ent)){
		entityDispose(ent);
	}
}

public Monster_respawn(params[4]) //params[0] = npcid; params[1,2,3] = origin
{
	new Float:origin[3];
	origin[0] = float(params[1]);
	origin[1] = float(params[2]);
	origin[2] = float(params[3]);
	while(!isHullVacant(origin)){
		origin[randNum(0,1)] += randFloat(-32.0,32.0);
	}
	Monster_doSpawn(-1,  params[0], origin, Float:{0.0,0.0,0.0}); //player, npcid, Float:origin[3], Float:angles[3]
}

/**
 * General think method.
 */
public Monster_think(Npc, NpcId, IdleAnim, WalkAnim, WalkSpeed, Float:WalkTime, SpWaitAnim, Float:SpWaitTime, SpWaitSound[])
{
	if(!is_valid_ent(Npc))
        return;
	else if(pev(Npc, pev_health) <= 0.0){
		Log(WARN, "Monster", "think(ObjId=%d,NpcId=%d): is dead but not removed", Npc, NpcId);
		return;
	}
	else
	{
		drop_to_floor(Npc);
		
		new victim = Monster_aggrCheck(Npc, NpcId); // Check aggr range
		if(victim)
		{
			entity_set_float(Npc, EV_FL_nextthink, get_gametime() + INFINITE); // Stop NPC Think

			Log(DEBUG, "Monster", "think(ObjId=%d,NpcId=%d): found victim '%s'", Npc, NpcId, getName(victim));
			new params[4];
			params[0] = Npc; params[1] = NpcId; params[2] = victim; 
			params[3] = -1; // unused
			set_task(0.2, "Monster_attack", Npc+TASK_MONSTER_ATTACK, params, 4);
		}
		else if(pev(Npc, pev_sequence) == IdleAnim)
		{
			if(randomChance(MONSTER_RANDOM_WALK_CHANCE)){
				Monster_randomWalk(Npc, WalkSpeed, IdleAnim, WalkAnim, WalkTime);
			}
			else if(randomChance(MONSTER_SP_WAIT_CHANCE)){
				Monster_spWait(Npc, SpWaitAnim, SpWaitSound, SpWaitTime);
			}
			else{
				entity_set_float(Npc, EV_FL_nextthink, get_gametime() + 1.5);
			}
		}
		else{
			entity_set_float(Npc, EV_FL_nextthink, get_gametime() + 1.5);
			Npc_playAnim(Npc, IdleAnim, 1.0);
		}
	}
}

//Monster_think(Npc, NpcId, IdleAnim, WalkAnim, WalkSpeed, Float:WalkTime, SpWaitAnim, Float:SpWaitTime, SpWaitSound[])}

public Imp_think(Npc){
	Monster_think(Npc, MONID_IMP, ANIM_IMP_WAIT, ANIM_IMP_WALK, 250, 4.0, ANIM_IMP_SPWAIT, 2.0, S_IMP[7] /*@WAIT sounds*/);}
public Porta_think(Npc){
	Monster_think(Npc, MONID_PORTA, ANIM_PORTA_WAIT, ANIM_PORTA_WALK, 12, 4.0, ANIM_PORTA_SPWAIT, 2.0, S_IMP[7] /*@WAIT sounds*/);}
public Excuro_think(Npc){
	Monster_think(Npc, MONID_EXCURO, ANIM_EXCURO_WAIT, ANIM_EXCURO_WALK, 280, 3.3, ANIM_EXCURO_WAIT, 2.5, S_IMP[7] /*@WAIT sounds*/);}
public Mordeo_think(Npc){
	Monster_think(Npc, MONID_MORDEO, ANIM_MORDEO_WAIT, ANIM_MORDEO_WALK, 330, 3.0, ANIM_MORDEO_SPWAIT, 2.5, S_IMP[7] /*@WAIT sounds*/);}
public Catherok_think(Npc){
	Monster_think(Npc, MONID_CATHEROK, ANIM_CATHEROK_WAIT, ANIM_CATHEROK_WALK, 300, 3.0, ANIM_CATHEROK_SPWAIT, 2.5, S_IMP[7] /*@WAIT sounds*/);}
public TreasureChest_think(Npc){
	Monster_think(Npc, MONID_TREASURE_CHEST, ANIM_CHEST_WAIT, ANIM_CHEST_WALK, 240, 3.5, ANIM_CHEST_WAIT, 3.0, S_IMP[7] /*@WAIT sounds*/);}
public GuardianArchangel_think(Npc){
	Monster_think(Npc, MONID_GUARDIAN_ANGEL, ANIM_ANGEL_WAIT, ANIM_ANGEL_WALK, 240, 3.5, ANIM_ANGEL_SPWAIT, 3.0, S_IMP[7] /*@WAIT sounds*/);}
public BonePuppeteer_think(Npc){
	Monster_think(Npc, MONID_BONE_PUPPETEER, ANIM_BONE_PUPPETEER_WAIT, ANIM_BONE_PUPPETEER_WALK, 240, 3.5, ANIM_BONE_PUPPETEER_SPWAIT, 3.0, S_IMP[7] /*@WAIT sounds*/);}
public BehemothZombie_think(Npc){
	Monster_think(Npc, MONID_BEHEMOTH_ZOMBIE, ANIM_BEHEMOTH_ZOMBIE_WAIT, ANIM_BEHEMOTH_ZOMBIE_WALK, 240, 3.5, ANIM_BEHEMOTH_ZOMBIE_SPWAIT, 3.0, S_IMP[7] /*@WAIT sounds*/);}

public Monster_randomWalk(ent, speed, idle_anim, walk_anim, Float:walk_time)
{
	entity_set_float(ent, EV_FL_nextthink, get_gametime() + walk_time);
	Npc_taskAnim(walk_time, ent, idle_anim);

	new Float:veloc[3];
	pev(ent, pev_velocity, veloc);

	if(vector_length(veloc) < 150.0)
	{
		pev(ent, pev_v_angle, veloc);
		veloc[1] += random_float(-180.0, 180.0);
		set_pev(ent, pev_v_angle, veloc);
		set_pev(ent, pev_angles, veloc);
	}
	
	if(pev(ent, pev_sequence) != walk_anim)
		set_pev(ent, pev_sequence, walk_anim);	

	velocity_by_aim(ent, speed, veloc);
	set_pev(ent, pev_velocity, veloc);
	pev(ent, pev_origin, veloc);
	veloc[2] += 5.0;
	set_pev(ent, pev_origin, veloc);
}

public Monster_spWait(ent, spwait_anim, spwait_sound[], Float:spwait_time)
{
	entity_set_float(ent, EV_FL_nextthink, get_gametime() + spwait_time);
	Npc_playAnim(ent, spwait_anim, 1.2);
	emit_sound(ent, CHAN_VOICE, spwait_sound, random_float(0.7,1.0), ATTN_NORM, 0, PITCH_NORM);
}

public Monster_attackAction(npc, anim_atk, anim_atkwait, Float:atkwait_time, atk_sound[], target, Float:damage)
{
	Monster_stopMove(npc, anim_atkwait);
	Npc_playAnim(npc, anim_atk, 1.4);
	Npc_taskAnim(atkwait_time, npc, anim_atkwait);
	emit_sound(npc, CHAN_BODY, atk_sound, random_float(0.7,1.0), ATTN_NORM, 0, PITCH_NORM);
	ExecuteHam(Ham_TakeDamage, target, npc, npc, damage, DMG_BULLET);
}

public Monster_attack(params[4], taskid)
{
	new Npc = params[0]; new NpcId = params[1]; new Target = params[2];

	//Log(DEBUG, "Monster", "attack(Npc=%d): target '%s'", Npc, getName(Target));

	if(isAlive(Target))
	{
		Npc_setAim(Npc, Target);

		new Float:ent_origin[3], Float:vic_origin[3], Float:distance;
		pev(Npc, pev_origin, ent_origin);
		pev(Target, pev_origin, vic_origin);
		distance = get_distance_f(ent_origin, vic_origin);

		if(distance <= MONSTER_MIN_ATTACK_RANGE) // target is near
		{
			switch(NpcId)
			{
				case MONID_IMP: {
					Monster_attackAction(Npc, ANIM_IMP_ATK, ANIM_IMP_ATK_WAIT, 1.5, S_IMP[0], Target, calcPAtk(IMP_PATK,0,5));
					set_task(1.9, "Monster_attack", taskid, params, 4);
				}
				case MONID_PORTA: {
					Monster_attackAction(Npc, ANIM_PORTA_ATK, ANIM_PORTA_ATKWAIT, 1.5, S_IMP[0], Target, calcPAtk(PORTA_PATK,-4,16)); // @WAIT sounds
					set_task(2.2, "Monster_attack", taskid, params, 4);
				}
				case MONID_EXCURO: {
					Monster_attackAction(Npc, ANIM_EXCURO_ATK, ANIM_EXCURO_ATKWAIT, 1.5, S_IMP[0], Target, calcPAtk(EXCURO_PATK,0,6)); // @WAIT sounds
					set_task(1.8, "Monster_attack", taskid, params, 4);
				}
				case MONID_MORDEO: {
					Monster_attackAction(Npc, ANIM_MORDEO_ATK, ANIM_MORDEO_ATKWAIT, 1.5, S_IMP[0], Target, calcPAtk(MORDEO_PATK,-6,6)); // @WAIT sounds
					set_task(1.6, "Monster_attack", taskid, params, 4);
				}
				case MONID_CATHEROK: {
					Monster_attackAction(Npc, ANIM_CATHEROK_ATK, ANIM_CATHEROK_ATKWAIT, 1.5, S_IMP[0], Target, calcPAtk(CATHEROK_PATK,-12,6)); // @WAIT sounds
					set_task(1.7, "Monster_attack", taskid, params, 4);
				}
				case MONID_TREASURE_CHEST: {
					//@WAIT logic TODO
				}
				case MONID_GUARDIAN_ANGEL: {
					new atk_anim = (random_num(1,2)==1 ? ANIM_ANGEL_ATK1 : ANIM_ANGEL_ATK2);
					Monster_attackAction(Npc, atk_anim, ANIM_ANGEL_ATKWAIT, 1.5, S_IMP[0], Target, calcPAtk(GUARDIAN_ANGEL_PATK,-3,14)); // @WAIT sounds
					set_task(1.9, "Monster_attack", taskid, params, 4);
				}
				case MONID_BONE_PUPPETEER: {
					Monster_attackAction(Npc, ANIM_BONE_PUPPETEER_ATK, ANIM_BONE_PUPPETEER_ATKWAIT, 1.5, S_IMP[0], Target, calcPAtk(BONE_PUPPETEER_PATK,-4,4)); // @WAIT sounds
					set_task(1.9, "Monster_attack", taskid, params, 4);
				}
				case MONID_BEHEMOTH_ZOMBIE: {
					Monster_attackAction(Npc, ANIM_BEHEMOTH_ZOMBIE_ATK, ANIM_BEHEMOTH_ZOMBIE_ATKWAIT, 1.7, S_IMP[0], Target, calcPAtk(BEHEMOTH_ZOMBIE_PATK,-6,20)); // @WAIT sounds
					set_task(2.3, "Monster_attack", taskid, params, 4);
				}
			}
		}
		else if(distance > MONSTER_MIN_ATTACK_RANGE && distance <= MONSTER_MAX_FOLLOW_DIST)
		{
			switch(NpcId){ // move speeds
				case MONID_IMP:					{Monster_run(Npc, Target, ANIM_IMP_RUN, 55);}
				case MONID_PORTA: 				{Monster_run(Npc, Target, ANIM_PORTA_RUN, 15);}
				case MONID_EXCURO:				{Monster_run(Npc, Target, ANIM_EXCURO_RUN, 90);}
				case MONID_MORDEO:				{Monster_run(Npc, Target, ANIM_MORDEO_RUN, 130);}
				case MONID_CATHEROK:			{Monster_run(Npc, Target, ANIM_CATHEROK_RUN, 120);}
				case MONID_GUARDIAN_ANGEL:		{Monster_run(Npc, Target, ANIM_ANGEL_RUN, 95);}
				case MONID_BONE_PUPPETEER:		{Monster_run(Npc, Target, ANIM_BONE_PUPPETEER_RUN, 118);}
				case MONID_BEHEMOTH_ZOMBIE:		{Monster_run(Npc, Target, ANIM_BEHEMOTH_ZOMBIE_RUN, 68);}
			}
			set_task(0.1, "Monster_attack", taskid, params, 4);
		}
		else{ // victim is lost (distance between is very high)
			Monster_lostTarget(Npc, NpcId);
		}
	}
	else{ // victim is lost (teleported/killed/disconnected etc)
		Monster_lostTarget(Npc, NpcId);
	}
}

public Monster_lostTarget(ent, npcid)
{
	Log(DEBUG, "Monster", "lostTarget(ObjId=%d, NpcId=%d)", ent, npcid);
	switch(npcid){
		case MONID_IMP:				{Monster_stopMove(ent, ANIM_IMP_ATK_WAIT);}
		case MONID_PORTA:			{Monster_stopMove(ent, ANIM_PORTA_ATKWAIT);}
		case MONID_EXCURO:			{Monster_stopMove(ent, ANIM_EXCURO_ATKWAIT);}
		case MONID_MORDEO:			{Monster_stopMove(ent, ANIM_MORDEO_ATKWAIT);}
		case MONID_CATHEROK:		{Monster_stopMove(ent, ANIM_CATHEROK_ATKWAIT);}	
		case MONID_GUARDIAN_ANGEL:	{Monster_stopMove(ent, ANIM_ANGEL_ATKWAIT);}	
		case MONID_BONE_PUPPETEER:	{Monster_stopMove(ent, ANIM_BONE_PUPPETEER_ATKWAIT);}	
		case MONID_BEHEMOTH_ZOMBIE:	{Monster_stopMove(ent, ANIM_BEHEMOTH_ZOMBIE_ATKWAIT);}	
	}
	entity_set_float(ent, EV_FL_nextthink, get_gametime() + 2.5); // Back to think() in 2.5 s
}

public Monster_stopMove(ent, anim_atkwait)
{
	Npc_playAnim(ent, anim_atkwait, 1.4);
	set_pev(ent, pev_velocity, Float:{0.0,0.0,0.0});
}

public Monster_run(ent, victim, run_anim, run_speed)
{
	if(pev(ent, pev_sequence) != run_anim)
		set_pev(ent, pev_sequence, run_anim);
	
	// speed = move speed / thinks per second 
	//engfunc(EngFunc_WalkMove, Npc, 0.5, NPC_SPEED / 100.0, WALKMOVE_NORMAL);
	//engfunc(EngFunc_WalkMove, Npc, 0.5, NPC_SPEED / 300.0, WALKMOVE_NORMAL);

	/*
	// ent will move at yaw angle for a distance of dist units, and you cannot change that until it is done.
	engfunc(EngFunc_WalkMove, edict_t *ent, float yaw, float dist, int iMode);
	WALKMOVE_NORMAL		0	// Normal walkmove 
	WALKMOVE_WORLDONLY	1	// Doesn't hit ANY entities, no matter what the solid type 
	WALKMOVE_CHECKONLY	2	// Move, but don't touch triggers  
	*/

	new Float:vic_origin[3], Float:npc_origin[3], Float:angles[3];
	pev(ent, pev_origin, npc_origin);
	pev(victim, pev_origin, vic_origin);

	new Float:dist = get_distance_f(npc_origin, vic_origin);
	if(dist > MONSTER_MIN_ATTACK_RANGE)
	{
		// thanks to DavidJr
		xs_vec_sub(vic_origin, npc_origin, vic_origin); 
		xs_vec_normalize(vic_origin, vic_origin); 
		vector_to_angle(vic_origin, angles); 
		angles[0] = 0.0; 
		entity_set_vector(ent, EV_VEC_angles, angles); 

		static Float:direction[3]; 
		//engfunc(EngFunc_WalkMove, ent, angles[1], 100.0/100.0, WALKMOVE_NORMAL);
		//engfunc(EngFunc_WalkMove, ent, angles[1], float(run_speed)/100.0, WALKMOVE_NORMAL);
		engfunc(EngFunc_WalkMove, ent, angles[1], float(run_speed)/10.0, WALKMOVE_NORMAL);
		angle_vector(angles, ANGLEVECTOR_FORWARD, direction); 
		xs_vec_mul_scalar(direction, 100.0, direction); 
		entity_set_vector(ent, EV_VEC_velocity, direction);
	}
	
	/*new Float:vic_origin[3], Float:npc_origin[3];
	pev(ent, pev_origin, npc_origin);
	pev(victim, pev_origin, vic_origin);

	new Float:velocity[3];
	new Float:dist = get_distance_f(npc_origin, vic_origin);
	if(dist > MONSTER_MIN_ATTACK_RANGE)
	{
		new Float:time = dist / run_speed;
		velocity[0] = (vic_origin[0] - npc_origin[0]) / time;
		velocity[1] = (vic_origin[1] - npc_origin[1]) / time;
		velocity[2] = (vic_origin[2] - npc_origin[2]) / time;
	}
	else{
		velocity[0] = 0.0; velocity[1] = 0.0; velocity[2] = 0.0;
	}
	set_pev(ent, pev_velocity, velocity);*/
}

public Monster_aggrCheck(entid, npcid)
{
	new Float:dist, Float:maxdistance;

	switch(npcid){
		case MONID_MORDEO:			{maxdistance = float(MORDEO_AGGR);}
		case MONID_CATHEROK:		{maxdistance = float(CATHEROK_AGGR);}
		case MONID_GUARDIAN_ANGEL:	{maxdistance = float(GUARDIAN_ANGEL_AGGR);}
		case MONID_BONE_PUPPETEER:	{maxdistance = float(BONE_PUPPETEER_AGGR);}
		default:					{maxdistance = MONSTER_MIN_ATTACK_RANGE + 25.0;}
	}
	npcid = 0; // we write here return value

	for(new id=1; id<=GMaxPlayers; id++){
		if(Util_canSee(entid, id) && isAlive(id))
		{
			dist = fm_entity_range(entid, id); //entity_range(entid, id);
			if(dist <= maxdistance)
			{
				maxdistance = dist;
				npcid = id;
				return npcid;
			}
		}
	}
	return 0;
}
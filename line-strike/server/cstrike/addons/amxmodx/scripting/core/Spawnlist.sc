// --------------------------------------------------
// Spawnlist
// --------------------------------------------------

#define MAP_SPAWNS_AFTER 5.0
#define MAP_SPAWNS_DELAY 0.5

new Trie:TSpawnlist;

public Spawnlist_pluginEnd()
{
	TrieDestroy(TSpawnlist);
}

public Spawnlist_init()
{
	Log(INFO, "Spawnlist", "Initializing Spawnlist");
	
	if(mapIs(MAP_BAIUMS_LAIR)
	|| mapIs(MAP_CRUMA_TOWER)
	/*|| mapIs(MAP_DECK17)*/)
	{
		TSpawnlist = TrieCreate();
		Spawnlist_load();
	}
	else{
		Log(INFO, "Spawnlist", "This map (%s) doesn't have any spawns", GMapName);
	}
}

public bool:Spawnlist_load()
{
	new SQL_ErrorCode, SQL_ErrorString[512];
	new Handle:SqlConnection = SQL_Connect(SQL_Tuple, SQL_ErrorCode, SQL_ErrorString, 511);
	if(SqlConnection == Empty_Handle){
		Log(FATAL, "Database", "MySQL connection failed!");
		pluginFail(SQL_ErrorString, 0);
	}

	new Handle:Query = SQL_PrepareQuery(SqlConnection, "SELECT descr, npcid, loc, heading FROM `spawnlist` WHERE map = '%s';", GMapName);
	if(!SQL_Execute(Query)){
		Log(FATAL, "Spawnlist", "init(): Error on loading `spawnlist` table");
		SQL_QueryError(Query, SQL_ErrorString, 511);
		pluginFail(SQL_ErrorString, 0);
	}

	if(SQL_NumResults(Query) > 0)
	{
		new NpcsCount = 0;
		new Float:params[7];
		params[6] = 0.0;
		new Float:sptime = MAP_SPAWNS_AFTER;
		
		new npcid, loc[64], heading[48];
		new str_x[12], str_y[12], str_z[12], heading_x[12], heading_y[12];
		new desc[256];
		
		while(SQL_MoreResults(Query))
		{
			SQL_ReadResult(Query, 0, desc, 255);
			npcid = SQL_ReadResult(Query, 1);
			SQL_ReadResult(Query, 2, loc, 63);
			SQL_ReadResult(Query, 3, heading, 47);
			
			parse(loc, str_x, 11, str_y, 11, str_z, 11);
			parse(heading, heading_x, 11, heading_y, 11);
			
			params[0] = float(npcid);
			params[1] = str_to_float(str_x);
			params[2] = str_to_float(str_y);
			params[3] = str_to_float(str_z);
			params[4] = str_to_float(heading_x);
			params[5] = str_to_float(heading_y);
			
			set_task(sptime, "Monster_taskedSpawn", NpcsCount+TASK_NPC_SPAWN, _:params, 7);
			sptime += MAP_SPAWNS_DELAY;
			NpcsCount++;
			
			Log(DEBUG, "Spawnlist", "load(%s): npcid=%d, loc=%s, heading=%s", desc, npcid, loc, heading);
			
			//native TrieSetCell(Trie:handle, const key[], any:value);
			
			SQL_NextRow(Query);
			Log(DEBUG, "Spawnlist", "load(): SQL_NextRow");
		}
		
		Log(INFO, "Spawnlist", "%d tasked mob spawns in %.1f s", NpcsCount, sptime);
	}
	else{
		Log(ERROR, "Spawnlist", "init(): No results for exist map %s", GMapName);
	}
}
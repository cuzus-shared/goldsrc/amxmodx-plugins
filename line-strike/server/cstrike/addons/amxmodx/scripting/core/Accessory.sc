// --------------------------------------------------
// Accessory
// --------------------------------------------------

new EntPlayerAccessory[XX];

public haveAccessory(id)
{
	if(EntPlayerAccessory[id] > 0 && pev_valid(EntPlayerAccessory[id]))
		return true;
	return false;
}

public haveAccessoryDb(id)
{
	if(PlayerData[id][HAIR] > 0)
		return true;
	return false;
}

enum _:ACCESSORIES
{
    H_LITTLE_ANGEL_WINGS = 0,		// 0
    H_ARTISAN_GOGGLES,				// 1
	H_CAT_EARS,						// 2
	H_LORD_CROWN,					// 3
	H_DEMON_CIRCLET,				// 4
	H_DEMON_HORNS,					// 5
	H_FAIRY_ANTENNA,				// 6
	H_FEATHERED_HAT,				// 7
	H_HERO_CIRCLET,					// 8
	H_CIRCLET_OF_ICE_FAIRY_SIRRA,	// 9
	H_JESTER_HAT,					// 10
	H_NOBLESSE_TIARA,				// 11
	H_PARTY_HAT,					// 12
	H_PARTY_MASK,					// 13
	H_RED_PARTY_MASK,				// 14
	H_PIRATE_HAT,					// 15
	H_RABBIT_EARS,					// 16
	H_SANTAS_HAT,					// 17
	H_VALAKAS_SLAYER_CIRCLET,		// 18
	H_WIZARD_HAT					// 19
};

public Accessory_attach(id)
{
	id -= TASK_ATTACH_ACCESSORY;

	if(haveAccessoryDb(id))
	{
		Accessory_checkedRemove(id); // remove if created/attached already
		Accessory_create(id);
	}
	else
		Log(DEBUG, "Accessory", "attach(): Player have not accessory to attach");
}

public Accessory_create(id)
{
	/*if(EntPlayerAccessory[id] > 0) // if player already have hat ent
	{
		fm_set_entity_visibility(EntPlayerAccessory[id], false); // set invis
	}*/

	//submodel IDs (from *mdl) :  0-19
	new submodel = PlayerData[id][HAIR] - 1; // 0 = havent accessory
	//submodel(mdl:0) = 1 - 1
	//submodel(mdl:1) = 2 - 1
	// ...
//	Log(DEBUG, "Accessory", "'%s' id=%d, submodel=%d", getName(id), PlayerData[id][HAIR], submodel);
	
	if(submodel < 0 || submodel > H_WIZARD_HAT)
		return;

	EntPlayerAccessory[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));

	if(EntPlayerAccessory[id] > 0)
	{
		set_pev(EntPlayerAccessory[id], pev_classname, "accessory");
		engfunc(EngFunc_SetModel, EntPlayerAccessory[id], M_L2ACCESSORY);
		set_pev(EntPlayerAccessory[id], pev_body, submodel);
		set_pev(EntPlayerAccessory[id], pev_movetype, MOVETYPE_FOLLOW);
		set_pev(EntPlayerAccessory[id], pev_aiment, id);
		set_pev(EntPlayerAccessory[id], pev_owner, id);
		set_pev(EntPlayerAccessory[id], pev_rendermode, kRenderNormal);
	}
	else
		Log(ERROR, "Accessory", "Can't create accessory ent for player '%s'", getName(id));

	//fm_set_entity_visibility(EntPlayerAccessory[id], 1); // set vis
}

public Accessory_checkedRemove(id)
{
	if(haveAccessory(id)){
//		Log(DEBUG, "Accessory", "Removing accessory (ID=%d) from player '%s'", EntPlayerAccessory[id], getName(id));
		entityDispose(EntPlayerAccessory[id]);
		EntPlayerAccessory[id] = 0;
	}
//	else
//		Log(WARN, "Accessory", "remove(): Entity (ID=%d) is invalid or not exists", EntPlayerAccessory[id]);
}

/*stock fm_set_entity_visibility(id, bool:visible=1)
{
	set_pev(id, pev_effects, visible == 1 ? pev(id, pev_effects) & ~EF_NODRAW : pev(id, pev_effects) | EF_NODRAW);
}*/
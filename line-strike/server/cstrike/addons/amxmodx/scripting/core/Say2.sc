// --------------------------------------------------
// Say2
// --------------------------------------------------

// 255 112 0	![message]	Says sentence in Shout channel. This sentence is seen by any character in distance of several hundreds steps.
// 218 215 218	[message]	Says sentence in General channel. This sentence is seen by any character in distance of several steps.
// 				+[message]	Says sentence in Trade channel. This sentence is seen by any character in distance of several hundreds steps.
// 0 249 0		#[message]	Says sentence in Party channel. This sentence is seen by any character in party at any distance.
// 125 117 255	@[message]	Says sentence in Clan channel. This sentence is seen by any character in clan at any distance.
// 				$[message]	Says sentence in Alliance channel. This sentence is seen by any character in alliance at any distance.
// 				"[character] [message]	Says sentence to chosen character in private chat Whisper channel. This sentence is seen only by chosen char at any distance
// 64 138 255	%[message]	Says sentence in Hero channel. This sentence is seen by any character currently online.

// 174 152 121	SYSTEM
// 128 251 255	Announcements

enum _:SAY2_VALUES
{
    S2_PLAYING_HAND = 0,
    S2_INVUL,		//1
	S2_INVIS,		//2
	S2_RIDE,		//3
	S2_FLY,			//4
	S2_SUPER_HASTE,	//5
	S2_SQUEAK,		//6
	S2_WTF,			//7
	S2_ARROW_CAM,	//8
//	S2_EXP_GAIN		//9
};
new SAY2_DATA[XX][SAY2_VALUES];

new Float:Say2_SqueakNextStep[XX];
new EntHeroGlow[XX];

#define SAY2_SEND_MESSAGE_DELAY	0.75
new Say2_FloodProtectMult[XX] = {0, ...};
new Float:Say2_FloodProtectTime[XX] = {0.0, ...};

stock bool:inWtfMode(id) /* Skills WTF Mode */
{
	if(isGM(id) && SAY2_DATA[id][S2_WTF])
		return true;
	return false;
}

public Say2_clientDisconnect(id)
{
	//if(isGM(id)){
	SAY2_DATA[id][S2_PLAYING_HAND] = true;

	//if(isHero(id)){	// todo
	heroGlowRemove(id);
//	Log(DEBUG, "Say2", "heroGlowRemove(id);");

	SAY2_DATA[id][S2_INVUL] = false;
	SAY2_DATA[id][S2_INVIS] = false;

	SAY2_DATA[id][S2_SUPER_HASTE] = false;
	
	SAY2_DATA[id][S2_WTF] = false;

	SAY2_DATA[id][S2_SQUEAK] = false;
	Say2_SqueakNextStep[id] = 0.0;
	
//	SAY2_DATA[id][S2_EXP_GAIN] = false;
}

public Say2_hamPlayerKilledPost(id, killer, gib)
{
	SAY2_DATA[id][S2_INVUL] = false;
	SAY2_DATA[id][S2_INVIS] = false;

	//SAY2_DATA[id][S2_SUPER_HASTE] = false;
	
	//SAY2_DATA[id][S2_SQUEAK] = false;
	//Say2_SqueakNextStep[id] = 0.0;
}

public Say2_hookSay(id) // All chat messages are hooked here
{
	// TODO: if !chatBanned @WAIT db gets
	//	activeChar.sendMessage("You may not chat while a chat ban is in effect.");

	// Flood Protector
	//<!--
	new Float:time_now = get_gametime();
	if(Say2_FloodProtectTime[id] > time_now)
	{
		if(Say2_FloodProtectMult[id] >= 3){
			client_print(id, print_center, "You cannot speak too fast.");
			Say2_FloodProtectTime[id] = time_now + SAY2_SEND_MESSAGE_DELAY + 3.0;
			return PLUGIN_HANDLED;
		}

		Say2_FloodProtectMult[id]++;
	}
	else if(Say2_FloodProtectMult[id]){
		Say2_FloodProtectMult[id]--;
	}
	Say2_FloodProtectTime[id] = time_now + SAY2_SEND_MESSAGE_DELAY;
	//-->

	new message[192];
	read_args(message, 191);
	remove_quotes(message); // "some text" ==> some text
	trim(message);

	new arg1[64], arg2[64], arg3[64], arg4[64], arg5[64], arg6[64];
	parse(message, arg1, charsmax(arg1), arg2, charsmax(arg2), arg3, charsmax(arg3), arg4, charsmax(arg4), arg5, charsmax(arg5), arg6, charsmax(arg6));

	new msglen = strlen(message);
	if(msglen < 2)
		return PLUGIN_HANDLED;
	
	switch(message[0]) // read first symbol
	{
		case '!', '+' : // shout, trade
		{
			Util_colorPrint(id, "/ctrChannel is not implemented.");
			return PLUGIN_HANDLED;
		}
		case '#' : // party (team)
		{
			replace(message, charsmax(message), "#", "");
			for(new pl = 1; pl <= GMaxPlayers; pl++){
				if(isConnected(pl) && !isBot(pl) && getTeam(id) == getTeam(pl))
					Util_colorPrint(pl, "/g%s: %s", getName(id), message);
			}
			return PLUGIN_HANDLED;
		}
		case '@' : 
		{
			// TODO : auto-find team tag in nicknames and send this msg to players with it
			// клановые теги определяются только символами [ ] а клан тем что внутри
			Util_colorPrint(id, "/ctrChannel is not implemented.");
			return PLUGIN_HANDLED;
		}
		case '%' : 
		{
			// TODO : ADM-only or PREMIUMS blue(hero) HUD Message
			Util_colorPrint(id, "/ctrChannel is not implemented.");
			return PLUGIN_HANDLED;
		}
		case '$' : // gmchat (announcement)
		{
			if(isGM(id)){
				//new player[MAX_CHARNAME_LENGTH];
				//get_user_name(id, player, MAX_CHARNAME_LENGTH-1);
				set_hudmessage(128,251,255, 0.008, 0.76, 0, 6.0, 5.0); 
				//set_hudmessage(r,g,b,      x, y,   effects, fxtime, holdtime, fadeintime, fadeouttime, channel)
				/* x - слева направо, 0.0 слева, 1.0 справа, -1.0 по центру */
				/* y - идёт сверху вниз, 0.0 сверху, 1.0 снизу, -1.0 по центру */
				ShowSyncHudMsg(0, randInt(1,2)==1 ? KillStreakSyncHud1 : KillStreakSyncHud2, "Announcements: %s" /*, player*/, message);

				//client_print(id, print_chat, "Usage: //announce <Your announcement here>");
			}
			return PLUGIN_HANDLED;
		}
		case '"' : // private message
		{
			new receiver[MAX_CHARNAME_LENGTH];
			new receiver_notsens[MAX_CHARNAME_LENGTH];

			copyc(receiver, MAX_CHARNAME_LENGTH-1, message, ' ');
			replace(receiver, MAX_CHARNAME_LENGTH-1, "^"", "");		//	==> PlayerName

			Log(DEBUG, "Say2", "receiver(%s)", receiver);

			replace(message, charsmax(message), receiver, "");
			replace(message, charsmax(message), "^" ", ""); // bugfix

			Log(DEBUG, "Say2", "message(%s)", message);

			new bool:ex = false;
			
			for(new pl=1; pl<=GMaxPlayers; pl++){
				if(isConnected(pl)){
					get_user_name(pl, receiver_notsens, MAX_CHARNAME_LENGTH-1);
					if(equali(receiver_notsens, receiver)){
						Log(DEBUG, "Say2", "if(equali(%s, %s))", receiver_notsens, receiver);
						format(receiver, MAX_CHARNAME_LENGTH-1, "%s", receiver_notsens);
						ex = true;
						break;
					}
				}
			}

			if(ex) // receiver logged in
			{
				get_user_name(id, receiver_notsens, MAX_CHARNAME_LENGTH-1); // sender name
				
				if(equali(receiver_notsens, receiver)){
					client_print(id, print_chat, "Are you going crazy?");
				}
				else{
					Util_colorPrint(id, "/ctr->%s: %s", receiver, message);
					Util_colorPrint(get_user_index(receiver), "/ctr->%s: %s", receiver_notsens, message);
				}
			}
			else 
				client_print(id, print_chat, "%s is not currently logged in.", receiver);

			return PLUGIN_HANDLED;
		}
		case '/' : // chat commands
		{
			switch(message[1]) // second symbol
			{
				case '/' : // Admin commands
				{
					if(isGM(id))
					{
					// server related
						if(equali(arg1, "//grav")){
							admcmd_Gravity(id, arg2);
						}
					// other
						else if(equali(arg1, "//spawn")){
							admcmd_Spawn(id, arg2, arg3);
						}
						else if(equali(arg1, "//delete")){
							admcmd_Delete(id, str_to_num(arg2));
						}
						else if(equal(message, "//admin")){
							admcmd_Admin(id);
						}
						else if(equal(arg1, "//getid")){
							admcmd_GetId(id);
						}
						else if(equal(arg1, "//kick")){
							admcmd_Kick(id, str_to_num(arg2));
						}
						else if(equal(arg1, "//map")){
							admcmd_ChangeLevel(id, arg2);
						}
						else if(equal(arg1, "//shutdown")){
							admcmd_Shutdown(id, str_to_num(arg2));
						}
						else if(equal(arg1, "//re")){
							admcmd_Restart(id);
						}
						else if(equal(arg1, "//drop")){
							admcmd_Drop(id, arg2);
						}
						else if(equal(arg1, "//setadena")){
							admcmd_SetAdena(id, str_to_num(arg2));
						}
						else if(equal(arg1, "//setlevel")){
							admcmd_SetLevel(id, str_to_num(arg2));
						}
						else if(equal(arg1, "//kill")){
							admcmd_Kill(id, str_to_num(arg2));
						}
						else if(equal(arg1, "//get")){
							admcmd_Get(id, arg2);
						}
						else if(equal(arg1, "//para")){
							admcmd_Para(id);
						}
						else if(equal(arg1, "//unpara")){
							admcmd_UnPara(id);
						}
						else if(equal(arg1, "//setlights")){
							admcmd_SetLights(id, str_to_num(arg2));
						}
						else if(equal(arg1, "//dynlights")){
							admcmd_DynLights(id);
						}
						else if(equal(arg1, "//setskin")){
							admcmd_SetSkin(id, str_to_num(arg2));
						}
						else if(equal(arg1, "//moveto")){
							admcmd_MoveTo(id, str_to_num(arg2), str_to_num(arg3), str_to_num(arg4));
						}
						else if(equal(arg1, "//go")){
							admcmd_Go(id, arg2);
						}
						else if(equal(arg1, "//ambpoints")){
							admcmd_AmbPoints(id, str_to_num(arg2), str_to_num(arg3));
						}
						else if(equal(arg1, "//banchat")){
							admcmd_BanChat(get_user_index(arg2), str_to_num(arg3));
						}
						else if(equal(arg1, "//unbanchat")){
							admcmd_UnBanChat(get_user_index(arg2));
						}
						else if(equal(message, "//superhaste")){
							admcmd_SuperHaste(id, str_to_num(arg2));
						}
						else if(equal(message, "//sethero")){	// ? move to <Olympiad>
							admcmd_SetHero(id);
						}
						else if(equal(message, "//mapents")){
							admcmd_MapEnts(id);
						}
						else if(equal(message, "//invul")){
							admcmd_Invul(id);
						}
						else if(equal(message, "//invis")){
							admcmd_Invis(id);
						}
						else if(equal(message, "//fly")){
							amdcmd_Fly(id);
						}
						else if(equal(message, "//ride")){
							admcmd_Ride(id);
						}
						else if(equal(message, "//noweapon")){
							set_pev(id, pev_viewmodel2, "");
							set_pev(id, pev_weaponmodel2, "");
						}
						else if(equal(arg1, "//heal")){
							admcmd_Heal(id, str_to_num(arg2));
						}
						// TODO: move to //heal --> add arg3 for radius
						else if(equal(arg1, "//radheal")){
							admcmd_HealInRadius(id, str_to_num(arg2));
						}
						else if(equal(message, "//removeweather")){
							admcmd_RemoveWeather(id);
						}
						else if(equal(arg1, "//partburst")){
							admcmd_ParticleBurst(id); // todo: arg2,3 using / if no arg2 -> randoms
						}
						else if(equal(arg1, "//dlight")){
							admcmd_DLight(id, str_to_num(arg2), str_to_num(arg3), str_to_num(arg4), str_to_num(arg5), str_to_num(arg6));
						}
						else if(equal(message, "//squeak")){ // @l2: squeakin shoes sound
							admcmd_Squeak(id);
						}
						else if(equal(arg1, "//pod")){
							//pod <kick t/ct/(random)>
							//pod <killall>
							// TODO: //pod <add t/ct>
							admcmd_PodCommands(id, arg2, arg3, arg4);
						}
						else if(equal(arg1, "//banperm")){
							admcmd_BanPerm(id, arg2, arg3, arg4);
						}
						// TODO
						else if(equal(message, "//wtfmode")){
							admcmd_WtfMode(id);
						}
					}
					return PLUGIN_HANDLED;
					/*
					todo@new :
						http://ya-cs.ru/plugins/773-admin-esp-mini-v15.html
						http://next21.ru/2013/07/sprite-esp/
						http://www.servforcs.ru/protection/165-format_player.html
						https://forums.alliedmods.net/showthread.php?t=106063
						https://forums.alliedmods.net/showthread.php?t=206688		Destroy
						http://amxservera.com/plugins/1723-icqadmins-10.html
						http://amxservera.com/plugins/1125-fucker-evil-v19.html
						http://amxservera.com/plugins/769-gm-screens.html
						http://amxservera.com/plugins/343-game-destroyer.html
						http://amxservera.com/plugins/229-amx_bancs.html
						quit_cs menu
						eject_cd
						Admin Screen 2
						amx_idiot
						admin/Migraine.amxx				; to Cmd --> test
						http://vcsgame.ru/load/43-1-0-246
						http://next21.ru/2014/05/%D0%BC%D0%BE%D0%B4%D1%83%D0%BB%D1%8C-inkonita-eye/
						//todo: podbot adm commands @chk	 misc_and_unused/PodBotMenu.sma
						
					todo@l2 :
						//teleport_to_character <kadar> 	//teleto
						//recall <kadar>
						//bighead
					*/
				}
				default: // User commands (any symbol after first slash)
				{
					if(warmupConfirmed() || isGM(id))
					{
						// Main menu
						if(equali(message, "/menu") || equali(message, "/class")){
							if(!AllMenusBlocked[id])
								usrcmd_MainMenu(id);
						}
						else if(equal(message, "/sit")){
							usrcmd_Sit(id);
						}
						else if(equal(message, "/stand")){
							usrcmd_Stand(id);
						}
						// Camera
						else if(equali(message, "/cam") || equali(message, "/camera")){
							Camera_change(id); // <Camera>
						}
						else if(equali(message, "/mpz")){
							usrcmd_ArrowChase(id);
						}
						else if(equali(message, "/hand")){
							usrcmd_HandSwitch(id);
						}
						// awe..Some from DotA
						else if(equali(message, "/ms") || equali(message, "/movespeed")){
							usrcmd_DMoveSpeed(id);
						}
						else if(equali(message, "/msf")){
							usrcmd_DMoveSpeedFloat(id);
						}
						else if(equali(message, "/roll")){
							usrcmd_DRoll(id);
						}
						else if(equali(message, "/matchup") || equali(message, "/ma")){
							usrcmd_DMatchup(id);
						}
						else if(equali(message, "/richtop") || equali(message, "/rt")){
							usrcmd_RichTop(id);
						}
						// Lineage II chat commands
						else if(equali(message, "/gmlist")){
							usrcmd_GmList(id);
						}
						else if(equali(message, "/loc")){
							usrcmd_Loc(id);
						}
						else if(equali(message, "/locf")){
							usrcmd_LocFloat(id);
						}
						else if(equali(message, "/locv")){ // todo: MOVE to GM cmd
							usrcmd_LocV(id);
						}
						else if(equali(message, "/time")){
							usrcmd_Time(id);
						}
						else if(equali(message, "/unstuck")){ // NOT IMPL
							usrcmd_Unstuck(id);
						}
						/*else if(equali(message, "/xpon") || equali(message, "/expon")){
							usrcmd_ExpOn(id);
						}
						else if(equali(message, "/xpoff") || equali(message, "/expoff")){
							usrcmd_ExpOff(id);
						}*/
						else if(equali(message, "/unsummon")){
							if(getClass(id) == CLASS_F)
								Summon_killed(id); // have removeSummonedUnit() inside
							else
								removeSummonedUnit(id); // <Summon>
						}
					}
					return PLUGIN_HANDLED;
				}
			//-->
			}
		}
	}

	// Default channel (white)
	/*
	new from[MAX_CHARNAME_LENGTH];
	get_user_name(id, from, MAX_CHARNAME_LENGTH-1);
	replace(message, charsmax(message), "^"", "");	// "NAME some message ==> NAME some message
	replace(message, charsmax(message), from, "");	// "NAME some message ==>  some message
	replace(message, charsmax(message), " ", "");	// " some message ==> some message
	*/
	
	/*
	// get sender team
	new player_team[10];
	get_user_team(id, player_team, 9);
	// temporarily change team
	message_begin(MSG_ONE, gmsgTeamInfo, _, id);
	write_byte(id);
	write_string("SPECTATOR");
	message_end();
	// send message
	message_begin(MSG_ONE, gmsgSayText, {0,0,0}, id);
	write_byte(id);
	write_string(message);
	message_end();
	// back sender team
	message_begin(MSG_ONE, gmsgTeamInfo, _, id);
	write_byte(id);
	write_string(player_team);
	message_end();
	*/
	
	//https://forums.alliedmods.net/showthread.php?t=9213
	//http://amx-x.ru/viewtopic.php?f=8&t=32625
	// TODO: empty message checks! (spaces like " " , "  ", etc)
	new string[512] = "^x01";
	new players_list[32];
	new players_num;
	get_players(players_list, players_num, "ch");
	
	new sender[MAX_CHARNAME_LENGTH];
	get_user_name(id, sender, MAX_CHARNAME_LENGTH-1);
	add(string, 511, sender);
	add(string, 511,": ");
	add(string, 511, message, (511 - strlen(string)));
	
	for(new i=0; i < players_num; i++)
	{
		message_begin(MSG_ONE, gmsgSayText, {0,0,0}, players_list[i])
		write_byte(players_list[i]);
		write_string(string);
		message_end();

		console_print(players_list[i], string);
	}

	return PLUGIN_HANDLED; //PLUGIN_CONTINUE
}

public usrcmd_MainMenu(id)
{
	if(!AllMenusBlocked[id])
	{
		if(GameMode == MODE_LAST_HERO){
			classMenuShow(id);
		}else{
			if(canShowMenu(id))
				showMainMenu(id);
		}
	}
}

public usrcmd_HandSwitch(id)
{
	if(SAY2_DATA[id][S2_PLAYING_HAND]){
		SAY2_DATA[id][S2_PLAYING_HAND] = false;
		client_cmd(id, "cl_righthand 0");
		client_print(id, print_center, "Setting your playing hand to left.");
	}
	else if(!SAY2_DATA[id][S2_PLAYING_HAND]){
		SAY2_DATA[id][S2_PLAYING_HAND] = true;
		client_cmd(id, "cl_righthand 1");
		client_print(id, print_center, "Setting your playing hand to right.");
	}
}

public usrcmd_DMoveSpeed(id)
{
	client_print(id, print_chat, "Your movement speed is %d", floatround(get_user_maxspeed(id)));
}
public usrcmd_DMoveSpeedFloat(id)
{
	client_print(id, print_chat, "Your movement speed is %f", get_user_maxspeed(id));
}

public usrcmd_DRoll(id)
{
	client_print(0, print_chat, "%s has rolled %d out of 100.", getName(id), randInt(1, 100));
}

public usrcmd_DMatchup(player)
{
	if(warmupConfirmed())
		for(new id=1; id <= GMaxPlayers; id++)
			if(haveClass(id)) // <Player>
			{
				new name[MAX_CHARNAME_LENGTH];
				get_user_name(id, name, MAX_CHARNAME_LENGTH-1);
				client_print(player, print_chat, "%s (%s Lv %d)", name, getClassName(id), getLevel(id));
				client_print(player, print_console, "%s (%s Lv %d)", name, getClassName(id), getLevel(id));
			}
}

public usrcmd_RichTop(id)
{
	if(warmupConfirmed())
		for(new pl=1; pl<=GMaxPlayers; pl++)
			if(isConnected(pl) && haveTeam(pl))
			{
				new name[MAX_CHARNAME_LENGTH];
				get_user_name(pl, name, MAX_CHARNAME_LENGTH-1);
				// TODO: sort max to min
				client_print(id, print_chat, "%s (%d Adena)", name, Stats_getTotalAdena(pl));
				client_print(id, print_console, "%s (%d Adena)", name, Stats_getTotalAdena(pl));
				// THINK: show from motd window?
			}
}

// ----------------------------------------------------------------------------

public usrcmd_GmList(player)
{
	client_print(player, print_chat, "===<GM List>===");
	for(new id=1; id <= GMaxPlayers; id++)
	{
		new name[MAX_CHARNAME_LENGTH];
		get_user_name(id, name, MAX_CHARNAME_LENGTH-1);
		
		if(!equali(name, "HLTV Proxy"))
			if(isGM(id))
				client_print(player, print_chat, "%s", name);
	}
}

public usrcmd_Loc(id) 
{
	static Float:loc[3];
	pev(id, pev_origin, loc);
	client_print(id, print_chat, "Current location: %d, %d, %d", floatround(loc[0],floatround_round), floatround(loc[1],floatround_round), floatround(loc[2],floatround_round));
	client_print(id, print_console, "Current location: %d, %d, %d", floatround(loc[0],floatround_round), floatround(loc[1],floatround_round), floatround(loc[2],floatround_round));
}
public usrcmd_LocFloat(id)
{
	static Float:loc[3];
	pev(id, pev_origin, loc);
	client_print(id, print_chat, "Current location: %f, %f, %f", loc[0], loc[1], loc[2]);
	client_print(id, print_console, "Current location: %f, %f, %f", loc[0], loc[1], loc[2]);
}
public usrcmd_LocV(id)
{
	new Float:origin[3];
	pev(id, pev_origin, origin);
	client_print(id, print_chat, "Origin: %f, %f, %f", origin[0], origin[1], origin[2]);
	client_print(id, print_console, "Origin: %f, %f, %f", origin[0], origin[1], origin[2]);
	pev(id, pev_angles, origin);
	client_print(id, print_chat, "Angles: %f, %f, %f", origin[0], origin[1], origin[2]);
	client_print(id, print_console, "Angles: %f, %f, %f", origin[0], origin[1], origin[2]);
}

public usrcmd_Time(id)
{
	new hh, mm, ss;
	time(hh,mm,ss);
	client_print(id, print_chat, "Time: %02d:%02d", hh, mm);
}

public usrcmd_Unstuck(id)
{
	client_print(id, print_chat, "Not implemented yet!");
}

/*
// TODO: move to 1 func like others
public usrcmd_ExpOn(id)
{
	SAY2_DATA[id][S2_EXP_GAIN] = true;
	client_print(id, print_chat, "Expirience gain is enabled.");
}
public usrcmd_ExpOff(id)
{
	SAY2_DATA[id][S2_EXP_GAIN] = false;
	client_print(id, print_chat, "Expirience gain is disabled.");
}
*/

public usrcmd_Sit(id)
{
	Player_sit(id);
	// TODO: block move / attack / skills / ...
}

public usrcmd_Stand(id)
{
	Player_stand(id);
	// TODO: unblock ...
}

public usrcmd_ArrowChase(id)
{
	if(!SAY2_DATA[id][S2_ARROW_CAM]){
		SAY2_DATA[id][S2_ARROW_CAM] = true;
		client_print(id, print_center, "Your camera will chase the arrows.");
	}else{
		SAY2_DATA[id][S2_ARROW_CAM] = false;
		client_print(id, print_center, "Default camera mode.");
	}
}

public Say2_cameraBack(id)
{
	id -= TASK_CAMERA_ARROW_CHASE;
	engfunc(EngFunc_SetView, id, id);
}

public admcmd_Gravity(id, arg[])
{
	new Float:value = str_to_float(arg);
	server_cmd("sv_gravity %f", value);
	client_print(id, print_center, "Setting gravity to .1f", value);
}

public admcmd_Spawn(id, npc[], params[])
{
	// NPC
	if(equali(npc, "clarissa") /*|| equali(str_to_float(npc), NPCID_CLARISSA)*/){
		Npc_spawn(id, NPCID_GATEKEEPER);
	}
	else if(equali(npc, "taurin")){
		Npc_spawn(id, NPCID_WAREHOUSE);
	}
	else if(equali(npc, "fmagic")){
		FMagic_spawn(id); // <Npc>
	}
	// Monster
	else if(equali(npc, "imp")){
		Monster_spawn(id, MONID_IMP);}
	else if(equali(npc, "porta")){
		Monster_spawn(id, MONID_PORTA);}
	else if(equali(npc, "excuro")){
		Monster_spawn(id, MONID_EXCURO);}
	else if(equali(npc, "mordeo")){
		Monster_spawn(id, MONID_MORDEO);}
	else if(equali(npc, "catherok")){
		Monster_spawn(id, MONID_CATHEROK);}
	else if(equali(npc, "chest")){
		Monster_spawn(id, MONID_TREASURE_CHEST);}
	
	/*else if(equali(npc, "avanpost")){
		//&& params is T/CT
	}
	else if(equali(npc, "guard")){
		//&& params is T/CT
	}
	*/
}

public admcmd_Delete(id, radius)
{
	if(radius > 0 && radius <= 65535)
	{
		new ent = -1;
		new i=0, p=0;
		new Float:origin[3];
		pev(id, pev_origin, origin);
		while((ent = engfunc(EngFunc_FindEntityInSphere, ent, origin, float(radius))) != 0)
		{
			new this[32];
			pev(ent, pev_classname, this, 31);
			if(pev_valid(ent) && 
				(equali(this, "env_glow")) ||
				(equali(this, "env_sprite")) ||
				(equali(this, "ambient_generic")) ||
				(equali(this, "cycler")) ||
				(equali(this, "cycler_sprite")))
			{
				entityDispose(ent);
				i++;
			}
			else if(equali(this, "player")){
				p++;
				continue;
			}
		}
		if(p > 0){client_print(id, print_chat, "Skipped %d players", p);}
		client_print(id, print_chat, "Deleted %d ents in radius %d", i, radius);
	}else{
		client_print(id, print_chat, "Usage: //delete <radius(0-65535)>");
	}
}

// ----------------------------------------------------------------------------
public admcmd_SuperHaste(id, val)
{
	val = (val <= 0 || val > 4) ? 4 : val;
	new power[5];

	switch(val){
		case 1: format(power, 4, "800");
		case 2: format(power, 4, "1000");
		case 3: format(power, 4, "1200");
		case 4: format(power, 4, "1400");
	}

	client_print(id, print_chat, "[DEBUG] val=%d, power=%s", val, power);

	/*Default and legal:
	cl_forwardspeed "400"
	cl_backspeed "400"
	cl_sidespeed "400"
	cl_upspeed "320"
	cl_yawspeed "210"*/

	if(!SAY2_DATA[id][S2_SUPER_HASTE]){
		client_cmd(id, "cl_forwardspeed %s", power);
		client_cmd(id, "cl_backspeed %s", power);
		client_cmd(id, "cl_sidespeed %s", power);
		SAY2_DATA[id][S2_SUPER_HASTE] = true;
		client_print(id, print_center, "You use Super Haste.");
	}else{
		SAY2_DATA[id][S2_SUPER_HASTE] = false;
		client_cmd(id, "cl_forwardspeed 400");
		client_cmd(id, "cl_backspeed 400");
		client_cmd(id, "cl_sidespeed 400");
		client_print(id, print_center, "Super Haste has been aborted.");
	}
}

public admcmd_SetLevel(id, level)
{
	if(level > 0 && level <= 78)
	{
		new target, body;
		get_user_aiming(id, target, body, 9999);
		if(!target)
			target = id;
		setLevelWithExp(target, level);
		client_print(id, print_chat, "Gained %d level to %s", level, getName(target));
	}else{
		client_print(id, print_chat, "Usage: //setlevel <1-78>");
	}
}

public admcmd_SetHero(id)
{
	new target, body;
	get_user_aiming(id, target, body, 9999); //Float: get_user_aiming ( index, &id, &body, [ distance = 9999 ] )
	if(target){
		id = target;
	}

	if(EntHeroGlow[id]){
		heroGlowRemove(id);
	}
	else
	{
		EntHeroGlow[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));
		set_pev(EntHeroGlow[id], pev_classname, "hero_glow");

		engfunc(EngFunc_SetModel, EntHeroGlow[id], SPR_HEROGLOW);
		engfunc(EngFunc_SetSize, EntHeroGlow[id], Float:{0.0,0.0,0.0}, Float:{0.0,0.0,0.0});

		set_pev(EntHeroGlow[id], pev_solid, SOLID_NOT);
		set_pev(EntHeroGlow[id], pev_movetype, MOVETYPE_FOLLOW);
		set_pev(EntHeroGlow[id], pev_aiment, id);
		set_pev(EntHeroGlow[id], pev_rendermode, 5); // 5 - Additive
		set_pev(EntHeroGlow[id], pev_renderamt, 60.0);
		set_pev(EntHeroGlow[id], pev_renderfx, 16); // 16 - Hologram (Distort + fade)
		set_pev(EntHeroGlow[id], pev_scale, 0.8);
		
		//set_pev(EntHeroGlow[id], pev_rendercolor, 255,0,0);
		
		set_task(0.1, "heroGlowTask", id+TASK_SAY2_HERO_GLOW, _, _, "b");

		Log(INFO, "Say2", "Hero Glow added to '%s'", getName(id));
	}
}

public heroGlowTask(id)
{
	id -= TASK_SAY2_HERO_GLOW;
	Create_TE_DLIGHT(id, 9, 155,155,100, 1, 5);
	//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)
}

public heroGlowRemove(id)
{
	if(EntHeroGlow[id])
	{
		entityDispose(EntHeroGlow[id]);
		EntHeroGlow[id] = 0;

		if(task_exists(id+TASK_SAY2_HERO_GLOW))
			remove_task(id+TASK_SAY2_HERO_GLOW);

		Log(INFO, "Say2", "Hero Glow removed from '%s'", getName(id));
	}
}

public admcmd_Admin(id)
{
	client_print(id, print_chat, "Hacking attempt! GM reported...");
	Log(WARN, "Say2", "Player '%s' trying to use //admin command", getName(id));
}

public admcmd_GetId(id)
{
	new target, body, authid[35];
	get_user_aiming(id, target, body, 9999); //Float: get_user_aiming ( index, &id, &body, [ distance = 9999 ] )

	if(target){
		new name[MAX_CHARNAME_LENGTH];
		get_user_name(target, name, MAX_CHARNAME_LENGTH-1);
		body = getIdFromName(name);
		get_user_authid(target, authid, 34);
		client_print(id, print_chat, "%d (%s)  %s", body, authid, name);
		client_print(id, print_console, "%d (%s)  %s", body, authid, name);
	}
	else{
		for(new p = 1; p <= GMaxPlayers; p++)
			if(isConnected(p)){
				new name[MAX_CHARNAME_LENGTH];
				get_user_name(p, name, MAX_CHARNAME_LENGTH-1);
				body = getIdFromName(name);
				get_user_authid(p, authid, 34);
				client_print(id, print_chat, "%d (%s)  %s", body, authid, name);
				client_print(id, print_console, "%d (%s)  %s", body, authid, name);
			}
	}
}

public admcmd_Invul(id)
{
	if(!SAY2_DATA[id][S2_INVUL]){
		SAY2_DATA[id][S2_INVUL] = true;
		set_user_godmode(id, 1);
		client_print(id, print_chat, "You is now invulnerable");
	}else{
		SAY2_DATA[id][S2_INVUL] = false;
		set_user_godmode(id, 0);
		client_print(id, print_chat, "You is now mortal");
	}
}

public admcmd_Invis(id)
{
	if(!SAY2_DATA[id][S2_INVIS]){
		SAY2_DATA[id][S2_INVIS] = true;
		fm_set_user_rendering(id, kRenderFxGlowShell, 0,0,0, kRenderTransAlpha, 0);
		set_user_footsteps(id, 1); // silent move
		client_print(id, print_chat, "Invisible mode enabled");
	}else{
		SAY2_DATA[id][S2_INVIS] = false;
		fm_set_user_rendering(id);
		set_user_footsteps(id, 0);
		client_print(id, print_chat, "Invisible mode turned off");
	}
}

public amdcmd_Fly(id)
{
	// TODO: wyvern model
	if(!SAY2_DATA[id][S2_FLY]){
		SAY2_DATA[id][S2_FLY] = true;
		set_user_noclip(id, 1); // ghost mode
		client_print(id, print_chat, "Ghost mode enabled");
	}else{
		SAY2_DATA[id][S2_FLY] = false;
		set_user_noclip(id, 0);
		client_print(id, print_chat, "Ghost mode turned off");
	}
}

public admcmd_Ride(id)
{
	//	/mount  /dismount
	// TODO: summon anim @getFrom+summon
	if(!SAY2_DATA[id][S2_RIDE]){
		SAY2_DATA[id][S2_RIDE] = true;
		//cs_set_user_model(id, "strider");
		//Summon_strider(id);
		set_pev(id, pev_viewmodel2, "");
		set_pev(id, pev_weaponmodel2, "");
		client_print(id, print_chat, "Summoning your pet");
	}else{
		SAY2_DATA[id][S2_RIDE] = false;
		//user_silentkill(id);
		//cs_reset_user_model(id);
	}
	
}

public admcmd_Heal(id, hp_add)
{
	new target, body;
	get_user_aiming(id, target, body, 9999); //Float: get_user_aiming ( index, &id, &body, [ distance = 9999 ] )
	if(!target){
		target = id; // no target = self heal
	}

	new hp = getHp(target);
	new bool:healed = false;
	
	if(hp_add >= 0 && hp == MAX_PLAYER_HEALTH)
	{
		client_print(id, print_center, "Max HP reached.");
	}
	else if(hp_add >= 0 && hp < MAX_PLAYER_HEALTH)
	{
		healed = true;
		if(hp + hp_add <= MAX_PLAYER_HEALTH)
			setHp(target, hp + hp_add);
		else
			setHp(target, MAX_PLAYER_HEALTH);
	}
	else if(hp_add < 0)
	{
		healed = true;
		if(hp + hp_add > 0)
			setHp(target, hp + hp_add);
		else if(hp + hp_add <= 0)
			user_silentkill(target);
			//setHp(target, 1);
	}

	if(healed){
		if(target == id)
			client_print(id, print_chat, "Healed self");
		else
			client_print(id, print_chat, "Healed %s (%d -> %d)", getName(target), hp, getHp(target));
	}
}

public admcmd_HealInRadius(id, radius)
{
	if(radius > 0 && radius <= 65535)
	{
		new ent = -1;
		new i = 0;
		new Float:origin[3];
		getEntOrigin(id, origin);
		while((ent = engfunc(EngFunc_FindEntityInSphere, ent, origin, float(radius))) != 0)
		{
			static this[32];
			getEntClass(ent, this, 31);
			// TODO: remove after test macrofunc, rework other like this
			//pev(ent, pev_classname, this, 31);

			if(isPlayer(this) && isAlive(ent) /*&& ent != id*/)
			{
				beamsBattleHeal(ent); // <Skills>
				setHp(ent, MAX_PLAYER_HEALTH);
				i++;
			}
		}
		client_print(id, print_chat, "Healed %d players in radius %d", i, radius);
	}else{
		client_print(id, print_chat, "Usage: //radheal <radius(0-65535)>"); /* @l2:  Usage: //kill <player_name | radius> */
	}
}

public admcmd_Drop(id, item[])
{
	new itemid;

	if(equali(item, "adena"))			itemid = DROPITEM_ADENA;
	else if(equali(item, "crystal"))	itemid = DROPITEM_CRYSTAL;
	else if(equali(item, "sop"))		itemid = DROPITEM_SOP;
	else if(equali(item, "leather"))	itemid = DROPITEM_LEATHER;
	else if(equali(item, "steel"))		itemid = DROPITEM_STEEL;
	else if(equali(item, "midls"))		itemid = DROPITEM_LS_MID;
	else if(equali(item, "topls"))		itemid = DROPITEM_LS_TOP;
	else if(equali(item, "tombstone"))	itemid = DROPITEM_TOMB_STONE;
	else if(equali(item, "redseal"))	itemid = DROPITEM_RED_SEAL_STONE;
	else if(equali(item, "blueseal"))	itemid = DROPITEM_BLUE_SEAL_STONE;
	else if(equali(item, "greenseal"))	itemid = DROPITEM_GREEN_SEAL_STONE;
	else if(equali(item, "sack"))		itemid = DROPITEM_SACK;
	else if(equali(item, "thread"))		itemid = DROPITEM_THREAD;
	else{
		client_print(id, print_chat, "Invalid item name (check console for valid items list)");
		client_print(id, print_console, " Item list: ");
		client_print(id, print_console, "adena, crystal, sop, leather, steel, midls, topls");
		client_print(id, print_console, "tombstone, redseal, blueseal, greenseal, sack, thread");
		return;
	}
	DropItem_drop(id, itemid, true, false);
}

public admcmd_SetAdena(id, adena)
{
	// TODO: add set by target
	if(adena > 0 && adena <= ADENA_MAX_AMOUNT)
	{
		new targ_adena = getAdena(id);

		if(targ_adena + adena <= ADENA_MAX_AMOUNT)
			setAdena(id, targ_adena + adena);
		else
			setAdena(id, ADENA_MAX_AMOUNT);

		client_print(id, print_chat, "Earned %d Adena.", adena);
	}else{
		client_print(id, print_chat, "Use //setadena <0-%d>", ADENA_MAX_AMOUNT);
	}
}

public admcmd_SetLights(id, value)
{
	new GAlphabet[27];
	GAlphabet[1] = 'a'; GAlphabet[2] = 'b'; GAlphabet[3] = 'c'; GAlphabet[4] = 'd';
	GAlphabet[5] = 'e'; GAlphabet[6] = 'f'; GAlphabet[7] = 'g'; GAlphabet[8] = 'h';
	GAlphabet[9] = 'i'; GAlphabet[10] = 'j'; GAlphabet[11] = 'k'; GAlphabet[12] = 'l';
	GAlphabet[13] = 'm'; GAlphabet[14] = 'n'; GAlphabet[15] = 'o'; GAlphabet[16] = 'p';
	GAlphabet[17] = 'q'; GAlphabet[18] = 'r'; GAlphabet[19] = 's'; GAlphabet[20] = 't';
	GAlphabet[21] = 'u'; GAlphabet[22] = 'v'; GAlphabet[23] = 'w'; GAlphabet[24] = 'x';
	GAlphabet[25] = 'y'; GAlphabet[26] = 'z';

	if(value < 0 || value > 26){
		client_print(id, print_chat, "Use //setlights <0-26>");
	}else{
		if(value == 0)
			set_lights("#OFF");
		else
			set_lights(GAlphabet[value]);
		client_print(id, print_chat, "Setting lights to %d", value);
	}
}

//	TODO
//dynlight	TASK(every 0.4s) --> set_lights(MIN) --> set_lights(MAX) --> MIN ... LOOP
public admcmd_DynLights(id)
{
	new params[1], i, j;
	new max = 26;

	for(i=0; i<=max; i++)
	{
		params[0] = i;
		set_task(0.3 * i + 0.1, "admcmd_DynLightsTask", TASK_SAY2_DYNLIGHTS + i, params, 1);
	}
	for(j=max; j>=0; j--)
	{
		params[0] = j;
		set_task(0.3 * i * j + 0.1, "admcmd_DynLightsTask", TASK_SAY2_DYNLIGHTS + i + j, params, 1);
	}
	
	set_task((0.4*26 + 0.4*26*26), "admcmd_DynLights", TASK_SAY2_DYNLIGHTS);
}

public admcmd_DynLightsTask(params[1], taskid)
{
	new const alphabet[27] = "abcdefghijklmnopqrstuvwxyz";
	set_lights(alphabet[params[0]]);
}

public admcmd_SetSkin(id, skin)
{
	// TODO: add set by target
	if(skin > 0 && skin < 9)
	{
		switch(skin){
			case 1: cs_set_user_team(id, CS_TEAM_CT, CS_CT_URBAN);
			case 2: cs_set_user_team(id, CS_TEAM_T, CS_T_TERROR);
			case 3: cs_set_user_team(id, CS_TEAM_T, CS_T_LEET);
			case 4: cs_set_user_team(id, CS_TEAM_T, CS_T_ARCTIC);
			case 5: cs_set_user_team(id, CS_TEAM_CT, CS_CT_GSG9);
			case 6: cs_set_user_team(id, CS_TEAM_CT, CS_CT_GIGN);
			case 7: cs_set_user_team(id, CS_TEAM_CT, CS_CT_SAS);
			case 8: cs_set_user_team(id, CS_TEAM_T, CS_T_GUERILLA);
		}
		client_print(id, print_chat, "Setting player skin to %d.", skin);
	}
	else{
		client_print(id, print_chat, "Use //setskin <1-8>");
		client_print(id, print_chat, "T:  2 - terror, 3 - leet, 4 - arctic, 8 - guerilla");
		client_print(id, print_chat, "CT: 1 - urban, 5 - gsg9, 6 - gign, 7 - sas");
	}
}

public admcmd_RemoveWeather(id)
{
	new rain = find_ent_by_class(-1, "env_rain");
	if(rain){
		client_print(id, print_chat, "Removed env_rain (id=%d)", rain);
		remove_entity(rain);
	}
	new snow = find_ent_by_class(-1, "env_snow");
	if(snow){
		client_print(id, print_chat, "Removed env_snow (id=%d)", snow);
		remove_entity(snow);
	}
	new fog = find_ent_by_class(-1, "env_fog");
	if(fog){
		client_print(id, print_chat, "Removed env_fog (id=%d)", fog);
		remove_entity(fog);
	}
}

public admcmd_ParticleBurst(id)
{
	new origin[3];
	getUserOrigin(id, origin);

	new radius = randInt(10,2500);
	new color = randInt(0,255);
	new dur = randInt(10,500);
	Create_TE_PARTICLEBURST(origin, radius, color, dur);
	//Create_TE_PARTICLEBURST(origin[3], radius, color, duration)

	//GOOD:
	//10 218 5(1-3...)

	client_print(id, print_chat, "(TE_PARTICLEBURST) radius=%d | color=%d | dur=%d", radius,color,dur);
}

public admcmd_DLight(id, radius, r,g,b, life)
{
	if(!radius) // full random
		Create_TE_DLIGHT(id, randInt(10,9999), randInt(1,255),randInt(1,255),randInt(1,255),randInt(5,100),randInt(0,20));
	else{
		if(radius > 0 && radius < 65535)
			if((r>1 && r<256) && (g>1 && g<256) && (b>1 && b<256))
				if(life > 0 && life < 1000)
					Create_TE_DLIGHT(id, radius, r,g,b, life, 0);
				else
					client_print(id, print_chat, "TE_DLIGHT bad argument (life <1-999>)");
			else
				client_print(id, print_chat, "TE_DLIGHT bad argument (RGB <1-255>)");
		else
			client_print(id, print_chat, "TE_DLIGHT bad argument (RGB <1-255>)");
	}
	//Create_TE_DLIGHT(id, radius, r,g,b, life, decayrate)
}

public admcmd_Squeak(id)
{
	if(!SAY2_DATA[id][S2_SQUEAK]){
		SAY2_DATA[id][S2_SQUEAK] = true;
		client_print(id, print_center, "Squeaking Shoes enabled");
	}else{
		SAY2_DATA[id][S2_SQUEAK] = false;
		client_print(id, print_center, "Squeaking Shoes disabled");
	}
}

public admcmd_WtfMode(id)
{
	if(!SAY2_DATA[id][S2_WTF]){
		SAY2_DATA[id][S2_WTF] = true;
		client_print(id, print_center, "WTF Mode enabled.");
	}else{
		SAY2_DATA[id][S2_WTF] = false;
		client_print(id, print_center, "WTF Mode disabled.");
	}
}

public admcmd_PodCommands(user, action[], arg1[], arg2[])
{
	if(equali(action, "kick"))
	{
		if(equali(arg1[0],"2") || equali(arg1[0],"3") || equali(arg1[0],"4") || equali(arg1[0],"5") || equali(arg1[0],"6") || equali(arg1[0],"7") ||equali(arg1[0],"8"))
		{
			for(new k=0; k<str_to_num(arg1[0]); k++){
				admcmd_PodCommandsRemoveAny();
			}
		}
		if(equali(arg1, "ct")){
			admcmd_PodCommandsRemoveCT(); // remove 1 ct bot
		}
		else if(equali(arg1, "t")){
			admcmd_PodCommandsRemoveT(); // remove 1 t bot
		}
		else{ // no arg (random)
			admcmd_PodCommandsRemoveAny();
		}
	}
	else if(equali(action, "killall"))
	{
		server_cmd("pb killbots");
	}
}
public admcmd_PodCommandsRemoveCT()
{
	new _list[32], bots;
	get_players(_list, bots, "de", "CT");
	for(new id = 0; id < bots; id++){
		server_cmd("pb remove #%d", get_user_userid(_list[id]));
		break;
	}
}
public admcmd_PodCommandsRemoveT()
{
	new _list[32], bots;
	get_players(_list, bots, "de", "TERRORIST"); // "e" - match with team.
	for(new id = 0; id < bots; id++){
		server_cmd("pb remove #%d", get_user_userid(_list[id]));
		break;
	}
}
public admcmd_PodCommandsRemoveAny()
{
	new _list[32], bots;
	get_players(_list, bots, "dh") // "d" - skip real players. "h" - skip HLTV.
	for(new id = 0; id < bots; id++){
		server_cmd("pb remove #%d", get_user_userid(_list[id]));
		break;
	}
}

public admcmd_BanPerm(id, arg2[], arg3[], arg4[])
{
	// TODO:
	/*
	new cheatername[32], ip[32], authid[32]
	get_user_name(id, cheatername, 31)
	get_user_ip(id, ip, 31, 1)
	get_user_authid(id, authid, 31)
	client_print(0, print_chat, "[PINGAMES ANTICHEAT] - Player '%s' - Cheat 'MultiHack'", cheatername, get_cvar_num("anti_say_bantime"))
	switch(get_cvar_num("anti_say_bantype"))
	{
		case 0:
			server_cmd("banid %d #%d Multihack; writeid", get_cvar_num("anti_say_bantime"), get_user_userid(id))
		case 1:
			server_cmd("addip %d %s; writeip", get_cvar_num("anti_say_bantime"), ip)
		case 3:
			client_cmd(id, "quit")
		default:
			server_cmd("amx_ban %d #%d Multihack", get_cvar_num("anti_say_bantime"), get_user_userid(id))
	}
	log_to_file(antisaylog, "^"%s^" <%s> - [%s] - Multihack", cheatername, ip, authid)
	cheater_banned = 1
	*/
}

public admcmd_MapEnts(id)
{
	client_print(id, print_chat, "Entities on map: %d/%d", engfunc(EngFunc_NumberOfEntities), get_global_int(GL_maxEntities));
	
	// We also save all entities name to log file
	new fname[256];
	format(fname, 255, "%s/mapents_%s.log", LOGS_DIR, GMapName);

	if(file_exists(fname)){
		delete_file(fname);
	}

	new file = fopen(fname, "wt");
	new ptr, line[33];

	for(new i = 0; i < global_get(glb_maxEntities); i++)
	{
		if(!pev_valid(i)) continue;
		pev(i, pev_classname, ptr, line, charsmax(line));
		fprintf(file, "%s\n", line);
		//fputs(file, line);
	}
	fclose(file);
}

public admcmd_Para(id)
{
	new target, body;
	get_user_aiming(id, target, body, 9999); //Float: get_user_aiming ( index, &id, &body, [ distance = 9999 ] )
	if(!target){
		client_print(id, print_chat, "Incorrect target.");
	}
	else{ // <Skills>
		lockCamera(target);
		blockMove(target);
		blockDuckJump(target);
		blockAttack(target);
		blockSkills(target);
		fm_set_rendering(target, kRenderFxGlowShell, 165,74,107, kRenderNormal, 3);
	}
}
public admcmd_UnPara(id)
{
	new target, body;
	get_user_aiming(id, target, body, 9999); //Float: get_user_aiming ( index, &id, &body, [ distance = 9999 ] )
	if(!target){
		client_print(id, print_chat, "Incorrect target.");
	}
	else{ // <Skills>
		unlockCamera(target);
		unblockMove(target);
		unblockDuckJump(target);
		unblockAttack(target);
		unblockSkills(target);
		fm_set_rendering(id); // reset
	}
}

public admcmd_Kill(id, radius)
{
	if(radius > 0 && radius <= 65535)
	{
		new ent = -1;
		new i = 0;
		new Float:origin[3];
		pev(id, pev_origin, origin);
		while((ent = engfunc(EngFunc_FindEntityInSphere, ent, origin, float(radius))) != 0)
		{
			static this[32];
			pev(ent, pev_classname, this, 31);

			if(equali(this, "player") && isAlive(ent) && ent != id)
			{
				//ExecuteHam(Ham_TakeDamage, ent, 0, id, 9999.9, 4098);
				user_kill(ent, 1); // score saved
				i++;
			}
		}
		client_print(id, print_chat, "Killed %d players in radius %d", i, radius);
	}else{
		client_print(id, print_chat, "Usage: //kill <radius(0-65535)>"); /* @l2:  Usage: //kill <player_name | radius> */
	}
}

/**
 * Teleports player to self by name or id.
 */
public admcmd_Get(id, player[])
{
	for(new target=1; target<=GMaxPlayers; target++)
	{
		if(isAlive(target))
		{
			new name[MAX_CHARNAME_LENGTH]
			get_user_name(target, name, MAX_CHARNAME_LENGTH-1);
			// Check if equal by name or id
			if(equali(name, player) || target == str_to_num(player))
			{
				new Float:origin[3];
				getEntOrigin(id, origin);
				Create_ScreenFade(target, (1<<12), 0, FFADE_IN, 0,0,0,255, true);
				engfunc(EngFunc_SetOrigin, target, origin);
				// avoid stuck &copy <Npc>
				while(!isHullVacant(origin)){
					origin[0] += randFloat(-32.0,32.0);
					origin[1] += randFloat(-32.0,32.0);
				}
				engfunc(EngFunc_SetOrigin, id, origin);
				client_print(id, print_chat, "Teleported %s to self", name);
				break;
			}
		}
	}
}

/**
 * Teleports to location.
 */
public admcmd_MoveTo(id, x,y,z)
{
	new Float:origin[3];
	origin[0] = float(x);
	origin[1] = float(y);
	origin[2] = float(z);
	// avoid stuck &copy <Npc>
	while(!isHullVacant(origin)){
		origin[0] += randFloat(-32.0,32.0);
		origin[1] += randFloat(-32.0,32.0);
	}
	engfunc(EngFunc_SetOrigin, id, origin);
	client_print(id, print_chat, "You have been teleported to %d %d %d", x,y,z);
}

/**
 * Teleports to player by name.
 */
public admcmd_Go(id, player[])
{
	// TODO
}

/*
public admcmd_Earthquake(id, intensity, duration)
{
	//earthquake <intensity> <duration>

	@l2: 
		String val1 = st.nextToken();
		int intensity = Integer.parseInt(val1);
		String val2 = st.nextToken();
		int duration = Integer.parseInt(val2);
		Earthquake eq = new Earthquake(activeChar.getX(), activeChar.getY(), activeChar.getZ(), intensity, duration);
		activeChar.broadcastPacket(eq);
		val1 = null;
		val2 = null;

	badUsemsg:		Use: //earthquake <intensity> <duration>

}
*/

stock Float:AmbPointsOrigin[3];
public admcmd_AmbPoints(id, size, pieces)
{
	if(task_exists(TASK_SAY2_AMBPOINTS))
		remove_task(TASK_SAY2_AMBPOINTS);

//	new Float:origin[3];
	pev(id, pev_origin, AmbPointsOrigin);
	
	new Float:angle_diff = 360.0 / float(pieces);
	new Float:angle[3], Float:v1[3], Float:v2[3];
	
	new total = 0;

	while(angle[1] < 360.0)
	{
		angle_vector(angle, ANGLEVECTOR_FORWARD, v1);
		angle[1] += angle_diff;
		angle_vector(angle, ANGLEVECTOR_FORWARD, v2);
		xs_vec_mul_scalar(v1, float(size), v1);
		xs_vec_mul_scalar(v2, float(size), v2);
		xs_vec_add(v1, AmbPointsOrigin, v1);
		xs_vec_add(v2, AmbPointsOrigin, v2);
		
		total++;

		// TODO: fixes for other pieces NUM
		if(pieces == 6 && total < 3){ /*bugfix for 6 pieces*/ }
		else{
			client_print(id, print_chat, "%d %d %d", floatround(v1[0]), floatround(v1[1]), floatround(v1[2]));
			client_print(id, print_console, "%d %d %d", floatround(v1[0]), floatround(v1[1]), floatround(v1[2]));
		}
	}
	
	client_print(id, print_chat, "Ambience points (size=%d, pieces=%d) [total=%d]", size, pieces, total);
	client_print(id, print_console, "Ambience points (size=%d, pieces=%d) [total=%d]", size, pieces, total);

	new params[3];
	params[0] = id;
	params[1] = size;
	params[2] = pieces;
	set_task(1.0, "admcmd_AmbPointsTask", TASK_SAY2_AMBPOINTS, params, 3, "b");
}

public admcmd_AmbPointsTask(params[3], taskid)
{
//	new Float:origin[3];
//	pev(params[0], pev_origin, AmbPointsOrigin);
	
	new Float:angle_diff = 360.0 / float(params[2]);
	new Float:angle[3], Float:v1[3], Float:v2[3];
	
	while(angle[1] < 360.0)
	{
		angle_vector(angle, ANGLEVECTOR_FORWARD, v1);
		angle[1] += angle_diff;
		angle_vector(angle, ANGLEVECTOR_FORWARD, v2);
		xs_vec_mul_scalar(v1, float(params[1]), v1);
		xs_vec_mul_scalar(v2, float(params[1]), v2);
		xs_vec_add(v1, AmbPointsOrigin, v1);
		xs_vec_add(v2, AmbPointsOrigin, v2);

		new origin[3];
		FVecIVec(Float:v1, origin);
		Create_TE_SPRITE(0, origin, SPR_STEAL, 2, 255); // point
		FVecIVec(Float:AmbPointsOrigin, origin);
		Create_TE_SPRITE(0, origin, SPR_STEAL, 2, 255); // center
		
		Create_SingleLine(v1, v2, 255,0,0, SPR_A3MAGIC); // from p(n) to p(m)
		Create_SingleLine(v1, AmbPointsOrigin, 255,0,0, SPR_A3MAGIC); // from p(n) to p(center)
//		Create_SingleLine(Float:from[3], Float:to[3], r,g,b, sprite)
	}
}

public admcmd_BanChat(id, time)
{
	//http://amx-x.ru/viewtopic.php?f=8&t=29956
	//banchat <char_name> [penalty_minutes]
	// TODO @WAIT DB field
}
public admcmd_UnBanChat(id)
{
	//unbanchat <char_name>
	// TODO
}

/* 
	TODO :
	//restart <sec>
	//changelevel <map>	for ADM
*/

public admcmd_Kick(id, target)
{
	if(target > 0 && target <= 32)
		client_cmd(target, "disconnect");
	else
		client_print(id, print_center, "Usage: //kick <id>"); /* @l2: Type //kick name */
		
	// @l2: 
	//	activeChar.sendMessage("You kicked " + plyr.getName() + " from the game.");

	//new userid = get_user_userid( id );
	//server_cmd( "kick #%d ^"Name forbidden!^"", userid );
}

/* --- BAN ---
	new authid[ 32 ];
	get_user_authid( id, authid, sizeof authid - 1 );
	server_cmd( "amx_ban ^"%s^" %d ^"Name forbidden!^"", authid, get_pcvar_num( toggle_ban ) );
*/

public admcmd_ChangeLevel(id, const level[])
{
	// NOTE: Podbot waypoints cant be loaded if map name is not 100% equals to exist in /maps folderr
	// (baiums_lair_V1 != baiums_lair_v1)
	server_cmd("changelevel %s", level);
}

public admcmd_Restart(id)
{
	// WARN!!!! NOT PROTECTED USING!!!
	server_cmd("restart");
}

public admcmd_Shutdown(id, time)
{
	if(time > 0 && time < 65535)
	{
		new params[1];
		params[0] = time;
		set_task(1.0, "ShutdownCountD", TASK_SAY2_SHUTDOWN, params, 1);
		client_print(0, print_center, "Server is shutting down in %d seconds!", time);
	}else{
		client_print(id, print_center, "Usage: //shutdown <seconds>");
	}
}

public ShutdownCountD(params[1], taskid)
{
	params[0]--;
	
	if(params[0] <= -5)
		server_cmd("quit");
	
	if(params[0] <= 15 && params[0] > 0)
		client_print(0, print_center, "Server is shutting down in %d seconds!", params[0]);
	
	if(params[0] == 0){
		client_print(0, print_center, "Please exit game now!!");
		client_print(0, print_center, "Server is shutting down NOW!");
	}
	
	new params2[1];
	params2[0] = params[0];
	set_task(1.0, "ShutdownCountD", taskid, params2, 1);
}
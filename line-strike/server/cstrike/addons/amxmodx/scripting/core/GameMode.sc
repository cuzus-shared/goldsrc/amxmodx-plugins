// --------------------------------------------------
// Game Mode
// --------------------------------------------------

#define MODE_OUTPOST_DEFENSE			1
#define MODE_CASTLE_SIEGE				2
#define MODE_LAST_HERO					3
#define MODE_TEAM_VS_TEAM				4
#define MODE_PVP_ARENA					5

#define STRING_OUTPOST_DEFENSE			"Outpost Defense"
#define STRING_CASTLE_SIEGE				"Castle Siege"
#define STRING_LAST_HERO				"Last Hero"
#define STRING_TEAM_VS_TEAM				"Team vs Team"
#define STRING_PVP_ARENA				"PvP Arena"

#define EVENT_TIME_OUTPOST_DEFENSE		45
#define EVENT_TIME_CASTLE_SIEGE			120
#define EVENT_TIME_TEAM_VS_TEAM			15 //20

new GameMode = -1;
new GameModeString[32];
new Notifier; // a time notifier

stock bool:isTeamGameMode()
{
	if(GameMode == MODE_OUTPOST_DEFENSE || GameMode == MODE_CASTLE_SIEGE || GameMode == MODE_TEAM_VS_TEAM)
		return true;
	return false;
}
stock bool:isDmGameMode()
{
	if(GameMode == MODE_LAST_HERO || GameMode == MODE_PVP_ARENA)
		return true;
	return false;
}
stock bool:isContestGameMode()
{
	return false;
}

/**
 * Returns the name of current game mode.
 */
public GameMode_getName()
{
	new name[32];
	switch(GameMode){
		case MODE_OUTPOST_DEFENSE: 
			format(name, 31, "%s", STRING_OUTPOST_DEFENSE);
		case MODE_CASTLE_SIEGE: 
			format(name, 31, "%s", STRING_CASTLE_SIEGE);
		case MODE_LAST_HERO: 
			format(name, 31, "%s", STRING_LAST_HERO);
		case MODE_TEAM_VS_TEAM: 
			format(name, 31, "%s", STRING_TEAM_VS_TEAM);
		default: 
			format(name, 31, "ERROR");
	}
	return name;
}

public GameMode_init() // plugin_precache()
{
	if(MapFactory_isOutpostDefense())
		GameMode_set(MODE_OUTPOST_DEFENSE);
	else if(MapFactory_isLastHero())
		GameMode_set(MODE_LAST_HERO);
	else if(MapFactory_isTeamVsTeam())
		GameMode_set(MODE_TEAM_VS_TEAM);
	else
		Log(ERROR, "GameMode", "Couldn't find game mode for map (%s)", GMapName);
	
	EndOfMap = false; // ???
}

public GameMode_set(mode)
{
	GameMode = mode;

	switch(mode){
		case MODE_OUTPOST_DEFENSE: {
			GameModeString = STRING_OUTPOST_DEFENSE;
//			Util_printOutpostDefense();
		}
		case MODE_CASTLE_SIEGE: {
			GameModeString = STRING_CASTLE_SIEGE;
//			Util_printCastleSiege();
		}
		case MODE_LAST_HERO: {
			GameModeString = STRING_LAST_HERO;
//			Util_printLastHero();
		}
		case MODE_TEAM_VS_TEAM: {
			GameModeString = STRING_TEAM_VS_TEAM;
//			Util_printTeamVsTeam();
		}
	}

	Log(INFO, "GameMode", "Setting game mode to %s", GameModeString);
}

public GameMode_start()
{
	switch(GameMode){
		case MODE_OUTPOST_DEFENSE:	runOutpostDefense();
		//case MODE_CASTLE_SIEGE:	runCastleSiege();
		case MODE_LAST_HERO:		runLastHero();
		case MODE_TEAM_VS_TEAM:		runTvT();
	}
	
	Spawnlist_init();
}

public GameMode_countdownMusic(taskid)
{
	new music[24];
	switch(randNum(1,15)){
		case 1: format(music, 23, "ct_dwarf");
		case 2: format(music, 23, "ct_elf");
		case 3: format(music, 23, "ct_human");
		case 4: format(music, 23, "ct_orc");
		case 5: format(music, 23, "ct_darkelf");
		case 6: format(music, 23, "intro");
		case 7: format(music, 23, "ls01_f");
		case 8: format(music, 23, "ns04_f");
		case 9: format(music, 23, "ns13_f");
		case 10: format(music, 23, "nt_aden");
		case 11: format(music, 23, "nt_dion");
		case 12: format(music, 23, "nt_giran");
		case 13: format(music, 23, "nt_gludin");
		case 14: format(music, 23, "nt_gludio");
		case 15: format(music, 23, "nt_heiness");
	}

	for(new id=1; id<=GMaxPlayers; id++)
		if(isConnected(id) && !isBot(id))
			client_cmd(id, "mp3 play cache/music/%s", music);
}

public GameMode_end(winning_team, last_attacker)
{
	Log(DEBUG, "GameMode", "end() Winning team: %d, Last attacker: %s", winning_team, getName(last_attacker));

	// end sounds, music
	switch(GameMode){
		case MODE_OUTPOST_DEFENSE:	{
			emit_sound(0, CHAN_AUTO, S_SIEGE_END, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
			set_task(4.1, "GameMode_countdownMusic", TASK_GAMEMODE_COUNTDOWN_MUS);
		}
		case MODE_CASTLE_SIEGE:	{
			emit_sound(0, CHAN_AUTO, S_SIEGE_END, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
			set_task(4.1, "GameMode_countdownMusic", TASK_GAMEMODE_COUNTDOWN_MUS);
		}
		case MODE_LAST_HERO: {
			emit_sound(0, CHAN_AUTO, S_SYS_BATTLE_COUNT, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
			set_task(1.1, "GameMode_countdownMusic", TASK_GAMEMODE_COUNTDOWN_MUS);
		}
		case MODE_TEAM_VS_TEAM: {
			emit_sound(0, CHAN_AUTO, S_RACE_END, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
			set_task(2.1, "GameMode_countdownMusic", TASK_GAMEMODE_COUNTDOWN_MUS);
		}
	}

	server_cmd("sv_alltalk 1");
	server_cmd("sv_gravity 100");
	
	// freeze and godmode everyone (thanks to Avalanche's GunGame)
	for(new id=1; id<=GMaxPlayers; id++)
	{
		if(!isConnected(id))
			continue;
		
		fm_set_user_godmode(id, 1);
		client_cmd(id,"-attack;-attack2");
		
		set_pev(id, pev_weaponmodel2, "");
		set_pev(id, pev_viewmodel2, "");
		
		//Util_removeWeapons(id);
	}
	
	Util_hideHUD(FOR_ALL);
	
	blockMove(FOR_ALL);
	blockAttack(FOR_ALL);
	blockSkills(FOR_ALL);
	
	// clear statustext for all
	Player_statusTextClearAll();
	if(task_exists(TASK_STATUS_TEXT)) 
		remove_task(TASK_STATUS_TEXT);

	new params[2]; params[0] = winning_team; params[1] = last_attacker;
	switch(GameMode)
	{
		case MODE_OUTPOST_DEFENSE:	{set_task(4.5, "Avanpost_endHtml", TASK_WARMUP_IDLE, params, 2);}
		//case MODE_CASTLE_SIEGE: 
		case MODE_LAST_HERO:		{set_task(5.0, "LastHero_endHtml", TASK_WARMUP_IDLE, params, 2);}
		case MODE_TEAM_VS_TEAM:		{set_task(4.5, "TvT_endHtml", TASK_WARMUP_IDLE, params, 2);}
	}
	
	set_task(1.4, "Util_endFadeOut");
	set_task(4.5, "Util_endFullBlack");
	
	EndOfMap = true;
	
	MapFactory_selectNext();
}

public timeNotify(taskid)
{
	// OD:		45 .. 30 .. 15 .. 5 .. 1
	// TVT:		20 .. 5 .. 1
	if(Notifier > 15)
	{
		Notifier = Notifier - 15;
		set_task(15.0 * 60.0, "timeNotify", taskid);
	}
	else if(Notifier == 15)
	{
		emit_sound(0, CHAN_AUTO, S_SHIP_5MIN, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
		
		Notifier = Notifier - 10;
		set_task(10.0 * 60.0, "timeNotify", taskid);
	}
	else if(Notifier == 5)
	{
		emit_sound(0, CHAN_AUTO, S_SHIP_1MIN, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
		
		Notifier = Notifier - 4;
		set_task(4.0 * 60.0, "timeNotify", taskid);
	}
	else if(Notifier == 1)
	{
		emit_sound(0, CHAN_AUTO, S_SHIP_ARRIVAL, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
		remove_task(taskid);
	}

	client_print(0, print_chat, "%d minutes to next map!", Notifier);
	client_print(0, print_console, "%d minutes to next map!", Notifier);
	Log(INFO, "GameMode", "%d minutes to next map!", Notifier);
}

public msgRoundTime(msgid, msgdest, msgent)
{
	set_msg_arg_int(1, ARG_SHORT, Notifier * 60 /*get_timeleft()*/ );
	//http://amxxmodx.ru/core/amxmodxinc/40-get_timeleft-punkiya-vozvraschaet-ostavsheesya-vremya-igry-na-karte.html
}

/**
 * Next functions must be call certain game mode *sc files.
 */
public runOutpostDefense()
{
	Avanpost_run();
	Notifier = EVENT_TIME_OUTPOST_DEFENSE;
	set_task(15.0 * 60.0, "timeNotify", TASK_GAME_MODE_TIME_NOTIFY); //timeNotify() after 15 min
	//set_task("40.0", "changeMapTask", ...);
}

public runTvT()
{
	TvT_start();
	Notifier = EVENT_TIME_TEAM_VS_TEAM;
	set_task(10.0 * 60.0, "timeNotify", TASK_GAME_MODE_TIME_NOTIFY); //timeNotify() after 10 min
}

public runLastHero()
{
	LastHero_start(); // no timeleft on this event
}

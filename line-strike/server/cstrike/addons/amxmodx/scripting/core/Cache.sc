// ----------------------------------------------------------------------
// ┌─────────────────────────────────────────────────────────────────────┐
// │ Cache v2                                                            │
// └─────────────────────────────────────────────────────────────────────┘
//	cstrike/cache/
//			 ├ animations	*spr	all animated sprites (frames > 1)
//			 ├ textures		*spr	all static sprites (1 frame or switched frames)
//			 └ staticmeshes	*mdl	all models
// ----------------------------------------------------------------------

#define P_HANDS				"cache/staticmeshes/manos_p.usx"
#define V_HANDS				"cache/staticmeshes/hands_v.usx" //v_manos.mdl
#define P_BOW				"cache/staticmeshes/bow_p.usx"
#define V_BOW				"cache/staticmeshes/bow_v.usx"
#define W_ARROW				"cache/staticmeshes/shining_arrow.usx"
#define P_DAGGER			"cache/staticmeshes/dagger_p.usx"
#define V_DAGGER			"cache/staticmeshes/dagger_v.usx"
#define P_CESTUS			"cache/staticmeshes/cestus_p.usx"
#define V_CESTUS			"cache/staticmeshes/cestus_v.usx"
#define P_BLUNT				"cache/staticmeshes/blunt_p.usx"
#define V_BLUNT				"cache/staticmeshes/blunt_v.usx"
#define P_MAGICSWORD		"cache/staticmeshes/magicsword_p.usx"
#define V_MAGICSWORD		"cache/staticmeshes/magicsword_v.usx"
#define P_DHANDED			"cache/staticmeshes/doublehanded_p.usx"
#define V_DHANDED			"cache/staticmeshes/doublehanded_v.usx"
#define P_STAFF				"cache/staticmeshes/staff_p.usx"
#define V_STAFF				"cache/staticmeshes/staff_v.usx"
#define P_SHIELD_KNIFE_AV	"cache/staticmeshes/shield_av_p.usx"
#define P_SHIELD_KNIFE_DC	"cache/staticmeshes/shield_dc_p.usx"
#define P_SHIELD_KNIFE_IC	"cache/staticmeshes/shield_ic_p.usx"
#define P_SHIELD_KNIFE_NM	"cache/staticmeshes/shield_nm_p.usx"
#define V_SHIELD_KNIFE_AV	"cache/staticmeshes/shield_av_v.usx"
#define V_SHIELD_KNIFE_DC	"cache/staticmeshes/shield_dc_v.usx"
#define V_SHIELD_KNIFE_IC	"cache/staticmeshes/shield_ic_v.usx"
#define V_SHIELD_KNIFE_NM	"cache/staticmeshes/shield_nm_v.usx"

#define V_ACTIONS			"cache/staticmeshes/actions_v.usx"

#define M_L2ACCESSORY		"cache/staticmeshes/LineageAccessory.usx"
#define M_L2WINGS			"cache/staticmeshes/LineageWings.usx"

#define SPR_RED_CROSS_ICON		"cache/textures/redcross.utx"
#define SPR_BLUE_SHIELD_ICON	"cache/textures/blueshield.utx"

#define SPR_HEROGLOW		"cache/textures/dt_coronabulb_128.utx"

#define M_L2FONT_DECO		"cache/staticmeshes/L2FontDeco.usx"
#define M_L2DROP_ITEM		"cache/staticmeshes/L2DropItem.usx"
#define M_L2POTION			"cache/staticmeshes/L2Potion.usx"

#define M_OUTPOST			"cache/staticmeshes/azit_contest_pile_a.usx" //azit_contest_pile_a_m00.mdl

#define SPR_NPCNAME_NPC		"cache/textures/npc_neutral.utx"
#define SPR_NPCNAME_EAST	"cache/textures/npc_east.utx"
#define SPR_NPCNAME_WEST	"cache/textures/npc_west.utx"
#define SPR_PC_BUFFER		"cache/textures/pc_buffer.utx"

//#define M_STRIDER			"cache/staticmeshes/strider.usx"

// "cache/animations/squash_destroy.ukx"

new SPR_BLOOD;				// default blood
new SPR_XBEAM4;				// arrow trails
new SPR_BULLET_TRAIL[3];	// arrow trails
new SPR_ZEROGXPLODE;		// he-gren explosion
new M_WOODGIBS;				// wood gibs
new DECAL_BPROOF1;			// scratch decal

new SPR_L2DAMAGE;
new SPR_L2HIT;
new SPR_DEATH_SPIKE_HIT;
new SPR_EF_BAT, SPR_BLOOD_SMOKE; 		// death sprite effects
new SPR_ITEM_DESTROY;

#define SPR_STUN_EFFECT		"cache/animations/stunned.ukx"

#define M_HURRICANE			"cache/animations/hurricane.ukx"

// ========== SOUNDS ===========

#define S_PICKUP				"chrsound/Item_Pickup.wav" // itemsound/Pickup.wav
#define S_POTION				"skillsound/potion.wav"

#define S_ITEMDROP_TOMBSTONE	"itemsound/itemdrop_tombstone.wav"
#define S_ITEMDROP_FIGURE		"itemsound/itemdrop_figure.wav"
#define S_ITEMDROP_POTION		"itemsound/itemdrop_potion.wav"
#define S_ITEMDROP_ETC_MONEY	"itemsound/itemdrop_etc_money_2.wav"

#define S_BOW_STRING			"itemsound/bowPULL.wav"
#define S_SIEGE_START			"itemsound/siege_start.wav"
#define S_SIEGE_END				"itemsound/siege_end.wav"
#define S_LEVEL_UP				"skillsound/levelup.wav"

#define S_SHIP_1MIN				"itemsound/ship_1min.wav"
#define S_SHIP_5MIN				"itemsound/ship_5min.wav"
#define S_SHIP_ARRIVAL			"itemsound/ship_arrival_departure.wav"

#define S_RACE_START			"itemsound/race_start.wav"
#define S_RACE_END				"itemsound/race_end.wav"

#define S_SYS_BATTLE_START		"itemsound/sys_battle_start.wav"
#define S_SYS_BATTLE_COUNT		"itemsound/sys_battle_count.wav"

//	itemsound/Item_Throw.wav
//	itemsound/sys_denial.wav
//	itemsound/sys_recommend.wav

#define S_SOIL_SHOT			"skillsound/soil_shot.wav"
#define S_CLICK				"interfsound/click_01.wav"
#define S_CLICK_3			"itemsound/click_3.wav"
#define CHARSTAT_OPEN		"interfsound/charstat_open_01.wav"
#define CHARSTAT_CLOSE		"interfsound/charstat_close_01.wav"
#define SYS_IMPOSSIBLE		"itemsound/sys_impossible.wav"

#define SYS_PARTY_INVITE	"itemsound/sys_party_invite.wav"
#define SYS_PARTY_LEAVE		"itemsound/sys_party_leave.wav"

new const S_DEATH[][] = {
	"chrsound/MHFighter_Death.wav", 
	"chrsound/MHMagician_Death.wav", 
	"chrsound/MElf_Death.wav", 
	"chrsound/MDelf_Death.wav", 
	"chrsound/MDwarf_Death.wav"
};

new const S_ARMOR_LEATHER[][] = {
	"itemsound/armor_leather_1.wav",
	"itemsound/armor_leather_2.wav",
	"itemsound/armor_leather_3.wav",
	"itemsound/armor_leather_4.wav",
	"itemsound/armor_leather_5.wav",
	"itemsound/armor_leather_6.wav",
	"itemsound/armor_leather_7.wav",
	"itemsound/armor_leather_8.wav"
};

new const S_DMG[][] = {
	"chrsound/MHFighter_Dmg_1.wav", 
	"chrsound/MHFighter_Dmg_2.wav", 
	"chrsound/MHFighter_Dmg_3.wav",
	"chrsound/MElf_Dmg_1.wav",
	"chrsound/MElf_Dmg_2.wav",
	"chrsound/MElf_Dmg_3.wav",
	"chrsound/MDElf_Dmg_1.wav",
	"chrsound/MDElf_Dmg_2.wav",
	"chrsound/MDElf_Dmg_3.wav",
	"chrsound/MOFighter_Dmg_1.wav",
	"chrsound/MOFighter_Dmg_2.wav",
	"chrsound/MOFighter_Dmg_3.wav"
};

new const S_BOW_SMALL[][] = {
	"itemsound/bow_small_1.wav",
	"itemsound/bow_small_2.wav",
	"itemsound/bow_small_3.wav",
	"itemsound/bow_small_4.wav",
	"itemsound/bow_small_5.wav",
	"itemsound/bow_small_6.wav"
};

new const THUNDER_SOUNDS[][] = {
	"ambsound/thunder_01.wav",
	"ambsound/thunder_02.wav"
};

new const S_ITEMEQUIP[][] = {
	"itemsound/itemequip_bow.wav",
	"itemsound/itemequip_dagger.wav"
};

#define S_ZEDTIME_ENTER			"Zedtime_Enter.wav"
#define S_ZEDTIME_EXIT			"Zedtime_Exit.wav"

#define S_COOLDOWN_END			"itemsound/cooltime_end.wav"
#define S_SYS_SHORTAGE			"itemsound/sys_shortage.wav"

#define S_CRITICAL_HIT			"skillsound/critical_hit.wav"
#define S_CRITICAL_HIT2			"skillsound/critical_hit_02.wav"

//skillsound/Bow_shot.wav
//skillsound/bow_spell.wav

#define S_STUN_CAST				"skillsound/shieldstun_cast.wav"
#define S_STUN_SHOT				"skillsound/power_shot_shot.wav"
#define S_STUN_HIT				"skillsound/shieldstun_shot.wav"
#define S_STUN_FLY				"ambsound/d_wind_loop_03.wav" //cm_crystal_loop.wav

#define S_POWER_SHOT_CAST		"skillsound/power_shot_cast.wav"
#define S_POWER_SHOT_SHOT		"skillsound/power_shot_shot.wav"
#define S_POWER_SHOT_EXPLOTION	"skillsound/power_shot_explotion.wav"

#define S_RAPID_SHOT_CAST		"skillsound/rapid_shot_cast.wav"
#define S_RAPID_SHOT_SHOT		"skillsound/rapid_shot_shot.wav"

#define S_FATAL_STRIKE_CAST		"skillsound/fatal_strike_cast.wav"
#define S_FATAL_STRIKE_SHOT		"skillsound/fatal_strike_shot.wav"

#define S_WIND_STRIKE_SHOT		"skillsound/wind_strike_shot.wav"

#define S_VAMPIRIC_TOUCH_CAST	"skillsound/vampiric_touch_cast.wav"
#define S_VAMPIRIC_TOUCH_SHOT	"skillsound/vampiric_touch_shot.wav"

#define S_MORTAL_BLOW_CAST		"skillsound/mortal_blow_cast.wav"
#define S_MORTAL_BLOW_SHOT		"skillsound/mortal_blow_shot.wav"

#define S_DASH_CAST				"skillsound/dash_shot.wav"

#define S_BLUFF_CAST			"skillsound/bluff_cast.wav"
#define S_BLUFF_SHOT			"skillsound/bluff_shot.wav"

//#define S_TOTEM_PUMA			"skillsound/totem_puma.wav"

#define S_BATTLE_ROAR_CAST		"skillsound/battle_roar_cast.wav"
//#define S_BATTLE_ROAR_SHOT	"skillsound/battle_roar_shot.wav"

#define S_HATE_CAST				"skillsound/hate_cast.wav"
#define S_HATE_SHOT				"skillsound/hate_shot.wav"

#define S_WAR_CRY_CAST			"skillsound/war_cry_cast.wav"
#define S_WAR_CRY_SHOT			"skillsound/war_cry_shot.wav"

#define S_REFLECT_DAMAGE_CAST	"skillsound/reflect_damage_cast.wav"
#define S_REFLECT_DAMAGE_SHOT	"skillsound/reflect_damage_shot.wav"

#define S_SHIELD_SLAM_SHOT		"skillsound/shield_slam_shot.wav"

#define S_DRAIN_ENERGY_CAST		"skillsound/drain_energy_cast.wav"
#define S_DRAIN_ENERGY_SHOT		"skillsound/drain_energy_shot.wav"

#define S_HEAL_CAST				"skillsound/heal_cast.wav"
#define S_HEAL_SHOT				"skillsound/heal_shot.wav"

#define S_BALANCE_LIFE_CAST		"skillsound/balance_life_cast.wav"
#define S_BALANCE_LIFE_SHOT		"skillsound/balance_life_shot.wav"

#define S_MIGHT_CAST			"skillsound/might_cast.wav"
#define S_TELEPORT_OUT			"skillsound/teleport_out.wav"

#define S_DRYAD_ROOT_CAST		"skillsound/dryad_root_cast.wav"
#define S_DRYAD_ROOT_SHOT		"skillsound/dryad_root_shot.wav"

//#define S_AURA_FLARE_CAST		"skillsound/aura_flare_cast.wav"
#define S_AURA_FLARE_SHOT		"skillsound/aura_flare_shot.wav"

#define S_SLEEP_CAST			"skillsound/sleep_cast.wav"
#define S_SLEEP_SHOT			"skillsound/sleep_shot.wav"

#define S_POISON_CLOUD_CAST		"skillsound/poison_cloud_cast.wav"
#define S_POISON_CLOUD_SHOT		"skillsound/poison_cloud_shot.wav"

//#define S_BLAZE_CAST			"skillsound/blaze_cast.wav"
//#define S_BLAZE_SHOT			"skillsound/blaze_shot.wav"
//#define S_BLAZE_EXPLOTION		"skillsound/blaze_explotion.wav"

#define S_BIND_WILL_CAST		"skillsound/bind_will_cast.wav"
#define S_BIND_WILL_SHOT		"skillsound/bind_will_shot.wav"

//#define S_HURRICANE_CAST		"skillsound/hurricane_cast.wav"
#define S_HURRICANE_SHOT		"skillsound/hurricane_shot.wav"
#define S_HURRICANE_EXPLOSION	"skillsound/hurricane_explosion.wav"

#define S_SUMMON_CAST			"skillsound/summon_cast.wav"
#define S_SUMMON_SHOT			"skillsound/summon_shot.wav"

#define S_M_HMAGICIAN_NOTARGET		"chrsound/M_HMagician_Notarget.wav"
#define S_MOFIGHTER_2H_S			"chrsound/MOFighter_2H_S_Atk_1.wav"
#define S_M_HMAGICIAN_BLACK			"chrsound/M_HMagician_Black.wav"
#define S_M_HMAGICIAN_SHOT			"chrsound/M_HMagician_Shot.wav"
#define S_M_HMAGICIAN_WHITE			"chrsound/M_HMagician_White.wav"
#define S_M_HMAGICIAN_THROW			"chrsound/M_HMagician_Throw.wav"
#define S_M_HMAGICIAN_ELEMENT		"chrsound/M_HMagician_Element.wav"
//#define S_M_HFIGHTER_BLACK		"chrsound/M_HFighter_Black.wav"
#define S_M_HFIGHTER_THROW			"chrsound/M_HFighter_Throw.wav"
#define S_H_HFIGHTER_SUB			"chrsound/M_HFighter_Sub.wav"
#define S_MHFIGHTER_PREATK_1		"chrsound/MHFighter_Preatk_1.wav"
#define S_MHFIGHTER_1H_ATK_4		"chrsound/MHFighter_1H_Atk_4.wav"
#define S_MHFIGHTER_2H_ATK_4		"chrsound/MHFighter_2H_Atk_4.wav"
#define S_MHFIGHTER_POLE_ATK_1		"chrsound/MHFighter_Pole_Atk_1.wav"
#define S_MDELF_2H_S_POLE_ATK_1		"chrsound/MDElf_2H_S_Pole_Atk_1.wav"
#define S_M_ELF_SHOT				"chrsound/M_Elf_Shot.wav"
#define S_MOFIGHTER_2H_ATK_5		"chrsound/MOFighter_2H_Atk_5.wav"
#define S_MOMAGICIAN_1H_ATK_2		"chrsound/MOMagician_1H_Atk_2.wav"
#define S_M_ORC_MAGICIAN_TYPE_B		"chrsound/M_Orc_Magician_Type_B.wav"
#define S_M_ORC_MAGICIAN_NOTARGET	"chrsound/M_Orc_Magician_Notarget.wav"
#define S_M_HMAGICIAN_SUB			"chrsound/M_HMagician_Sub.wav"
#define S_MOMAGICIAN_POLE_S_ATK_1	"chrsound/MOMagician_Pole_S_Atk_1.wav"

new const S_STEP_NOISE[][]	= {"stepsound/noise_01.wav", "stepsound/noise_02.wav", "stepsound/noise_03.wav"};

new const S_CORRUPTED_MAN[][] = {
//	"monsound/zombie_Atk.wav", 
//	"monsound/zombie_AtkWait.wav", 
	"monsound/zombie_breathe.wav", 
	"monsound/zombie_Death.wav", 
	"monsound/zombie_dmg_1.wav", 
	"monsound/zombie_dmg_2.wav", 
	"monsound/zombie_dmg_3.wav", 
	"monsound/zombie_fall.wav", 
	"monsound/zombie_step_1.wav", 
	"monsound/zombie_step_2.wav", 
//	"monsound/zombie_Swish.wav", 
	"monsound/zombie_Wait.wav"
};

new SPR_DEADLYBLOW_TRAIL;		// Deadly Blow (hit1)

new SPR_STUNSHOT_TRAIL;		// Stun Shot (trail1)
new SPR_STUNSHOT_HIT;		// Stun Shot (hit - smoke puff)
new SPR_STUN_CASTER;		// Stun Shot (cast eff @l2)

new SPR_LETHAL_SHOT;		// Lethal Shot (cast and hit eff)
new SPR_LETHALSHOT_TRAIL;	// Lethal Shot (trail)

new SPR_NUC_RING;			// Shield Stun (push trail)

new SPR_FX_M_T1014_2;		// Reflect Damage

new SPR_SHOCKWAVE;			// Drain Health (cast beams), Gate Chant (cast sub)

new SPR_STEAL;				// Steal Essence (target eff)

//#define SPR_DEATHSPIKE_FLY	"cache/animations/death_spike_fly.ukx"
new SPR_XBEAM3;				// Battle Heal (beams)

#define SPR_FORCE_BLASTER	"cache/animations/force_blaster_shot.ukx"

new SPR_A3MAGIC;		// Death Spike (trail1)
new SPR_FX_M_T0084;		// Death Spike (trail2)
new SPR_EGONBEAM;		// Death Spike (trail3)
new SPR_AURA_FLARE;		// Aura Flare (hit) @l2full

#define M_CORRUPTED_MAN		"cache/staticmeshes/zombie.usx" //zombie_m00.mdl

#define LineageEffectA		"cache/staticmeshes/LineageEffectA.usx" // Auras

// ,-----------------,
// |  NPC resources  |
// ,-----------------,

#define M_IMP				"cache/staticmeshes/imp.usx"
#define M_PORTA				"cache/staticmeshes/stone_golem.usx"
#define M_EXCURO			"cache/staticmeshes/mana_vampire.usx"
#define M_MORDEO			"cache/staticmeshes/onyx_beast.usx"
#define M_CATHEROK			"cache/staticmeshes/marsh_stakato_soldier.usx"
#define M_TREASURE_CHEST	"cache/staticmeshes/mimic.usx"
#define M_GUARDIAN_ANGEL	"cache/staticmeshes/angel.usx"
#define M_BONE_PUPPETEER	"cache/staticmeshes/black_magician.usx"
#define M_BEHEMOTH_ZOMBIE	"cache/staticmeshes/giant_zombie.usx"

new const S_IMP[][] = {
	"monsound/imp_Atk.wav",
	"monsound/imp_AtkWait.wav",
//	"monsound/imp_breathe.wav",
	"monsound/imp_Death.wav",
	"monsound/imp_dmg_1.wav",
	"monsound/imp_dmg_2.wav",
	"monsound/imp_dmg_3.wav",
	"monsound/imp_fall.wav",
//	"monsound/imp_Swish.wav",
	"monsound/imp_Wait.wav"
//	"monsound/imp_Wing.wav"
};

/*new const M_NPC_GUARD[] = "cache/staticmeshes/";
new const S_NPC_GUARD[][] = {
//	"chrsound/MNpc_Fighter_Atk_1.wav", 
//	"chrsound/MNpc_Fighter_Atk_2.wav", 
	"chrsound/MNpc_Fighter_Atk_3.wav", 
	"chrsound/MNpc_Fighter_Atk_4.wav", 
	"chrsound/MNpc_Fighter_Atk_5.wav", 
	"chrsound/MNpc_Fighter_Death.wav", 
	"chrsound/MNpc_Fighter_Dmg_1.wav", 
	"chrsound/MNpc_Fighter_Dmg_2.wav", 
	"chrsound/MNpc_Fighter_Dmg_3.wav"
};*/
new const M_NPC_GUARD_FDARKELF[] = "cache/staticmeshes/guard_FDarkElf.usx";
new const S_NPC_GUARD_FDARKELF[][] = {
	"chrsound/FNpc_Fighter_Atk_1.wav", 
	"chrsound/FNpc_Fighter_Atk_2.wav", 
	"chrsound/FNpc_Fighter_Atk_3.wav", 
//	"chrsound/FNpc_Fighter_Atk_4.wav", 
//	"chrsound/FNpc_Fighter_Atk_5.wav", 
	"chrsound/FNpc_Fighter_Death.wav", 
	"chrsound/FNpc_Fighter_Dmg_1.wav", 
	"chrsound/FNpc_Fighter_Dmg_2.wav", 
	"chrsound/FNpc_Fighter_Dmg_3.wav"
};

new const M_NPC_TELEPORTER[] = "cache/staticmeshes/teleporter_FHuman.usx";
new const S_NPC_TELEPORTER[][] = {
//	"chrsound/FNpc_Lady_Atk_1.wav", 
//	"chrsound/FNpc_Lady_Atk_2.wav", 
//	"chrsound/FNpc_Lady_Atk_3.wav", 
//	"chrsound/FNpc_Lady_Atk_4.wav", 
//	"chrsound/FNpc_Lady_Atk_5.wav", 
//	"chrsound/FNpc_Lady_Death.wav", 
	"chrsound/FNpc_Lady_Dmg_1.wav", 
	"chrsound/FNpc_Lady_Dmg_2.wav", 
	"chrsound/FNpc_Lady_Dmg_3.wav"
};

new const M_NPC_WAREHOUSE[] = "cache/staticmeshes/warehouse_keeper_MDwarf.usx";

new const M_NPC_GOURD[] = "cache/staticmeshes/drop_gourd.usx";

new const M_NPC_FMAGIC[] = "cache/staticmeshes/FMagic.usx";

new const S_NPC_FMAGIC[][] = {
	"chrsound/FHMagician_Dmg_1.wav",
	"chrsound/FHMagician_Dmg_2.wav",
//	"chrsound/FHMagician_Dmg_3.wav",
	"chrsound/FHMagician_Death.wav",
	"chrsound/F_HFighter_Sub.wav",
	"chrsound/F_HMagician_Notarget.wav",
	"chrsound/F_HMagician_Throw.wav",
	"chrsound/F_HMagician_White.wav"
};

new SPR_BSOE;
new SPR_RAPID_SHOT;

// Animated effects
new const SPR_AGGRESSION[]				= "cache/animations/aggression_new.ukx";
new const SPR_BACKSTAB_CAST[]			= "cache/animations/backstab_cast.ukx";
new const SPR_BACKSTAB_HIT[]			= "cache/animations/backstab_hit.ukx";
new const SPR_BALANCE_LIFE_SHOT[]		= "cache/animations/balance_life.ukx";
new const SPR_BLUFF[]					= "cache/animations/bluff.ukx";
new const SPR_DASH_CAST[]				= "cache/animations/dash_cast.ukx";
new const SPR_FRENZY_CAST[]				= "cache/animations/frenzy_cast.ukx";
new const SPR_DRYAD_ROOT_SHOT[]			= "cache/animations/dryadroot_shot.ukx";
new const SPR_FOCUSED_FORCE_SHOT[] 		= "cache/animations/focused_force.ukx";
//new const SPR_FREEZING_FLAME_CAST[]	= "cache/animations/freezing_flame_cast.ukx";
new const SPR_DREAMING_SPIRIT_CAST[]	= "cache/animations/dreaming_spirit_cast.ukx";
new const SPR_DREAMING_SPIRIT_SHOT[]	= "cache/animations/dreaming_spirit_shot.ukx";
new const SPR_DREAMING_SPIRIT_HIT[]		= "cache/animations/dreaming_spirit_hit.ukx";
new const SPR_SHIELD_STUN_SHOT[] 		= "cache/animations/shield_stun.ukx";
new const SPR_SLEEP_SHOT[] 				= "cache/animations/sleep_shot.ukx";
new const SPR_SILENCED[]				= "cache/animations/silence.ukx";
new const SPR_SLEEP_TARGET[]			= "cache/animations/sleeped.ukx";
// -- cast, shot
new const SPR_ULT_DEFENSE_CAST[]		= "cache/animations/ult_defense_cast.ukx";
new const SPR_ULT_DEFENSE_USE[]			= "cache/animations/ult_defense_shot.ukx";
new const SPR_SUMMON_CUBICS[]			= "cache/animations/summon_cubics.ukx";
// -- hits
new const SPR_ARMOR_CRUSH[]				= "cache/animations/armor_crush.ukx";

new const SPR_DEADLY_BLOW_HIT[]			= "cache/animations/deadly_blow_hit.ukx";
// -- buffs
new const SPR_ACUMEN[]					= "cache/animations/acumen.ukx";
new const SPR_BERSERKER_SPIRIT[]		= "cache/animations/berserker_spirit_new.ukx";
new const SPR_BLESS_THE_BODY[]			= "cache/animations/bless_the_body.ukx";
new const SPR_BLESS_THE_SOUL[]			= "cache/animations/bless_the_soul.ukx";
new const SPR_EMPOWER[]					= "cache/animations/empower.ukx";
new const SPR_MIGHT[]					= "cache/animations/might.ukx";
new const SPR_PROPHECY[]				= "cache/animations/prophecy.ukx";
// -- potions
new const SPR_POTION_HP[]				= "cache/animations/potion_hp.ukx";
new const SPR_POTION_MP[]				= "cache/animations/potion_mp.ukx";

new const SPR_LIGHT64[]				= "sprites/lights/light64.spr";	// weapon enchant lights

new SPR_QUADRUPLE_THROW;	// Soul Breaker (attack clutch)
new SPR_LIHIKIN_GLOW;		// Summon Corrupted Man (cast beams), Avanpost (destroy beams)

/**
 * Precaching of all resources.
 */
public Cache_pluginPrecache()
{
	precache_model("models/player/gign/gign.mdl");
	precache_model("models/player/sas/sas.mdl");
	precache_model("models/player/gsg9/gsg9.mdl");
	precache_model("models/player/urban/urban.mdl");
	precache_model("models/player/arctic/arctic.mdl");
	precache_model("models/player/leet/leet.mdl");
	precache_model("models/player/terror/terror.mdl");
	precache_model("models/player/guerilla/guerilla.mdl");

	precache_model(P_HANDS);
	precache_model(V_HANDS);
	precache_model(P_BOW);
	precache_model(V_BOW);
	precache_model(W_ARROW);
	precache_model(P_DAGGER);
	precache_model(V_DAGGER);
	precache_model(P_CESTUS);
	precache_model(V_CESTUS);
	precache_model(P_BLUNT);
	precache_model(V_BLUNT);
	precache_model(P_MAGICSWORD);
	precache_model(V_MAGICSWORD);
	precache_model(P_DHANDED);
	precache_model(V_DHANDED);
	precache_model(P_STAFF);
	precache_model(V_STAFF);
	precache_model(P_SHIELD_KNIFE_AV);
	precache_model(P_SHIELD_KNIFE_DC);
	precache_model(P_SHIELD_KNIFE_IC);
	precache_model(P_SHIELD_KNIFE_NM);
	precache_model(V_SHIELD_KNIFE_AV);
	precache_model(V_SHIELD_KNIFE_DC);
	precache_model(V_SHIELD_KNIFE_IC);
	precache_model(V_SHIELD_KNIFE_NM);

	precache_model(V_ACTIONS);

	precache_model(M_L2ACCESSORY);
	precache_model(M_L2WINGS);

	precache_sound("common/null.wav");

	DECAL_BPROOF1 = engfunc(EngFunc_DecalIndex,"{bproof1");

	SPR_L2DAMAGE = precache_model("cache/textures/fx_m_t0006.utx");
	SPR_L2HIT = precache_model("cache/textures/fx_m_t1014.utx");			// Stun Shot, Double Shot, ...
	SPR_DEATH_SPIKE_HIT = precache_model("cache/textures/fx_m_t0002.utx");	// Death Spike (hit)
	
	SPR_ZEROGXPLODE = precache_model("sprites/zerogxplode.spr");			// Outpost destroy effect @ TEST: impact.spr, steam1.spr, zbt_megabombexp.spr, exp2.spr

	// Smoke sprites
	SPR_EF_BAT		= precache_model("cache/animations/sm_spray.ukx");
	SPR_BLOOD_SMOKE	= precache_model("cache/animations/sm_spread_red.ukx");

	M_WOODGIBS = precache_model("cache/staticmeshes/woodgibs.usx");

	SPR_ITEM_DESTROY = precache_model("cache/animations/item_destroy.ukx");
	
	precache_model(SPR_HEROGLOW);

	// L2 Models
	precache_model(M_L2FONT_DECO);
	precache_model(M_OUTPOST);
	
	precache_model(SPR_NPCNAME_NPC);
	precache_model(SPR_NPCNAME_EAST);
	precache_model(SPR_NPCNAME_WEST);
	precache_model(SPR_PC_BUFFER);
	
	//precache_model(M_STRIDER);

	// Drop Items
	precache_model(M_L2DROP_ITEM);
	precache_model(M_L2POTION);
	precache_sound(S_POTION);

	precache_sound(S_ITEMDROP_TOMBSTONE);
	precache_sound(S_ITEMDROP_FIGURE);
	precache_sound(S_ITEMDROP_POTION);
	precache_sound(S_ITEMDROP_ETC_MONEY);
	
	precache_sound(S_PICKUP);

	// Siege Target
	precache_model(SPR_RED_CROSS_ICON);
	precache_model(SPR_BLUE_SHIELD_ICON);
	
	for(new k; k < sizeof S_DEATH; k++){
		precache_sound(S_DEATH[k])
	}
	for(new l; l < sizeof S_ARMOR_LEATHER; l++){
		precache_sound(S_ARMOR_LEATHER[l]);
	}
	for(new d; d < sizeof S_DMG; d++){
		precache_sound(S_DMG[d]);
	}
	for(new b; b < sizeof S_BOW_SMALL; b++){
		precache_sound(S_BOW_SMALL[b]);
	}
	for(new q; q < sizeof S_ITEMEQUIP; q++){
		precache_sound(S_ITEMEQUIP[q])
	}

	precache_sound(S_SOIL_SHOT);
	precache_sound(S_CLICK);
	precache_sound(S_CLICK_3);
	precache_sound(CHARSTAT_OPEN);
	precache_sound(CHARSTAT_CLOSE);
	precache_sound(SYS_IMPOSSIBLE);
	
	precache_sound(SYS_PARTY_INVITE);
	precache_sound(SYS_PARTY_LEAVE);
	
	for(new j; j < sizeof THUNDER_SOUNDS; j++){
		precache_sound(THUNDER_SOUNDS[j])
	}

	precache_sound(S_BOW_STRING);
	precache_sound(S_SIEGE_START);
	precache_sound(S_SIEGE_END);
	precache_sound(S_LEVEL_UP);
	
	precache_sound(S_SHIP_1MIN);	// time notifier sounds
	precache_sound(S_SHIP_5MIN);
	precache_sound(S_SHIP_ARRIVAL);
	
	precache_sound(S_RACE_START);	// TvT start end sounds
	precache_sound(S_RACE_END);
	
	precache_sound(S_SYS_BATTLE_START);	// LastHero start end sounds
	precache_sound(S_SYS_BATTLE_COUNT);

//	precache_model(M_REANIMATED_MAN);
	precache_model(M_CORRUPTED_MAN);

	precache_sound(S_ZEDTIME_ENTER);
	precache_sound(S_ZEDTIME_EXIT);
	
	precache_sound(S_COOLDOWN_END);
	precache_sound(S_SYS_SHORTAGE);

	precache_sound(S_STUN_CAST);
	precache_sound(S_STUN_SHOT);
	precache_sound(S_STUN_HIT);
	precache_sound(S_STUN_FLY);
	
	precache_sound(S_POWER_SHOT_CAST);
	precache_sound(S_POWER_SHOT_SHOT);
	precache_sound(S_POWER_SHOT_EXPLOTION);
	
	precache_sound(S_RAPID_SHOT_CAST);			// Snipe
	precache_sound(S_RAPID_SHOT_SHOT);			// Snipe
	
	precache_sound(S_FATAL_STRIKE_CAST);		// Backstab, Force Blaster
	precache_sound(S_FATAL_STRIKE_SHOT);		// Backstab
	
	precache_sound(S_WIND_STRIKE_SHOT);			// Force Blaster
	
	precache_sound(S_VAMPIRIC_TOUCH_CAST);		// SlowMo
	precache_sound(S_VAMPIRIC_TOUCH_SHOT);
	
	precache_sound(S_MORTAL_BLOW_CAST);			// Deadly Blow
	precache_sound(S_MORTAL_BLOW_SHOT);			// Deadly Blow
	
	precache_sound(S_DASH_CAST);				// Dash
	
	precache_sound(S_BLUFF_CAST);
	precache_sound(S_BLUFF_SHOT);
	
	for(new i; i < sizeof S_STEP_NOISE; i++){
		precache_sound(S_STEP_NOISE[i]);		// squeaking shoes
	}
	
	precache_sound(S_HEAL_CAST);				// battle heal, 
	precache_sound(S_HEAL_SHOT);				// battle heal, 
	precache_sound(S_M_HMAGICIAN_NOTARGET);		// battle heal, 
	
	precache_sound(S_BALANCE_LIFE_CAST);
	precache_sound(S_BALANCE_LIFE_SHOT);

	precache_sound(S_MIGHT_CAST);				// return,
	precache_sound(S_TELEPORT_OUT);				// return, 
	
	precache_sound(S_DRYAD_ROOT_CAST);			// dryad root
	precache_sound(S_DRYAD_ROOT_SHOT);			// dryad root
	
//	precache_sound(S_AURA_FLARE_CAST);			// aura flare
	precache_sound(S_AURA_FLARE_SHOT);			// aura flare
	
	precache_sound(S_SLEEP_CAST);
	precache_sound(S_SLEEP_SHOT);
	
	precache_sound(S_POISON_CLOUD_CAST);		// fear
	precache_sound(S_POISON_CLOUD_SHOT);		// fear
	
//	precache_sound(S_BLAZE_CAST);				// freezing flame
//	precache_sound(S_BLAZE_SHOT);				// freezing flame
//	precache_sound(S_BLAZE_EXPLOTION);			// freezing flame

	precache_sound(S_BIND_WILL_CAST);
	precache_sound(S_BIND_WILL_SHOT);
	
	precache_sound(S_WAR_CRY_CAST);				// Ultimate Defense, Frenzy, Zealot, Guts
	precache_sound(S_WAR_CRY_SHOT);				// Ultimate Defense, Frenzy, Zealot, Guts
	
	precache_sound(S_REFLECT_DAMAGE_CAST);		// Reflect Damage
	precache_sound(S_REFLECT_DAMAGE_SHOT);		// Reflect Damage
	precache_sound(S_SHIELD_SLAM_SHOT);			// Reflect Damage
	
	precache_sound(S_DRAIN_ENERGY_CAST);		// Drain Health
	precache_sound(S_DRAIN_ENERGY_SHOT);		// Drain Health
	
	precache_sound(S_MOFIGHTER_2H_S);			// Focused Force, Frenzy, Guts
	precache_sound(S_MOFIGHTER_2H_ATK_5);		// Soul Breaker
//	precache_sound(S_TOTEM_PUMA);				// Puma Spirit Totem
	
	precache_sound(S_BATTLE_ROAR_CAST);			// Battle Roar
//	precache_sound(S_BATTLE_ROAR_SHOT);			// Battle Roar

	precache_sound(S_HATE_CAST);				// Aggression
	precache_sound(S_HATE_SHOT);				// Aggression
	
//	precache_sound(S_M_HFIGHTER_BLACK);			// Drain Health
	precache_sound(S_M_HFIGHTER_THROW);			// Aggression
	precache_sound(S_H_HFIGHTER_SUB);			// Aggression
	precache_sound(S_M_HMAGICIAN_BLACK);		// death spike, 
	precache_sound(S_M_HMAGICIAN_SHOT);			// death spike, 
	precache_sound(S_M_HMAGICIAN_WHITE);		// Resurrection, 
	precache_sound(S_M_HMAGICIAN_THROW);		// Dryad Root, 
	precache_sound(S_M_HMAGICIAN_ELEMENT);		// Dryad Root,
	precache_sound(S_MDELF_2H_S_POLE_ATK_1);	// Snipe
	precache_sound(S_MHFIGHTER_2H_ATK_4);		// Dash 
	precache_sound(S_MHFIGHTER_POLE_ATK_1);		// Deadly Blow
	precache_sound(S_MHFIGHTER_1H_ATK_4);		// Backstab
	precache_sound(S_MHFIGHTER_PREATK_1);		// Backstab
	precache_sound(S_M_ELF_SHOT);				// Aura Flare
	precache_sound(S_MOMAGICIAN_1H_ATK_2);		// Steal Essence, 
	precache_sound(S_M_ORC_MAGICIAN_TYPE_B);	// Steal Essence, Dreaming Spirit
	precache_sound(S_M_ORC_MAGICIAN_NOTARGET);	// Dreaming Spirit
	precache_sound(S_M_HMAGICIAN_SUB);			// Sleep
	precache_sound(S_MOMAGICIAN_POLE_S_ATK_1);	// Hammer Crush
	
	precache_sound(S_CRITICAL_HIT);
	precache_sound(S_CRITICAL_HIT2);
	
	precache_sound(S_SUMMON_CAST);				// Summon
	precache_sound(S_SUMMON_SHOT);				// Summon
	
	//precache_sound(S_HURRICANE_CAST);
	precache_sound(S_HURRICANE_SHOT);			// Death Spike
	precache_sound(S_HURRICANE_EXPLOSION);		// Death Spike

	for(new j; j < sizeof S_CORRUPTED_MAN; j++){
		precache_sound(S_CORRUPTED_MAN[j]);		// Summon Corrupted Man
	}

	precache_model(LineageEffectA); // Auras

	SPR_BLOOD = precache_model("sprites/blood.spr");
	SPR_XBEAM4 = precache_model("sprites/xbeam4.spr");
	SPR_BULLET_TRAIL[0] = precache_model("cache/textures/tr0.utx");
	SPR_BULLET_TRAIL[1] = precache_model("cache/textures/tr1.utx");
	SPR_BULLET_TRAIL[2] = precache_model("cache/textures/tr2.utx");

	SPR_DEADLYBLOW_TRAIL	= precache_model("cache/textures/fx_m_t2039.utx");

	SPR_STUNSHOT_TRAIL		= precache_model("sprites/xbeam5.spr");
	SPR_STUN_CASTER			= precache_model("cache/textures/fx_m_t0133.utx");
	SPR_STUNSHOT_HIT		= precache_model("sprites/smokepuff.spr");
	precache_model(SPR_STUN_EFFECT);

	SPR_LETHAL_SHOT			= precache_model("cache/textures/fx_m_t0056.utx");
	
	SPR_LETHALSHOT_TRAIL	= precache_model("cache/textures/trail_pfl.utx");	// Lethal Shot (fly trail)

	SPR_NUC_RING			= precache_model("cache/textures/trail_nuc.utx");	// Shield Stun (push trail)

	SPR_FX_M_T1014_2 = precache_model("cache/textures/fx_m_t1014_2.utx");		// Reflect Damage

	precache_model(M_HURRICANE);
	//precache_model(SPR_DEATHSPIKE_FLY);
	SPR_XBEAM3 = precache_model("sprites/xbeam3.spr");							// Battle Heal (beams)
	
	SPR_A3MAGIC = precache_model("cache/animations/trail_a3.ukx");				// Death Spike (trail1)
	SPR_EGONBEAM = precache_model("cache/animations/trail_egonbeam.ukx");		// Death Spike (trail3)
	SPR_FX_M_T0084 = precache_model("cache/textures/fx_m_t0084.utx");			// Death Spike (trail2)

	precache_model(SPR_FORCE_BLASTER);
	
	SPR_AURA_FLARE = precache_model("cache/animations/aura_flare.ukx");

	SPR_SHOCKWAVE = precache_model("cache/textures/shockwave.utx");				// Drain Health (cast beams), ...

	SPR_STEAL = precache_model("cache/animations/steal.ukx");					// Steal Essence (target eff)

	// 
	// NPC
	precache_model(M_IMP);
	for(new p; p < sizeof S_IMP; p++){
		precache_sound(S_IMP[p]);
	}

	precache_model(M_TREASURE_CHEST);

	// opt precache
	if(mapIs(MAP_CRUMA_TOWER)){
		precache_model(M_PORTA);
		precache_model(M_EXCURO);
		precache_model(M_MORDEO);
		precache_model(M_CATHEROK);
	}
	else if(mapIs(MAP_BAIUMS_LAIR)){
		precache_model(M_GUARDIAN_ANGEL);
	}
	//else if(mapIs(MAP_DECK17)){
	//	precache_model(M_BONE_PUPPETEER);
	//	precache_model(M_BEHEMOTH_ZOMBIE);
	//}

	/*precache_model(M_NPC_GUARD);
	
	for(new g; g < sizeof S_NPC_GUARD; g++){
		precache_sound(S_NPC_GUARD[g]);
	}*/
	//if(GameMode == MODE_OUTPOST_DEFENSE){
	precache_model(M_NPC_GUARD_FDARKELF);
	for(new f; f < sizeof S_NPC_GUARD_FDARKELF; f++){
		precache_sound(S_NPC_GUARD_FDARKELF[f]);
	}
	//}
	
	precache_model(M_NPC_TELEPORTER);
	for(new t; t < sizeof S_NPC_TELEPORTER; t++){
		precache_sound(S_NPC_TELEPORTER[t]);
	}
	
	precache_model(M_NPC_WAREHOUSE);
	precache_model(M_NPC_GOURD);
	
	precache_model(M_NPC_FMAGIC);
	for(new f; f < sizeof S_NPC_FMAGIC; f++){
		precache_sound(S_NPC_FMAGIC[f]);
	}

	precache_model(SPR_AGGRESSION);
	precache_model(SPR_BACKSTAB_CAST);
	precache_model(SPR_BALANCE_LIFE_SHOT);
	precache_model(SPR_BLUFF);
	SPR_BSOE = precache_model("cache/animations/bsoe.ukx");
	precache_model(SPR_DASH_CAST);
	precache_model(SPR_FRENZY_CAST);
	precache_model(SPR_DRYAD_ROOT_SHOT);
	precache_model(SPR_FOCUSED_FORCE_SHOT);
	//precache_model(SPR_FREEZING_FLAME_CAST);
	precache_model(SPR_DREAMING_SPIRIT_CAST);
	precache_model(SPR_DREAMING_SPIRIT_SHOT);
	precache_model(SPR_DREAMING_SPIRIT_HIT);
	SPR_RAPID_SHOT = precache_model("cache/animations/rapid_shot.ukx");
	precache_model(SPR_SHIELD_STUN_SHOT);
	precache_model(SPR_SLEEP_SHOT);
	precache_model(SPR_SILENCED);
	precache_model(SPR_SLEEP_TARGET);
	// -- cast, shot
	precache_model(SPR_ULT_DEFENSE_CAST);
	precache_model(SPR_ULT_DEFENSE_USE);	
	precache_model(SPR_SUMMON_CUBICS);
	// -- hits
	precache_model(SPR_ARMOR_CRUSH);
	precache_model(SPR_BACKSTAB_HIT);
	precache_model(SPR_DEADLY_BLOW_HIT);
	// -- buffs
	precache_model(SPR_ACUMEN);
	precache_model(SPR_BERSERKER_SPIRIT);
	precache_model(SPR_BLESS_THE_BODY);
	precache_model(SPR_BLESS_THE_SOUL);
	precache_model(SPR_EMPOWER);
	precache_model(SPR_MIGHT);
	precache_model(SPR_PROPHECY);
	// -- potions
	precache_model(SPR_POTION_HP);
	precache_model(SPR_POTION_MP);

	precache_model(SPR_LIGHT64); // weapon enchant lights

	SPR_QUADRUPLE_THROW = precache_model("cache/animations/quadruple_throw.ukx");		// Soul Breaker (attack clutch)
	SPR_LIHIKIN_GLOW = precache_model("cache/textures/beam_lihikin.utx");				// Summon Corrupted Man (cast beams), Avanpost (destroy beams)
}
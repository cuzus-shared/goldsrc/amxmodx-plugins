/* AMX Mod X
*   No Weapon Drop on Death
*
* (c) Copyright 2006 by VEN
*
* This file is provided as is (no warranties)
*
*
*  Requested by [CHEEZ-T] - http://www.amxmodx.org/forums/viewtopic.php?t=24639
*
*  Plugin disables weapon drop on death (shiled also supported, bomb excluded).
*
*  Engine and fakemeta module required.
*
*  CVAR amx_noweapdrop_ondeath (0: OFF, 1: ON, default: 1) disables/enables the plugin
*
*  Versions:
*    0.2   - solved possible server crash issues
*            maxents num uses global int now
*    0.1.2 - added entity validity extra check
*    0.1.1 - added shield support
*    0.1   - initial release
*/
#include <amxmodx>
#include <engine>
#include <fakemeta>

#define CVAR "amx_noweapdrop_ondeath"

#define WBOX "models/w_weaponbox.mdl"
#define BOMB "models/w_backpack.mdl"
#define SHLD "models/w_shield.mdl"

#define MAX_PLAYERS 32

new g_entid[MAX_PLAYERS + 1];
new g_maxents;

public plugin_init()
{
	register_plugin("No Weapon Drop on Death", "0.2", "VEN");
	
	register_forward(FM_SetModel, "fwSetModel");
	register_cvar(CVAR, "1");
	g_maxents = get_global_int(GL_maxEntities);
}

public fwSetModel(entid, model[])
{
	if(!is_valid_ent(entid) || !equal(model, WBOX, 9) || !get_cvar_num(CVAR))
		return FMRES_IGNORED

	new id = entity_get_edict(entid, EV_ENT_owner)
	if(!id || !is_user_connected(id) || is_user_alive(id))
		return FMRES_IGNORED

	if(equal(model, SHLD)) {
		kill_entity(entid)
		return FMRES_IGNORED
	}

	if(equal(model, WBOX)) {
		g_entid[id] = entid
		return FMRES_IGNORED
	}

	if(entid != g_entid[id])
		return FMRES_IGNORED

	g_entid[id] = 0

	if(equal(model, BOMB))
		return FMRES_IGNORED

	for(new i = 1; i <= g_maxents; ++i) {
		if(is_valid_ent(i) && entid == entity_get_edict(i, EV_ENT_owner)) {
			kill_entity(entid)
			kill_entity(i)
		}
	}

	return FMRES_IGNORED;
}

stock kill_entity(id)
{
	entity_set_int(id, EV_INT_flags, entity_get_int(id, EV_INT_flags)|FL_KILLME)
}
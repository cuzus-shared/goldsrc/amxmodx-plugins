#include <amxmodx>  
#include <cstrike>  
#include <fakemeta>

new PrecachedTotal[4];
//new PModel, PSound, PGeneric;

public plugin_precache()
{
	register_plugin("UnPrecacher", "0.1", "Proo.Noob");
	
	// NOTE: Functions will calls every model/spr/sound use in game
	/*PModel =*/
	//register_forward(FM_PrecacheModel, "fmPrecacheModel");
	/*PSound =*/
	register_forward(FM_PrecacheSound, "fmPrecacheSound");
	/*PGeneric =*/
	register_forward(FM_PrecacheGeneric, "fmPrecacheGeneric");
}

public plugin_init()
{
	server_print("<UnPrecacher> Un-precached resources %d of total %d", (PrecachedTotal[0]+PrecachedTotal[1]+PrecachedTotal[2]), PrecachedTotal[3]);
	for(new i=0; i<4; i++)
		PrecachedTotal[i] = 0;
}

/*public plugin_cfg()
{
	server_print("<UnPrecacher> Removing forwards ...");
	unregister_forward(FM_PrecacheModel, PModel);
	unregister_forward(FM_PrecacheSound, PSound);
	unregister_forward(FM_PrecacheGeneric, PGeneric);
}*/

stock fmPrecacheModel(const model[])
{
	//server_print("fmPrecacheModel(): %s", model);
	
	// �� ������� ������� ������ �� �������-��� ��� �������� ������� �������
	new const MODELS_LIST[][] = 
	{
//-----------------------------------------
// DIR: /valve/models
//-----------------------------------------
		// "models/w_kevlar.mdl",
		// "models/w_battery.mdl",
		// "models/w_antidote.mdl",
		// "models/w_security.mdl",
		// "models/w_longjump.mdl",
		// "models/w_assault.mdl",
		// "models/w_thighpack.mdl",
		// "models/w_shotbox.mdl",
//-----------------------------------------
// DIR: /cstrike/models
//-----------------------------------------
		//"models/rshell.mdl",
		//"models/rshell_big.mdl",
		//"models/shotgunshell.mdl",
		//"models/pshell.mdl",
		//"models/w_9mmclip.mdl",
// default weapons
		// "models/v_awp.mdl",
		// "models/w_awp.mdl",
		// "models/v_g3sg1.mdl",
		// "models/w_g3sg1.mdl",
		// "models/v_ak47.mdl",
		// "models/w_ak47.mdl",
		// "models/v_scout.mdl",
		// "models/w_scout.mdl",
		// "models/v_m249.mdl",
		// "models/w_m249.mdl",
		// "models/v_m4a1.mdl",
		// "models/w_m4a1.mdl",
		// "models/v_sg552.mdl",
		// "models/w_sg552.mdl",
		// "models/v_aug.mdl",
		// "models/w_aug.mdl",
		// "models/v_sg550.mdl",
		// "models/w_sg550.mdl",
		// "models/v_m3.mdl",
		// "models/w_m3.mdl",
		// "models/v_xm1014.mdl",
		// "models/w_xm1014.mdl",
		//"models/v_usp.mdl",
		//"models/w_usp.mdl",
		// "models/v_mac10.mdl",
		// "models/w_mac10.mdl",
		// "models/v_ump45.mdl",
		// "models/w_ump45.mdl",
		// "models/v_fiveseven.mdl",
		// "models/w_fiveseven.mdl",
		// "models/v_p90.mdl",
		// "models/w_p90.mdl",
		// "models/v_deagle.mdl",
		// "models/w_deagle.mdl",
		// "models/v_p228.mdl",
		// "models/w_p228.mdl",
		//"models/v_glock18.mdl",
		//"models/w_glock18.mdl",
		// "models/v_tmp.mdl",
		// "models/w_tmp.mdl",
		// "models/v_elite.mdl",
		// "models/w_elite.mdl",
		// "models/v_galil.mdl",
		// "models/w_galil.mdl",
		// "models/v_famas.mdl",
		// "models/w_famas.mdl",
		//"models/v_knife.mdl",
		//"models/w_knife.mdl",
		//"models/v_mp5.mdl",
		//"models/w_mp5.mdl"
		//"models/shield/v_shield_usp.mdl",
		// "models/shield/v_shield_fiveseven.mdl",
		// "models/shield/v_shield_deagle.mdl",
		// "models/shield/v_shield_p228.mdl",
		//"models/shield/v_shield_knife.mdl",
		//"models/shield/v_shield_glock18.mdl",
		// "models/shield/v_shield_flashbang.mdl",
		// "models/shield/v_shield_hegrenade.mdl",
		// "models/shield/v_shield_smokegrenade.mdl",
		// "models/v_flashbang.mdl",
		// "models/v_hegrenade.mdl",
		// "models/v_smokegrenade.mdl",
		// "models/v_c4.mdl"
		//"models/w_backpack.mdl"
		//"models/w_weaponbox.mdl"
	};
	
	PrecachedTotal[3]++;

	for(new i = 0; i < sizeof(MODELS_LIST); i++){
		if(containi(model, MODELS_LIST[i]) != -1){
			PrecachedTotal[0]++;
			forward_return(FMV_CELL, 0);
			return FMRES_SUPERCEDE;
		}
	}

	return FMRES_IGNORED;
}

public fmPrecacheSound(const snd[])
{
	//server_print("fmPrecacheSound(): %s", snd);
	
	new const SOUNDS_LIST[][] = 
	{
		"common/bodysplat.wav",
		"items/flashlight1.wav",
//-----------------------------------------
// DIR: /valve/sound/buttons (FULL)
//-----------------------------------------
		"buttons/bell1.wav",
		"buttons/blip1.wav",
		"buttons/blip2.wav",
		"buttons/button1.wav",
		"buttons/button2.wav",
		"buttons/button3.wav",
		"buttons/button4.wav",
		"buttons/button5.wav",
		"buttons/button6.wav",
		"buttons/button7.wav",
		"buttons/button8.wav",
		"buttons/button9.wav",
		"buttons/button10.wav",
		"buttons/button11.wav",
		"buttons/latchlocked1.wav",
		"buttons/latchlocked2.wav",
		"buttons/latchunlocked1.wav",
		"buttons/latchunlocked2.wav",
		"buttons/lever1.wav",
		"buttons/lever2.wav",
		"buttons/lever3.wav",
		"buttons/lever4.wav",
		"buttons/lever5.wav",
		"buttons/lightswitch2.wav",
		//"buttons/spark1.wav",			//ka_traffic2
		//"buttons/spark2.wav",			//ka_traffic2
		//"buttons/spark3.wav",			//ka_traffic2
		//"buttons/spark4.wav",			//ka_traffic2
		//"buttons/spark5.wav",			//ka_traffic2
		//"buttons/spark6.wav",			//ka_traffic2
//-----------------------------------------
// DIR: /valve/sound/plats (FULL)
//-----------------------------------------
		"plats/bigmove1.wav",
		"plats/bigmove2.wav",
		"plats/bigstop1.wav",
		"plats/bigstop2.wav",
		"plats/elevbell1.wav",
		"plats/elevmove1.wav",
		"plats/elevmove2.wav",
		"plats/elevmove3.wav",
		"plats/freightmove1.wav",
		"plats/freightmove2.wav",
		"plats/freightstop1.wav",
		"plats/heavymove1.wav",
		"plats/heavystop1.wav",
		"plats/heavystop2.wav",
		"plats/platmove1.wav",
		"plats/platstop1.wav",
		//"plats/rackmove1.wav",		//ka_traffic2
		"plats/rackstop1.wav",
		"plats/railmove1.wav",
		"plats/railstop1.wav",
		//"plats/squeekmove1.wav",		//ka_traffic2
		"plats/squeekstop1.wav",
		"plats/talkmove1.wav",
		"plats/talkmove2.wav",
		"plats/talkstop1.wav",
		"plats/train1.wav",
		"plats/train2.wav",
		"plats/train_use1.wav",
		"plats/ttrain1.wav",
		"plats/ttrain2.wav",
		"plats/ttrain3.wav",
		"plats/ttrain4.wav",
		"plats/ttrain6.wav",
		"plats/ttrain7.wav",
		"plats/ttrain_brake1.wav",
		"plats/ttrain_start1.wav",
		"plats/vehicle_brake1.wav",
		"plats/vehicle_start1.wav",
		"plats/vehicle_ignition.wav",
//-----------------------------------------
// DIR: ???
//-----------------------------------------
		"plats/vehicle1.wav",
		"plats/vehicle2.wav",
		"plats/vehicle3.wav",
		"plats/vehicle4.wav",
		"plats/vehicle5.wav",
		"plats/vehicle6.wav",
		"plats/vehicle7.wav",
//-----------------------------------------
// DIR: /valve/sound/debris
//-----------------------------------------
		// env_breakable destroy sounds
		//"debris/glass1.wav",
		//"debris/glass2.wav",
		//"debris/glass3.wav",
		//"debris/wood1.wav",
		//"debris/wood2.wav",
		//"debris/wood3.wav",
//-----------------------------------------
// DIR: /valve/sound/player
//-----------------------------------------
		//"player/sprayer.wav",
		"player/pl_fallpain1.wav",
		"player/pl_fallpain2.wav",
		"player/pl_fallpain3.wav",
		"player/heartbeat1.wav",
		"player/geiger1.wav",
		"player/geiger2.wav",
		"player/geiger3.wav",
		"player/geiger4.wav",
		"player/geiger5.wav",
		"player/geiger6.wav",
		"player/breathe1.wav",
		"player/breathe2.wav",
//-----------------------------------------
// DIR: /cstrike/sound/radio
//-----------------------------------------
		"radio/blow.wav",
		"radio/bombdef.wav",
		"radio/bombpl.wav",
		"radio/circleback.wav",
		"radio/clear.wav",
		"radio/com_followcom.wav",
		"radio/com_getinpos.wav",
		"radio/com_go.wav",
		"radio/com_reportin.wav",
		"radio/ct_affirm.wav",
		"radio/ct_backup.wav",
		"radio/ct_coverme.wav",
		"radio/ct_enemys.wav",
		"radio/ct_fireinhole.wav",
		"radio/ct_imhit.wav",
		"radio/ct_inpos.wav",
		"radio/ct_point.wav",
		"radio/ct_reportingin.wav",
		"radio/ctwin.wav",
		"radio/elim.wav",
		"radio/enemydown.wav",
		"radio/fallback.wav",
		"radio/fireassis.wav",
		"radio/flankthem.wav",
		"radio/followme.wav",
		"radio/getout.wav",
		"radio/go.wav",
		"radio/hitassist.wav",
		"radio/hosdown.wav",
		"radio/letsgo.wav",
		"radio/locknload.wav",
		"radio/matedown.wav",
		"radio/meetme.wav",
		"radio/moveout.wav",
		"radio/negative.wav",
		"radio/position.wav",
		"radio/regroup.wav",
		"radio/rescued.wav",
		"radio/roger.wav",
		"radio/rounddraw.wav",
		"radio/sticktog.wav",
		"radio/stormfront.wav",
		"radio/takepoint.wav",
		"radio/terwin.wav",
		"radio/vip.wav",
//-----------------------------------------
// DIR: /cstrike/sound/weapons
//-----------------------------------------
		"weapons/c4_beep1.wav",
		"weapons/c4_beep2.wav",
		"weapons/c4_beep3.wav",
		"weapons/c4_beep4.wav",
		"weapons/c4_beep5.wav",
		"weapons/c4_click.wav",
		"weapons/c4_disarm.wav",
		"weapons/c4_disarmed.wav",
		"weapons/c4_explode1.wav",
		"weapons/c4_plant.wav",
		"weapons/headshot2.wav",
		"weapons/debris1.wav",
		"weapons/debris2.wav",
		"weapons/debris3.wav",
		// !!!!!!!!!!!!!!!!!!!!
		// WARN: tested ==> BAD if unprecached
		// !!!!!!!!!!!!!!!!!!!!
		//"weapons/357_cock1.wav",
		//"weapons/boltpull1.wav",
		//"weapons/boltup.wav",
		//"weapons/boltdown.wav",
		//"weapons/zoom.wav",
		//"weapons/awp1.wav",
		//"weapons/awp_deploy.wav",
		//"weapons/awp_clipin.wav",
		//"weapons/awp_clipout.wav",
		// "weapons/g3sg1-1.wav",
		// "weapons/g3sg1_slide.wav",
		// "weapons/g3sg1_clipin.wav",
		// "weapons/g3sg1_clipout.wav",
		// "weapons/ak47-1.wav",
		// "weapons/ak47-2.wav",
		// "weapons/ak47_clipout.wav",
		// "weapons/ak47_clipin.wav",
		// "weapons/ak47_boltpull.wav",
		// "weapons/scout_fire-1.wav",
		// "weapons/scout_bolt.wav",
		// "weapons/scout_clipin.wav",
		// "weapons/scout_clipout.wav",
		// "weapons/m249-1.wav",
		// "weapons/m249-2.wav",
		// "weapons/m249_boxout.wav",
		// "weapons/m249_boxin.wav",
		// "weapons/m249_chain.wav",
		// "weapons/m249_coverup.wav",
		// "weapons/m249_coverdown.wav",
		// "weapons/m4a1-1.wav",
		// "weapons/m4a1_unsil-1.wav",
		// "weapons/m4a1_unsil-2.wav",
		// "weapons/m4a1_clipin.wav",
		// "weapons/m4a1_clipout.wav",
		// "weapons/m4a1_boltpull.wav",
		// "weapons/m4a1_deploy.wav",
		// "weapons/m4a1_silencer_on.wav",
		// "weapons/m4a1_silencer_off.wav",
		// "weapons/sg552-1.wav",
		// "weapons/sg552-2.wav",
		// "weapons/sg552_clipout.wav",
		// "weapons/sg552_clipin.wav",
		// "weapons/sg552_boltpull.wav",
		// "weapons/aug-1.wav",
		// "weapons/aug_clipout.wav",
		// "weapons/aug_clipin.wav",
		// "weapons/aug_boltpull.wav",
		// "weapons/aug_boltslap.wav",
		// "weapons/aug_forearm.wav",
		// "weapons/sg550-1.wav",
		// "weapons/sg550_boltpull.wav",
		// "weapons/sg550_clipin.wav",
		// "weapons/sg550_clipout.wav",
		// "weapons/m3-1.wav",
		// "weapons/m3_insertshell.wav",
		// "weapons/m3_pump.wav",
		//"weapons/reload1.wav",
		//"weapons/reload3.wav",
		//"weapons/xm1014-1.wav",
		//"weapons/usp1.wav",
		//"weapons/usp2.wav",
		//"weapons/usp_unsil-1.wav",
		//"weapons/usp_clipout.wav",
		//"weapons/usp_clipin.wav",
		//"weapons/usp_silencer_on.wav",
		//"weapons/usp_silencer_off.wav",
		//"weapons/usp_sliderelease.wav",
		//"weapons/usp_slideback.wav",
		// "weapons/mac10-1.wav",
		// "weapons/mac10_clipout.wav",
		// "weapons/mac10_clipin.wav",
		// "weapons/mac10_boltpull.wav",
		// "weapons/ump45-1.wav",
		// "weapons/ump45_clipout.wav",
		// "weapons/ump45_clipin.wav",
		// "weapons/ump45_boltslap.wav",
		// "weapons/fiveseven-1.wav",
		// "weapons/fiveseven_clipout.wav",
		// "weapons/fiveseven_clipin.wav",
		// "weapons/fiveseven_sliderelease.wav",
		// "weapons/fiveseven_slidepull.wav",
		// "weapons/p90-1.wav",
		// "weapons/p90_clipout.wav",
		// "weapons/p90_clipin.wav",
		// "weapons/p90_boltpull.wav",
		// "weapons/p90_cliprelease.wav",
		// "weapons/deagle-1.wav",
		// "weapons/deagle-2.wav",
		//"weapons/de_clipout.wav",
		//"weapons/de_clipin.wav",
		//"weapons/de_deploy.wav",
		// "weapons/p228-1.wav",
		// "weapons/p228_clipout.wav",
		// "weapons/p228_clipin.wav",
		// "weapons/p228_sliderelease.wav",
		// "weapons/p228_slidepull.wav",
		//"weapons/knife_deploy1.wav",
		//"weapons/knife_hit1.wav",
		//"weapons/knife_hit2.wav",
		//"weapons/knife_hit3.wav",
		//"weapons/knife_hit4.wav",
		//"weapons/knife_slash1.wav",
		//"weapons/knife_slash2.wav",
		//"weapons/knife_stab.wav",
		//"weapons/knife_hitwall1.wav",
		//"weapons/glock18-1.wav",
		//"weapons/glock18-2.wav",
		//"weapons/clipout1.wav",
		//"weapons/clipin1.wav",
		//"weapons/sliderelease1.wav",
		//"weapons/slideback1.wav",
		//"weapons/mp5-1.wav",
		//"weapons/mp5-2.wav",
		//"weapons/mp5_clipout.wav",
		//"weapons/mp5_clipin.wav",
		//"weapons/mp5_slideback.wav",
		// "weapons/tmp-1.wav",
		// "weapons/tmp-2.wav",
		// "weapons/elite_fire.wav",
		// "weapons/elite_reloadstart.wav",
		// "weapons/elite_leftclipin.wav",
		// "weapons/elite_clipout.wav",
		// "weapons/elite_sliderelease.wav",
		// "weapons/elite_rightclipin.wav",
		// "weapons/elite_deploy.wav",
		// "weapons/flashbang-1.wav",
		// "weapons/flashbang-2.wav",
		// "weapons/hegrenade-1.wav",
		// "weapons/hegrenade-2.wav",
		// "weapons/he_bounce-1.wav",
		// "weapons/sg_explode.wav",
		// "weapons/galil-1.wav",
		// "weapons/galil-2.wav",
		// "weapons/galil_clipout.wav",
		// "weapons/galil_clipin.wav",
		// "weapons/galil_boltpull.wav",
		// "weapons/famas-1.wav",
		// "weapons/famas-2.wav",
		// "weapons/famas_clipout.wav",
		// "weapons/famas_clipin.wav",
		// "weapons/famas_boltpull.wav",
		// "weapons/famas_boltslap.wav",
		// "weapons/famas_forearm.wav",
		// "weapons/famas-burst.wav",
		//"weapons/generic_reload.wav",
		//"weapons/pinpull.wav",
		//"weapons/dryfire_pistol.wav",
		//"weapons/dryfire_rifle.wav",
	// <!-- /valve
		"weapons/grenade_hit1.wav",
		"weapons/grenade_hit2.wav",
		"weapons/grenade_hit3.wav",
		//"weapons/bullet_hit1.wav",
		//"weapons/bullet_hit2.wav",
		"weapons/explode3.wav",
		"weapons/explode4.wav",
		"weapons/explode5.wav",
	// -->
//-----------------------------------------
// DIR: /cstrike/sound/items
//-----------------------------------------
		"items/kevlar.wav",
		"items/equip_nvg.wav",
		"items/nvg_off.wav",
		"items/nvg_on.wav",
		"items/tr_kevlar.wav",
//-----------------------------------------
// DIR: /valve/sound/items
//-----------------------------------------
		"items/suitchargeok1.wav",
		// !!!!!!!!!!!!!!!!!!!!
		// WARN: tested
		// !!!!!!!!!!!!!!!!!!!!
		//"items/9mmclip1.wav",
		//"items/weapondrop1.wav",
		//"items/ammopickup2.wav",		
//-----------------------------------------
// DIR: /valve/sound/common
//-----------------------------------------
		"common/npc_step1.wav",
		"common/npc_step2.wav",
		"common/npc_step3.wav",
		"common/npc_step4.wav",
		"common/wpn_hudoff.wav",
		"common/wpn_moveselect.wav",
		"common/wpn_select.wav",
		"common/wpn_denyselect.wav",
		//"common/null.wav",
		"common/bodydrop3.wav",
		"common/bodydrop4.wav",
//-----------------------------------------
// DIR: /valve/sound/ambience
//-----------------------------------------
		"ambience/quail1.wav",
//-----------------------------------------
// DIR: /cstrike/sound/events
//-----------------------------------------
		"events/tutor_msg.wav",
		"events/enemy_died.wav",
		"events/friend_died.wav",
		"events/task_complete.wav",
//-----------------------------------------
// DIR: ???
//-----------------------------------------
		"foxer/soundez.wav",
//-----------------------------------------
// DIR: /cstrike/sound/hostage
//-----------------------------------------
		"hostage/hos1.wav",
		"hostage/hos2.wav",
		"hostage/hos3.wav",
		"hostage/hos4.wav",
		"hostage/hos5.wav"
	};

	PrecachedTotal[3]++;
	
	for(new i = 0; i < sizeof(SOUNDS_LIST); i++){
		if(containi(snd, SOUNDS_LIST[i]) != -1){
			PrecachedTotal[1]++;
			forward_return(FMV_CELL, 0);
			return FMRES_SUPERCEDE;
		}
	}
	return FMRES_IGNORED;
}

public fmPrecacheGeneric(const file[])
{
	//server_print("fmPrecacheGeneric(): %s", file);

	new const GENERICS_LIST[][] = 
	{
		"sprites/scope_arc.tga",
		"sprites/scope_arc_nw.tga",
		"sprites/scope_arc_ne.tga",
		"sprites/scope_arc_sw.tga",
		//"sprites/blood.spr",
		//"sprites/bloodspray.spr",
		//"sprites/smokepuff.spr",
		//"sprites/steam1.spr",
		//"sprites/laserbeam.spr",
		//"sprites/voiceicon.spr",
		//"sprites/radio.spr",
		"sprites/bubble.spr",
		"sprites/eexplo.spr",
		"sprites/fexplo.spr",
		"sprites/fexplo1.spr",
		"sprites/zerogxplode.spr",
		"sprites/WXplo1.spr"
	};

	PrecachedTotal[3]++;
	
	for(new i = 0; i < sizeof(GENERICS_LIST); i++){
		if(containi(file, GENERICS_LIST[i]) != -1){
			PrecachedTotal[2]++;
			forward_return(FMV_CELL, 0);
			return FMRES_SUPERCEDE;
		}
	}
	
	return FMRES_IGNORED;
}
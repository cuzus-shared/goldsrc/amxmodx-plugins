"Resource/UI/ScoreBoard.res"
{
	"ClientScoreBoard"
	{
		"ControlName"		"CClientScoreBoardDialog"
		"fieldName"			"ClientScoreBoard"
		"xpos"				"c-260"
		"ypos"				"32" // 48
		"wide"				"520"
		"tall"				"0"
		"autoResize"		"0"
		"pinCorner"			"0"
		"visible"			"0"
		"enabled"			"1"
		"tabPosition"		"0"
	}
	"ServerName"
	{
		"ControlName"		"Label"
		"fieldName"			"ServerName"
		"xpos"				"3"
		"ypos"				"2"
		"wide"				"250"
		"tall"				"24"
		"autoResize"		"0"
		"pinCorner"			"0"
		"visible"			"0"
		"enabled"			"0"
		"labelText"			""
		"textAlignment"		"north-west"
		"dulltext"			"0"
		"brighttext"		"1"
	}
	"PlayerList"
	{
		"ControlName"		"SectionedListPanel"
		"fieldName"			"PlayerList"
		"xpos"				"0"
		"ypos"				"0"
		"wide"				"520"
		"tall"				"340"
		"autoResize"		"0"
		"pinCorner"			"0"
		"visible"			"1"
		"enabled"			"1"
		"tabPosition"		"0"
		"autoresize"		"3"
		"linespacing"		"13"
	}
	"onscrmsg_pattern01_1"
	{
		"ControlName"		"ImagePanel"
		"fieldName"			"onscrmsg_pattern01_1"
		"xpos"				"168"
		"ypos"				"0"
		"wide"				"292"
		"tall"				"32"
		"autoResize"		"0"
		"pinCorner"			"0"
		"visible"			"1"
		"enabled"			"1"
		"textAlignment"		"west"
		"image"				"gfx/onscrmsg_pattern01_1"
		"zpos"				"1"
	}
	// "onscrmsg_pattern01_2"
	// {
		// "ControlName"		"ImagePanel"
		// "fieldName"			"onscrmsg_pattern01_2"
		// "xpos"				"165"
		// "ypos"				"45"
		// "wide"				"292"
		// "tall"				"32"
		// "autoResize"		"0"
		// "pinCorner"			"0"
		// "visible"			"1"
		// "enabled"			"1"
		// "textAlignment"		"west"
		// "image"				"gfx/onscrmsg_pattern01_2"
		// "zpos"				"1"
	// }
}
// 
// Line-Strike GUI by Geekrainian.
// (c) 2009-2014
// https://cuzus.games
// 
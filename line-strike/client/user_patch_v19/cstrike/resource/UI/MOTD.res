"Resource/UI/MOTD.res"
{
	"ClientMOTD"
	{
		"ControlName"			"Frame"
		"fieldName"				"ClientMOTD"
		"xpos"					"130"
		"ypos"					"40" //60
		"wide"					"283" //310
		"tall"					"350" //370
		"autoResize"			"0"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"setTitleBarVisible"	"0"
	}
	"Message"
	{
		"ControlName"			"HTML" //"TextEntry"
		"fieldName"				"Message"
		"xpos"					"91"
		"ypos"					"64" //84
		"wide"					"263" //290
		"tall"					"267" //287
		"autoResize"			"0" //3
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"textHidden"			"0"
		"editable"				"0"
		"maxchars"				"-1"
		"NumericInputOnly"		"0"
	}
	"WindowTop"
	{
		"ControlName"			"ImagePanel"
		"fieldName" 			"WindowTop"
		"xpos"					"91" //130
		"ypos"					"53" //73
		"wide"					"308"
		"tall"					"19"
		"autoResize"			"0"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"textAlignment"			"west"
		"image"					"gfx/ui/windowtop"
		//"scaleImage"			"1"
		"zpos"					"1"
	}
	"ok"
	{
		"ControlName"			"Button"
		"fieldName"				"ok"
		"xpos"					"270"
		"ypos"					"54"
		"wide"					"10"
		"tall"					"10"
		"autoResize"			"0"
		"pinCorner"				"0" //2
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"labelText"				" "
		"textAlignment"			"center"
		"dulltext"				"0"
		"brighttext"			"0"
		"Command"				"okay"
		"Default"				"1"
		"zpos"					"2"
	}
}
// 
// Line-Strike GUI by Geekrainian.
// (c) 2009-2014
// https://cuzus.games
// 
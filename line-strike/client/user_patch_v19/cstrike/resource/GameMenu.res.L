"GameMenu"
{
	"5"
	{
		"label" "#GameUI_GameMenu_L2LogIn"
		"command" "engine connect 95.31.33.84:30000"
		"OnlyInGame" "0"
	}
	"7"
	{
		"label" ""
		"command" ""
	}
	"8"
	{
		"label" ""
		"command" ""
	}
	"9"
	{
		"label" ""
		"command" ""
	}
	"10"
	{
		"label" ""
		"command" ""
	}
	"11"
	{
		"label" ""
		"command" ""
	}	
	"12"
	{
		"label" ""
		"command" ""
	}
	"13"
	{
		"label" ""
		"command" ""
	}
	"14"
	{
		"label" "#GameUI_GameMenu_Disconnect"
		"command" "Disconnect"
		"OnlyInGame" "1"
		"notsingle" "1"
	}
	"15"
	{
		"label" "#GameUI_GameMenu_L2NewAccount"
		"command" "OpenServerBrowser"
	}	
	"16"
	{
		"label" "#GameUI_GameMenu_L2LostAccount"
		"command" ""
	}
	"17"
	{
		"label" "#GameUI_GameMenu_L2Options"
		"command" "engine toggleconsole"
	}
	"18"
	{
		"label" "#GameUI_GameMenu_L2ProductionTeam"
		"command" "OpenNewGameDialog"
	}
	"19"
	{
		"label" "#GameUI_GameMenu_L2Replay"
		"command" "OpenLoadDemoDialog"
	}
}
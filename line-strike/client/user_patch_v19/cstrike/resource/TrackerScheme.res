Scheme
{
	Colors
	{
		// base colors
		"BaseText"			"156 154 144 225"	// used in text windows, lists
		"BrightBaseText"	"255 255 255 215"	// brightest text
		"SelectedText"		"255 255 255 200"	// selected text
		"DimBaseText"		"178 178 159 225"	// dim base text
		"LabelDimText"		"178 178 159 225"	// used for info text
		"ControlText"		"156 154 144 210"	// used in all text controls
		"BrightControlText"	"255 255 255 210"	// use for selected controls
		"DisabledText1"		"117 128 111 225"	// disabled text
		"DisabledText2"		"117 128 111 225"	// overlay color for disabled text (to give that inset look)
		"DimListText"		"117 134 102 225"	// offline friends, unsubscribed games, etc.

		// backgrounds
		"ControlBG"			"35 39 38 5" 		// background color of controls		// loading dialog window bg
		"ControlBG0"		"35 39 38 90"
		"ControlDarkBG"		"35 39 38 70" 		// darker background color; used for background of scrollbars
		"WindowBG"			"35 39 38 110"		// background color of text edit panes (chat, text entries, etc.)	// console bg
		"SelectionBG"		"30 110 110 80" 	// background color of any selected text or menu item
		"SelectionBG2"		"10 90 90 80" 		// selection background in window w/o focus
		"ListBG"			"35 39 38 80"		// background of server browser, buddy list, etc.

		// titlebar colors
		"TitleText"			"255 255 255 230"
		"TitleDimText"		"136 145 128 255"
		"TitleBG"			"76 88 68 0"
		"TitleDimBG"		"76 88 68 0"
		
		// slider tick colors
		"SliderTickColor"	"75 75 75 255"
		"SliderTrackColor"	"102 255 255 255"

		// border colors
		"BorderBright"		"106 106 106 175"	// the lit side of a control
		"BorderDark"		"78 78 78 175"		// the dark/unlit side of a control
		"BorderSelection"	"198 194 182 175"	// the additional border color for displaying the default/selected button
	}

	///////////////////// BASE SETTINGS ////////////////////////
	// default settings for all panels
	// controls use these to determine their settings
	BaseSettings
	{
		"FgColor"							"ControlText"
		"BgColor"							"ControlBG"
		"LabelBgColor"						"ControlBG"
		"SubPanelBgColor"					"ControlBG"

		"DisabledFgColor1"					"DisabledText1" 
		"DisabledFgColor2"					"DisabledText2"	// set this to the BgColor if you don't want it to draw

		"TitleBarFgColor"					"TitleText"
		"TitleBarDisabledFgColor"			"TitleDimText"
		"TitleBarBgColor"					"TitleBG"
		"TitleBarDisabledBgColor"			"TitleDimBG"

		"TitleBarIcon"						"resource/popup"
		"TitleBarDisabledIcon"				"resource/popup"

		"TitleButtonFgColor"				"BorderBright"
		"TitleButtonBgColor"				"ControlBG"
		"TitleButtonDisabledFgColor"		"TitleDimText"
		"TitleButtonDisabledBgColor"		"TitleDimBG"

		"TextCursorColor"					"BaseText"			// color of the blinking text cursor in text entries
		"URLTextColor"						"BrightBaseText"	// color that URL's show up in chat window

		Menu
		{
			"FgColor"					"DimBaseText"
			"BgColor"					"ControlBG"
			"ArmedFgColor"				"BrightBaseText"
			"ArmedBgColor"				"SelectionBG"
			"DividerColor"				"BorderDark"
			"TextInset"					"6"
		}
		// the little arrow on the side of boxes that triggers drop down menus
		MenuButton
		{
			"ButtonArrowColor"			"DimBaseText"		// color of arrows
		   	"ButtonBgColor"				"WindowBG"			// bg color of button. same as background color of text edit panes 
			"ArmedArrowColor"			"BrightBaseText"	// color of arrow when mouse is over button
			"ArmedBgColor"				"DimBaseText"		// bg color of button when mouse is over button
		}
		Slider
		{
			"SliderFgColor"				"ControlBG"			// handle with which the slider is grabbed
			"SliderBgColor"				"ControlDarkBG"		// area behind handle
		}
		ScrollBarSlider
		{
			"BgColor"					"ControlBG"			// this isn't really used
			"ScrollBarSliderFgColor"	"ControlBG"			// handle with which the slider is grabbed
			"ScrollBarSliderBgColor"	"ControlDarkBG"		// area behind handle
			"ButtonFgColor"				"DimBaseText"		// color of arrows
		}

		// text edit windows
		"WindowFgColor"					"BaseText"		// off-white
		"WindowBgColor"					"WindowBG"		// redundant. can we get rid of WindowBgColor and just use WindowBG?
		"WindowDisabledFgColor"			"DimBaseText"
		"WindowDisabledBgColor"			"ListBG"		// background of chat conversation
		"SelectionFgColor"				"SelectedText"		// fg color of selected text
		"SelectionBgColor"				"SelectionBG"
		"ListSelectionFgColor"			"SelectedText"
		"ListBgColor"					"ListBG"		// background of server browser control, etc
		"BuddyListBgColor"				"ListBG"		// background of buddy list pane
		
		// App-specific stuff
		"ChatBgColor"					"WindowBG"

		// status selection
		"StatusSelectFgColor"			"BrightBaseText"
		"StatusSelectFgColor2"			"BrightControlText"	// this is the color of the friends status

		// checkboxes
		"CheckButtonBorder1"   			"BorderDark"		// the left checkbutton border
		"CheckButtonBorder2"   			"BorderBright"		// the right checkbutton border
		"CheckButtonCheck"				"BrightControlText"	// color of the check itself
		"CheckBgColor"					"ListBG"

		// buttons (default fg/bg colors are used if these are not set)
		// "ButtonArmedFgColor"
		// "ButtonArmedBgColor"
		// "ButtonDepressedFgColor"		"BrightControlText"
		// "ButtonDepressedBgColor"

		// buddy buttons
		BuddyButton
		{
			"FgColor1"					"ControlText"
			"FgColor2"					"DimListText"
			"ArmedFgColor1"				"BrightBaseText"
			"ArmedFgColor2"				"BrightBaseText"
			"ArmedBgColor"				"SelectionBG"
		}
		Chat
		{
			"TextColor"					"BrightControlText"
			"SelfTextColor"				"BaseText"
			"SeperatorTextColor"		"DimBaseText"
		}
		InGameDesktop
		{
			"MenuColor"					"230 217 190 255" //188 188 188 155
			"ArmedMenuColor"			"230 217 190 200" //188 188 188 155
			"DepressedMenuColor"		"230 217 190 255"
			"WidescreenBarColor"		"0 0 0 0"
			"MenuItemVisibilityRate"	"0.01" // time it takes for one menu item to appear
			"MenuItemHeight"			"24" //24
			"GameMenuInset"				"32"
		}

		"SectionTextColor"				"BrightControlText"		// text color for IN-GAME, ONLINE, OFFLINE sections of buddy list
		"SectionDividerColor"			"BorderDark"			// color of line that runs under section name in buddy list
	}

	// describes all the fonts
	Fonts
	{
		// fonts are used in order that they are listed
		// fonts listed later in the order will only be used if they fulfill a range not already filled
		// if a font fails to load then the subsequent fonts will replace
		"Default"
		{
			"1"
			{
				"name"			"Tahoma"
				"tall"			"16"
				"weight"		"0"
				"dropshadow"	"1"
			}
		}
		"DefaultBold"
		{
			"1"
			{
				"name"			"Tahoma"
				"tall"			"16"
				"weight"		"500"
			}
		}
		"DefaultUnderline"
		{
			"1"
			{
				"name"			"Tahoma"
				"tall"			"16"
				"weight"		"0"
				"underline" 	"1"
			}
		}
		"DefaultSmall"
		{
			"1"
			{
				"name"			"Tahoma"
				"tall"			"13"
				"weight"		"0"
			}
		}
		"DefaultSmallBold"
		{
			"1"
			{
				"name"			"Tahoma"
				"tall"			"12"
				"weight"		"600"
			}
		}
		"DefaultVerySmall"
		{
			"1"
			{
				"name"			"Tahoma"
				"tall"			"12"
				"weight"		"0"
			}
		}
		"MenuLarge"
		{
			"1"
			{
				"name"			"Tahoma"
				"tall"			"13" //18
				"weight"		"0"
				"antialias"		"0"
				"dropshadow"	"0"
			}
		}
		"UiHeadline"
		{
			"1"
			{
				"name"			"Verdana"
				"tall"			"16"
				"weight"		"1000"
				"antialias"		"0"
			}
		}
		"Marlett"
		{
			"1"
			{
				"name"			"Marlett"
				"tall"			"14"
				"weight"		"0"
				"symbol"		"1"
			}
		}
		"EngineFont"
		{
			"1"
			{
				"name"			"Tahoma" //Verdana
				"tall"			"12"
				"weight"		"600"
				"yres"			"480 599"
				"dropshadow"	"1"
			}
			"2"
			{
				"name"			"Tahoma"
				"tall"			"13"
				"weight"		"600"
				"yres"			"600 767"
				"dropshadow"	"1"
			}
			"3" // chat, kill msg, status text
			{
				"name"			"Tahoma"
				"tall"			"17" //"14"
				"weight"		"600" //"600"
				"yres"			"768 1023"
				"dropshadow"	"1"
			}
			"4"
			{
				"name"			"Tahoma"
				"tall"			"20"
				"weight"		"600"
				"yres"			"1024 1199"
				"dropshadow"	"1"
			}
			"5"
			{
				"name"			"Tahoma"
				"tall"			"24"
				"weight"		"600"
				"yres"			"1200 6000"
				"dropshadow"	"1"
			}
		}	
		
		"CreditsFont"
		{
			"1"
			{
				"name"			"Trebuchet MS"
				"tall"			"18"
				"weight"		"600"
				"antialias"		"1"
			}
		}

		"Legacy_CreditsFont" // Added to accomodate 3rd party server plugins, etc. This version should not scale.
		{
			"1"
			{
				"name"			"Trebuchet MS"
				"tall"			"20"
				"weight"		"700"
				"antialias"		"1"
				"yres"			"1 10000"
			}
		}
	}

	// describes all the border types
	Borders
	{
		// references to other borders
		BaseBorder			"InsetBorder"
		ComboBoxBorder		"InsetBorder"
		BrowserBorder		"InsetBorder"
		ButtonBorder		"RaisedBorder"
		FrameBorder			"RaisedBorder"
		TabBorder			"RaisedBorder"
		MenuBorder			"RaisedBorder"
		
		// standard borders
		InsetBorder
		{
			"inset" "0 0 1 1"
			Left
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 1"
				}
			}

			Right
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "1 0"
				}
			}

			Top
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 0"
				}
			}

			Bottom
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "0 0"
				}
			}
		}

		RaisedBorder
		{
			"inset" "0 0 1 1"
			Left
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "0 1"
				}
			}

			Right
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 0"
				}
			}

			Top
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "0 1"
				}
			}

			Bottom
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 0"
				}
			}
		}

		// special border types
		TitleButtonBorder
		{
			"inset" "0 0 1 1"
			Left
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "0 1"
				}
			}

			Right
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "1 0"
				}
			}

			Top
			{
				"4"
				{
					"color" "BorderBright"
					"offset" "0 0"
				}
			}

			Bottom
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 0"
				}
			}
		}

		TitleButtonDisabledBorder
		{
			"inset" "0 0 1 1"
			Left
			{
				"1"
				{
					"color" "BgColor"
					"offset" "0 1"
				}
			}

			Right
			{
				"1"
				{
					"color" "BgColor"
					"offset" "1 0"
				}
			}
			Top
			{
				"1"
				{
					"color" "BgColor"
					"offset" "0 0"
				}
			}

			Bottom
			{
				"1"
				{
					"color" "BgColor"
					"offset" "0 0"
				}
			}
		}

		TitleButtonDepressedBorder
		{
			"inset" "1 1 1 1"
			Left
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 1"
				}
			}

			Right
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "1 0"
				}
			}

			Top
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 0"
				}
			}

			Bottom
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "0 0"
				}
			}
		}

		ScrollBarButtonBorder
		{
			"inset" "1 0 0 0"
			Left
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "0 1"
				}
			}

			Right
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "1 0"
				}
			}

			Top
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "0 0"
				}
			}

			Bottom
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 0"
				}
			}
		}

		ScrollBarButtonDepressedBorder
		{
			"inset" "2 2 0 0"
			Left
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 1"
				}
			}

			Right
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "1 0"
				}
			}

			Top
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 0"
				}
			}

			Bottom
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "0 0"
				}
			}
		}
		
		TabActiveBorder
		{
			"inset" "0 0 1 0"
			Left
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "0 0"
				}
			}

			Right
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "1 0"
				}
			}

			Top
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "0 0"
				}
			}

			Bottom
			{
				"1"
				{
					"color" "ControlBG"
					"offset" "6 2"
				}
			}
		}


		ToolTipBorder
		{
			"inset" "0 0 1 0"
			Left
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 0"
				}
			}

			Right
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "1 0"
				}
			}

			Top
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 0"
				}
			}

			Bottom
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 0"
				}
			}
		}

		// this is the border used for default buttons (the button that gets pressed when you hit enter)
		ButtonKeyFocusBorder
		{
			"inset" "0 0 1 1"
			Left
			{
				"1"
				{
					"color" "BorderSelection"
					"offset" "0 0"
				}
				"2"
				{
					"color" "BorderBright"
					"offset" "0 1"
				}
			}
			Top
			{
				"1"
				{
					"color" "BorderSelection"
					"offset" "0 0"
				}
				"2"
				{
					"color" "BorderBright"
					"offset" "1 0"
				}
			}
			Right
			{
				"1"
				{
					"color" "BorderSelection"
					"offset" "0 0"
				}
				"2"
				{
					"color" "BorderDark"
					"offset" "1 0"
				}
			}
			Bottom
			{
				"1"
				{
					"color" "BorderSelection"
					"offset" "0 0"
				}
				"2"
				{
					"color" "BorderDark"
					"offset" "1 1"
				}
			}
		}

		ButtonDepressedBorder
		{
			"inset" "2 1 1 1"
			Left
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 1"
				}
			}

			Right
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "1 0"
				}
			}

			Top
			{
				"1"
				{
					"color" "BorderDark"
					"offset" "0 0"
				}
			}

			Bottom
			{
				"1"
				{
					"color" "BorderBright"
					"offset" "0 0"
				}
			}
		}
	}
	
	CustomFontFiles
	{
		"1"		"resource/csp-font.ttf"
		"2"		"resource/bebaneue-regular.ttf"
		"3"		"resource/Lineage2Font.ttf"
	}
}
// 
// Line-Strike GUI by Geekrainian.
// (c) 2009-2014
// https://cuzus.games
// 
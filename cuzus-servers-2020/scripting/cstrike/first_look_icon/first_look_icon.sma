#include <amxmodx>
#include <amxmisc>
#include <engine>
#include <fakemeta>

#define SPR_RED_CROSS_ICON		"sprites/lineage2/redcross.spr"
#define SPR_BLUE_SHIELD_ICON	"sprites/lineage2/blueshield.spr"

#define ent_remove(%1) entity_set_int(%1, EV_INT_flags, entity_get_int(%1, EV_INT_flags) | FL_KILLME)

#define TASKID_EFFECT_REMOVE 100

public plugin_init()
{
	register_plugin("First Look Icon", "2019.03.12", "Geekrainian @ cuzus.games");

	register_event("StatusValue", "status_value_show", "be", "1=2", "2!0");
	register_event("StatusValue", "status_value_hide", "be", "1=1", "2=0");
}

public plugin_precache()
{
	precache_model(SPR_RED_CROSS_ICON);
	// precache_model(SPR_BLUE_SHIELD_ICON);
}

public status_value_show(id)
{
	new target = read_data(2);
	new Float:origin[3];

	pev(target, pev_origin, origin);

	origin[2] += 73.0;

	// TODO: team check
	// if (getTeam(id) == getTeam(target))
	// 	create_effect(target, SPR_BLUE_SHIELD_ICON, 0.25, 100.0, 1.0, 2.0);
	// else
	create_effect(target, SPR_RED_CROSS_ICON, 0.25, 100.0, 1.0, 2.0);

	// return PLUGIN_CONTINUE;
}

public status_value_hide(id){}

public create_effect(const player, const effect[], Float:size, Float:opacity, Float:framerate, const Float:time)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "env_sprite"));

	if (!ent)
	{
		return;
	}

	set_pev(ent, pev_classname, "dummy");
	engfunc(EngFunc_SetModel, ent, effect);
	engfunc(EngFunc_SetSize, ent, Float: { 0.0, 0.0, 0.0 }, Float: { 0.0, 0.0, 0.0 });
	// engfunc(EngFunc_SetOrigin, ent, origin); // not for pev_aiment
	set_pev(ent, pev_scale, size);
	set_pev(ent, pev_solid, SOLID_NOT);
	set_pev(ent, pev_movetype, MOVETYPE_FOLLOW);
	set_pev(ent, pev_aiment, player);
	set_pev(ent, pev_rendermode, 5); // 5 - Additive
	set_pev(ent, pev_renderfx, kRenderFxNone);
	set_pev(ent, pev_renderamt, opacity);
	set_pev(ent, pev_animtime, 1.0);
	set_pev(ent, pev_frame, 0);
	set_pev(ent, pev_framerate, framerate);
	set_pev(ent, pev_spawnflags, SF_SPRITE_STARTON);
	dllfunc(DLLFunc_Spawn, ent);

	new params[2];
	params[0] = player;
	params[1] = ent;

	set_task(time, "remove_effect", ent + TASKID_EFFECT_REMOVE, params, 2);
}

public remove_effect(params[2], taskid) // params[0] = player, params[1] = effect
{
	if (pev_valid(params[1]))
	{
		ent_remove(params[1]);
	}
}

#include <amxmodx>

new const CRITICAL_HIT[][] =
{
	"lineage2/skillsound/critical_hit_02.wav",
	"lineage2/skillsound/critical_hit.wav",
	"lineage2/skillsound/shield_stun_shot.wav"
}

public plugin_init()
{
	register_plugin("Headshot Sounds", "2019.02.22", "Geekrainian @ cuzus.games");

	register_event("DeathMsg", "DeathMsg_event", "a", "1>0", "3=1");
}

public plugin_precache()
{
	for (new i; i < sizeof CRITICAL_HIT; i++)
	{
		precache_sound(CRITICAL_HIT[i]);
	}
}

public DeathMsg_event()
{
	new attacker = read_data(1);
	new victim = read_data(2);

	if (attacker == victim)
	{
		return;
	}

	emit_sound(victim, CHAN_VOICE, CRITICAL_HIT[random_num(0, sizeof CRITICAL_HIT - 1)], VOL_NORM, ATTN_STATIC, 0, PITCH_NORM);
}


// Как убрать сообщения Terrorists Win и Counter-Terrorists Win
// register_message(get_user_msgid("TextMsg"), "message_textmsg");
// public message_textmsg() 
// {
// 	static textmsg[22]
// 	get_msg_arg_string(2, textmsg, charsmax(textmsg))
// 	if (equal(textmsg, "#Round_Draw") || equal(textmsg, "#Terrorists_Win") || equal(textmsg, "#CTs_Win"))
// 	{
// 		return PLUGIN_HANDLED;
// 	}
// 	return PLUGIN_CONTINUE;
// }

#include <amxmodx>

new Float:gRadio[33]
new pTime, pBlock

new szRadioCommands[][] = 
{
	"radio1", "coverme", "takepoint", "holdpos", "regroup", "followme", "takingfire", 
	"radio2", "go", "fallback", "sticktog", "getinpos", "stormfront", "report", 
	"radio3", "roger", "enemyspot", "needbackup", "sectorclear", "inposition", 
	"reportingin", "getout", "negative", "enemydown"
}

public plugin_init()
{
	register_plugin("No Radio Flood", "1.1", "Starsailor")
	
	for(new i=0; i<sizeof szRadioCommands; i++){
		register_clcmd(szRadioCommands[i], "cmdRadio")
	}
	
	pTime = register_cvar("nrf_time", "30")  //0 Disabled
	pBlock = register_cvar("nrf_block_fith", "1")
	
	register_message(get_user_msgid("SendAudio"), "FireInTheHole")
	register_message(get_user_msgid("TextMsg"), "MsgTextMsg");
}

public MsgTextMsg()
{
	if(get_msg_args() != 5 || get_msg_arg_int(1) != 5)
		return PLUGIN_CONTINUE;

	new szMsg[13];
	get_msg_arg_string(3, szMsg, 12);

	if(equal(szMsg, "#Game_radio"))
		return PLUGIN_HANDLED;

	return PLUGIN_CONTINUE;
}

public cmdRadio(id)
{
	if(!is_user_alive(id)){
		return PLUGIN_HANDLED_MAIN
	}
	
	new iTime = get_pcvar_num(pTime)
	
	if (iTime > 0)
	{
		new Float:fTime = get_gametime()
		
		if (fTime - gRadio[id] < iTime)
		{
			//client_print(id,print_center, "Sorry, but you can't abuse this command!")
			return PLUGIN_HANDLED_MAIN
		}
		gRadio[id] = fTime
	}

	return PLUGIN_CONTINUE
}

public FireInTheHole(msgid,msg_dest,msg_entity)
{
	if (get_msg_args() < 3 || get_msg_argtype(2) != ARG_STRING)
	{
		return PLUGIN_HANDLED
	}
	
	new szArg[32]
	
	get_msg_arg_string(2,szArg,31)
	
	if (equal(szArg ,"%!MRAD_FIREINHOLE") && get_pcvar_num(pBlock))
	{
		return PLUGIN_HANDLED;
	}
	
	return PLUGIN_CONTINUE;
}
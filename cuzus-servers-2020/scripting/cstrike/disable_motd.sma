#include <amxmodx>
#include <amxmisc>

new bool:saw[33]

public plugin_init() 
{
	register_plugin("Disable startup MOTD", "1.0", "Sn!ff3r")
	register_message(get_user_msgid("MOTD"), "message_MOTD")
}

public client_connect(id)
{
	saw[id] = false
}

public message_MOTD(const MsgId, const MsgDest, const MsgEntity)
{
	if(!saw[MsgEntity])
	{
		if(get_msg_arg_int(1) == 1)
		{
			saw[MsgEntity] = true
			return PLUGIN_HANDLED
		}
	}
	return PLUGIN_CONTINUE
}
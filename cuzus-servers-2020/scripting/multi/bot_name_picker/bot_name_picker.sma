#include <amxmodx>
#include <amxmisc>
#include <hamsandwich>
#include <fun>
#include <fakemeta>

new const NAMES_FILE[] = "botnames.txt";

new Array:PlayerNames;

public plugin_init()
{
	register_plugin("Bot Name Picker", "2020.01.03", "Geekrainian @ cuzus.games");

	PlayerNames = ArrayCreate(32);
}

public plugin_end()
{
	ArrayDestroy(PlayerNames);
}

public plugin_cfg()
{
	new dir[64], file[64];
	get_configsdir(dir, sizeof dir - 1);
	formatex(file, sizeof file - 1, "%s/%s", dir, NAMES_FILE);
	new count;

	if (file_exists(file))
	{
		new szFile = fopen( file, "rt" ), Buffer[512];

		while (!feof(szFile))
		{
			fgets(szFile, Buffer, sizeof Buffer - 1);

			if (!Buffer[0] || Buffer[0] == ';' || strlen(Buffer) < 3)
				continue;

			trim(Buffer);

			ArrayPushString(PlayerNames, Buffer);

			count++;
		}

		log("Loaded %d bot names from file", count);

		fclose(szFile);
	}
}

public get_random_name()
{
	new dataSize = ArraySize(PlayerNames);
	new index = random(dataSize - 1);
	new output[32];

	ArrayGetString(PlayerNames, index, output, sizeof output - 1);

	return output;
}

public client_putinserver(id)
{
	if (!is_user_bot(id))
	{
		return;
	}

	new nameOld[32], nameNew[32];

	get_user_name(id, nameOld, 31);

	format(nameNew, 31, "%s", get_random_name());

	log("client_putinserver: Changing bot name %s -> %s", nameOld, nameNew);

	if (is_user_alive(id))
	{
		set_user_info(id, "name", nameNew);
	}
	else if (is_user_connected(id))
	{
		spawn(id);
		set_user_info(id, "name", nameNew);
		user_kill(id, 1);
	}
}

stock log(const str[], any:...)
{
	new msg[512];
	vformat(msg, 511, str, 2); // 2 - position of arg with "any:..."

	new tag[8] = "[DEBUG]";
	format(msg, 511, "%s %s", tag, msg);

	new hh, mm, ss;
	time(hh,mm,ss);
	format(msg, 511, "%02d:%02d:%02d %s", hh, mm, ss, msg);

	server_print(msg);
}

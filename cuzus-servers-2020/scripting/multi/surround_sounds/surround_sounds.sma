#include <amxmodx>

new const S_AMBIENCE[][] =
{
	"lineage2/skillsound/bind_will_shot.wav",
	"lineage2/skillsound/bodytomind_shot.wav",
	"lineage2/skillsound/death_whisper_shot.wav",
	"lineage2/skillsound/might_shot.wav",
	"lineage2/skillsound/wind_shackle_shot.wav",
	"lineage2/skillsound/soul_shot_cast.wav",
	"lineage2/skillsound/spirits_shot_cast.wav",
	"lineage2/skillsound/song_of_full.wav",

	"lineage2/ambsound/ct_bell_01.wav",
	//"lineage2/ambsound/d_drone_02.wav",
	//"lineage2/ambsound/ds_wind_01.wav",
	"lineage2/ambsound/dv_pipe_01.wav",
	"lineage2/ambsound/ed_chimes_05.wav",
	"lineage2/ambsound/id_cicada_01.wav",
	"lineage2/ambsound/id_cicada_02.wav",
	"lineage2/ambsound/in_cricket_03.wav",
	"lineage2/ambsound/ms_bell_01.wav",
	"lineage2/ambsound/nz_pipe_02.wav",
	//"lineage2/ambsound/st_horror_01.wav",
	"lineage2/ambsound/an_bird_11.wav",
	"lineage2/ambsound/an_crow_01.wav",
	"lineage2/ambsound/an_crow_02.wav",
	"lineage2/ambsound/id_cicada_04.wav",
	"lineage2/ambsound/nz_cricket_02.wav",
	"lineage2/ambsound/nz_cricket_03.wav",
	"lineage2/ambsound/t_black_smith_loop_01.wav",

	"lineage2/monsound/queencat_move_01.wav",
	"lineage2/monsound/queencat_move_03.wav",
	"lineage2/monsound/cutie_cat_wait.wav",
	"lineage2/monsound/cutie_cat_death.wav",
	"lineage2/monsound/cutie_cat_fall.wav",
	"lineage2/monsound/dwarf_ghost_dmg_3.wav",
	"lineage2/monsound/fox_Wait.wav",
	"lineage2/monsound/hatchling_atkwait.wav",
	"lineage2/monsound/pet_wolf_wait.wav",
	"lineage2/monsound/strider_Wait.wav",
	"lineage2/monsound/vamp_bat_wait.wav",
	"lineage2/monsound/vamp_bat_atkwait.wav",

	"lineage2/chrsound/NPC_old_cough_01.wav"
};

public plugin_precache()
{
	for (new i; i < sizeof S_AMBIENCE; i++)
	{
		precache_sound(S_AMBIENCE[i]);
	}
}

public plugin_init()
{
	register_plugin("Surround Sounds", "2019.02.18", "Geekrainian @ cuzus.games");

	register_cvar("ss_random_sound_chance", "70");
	register_cvar("ss_death_sound_chance", "5");
	register_cvar("ss_volume_min", "0.21");
	register_cvar("ss_volume_max", "0.56");

	register_event("DeathMsg", "DeathMsg_event", "a");

	set_task(60.0, "scheduler");
}

public DeathMsg_event()
{
	if (random_num(0, 100) <= get_cvar_num("ss_death_sound_chance"))
	{
		set_task(random_float(3.0, 10.0), "make_sound");
	}
}

public scheduler()
{
	if (random_num(0, 100) <= get_cvar_num("ss_random_sound_chance"))
	{
		make_sound();
	}

	set_task(random_float(3.0, 45.0), "scheduler");
}

public make_sound()
{
	emit_sound(0, CHAN_AUTO, S_AMBIENCE[random_num(0, sizeof S_AMBIENCE - 1)], random_float(get_cvar_float("ss_volume_min"), get_cvar_float("ss_volume_max")), ATTN_NORM, 0, PITCH_NORM);
}

#include <amxmodx>
#include <amxmisc>
#include <engine>
#include <fakemeta>

#define entity_remove(%1) entity_set_int(%1, EV_INT_flags, entity_get_int(%1, EV_INT_flags) | FL_KILLME)

#define XX 33

#define M_L2ACCESSORY "models/lineage2/LineageAccessory.mdl"

#define HAT_ENTITY_NAME "hat_ent"

enum (+= 33)
{
    TASK_ATTACH_ACCESSORY = 1
};

enum _:ACCESSORIES
{
	H_LITTLE_ANGEL_WINGS = 0,		// 0
	H_ARTISAN_GOGGLES,				// 1
	H_CAT_EARS,						// 2
	H_LORD_CROWN,					// 3
	H_DEMON_CIRCLET,				// 4
	H_DEMON_HORNS,					// 5
	H_FAIRY_ANTENNA,				// 6
	H_FEATHERED_HAT,				// 7
	H_HERO_CIRCLET,					// 8
	H_CIRCLET_OF_ICE_FAIRY_SIRRA,	// 9
	H_JESTER_HAT,					// 10
	H_NOBLESSE_TIARA,				// 11
	H_PARTY_HAT,					// 12
	H_PARTY_MASK,					// 13
	H_RED_PARTY_MASK,				// 14
	H_PIRATE_HAT,					// 15
	H_RABBIT_EARS,					// 16
	H_SANTAS_HAT,					// 17
	H_VALAKAS_SLAYER_CIRCLET,		// 18
	H_WIZARD_HAT					// 19
};

new EntPlayerHat[XX];

public plugin_precache()
{
	precache_model(M_L2ACCESSORY);
}

public plugin_init()
{
	register_plugin("Yet Another Hats (Lineage 2 Accessories)", "2019.03.13", "Geekrainian @ cuzus.games");
}

public client_putinserver(id)
{
	set_task(1.0, "attach_hat", id + TASK_ATTACH_ACCESSORY);
}

public client_disconnect(id)
{
	remove_hat(id);
}

public create_hat(id)
{
	EntPlayerHat[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));

	if (EntPlayerHat[id] > 0)
	{
		new submodel = random_num(0, ACCESSORIES - 1);

		set_pev(EntPlayerHat[id], pev_classname, HAT_ENTITY_NAME);
		engfunc(EngFunc_SetModel, EntPlayerHat[id], M_L2ACCESSORY);
		set_pev(EntPlayerHat[id], pev_body, submodel);
		set_pev(EntPlayerHat[id], pev_movetype, MOVETYPE_FOLLOW);
		set_pev(EntPlayerHat[id], pev_aiment, id);
		set_pev(EntPlayerHat[id], pev_owner, id);
		set_pev(EntPlayerHat[id], pev_rendermode, kRenderNormal);
	}
}

public remove_hat(id)
{
	if (has_hat(id))
	{
		entity_remove(EntPlayerHat[id]);
		EntPlayerHat[id] = 0;
	}
}

public attach_hat(id)
{
	id -= TASK_ATTACH_ACCESSORY;

	remove_hat(id);
	create_hat(id);
}

public bool:has_hat(id)
{
	return EntPlayerHat[id] > 0 && pev_valid(EntPlayerHat[id]);
}

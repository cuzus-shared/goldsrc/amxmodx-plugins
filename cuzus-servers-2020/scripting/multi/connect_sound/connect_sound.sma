#include <amxmodx>
#include <amxmisc>

#define TASKID_STOP_MP3 100
#define TASKID_REDUCE_VOLUME 200
#define BASIC_VOLUME 0.75
#define REDUCE_VOLUME_RATE 0.05

new const MUSIC[][] =
{
	"lineage2/music/ct_darkelf.mp3",
	"lineage2/music/ct_dwarf.mp3",
	"lineage2/music/ct_elf.mp3",
	"lineage2/music/ct_human.mp3",
	"lineage2/music/ct_orc.mp3",
	"lineage2/music/intro.mp3",
	"lineage2/music/ls01_f.mp3",
	"lineage2/music/ns04_f.mp3",
	"lineage2/music/ns13_f.mp3",
	"lineage2/music/nt_aden.mp3",
	"lineage2/music/nt_dion.mp3",
	"lineage2/music/nt_giran.mp3",
	"lineage2/music/nt_gludin.mp3",
	"lineage2/music/nt_gludio.mp3",
	"lineage2/music/nt_heiness.mp3",
	"lineage2/music/nt_oren.mp3"
};

new MusicFile[64];
new Float:Volume[33];

public plugin_precache()
{
	formatex(MusicFile, charsmax(MusicFile), MUSIC[random_num(0, sizeof MUSIC - 1)]);

	server_print("Connect sound will be %s", MusicFile);

	precache_generic(MusicFile); // precache_sound cannot precache mp3
}

public plugin_init()
{
	register_plugin("Connect Sound", "2020.07.03", "Geekrainian @ cuzus.games");
}

public client_connect(id)
{
	Volume[id] = BASIC_VOLUME;

	// TODO: save and restore client volume

	client_cmd(id, "MP3Volume %f", Volume[id]);
	client_cmd(id, "mp3 play sound/%s", MusicFile);
}

public client_putinserver(id)
{
	if (!is_user_bot(id))
	{
		set_task(5.0, "reduce_volume", id + TASKID_REDUCE_VOLUME);
	}
}

public stop_mp3(id)
{
	id -= TASKID_STOP_MP3;

	client_cmd(id, "mp3 stop");

	Volume[id] = BASIC_VOLUME;
	client_cmd(id, "MP3Volume %f", Volume[id]);
}

public reduce_volume(id)
{
	id -= TASKID_REDUCE_VOLUME;

	new bool:triggerStop;

	if (Volume[id] > REDUCE_VOLUME_RATE)
	{
		Volume[id] -= REDUCE_VOLUME_RATE;
	}
	else if (Volume[id] <= REDUCE_VOLUME_RATE)
	{
		triggerStop = true;
	}

	if (triggerStop)
	{
		set_task(0.1, "stop_mp3", id + TASKID_STOP_MP3);
	}
	else
	{
		client_cmd(id, "MP3Volume %f", Volume[id]);
		set_task(0.3, "reduce_volume", id + TASKID_REDUCE_VOLUME);
	}
}

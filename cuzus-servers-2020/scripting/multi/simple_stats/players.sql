DROP TABLE IF EXISTS `players`;

CREATE TABLE `players` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `steam_id` varchar(128) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `damage` bigint(20) unsigned NOT NULL DEFAULT '0',
  `headshots` bigint(20) unsigned NOT NULL DEFAULT '0',
  `kills_knife` int(11) unsigned NOT NULL DEFAULT '0',
  `kills_weapon` int(11) unsigned NOT NULL DEFAULT '0',
  `assists` int(11) unsigned NOT NULL DEFAULT '0',
  `deaths` int(11) unsigned NOT NULL DEFAULT '0',
  `wins` int(11) unsigned NOT NULL DEFAULT '0',
  `losts` int(11) unsigned NOT NULL DEFAULT '0',
  `exp` bigint(20) unsigned NOT NULL DEFAULT '0',
  `online_time` bigint(20) unsigned NOT NULL DEFAULT '0',
  `maps_played` int(11) unsigned NOT NULL DEFAULT '0',
  `last_map` varchar(64) DEFAULT NULL,
  `reg_ip` varchar(16) DEFAULT NULL,
  `last_ip` varchar(16) DEFAULT NULL,
  `first_visit` varchar(32) DEFAULT NULL,
  `last_visit` varchar(32) DEFAULT NULL,
  `first_enter` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `inactive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

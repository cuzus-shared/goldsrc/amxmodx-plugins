#include <amxmodx>
#include <amxmisc>
#include <engine>
#include <hamsandwich>
#include <fakemeta>
#include <sqlx>

#define FFADE_IN		0x0000	// Just here so we don't pass 0 into the function
#define FFADE_OUT		0x0001	// Fade out (not in)
#define FFADE_MODULATE	0x0002	// Modulate (don't blend)
#define FFADE_STAYOUT	0x0004	// ignores the duration, stays faded out until new ScreenFade message received

#define entity_remove(%1) entity_set_int(%1, EV_INT_flags, entity_get_int(%1, EV_INT_flags) | FL_KILLME)

#define PLUGIN_NAME "Simple Stats"

#define HLW_CROWBAR 1
#define CSW_KNIFE 29

#define XX 33

#define S_LEVEL_UP "lineage2/skillsound/levelup.wav"
#define S_SIEGE_START "lineage2/itemsound/siege_start.wav"
#define S_SIEGE_END "lineage2/itemsound/siege_end.wav"

#define SPR_HEROGLOW "sprites/lineage2/dt_coronabulb_128.spr"

enum (+= 33)
{
	TASKID_STATUS_TEXT = 100,
	TASKID_LOAD_PLAYER_DATA,
	TASKID_STORE_PLAYER_DATA,
	TASKID_UNIQUE_PLAYER_CHECK
};

enum _:EPlayerData
{
	DAMAGE,
	HEADSHOTS,
	KILLS_KNIFE,
	KILLS_WEAPON,
	ASSISTS,
	DEATHS,
	EXP,
	ONLINE_TIME,
	MAPS_PLAYED
};

enum _:EMapData
{
	DAMAGE,
	KILLS_KNIFE,
	KILLS_WEAPON,
	ASSISTS,
	DEATHS,
	PLAY_ON_MAP
};

new PlayerData[XX][EPlayerData];
new MapData[XX][EMapData];
new Array:UniquePlayers;
new AssistDamage[XX][XX];

new HeroGlowEntity[XX];

new ModName[32];
new BestPlayerAuthId[32];

new Handle:MysqlDbTuple;
new Handle:MysqlConnect;
new MysqlDbTable[32];

new MsgidStatusText;
new MsgidScreenFade;

new SPR_SHOCKWAVE;

public plugin_precache()
{
	SPR_SHOCKWAVE = precache_model("sprites/misc/shockwave.spr");

	precache_model(SPR_HEROGLOW);

	precache_sound(S_LEVEL_UP);
	//precache_sound(S_SIEGE_START);
	precache_sound(S_SIEGE_END);
}

public plugin_init()
{
	register_plugin(PLUGIN_NAME, "2019.03.12", "Geekrainian @ cuzus.games");

	register_cvar("ss_enable_map_end_motd", "0"); // NOTE: Highly recommended to disable for Half-Life due to causeless server shutdown
	register_cvar("ss_play_on_map_min_time", "15"); // in minutes
	register_cvar("ss_exp_kill_multiplier", "1"); // formula: reward exp = ss_exp_kill_multiplier * player level
	register_cvar("ss_exp_gain_rate", "1");
	register_cvar("ss_exp_gain_multiplier", "1");
	register_cvar("ss_exp_lost_rate", "1");
	register_cvar("ss_max_level", "255");
	register_cvar("ss_assist_min_damage", "33");
	register_cvar("ss_assist_exp_multiplier", "0.33");

	register_cvar("ss_mysql_host", "127.0.0.1");
	register_cvar("ss_mysql_user", "root");
	register_cvar("ss_mysql_pass", "root");
	register_cvar("ss_mysql_db", "halflife");
	register_cvar("ss_mysql_table", "players");

	RegisterHam(Ham_TakeDamage, "player", "ham_take_damage");
	RegisterHam(Ham_Spawn, "player", "ham_spawn");

	register_event("DeathMsg", "deathmsg_event", "a"); // "a" - Global Event

	register_event("30", "map_end_event", "a");
	//register_message(SVC_INTERMISSION, "map_end_event");

	MsgidStatusText = get_user_msgid("StatusText");

	UniquePlayers = ArrayCreate(512);

	get_modname(ModName, charsmax(ModName));
}

public plugin_cfg()
{
	set_task(1.0, "make_db_tuple");
	set_task(3.0, "status_text_update_task", TASKID_STATUS_TEXT, _ /*data*/, _ /*size of data*/, "b"); // "b" - loop indefinitely until timer is stopped
}

public plugin_end()
{
	ArrayDestroy(UniquePlayers);

	if (MysqlConnect != Empty_Handle)
	{
		SQL_FreeHandle(MysqlConnect);
	}

	if (MysqlDbTuple != Empty_Handle)
	{
		SQL_FreeHandle(MysqlDbTuple);
	}

	if (task_exists(TASKID_STATUS_TEXT))
	{
		remove_task(TASKID_STATUS_TEXT);
	}
}

public client_putinserver(id)
{
	reset_stats(id);
	reset_assist_damage(id);

	set_task(1.0, "load_player_data", id + TASKID_LOAD_PLAYER_DATA);

	status_text_clear(id);
	check_unique_player_exists(id);
	check_is_hero(id);
}

public client_disconnect(id)
{
	if (!is_user_bot(id))
	{
		set_task(0.1, "store_player_data", id + TASKID_STORE_PLAYER_DATA);
	}

	reset_stats(id);
	reset_assist_damage(id);
}

public check_is_hero(id)
{
	if (!strlen(BestPlayerAuthId))
	{
		return;
	}

	new authid[32];

	if (is_user_bot(id))
	{
		new username[32];
		get_user_name(id, username, charsmax(username));
		formatex(authid, charsmax(authid), "BOT:%s", username);
	}
	else
	{
		get_user_authid(id, authid, charsmax(authid));
	}

	if (equal(authid, BestPlayerAuthId))
	{
		log("Hero player connected: %s", authid);
		set_hero(id);
	}
}

public set_hero(id)
{
	if (HeroGlowEntity[id])
	{
		remove_hero(id);
	}
	else
	{
		HeroGlowEntity[id] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));

		set_pev(HeroGlowEntity[id], pev_classname, "hero_glow");

		engfunc(EngFunc_SetModel, HeroGlowEntity[id], SPR_HEROGLOW);
		engfunc(EngFunc_SetSize, HeroGlowEntity[id], Float:{0.0,0.0,0.0}, Float:{0.0,0.0,0.0});

		set_pev(HeroGlowEntity[id], pev_solid, SOLID_NOT);
		set_pev(HeroGlowEntity[id], pev_movetype, MOVETYPE_FOLLOW);
		set_pev(HeroGlowEntity[id], pev_aiment, id);
		set_pev(HeroGlowEntity[id], pev_rendermode, 5); // 5 - Additive
		set_pev(HeroGlowEntity[id], pev_renderamt, 60.0);
		set_pev(HeroGlowEntity[id], pev_renderfx, 16); // 16 - Hologram (Distort + fade)
		set_pev(HeroGlowEntity[id], pev_scale, 0.8);

		//set_pev(HeroGlowEntity[id], pev_rendercolor, 255,0,0);

		//set_task(0.1, "heroGlowTask", id + TASK_SAY2_HERO_GLOW, _, _, "b");
	}
}

/*
public heroGlowTask(id)
{
	id -= TASK_SAY2_HERO_GLOW;
	Create_TE_DLIGHT(id, 9, 155,155,100, 1, 5);
}
*/

public remove_hero(id)
{
	if (HeroGlowEntity[id])
	{
		entity_remove(HeroGlowEntity[id]);
		HeroGlowEntity[id] = 0;

		/*
		if(task_exists(id+TASK_SAY2_HERO_GLOW))
			remove_task(id+TASK_SAY2_HERO_GLOW);
		*/
	}
}

public check_unique_player_exists(id)
{
	new targetIp[16];

	if (is_user_bot(id))
	{
		formatex(targetIp, 15, "%d.%d.%d.%d", random_num(0, 255), random_num(0, 255), random_num(0, 255), random_num(0, 255));
	}
	else
	{
		get_user_ip(id, targetIp, 15, 1 /*without_port*/);
	}

	new bool:exist;

	for (new i; i < ArraySize(UniquePlayers); i++)
	{
		new storedIp[16];
		ArrayGetString(UniquePlayers, i, storedIp, sizeof(storedIp));

		if (equali(targetIp, storedIp))
		{
			exist = true;
			break;
		}
	}

	if (!exist)
	{
		ArrayPushString(UniquePlayers, targetIp);
	}
}

public reset_stats(id)
{
	PlayerData[id][DAMAGE] = 0;
	PlayerData[id][HEADSHOTS] = 0;
	PlayerData[id][KILLS_KNIFE] = 0;
	PlayerData[id][KILLS_WEAPON] = 0;
	PlayerData[id][ASSISTS] = 0;
	PlayerData[id][DEATHS] = 0;
	PlayerData[id][EXP] = 0;
	PlayerData[id][ONLINE_TIME] = 0;
	PlayerData[id][MAPS_PLAYED] = 0;

	MapData[id][DAMAGE] = 0;
	MapData[id][KILLS_KNIFE] = 0;
	MapData[id][KILLS_WEAPON] = 0;
	MapData[id][ASSISTS] = 0;
	MapData[id][DEATHS] = 0;
	MapData[id][PLAY_ON_MAP] = 0;
}

public reset_assist_damage(id)
{
	new const maxplayers = get_maxplayers();

	for (new i = 1; i <= maxplayers; i++)
	{
		AssistDamage[i][id] = 0;
		AssistDamage[id][i] = 0;
	}
}

public ham_spawn(id)
{
	reset_assist_damage(id);
}

public ham_take_damage(victim, inflictor, attacker, Float:damage, bits)
{
	if (attacker != victim) //is_user_connected(attacker) &&
	{
		PlayerData[attacker][DAMAGE] += floatround(damage);
		MapData[attacker][DAMAGE] += floatround(damage);

		if (victim)
		{
			AssistDamage[attacker][victim] += floatround(damage);
		}
	}
}

public deathmsg_event()
{
	new victim = read_data(2);

	PlayerData[victim][DEATHS]++;
	MapData[victim][DEATHS]++;

	new killer = read_data(1);

	if (!killer)
	{
		return;
	}

	if (killer == victim)
	{
		reduce_exp_check_level(victim, get_cvar_num("ss_exp_lost_rate"), "suicide");
	}
	else
	{
		// Check for weapon
		new weapon = get_user_weapon(killer);

		if (equali(ModName, "valve") && weapon == HLW_CROWBAR || equali(ModName, "cstrike") && weapon == CSW_KNIFE)
		{
			PlayerData[killer][KILLS_KNIFE]++;
			MapData[killer][KILLS_KNIFE]++;

			reduce_exp_check_level(victim, get_cvar_num("ss_exp_lost_rate"), "knife death");
		}
		else
		{
			PlayerData[killer][KILLS_WEAPON]++;
			MapData[killer][KILLS_WEAPON]++;
		}

		// Headshots is not applicable in Half-Life
		if (!equali(ModName, "valve") && read_data(3))
		{
			PlayerData[killer][HEADSHOTS]++;
		}

		// Set killer experience
		new expForKill = get_exp_for_killed_player(killer, victim);
		add_exp_check_level(killer, expForKill, "kill");

		// Calculate assists
		new playerIndex[XX - 1], playersCount, id;
		get_players(playerIndex, playersCount, "h"); // skip hltv

		for (new i; i < playersCount; i++)
		{
			id = playerIndex[i];

			if (id == killer)
			{
				continue;
			}

			if (AssistDamage[id][victim] >= get_cvar_num("ss_assist_min_damage"))
			{
				PlayerData[id][ASSISTS]++;
				MapData[id][ASSISTS]++;

				new rewardExp = floatround(expForKill * get_cvar_float("ss_assist_exp_multiplier"));
				add_exp_check_level(id, rewardExp, "assist");
			}
		}
	}
}

public add_exp_check_level(id, expAmount, event[])
{
	if (id < 1 || expAmount < 1)
	{
		return;
	}

	new bool:isLevelGained = is_level_gained(PlayerData[id][EXP], expAmount);

	if (isLevelGained)
	{
		effect_level_up(id);
	}

	PlayerData[id][EXP] += expAmount;

	if (isLevelGained)
	{
		new level = get_player_level_from_exp(PlayerData[id][EXP]);
		client_print(id, print_center, "Your level has increased to %d!", level);
	}
	else
	{
		client_print(id, print_center, "+%d exp %s", expAmount, event);
	}
}

public reduce_exp_check_level(id, expAmount, event[])
{
	if (id < 1 || expAmount < 1)
	{
		return;
	}

	if (PlayerData[id][EXP] >= expAmount)
	{
		PlayerData[id][EXP] -= expAmount;

		client_print(id, print_center, "-%d exp %s", expAmount, event);
	}
}

public screen_fade_out()
{
	new playerIndex[XX - 1], playersCount;
	get_players(playerIndex, playersCount, "ch"); // skip bots, hltv

	for (new i; i < playersCount; i++)
	{
		if (playerIndex[i] > 0)
			Create_ScreenFade(playerIndex[i], (1<<12)*4, (1<<12)*1, FFADE_OUT, 0, 0, 0, 255, true);
	}
}

public screen_full_black()
{
	new playerIndex[XX - 1], playersCount;
	get_players(playerIndex, playersCount, "ch"); // skip bots, hltv

	for (new i; i < playersCount; i++)
	{
		if (playerIndex[i] > 0)
			Create_ScreenFade(playerIndex[i], 1<<0, 1<<0, 1<<2, 0, 0, 0, 255, true);
	}
}

public map_end_event()
{
	set_task(0.1, "screen_fade_out");
	set_task(3.0, "screen_full_black");

	server_cmd("sv_alltalk 1");
	server_cmd("sv_gravity 100");

	client_print(0, print_center, "");

	emit_sound(0, CHAN_AUTO, S_SIEGE_END, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);

	// TODO: Clear status text for all
	// status_text_clear(id);

	if (get_cvar_num("ss_enable_map_end_motd"))
	{
		generate_motd();
	}

	store_all_players();
}

public get_best_player_id()
{
	new playerIndex[XX - 1], playersCount, id;
	get_players(playerIndex, playersCount, "h"); // skip hltv

	new maxKills, bestPlayerId;

	for (new i; i < playersCount; i++)
	{
		id = playerIndex[i];

		new pKills = MapData[id][KILLS_KNIFE] + MapData[id][KILLS_WEAPON];

		if (pKills > maxKills)
		{
			bestPlayerId = id;
			maxKills = pKills;
		}
	}

	return bestPlayerId;
}

public generate_motd()
{
	new bestPlayerId = get_best_player_id();
	new bestKills = MapData[bestPlayerId][KILLS_KNIFE] + MapData[bestPlayerId][KILLS_WEAPON];
	new bestDamage = MapData[bestPlayerId][DAMAGE];

	new motd[1024];

	new username[32];
	get_user_name(bestPlayerId, username, charsmax(username));

	format_motd(motd, charsmax(motd), username, bestDamage, bestKills);

	new mapName[64];
	get_mapname(mapName, charsmax(mapName));

	new title[128];
	formatex(title, charsmax(title), "Stats on map: %s", mapName);

	remove_scoreboard();

	display_motd(motd, title);
}

public display_motd(motd[], title[])
{
	new playerIndex[XX - 1], playersCount, i;
	get_players(playerIndex, playersCount, "ch"); // skip bots, hltv

	for (i = 0; i < playersCount; i++)
	{
		show_motd(playerIndex[i], motd, title);
	}
}

/**
 * StatusText
 */
public status_text_update_task()
{
	new playerIndex[XX - 1], playersCount, i, id;
	get_players(playerIndex, playersCount, "ch"); // skip bots, hltv

	for (i = 0; i < playersCount; i++)
	{
		id = playerIndex[i];

		if (is_user_alive(id))
		{
			new exp = PlayerData[id][EXP];

			static buf[128];
			formatex(buf, charsmax(buf), "Lvl %d  Exp %s", get_player_level_from_exp(exp), get_player_exp_string(exp));

			message_begin(MSG_ONE_UNRELIABLE, MsgidStatusText, _, id);
			write_byte(0);
			write_string(buf);
			message_end();
		}
	}
}

public status_text_clear(id)
{
	message_begin(MSG_ONE_UNRELIABLE, MsgidStatusText, _, id);
	write_byte(0);
	write_string(" ");
	message_end();
}

/**
 * Experience
 */
public get_exp_for_killed_player(attacker, victim)
{
	new killedLevel = get_player_level_from_exp(PlayerData[victim][EXP]);

	new baseVal = get_cvar_num("ss_exp_kill_multiplier");

	if (killedLevel == 0)
	{
		return baseVal;
	}

	return baseVal * killedLevel;
}

public get_player_level_from_exp(exp)
{
	for (new i; i <= get_cvar_num("ss_max_level"); i++)
	{
		new curLevelExp = get_exp_for_level(i);
		new nextLevelExp = get_exp_for_level(i + 1);

		if (exp >= curLevelExp && exp < nextLevelExp)
		{
			return i;
		}
	}

	return 0;
}

public get_exp_for_level(level)
{
	if (level == 0)
	{
		return 0;
	}

	new baseVal = get_cvar_num("ss_exp_gain_rate");
	new mult = get_cvar_num("ss_exp_gain_multiplier");

	if (level >= 1)
	{
		for (new i; i < level; i++)
		{
			mult += mult;
		}
	}

	return baseVal * mult;
}

public get_player_exp_string(kills)
{
	new curLevel = get_player_level_from_exp(kills);
	new nextLevelExp = get_exp_for_level(curLevel + 1);

	new expString[32];
	formatex(expString, charsmax(expString), "%d/%d", kills, nextLevelExp);

	return expString;
}

public bool:is_level_gained(curExp, expForKill)
{
	new curLevel = get_player_level_from_exp(curExp);
	new nextLevel = get_player_level_from_exp(curExp + expForKill);

	if (nextLevel > curLevel)
	{
		return true;
	}

	return false;
}

public effect_level_up(id)
{
	//id -= TASK_EFFECT_LEVEL_UP;

	new Float:origin[3], Float:axis[3];
	pev(id, pev_origin, origin);

	axis = origin;
	axis[0] += 180.0;
	axis[1] += 180.0;
	axis[2] += 30.0;

	new lifetime = 7;
	new width = 60;

	new R = 253;
	new G = 244;
	new B = 200;
	new A = 140;

	Create_TE_BEAMCYLINDER(origin, axis, SPR_SHOCKWAVE, 0, 0, lifetime, width, 0, R, G, B, A, 0);

	new user_origin[3];
	get_user_origin(id, user_origin);

	new radius = 20;
	Create_TE_DLIGHT(user_origin, radius, R, G, B, lifetime, 0);

	emit_sound(id, CHAN_VOICE, S_LEVEL_UP, random_float(0.75, 1.0), ATTN_NORM, 0, PITCH_NORM);
}

/**
 * SQL functions
 */
public make_db_tuple()
{
	new host[32], user[32], pass[32], db[32];
	get_cvar_string("ss_mysql_host", host, charsmax(host));
	get_cvar_string("ss_mysql_user", user, charsmax(user));
	get_cvar_string("ss_mysql_pass", pass, charsmax(pass));
	get_cvar_string("ss_mysql_db", db, charsmax(db));

	// This does not connect to the DB; it only caches the connection information.
	MysqlDbTuple = SQL_MakeDbTuple(host, user, pass, db);

	get_cvar_string("ss_mysql_table", MysqlDbTable, charsmax(MysqlDbTable));

	new errorString[512], errorCode;

	MysqlConnect = SQL_Connect(MysqlDbTuple, errorCode, errorString, charsmax(errorString))

	if (MysqlConnect == Empty_Handle)
	{
		log("SQL_Connect Error:");
		log(errorString);
	}
	else
	{
		log("MySQL connection OK");

		set_task(1.0, "load_best_player_authid");
	}
}

public load_best_player_authid()
{
	if (MysqlConnect == Empty_Handle)
	{
		return;
	}

	new Handle:prepareQuery = SQL_PrepareQuery(MysqlConnect, "SELECT steam_id from %s WHERE kills_weapon = (SELECT MAX(kills_weapon) FROM %s);", MysqlDbTable, MysqlDbTable);
	new errorString[512];

	if (!SQL_Execute(prepareQuery))
	{
		SQL_QueryError(prepareQuery, errorString, charsmax(errorString));

		log("SQL_QueryError:");
		log(errorString);
	}
	else
	{
		if (SQL_NumResults(prepareQuery) > 0)
		{
			SQL_ReadResult(prepareQuery, 0, BestPlayerAuthId, charsmax(BestPlayerAuthId));
			SQL_FreeHandle(prepareQuery);

			log("Loaded best player: %s", BestPlayerAuthId);
		}
	}
}

public load_player_data(id)
{
	if (MysqlConnect == Empty_Handle)
	{
		return;
	}

	id -= TASKID_LOAD_PLAYER_DATA;

	new username[32];
	get_user_name(id, username, charsmax(username));

	new authid[32];

	if (is_user_bot(id))
	{
		formatex(authid, charsmax(authid), "BOT:%s", username);
	}
	else
	{
		get_user_authid(id, authid, charsmax(authid));
	}

	new Handle:prepareQuery = SQL_PrepareQuery(MysqlConnect, "SELECT damage, headshots, kills_knife, kills_weapon, assists, deaths, `exp`, online_time, maps_played FROM `%s` WHERE steam_id='%s'", MysqlDbTable, authid);
	new errorString[512];

	if (!SQL_Execute(prepareQuery))
	{
		SQL_QueryError(prepareQuery, errorString, charsmax(errorString));

		log("SQL_QueryError:");
		log(errorString);
	}
	else
	{
		if (SQL_NumResults(prepareQuery) > 0)
		{
			// SQL_ReadResult() If no extra arguments are passed, returns an integer value
			PlayerData[id][DAMAGE] = SQL_ReadResult(prepareQuery, 0);
			PlayerData[id][HEADSHOTS] = SQL_ReadResult(prepareQuery, 1);
			PlayerData[id][KILLS_KNIFE] = SQL_ReadResult(prepareQuery, 2);
			PlayerData[id][KILLS_WEAPON] = SQL_ReadResult(prepareQuery, 3);
			PlayerData[id][ASSISTS] = SQL_ReadResult(prepareQuery, 4);
			PlayerData[id][DEATHS] = SQL_ReadResult(prepareQuery, 5);
			PlayerData[id][EXP] = SQL_ReadResult(prepareQuery, 6);
			PlayerData[id][ONLINE_TIME] = SQL_ReadResult(prepareQuery, 7);
			PlayerData[id][MAPS_PLAYED] = SQL_ReadResult(prepareQuery, 8);

			SQL_FreeHandle(prepareQuery);
		}
	}
}

public store_player_data(id)
{
	log("Store player data %d", id);

	if (MysqlConnect == Empty_Handle)
	{
		log("WARNING: MysqlConnect == Empty_Handle");
		return;
	}

	id -= TASKID_STORE_PLAYER_DATA;

	new username[32];
	get_user_name(id, username, charsmax(username));

	new authid[32];

	if (is_user_bot(id))
	{
		formatex(authid, charsmax(authid), "BOT:%s", username);
	}
	else
	{
		get_user_authid(id, authid, charsmax(authid));
	}

	new Handle:h_Query = SQL_PrepareQuery(MysqlConnect, "SELECT id FROM `%s` WHERE steam_id='%s'", MysqlDbTable, authid);
	new s_Error[512];

	if (!SQL_Execute(h_Query))
	{
		SQL_QueryError(h_Query, s_Error, charsmax(s_Error));

		log("SQL_QueryError:");
		log(s_Error);
	}
	else
	{
		new last_ip[16];
		get_user_ip(id, last_ip, charsmax(last_ip), 1);

		new datetime[128];
		format_time(datetime, charsmax(datetime), "%Y.%m.%d %H:%M");

		new last_map[64];
		get_mapname(last_map, charsmax(last_map));

		new last_visit[32];
		formatex(last_visit, charsmax(last_visit), "%s", datetime);

		new playedTimeSec = get_user_time(id, 1 /*will not include the time it took the client to connect*/);

		// Count map only if played more than CVAR min
		if (playedTimeSec / 60 >= get_cvar_num("ss_play_on_map_min_time"))
		{
			MapData[id][PLAY_ON_MAP] = 1;
		}

		if (SQL_NumResults(h_Query) > 0)
		{
			log("Update existing player %s", username);

			SQL_FreeHandle(h_Query);

			PlayerData[id][ONLINE_TIME] += playedTimeSec;

			static sqlQuery[1024];
			static len;
			len = 0;

			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "UPDATE `%s` SET ", MysqlDbTable);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "`name`='%s',", username);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "damage=%d,", PlayerData[id][DAMAGE]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "headshots=%d,", PlayerData[id][HEADSHOTS]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "kills_knife=%d,", PlayerData[id][KILLS_KNIFE]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "kills_weapon=%d,", PlayerData[id][KILLS_WEAPON]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "assists=%d,", PlayerData[id][ASSISTS]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "deaths=%d,", PlayerData[id][DEATHS]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "`exp`=%d,", PlayerData[id][EXP]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "online_time=%d,", PlayerData[id][ONLINE_TIME]);

			if (MapData[id][PLAY_ON_MAP])
			{
				PlayerData[id][MAPS_PLAYED]++;
			}

			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "maps_played=%d,", PlayerData[id][MAPS_PLAYED]);

			if (!is_user_bot(id))
			{
				len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "last_ip='%s',", last_ip);
			}
			else
			{
				len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "last_ip=NULL,");
			}

			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "last_map='%s',", last_map);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "last_visit='%s'", last_visit);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, " WHERE steam_id='%s';", authid);

			h_Query = SQL_PrepareQuery(MysqlConnect, sqlQuery);

			if (!SQL_Execute(h_Query))
			{
				log("SQL_QueryError:");
				log(s_Error);
			}

			SQL_FreeHandle(h_Query);
		}
		else
		{
			log("Saving new player %s", username);

			PlayerData[id][ONLINE_TIME] = playedTimeSec;

			static sqlQuery[1024];
			static len;
			len = 0;

			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "INSERT INTO `%s` (`id`, steam_id, `name`, damage, headshots, kills_knife, kills_weapon, assists, deaths, wins, losts, `exp`, online_time, maps_played, last_map, reg_ip, last_ip, first_visit, last_visit, first_enter, inactive) VALUES ", MysqlDbTable);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "(NULL,");
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "'%s',", authid);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "'%s',", username);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "%d,", PlayerData[id][DAMAGE]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "%d,", PlayerData[id][HEADSHOTS]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "%d,", PlayerData[id][KILLS_KNIFE]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "%d,", PlayerData[id][KILLS_WEAPON]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "%d,", PlayerData[id][ASSISTS]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "%d,", PlayerData[id][DEATHS]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "%d,", 0);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "%d,", 0);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "%d,", PlayerData[id][EXP]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "%d,", PlayerData[id][ONLINE_TIME]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "%d,", PlayerData[id][MAPS_PLAYED]);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "'%s',", last_map);

			if (!is_user_bot(id))
			{
				len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "'%s',", last_ip);
				len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "'%s',", last_ip);
			}
			else
			{
				len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "NULL,");
				len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "NULL,");
			}

			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "'%s',", last_visit);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "'%s',", last_visit);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "'%d',", 0);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, "'%d'", 0);
			len += formatex(sqlQuery[len], charsmax(sqlQuery)-len, ");");

			h_Query = SQL_PrepareQuery(MysqlConnect, sqlQuery);

			if (!SQL_Execute(h_Query))
			{
				log("SQL_QueryError:");
				log(s_Error);
			}

			SQL_FreeHandle(h_Query);
		}
	}
}

public store_all_players()
{
	new playerIndex[XX - 1], playersCount, i, id;
	get_players(playerIndex, playersCount, "h"); // skip hltv

	for (i = 0; i < playersCount; i++)
	{
		id = playerIndex[i];
		set_task(0.1, "store_player_data", id + TASKID_STORE_PLAYER_DATA);
	}
}

/**
 * Stocks
 */
stock format_motd(szMotd[], iLen, bestPlayerName[], bestDamage, bestKills)
{
	if (equali(ModName, "valve"))
	{
		formatex(szMotd, iLen, "Best player is %s with total %d kills and %d damage. %d unique players total.", bestPlayerName, bestKills, bestDamage, ArraySize(UniquePlayers));
	}
	else
	{
		new iLeft;

		iLeft = formatex(szMotd, iLen, "<body bgcolor=^"#000000^">\
			<p align=^"center^"><font size=5; color=#FFFF00>Player</font><br>\
			<font size=7; color=#0033CC>.: %s :.</font><br>", bestPlayerName);

		iLeft +=
			formatex(szMotd[iLeft], iLen - iLeft, "<font color = #FFB00>Made the most damage in the map with a total of<br>\
			<font size = 5; color = #66FFFF>%d</font><font size = 5; color = #FFFF00> Damage</font><br>\
			And also made<br></font><font size = 5; color = #66FFFF>%d</font><font size = 5; color = #FFFF00> Kills</font>\
			</p>\
			</body>",
		bestDamage, bestKills);
	}
}

stock Create_TE_BEAMCYLINDER(Float:coord[3], Float:axis[3], spr, startframe, framerate, life, width, noise, r, g, b, alpha, speed)
{
	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, coord, 0);
	write_byte(TE_BEAMCYLINDER);
	engfunc(EngFunc_WriteCoord, coord[0]);
	engfunc(EngFunc_WriteCoord, coord[1]);
	engfunc(EngFunc_WriteCoord, coord[2]);
	engfunc(EngFunc_WriteCoord, axis[0]);
	engfunc(EngFunc_WriteCoord, axis[1]);
	engfunc(EngFunc_WriteCoord, axis[2]);
	write_short(spr);
	write_byte(startframe);
	write_byte(framerate);
	write_byte(life);
	write_byte(width);
	write_byte(noise);
	write_byte(r);
	write_byte(g);
	write_byte(b);
	write_byte(alpha);
	write_byte(speed);
	message_end();
}

stock Create_TE_DLIGHT(origin[3], radius, r, g, b, life, decayrate)
{
	message_begin(MSG_PVS, SVC_TEMPENTITY, origin);
	write_byte(TE_DLIGHT);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2]);
	write_byte(radius);
	write_byte(r);
	write_byte(g);
	write_byte(b);
	write_byte(life);
	write_byte(decayrate);
	message_end();
}

stock Create_ScreenFade(id, duration, holdtime, fadetype, r, g, b, a, bool:reliable)
{
	message_begin(reliable ? MSG_ONE : MSG_ONE_UNRELIABLE, MsgidScreenFade, _, id);
	write_short(duration); // (1<<12)*duration
	write_short(holdtime); // (1<<12)*holdtime
	write_short(fadetype);
	write_byte(r);
	write_byte(g);
	write_byte(b);
	write_byte(a);
	message_end();
}

stock remove_scoreboard()
{
	message_begin(MSG_ALL, SVC_FINALE);
	write_string("");
	message_end();
}

stock log(const str[], any:...)
{
	new msg[512];
	vformat(msg, charsmax(msg), str, 2); // 2 - position of arg with "any:..."

	new datetime[128];
	format_time(datetime, charsmax(datetime), "%H:%M:%S");

	server_print("%s [%s] %s", datetime, PLUGIN_NAME, msg);
}

#include <amxmodx>
#include <engine>
#include <fakemeta>
#include <fakemeta_util>

enum (+= 33)
{
    TASKID_SET_PLAYER_FOG = 1,
};

enum _:WEATHER_VALUES {
	ENTITY,
	//SKYBOX[128],
	bool:HAVE_SKY,
	bool:HAVE_LIGHTS,
	bool:HAVE_FOG_COLOR,
	bool:HAVE_FOG_DENSITY,
	bool:HAVE_RAIN,
	bool:HAVE_SNOW
};

new Weather[WEATHER_VALUES];

new GSkybox[128];
new GFogColor[16];
new GFogDensity[16];
new GLights[2];

new Float:FogDensity;
new FogColors[3];

public plugin_precache()
{
	set_weather();
}

public plugin_init()
{
	register_plugin("Weather Effects", "2019.03.17", "Geekrainian @ cuzus.games");

	register_clcmd("say", "say_hook");
	register_clcmd("say_team", "say_hook");

	register_clcmd("changesky", "change_sky_command");
	register_clcmd("changelight", "change_light_command");
}

public change_sky_command()
{
	randomizeSkybox();
}

public change_light_command()
{
	randomizeLights();
}

public client_putinserver(id)
{
	new ModName[32];
	get_modname(ModName, charsmax(ModName));

	// NOTE: Fog is not working in Half-Life
	if (!equali(ModName, "valve") && id > 0 && !is_user_bot(id))
	{
		set_task(0.1, "set_player_fog", id + TASKID_SET_PLAYER_FOG);
	}
}

public say_hook(id)
{
	new message[192];
	read_args(message, 191);
	remove_quotes(message); // "some text" ==> some text
	trim(message);

	new arg1[64], arg2[64], arg3[64], arg4[64], arg5[64], arg6[64];
	parse(message, arg1, charsmax(arg1), arg2, charsmax(arg2), arg3, charsmax(arg3), arg4, charsmax(arg4), arg5, charsmax(arg5), arg6, charsmax(arg6));

	if (equali(arg1, "lt"))
	{
		new value = str_to_num(arg2);
		set_lights_command(id, value);
	}
}

public set_player_fog(id)
{
	id -= TASKID_SET_PLAYER_FOG;

	make_fog(id, FogColors[0], FogColors[1], FogColors[2], FogDensity);
}

public set_weather()
{
	if (!Weather[HAVE_SKY])
	{
		randomizeSkybox();
	}

	if (!Weather[HAVE_LIGHTS])
	{
		randomizeLights();
	}

	// Weather chance
	new const weatherEff = random_num(0, 3);
	switch (weatherEff)
	{
		case 1:
		{
			if (!Weather[HAVE_RAIN])
			{
				renderRain();
			}
		}
		case 2:
		{
			if (!Weather[HAVE_SNOW])
			{
				renderSnow();
			}
		}
		default:
			server_print("Weather: OFF");
	}

	//renderFog(1);

	FogDensity = random_float(0.0007, 0.00001);
	FogColors[0] = random_num(0, 255);
	FogColors[1] = random_num(0, 255);
	FogColors[2] = random_num(0, 255);

	server_print("Setting Fog: %d %d %d, %f", FogColors[0], FogColors[1], FogColors[2], FogDensity);
}

new const SKYES_NIGHT[][] = {
	"bioshock_rapture_",
	"mnight",
	"grimmnight",
	"52h03",
	"dark",
	"dwell",
	"z3",
	"excity",
	"dx_deepwater",
	"dx_village",
	"farmyard",
	"nightocean",
	"nightsea",
	"ravi",
	"lunarsky_01",
	"sky51",
	"sky_oar_night_01_",
	"fullmoon",
	"devilish_",
	"arml_",
	"ngblue_",
	"nightsky",
	"sealed",
	"streetsky",
	"desert_night_",
	"waterworld15",
	"hell",
	"siv2",
	"cv_",
	"dawn",
	"lmmnts_1",
	"nuclear_winter2",
	"onlymoon_",
	"spdog_",
	"xld_",
	"zombiehell"
};

public randomizeSkybox()
{
	server_print("Getting random skybox");

	switch (random_num(2, 11)) // [2]night ... [11]L2
	{
		// night
		case 2: {
			new pos = random(sizeof SKYES_NIGHT - 1);

			server_print("Skybox: [2]night-> ...");
			server_print("Skybox: ... [%d]", pos);

			formatex(GSkybox, 127, SKYES_NIGHT[pos]);
		}
		// evening
		case 3: {
			new _even = random_num(2, 15);

			server_print("Skybox: [3]evening-> ...");
			server_print("Skybox: ... [%d]", _even);

			switch (_even)
			{
				case 2:	formatex(GSkybox, 127, "inferno");
				case 3:		formatex(GSkybox, 127, "aristocracy");
				case 4:		formatex(GSkybox, 127, "whit");
				case 5:		formatex(GSkybox, 127, "1");
				case 6:	formatex(GSkybox, 127, "sky_quarry03");
				case 7:	formatex(GSkybox, 127, "badomen_");
				case 8:		formatex(GSkybox, 127, "mpa119");
				case 9:		formatex(GSkybox, 127, "mpa37");
				case 10:	formatex(GSkybox, 127, "sky38");
				case 11:	formatex(GSkybox, 127, "sunsetmountain");
				case 12:	formatex(GSkybox, 127, "nightball_");
				case 13:	formatex(GSkybox, 127, "sky_l4d_c2m1_hdr");
				case 14:	formatex(GSkybox, 127, "sky_l4d_night02_hdr");
				case 15:	formatex(GSkybox, 127, "ntdm2_");
			}
		}
		// overcast
		case 4: {
			new _gameover = random_num(1, 23);

			server_print("Skybox: [4]overcast-> ...");
			server_print("Skybox: ... [%d]", _gameover);

			switch(_gameover)
			{
				case 1: formatex(GSkybox, 127, "badweather");
				//formatex(GSkybox, 127, "CCCP"); // == badweather, but warmified
				case 2: formatex(GSkybox, 127, "de_sbd_");
				case 3:	formatex(GSkybox, 127, "tumanno_");
				case 4:	formatex(GSkybox, 127, "sarajevo_");
				case 5: formatex(GSkybox, 127, "cs_frigid_");
				case 6: formatex(GSkybox, 127, "snowy_");
				case 7:		formatex(GSkybox, 127, "l1escape1_");
				case 8:		formatex(GSkybox, 127, "italy");
				case 9:		formatex(GSkybox, 127, "spree");
				case 10:	formatex(GSkybox, 127, "jagd");
				case 11:	formatex(GSkybox, 127, "sky_airexchange01");
				case 12:	formatex(GSkybox, 127, "sky_borealis01");
				case 13:	formatex(GSkybox, 127, "morass");
				case 14:	formatex(GSkybox, 127, "sky_c17_02");
				case 15:	formatex(GSkybox, 127, "sky_c17_03");
				case 16:	formatex(GSkybox, 127, "sky_depot01");
				case 17:	formatex(GSkybox, 127, "snowy");
				case 18:	formatex(GSkybox, 127, "l2sky06_");
				case 19:	formatex(GSkybox, 127, "l2sky15_");
				case 20:	formatex(GSkybox, 127, "l2sky18_");
				case 21:	formatex(GSkybox, 127, "l2sky45_");
				case 22:	formatex(GSkybox, 127, "hs_");
				case 23:	formatex(GSkybox, 127, "kvartal");
			}
		}
		// sunset
		case 5: {
			new _sunstrike = random_num(1, 17);

			server_print("Skybox: [5]sunset-> ...");
			server_print("Skybox: ... [%d]", _sunstrike);

			switch (_sunstrike)
			{
				case 1:		formatex(GSkybox, 127, "deadlock");
				case 2:	formatex(GSkybox, 127, "mpa109");
				case 3:	formatex(GSkybox, 127, "sky17");
				case 4:		formatex(GSkybox, 127, "l2sky01_");
				case 5:		formatex(GSkybox, 127, "dx_dusted");
				case 6:		formatex(GSkybox, 127, "mpa95");
				case 7:	formatex(GSkybox, 127, "desert01");
				case 8:		formatex(GSkybox, 127, "bloody-heresy");
				case 9:		formatex(GSkybox, 127, "ze");
				case 10:	formatex(GSkybox, 127, "dm_byakugan");
				case 11:	formatex(GSkybox, 127, "dusk");
				case 12:	formatex(GSkybox, 127, "sky32");
				case 13:	formatex(GSkybox, 127, "mpa82_cs");
				case 14:	formatex(GSkybox, 127, "twildes");
				case 15:	formatex(GSkybox, 127, "dam_");
				case 16:	formatex(GSkybox, 127, "depression-pass_");
				case 17:	formatex(GSkybox, 127, "l2sky61_");
			}
		}
		// fair
		case 6: {
			new _hellfair = random_num(1,13);

			server_print("Skybox: [6]fair-> ...");
			server_print("Skybox: ... [%d]", _hellfair);

			switch (_hellfair)
			{
				case 1:		formatex(GSkybox, 127, "grnplsnt");
				case 2:		formatex(GSkybox, 127, "sky3");
				case 3:		formatex(GSkybox, 127, "zp_trains2");
				case 4:		formatex(GSkybox, 127, "zps_inboxed");
				case 5:	formatex(GSkybox, 127, "desert");
				case 6:		formatex(GSkybox, 127, "minecraft_skybox");
				case 7:		formatex(GSkybox, 127, "sky_world");
				case 8:	formatex(GSkybox, 127, "mountain");
				case 9:		formatex(GSkybox, 127, "cieloxd");
				case 10:	formatex(GSkybox, 127, "sky25");
				case 11:	formatex(GSkybox, 127, "landscape_");
				case 12:	formatex(GSkybox, 127, "desnoon");
				case 13:	formatex(GSkybox, 127, "52h06");
			}
		}
		// cloudy
		case 7: {
			new _claud9 = random_num(1, 22);

			server_print("Skybox: [7]cloudy-> ...");
			server_print("Skybox: ... [%d]", _claud9);

			switch (_claud9)
			{
				case 1: formatex(GSkybox, 127, "ferreiro_");
				case 2: formatex(GSkybox, 127, "a_s2d");
				case 3: formatex(GSkybox, 127, "blood");
				case 4: formatex(GSkybox, 127, "de_shrine");
				case 5:	formatex(GSkybox, 127, "water_");
				case 6: formatex(GSkybox, 127, "balsun");
				case 7: formatex(GSkybox, 127, "misty");
				case 8: formatex(GSkybox, 127, "stormyday");
				case 9: formatex(GSkybox, 127, "flowsky09_");
				case 10:	formatex(GSkybox, 127, "sky45");
				case 11:	formatex(GSkybox, 127, "blue_winter_");
				case 12:	formatex(GSkybox, 127, "sky_palace01");
				case 13:	formatex(GSkybox, 127, "sky_quarry01hdr");
				case 14:	formatex(GSkybox, 127, "sky_spire01");
				case 15:	formatex(GSkybox, 127, "sky_station01");
				case 16:	formatex(GSkybox, 127, "urbannightburning_ldr");
				case 17:	formatex(GSkybox, 127, "urbannightstorm_ldr");
				case 18:	formatex(GSkybox, 127, "fatal");
				case 19:	formatex(GSkybox, 127, "cliff");
				case 20:	formatex(GSkybox, 127, "sky_19_cube_");
				case 21:	formatex(GSkybox, 127, "mist");
				case 22:	formatex(GSkybox, 127, "fellaville_");
			}
		}
		// quake 3 skies
		case 8: {
			new _quake3 = random_num(1, 4);

			server_print("Skybox: [8]q3-> ...");
			server_print("Skybox: ... [%d]", _quake3);

			switch (_quake3)
			{
				case 1:		formatex(GSkybox, 127, "sky01_");
				case 2:		formatex(GSkybox, 127, "sky05_");
				case 3:		formatex(GSkybox, 127, "sky06_");
				case 4:		formatex(GSkybox, 127, "sky12_");
			}
		}
		// gradients
		case 9: {
			new _grad = random_num(1, 12);

			server_print("Skybox: [9]gradients-> ...");
			server_print("Skybox: ... [%d]", _grad);

			switch(_grad)
			{
				case 1:		formatex(GSkybox, 127, "grad2_01_");
				case 2:		formatex(GSkybox, 127, "grad2_02_");
				case 3:		formatex(GSkybox, 127, "grad6_01_");
				case 4:		formatex(GSkybox, 127, "grad6_02_");
				case 5:		formatex(GSkybox, 127, "grad6_03_");
				case 6:		formatex(GSkybox, 127, "grad6_04_");
				case 7:		formatex(GSkybox, 127, "grad6_05_");
				case 8:		formatex(GSkybox, 127, "grad6_06_");
				case 9:		formatex(GSkybox, 127, "grad6_07_");
				case 10:	formatex(GSkybox, 127, "grad13_01_");
				case 11:	formatex(GSkybox, 127, "grad13_02_");
				case 12:	formatex(GSkybox, 127, "grad13_03_");
			}
		}
		// killing floor
		case 10: {
			new _kf = random_num(1, 3);

			server_print("Skybox: [10]kf-> ...");
			server_print("Skybox: ... [%d]", _kf);

			switch (_kf)
			{
				case 1: formatex(GSkybox, 127, "ForestFiller");
				case 2: formatex(GSkybox, 127, "night02");
				case 3: formatex(GSkybox, 127, "m10_");
			}
		}
		// Lineage 2
		case 11: {
			new _lin2 = random_num(1, 7);

			server_print("Skybox: [11]L2-> ...");
			server_print("Skybox: ... [%d]", _lin2);

			switch (_lin2)
			{
				case 1: formatex(GSkybox, 127, "l2sky107_");
				case 2: formatex(GSkybox, 127, "l2sky138_");
				case 3: formatex(GSkybox, 127, "l2sky177_");
				case 4: formatex(GSkybox, 127, "l2sky227_");
				case 5: formatex(GSkybox, 127, "l2sky568_");
				case 6: formatex(GSkybox, 127, "l2sky673_");
				case 7: formatex(GSkybox, 127, "l2sky917_");
			}
		}
	}

	new _tmp[128];

	formatex(_tmp, 127, "gfx/env/%srt.tga", GSkybox);
	precache_generic(_tmp);
	formatex(_tmp, 127, "gfx/env/%slf.tga", GSkybox);
	precache_generic(_tmp);
	formatex(_tmp, 127, "gfx/env/%sft.tga", GSkybox);
	precache_generic(_tmp);
	formatex(_tmp, 127, "gfx/env/%sdn.tga", GSkybox);
	precache_generic(_tmp);
	formatex(_tmp, 127, "gfx/env/%sbk.tga", GSkybox);
	precache_generic(_tmp);
	formatex(_tmp, 127, "gfx/env/%sup.tga", GSkybox);
	precache_generic(_tmp);

	replace(_tmp, 127, ".tga", "");

	formatex(_tmp, 127, "sv_skyname %s", GSkybox);
	server_cmd(_tmp);

	server_print("Setting skybox to: %s", GSkybox);

	Weather[HAVE_SKY] = true;
}

public randomizeLights()
{
	new alphabet[26];

	alphabet[0] = 'a'; alphabet[1] = 'b'; alphabet[2] = 'c'; alphabet[3] = 'd';
	alphabet[4] = 'e'; alphabet[5] = 'f'; alphabet[6] = 'g'; alphabet[7] = 'h';
	alphabet[8] = 'i'; alphabet[9] = 'j'; alphabet[10] = 'k'; alphabet[11] = 'l';
	alphabet[12] = 'm'; alphabet[13] = 'n'; alphabet[14] = 'o'; alphabet[15] = 'p';
	alphabet[16] = 'q'; alphabet[17] = 'r'; alphabet[18] = 's'; alphabet[19] = 't';
	alphabet[20] = 'u'; alphabet[21] = 'v'; alphabet[22] = 'w'; alphabet[23] = 'x';
	alphabet[24] = 'y'; alphabet[25] = 'z';

	new _val[2];

	formatex(_val, 1, "%s", alphabet[random_num(3, sizeof alphabet - 1)]);

	/*
	new daytime = random_num(2,5);

	switch (daytime)
	{
		case 1: formatex(_val, 1, "%s", alphabet[0]);				// full dark
		case 2: formatex(_val, 1, "%s", alphabet[random_num(1,3)]);	// twilight
		case 3: formatex(_val, 1, "%s", alphabet[random_num(4,7)]);	// dawning
		case 4: formatex(_val, 1, "%s", alphabet[random_num(8,13)]);	// half-day
		case 5: formatex(_val, 1, "%s", alphabet[random_num(14,18)]);	// lightest day
		case 6: formatex(_val, 1, "%s", alphabet[random_num(19,25)]);	// EPIC LIGHT
	}
	*/

	//set_lights(_val);
	formatex(GLights, 1, "%s", _val);

	set_lights(GLights);

	server_print("Setting lights to: %s", strlen(GLights) > 0 ? GLights : "map value");
}

public renderFog(power)
{
	new fog = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "env_fog"));

	if (fog)
	{
		switch (power)
		{
			case 1: // low
			{
				if (!Weather[HAVE_FOG_COLOR])
				{
					//zm_italy: 64 102 121
					formatex(GFogColor, 15, "%d %d %d", random_num(0,255), random_num(0,255), random_num(0,255));
					//formatex(GFogColor, 15, "%s", FOG_COLORS[randVal(sizeof FOG_COLORS)]);
				}

				if (!Weather[HAVE_FOG_DENSITY])
				{
					new Float:rndens = random_float(0.0007, 0.00001);
					formatex(GFogDensity, 15, "%.6f", rndens);
					//formatex(GFogDensity, 15, "%s", FOG_DENSITIES[randVal(sizeof FOG_DENSITIES)]);
				}

				fm_set_kvd(fog, "rendercolor", GFogColor, "env_fog");
				fm_set_kvd(fog, "density", GFogDensity, "env_fog");

				server_print("Fog: RGB = %s, density = %s", GFogColor, GFogDensity);
			}
			case 2: // normal
			{
				fm_set_kvd(fog, "density", "0.0010", "env_fog");
				fm_set_kvd(fog, "rendercolor", "128 128 128", "env_fog");
			}
			case 3: // high
			{
				fm_set_kvd(fog, "density", "0.0015", "env_fog");
				fm_set_kvd(fog, "rendercolor", "128 128 128", "env_fog");
			}
		}
	}
}

// TODO: Rain sound & lightning effects
public renderRain()
{
	//SPR_LASERBEAM = precache_model("sprites/laserbeam.spr");

	//precache_sound("ambience/rain.wav");
	//precache_sound("ambience/thunder_clap.wav");

	Weather[ENTITY] = create_entity("env_rain");

	//register_think("env_rain", "thinkEnvRain");
	//mSetNextThink(Weather[ENTITY], 1.0);

	server_print("Weather: Rainy");
}

public renderSnow()
{
	Weather[ENTITY] = create_entity("env_snow");

	server_print("Weather: Snowy");
}

// NOTE: Is not working properly in Half-Life
public set_lights_command(id, value)
{
	new GAlphabet[27];
	GAlphabet[1] = 'a'; GAlphabet[2] = 'b'; GAlphabet[3] = 'c'; GAlphabet[4] = 'd';
	GAlphabet[5] = 'e'; GAlphabet[6] = 'f'; GAlphabet[7] = 'g'; GAlphabet[8] = 'h';
	GAlphabet[9] = 'i'; GAlphabet[10] = 'j'; GAlphabet[11] = 'k'; GAlphabet[12] = 'l';
	GAlphabet[13] = 'm'; GAlphabet[14] = 'n'; GAlphabet[15] = 'o'; GAlphabet[16] = 'p';
	GAlphabet[17] = 'q'; GAlphabet[18] = 'r'; GAlphabet[19] = 's'; GAlphabet[20] = 't';
	GAlphabet[21] = 'u'; GAlphabet[22] = 'v'; GAlphabet[23] = 'w'; GAlphabet[24] = 'x';
	GAlphabet[25] = 'y'; GAlphabet[26] = 'z';

	if (value < 0 || value > 26)
	{
		client_print(id, print_chat, "Use //setlights <0-26>");
	}
	else
	{
		client_print(id, print_chat, "Setting lights to %d", value);

		if (value == 0)
			set_lights("#OFF");
		else
			set_lights(GAlphabet[value]);
	}
}

stock make_fog(id, R, G, B, Float:amount)
{
	message_begin(id ? MSG_ONE : MSG_ALL, get_user_msgid("Fog"), {0,0,0}, id);
	write_byte(R); // Red
	write_byte(G); // Green
	write_byte(B); // Blue
	write_long(_:amount); // Amount
	message_end();
}

#include <amxmodx>
#include <amxmisc>

#define CVAR "fake_slots"

new kvar1;

public plugin_init()
{
	register_plugin("Fake Slots", "1.0", "souriz");

	register_cvar(CVAR, "255");
}

public plugin_cfg()
{
	set_task(1.0, "postCfg");
}

public postCfg()
{
	kvar1 = get_cvar_num(CVAR);

	if (kvar1 > 255)
		set_cvar_num(CVAR, 255)

	server_cmd("sv_visiblemaxplayers %d", kvar1)

	set_task(0.9, "cmdUpdateCvar", 512, "", 0, "b")
}

public cmdUpdateCvar()
{
	kvar1 = get_cvar_num(CVAR);

	if (kvar1 > 255)
		set_cvar_num(CVAR, 255);

	server_cmd("sv_visiblemaxplayers %d", kvar1);
}

#include <amxmodx>
#include <hamsandwich>
#include <fun>
#include <fakemeta>

public plugin_init()
{
	register_plugin("Bot Power", "2020.07.04", "Geekrainian @ cuzus.games");

	register_cvar("bp_mult_damage_chance", "70");
	register_cvar("bp_mult_damage_min_rate", "2.0");
	register_cvar("bp_mult_damage_max_rate", "3.0");

	register_cvar("bp_reduce_damage_chance", "20");
	register_cvar("bp_reduce_damage_min_rate", "0.6");
	register_cvar("bp_reduce_damage_max_rate", "0.3");

	register_cvar("bp_refill_ammo_chance", "20");

	RegisterHam(Ham_TakeDamage, "player", "ham_take_damage_pre", 0);
}

public ham_take_damage_pre(victim, inflictor, attacker, Float:damage, damagebits)
{
	new attackerName[32];
	get_user_name(attacker, attackerName, 31);

	new victimName[32];
	get_user_name(victim, victimName, 31);

	if (is_user_bot(attacker))
	{
		if (random_num(0, 100) <= get_cvar_num("bp_refill_ammo_chance"))
		{
			refill_ammo(attacker);
		}

		if (random_num(0, 100) <= get_cvar_num("bp_mult_damage_chance"))
		{
			new Float:mult = random_float(get_cvar_float("bp_mult_damage_min_rate"), get_cvar_float("bp_mult_damage_max_rate"));
			damage *= mult;
			SetHamParamFloat(4, damage);
		}

		// If you're changing a parameter you have to return HAM_HANDLED. Otherwise it will be ignored by Ham.
		return HAM_HANDLED;
	}

	if (is_user_bot(victim))
	{
		if (random_num(0, 100) <= get_cvar_num("bp_refill_ammo_chance"))
		{
			refill_ammo(victim);
		}

		if (!is_user_bot(attacker))
		{
			if (random_num(0, 100) <= get_cvar_num("bp_reduce_damage_chance"))
			{
				new Float:mult = random_float(get_cvar_float("bp_reduce_damage_min_rate"), get_cvar_float("bp_reduce_damage_max_rate"));
				damage *= mult;
				SetHamParamFloat(4, damage);
			}

			return HAM_HANDLED;
		}
	}

	return HAM_IGNORED;
}

public refill_ammo(id)
{
	new mod[16];
	get_modname(mod, charsmax(mod));

	if (!equal(mod, "cstrike"))
	{
		refill_hl_ammo(id);
	}
}

stock refill_hl_ammo(id)
{
	set_user_bpammo(id, HLW_HORNETGUN, 8);
	set_user_bpammo(id, HLW_PYTHON, 36);
	set_user_bpammo(id, HLW_PYTHON, 36);
	set_user_bpammo(id, HLW_CROSSBOW, 50);
	set_user_bpammo(id, HLW_SNARK, 15);
	set_user_bpammo(id, HLW_TRIPMINE, 5);
	set_user_bpammo(id, HLW_SATCHEL, 5);
	set_user_bpammo(id, HLW_HANDGRENADE, 10);
	set_user_bpammo(id, HLW_GLOCK, 250);
	set_user_bpammo(id, HLW_MP5, 250);
	set_user_bpammo(id, HLW_GAUSS, 100);
	set_user_bpammo(id, HLW_MP5, 250);
	set_user_bpammo(id, HLW_EGON, 100);
	set_user_bpammo(id, HLW_RPG, 5);
	set_user_bpammo(id, HLW_SHOTGUN, 125);

	for (new i; i < 5; i++)
	{
		give_item(id, "ammo_ARgrenades");
	}

	for (new i; i < 5; i++)
	{
		give_item(id, "ammo_mp5grenades");
	}
}

#define RPG_BPAMMO_OFFSET				315
#define TRIPMINE_BPAMMO_OFFSET			317
#define SATCHEL_BPAMMO_OFFSET			318
#define HORNET_BPAMMO_OFFSET			321
#define GRENADE_BPAMMO_OFFSET			319
#define SNARK_BPAMMO_OFFSET				320
#define PYTHON_BPAMMO_OFFSET			313
#define CROSSBOW_BPAMMO_OFFSET			316
#define GAUSS_EGON_BPAMMO_OFFSET		314
#define SHOTGUN_BPAMMO_OFFSET			310
#define GLOCK_MP5_9MM_BPAMMO_OFFSET		311
#define CHAINGUN_BPAMMO_OFFSET			312

stock set_user_bpammo(index, weapon, amount)
{
	new offset;

	switch (weapon)
	{
		case HLW_GLOCK, HLW_MP5: offset = GLOCK_MP5_9MM_BPAMMO_OFFSET;
		case HLW_PYTHON: offset = PYTHON_BPAMMO_OFFSET;
		case HLW_CHAINGUN: offset = CHAINGUN_BPAMMO_OFFSET;
		case HLW_CROSSBOW: offset = CROSSBOW_BPAMMO_OFFSET;
		case HLW_SHOTGUN: offset = SHOTGUN_BPAMMO_OFFSET;
		case HLW_RPG: offset = RPG_BPAMMO_OFFSET;
		case HLW_GAUSS, HLW_EGON: offset = GAUSS_EGON_BPAMMO_OFFSET;
		case HLW_HORNETGUN: offset = HORNET_BPAMMO_OFFSET;
		case HLW_HANDGRENADE: offset = GRENADE_BPAMMO_OFFSET;
		case HLW_TRIPMINE: offset = TRIPMINE_BPAMMO_OFFSET;
		case HLW_SATCHEL: offset = SATCHEL_BPAMMO_OFFSET;
		case HLW_SNARK: offset = SNARK_BPAMMO_OFFSET;
	}

	set_pdata_int(index, offset, amount);
}

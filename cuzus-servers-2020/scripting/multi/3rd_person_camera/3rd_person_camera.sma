#include <amxmodx>
#include <amxmisc>
#include <engine>
#include <fakemeta>
#include <hamsandwich>

#define clearUserInCamera(%0)		PlayerCamFlag &= ~(1<<(%0&31))
#define isUserInCamera(%0)			PlayerCamFlag & 1<<(%0&31)
#define toggleUserCameraState(%0)	PlayerCamFlag ^= 1<<(%0&31)

#define USE_TOGGLE 3

#define XX 33

new PlayerCamera[XX];
new PlayerCamFlag;

new CamDist[XX] = {10, ...};

public plugin_init()
{
	register_plugin("3rd Person Camera (Lineage 2 like)", "2019.03.13", "Geekrainian @ cuzus.games");

	register_clcmd("+camera", "Camera_change");
	register_clcmd("+camdistup", "Camera_distUp");
	register_clcmd("+camdistdn", "Camera_distDn");
	register_clcmd("+camres", "Camera_reset");
}

public client_disconnect(id)
{
    new ent = PlayerCamera[id];

    if (pev_valid(ent))
	{
        engfunc(EngFunc_RemoveEntity, ent);
    }

    PlayerCamera[id] = 0;

    clearUserInCamera(id);

    Camera_CheckForward();
}

public client_putinserver(id)
{
	PlayerCamera[id] = 0;
	clearUserInCamera(id);

	client_cmd(id, "bind ^"n^" ^"+camera^"");
	client_cmd(id, "bind ^"MWHEELDOWN^" ^"+camdistdn^"");
	client_cmd(id, "bind ^"MWHEELUP^" ^"+camdistup^"");
	client_cmd(id, "bind ^"MOUSE3^" ^"+camres^"");
}

public Camera_change(id)
{
    if (!is_user_alive(id))
        return;

    new ent = PlayerCamera[id];

    if (!pev_valid(ent))
    {
        static triggercam;

        if (!triggercam)
		{
            triggercam = engfunc(EngFunc_AllocString, "trigger_camera");
        }

        ent = engfunc(EngFunc_CreateNamedEntity, triggercam);

        set_kvd(0, KV_ClassName, "trigger_camera");
        set_kvd(0, KV_fHandled, 0);
        set_kvd(0, KV_KeyName, "wait");
        set_kvd(0, KV_Value, "999999");

        dllfunc(DLLFunc_KeyValue, ent, 0);

        set_pev(ent, pev_spawnflags, SF_CAMERA_PLAYER_TARGET | SF_CAMERA_PLAYER_POSITION);
        set_pev(ent, pev_flags, pev(ent, pev_flags) | FL_ALWAYSTHINK);

        dllfunc(DLLFunc_Spawn, ent);

        PlayerCamera[id] = ent;
    }

    toggleUserCameraState(id);

    Camera_CheckForward();

    new Float:flMaxSpeed, iFlags = pev(id, pev_flags);
    pev(id, pev_maxspeed, flMaxSpeed);

    ExecuteHam(Ham_Use, ent, id, id, USE_TOGGLE, 1.0);

    set_pev(id, pev_flags, iFlags);
    // depending on mod, you may have to send SetClientMaxspeed here.
    // engfunc(EngFunc_SetClientMaxspeed, id, flMaxSpeed)
    set_pev(id, pev_maxspeed, flMaxSpeed);

	//return PLUGIN_HANDLED;
}

public Camera_distUp(id)
{
	if (get_user_flags(id) & ADMIN_IMMUNITY)
	{
		if (CamDist[id] < 999)
			CamDist[id]++;
	}
	else
	{
		if (CamDist[id] < 46)
			CamDist[id]++;
	}
}

public Camera_distDn(id)
{
	if (get_user_flags(id) & ADMIN_IMMUNITY)
	{
		if (CamDist[id] > -999)
			CamDist[id]--;
	}
	else
	{
		if (CamDist[id] > 1)
			CamDist[id]--;
	}
}

public Camera_reset(id)
{
	CamDist[id] = 10; // default distance for 3rd person
}

public hamThinkCamera(ent)
{
	static id;

	if (!(id = get_cam_owner(ent)))
		return;

	static Float:fVecPlayerOrigin[3], Float:fVecCameraOrigin[3], Float:fVecAngles[3], Float:fVecBack[3];

	pev(id, pev_origin, fVecPlayerOrigin);
	pev(id, pev_view_ofs, fVecAngles);
	fVecPlayerOrigin[2] += fVecAngles[2];

	pev(id, pev_v_angle, fVecAngles);

	// See player from front?
	//fVecAngles[0] = 15.0
	//fVecAngles[1] += fVecAngles[1] > 180.0 ? -180.0 : 180.0

	angle_vector(fVecAngles, ANGLEVECTOR_FORWARD, fVecBack);

	// Move back to see ourself (150 units)
	fVecCameraOrigin[0] = fVecPlayerOrigin[0] + (-fVecBack[0] * float(CamDist[id]) * 10.0); //150.0);
	fVecCameraOrigin[1] = fVecPlayerOrigin[1] + (-fVecBack[1] * float(CamDist[id]) * 10.0); //150.0);
	fVecCameraOrigin[2] = fVecPlayerOrigin[2] + (-fVecBack[2] * float(CamDist[id]) * 10.0); //150.0);

	engfunc(EngFunc_TraceLine, fVecPlayerOrigin, fVecCameraOrigin, IGNORE_MONSTERS, id, 0);

	static Float:flFraction;
	get_tr2(0, TR_flFraction, flFraction);

	if (flFraction != 1.0) // adjust camera place if close to a wall
	{
		flFraction *= float(CamDist[id]) * 10.0; //150.0;
		fVecCameraOrigin[0] = fVecPlayerOrigin[0] + (-fVecBack[0] * flFraction);
		fVecCameraOrigin[1] = fVecPlayerOrigin[1] + (-fVecBack[1] * flFraction);
		fVecCameraOrigin[2] = fVecPlayerOrigin[2] + (-fVecBack[2] * flFraction);
	}

	set_pev(ent, pev_origin, fVecCameraOrigin);
	set_pev(ent, pev_angles, fVecAngles);
}

public Camera_CheckForward()
{
    static HamHook:HhCameraThink, FhSetView;

    if (PlayerCamFlag)
    {
        if (!FhSetView)
		{
            FhSetView = register_forward(FM_SetView, "SetView");
        }

        if (!HhCameraThink)
		{
            HhCameraThink = RegisterHam(Ham_Think, "trigger_camera", "hamThinkCamera");
        }
        else
		{
            EnableHamForward(HhCameraThink);
        }
    }
	else
	{
        if (FhSetView)
		{
            unregister_forward(FM_SetView, FhSetView);
            FhSetView = 0;
        }

        if (HhCameraThink)
		{
            DisableHamForward(HhCameraThink);
        }
    }
}

get_cam_owner(ent)
{
    static id;

    for (id = 1; id <= get_maxplayers(); id++)
    {
        if (PlayerCamera[id] == ent)
		{
            return id;
        }
    }

    return 0;
}

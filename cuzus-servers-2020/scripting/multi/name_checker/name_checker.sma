#include <amxmodx>
#include <amxmisc>
#include <fakemeta>

new const DOMAINS[][] =
{
	".com",
	".cz",
	".kz",
	".net",
	".org",
	".tk",
	".ru"
};

new const BAD_NAMES[][] =
{
	"player",
	"player (1)",
	"iplayer",
	"player1",
	"1player1",
	"qplayer",
	"name",
	"noname",
	"bad",
	"suka",
	"ska",
	"gandon",
	"pidor",
	"pidar",
	"loh",
	"lohh",
	"lox",
	"lol",
	"asd",
	"qwe",
	"asdasd",
	"qweqwe",
	"qwerty"
};

new const FALLBACK_NAMES[][] =
{
	"Peach",
	"Apple",
	"Mellon",
	"Banana",
	"Berry",
	"Cherry",
	"Mango",
	"Pomelo",
	"Avocado",
	"Pumpkin",
	"Tomato",
	"Potato",
	"Coconut",
	"Orange",
	"Plum"
};

public plugin_init()
{
    register_plugin("Name Checker", "2019.03.12" , "Geekrainian @ cuzus.games");
}

public client_putinserver(id)
{
	check_bad_name(id);
}

public check_bad_name(id)
{
	new username[64];
	get_user_name(id, username, charsmax(username));

	new bool:isBadName;

	for (new k; k < sizeof BAD_NAMES; k++)
	{
		if (equali(username, BAD_NAMES[k]))
		{
			isBadName = true;
			break;
		}
	}

	new bool:isContainsDomain;

	for (new k; k < sizeof DOMAINS; k++)
	{
		new pos = containi(username, DOMAINS[k]);

		if (pos != -1)
		{
			isContainsDomain = true;
			break;
		}
	}

	if (isBadName || isContainsDomain)
	{
		formatex(username, charsmax(username), "%s%d", FALLBACK_NAMES[random_num(0, sizeof FALLBACK_NAMES)], random_num(100, 999));
		set_pev(id, pev_netname, username);
	}
}

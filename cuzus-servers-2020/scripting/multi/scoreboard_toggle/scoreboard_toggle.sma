#include <amxmodx>
#include <fakemeta>

#define CHARSTAT_OPEN "lineage2/interfsound/charstat_open_01.wav"
#define CHARSTAT_CLOSE "lineage2/interfsound/charstat_close_01.wav"

public plugin_init()
{
	register_plugin("Scoreboard Toggle", "2019.03.17", "Geekrainian @ cuzus.games");

	register_forward(FM_CmdStart, "fw_cmd_start");
}

public plugin_precache()
{
	precache_sound(CHARSTAT_OPEN);
	precache_sound(CHARSTAT_CLOSE);
}

public fw_cmd_start(id, uc_handle, random_seed)
{
	static CurButton, OldButton;
	CurButton = get_uc(uc_handle, UC_Buttons);

	OldButton = (pev(id, pev_oldbuttons) & IN_SCORE);

	if (CurButton & IN_SCORE && !OldButton)
	{
		client_cmd(id, "spk %s", CHARSTAT_OPEN);
	}

	if (!(CurButton & IN_SCORE) && OldButton)
	{
		client_cmd(id, "spk %s", CHARSTAT_CLOSE);
	}
}

#include <amxmodx>
#include <amxmisc>
#include <engine>
#include <hamsandwich>
#include <fakemeta>
#include <fun> //set_user_armor
#include <vector>

#define entity_remove(%1) entity_set_int(%1, EV_INT_flags, entity_get_int(%1, EV_INT_flags) | FL_KILLME)

#define FFADE_IN		0x0000 // Just here so we don't pass 0 into the function
#define FFADE_OUT		0x0001 // Fade out (not in)
#define FFADE_MODULATE	0x0002 // Modulate (don't blend)
#define FFADE_STAYOUT	0x0004 // ignores the duration, stays faded out until new ScreenFade message received

#define HP_POTION_ENTITY_NAME "hp_potion_ent"
#define AP_POTION_ENTITY_NAME "ap_potion_ent"

#define MAX_PLAYER_HEALTH 100

#define M_L2POTION "models/lineage2/L2Potion.mdl"
#define POTION_SKIN_LESSER_RED 3
#define POTION_SKIN_LESSER_BLUE 5

#define S_ITEMDROP_POTION "lineage2/itemsound/itemdrop_potion.wav"
#define S_POTION "lineage2/skillsound/potion.wav"

#define ITEM_DESTROY_SPR "sprites/lineage2/item_destroy.spr"

new const SPR_POTION_HP[] = "sprites/lineage2/potion_hp.spr";
new const SPR_POTION_MP[] = "sprites/lineage2/potion_mp.spr";

new SPR_ITEM_DESTROY;

new MsgidScreenFade;

enum (+= 33)
{
	TASKID_EFFECT_REMOVE = 1
};

enum
{
	POTION_TYPE_HP = 0,
	POTION_TYPE_AP
};

public plugin_init()
{
	register_plugin("HP AP Potions", "2019.03.12", "Geekrainian @ cuzus.games");

	register_cvar("hap_drop_potion_chance", "10");
	register_cvar("hap_drop_both_potions", "0"); // 0 - drop random one
	register_cvar("hap_pickup_hp_min", "5");
	register_cvar("hap_pickup_hp_max", "20");
	register_cvar("hap_pickup_ap_min", "5");
	register_cvar("hap_pickup_ap_max", "20");

	RegisterHam(Ham_TakeDamage, "info_target", "ham_take_damage_entity");
	register_forward(FM_Touch, "fm_touch");
	register_event("DeathMsg", "deathmsg_event", "a");

	MsgidScreenFade = get_user_msgid("ScreenFade");
}

public plugin_precache()
{
	SPR_ITEM_DESTROY = precache_model(ITEM_DESTROY_SPR);

	precache_model(SPR_POTION_HP);
	precache_model(SPR_POTION_MP);

	precache_sound(S_POTION);
	precache_sound(S_ITEMDROP_POTION);

	precache_model(M_L2POTION);
}

public deathmsg_event()
{
	new victim = read_data(2);

	if (random_num(0, 100) <= get_cvar_num("hap_drop_potion_chance"))
	{
		if (get_cvar_num("hap_drop_both_potions"))
		{
			drop_potion(victim, POTION_TYPE_HP);
			drop_potion(victim, POTION_TYPE_AP);
		}
		else
		{
			drop_potion(victim, random_num(POTION_TYPE_HP, POTION_TYPE_AP));
		}
	}
}

public drop_potion(target, type)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"));

	if (!ent)
	{
		server_print("Warning: entity could not be created");
	}

	engfunc(EngFunc_SetModel, ent, M_L2POTION);
	engfunc(EngFunc_SetSize, ent, Float: { -1.0, -1.0, -7.5 }, Float: { 1.0, 1.0, 7.5 }); // {-23.160000, -13.660000, -0.050000}, {11.470000, 12.780000, 6.720000}

	switch (type)
	{
		case POTION_TYPE_HP:
		{
			set_pev(ent, pev_classname, HP_POTION_ENTITY_NAME);
			set_pev(ent, pev_body, POTION_SKIN_LESSER_RED);
		}
		case POTION_TYPE_AP:
		{
			set_pev(ent, pev_classname, AP_POTION_ENTITY_NAME);
			set_pev(ent, pev_body, POTION_SKIN_LESSER_BLUE);
		}
	}

	new Float:origin[3], Float:angles[3];
	pev(target, pev_origin, origin);
	pev(target, pev_angles, angles);

	set_origin_and_drop(ent, origin, angles);

	emit_sound(ent, CHAN_VOICE, S_ITEMDROP_POTION, random_float(0.7, 1.0), ATTN_STATIC, 0, PITCH_NORM);
}

public ham_take_damage_entity(victim, inflicator, attacker, Float:damage, damage_type)
{
	new entityName[32];
	pev(victim, pev_classname, entityName, charsmax(entityName));

	if (pev_valid(victim) && (equali(entityName, HP_POTION_ENTITY_NAME) || equali(entityName, AP_POTION_ENTITY_NAME)))
	{
		destroy_effect(victim);
		entity_remove(victim);
	}
}

public fm_touch(entity, toucher)
{
	new touchedName[32];
	pev(toucher, pev_classname, touchedName, charsmax(touchedName));

	if (!equal(touchedName, "player"))
	{
		return;
	}

	new entityName[32];
	pev(entity, pev_classname, entityName, charsmax(entityName));

	if (equal(entityName, HP_POTION_ENTITY_NAME))
	{
		new hp = get_user_health(toucher);

		if (!(hp >= MAX_PLAYER_HEALTH))
		{
			new hp_add = random_num(get_cvar_num("hap_pickup_hp_min"), get_cvar_num("hap_pickup_hp_max"));

			if (hp_add + hp >= MAX_PLAYER_HEALTH)
				set_user_health(toucher, MAX_PLAYER_HEALTH);
			else
				set_user_health(toucher, hp + hp_add);

			create_effect(toucher, SPR_POTION_HP, 0.4, 204.0, 18.0, 0.6);

			emit_sound(toucher, CHAN_ITEM, S_POTION, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);

			if (!is_user_bot(toucher))
				Create_ScreenFade(toucher, 1<<10, 1<<10, FFADE_IN, 150, 75, 0, 75, false);

			entity_remove(entity);
		}
	}
	else if (equal(entityName, AP_POTION_ENTITY_NAME))
	{
		new ap = get_user_armor(toucher);

		if (!(ap >= MAX_PLAYER_HEALTH))
		{
			new ap_add = random_num(get_cvar_num("hap_pickup_ap_min"), get_cvar_num("hap_pickup_ap_max"));

			if (ap_add + ap >= MAX_PLAYER_HEALTH)
				set_user_armor(toucher, MAX_PLAYER_HEALTH);
			else
				set_user_armor(toucher, ap + ap_add);

			create_effect(toucher, SPR_POTION_MP, 0.4, 204.0, 18.0, 0.6);

			emit_sound(toucher, CHAN_ITEM, S_POTION, VOL_NORM, ATTN_NORM, 0, PITCH_NORM);

			if (!is_user_bot(toucher))
				Create_ScreenFade(toucher, 1<<10, 1<<10, FFADE_IN, 0, 75, 150, 75, false);

			entity_remove(entity);
		}
	}
}

public create_effect(const player, const effect[], Float:size, Float:opacity, Float:framerate, const Float:time)
{
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "env_sprite"));

	if (!ent)
	{
		return;
	}

	set_pev(ent, pev_classname, "dummy");
	engfunc(EngFunc_SetModel, ent, effect);
	engfunc(EngFunc_SetSize, ent, Float: { 0.0, 0.0, 0.0 }, Float: { 0.0, 0.0, 0.0 });
	//engfunc(EngFunc_SetOrigin, ent, origin); // not works with pev_aiment
	set_pev(ent, pev_scale, size);
	set_pev(ent, pev_solid, SOLID_NOT);
	set_pev(ent, pev_movetype, MOVETYPE_FOLLOW);
	set_pev(ent, pev_aiment, player);
	set_pev(ent, pev_rendermode, 5); // 5 - Additive
	set_pev(ent, pev_renderfx, kRenderFxNone);
	set_pev(ent, pev_renderamt, opacity);
	set_pev(ent, pev_animtime, 1.0); //time
	set_pev(ent, pev_frame, 0);
	set_pev(ent, pev_framerate, framerate);
	set_pev(ent, pev_spawnflags, SF_SPRITE_STARTON);
	dllfunc(DLLFunc_Spawn, ent);

	new params[2];
	params[0] = player;
	params[1] = ent;

	set_task(time, "remove_effect", ent + TASKID_EFFECT_REMOVE, params, 2);
}

public remove_effect(params[2], taskid) // params[0] = player, params[1] = effect
{
	if (pev_valid(params[1]))
	{
		entity_remove(params[1]);
	}
}

stock destroy_effect(ent)
{
	if (pev_valid(ent))
	{
		new iorigin[3], Float:origin[3];
		pev(ent, pev_origin, origin);
		origin[2] += 6.0;

		FVecIVec(Float:origin, iorigin);

		Create_TE_SPRITE(0, iorigin, SPR_ITEM_DESTROY, random_num(3, 4), random_num(80, 200));
	}
}

stock set_origin_and_drop(ent, Float:origin[3], Float:angles[3])
{
	origin[0] += random_float(-30.0, 30.0);
	origin[1] += random_float(-30.0, 30.0);
	set_pev(ent, pev_origin, origin);

	angles[1] += random_num(1, 360);
	set_pev(ent, pev_angles, angles);

	set_pev(ent, pev_solid, SOLID_TRIGGER);
	set_pev(ent, pev_movetype, MOVETYPE_TOSS);

	engfunc(EngFunc_DropToFloor, ent);
}

stock Create_ScreenFade(id, duration, holdtime, fadetype, r, g, b, a, bool:reliable)
{
	message_begin(reliable ? MSG_ONE : MSG_ONE_UNRELIABLE, MsgidScreenFade, _, id);
	write_short(duration);
	write_short(holdtime);
	write_short(fadetype);
	write_byte(r);
	write_byte(g);
	write_byte(b);
	write_byte(a);
	message_end();
}

stock Create_TE_SPRITE(id = 0, origin[3], sprite, scale, brightness)
{
	if (id > 0 && id < 33) // show only to 1 player
		message_begin(MSG_ONE, SVC_TEMPENTITY, .player = id);
	else // show to all
		message_begin(MSG_BROADCAST, SVC_TEMPENTITY);

	write_byte(TE_SPRITE);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2]);
	write_short(sprite);
	write_byte(scale);
	write_byte(brightness);
	message_end();
}

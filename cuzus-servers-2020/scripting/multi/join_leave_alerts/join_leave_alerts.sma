#include <amxmodx>
#include <amxmisc>

#define S_SOIL_SHOT				"lineage2/skillsound/soil_shot.wav"
#define SYS_PARTY_INVITE		"lineage2/itemsound/sys_party_invite.wav"
#define SYS_PARTY_LEAVE			"lineage2/itemsound/sys_party_leave.wav"

#define MAX_CHARNAME_LENGTH		33

#define TASKID_PUTINSERVER	100
#define TASKID_CONNECT 200
#define TASKID_DISCONNECTED 300

new MaxPlayers;
new SyncHud;

public plugin_init()
{
	register_plugin("Join Leave Alerts", "2019.02.17", "Geekrainian @ cuzus.games");

	MaxPlayers = get_maxplayers();
	SyncHud = CreateHudSyncObj();
}

public plugin_precache()
{
	register_cvar("jls_enable_sound", "1");

	if (get_cvar_num("jls_enable_sound") > 0)
	{
		precache_sound(S_SOIL_SHOT);
		precache_sound(SYS_PARTY_INVITE);
		precache_sound(SYS_PARTY_LEAVE);
	}
}
public plugin_natives()
{
	register_library("join_leave_alerts_lib");
	register_native("on_disconnect", "native_on_disconnect", 1);
}

public client_connect(id)
{
	set_task(1.0, "connect_delayed", id + TASKID_CONNECT);
}

public connect_delayed(id)
{
	id -= TASKID_CONNECT;

	if (get_cvar_num("jls_enable_sound") && get_user_flags(id) & ADMIN_IMMUNITY)
	{
		client_cmd(0, "spk %s", S_SOIL_SHOT);
		//emit_sound(0, CHAN_AUTO, S_SOIL_SHOT, 0.8, ATTN_STATIC, 0, PITCH_NORM);
	}

	new message[128], name[MAX_CHARNAME_LENGTH];
	get_user_name(id, name, MAX_CHARNAME_LENGTH-1);
	formatex(message, 127, "%s is connecting to server.", name);
	new channel = 3;
	set_hudmessage(225,225,0, 0.8, -1.0, 0, 6.0, 6.0, 0.5, 0.15, channel);
	ShowSyncHudMsg(0, SyncHud, message);
}

public client_putinserver(id)
{
	set_task(1.0, "putinserver_delayed", id + TASKID_PUTINSERVER);
}

public putinserver_delayed(id)
{
	id -= TASKID_PUTINSERVER;

	if (get_cvar_num("jls_enable_sound") == 1)
	{
		for (new pl = 1; pl <= MaxPlayers; pl++)
		{
			if (!is_user_bot(pl))
			{
				new mod[32];
				get_modname(mod, 31);

				if (equali(mod, "valve") || (equali(mod, "cstrike") && get_user_team(pl) == get_user_team(id)))
				{
					client_cmd(pl, "spk %s", SYS_PARTY_INVITE);
				}
			}
		}
	}

	new message[128], name[MAX_CHARNAME_LENGTH];
	get_user_name(id, name, MAX_CHARNAME_LENGTH-1);
	formatex(message, 127, "%s joined the game.", name);
	new channel = 3;
	set_hudmessage(0,225,0, 0.8, -1.0, 0, 6.0, 6.0, 0.5, 0.15, channel);
	ShowSyncHudMsg(0, SyncHud, message);
}

public client_disconnected(id)
{
	set_task(1.0, "disconnected_delayed", id + TASKID_DISCONNECTED);
}

public disconnected_delayed(id)
{
	id -= TASKID_DISCONNECTED;

	native_on_disconnect(id);
}

public native_on_disconnect(id)
{
	if (get_cvar_num("jls_enable_sound") == 1)
	{
		for (new pl = 1; pl <= MaxPlayers; pl++)
		{
			if (!is_user_bot(pl))
			{
				new mod[32];
				get_modname(mod, 31);

				if (equali(mod, "valve") || (equali(mod, "cstrike") && get_user_team(pl) == get_user_team(id)))
				{
					client_cmd(pl, "spk %s", SYS_PARTY_LEAVE);
				}
			}
		}
	}

	new message[128], name[MAX_CHARNAME_LENGTH];
	get_user_name(id, name, MAX_CHARNAME_LENGTH-1);
	formatex(message, 127, "%s has left the game.", name);
	new channel = 3;
	set_hudmessage(225,0,0, 0.8, -1.0, 0, 6.0, 6.0, 0.5, 0.15, channel);
	ShowSyncHudMsg(0, SyncHud, message);
}

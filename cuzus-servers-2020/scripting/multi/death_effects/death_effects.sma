#include <amxmodx>
#include <amxmisc>
#include <engine>
#include <fakemeta>
#include <fakemeta_util>
#include <hamsandwich>

#define TASK_DEATH_SHOW_SPRITE 1111

new const S_DEATH[][] = {
	"lineage2/chrsound/MHFighter_Death.wav",
	"lineage2/chrsound/MHMagician_Death.wav",
	"lineage2/chrsound/MElf_Death.wav",
	"lineage2/chrsound/MDelf_Death.wav",
	"lineage2/chrsound/MDwarf_Death.wav"
};

new SPR_BLOOD, SPR_EF_BAT, SPR_BLOOD_SMOKE;
new gmsgScreenFade, gmsgScreenShake;

public plugin_precache()
{
	for (new k; k < sizeof S_DEATH; k++)
	{
		precache_sound(S_DEATH[k]);
	}

	SPR_BLOOD = precache_model("sprites/blood.spr"); // default sprite
	SPR_EF_BAT = precache_model("sprites/ef_bat.spr");
	SPR_BLOOD_SMOKE = precache_model("sprites/blood_smoke.spr");
}

public plugin_init()
{
	register_plugin("Death Effects", "2019.02.18", "Geekrainian @ cuzus.games");

	RegisterHam(Ham_Killed, "player", "Ham_Killed_Post", 1);

	gmsgScreenFade = get_user_msgid("ScreenFade");
	gmsgScreenShake	= get_user_msgid("ScreenShake");
}

public Ham_Killed_Post(victim, killer, gib)
{
	// Random death sound
	emit_sound(victim, CHAN_VOICE, S_DEATH[random_num(0, sizeof S_DEATH - 1)], random_float(0.8, 1.0), ATTN_NORM, 0, PITCH_NORM);

	// 1st person death effects
	if (!is_user_bot(victim))
	{
		new const R = random_num(60, 255);
		new const G = 0;
		new const B = 0;
		new const A = random_num(60, 255);
		Create_ScreenFade(victim, 2<<12, 1<<11, 0, R, G, B, A, true);

		new const duration = 2500;
		Create_ScreenShake(victim, 255<<14, duration, 255<<14, true);

		/*	iuser1 - (для спектаторов в CS) номер типа режима наблюдения:
			1 - Locked Chase Cam, 2 - Free Chase Cam, 3 - Free Look, 4 - First Person, 5 - Free Overview, 6 - Chase Overview	*/
		set_pev(victim, pev_iuser1, 0); //SPECMODE_ALIVE

		//set_task(2.0, "Camera_setDeathCam", victim+TASK_SET_DEATH_CAM);
	}

	// Nemesis death effect
	if (random_num(0, 100) == 12)
	{
		new origin[3];
		get_user_origin(victim, origin);
		Create_TE_LAVASPLASH(origin);
	}
	// Bubble blood
	else if (random_num(0, 100) == 14)
	{
		new origin[3];
		get_user_origin(victim, origin);

		switch (random_num(1, 8))
		{
			case 1: Create_TE_BUBBLES(origin, origin, 112, SPR_BLOOD, 38, 131);
			case 2: Create_TE_BUBBLES(origin, origin, 34, SPR_BLOOD, 45, 36);
			case 3: Create_TE_BUBBLES(origin, origin, 116, SPR_BLOOD, 39, 31);
			case 4: Create_TE_BUBBLES(origin, origin, 141, SPR_BLOOD, 15, 93);
			case 5: Create_TE_BUBBLES(origin, origin, 55, SPR_BLOOD, 16, 119);
			case 6: Create_TE_BUBBLES(origin, origin, 101, SPR_BLOOD, 19, 35);
			case 7: Create_TE_BUBBLES(origin, origin, 112, SPR_BLOOD, 63, 5);
			case 8: Create_TE_BUBBLES(origin, origin, 138, SPR_BLOOD, 25, 10);
		}
	}
	// Bubble blood in trails
	else if (random_num(0, 100) == 7)
	{
		new origin[3], origin_t[3];
		get_user_origin(victim, origin);
		origin_t = origin;

		switch (random_num(1, 2) == 1)
		{
			case 1:
			{
				origin_t[0] += 77;
				origin_t[1] += 2;
			}
			case 2:
			{
				origin_t[0] += random_num(40, 49);
				origin_t[1] -= random_num(43, 58);
			}
		}

		Create_TE_BUBBLETRAIL(origin, origin_t, random_num(140, 200)/*height*/, SPR_BLOOD, random_num(30, 80)/*count*/, random_num(30, 90)/*speed*/);
	}

	// Sprite
	if (random_num(1, 8) == 1)
	{
		set_task(0.1, "showSpriteOnDeath", victim + TASK_DEATH_SHOW_SPRITE);
	}
}

public showSpriteOnDeath(id)
{
	id -= TASK_DEATH_SHOW_SPRITE;

	new origin[3];
	get_user_origin(id, origin);

	if (random_num(1, 2) == 1)
		Create_TE_SPRITE(0, origin, SPR_EF_BAT, 10, 200); //14
	else
		Create_TE_SPRITE(0, origin, SPR_BLOOD_SMOKE, 5, random_num(100, 255));

	//Create_TE_SPRITE(id=0, origin[3], sprite, scale, brightness)
}

stock Create_ScreenFade(id, duration, holdtime, fadetype, r,g,b,a, bool:reliable)
{
	if (id > 0)
	{
		new MSG_DEST;

		switch (id)
		{
			/*case 0: // to all
			{
				if(reliable) MSG_DEST = MSG_ALL;
				else MSG_DEST = MSG_BROADCAST;
			}*/
			default: // playerid
			{
				if (reliable) MSG_DEST = MSG_ONE;
				else MSG_DEST = MSG_ONE_UNRELIABLE;
			}
		}

		message_begin(MSG_DEST, gmsgScreenFade, _, id);
		write_short(duration); //(1<<12)*duration
		write_short(holdtime); //(1<<12)*holdtime
		write_short(fadetype);
		write_byte(r);
		write_byte(g);
		write_byte(b);
		write_byte(a);
		message_end();
	}
}

stock Create_ScreenShake(id, amount, duration, freq, bool:reliable)
{
	if (id > 0)
	{
		new MSG_DEST;

		switch (id)
		{
			/*case 0: // to all
			{
				if(reliable) MSG_DEST = MSG_ALL;
				else MSG_DEST = MSG_BROADCAST;
			}*/
			default: // playerid
			{
				if(reliable) MSG_DEST = MSG_ONE;
				else MSG_DEST = MSG_ONE_UNRELIABLE;
			}
		}

		message_begin(MSG_DEST, gmsgScreenShake, {0,0,0}, id);
		write_short(amount);
		write_short(duration);
		write_short(freq);
		message_end();
	}
}

stock Create_TE_LAVASPLASH(position[3])
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_LAVASPLASH);
	write_coord(position[0]);
	write_coord(position[1]);
	write_coord(position[2]);
	message_end();
}

stock Create_TE_BUBBLES(start[3], end[3], height, iSprite, count, speed)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_BUBBLES);
	write_coord(start[0]);			// start position
	write_coord(start[1]);
	write_coord(start[2]);
	write_coord(end[0]);				// end position
	write_coord(end[1]);
	write_coord(end[2]);
	write_coord(height);				// float height
	write_short(iSprite);			// Sprite Index
	write_byte(count);				// count
	write_coord(speed);				// speed
	message_end();
}

stock Create_TE_BUBBLETRAIL(min[3], max[3], height, sprite, count, speed)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_BUBBLETRAIL);
	write_coord(min[0]);	// min start position
	write_coord(min[1]);
	write_coord(min[2]);
	write_coord(max[0]);	// max start position
	write_coord(max[1]);
	write_coord(max[2]);
	write_coord(height);	// float height
	write_short(sprite);	// model index
	write_byte(count);		// count
	write_coord(speed);		// speed
	message_end();
}

stock Create_TE_SPRITE(id=0, origin[3], sprite, scale, brightness)
{
	if (id>0 && id<33) // show only to 1 player
		message_begin(MSG_ONE, SVC_TEMPENTITY, .player = id);
	else // show to all
		message_begin(MSG_BROADCAST, SVC_TEMPENTITY);

	write_byte(TE_SPRITE);
	write_coord(origin[0]);
	write_coord(origin[1]);
	write_coord(origin[2]);
	write_short(sprite);
	write_byte(scale);
	write_byte(brightness);
	message_end();
}

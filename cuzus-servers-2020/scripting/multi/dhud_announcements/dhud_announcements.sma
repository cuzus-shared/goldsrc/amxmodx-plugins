// This plugin is compatible with AmxModX < 1.9.x

#include <amxmodx>
#include <amxmisc>
#include <dhudmessage>

#define MAXADS 64
#define MAX_MSG_LEN 256

new ADS[MAXADS][MAX_MSG_LEN];
new currAd = -1;
new adCount = 0;

new MaxPlayers;

public plugin_init()
{
	register_plugin("DHUD Announcements", "2019.02.18", "Geekrainian @ cuzus.games");

	MaxPlayers = get_maxplayers();

	set_task(5.0, "load");
}

public plugin_cfg()
{
	register_cvar("dhud_config", "announcements.txt");
	register_cvar("dhud_messages_at_once", "2");
	register_cvar("dhud_next_ad_delay_min", "10.0");
	register_cvar("dhud_next_ad_delay_max", "10.0");
	register_cvar("dhud_message_colors", "254 152 50; 254 203 50; 203 152 50");
	register_cvar("dhud_allowed_chars", "-_/\\:.,!<>{}()");
}

public load()
{
	new filepath[64];
	new config[256];

	get_configsdir(filepath, 63);
	get_cvar_string("dhud_config", config, 255);
	format(filepath, 63, "%s/%s", filepath, config);

	if (file_exists(filepath))
	{
		new output[MAX_MSG_LEN];

		new fHandle = fopen(filepath, "rt");

		if (!fHandle)
		{
			return;
		}

		for (new i = 0; i < MAXADS && !feof(fHandle); i++)
		{
			fgets(fHandle, output, MAX_MSG_LEN - 1);

			if (output[0] == ';' || !output[0] || isspace(output[0]) || output[0] == 10)
			{
				continue;
			}

			new mod[32];
			get_modname(mod, 31);

			if (equali(mod, "valve"))
			{
				new custom[256];
				get_cvar_string("dhud_allowed_chars", custom, 255);

				new nomatch = 0;

				for (new n = 0; n <= strlen(output); n++)
				{
					new bool:isCustomChar;

					for (new m = 0; m <= strlen(custom); m++)
					{
						if (custom[m] == output[n])
						{
							isCustomChar = true;
							break;
						}
					}

					if (isspace(output[n]) || isCustomChar || isalnum(output[n]))
					{
						// do nothing
					}
					else
					{
						log("Warning: MOD (%s) skipped non ASCII line:", mod);
						log(output);

						nomatch = 1;
						break;
					}
				}

				if (nomatch)
				{
					continue;
				}
			}

			copy(ADS[adCount], MAX_MSG_LEN - 1, output);

			adCount++;
		}

		log("Loaded %d announcements from file", adCount);

		fclose(fHandle);

		if (adCount != 0)
		{
			scheduleNextAd();
		}
		else
		{
			log("Warning: 0 announces to display");
		}
	}
	else
	{
		format(filepath, 63, "Error: %s file does not exists", filepath);
		log(filepath);
	}
}

public scheduleNextAd()
{
	set_task(random_float(get_cvar_float("dhud_next_ad_delay_min"), get_cvar_float("dhud_next_ad_delay_max")), "eventTask");
}

public eventTask()
{
	for (new i = 0; i < adCount; i++)
	{
		new colorsString[512];
		get_cvar_string("dhud_message_colors", colorsString, 511);

		new buffer[32][16];
		explode_string(buffer, 31, 15, colorsString, ';');

		new maxIndex;

		for (new k = 0; k < 31; k++)
        {
			trim(buffer[k]);

			if (!strlen(buffer[k]))
			{
				maxIndex = k - 1;
				break;
			}
		}

		new randomIndex = random_num(0, maxIndex);

		new arg1[4];
		new arg2[4];
		new arg3[4];
		parse(buffer[randomIndex], arg1, charsmax(arg1), arg2, charsmax(arg2), arg3, charsmax(arg3));

		new R = str_to_num(arg1);
		new G = str_to_num(arg2);
		new B = str_to_num(arg3);

		new Float:posX = -1.0;
		new Float:posY = 0.01;

		for (new k = 0; k < get_cvar_num("dhud_messages_at_once"); k++)
		{
			currAd = currAd == adCount - 1 ? 0 : currAd + 1;

			sendDhud(ADS[currAd], R, G, B, posX, posY);

			posY += 0.03;
		}

		break;
	}

	scheduleNextAd();
}

public sendDhud(message[], R, G, B, Float:posX, Float:posY)
{
	// Start from 1 because 0 is the server
	for (new i = 1; i <= MaxPlayers; i++)
	{
		if (is_user_alive(i) && is_user_connected(i) && !is_user_bot(i) && !is_user_hltv(i))
		{
			//ClearDHUDMessages(i);

			// http://amxxmodx.ru/core/amxmodxinc/48-hud-soobschenie-iroku-funkcii-set_hudmessage-i-show_hudmessage.html

			new effects = 0;
			new Float:fxtime = 1.0;
			new Float:holdtime = 3.0;
			new Float:fadeInTime = 0.5;
			new Float:fadeOutTime = 2.0;
			new bool:reliable = false;

			set_dhudmessage(R, G, B, posX, posY, effects, fxtime, holdtime, fadeInTime, fadeOutTime, reliable);

			//show_dhudmessage( index, const message[], any:... )
			show_dhudmessage(i, message);

			client_print(i, print_chat, message);
		}
	}
}

stock ClearDHUDMessages(pId, iClear = 8)
{
	for (new iDHUD = 0; iDHUD < iClear; iDHUD++)
	{
		show_dhudmessage(pId, "");
	}
}

stock log(const str[], any:...)
{
	new msg[512];
	vformat(msg, 511, str, 2); // 2 - position of arg with "any:..."

	new tag[8] = "[DEBUG]";
	format(msg, 511, "%s %s", tag, msg);

	new hh, mm, ss;
	time(hh,mm,ss);
	format(msg, 511, "%02d:%02d:%02d %s", hh, mm, ss, msg);

	server_print(msg);
}

// https://forums.alliedmods.net/showthread.php?t=61324
stock explode_string(Output[][], Max, Size, Input[], Delimiter)
{
    new Idx, l = strlen(Input), Len;

    do Len += (1 + copyc(Output[Idx], Size, Input[Len], Delimiter));
    while((Len < l) && (++Idx < Max))

    return Idx;
}

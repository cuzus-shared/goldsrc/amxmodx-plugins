#include <amxmodx>
#include <amxmisc>

#define XX 32
#define MAX_CHARNAME_LENGTH 64

#define MULTIKILL_TIMESPAN 12.0

#define NEXT_MESSAGE_DELAY 2.0

new KillStreakSyncHud1, KillStreakSyncHud2;

new KS_FirstBlood = false;
new KS_TotalKills[XX];
new Float:MultiKillTime[XX];
new Float:LastMessageTime;
new KS_RevengeKill[XX][MAX_CHARNAME_LENGTH];

// NOTE: Do not change array due to index usage
new const SOUNDS[][] =
{
	"ut_killstreak/firstblood.wav",
	"ut_killstreak/firstblood2.wav",
	"ut_killstreak/firstblood3.wav",

	"ut_killstreak/doublekill.wav",
	"ut_killstreak/doublekill2.wav",

	"ut_killstreak/triplekill.wav",
	"ut_killstreak/killingspree.wav",
	"ut_killstreak/ultrakill.wav",
	"ut_killstreak/dominating.wav",
	"ut_killstreak/rampage.wav",
	"ut_killstreak/megakill.wav",

	"ut_killstreak/unstoppable.wav",
	"ut_killstreak/whickedsick.wav",
	"ut_killstreak/monsterkill.wav",
	"ut_killstreak/godlike.wav",
	"ut_killstreak/holyshit.wav",

	"ut_killstreak/suicide.wav",
	"ut_killstreak/payback.wav"
};

public plugin_init()
{
	register_plugin("UT Kill Streak Sounds", "2019.03.11", "Geekrainian @ cuzus.games");

	KillStreakSyncHud1 = CreateHudSyncObj();
	KillStreakSyncHud2 = CreateHudSyncObj();

	register_event("DeathMsg", "event_deathmsg", "a");

	new modName[32];
	get_modname(modName, charsmax(modName));

	if (equali(modName, "cstrike"))
	{
		// This logevents are only for Counter-Strike
		register_logevent("event_round_start", 2, "1=Round_Start");
	}
}

public plugin_precache()
{
	for (new k; k < sizeof SOUNDS; k++)
	{
		precache_sound(SOUNDS[k]);
	}
}

public client_disconnect(id)
{
	reset_stat(id);
}

reset_stat(id)
{
	KS_TotalKills[id] = 0;
	MultiKillTime[id] = 0.0;
	KS_RevengeKill[id] = "";
}

public event_round_start()
{
	KS_FirstBlood = false;

	new playerIndex[32], playersCount, i;
	get_players(playerIndex, playersCount, "h"); // skip hltv

	for (i = 0; i < playersCount; i++)
	{
		reset_stat(playerIndex[i]);
	}
}

public event_deathmsg()
{
	new killer = read_data(1);
	new victim = read_data(2);
	new headshot = read_data(3);

	on_player_kill(killer, victim, headshot);
}

public on_player_kill(killer, victim, headshot)
{
	KS_TotalKills[victim] = 0;
	MultiKillTime[victim] = 0.0;

	if (!killer)
	{
		return;
	}

	new victim_name[MAX_CHARNAME_LENGTH];
	get_user_name(victim, victim_name, MAX_CHARNAME_LENGTH-1);

	new R = random_num(0, 255);
	new G = random_num(0, 255);
	new B = random_num(0, 255);
	new Float:x = -1.0;
	new Float:y = 0.27;
	new effects = 0;
	new Float:fxtime = 6.0;
	new Float:holdtime = 2.0;

	// Suicide
	if (victim == killer)
	{
		// pink
		R = 255;
		G = 105;
		B = 180;
		set_hudmessage(R, G, B, x, y, effects, fxtime, holdtime);

		ShowSyncHudMsg(0, KillStreakSyncHud2, "%s blew up.", victim_name);
		//play_sound(SOUNDS[16]);
	}
	else
	{
		new killer_name[MAX_CHARNAME_LENGTH];
		get_user_name(killer, killer_name, MAX_CHARNAME_LENGTH-1);

		KS_TotalKills[killer]++;
		KS_RevengeKill[victim] = killer_name;

		// Revenge kill
		if (equal(victim_name, KS_RevengeKill[killer]))
		{
			R = 124;
			G = 252;
			B = 0;
			set_hudmessage(R, G, B, x, y, effects, fxtime, holdtime);

			ShowSyncHudMsg(killer, KillStreakSyncHud2, "You got avenged on %s!", victim_name);

			client_cmd(killer, "spk %s", SOUNDS[17]);

			KS_RevengeKill[killer] = "";

			R = 220;
			G = 20;
			B = 60;
			set_hudmessage(R, G, B, x, y, effects, fxtime, holdtime);

			ShowSyncHudMsg(victim, KillStreakSyncHud2, "%s got avenged on you!", killer_name);
		}

		/*
		if(headshot && g_iHeadshot){
			ShowSyncHudMsg(0, KillStreakSyncHud2, " ", killer_name, victim_name);
		}
		*/

		// First blood
		if (!KS_FirstBlood)
		{
			//if (getTeam(victim) == getTeam(killer))
			//	return;

			KS_FirstBlood = true;

			R = 255;
			G = 0;
			B = 0;
			set_hudmessage(R, G, B, x, y, effects, fxtime, holdtime);
			ShowSyncHudMsg(0, KillStreakSyncHud2, "%s made First Blood!!!", killer_name); //%s just drew First Blood!

			play_sound(SOUNDS[random_num(0, 2)]);

			return;
		}

		// Team kill
		//if (getTeam(victim) == getTeam(killer))
		//{
		//	ShowSyncHudMsg(0, KillStreakSyncHud2, "%s is probably delusional...", killer_name);
		//	return;
		//}

		new Float:nowtime = get_gametime();

		if (nowtime - LastMessageTime > NEXT_MESSAGE_DELAY)
		{
			new bool:isTimerKill = nowtime - MultiKillTime[killer] <= MULTIKILL_TIMESPAN;

			if (isTimerKill)
			{
				MultiKillTime[killer] = nowtime;
			}

			new streak_msg[256];

			switch (KS_TotalKills[killer])
			{
				case 2:
				{
					if (isTimerKill)
					{
						streak_msg = "%s made a double kill!";
						play_sound(SOUNDS[random_num(3, 4)]);
					}
				}
				case 3:
				{
					if (isTimerKill)
					{
						streak_msg = "%s is on triple kill!";
						play_sound(SOUNDS[5]);
					}
					else
					{
						streak_msg = "%s is on a killing spree!";
						play_sound(SOUNDS[6]);
					}
				}
				case 4:
				{
					if (isTimerKill)
					{
						streak_msg = "%s made an ultra kill!";
						play_sound(SOUNDS[7]);
					}
					else
					{
						streak_msg = "%s is dominating!";
						play_sound(SOUNDS[8]);
					}
				}
				case 5:
				{
					if (isTimerKill)
					{
						streak_msg = "%s is on rampage!";
						play_sound(SOUNDS[9]);
					}
					else
					{
						streak_msg = "%s made a mega kill!";
						play_sound(SOUNDS[10]);
					}
				}
				case 6:
				{
					streak_msg = "%s is unstoppable!";
					play_sound(SOUNDS[11]);
				}
				case 7:
				{
					streak_msg = "%s is whicked sick!";
					play_sound(SOUNDS[12]);
				}
				case 8:
				{
					streak_msg = "%s made a monster kill!";
					play_sound(SOUNDS[13]);
				}
				case 9:
				{
					streak_msg = "%s is G o d L i k e !!!";
					play_sound(SOUNDS[14]);
				}
				case 10:
				{
					streak_msg = "Holy shit! %s got another one!";
					play_sound(SOUNDS[15]);
				}
				case 11..100:
				{
					streak_msg = "%s is beyond GODLIKE. Someone KILL HIM!!!";
					play_sound(SOUNDS[14]);
				}
				//streak_msg = "%s is on multikill!";
				//streak_msg = "The (name of TEAM) are OWNING!"; // Ownage - 5 or more kills in a row by one team without them losing any heroes.
			}

			if (strlen(streak_msg) > 0)
			{
				new R = random_num(0, 255);
				new G = random_num(0, 255);
				new B = random_num(0, 255);
				y = 0.24;

				set_hudmessage(R, G, B, x, y, effects, fxtime, holdtime);

				ShowSyncHudMsg(0, KillStreakSyncHud1, streak_msg, killer_name);

				LastMessageTime = nowtime;
			}
		}
	}
}

public play_sound(sound[])
{
	new players[32], pnum;
	get_players(players, pnum, "ch");
	new i;

	for (i = 0; i < pnum; i++)
	{
		if (!is_user_connected(players[i]))
		{
			continue;
		}

		client_cmd(players[i], "spk %s", sound);
	}
}

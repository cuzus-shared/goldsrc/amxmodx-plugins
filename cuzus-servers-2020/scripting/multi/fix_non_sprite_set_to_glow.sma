#include <amxmodx>
#include <engine>

public plugin_init()
{
	register_plugin("Non-sprite set to glow! Fix", "1.0", "xPaw");

	new szModel[2],
	iEntity = get_maxplayers(),
	iMaxEntities = get_global_int(GL_maxEntities);

	while(++iEntity <= iMaxEntities)
	{
		if(is_valid_ent(iEntity) && entity_get_int(iEntity, EV_INT_rendermode) == kRenderGlow)
		{
			entity_get_string(iEntity, EV_SZ_model, szModel, 1);

			if(szModel[ 0 ] == '*')
				entity_set_int(iEntity, EV_INT_rendermode, kRenderNormal);
		}
	}
}

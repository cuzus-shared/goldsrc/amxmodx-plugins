#include <amxmodx>
//#include <cstrike>
#include <fakemeta>
#include <hamsandwich>

#define DISPLAY_MODE_ANY		1	// Show for all players
#define DISPLAY_MODE_ALLIED		2	// Show only for allied players
#define DISPLAY_MODE_ENEMY		3	// Show only on enemy players

#define VISIBILITY_ANY			1	// Visible forever
#define VISIBILITY_ALIVE		2	// Visible only if alive
#define VISIBILITY_DEAD			3	// Visible only if dead

#define HEALTHBAR_PLAYER	"sprites/lineage2/ps_hpbar.spr"
//#define HEALTHBAR_ADMIN		"sprites/lineage2/ps_expbar.spr"

new MaxPlayers;
new HealthBar[33];
new IsAlive[33];
//new CsTeams:PlayerTeam[33];
new PlayerTeam[33][16];

public plugin_precache()
{
	precache_model(HEALTHBAR_PLAYER);
//	precache_model(HEALTHBAR_ADMIN);
}

public plugin_init()
{
	register_plugin("Health Bars", "2019.02.18", "Geekrainian @ cuzus.games");

	register_cvar("hb_max_health", "100.0");
	register_cvar("hb_bar_scale", "0.3"); //0.1
	register_cvar("hb_bar_head_offset", "45.0"); //35.0
	register_cvar("hb_bar_display_mode", "1");
	register_cvar("hb_bar_visibility", "1");

	register_forward(FM_AddToFullPack, "fw_FM_AddToFullPack", 1);

	RegisterHam(Ham_Spawn, "player", "Ham_Spawn_Player", 1);

	register_event("DeathMsg", "evDeathMsg", "a");
	register_event("Health", "evHealth", "be");

	MaxPlayers = get_maxplayers();

	for (new i = 1; i <= MaxPlayers; i++)
	{
		HealthBar[i] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "env_sprite"));

		if (!pev_valid(HealthBar[i]))
		{
			continue;
		}

		engfunc(EngFunc_SetModel, HealthBar[i], HEALTHBAR_PLAYER);
		set_pev(HealthBar[i], pev_movetype, MOVETYPE_FOLLOW);
		set_pev(HealthBar[i], pev_aiment, i);

		set_pev(HealthBar[i], pev_scale, get_cvar_float("hb_bar_scale"));

		//set_pev(HealthBar[i], pev_renderfx, kRenderFxGlowShell);
		set_pev(HealthBar[i], pev_rendercolor, Float:{ 0.0, 0.0, 0.0 });
		set_pev(HealthBar[i], pev_rendermode, kRenderTransAlpha); //kRenderTransAdd
		set_pev(HealthBar[i], pev_renderamt, 0.0);
	}
}

public client_putinserver(id)
{
	IsAlive[id] = 0;
	//PlayerTeam[id] = CS_TEAM_SPECTATOR;
	get_user_team(id, PlayerTeam[id], 15);

	/*if(get_user_flags(id) & ADMIN_IMMUNITY){
		engfunc(EngFunc_SetModel, HealthBar[id], HEALTHBAR_ADMIN);
	}*/
}

public client_disconnect(id)
{
	IsAlive[id] = 0;
	//PlayerTeam[id] = CS_TEAM_UNASSIGNED;
	get_user_team(id, PlayerTeam[id], 15);

	/*if(get_user_flags(id) & ADMIN_IMMUNITY){
		engfunc(EngFunc_SetModel, HealthBar[id], HEALTHBAR_PLAYER);
	}*/
}

public fw_FM_AddToFullPack(es, e, ent, host, flags, player, pSet)
{
	static i;

	for (i = 1; i <= MaxPlayers; i++)
	{
		if (HealthBar[i] == ent)
		{
			if (i == host
			|| !IsAlive[i]
			|| (get_cvar_num("hb_bar_display_mode") == DISPLAY_MODE_ALLIED && !equali(PlayerTeam[i], PlayerTeam[host])) //PlayerTeam[i] != PlayerTeam[host]
			|| (get_cvar_num("hb_bar_display_mode") == DISPLAY_MODE_ENEMY && equali(PlayerTeam[i], PlayerTeam[host])) //PlayerTeam[i] == PlayerTeam[host]
			|| (get_cvar_num("hb_bar_visibility") == VISIBILITY_ALIVE && !IsAlive[host]) || (get_cvar_num("hb_bar_visibility") == VISIBILITY_DEAD && IsAlive[host]))
			{
				return;
			}

			static Float:fOrigin[3];
			pev(i, pev_origin, fOrigin);
			fOrigin[2] += get_cvar_float("hb_bar_head_offset");

			set_es(es, ES_AimEnt, 0);
			set_es(es, ES_Origin, fOrigin);
			set_es(es, ES_RenderAmt, 255.0);
		}
	}
}

public Ham_Spawn_Player(id)
{
	if (is_user_alive(id))
	{
		IsAlive[id] = 1;
		get_user_team(id, PlayerTeam[id], 15);
		//PlayerTeam[id] = cs_get_user_team(id);
		set_pev(HealthBar[id], pev_frame, get_cvar_float("hb_max_health")); // MAX_HEALTH as frame
	}
}

public evDeathMsg()
{
	static id;

	id = read_data(2);
	IsAlive[id] = 0;
	//PlayerTeam[id] = cs_get_user_team(id);
	get_user_team(id, PlayerTeam[id], 15);
}

public evHealth(id)
{
	static hp;

	hp = get_user_health(id);
	set_pev(HealthBar[id], pev_frame, float(hp) - (hp >= 1 ? 1.0 : 0.0)); // bugfix: frame offset -1

	//new Float:maxHealth = get_cvar_float("hb_max_health");
	//if (hp > maxHealth)
	//	hp = maxHealth;
	//	set_pev(HealthBar[id], pev_frame, hp * 128 / maxHealth - 1.0);
}

stock log(const str[], any:...)
{
	new msg[512];
	vformat(msg, 511, str, 2); // 2 - position of arg with "any:..."

	new tag[8] = "[DEBUG]";
	format(msg, 511, "%s %s", tag, msg);

	new hh, mm, ss;
	time(hh,mm,ss);
	format(msg, 511, "%02d:%02d:%02d %s", hh, mm, ss, msg);

	server_print(msg);
}

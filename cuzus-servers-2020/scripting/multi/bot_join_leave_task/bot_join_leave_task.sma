#include <amxmodx>
#include <amxmisc>
#include <fakemeta>
#include <join_leave_alerts_lib> // on_disconnect

new bool:isMapStarted;

public plugin_cfg()
{
	register_cvar("bjlt_bot_add_command", "jk_botti addbot");
	register_cvar("bjlt_min_bots", "2");
	register_cvar("bjlt_max_bots", "17");
	register_cvar("bjlt_join_chance", "60");
	register_cvar("bjlt_random_time_min", "60.0");
	register_cvar("bjlt_random_time_max", "500.0");
}

public plugin_init()
{
	register_plugin("Bot Join Leave Task", "2019.02.17", "Geekrainian @ cuzus.games");

	set_task(5.0, "scheduler");
}

public scheduler()
{
	if (!isMapStarted)
	{
		isMapStarted = true;

		new addCount = get_cvar_num("bjlt_min_bots");

		if (addCount > 0)
		{
			addCount = random_num(addCount, addCount);
		}

		log("Creating %d bots on init", addCount);

		for (new i = 0; i < addCount; i++)
		{
			add_server_bot();
		}
	}
	else
	{
		if (random_num(0, 100) <= get_cvar_num("bjlt_join_chance"))
		{
			add_server_bot();
		}
		else
		{
			kick_server_bot();
		}
	}

	new Float:delay = random_float(get_cvar_float("bjlt_random_time_min"), get_cvar_float("bjlt_random_time_max"));

	log("Scheduled next task in %fs", delay);

	set_task(delay, "scheduler"); //, _, _, "a"
}

public add_server_bot()
{
	new bots[32], botsCount;
	get_players(bots, botsCount, "dh"); // skip real players, skip hltv

	if (botsCount >= get_cvar_num("bjlt_max_bots"))
	{
		log("Cannot add bot due to max bots setting");
		return;
	}

	new cmd[64];
	get_cvar_string("bjlt_bot_add_command", cmd, 63);
	server_cmd(cmd);

	log("Added new bot");
}

public kick_server_bot()
{
	new botsIndex[32], botsCount, botid, botuserid;
	get_players(botsIndex, botsCount, "dh"); // skip real players, skip hltv

	if (botsCount <= get_cvar_num("bjlt_min_bots"))
	{
		log("Cannot kick bot due to min bots setting");
		return;
	}

	for (new i; i < botsCount; i++)
	{
		botid = botsIndex[i];
		botuserid = get_user_userid(botid);

		on_disconnect(botid); // native

		server_cmd("kick #%d", botuserid);
		log("Kicked bot #%d", botuserid);

		break;
	}
}

public check_server_slot()
{
	new players[32], playersCount;
	get_players(players, playersCount, "h"); // skip hltv

	new maxPlayers = get_maxplayers();

	if (playersCount == maxPlayers || playersCount == maxPlayers - 1)
	{
		log("Scheduled bot kick due to max players");

		new Float:minDelay = get_cvar_float("bjlt_random_time_min");
		new Float:delay = random_float(minDelay, minDelay * 3);
		set_task(delay, "kick_server_bot");
	}
}

public client_putinserver(id)
{
	if (!is_user_bot(id))
	{
		//log("client_putinserver %d", id);
		check_server_slot();
	}
	else
	{
		// https://forums.alliedmods.net/showpost.php?p=2563147&postcount=33
		// https://amx-x.ru/viewtopic.php?t=2236&p=22883#p22280
		//set_pev(id, pev_flags, pev(id, pev_flags) & ~FL_FAKECLIENT);
		//set_user_info(id, "*bot", "0");
	}
}

public client_disconnect(id)
{
	if (!is_user_bot(id))
	{
		//log("client_disconnected %d", id);
	}
}

stock log(const str[], any:...)
{
	new msg[512];
	vformat(msg, 511, str, 2); // 2 - position of arg with "any:..."

	new tag[8] = "[DEBUG]";
	format(msg, 511, "%s %s", tag, msg);

	new hh, mm, ss;
	time(hh,mm,ss);
	format(msg, 511, "%02d:%02d:%02d %s", hh, mm, ss, msg);

	server_print(msg);
}

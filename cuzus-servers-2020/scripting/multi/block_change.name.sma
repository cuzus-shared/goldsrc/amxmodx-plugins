#include <amxmodx> 
#include <fakemeta> 

public plugin_init()
{
    register_plugin("One Name", "0.1.0", "ConnorMcLeod");
    register_forward(FM_ClientUserInfoChanged, "ClientUserInfoChanged");
}

public ClientUserInfoChanged(id)
{ 
	if (is_user_admin(id))
	{
		return FMRES_IGNORED;
	}
	
	static szOldName[32], szNewName[32];
	pev(id, pev_netname, szOldName, charsmax(szOldName));

	if (szOldName[0])
	{
		get_user_info(id, "name", szNewName, charsmax(szNewName));

		if (!equal(szOldName, szNewName))
		{
			set_user_info(id, "name", szOldName);
			
			return FMRES_HANDLED;
		}
	}

	return FMRES_IGNORED;
}
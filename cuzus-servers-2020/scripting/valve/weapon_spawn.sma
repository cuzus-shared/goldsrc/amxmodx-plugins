#include <amxmodx>
#include <engine>
#include <hamsandwich>

new Trie:g_owners

new items[][] = {
	"ammo_357",
	"ammo_9mmAR",
	"ammo_9mmbox",
	"ammo_9mmclip",
	"ammo_ARgrenades",
	"ammo_buckshot",
	"ammo_crossbow",
	"ammo_gaussclip",
	"ammo_glockclip",
	"ammo_mp5clip",
	"ammo_mp5grenades",
	"ammo_rpgclip",
	"item_battery",
	"item_healthkit",
	"item_longjump"
}

new wps[][] = {
	"weapon_hornetgun",
	"weapon_python",
	"weapon_357",
	"weapon_crossbow",
	"weapon_snark",
	"weapon_tripmine",
	"weapon_satchel",
	"weapon_handgrenade",
	"weapon_9mmAR",
	"weapon_gauss",
	"weapon_mp5",
	"weapon_rpg",
	"weapon_egon",
	"weapon_shotgun"
}

new render_type,render_fx,render_amt,render_color

public plugin_init() {
	register_plugin("Weapon Spawn Effects","0.2.1","serfreeman1337")
	
	for(new i;i<sizeof items;++i){
		RegisterHam(Ham_Respawn,items[i],"fw_Respawn",1)
		RegisterHam(Ham_Think,items[i],"fw_Think",1)
	}
	
	for(new i;i<sizeof wps;++i){
		register_touch(wps[i],"worldspawn","fw_Respawn")
		RegisterHam(Ham_Think,wps[i],"fw_Think",1)
	}
	
	render_type = register_cvar("ws_render","1")
	render_fx = register_cvar("ws_renderfx","19")
	render_amt = register_cvar("ws_renderamt","6.0")
	render_color = register_cvar("ws_rendercolor","random")
	
	g_owners = TrieCreate()
}

public plugin_end()
{
	TrieDestroy(g_owners)
}

public fw_Respawn(ent){
	if(!is_valid_ent(ent))
		return
	
	new classname[32],Float:origin[3],Float:angles[3]
	
	entity_get_string(ent,EV_SZ_classname,classname,charsmax(classname))
	entity_get_vector(ent,EV_VEC_origin,origin)
	entity_get_vector(ent,EV_VEC_angles,angles)
	
	new ghost = create_entity(classname)
	
	if(!is_valid_ent(ghost))
		return
		
	new dnum[5],Float:rnd[3]
	num_to_str(ent,dnum,charsmax(dnum))
	TrieSetCell(g_owners,dnum,ghost)
	
	get_rendercolor(rnd)
		
	entity_set_int(ghost,EV_INT_rendermode,get_pcvar_num(render_type))
	entity_set_int(ghost,EV_INT_renderfx,get_pcvar_num(render_fx))
	entity_set_vector(ghost,EV_VEC_rendercolor,rnd)
	entity_set_float(ghost,EV_FL_renderamt,get_pcvar_float(render_amt))

	DispatchSpawn(ghost)
	
	entity_set_int(ghost,EV_INT_solid,SOLID_NOT)
	entity_set_int(ghost,EV_INT_movetype,MOVETYPE_FLY)
	entity_set_float(ghost,EV_FL_nextthink,0.0)

	
	entity_set_origin(ghost,origin)
	entity_set_vector(ghost,EV_VEC_angles,angles)
}

public fw_Think(ent){
	new dnum[5],ghost
	num_to_str(ent,dnum,charsmax(dnum))
	
	TrieGetCell(g_owners,dnum,ghost)
	
	if(ghost)
	{
		TrieDeleteKey(g_owners,dnum)
		
		if(is_valid_ent(ghost))
		{
			remove_entity(ghost)
		}
	}
}

get_rendercolor(Float:rnd[3]){
	new temp[36]
	get_pcvar_string(render_color,temp,charsmax(temp))
	
	if(equal(temp,"random")){
		rnd[0] = random_float(10.0,255.0)
		rnd[1] = random_float(10.0,255.0)
		rnd[2] = random_float(10.0,255.0)
	}else{
		new s[3][6]
		parse(temp,s[0],charsmax(s[]),s[1],charsmax(s[]),s[2],charsmax(s[]))
		
		rnd[0] = str_to_float(s[0])
		rnd[1] = str_to_float(s[1])
		rnd[2] = str_to_float(s[2])
	}
}
